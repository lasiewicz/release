﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" 
Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SubAdmin.Promo>" %>


<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">


    <%--Search Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", this.ViewData.Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	PromoDetail
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="float:right;width:85%;">
    <h2>PromoDetail</h2>
     <br /><br />
    <%if (ViewData["PageMessage"] != null)
    {%>
        <%=Html.Encode(ViewData["PageMessage"])%><br/>
    <%} %>

    <%--Display Clone and Edit promo buttons if we are not in edit mode--%>
    <% if (ViewData["PageMode"] == null || ViewData["PageMode"].ToString() != "edit")
       {%>
    <%=Html.ActionLink("Clone Promo", "ClonePromo", "SubAdmin",
                                             new {promoId = Model.PromoId, brandId = Model.BrandId}, null)%>
    <%=Html.ActionLink("Edit End Date", "EditPromoEndDate", "SubAdmin",
                                             new {promoId = Model.PromoId, brandId = Model.BrandId}, null)%>
    <% } %>
     <br /><br />
    <%--Promo info display--%>
    <% if (Model.PromoDetailLineItems != null)
       {%>
        <ul>
           <%foreach (var lineItem in Model.PromoDetailLineItems)
           {%>
                <%if (lineItem.IsLiteral)
                   {%>
                     <li class="list-item-edit">
                        <label class="memberLabel"><%=lineItem.LabelText%></label><span style="padding-left:35px;"><%=lineItem.LineItemText%></span>
                     </li>
                <% }
                  else
                   { %>
                    <li class="list-item-edit">
                        <label class="memberLabel"><%=Html.Encode(lineItem.LabelText)%></label><span style="padding-left:35px;"><%=Html.Encode(lineItem.LineItemText)%></span>
                    </li>
                <% } %>
        <% } %>
        </ul>
    <%} %>

    <%--For editing the end date, we need to display the date control and the button to submit--%>
    <% if (ViewData["PageMode"] != null)
       {
           if (ViewData["PageMode"].ToString() == "edit")
           {%>
                <% Html.BeginForm("CompleteEditPromoEndDate", "SubAdmin"); %>
                    <%=Html.HiddenFor(m => m.PromoId)%>
                    <%=Html.HiddenFor(m => m.BrandId)%>
                    <%=Html.TextBox("newEndDate", Model.PromoEndDate, new { @class = "date-field" })%>
                    <input type="submit" value="Update"/>
                    <%=Html.ActionLink("Cancel", "PromoDetail", "SubAdmin", new { promoId = Model.PromoId, brandId = Model.BrandId}, null) %>
                <% Html.EndForm();%>

    <%      }
       } %>

</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        // date fields
        $(".date-field").datepicker({ dateFormat: 'mm/dd/yy', changeMonth: true, changeYear: true });
    });
</script>
</asp:Content>




