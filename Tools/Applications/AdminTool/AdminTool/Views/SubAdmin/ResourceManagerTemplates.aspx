﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SubAdmin.ResourceManagerTemplates>"  %>


<asp:Content ID="ContentTitle" ContentPlaceHolderID="TitleContent" runat="server">
	Spark Networks Web Admin - Resource Template Manager
</asp:Content>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="float:right;width:85%;">
        <div class="template-query">
            <div id="statusmsg" style="font-weight: bold; color:Red; margin-bottom:10px;">
                <%= ViewData["Message"] %>
            </div>
            <a href="javascript:TemplateDetail()">Create New</a>
            <br/><br/>
            <span> Brand ID: </span>
            <%= Html.DropDownListFor(m => m.BrandId, new SelectList(Model.Brands, "Value", "Text", Model.BrandId))%>
            <span> Type: </span>
            <%= Html.DropDownListFor(m => m.TemplateType, Model.TemplateTypes) %>
            <br/><br/>
            <div id="results-templates">
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="ContentHead" ContentPlaceHolderID="HeadContent" runat="server">
 
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var brandId = $('#BrandId').val();
            var templateTypeId = $('#TemplateType').val();
            if(brandId >  0 && templateTypeId > 0) {
                GetResourceTemplates();
            }

            $('#TemplateType').change(function () {
                $('#statusmsg').text('');
                GetResourceTemplates();
            });
            $('#BrandId').change(function () {
                $('#statusmsg').text('');
                GetResourceTemplates();
            });
        });
        
        function GetResourceTemplates() {
            var brandId = $('#BrandId').val();
            var templateTypeId = $('#TemplateType').val();  
            var templateType = $('#TemplateType option:selected').text();
            var resultDiv = $('#results-templates');
            resultDiv.html('');
            
            if (templateTypeId > 0) {
                resultDiv.addClass('loading-bg');
                $.ajax({
                    type: "POST",
                    url: "/SubAdminAPI/GetResourceTemplateCollection",
                    data: { BrandId: brandId, TemplateTypeID: templateTypeId },
                    success: function (result) {
                        var table = $("<table id='search-results-table'>");
                        if (result.length > 0) {
                            var head = $("<tr>");
                            head.append($("<td>").text("ID")).append($("<td>").text("Content")).append($("<td>").text("Brand ID")).append($("<td>").text("Description")).append($("<td>").text("Type"));
                            table.append((head));
                        }
                        $.each(result, function (i, template) {
                            var row = $("<tr>");
                            row.append($("<td>").html("<a href='/SubAdmin/TemplateDetail?templateId=" + template.ResourceTemplateID + "'>" + template.ResourceTemplateID + "</a>"));
                            row.append($("<td>").text(template.Content));
                            row.append($("<td>").text(template.GroupID));
                            row.append($("<td>").text(template.Description));
                            row.append($("<td>").text(templateType));
                            table.append(row);
                        });
                        resultDiv.removeClass('loading-bg').html(table);
                    }
                });
            }
            }
        
        function TemplateDetail() {
            var brandId = $('#BrandId').val();
            var templateTypeId = $('#TemplateType').val();
            window.location.href = "/SubAdmin/TemplateDetail?brandId=" + brandId + "&templateType=" + templateTypeId;
        }
    </script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
