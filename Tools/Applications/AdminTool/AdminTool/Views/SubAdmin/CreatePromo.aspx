﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SubAdmin.Promo>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	CreatePromo
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">


    $(document).ready(function () {
        $('.plan-selector-dropdown').change(function () {
            GetPlan($('.brandID').val(), $(this).val());
        });
    });



</script>

<div style="float:right;width:85%;">
<div style="width:100%;float:left;">

<br/> <h2>CreatePromo</h2><br />
   
    <div style="color:Red;">
        <%= ViewData["PageMessage"] %>
        <br /><br />
    </div> 
    <% Html.BeginForm("CompleteCreatePromo", "SubAdmin"); %>
    
    <%=Html.Hidden("PromoStep", ViewData["PromoStep"], new {@class="promo-step"}) %>
    <%=Html.HiddenFor(m => m.SiteId, new { @class = "site-id-holder"}) %>
    <%=Html.HiddenFor(m => m.PromoApprovalStatus) %>
    <%=Html.HiddenFor(m => m.PromoId) %>
    <%=Html.HiddenFor(m => m.RecurrenceCronExpression) %>

    <div class="create-promo-step1 create-promo-step"> 
     <span class="gray_header_text">STEP ONE (BASIC INFO)</span> <br /><br />

    <div class="plan-input">
     <ul>
        <li class="list-item-edit">
            <label class="memberLabel">Select Web Site:</label>
                <span style="padding-left:5px;">
                    <%=Html.DropDownListFor(m=>m.BrandId, Model.BrandIdChoices) %>
                </span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Promo Name:</label> 
            <span style="padding-left:5px;">
                <%=Html.TextBoxFor(m => m.Description, new { style = "width: 450px;" })%>
            </span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel"></label> 
            <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Single column with <br /> multiple plan types? </label> 
            <span style="padding-left:5px;">
                <%=Html.CheckBoxFor(m=>m.IsSingleColWithMultPlanTypes, new {@class="single-col-with-multi-plantypes"})  %> 
            </span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel"></label> 
            <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Rows:</label> 
            <span style="padding-left:5px;">
                <%=Html.DropDownListFor(m=>m.RowDimension, Model.RowChoices) %>
            </span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Columns:</label> 
            <span style="padding-left:5px;">
                <%=Html.DropDownListFor(m=>m.ColDimension, Model.ColChoices, new { @class = "column-dimension"}) %><%=Html.HiddenFor(m => m.ColDimension, new { @class = "column-dimension-holder" })%>
                </span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel"></label> 
            <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Payment Type:</label>
            <span style="">
                  <table>
                    <%foreach (var p in Model.PaymentTypeChoices)
                        { %>
                            <tr>
                                <td><%=Html.RadioButtonFor(m=>m.PaymentType, p.Value, new {@class="payment-type-radio"})%></td>
                                <td valign="top"><span style="padding-left:5px;"><%=p.Text %></span></td> 
                            </tr>
                    <% } %>
                 </table>
                <input type="hidden" class="prev-radiobuttonlist-set" value="<%=(int)Model.PaymentType %>"/>
            </span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel"></label> 
            <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Promo Type:</label>
             <span style=" ">
                   <table>
                   <%foreach (var p in Model.PromoTypeChoices)
                    {%>
                        <tr>
                            <td><%=Html.RadioButtonFor(m=>m.PromoType, p.Value, new {@class="promo-type-radio"}) %></td>
                            <td><span style="padding-left:5px;"><%=p.Text %></span></td>
                        </tr>
                   <%}%>
                   </table>
                  <input type="hidden" class="prev-radiobuttonlist-set promo-type-value" value="<%=(int)Model.PromoType %>"/>
                  <br />
                  <span class="blue_sub_text">Note: If anything other than "Member" is selected, Payment Type must be set to "Credit Card"</span>
             </span>
          </li>
     </ul>
    </div>
    

    <br/>
    </div>
    
    <div class="create-promo-step2 create-promo-step">
    <span class="gray_header_text">STEP TWO (PLAN CONSTRAINTS)</span><br/><br />

    <ul>
        <li class="list-item-edit">
                <span class="blue_header_text"> Select duration and resource template for row headers </span> <br /><br />
                <%if (Model.RowHeaders != null)
                  {%>
                    <%=Html.EditorFor(m=>m.RowHeaders) %>
                <%} %>
        </li>
        
    </ul>
    <br />
    <ul>
        <li class="list-item-edit">
             <span class="blue_header_text">Select plan type and resource template for column headers</span> <br /><br />
            <%if (Model.ColHeaders != null )
              {%>
                <%=Html.EditorFor(m=>m.ColHeaders) %>  
              <%}%>
        </li>
     </ul>
    
    <br /> <br />
    </div>

    <div class="create-promo-step3 create-promo-step">
    <span class="gray_header_text">STEP THREE (PLAN SELECTION)</span> <br /> <br />
    
    <div style="width:100%;">
    <div style="float:left;width:60%;">
    <ul>
        <li class="list-item-edit">
                <%if (Model.PlanSelectors != null){%>
                    <%=Html.EditorFor(m=>m.PlanSelectors) %>
                <% } %>
        </li>
    </ul>
    </div>
    <div style="float:right;width:40%;" id="planDesc"> </div>
    </div>

    </div>

    <div class="create-promo-step4 create-promo-step">
     <span class="gray_header_text">STEP FOUR (CRITERIA STEP)</span> <br /> <br />
      <ul>
        <li class="list-item-edit">
                <label class="memberLabel">Default Plan:</label>
                <span style="padding-left:5px;">
                    <%=Html.DropDownListFor(m => m.DefaultPlanId, Model.DefaultPlanChoices) %>
                </span>
        </li>
     </ul>

    <div class="promo-expressions">
    <ul>
        <li class="list-item-edit">
                <label class="memberLabel">Subscriber status:<br /><span="grey_sub_title">(Select at least one)</span></label>
                <span style="padding-left:5px;"><%=Html.EditorFor(m => m.SubscriptionStatus)%></span>
        </li>
            <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel">Days SINCE renewal / termination:</label>
                <span style="padding-left:5px;"> <%=Html.EditorFor(m => m.DaysTillTermination)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel">Days since registration:</label>
                <span style="padding-left:5px;"><%=Html.EditorFor(m => m.DaysSinceRegistration)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel">Last digit of member ID:</label>
                <span style="padding-left:5px;"><%=Html.EditorFor(m => m.MemberIdLastDigit)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel">Price Target ID:</label>
                <span style="padding-left:5px;"><%=Html.EditorFor(m => m.PriceTargetID)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li> 
        <li class="list-item-edit">
                <label class="memberLabel">Year of birth:</label>
                <span style="padding-left:5px;"><%=Html.EditorFor(m => m.YearOfBirth)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li> 
         <li class="list-item-edit">
                <label class="memberLabel">PRM:</label>
                <span style="padding-left:5px;"><%=Html.EditorFor(m => m.PRM)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li> 
        <li class="list-item-edit">
                <label class="memberLabel">Age:</label> 
                <span style="padding-left:5px;"><%= Html.EditorFor(m => m.Age)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel">Birthday:</label>
                <span style="padding-left:5px;"><%= Html.EditorFor(m => m.DaysSinceBirthday)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                    <label class="memberLabel">Gender seeking gender:</label>
                    <span style="padding-left:5px;"><%=Html.EditorFor(m => m.GenderSeekingGender)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                    <label class="memberLabel">DMA:</label>
                    <span style="padding-left:5px;"> <%=Html.EditorFor(m => m.DMAs)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                    <label class="memberLabel">Country:</label>
                    <span style="padding-left:5px;"> <%=Html.EditorFor(m => m.Country)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                    <label class="memberLabel">State:</label>
                    <span style="padding-left:5px;">  <%=Html.EditorFor(m => m.State)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                    <label class="memberLabel">City:</label> 
                    <span style="padding-left:5px;"><%=Html.EditorFor(m => m.City)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                    <label class="memberLabel"> Marital Status: </label>
                    <span style="padding-left:5px;"><%=Html.EditorFor(m => m.MaritalStatus)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                    <label class="memberLabel">Education:</label>
                        <span style="padding-left:5px;"><%=Html.EditorFor(m => m.Education)%></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Profession:</label>
            <span style="padding-left:5px;"><%=Html.EditorFor(m => m.Profession)%></span>   
        </li>
        <li class="list-item-edit">
                <label class="memberLabel"></label>
                <span style="padding-left:5px;"></span>
        </li>
        <li class="list-item-edit">
                <label class="memberLabel">Device/OS:</label>
                <span style="padding-left:5px;"><%=Html.EditorFor(m => m.DeviceOS)%></span>
        </li>
        
    </ul>
    </div>
        <ul>
            <li class="list-item-edit">
                    <label class="memberLabel"></label>
                    <span style="padding-left:5px;"></span>
            </li>
            <li class="list-item-edit">
                <label class="memberLabel">Promotion start date time:</label>
                <span style="padding-left:5px;"><%=Html.TextBoxFor(m => m.PromoStartDate, new { @class = "date-field"}) %><%=Html.DropDownListFor(m => m.PromoStartTime, Model.PromoTimeChoices) %></span>  
            </li>
            <li class="list-item-edit">
                    <label class="memberLabel"></label>
                    <span style="padding-left:5px;"></span>
            </li>
            <li class="list-item-edit">
                <label class="memberLabel">Promotion end date time:</label>
                 <span style="padding-left:5px;"><%=Html.TextBoxFor(m => m.PromoEndDate, new { @class = "date-field" })%> <%=Html.DropDownListFor(m => m.PromoEndTime, Model.PromoTimeChoices) %> </span>
            </li>
            <li class="list-item-edit">
                    <label class="memberLabel"></label>
                    <span style="padding-left:5px;"></span>
            </li>
            <li class="list-item-edit">
                <label class="memberLabel"> Recurring:</label>
                
                    <table>
                        <tr>
                            <td><span style="padding-left:5px;"><%=Html.RadioButtonFor(m => m.IsRecurring, "True", new {@class="isrecurring-radio"}) %></span></td>
                            <td><span style="padding-left:5px;">Yes</span></td>
                        </tr>
                        <tr>
                            <td><span style="padding-left:5px;"><%=Html.RadioButtonFor(m => m.IsRecurring, "False", new { @class = "isrecurring-radio" })%></span></td>
                            <td><span style="padding-left:5px;">No</span></td>
                        </tr>
                    </table>
                
            </li>                            
            <li class="list-item-edit">   
                <div class="recurrence-panel">                 
                    <br />
                    <label style="text-align: center;">Recurrence Pattern:</label>
                    <table>
                        <tr>
                            <td><span style="padding-left: 5px;"><%=Html.RadioButtonFor(m => m.RecurringPattern, "Weekly", new { @class = "recurring-pattern-radio" })%></span></td>
                            <td><span style="padding-left: 5px;">Weekly</span></td>
                        </tr>
                        <tr>
                            <td><span style="padding-left: 5px;"><%=Html.RadioButtonFor(m => m.RecurringPattern, "Monthly", new { @class = "recurring-pattern-radio" })%></span></td>
                            <td><span style="padding-left: 5px;">Monthly</span></td>
                        </tr>                    
                    </table>                
                </div>
            </li>
            <li class="list-item-edit">                
                <div class="recurrence-panel">  
                    <label style="text-align: center;">Start Hour:</label>
                    <span style="padding-left: 5px;"><%=Html.DropDownListFor(m => m.RecurringStartHour, Model.PromoTimeChoices) %></span>                
                </div>
            </li>
            <li class="list-item-edit">                
                <div class="recurrence-panel">  
                    <label style="text-align: center;">Duration (hours):</label>
                    <span style="padding-left: 5px;"><%=Html.TextBoxFor(m => m.RecurringDuration) %></span>         
                    <span style="padding-left: 5px;">hour(s)</span>                 
                </div>
            </li>
            <li class="list-item-edit">            
                <div class="recurrence-panel">      
                    <div class="recurrence-weekly">
                        <label style="text-align: center; margin-bottom: 30px;">Day(s) of week:</label>
                        <% foreach(var option in Model.DayOfTheWeekChoices) { %>
                            <span style="padding-left: 5px;">                            
                                <input name="ReccuringDayOfWeek" type="checkbox" value="<%=option.Value %>" <%= Model.ReccuringDayOfWeekContains(option.Value) ? "checked='true'" : ""  %>/>
                                <%=option.Text %>                            
                            </span>   
                        <%} %>           
                    </div>
                    <div class="recurrence-monthly">
                        <label style="text-align: center; margin-bottom: 30px;">Day of month: Day </label>
                        <span style="padding-left: 5px;"><%=Html.TextBoxFor(m => m.RecurringDayOfMonth) %></span>      
                        <span style="padding-left: 5px;">of every month</span> 
                    </div>                                                                     
                </div>                
            </li>                            
            <li class="list-item-edit">
                <label class="memberLabel">Subscription page title:</label>
                <span style="padding-left:5px;"> <%=Html.TextBoxFor(m => m.PageTitle, new { style = "width: 450px;" })%></span>
            </li>
             <li class="list-item-edit">
                <label class="memberLabel">Subscription page legal note:</label>
                <span style="padding-left:5px;"> <%=Html.TextAreaFor(m => m.LegalNote, new { style = "width: 450px; height: 200px;" })%></span>
            </li>
            <li class="list-item-edit">
                    <label class="memberLabel"></label>
                    <span style="padding-left:5px;"></span>
            </li>
            <li class="list-item-edit">
                <label class="memberLabel"> Subscription page template:</label>
                <span style="padding-left:5px;"> <%=Html.DropDownListFor(m => m.PageTemplateId, Model.PageTemplateChoices) %></span>
            </li>
        </ul>
    </div>

    <div class="create-promo-step5 create-promo-step">
     <span class="gray_header_text">STEP FIVE (SUMMARY STEP)</span><br/><br />

     <ul>
        <% if (Model.PromoDetailLineItems != null)
       {%>
           <%foreach (var lineItem in Model.PromoDetailLineItems)
           {%>
                <li class="list-item-edit">
                    <label class="memberLabel"><%=Html.Encode(lineItem.LabelText)%></label><span style="padding-left:5px;"><%=Html.Encode(lineItem.LineItemText)%></span>
                </li>
                <li class="list-item-edit">
                    <label class="memberLabel"></label><span style="padding-left:5px;"></span>
                </li>
        <% } %>
       <%} %>
       </ul>
    </div>   
<div class="navigation" style="width:100%; float:left; padding-top:20px;" >
    <a href="http://admintool.matchnet.com/SubAdmin/CreatePromo/" class="button">Cancel</a>
    <input type="button" value="Previous" class="go-to-previous step-navigation-button"/>
    <input type="submit" value="Next" class="step-navigation-button" />
    <a value="preview" class="button preview"  href="#">Preview</a>
</div>
 </div> 
 </div>  
    <% Html.EndForm(); %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 

 <script type="text/javascript" language="javascript">
     function promopreview() {
         $('.preview').click(function () {
             //Capture selected planIds and ResourceTemplatedIds
             var planSelectors = [];
             var planCount = $('.create-promo-step3 .plan-selector-dropdown').length;
             for (var i = 0; i < planCount; i++) {
                 planSelectors.push(
                     $('#PlanSelectors_' + i + '__ColumnNumber').val() + ':' +
                     $('#PlanSelectors_' + i + '__RowNumber').val() + ':' +
                     $('#PlanSelectors_' + i + '__PlanId').val() + ':' +
                     $('#PlanSelectors_' + i + '__ResourceTemplateId').val()
                 );
             }
             window.open('<%= Url.Action("PreviewPromo", "SubAdmin")%>' + '?planSelectors=' + planSelectors.join(","));
             //postGo('<%= Url.Action("PreviewPromo", "SubAdmin")%>', { 'planSelectors': JSON.stringify(planSelectors) });
         });
    }

    function  postGo(url, params) {
        var $form = $("<form>").attr("method", "post").attr("action", url);
        $.each(params, function (name, value) {
            $("<input type='hidden'>").attr("name", name).attr("value", value).appendTo($form);
        });
        $form.appendTo("body");
        $form.submit();
    }

     function goToCurrentStep() {
         $(".create-promo-step").hide();
         $(".navigation").hide();
         $("input[type=submit]").val("Next");
         $(".preview.button").hide();
         
         // no need for previous button for the first step
         if ($(".promo-step").val() == 1) {
             $(".go-to-previous").hide();
         }

         //step3 show preview button
         if ($(".promo-step").val() == 3) {
             $(".preview.button").show();
         }

         //Step5 Change 'Next' button text
         if ($(".promo-step").val() == 5) {
             $("input[type=submit]").val("Create Promo");
         }
         
         // step 6 means we are done here
         if ($(".promo-step").val() != 6) {
             $(".navigation").show();
             $(".create-promo-step" + $(".promo-step").val()).show();
         }

         determineRecurringPanelVisibility();
         singleColumnMultiPlanTypes();
         determinePromoExpressionsVisibility();
     }

     function splitString(stringToSplit) {
         var stringArray = stringToSplit.split(",");
         if (stringArray.length > 0 && stringArray[stringArray.length - 1] == "") {
             stringArray.pop();
         }

         return stringArray;
     }

     function getChildRegion(controlToSetClassName, parentRegionId) {
         // first set the parentRegionId
         $("." + controlToSetClassName).prev().val(parentRegionId);
         var siteId = $(".site-id-holder").val();

         // make the ajax call and populate the dropdown
         $.ajax({
             type: "POST",
             url: "/RegionAPI/GetChildRegions/" + siteId,
             data: { ParentRegionId: parentRegionId },
             success: function (result) {
                 BuildDropdownOptions(result.SelectListItems, $("." + controlToSetClassName), true);
             }
         });
     }

     function determineRecurringPanelVisibility() {
         if ($(".isrecurring-radio:checked").val() == "True") {
             $(".recurrence-panel").show();
             determineRecurringChildPanelVisibility();
         }
         else {
             $(".recurrence-panel").hide();
         }
     }

     function determineRecurringChildPanelVisibility() {
         $(".recurrence-weekly").hide();
         $(".recurrence-monthly").hide();

         if ($(".recurring-pattern-radio:checked").val() == "Weekly") {
             $(".recurrence-weekly").show();
         }
         else if ($(".recurring-pattern-radio:checked").val() == "Monthly") {
             $(".recurrence-monthly").show();
         }
     }

     function determinePromoExpressionsVisibility() {
         if ($(".promo-type-value").val() == 1 || // admin only
            $(".promo-type-value").val() == 2 || // email link
            $(".promo-type-value").val() == 5) { // supervisor only
             $(".promo-expressions").hide();
         }
     }

     function singleColumnMultiPlanTypes() {
         if ($(".single-col-with-multi-plantypes").attr("checked")) {
             $(".column-dimension").val("1");
             $(".column-dimension-holder").val("1");
             $(".column-dimension").attr("disabled", true);
         }
         else {
             $(".column-dimension").attr("disabled", false);
         }
     }

     $(document).ready(function () {
         var matchnetNullInt = -2147483647;

         promopreview();

         goToCurrentStep();

         // handle previous click which will be on client side
         $(".go-to-previous").click(function () {
             $(".promo-step").val($(".promo-step").val() - 1);
             goToCurrentStep();
         });

         // wire up the country state city dropdowns
         $(".create-promo-country").change(function () {
             getChildRegion("create-promo-state", $(this).val());
             $(".create-promo-city").html("");
         });

         $(".create-promo-state").change(function () {
             getChildRegion("create-promo-city", $(this).val());
         });

         // since we have to clear ModelState, binding to the model does not happen automatically for some reason
         // it's ugly but since ModelState can't handle dynamic controls that well, we are stuck with this
         $(".prev-dropdown-set").each(function () {
             if ($(this).val() == matchnetNullInt) {
                 $(this).prev(".dropdown-autobind").find("option:first").attr("selected", "selected");
             }
             else {
                 $(this).prev(".dropdown-autobind").val($(this).val());
             }
         });

         // bind listboxes manually
         $(".prev-listbox-set").each(function () {
             if ($(this).val() == "")
                 return;

             var selectedValues = splitString($(this).val());

             for (var i = 0; i < selectedValues.length; i++) {
                 $(this).prev().find("option[value=\"" + selectedValues[i] + "\"]").attr("selected", "selected");
             }
         });

         // bind radio button list manually
         $(".prev-radiobuttonlist-set").each(function () {
             if ($(this).prev().find("input[value=" + $(this).val() + "]").length == 0) {
                 $(this).prev().find("input:first").attr("checked", "true");
             }
             else {
                 $(this).prev().find("input[value=" + $(this).val() + "]").attr("checked", "true");
             }

         });

         // single column with multi types of plans
         $(".single-col-with-multi-plantypes").change(function () {
             singleColumnMultiPlanTypes();
         });

         $(".column-dimension").change(function () {
             $(".column-dimension-holder").val($(this).val());
         });

         // date fields
         $(".date-field").datepicker({ dateFormat: 'mm/dd/yy', changeMonth: true, changeYear: true });

         // recurring stuff
         $(".isrecurring-radio").change(function () {
             determineRecurringPanelVisibility();
         });

         $(".recurring-pattern-radio").change(function () {
             determineRecurringChildPanelVisibility();
         });
     });
 </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Search Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", this.ViewData.Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
