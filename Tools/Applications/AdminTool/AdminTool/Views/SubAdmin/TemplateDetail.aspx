﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SubAdmin.ResourceTemplateDetail>"  %>


<asp:Content ID="ContentTitle" ContentPlaceHolderID="TitleContent" runat="server">
	Spark Networks Web Admin - Template Detail
</asp:Content>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="ContentMain" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.BeginForm(); %>
    <%= Html.HiddenFor(m => m.TemplateId) %>
 <%--   <%= Html.HiddenFor(m => m.BrandId) %>
    <%= Html.HiddenFor(m => m.TemplateTypeId) %>
 --%>   <%= Html.HiddenFor(m => m.IsNew) %>
    
   <table class="template-detail">
   <tr>
    <td colspan="2">
        <%=Html.ValidationSummary() %>
    </td>
   </tr>
    <tr>
        <td>ID:</td>
        <td>
            <% if (!Model.IsNew) { %>
                <%=Html.DisplayTextFor(m => m.TemplateId)%>
            <% } %>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: middle;">Content:</td>
        <td>
             <%= Html.TextArea("Content",Model.Content) %>
        </td>
    </tr>
    <tr>
        <td>Description:</td>
        <td>
             <%= Html.TextBoxFor(m => m.Description) %>
             <%= Html.ValidationMessageFor(model => model.Description, "*")%> 
        </td>
    </tr>
    <tr>
        <td>BrandID:</td>
        <td>
            <% if (Model.IsNew) {%>
                <%= Html.DropDownListFor(m => m.BrandId, new SelectList(Model.Brands, "Value", "Text", Model.BrandId))%>
            <% }
               else
               {%>
                    <%=Html.DropDownListFor(m => m.BrandId,
                                                          new SelectList(Model.Brands, "Value", "Text", Model.BrandId),
                                                          new {@disabled = "disabled"})%>
            <% } %>
        </td>
    </tr>
    <tr>
        <td>Type:</td>
        <td>
            <% if (Model.IsNew)
               {%>
                <%=Html.DropDownListFor(m => m.TemplateTypeId,
                                                          new SelectList(Model.TemplateTypes, "Value", "Text",
                                                                         Model.TemplateTypeId))%>
            <% }
               else
               { %>
                <%=Html.DropDownListFor(m => m.TemplateTypeId,
                                                          new SelectList(Model.TemplateTypes, "Value", "Text",
                                                                         Model.TemplateTypeId),
                                                          new {@disabled = "disabled"})%>
            <% } %>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input class="button" type="submit" value="Save"/>
            <a class="button" href="/SubAdmin/ResourceManagerTemplates?brandId=<%= Model.BrandId %>&templateTypeId=<%= Model.TemplateTypeId %>">Cancel</a>
        </td>
    </tr>
   </table>
   
    <% Html.EndForm(); %>
</asp:Content>

<asp:Content ID="ContentHead" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
