﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PromoSearchResults>" %>
<%@ Import Namespace="Matchnet.PromoEngine.ValueObjects" %>
 
<%if(Model.FoundPromos != null && Model.FoundPromos.Count > 0) {%>
<table id="search-results-table">
       <tr>
        <th align="left">Promo ID</th>
        <th align="left">Promo Name</th>
        <th align="left">Start Date</th>
        <th align="left">End Date</th>
        <th align="left">Approval Status</th>
       </tr>
            
    <%foreach (Promo p in Model.FoundPromos) {%>
        <tr>
            <td><%=Html.ActionLink(p.PromoID.ToString(), "PromoDetail", "SubAdmin", new { promoId = p.PromoID, brandId = Model.BrandId }, new { @class = "view-message-click" })%></td>
            <td ><%=p.Description %></td>
            <td ><%=p.StartDate.ToShortDateString() %></td>
            <td ><%=p.EndDate.ToShortDateString() %></td>
            <td ><%=p.PromoApprovalStatus %></td>
        </tr>
    <% } %>
</table>
<% }
  else if (Model.FoundPromos != null && Model.FoundPromos.Count <= 0)
  { %>
<table>
        <tr>
           <td> No results please search again! </td>
        </tr>
</table>
<script type="text/javascript">
    ToggleSearchParams();
</script>
<%} %>



