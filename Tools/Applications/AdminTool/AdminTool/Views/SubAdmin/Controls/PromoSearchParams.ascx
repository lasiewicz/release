﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PromoSearchParams>" %>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel">Select Web Site:</label> <span style="padding-left:35px;"> <%=Html.DropDownListFor(m=>m.BrandId, Model.BrandIds) %></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Promo Payment Type:</label>  <span style="padding-left:35px;"><%=Html.DropDownListFor(m=>m.PaymentType, Model.PaymentTypes) %></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Promo Type:</label>  <span style="padding-left:35px;"><%=Html.DropDownListFor(m=>m.PromoType, Model.PromoTypes) %></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Promo Approval Status:</label> <span style="padding-left:35px;"> <%=Html.DropDownListFor(m=>m.PromoApprovalStatus, Model.PromoApprovalStatuses) %> </span> 
            <span class="verySmallText">*When searching by promotion ID, all criteria exception for the web site and promo type are ignored.</span> 
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Search by Promotion ID:</label><span style="padding-left:35px;"> <%=Html.TextBoxFor(m=>m.PromoId) %></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Search by Promotion Name:</label><span style="padding-left:35px;"> <%=Html.TextBoxFor(m=>m.PromoName) %></span>
            <span class="verySmallText">*Search any portion of the promo title</span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel"></label><span style="padding-left:35px;"><input type="submit" value="Search" class="button-search" /></span>
        </li>
    </ul>
     <script type="text/javascript">
         // on load set the select web site list to default to "Jdate.com"

         $(document).ready(function () {

             // bind the collapse/expand of the search parameters
             $("#BrandId").val("1003");

         });
    
    
    </script>
