﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.Plan>" %>

Initial Cost: <%=Html.DisplayFor(x => x.InitialCost) %><br/>
Duration: <%=Html.DisplayFor(x => x.Duration) %>
<% if (Model.Duration != null)
   {%>
       <%=Enum.GetName(typeof (Matchnet.DurationType), Model.DurationType) %>
   <% }%>
<br/>
Cost per: <%=Html.DisplayFor(x => x.CostPer) %><br/>
Renewal Cost: <%=Html.DisplayFor(x => x.RenewalCost) %><br/>
Renewal Duration: <%=Html.DisplayFor(x => x.RenewalDuration) %>
<% if (Model.RenewalDuration != null)
   {%>
       <%=Enum.GetName(typeof (Matchnet.DurationType), Model.RenewalDurationType) %>
   <% }%>
<br/>
Free Trial Duration (if any): <%=Html.DisplayFor(x => x.FreeDuration) %>
<% if (Model.FreeDuration != null)
   {%>
       <%=Enum.GetName(typeof (Matchnet.DurationType), Model.FreeDurationType) %>
   <% }%>
<br/>
Plan Type: <%=Enum.GetName(typeof(Matchnet.Purchase.ValueObjects.PlanType), Model.PlanType) %>
<% if(Model.PlanType == (int)Matchnet.Purchase.ValueObjects.PlanType.ALaCarte)
   { %>
        <%=" - " + Enum.GetName(typeof(Matchnet.Purchase.ValueObjects.PremiumType), Model.ALaCartePlanType) %>  
   <%} else if(Model.PlanType == (int)Matchnet.Purchase.ValueObjects.PlanType.PremiumPlan)
    {%>
        <%=" - " + Html.DisplayFor(x => x.PremiumPlanTypeMaskNative) %>    
    <%} %>
<br/>

<% if (Model.PlanType == (int)Matchnet.Purchase.ValueObjects.PlanType.PremiumPlan)
   {
    foreach (var pb in Model.PremiumPlansPriceBreakdowns)
    {
        if((Model.PremiumPlanTypeMask & pb.PremiumPlanType) == pb.PremiumPlanType)
        {%>
            <%= "Cost for " + pb.LabelText + " (Base/Renewal): " + pb.InitialCost + " / " + pb.RenewalCost %><br/>
        <%}
    }
} %>

Additive Plan Types: <%=Html.DisplayFor(x => x.AdditiveMaskNative) %><br/>