﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.GiftDetailsModelView>" %>
<%@ Import Namespace="Spark.Common.DiscountService" %>
<%if (Model == null || Model.Gift == null) { %>
<p>No gifts were found.</p>
<% } else
  { %>
  <style>
    fieldset {
        border: 1px solid gray;
        padding-left: 10px;
        padding-top: 20px;
        padding-bottom: 20px;
        padding-right:10px;
        }
    </style>
<fieldset class="" > 
       <legend> Gift Details </legend>
        <ul>
            <li class="list-item-edit">
                <label class="memberLabel">Gift Type:</label>
                <span style="padding-left:35px;"><%=Model.Gift.GiftType.ToString()%></span>
            </li>
            <li class="list-item-edit">
                <label class="memberLabel">Gift Status:</label>
                 <span style="padding-left:35px;"><%=Model.Gift.Status.ToString()%></span>
            </li>
            <li class="list-item-edit">
                <label class="memberLabel">Is Valid?</label>
                <span style="padding-left:35px;"><%=Model.Gift.IsValid.ToYesNoString()%></span>
            </li>
            <li class="list-item-edit">
                <label class="memberLabel">Purchaser:</label>
                <span style="padding-left:35px;"><a href="/Member/View/<%=Model.Gift.CallingSystemID.ToString()%>/<%=Model.Gift.CustomerID.ToString()%>"><%=Model.Gift.CustomerID.ToString()%></a></span>
            </li>
            <% if (Model.Gift.GiftRedemption != null && Model.Gift.GiftRedemption.CustomerID > 0)
            { %>
                <li class="list-item-edit">
                    <label class="memberLabel">Redeemer: </label>
                    <span style="padding-left:35px;"><a href="/Member/View/<%=Model.Gift.GiftRedemption.CallingSystemID.ToString()%>/<%=Model.Gift.GiftRedemption.CustomerID.ToString()%>"><%=Model.Gift.GiftRedemption.CustomerID.ToString()%></a></span>
                </li>
            <% } %>
            <li class="list-item-edit">
                <label class="memberLabel">Created Date: </label>
                <span style="padding-left:35px;"><%=Model.Gift.InsertDate.ToString()%></span>
            </li>
            <li class="list-item-edit">
                <label class="memberLabel">Begin Valid Date: </label>
                <span style="padding-left:35px;"><%=Model.Gift.BeginValidDate.ToString()%></span>
             </li>
             <li class="list-item-edit">
                <label class="memberLabel">Expiration Date:</label>
               <span style="padding-left:35px;"><%=Model.Gift.ExpirationDate.ToString()%></span>
            </li>
         <% if (Model.EnableStatusToggle) { %>
            <li class="list-item-edit">
                <label class="memberLabel"></label><span style="padding-left:35px;">
            <% if (Model.Gift.Status != GiftStatus.Cancelled) { %>
                <input type="button" value="Cancel Gift" onclick="CancelGift(event, $(this), '<%= Model.Gift.Code%>');" />
            <% }  else {%>
                <input type="button" value="Reopen Gift" onclick="ReopenGift(event, $(this), '<%= Model.Gift.Code%>');" />
            <% } %>
            </span>
            </li>
        <% } %>
    </ul>
</fieldset>
<br /><br />
<fieldset > 
    <legend>Notifications</legend>
<% if (Model.Notifications != null){ %>
        
    <table id="search-results-table">
    <tr>
        <td>Notification Type</td>
        <td>Status</td>
        <td>Sent To</td>
        <td>Sent Date</td>
        <td>Create Date</td>
        <td>Update Date</td>
        <td>Admin</td>
    </tr>
    <% foreach (AdminTool.Models.ModelViews.SubAdmin.GiftNotificationExtended notification in Model.Notifications) { %>
        <tr>
            <td><%=notification.NotificationType.ToString() %></td>
            <td><%=notification.NotificationStatus.ToString() %></td>
            <td><%=notification.SentTo%></td>
            <td><%=notification.SentDate.ToStringEmptyIfMin()%></td>
            <td><%=notification.CreateDate.ToStringEmptyIfMin()%></td>
            <td><%=notification.UpdateDate.ToStringEmptyIfMin()%></td>
            <td><%=notification.AdminEmailAddress %></td>
        </tr>
    <% } %>
    </table><br/>
<% } %>
<% if (Model.EnableSendNotifications) { %>
    Send new notification: <%=Html.TextBox("EmailAddress") %>&nbsp;&nbsp;<input type="button" value="Send" onclick="SendGiftNotification(event, $(this), '<%= Model.Gift.CallingSystemID.ToString()%>', '<%= Model.Gift.CustomerID.ToString()%>', '<%= Model.Gift.GiftID.ToString()%>', '<%= Model.Gift.Code%>');" />
<% } %>
</fieldset>
<% } %>
