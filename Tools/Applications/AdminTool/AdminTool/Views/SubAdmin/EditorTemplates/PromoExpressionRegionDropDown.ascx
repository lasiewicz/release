﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PromoExpressionRegionDropDown>" %>

<%=Html.HiddenFor(m=>m.ExpPromoAttributeId) %>
<%=Html.HiddenFor(m=>m.ExpPromoAttributeName) %>
<%=Html.HiddenFor(m => m.RegionDepth)%>

<%=Html.HiddenFor(m => m.ParentRegionId)%>
<%=Html.DropDownListFor(m=>m.TheValue, Model.PossibleValues, new { @class = Model.CssClassName}) %>
<%=Html.HiddenFor(m => m.TheValue, new { @class = "prev-dropdown-set" })%>