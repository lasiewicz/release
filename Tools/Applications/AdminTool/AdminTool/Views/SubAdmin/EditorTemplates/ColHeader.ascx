﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.ColHeader>" %>

<%=Html.HiddenFor(m => m.HeaderNumber) %>
<%=Html.HiddenFor(m => m.IsMixedPlanType) %>
<span style="font-weight:bold; font-size:9pt;padding-right:10px;">Column<%=" "  + Model.HeaderNumber %></span>

<% if (!Model.IsMixedPlanType)
   {%>
<%= Html.DropDownListFor(m => m.PlanType, Model.PlanTypeChoices,
                                               new {@class = "dropdown-autobind"}) %>
<%= Html.HiddenFor(m => m.PlanType, new {@class = "prev-dropdown-set"}) %>
<%= Html.DropDownListFor(m => m.PremiumType, Model.PremiumTypeChoices,
                                               new {@class = "dropdown-autobind"}) %>
<%= Html.HiddenFor(m => m.PremiumType, new {@class = "prev-dropdown-set"}) %>
<% } %>

<%=Html.DropDownListFor(m => m.ResourceTemplateId, Model.ResourceTemplateChoices, new { @class = "dropdown-autobind" })%>
<%=Html.HiddenFor(m => m.ResourceTemplateId, new { @class = "prev-dropdown-set" })%>
<br/>
