﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PromoExpressionDropDown>" %>

<%=Html.HiddenFor(m=>m.ExpPromoAttributeId) %>
<%=Html.HiddenFor(m=>m.ExpPromoAttributeName) %>

<%=Html.DropDownListFor(m => m.TheValue, Model.PossibleValues, new { @class = "dropdown-autobind" })%>
<%=Html.HiddenFor(m => m.TheValue, new { @class = "prev-dropdown-set" })%>