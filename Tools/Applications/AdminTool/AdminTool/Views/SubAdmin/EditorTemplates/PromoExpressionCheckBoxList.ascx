﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PromoExpressionCheckBoxList>" %>

<%=Html.HiddenFor(m=>m.ExpPromoAttributeId) %>
<%=Html.HiddenFor(m=>m.ExpPromoAttributeName) %>

    <table>
    <% foreach(var option in Model.PossibleValues) { %>
        <tr>
            <td>
                <input name="<%=Model.PropertyName + ".TheValue"%>" type="checkbox" class="checkbox-list-item-non-mask" value="<%=option.Value %>" <%= Model.ValueContains(option.Value) ? "checked='true'" : ""  %>/>
            </td>
            <td>
                <%=option.Text %> 
            </td>
        </tr>
    <%} %>
    </table>