﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PromoExpressionMultiSelectTextBox>" %>

<%=Html.HiddenFor(m=>m.ExpPromoAttributeId) %>
<%=Html.HiddenFor(m=>m.ExpPromoAttributeName) %>
<%=Html.HiddenFor(m => m.PropertyFriendlyName)%>
<%=Html.HiddenFor(m => m.DataType)%>

<%=Html.TextBoxFor(m=>m.TheValue) %>

