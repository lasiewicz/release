﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PlanPriceBreakdown>" %>

<%=Html.HiddenFor(x=>x.LabelText) %>
<%=Html.HiddenFor(x=>x.PremiumPlanType) %>
<%=Html.HiddenFor(x=>x.EnabledDisburseCount) %>

<span style="text-align: center;"><%=Html.Label(Model.LabelText) %></span>
<span style="padding-left: 5px;"><%=Html.TextBoxFor(x => x.InitialCost) %><%=Html.ValidationMessageFor(x => x.InitialCost, "*") %></span>
<span style="padding-left: 5px;"><%=Html.TextBoxFor(x => x.RenewalCost) %><%=Html.ValidationMessageFor(x => x.RenewalCost, "*")%></span>
<span style="padding-left: 5px;"><% if (Model.EnabledDisburseCount)
   { %>
        <%=Html.TextBoxFor(x => x.Count) %>
        <span style="padding-left: 75px;"><%=Html.CheckBoxFor(x => x.IsDisburseCount) %></span>
<% } %></span>
