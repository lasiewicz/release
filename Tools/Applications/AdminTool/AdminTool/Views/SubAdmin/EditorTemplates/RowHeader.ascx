﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.RowHeader>" %>

<%=Html.HiddenFor(m => m.HeaderNumber) %>
<span style="font-weight:bold; font-size:9pt;padding-right:10px;"> Row<%= " " + Model.HeaderNumber %></span>
<%=Html.DropDownListFor(m => m.Duration, Model.DurationChoices, new { @class = "dropdown-autobind" })%>
<%=Html.HiddenFor(m => m.Duration, new { @class = "prev-dropdown-set" })%>
<%=Html.DropDownListFor(m => m.DurationType, Model.DurationTypeChoices, new { @class = "dropdown-autobind" })%>
<%=Html.HiddenFor(m => m.DurationType, new { @class = "prev-dropdown-set" })%>
<%=Html.DropDownListFor(m => m.ResourceTemplateId, Model.ResourceTemplateChoices, new { @class = "dropdown-autobind" })%>
<%=Html.HiddenFor(m => m.ResourceTemplateId, new { @class = "prev-dropdown-set" })%>
<br /><br />
