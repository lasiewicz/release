﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Matchnet.PromoEngine.ValueObjects.Promo>" %>


<tr class="promo-sortable-row">
<td>
    <%=Html.HiddenFor(m => m.PromoID) %>
    <%=Html.HiddenFor(m => m.ListOrder, new { @class="promo-listorder-holder"}) %>
</td>
<td><input type="image" value="UU" src="/images/admin-sub-order-up-all.gif" class="promo-order-button" /></td>
<td><input type="image" value="DD" src="/images/admin-sub-order-down-all.gif" class="promo-order-button" /></td>
<td class="promo-listorder-display"><%=Html.DisplayFor(m => m.ListOrder) %></td>
<td><%=Html.ActionLink(Model.PromoID.ToString(), "PromoDetail", "SubAdmin", new { promoId = Model.PromoID.ToString(), brandId = ViewData["BrandId"] }, new { @class = "view-message-click" })%></td>
<td><%=Html.DisplayFor(m => m.Description) %></td>
<td><%=Html.DisplayFor(m => m.StartDate) %></td>
<td><%=Html.DisplayFor(m => m.EndDate) %></td>
<td><%=Html.DisplayFor(m => m.PromoApprovalStatus) %></td>
</tr>
