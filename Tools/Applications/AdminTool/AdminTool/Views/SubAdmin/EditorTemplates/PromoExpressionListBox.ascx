﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PromoExpressionListBox>" %>

<%=Html.HiddenFor(m=>m.ExpPromoAttributeId) %>
<%=Html.HiddenFor(m=>m.ExpPromoAttributeName) %>

<%=Html.ListBoxFor(m=>m.TheValue, Model.PossibleValues) %>
<%=Html.HiddenFor(m => m.TheValueInString, new { @class = "prev-listbox-set" })%>