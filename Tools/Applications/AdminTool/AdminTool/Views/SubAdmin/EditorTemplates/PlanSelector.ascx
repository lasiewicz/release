﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PlanSelector>" %>

<%=Html.HiddenFor(m => m.ColumnNumber)%>
<%=Html.HiddenFor(m => m.RowNumber)%>
<span style="font-weight:bold; font-size:9pt;padding-right:10px;">Col<%=" " + Model.ColumnNumber + " "%>Row<%=" " + Model.RowNumber%></span>
<%=Html.DropDownListFor(m => m.PlanId, Model.PlanChoices, new {@class = "plan-selector-dropdown dropdown-autobind"})%>
<%=Html.HiddenFor(m=>m.PlanId, new {@class="prev-dropdown-set"}) %>
<%=Html.DropDownListFor(m => m.ResourceTemplateId, Model.ResourceTemplateChoices, new { @class = "dropdown-autobind" })%>
<%=Html.HiddenFor(m => m.ResourceTemplateId, new { @class = "prev-dropdown-set" })%><br/>
<%=Html.HiddenFor(m=>m.BrandID, new {@class="brandID"}) %><br />

<%  %>



