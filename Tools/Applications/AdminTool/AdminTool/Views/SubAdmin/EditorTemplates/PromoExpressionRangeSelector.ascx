﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PromoExpressionRangeSelector>" %>


<%=Html.HiddenFor(m=>m.ExpPromoAttributeId) %>
<%=Html.HiddenFor(m=>m.ExpPromoAttributeName) %>
<%=Html.HiddenFor(m => m.PropertyFriendlyName)%>
<%=Html.HiddenFor(m => m.Behavior)%>

<span><%=Html.Label(Model.FromLabel) %> <%=Html.TextBoxFor(m => m.FromValue, new { @class = "subadmin_text_box" })%> <span> &nbsp;&nbsp;&nbsp;&nbsp; </span>  <%=Html.Label(Model.ToLabel) %> <%=Html.TextBoxFor(m => m.ToValue, new { @class = "subadmin_text_box" })%></span>
