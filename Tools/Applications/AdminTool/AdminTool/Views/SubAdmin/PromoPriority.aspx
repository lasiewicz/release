﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SubAdmin.PromoPriority>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PromoPriority
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="float:right;width:97%;">
    <br />
    <h2>PromoPriority</h2>
    <br />


    <% Html.BeginForm("UpdatePromoPriority", "SubAdmin", FormMethod.Post, new { onsubmit = "return false" }); %>
    
    <%=Html.HiddenFor(m => m.IsRefresh, new { @class="is-refresh-val-holder"}) %>

    <span style=""> <%=Html.DropDownListFor(m => m.BrandId, Model.BrandIdChoices) %></span>
    <span style="padding-left:5px;"> <%=Html.DropDownListFor(m => m.PaymentType, Model.PaymentTypeChoices) %></span>
    <span style="padding-left:5px;"> <%=Html.DropDownListFor(m => m.PromoType, Model.PromoTypeChoices) %> </span>
    <span style="padding-left:5px;">  <input type="button" value="Refresh promo list" class="refresh-promo-list"/> </span>

    <br/>
    <br />
    <span class="memberText"> <%= ViewData["PageMessage"] %></span>
    <br />

    <% if (Model.Promos != null)
       {%>
            <table id="search-results-table">
            <tr class="promo-header-row nodrop nodrag">
                <th colspan="3"></th>
                <th>List Order</th>
                <th>Promo ID</th>
                <th>Promo Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Approval Status</th>
            </tr>
           <%for (int i = 0; i < Model.Promos.Count; i++)
           { %>
                <%=Html.EditorFor(m => m.Promos[i], "PromoEnginePromo", new { BrandID = Model.BrandId } )%>
        <% }%>
            </table>
       <%} %>
    <br />
    <input type="button" value="Update the Ordering" class="update-promo-ordering"/>
    <% Html.EndForm();%>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <script src="/Scripts/jquery.tablednd.0.7.min.js" type="text/javascript" ></script>
 <link href="../../Content/tablednd.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript">

    function swapListOrder(initiator, target, direction) {
        // for moving it to top or bot of the list, call a different function
        if (direction == "UU" || direction == "DD") {
            moveToBotOrTop(initiator, target, direction);
            return;
        }
        
        // swap the list order before swapping the rows
        if (direction == "U") {
            incrementListOrderHolderAndDisplay(target, 1);
            incrementListOrderHolderAndDisplay(initiator, -1);
            initiator.insertBefore(target);
        }
        else {
            incrementListOrderHolderAndDisplay(target, -1);
            incrementListOrderHolderAndDisplay(initiator, 1);
            initiator.insertAfter(target);
        }
    }
    
    function moveToBotOrTop(initiator, target, direction) {
        if (direction == "UU") {
            setListOrderHolderAndDisplay(initiator, 1);
            initiator.prevAll(".promo-sortable-row").each(function () {
                incrementListOrderHolderAndDisplay($(this), 1);
            });
            initiator.insertBefore(target);
        }
        else {
            setListOrderHolderAndDisplay(initiator, target.find("td:first").find("input.promo-listorder-holder").val());
            initiator.nextAll(".promo-sortable-row").each(function () {
                incrementListOrderHolderAndDisplay($(this), -1);
            });
            initiator.insertAfter(target);    
        }
    }

    function incrementListOrderHolderAndDisplay(rowObject, amountToIncrement) {
        var newListOrder = parseInt(rowObject.find("td:first").find("input.promo-listorder-holder").val()) + amountToIncrement;

        rowObject.find("td:first").find("input.promo-listorder-holder").val(newListOrder);
        rowObject.find("td.promo-listorder-display").html(newListOrder);
    }

    function setListOrderHolderAndDisplay(rowObject, listOrder) {
        rowObject.find("td:first").find("input.promo-listorder-holder").val(listOrder);
        rowObject.find("td.promo-listorder-display").html(listOrder);
    }

    $(document).ready(function () {

        // Initialise the first table (as before)
        $("#search-results-table").tableDnD();

        $("#search-results-table tr:even").addClass("alt");

        $("#search-results-table").tableDnD({
            onDragClass: "myDragClass",
            dragHandle: ".dragHandle"
        });


        $(".refresh-promo-list").click(function () {
            $(".is-refresh-val-holder").val("True");
            // this bypasses the jQuery bound event allowing the form to submit normally.
            $("form")[0].submit();
        });

        $(".update-promo-ordering").click(function () {

            var currentIndex = 0;
            var totalRows = parseInt($(".promo-sortable-row").length);

            $(".is-refresh-val-holder").val("False");

            //update all the order
            $("#search-results-table").find(".promo-sortable-row").each(function () {

                if (currentIndex != totalRows) {
                    currentIndex = currentIndex + 1;
                    $(this).find(".promo-listorder-holder").val(currentIndex);
                    $(this).find("td.promo-listorder-display").html(currentIndex);
                }

            });

           
            if (currentIndex === totalRows) {
                // this bypasses the jQuery bound event allowing the form to submit normally.
                $("form")[0].submit();
            }
        });

        $(".promo-order-button").click(function () {

            if ($(this).val() == "UP") {
                if ($(this).parent().parent().prevAll(".promo-sortable-row:first").length > 0) {
                    swapListOrder($(this).parent().parent(), $(this).parent().parent().prevAll(".promo-sortable-row:first"), "U");
                }
                else {
                    alert("Cannot go up anymore");
                }
            }
            else if ($(this).val() == "DOWN") {
                if ($(this).parent().parent().nextAll(".promo-sortable-row:first").length > 0) {
                    swapListOrder($(this).parent().parent(), $(this).parent().parent().nextAll(".promo-sortable-row:first"), "D");
                }
                else {
                    alert("Cannot go down anymore");
                }
            }
            else if ($(this).val() == "UU") {
                if ($(this).parent().parent().prevAll(".promo-sortable-row:last").length > 0) {
                    swapListOrder($(this).parent().parent(), $(this).parent().parent().prevAll(".promo-sortable-row:last"), "UU");
                }
                else {
                    alert("Cannot go up anymore");
                }
            }
            else if ($(this).val() == "DD") {
                if ($(this).parent().parent().nextAll(".promo-sortable-row:last").length > 0) {
                    swapListOrder($(this).parent().parent(), $(this).parent().parent().nextAll(".promo-sortable-row:last"), "DD");
                }
                else {
                    alert("Cannot go down anymore");
                }
            }
            else {
                alert("Invalid button function");
            }
        });




    });


</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Search Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", this.ViewData.Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
