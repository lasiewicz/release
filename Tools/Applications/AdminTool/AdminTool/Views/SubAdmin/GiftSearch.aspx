﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SubAdmin.GiftSearchModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	GiftSearch
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    function GetGiftDetails() {
        var giftCode = $('#GiftCode').val();

            $.ajax({
                type: "GET",
                url: '/SubAdminAPI/GetGiftDetails?giftCode=' + giftCode,
                dataType: 'html',
                success: function (result) {
                    $('#SearchResults').html(result);
                }
            });
        }

        $(document).ready(function () {
            var giftCode = $('#GiftCode').val();
            if(giftCode.length > 0) {
                $.ajax({
                    type: "GET",
                    url: '/SubAdminAPI/GetGiftDetails?giftCode=' + giftCode,
                    dataType: 'html',
                    success: function (result) {
                        $('#SearchResults').html(result);
                    }
                });
                
            }
        });
    </script>
   
   <div style="float:right;width:85%;">

    
    <div style="padding-top:20px; padding-bottom:20px;"> Enter the gift code: <%=Html.TextBox("GiftCode") %> <input type="button" id="btnSearch" name="btnSearch" onclick="GetGiftDetails();" value="Search" /></div>
    
    <div id="SearchResults" name="SearchResults"></div>
    
   
  </div> 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>