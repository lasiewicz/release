﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SubAdmin.PlanDisplay>" %>
<%@ Import Namespace="Matchnet" %>
<%@ Import Namespace="Matchnet.Purchase.ValueObjects" %>

<tr<%=Html.Encode(Model.PurchasePlan.EndDate != DateTime.MinValue ? " class='plan-with-end-date'" : string.Empty) %>>

<td><%=Html.DisplayFor(m => m.PurchasePlan.PlanID)%></td>
<td><%=Html.DisplayFor(m => m.PurchasePlan.InitialDuration)%></td>
<td><%=Html.DisplayFor(m => m.PurchasePlan.InitialDurationType)%></td>
<td><%=Html.DisplayFor(m => m.PurchasePlan.InitialCost)%></td>
<td><%=Html.DisplayFor(m => m.IncrementalPrice) %></td>
<td><%=Html.DisplayFor(m => m.PurchasePlan.RenewCost) %></td>
<td><%=Html.DisplayFor(m => m.PurchasePlan.RenewDuration) %></td>
<td><%=Html.DisplayFor(m => m.PurchasePlan.RenewDurationType)%></td>
<td><%=Html.DisplayFor(m => m.FreeTrialDuration) %></td>
<td>
<% if (Model.FreeTrialDurationType != Spark.Common.CatalogService.DurationType.None)
   {%>
    <%=Html.DisplayFor(m => m.FreeTrialDurationType)%>
 <%} %>
</td>
<td><%=Html.Encode( ((int)Model.PurchasePlan.PlanTypeMask & (int)PlanType.Installment) > 0 ? "[0]" : string.Empty) %></td>
<td><%=Html.Encode( ((int)Model.PurchasePlan.PlanTypeMask & (int)PlanType.AuthOnly) > 0 ? "[0]" : string.Empty) %></td>
<td><%=Html.Encode( ((int)Model.PurchasePlan.PlanTypeMask & (int)PlanType.OneTimeOnly) > 0 ? "[0]" : string.Empty) %></td>
<td><%=Html.DisplayFor(m => m.PlanTypeDescription) %></td>
<td><%=Html.DisplayFor(m => m.PurchasePlan.PlanResourcePaymentTypeID) %></td>
<td><%=Html.DisplayFor(m => m.BasicCost) %></td>
<td><%=Html.DisplayFor(m => m.SpotLightCost) %></td>
<td><%=Html.DisplayFor(m => m.HighlightCost) %></td>
<td><%=Html.DisplayFor(m => m.ReadReceipts) %></td>
<td><%=Html.DisplayFor(m => m.JMeterCost) %></td>
<td><%=Html.DisplayFor(m => m.AllAccessCost) %></td>
<td><%=Html.DisplayFor(m => m.AllAcessEmailCost) %></td>
<td><%=Html.DisplayFor(m => m.PurchasePlan.StartDate) %></td>
<td><%=Html.Encode(Model.PurchasePlan.EndDate != DateTime.MinValue ? Model.PurchasePlan.EndDate.ToString("d") : string.Empty) %></td>

</tr>