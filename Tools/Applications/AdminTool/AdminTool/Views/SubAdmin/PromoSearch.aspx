﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master"
 Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SubAdmin.PromoSearch>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	PromoSearch
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="float:right;width:97%;">
   <%Html.BeginForm("SubmitPromoSearch", "SubAdmin"); %>

    <%=Html.Hidden("PromoSearchErrorMessage", Model.ErrorMessage) %>
    <%=Html.Hidden("PromoSearchAutoHide", Model.AutoHideSearchParms) %>
    
    
    <div style="width:100%;">
        <div  class="expandCollapse">Hide</div>
        <div class="PromoSearchParams">
            <% Html.RenderPartial("Controls/PromoSearchParams", Model.MPromoSearchParams); %><br/>
        </div>
    </div>
    <br /><br />
    <div  >
     <% Html.RenderPartial("Controls/PromoSearchResults", Model.MPromoSearchResults); %>
    </div>
    <%Html.EndForm(); %>
</div>
</asp:Content> 

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

 

<script type="text/javascript">
    function ToggleSearchParams() {
        if ($(".expandCollapse").html() == "Hide") {
            $(".expandCollapse").html("Search Again");
        }
        else {
            $(".expandCollapse").html("Hide");
        }

        $(".PromoSearchParams").toggle(400);
    }

    $(document).ready(function () {

        // detect for error message
        if ($("#PromoSearchErrorMessage").val() != "") {
            alertMessage($("#PromoSearchErrorMessage").val(), 0, null);
        }

        // bind the collapse/expand of the search parameters
        $(".expandCollapse").click(function () {
            ToggleSearchParams();
        });

        // check for auto hide search params
        if ($("#PromoSearchAutoHide").val() == "True") {
            ToggleSearchParams();
        }
    });
</script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Search Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", this.ViewData.Model.TopNav); %>
</asp:Content>


