﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SubAdmin.Plan>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	CreatePlan
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div style="float:right;width:97%;">
   
     <%if(ViewData["PageMessage"] != null)
      {%>
        <%= ViewData["PageMessage"] %>    
      <%}%>
    
    <%=Html.ValidationSummary("Please correct the following:") %>
    <br/>
    <% Html.BeginForm("CompleteCreatePlan", "SubAdmin"); %>
    
    <%= Html.Hidden("PlanState", ViewData["PlanState"], new {@class="plan-state-indicator"}) %>

    <div class="plan-input">
     <ul>
        <li class="list-item-edit">
            <label class="memberLabel">Select Web Site:</label> <span style="padding-left:5px;"> <%=Html.DropDownListFor(m=>m.BrandId, Model.BrandIds, new { @class="brand-id-selection"}) %></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Currency:</label> <span style="padding-left:5px;"><div id="CurrencyType"></div></span>
        </li>
    </ul>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel"></label> <span style="padding-left:5px;"> </span>
        </li>
    </ul>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel">Initial Cost: </label> <span style="padding-left:5px;"><%=Html.TextBoxFor(m=>m.InitialCost, new { @class="initial-cost" }) %><%=Html.ValidationMessageFor(m=>m.InitialCost, "*") %></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Duration:</label> <span style="padding-left:5px;"><%=Html.TextBoxFor(m=>m.Duration, new { @class="initial-duration" }) %><%=Html.DropDownListFor(m=>m.DurationType, Model.DurationTypes) %><%=Html.ValidationMessageFor(m=>m.Duration, "*") %></span>
        </li>
         <li class="list-item-edit">
            <label class="memberLabel">Cost per:</label> <span style="padding-left:5px;"> <div id="CostPerLabel"></div></span>
         </li>
    </ul>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel"></label> <span style="padding-left:5px;"> </span>
        </li>
    </ul>
    <ul>
         <li class="list-item-edit">
            <label class="memberLabel">Renewal Cost:</label>  <span style="padding-left:5px;"><%=Html.TextBoxFor(m=>m.RenewalCost) %><%=Html.ValidationMessageFor(m=>m.RenewalCost, "*") %></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Renewal Duration:</label> <span style="padding-left:5px;"><%=Html.TextBoxFor(m=>m.RenewalDuration) %><%=Html.DropDownListFor(m=>m.RenewalDurationType, Model.RenewalDurationTypes) %><%=Html.ValidationMessageFor(m=>m.RenewalDuration, "*") %></span>
        </li>
    </ul>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel"></label> <span style="padding-left:5px;"> </span>
        </li>
    </ul>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel">Free Trial Duration (if any):</label> <span style="padding-left:5px;"><%=Html.TextBoxFor(m=>m.FreeDuration) %><%=Html.DropDownListFor(m=>m.FreeDurationType, Model.FreeDurationTypes) %><%=Html.ValidationMessageFor(m=>m.FreeDuration, "*") %></span>
        </li>
    </ul>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel"></label> <span style="padding-left:5px;"> </span>
        </li>
    </ul>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel">Check Plan?</label> <span style="padding-left:5px;"><%=Html.CheckBoxFor(m=>m.IsCheckPlan) %></span>
        </li>
    </ul>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel"></label> <span style="padding-left:5px;"> </span>
        </li>
    </ul>
    <ul>
        <li class="list-item-edit">
            <label class="memberLabel">Select Plan Type:</label> <span style="padding-left:5px;"><%=Html.DropDownListFor(m=>m.PlanType, Model.PlanTypes, new { @class="plan-type-selection" }) %></span>
        </li>
    </ul>
    <ul>
        <div id="PremiumTypesArea">
            <%foreach (var p in Model.PremiumPlanTypes)
                {%>
                <li class="list-item-edit">
                    <label class="memberLabel"></label>
                    <span style="padding-left:5px;"><%=Html.CheckBox("PremiumPlanTypeCB_" + p.Value, (Model.PremiumPlanTypeMask & Convert.ToInt32(p.Value)) == Convert.ToInt32(p.Value), new { @class = "premium-plan-type-cb", value=p.Value})%> <%=Html.Label(p.Text) %></span>
                </li>
            <%}%>
        </div>
        <%=Html.HiddenFor(m=>m.PremiumPlanTypeMask, new { @class="premium-plan-type-mask"})%>
        
        <div id="ALaCarteArea">
            <%foreach (var alc in Model.ALaCartePlanTypes)
              {%>
                 <li class="list-item-edit">
                      <label class="memberLabel"></label>
                       <span style="padding-left:5px;"><%=Html.RadioButton("ALaCartePlanType", alc.Value) %><%=Html.Label(alc.Text) %></span>
                  </li>
            <%}%>
        </div>
    </ul>

    <br /><br />

    <div id="PriceBreakdownArea">
        <ul>
            <%for (var i = -1; i < Model.PremiumPlansPriceBreakdowns.Count(); i++)
            {%>
                <li class="list-item-edit">
                    <%if (i == -1) 
                    {%>
                        <span style="width: 180px; text-align: center;"><strong>Type</strong></span>
                        <span style="width: 167px; text-align: center; margin-left: 5px; padding: 2px;"><strong>Initial Cost</strong></span>
                        <span style="width: 167px; text-align: center; margin-left: 5px; padding: 2px;"><strong>Renewal Cost</strong></span>
                        <span style="width: 167px; text-align: center; margin-left: 5px; padding: 2px;"><strong>Cost</strong></span>
                        <span style="width: 167px; text-align: center; margin-left: 5px; padding: 2px;"><strong>Is DisburseCount?</strong></span>
                    <%}
                    else
                    {%>
                        <div class="<%="pb-" + Model.PremiumPlansPriceBreakdowns[i].PremiumPlanType %> pb-row">
                            <%=Html.EditorFor(x => x.PremiumPlansPriceBreakdowns[i], "PlanPriceBreakdown")%>   
                        </div>  
                    <%}%>
                </li>
            <%} %>
        </ul>
    </div>
    
      <br /><br /><br />

      <ul>
        <li class="list-item-edit">
            <label class="memberLabel">One time only</label> 
            <span style="padding-left:5px;"> <%=Html.CheckBox("AdditiveMask_2", (Model.AdditiveMask & 2) == 2, new { @class="additive-mask-cb", value="2"})%></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Auth only (includes Free Trial Welcome) </label> 
            <span style="padding-left:5px;"><%=Html.CheckBox("AdditiveMask_20", (Model.AdditiveMask & 20) == 20, new { @class = "additive-mask-cb", value="20" })%></span>
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">Installment </label> 
             <span style="padding-left:5px;"><%=Html.CheckBox("AdditiveMask_64", (Model.AdditiveMask & 64) == 64, new { @class = "additive-mask-cb", value="64" })%></span>        
        </li>
        <li class="list-item-edit">
            <label class="memberLabel">
                <%=Html.HiddenFor(x=>x.AdditiveMask, new { @class="additive-mask"}) %>
            </label>
        </li>
       </ul>
    </div>

    <div class="plan-summary">
    <%=Html.Partial("Controls/PlanSummary", Model) %>
    </div>
    <div style="width:15%; padding-left:250px;">
        <span style="padding-right:5px;"><input type="submit" value="Create Plan" class="button-search" /></span> 
        
        <span style="padding-left:5px;float:right;"><input type="button" value="Cancel" class="cancel-plan-creation"/></span>
    </div>
    
    <%Html.EndForm(); %>

</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 

<script type="text/javascript" language="javascript">
    function setToStandardPlanUI() {
        $("#PremiumTypesArea").hide();
        $("#ALaCarteArea").hide();
        $("#PriceBreakdownArea").hide();
        $(".pb-row").hide();
        getCurrencyString($(".brand-id-selection").val());
    }

    function bindUI() {
        setToStandardPlanUI();
        handlePlanTypeSelection();
        handlePremiumPlanTypeSelections();
        
        if ($(".plan-state-indicator").val() == "ViewInfo" || $(".plan-state-indicator").val() == "Inserted") {
            $(".plan-input").hide();
            $(".plan-summary").show();
        }
        else {
            $(".plan-input").show();
            $(".plan-summary").hide();
        }
    }
    
    function handlePremiumPlanTypeSelections() {
        $(".premium-plan-type-cb:checked").each(function (index) {
            showPriceBreakdown($(this).val());
        });
    }
    
    function handlePlanTypeSelection() {
        if ($(".plan-type-selection").val() == "256") // premium service plan
        {
            $("#ALaCarteArea").hide();
            $("#PremiumTypesArea").show();
            handlePremiumPlanTypeSelections();
        }
        else if ($(".plan-type-selection").val() == "1024") // a la carte plan
        {
            $("#PremiumTypesArea").hide();
            $("#PriceBreakdownArea").hide();
            $("#ALaCarteArea").show();
        }
        else {
            setToStandardPlanUI();
        }
    }

    function showPriceBreakdown(pbNum) {
        if ($(".pb-row:visible").length == 0) {
            $("#PriceBreakdownArea").show();
            $(".pb-0").show();
        }

        $(".pb-" + pbNum).show();
        $(".premium-plan-type-mask").val($(".premium-plan-type-mask").val() | pbNum);
    }

    function hidePriceBreakdown(pbNum) {
        $(".pb-" + pbNum).hide();
        
        if ($(".pb-row:visible").length == 1) {
            $(".pb-0").hide();
            $("#PriceBreakdownArea").hide();
        }

        $(".premium-plan-type-mask").val($(".premium-plan-type-mask").val() & ~pbNum);
    }
    
    function getCurrencyString(brandid) {
        $.ajax({
            type: "POST",
            url: "/SubAdminAPI/GetCurrencyString/",
            data: { BrandId: brandid },
            success: function (result) {
                $("#CurrencyType").html(result.StringResult);
            }
        });
    }
    
    function calculatePricePer() {
        var price = Number($(".initial-cost").val());
        var duration = Number($(".initial-duration").val());
        var pricePer;
        
        if(isNaN(price) || isNaN(duration) || price == 0 || duration == 0) {
            pricePer = 0;
        }
        else {
            pricePer = price / duration;
            pricePer = Math.round(pricePer * 100) / 100;
        }

        $("#CostPerLabel").html(pricePer);
    }

    $(document).ready(function () {
        bindUI();

        $(".additive-mask-cb").change(function () {
            if ($(this).attr("checked")) {
                $(".additive-mask").val($(".additive-mask").val() | $(this).val());
            }
            else {
                $(".additive-mask").val($(".additive-mask").val() & ~$(this).val());
            }
        });

        $(".brand-id-selection").change(function () {
            getCurrencyString($(this).val());
        });

        $(".premium-plan-type-cb").change(function () {
            if ($(this).attr("checked")) {
                showPriceBreakdown($(this).val());
            }
            else {
                hidePriceBreakdown($(this).val());
            }

        });

        $(".plan-type-selection").change(function () {
            handlePlanTypeSelection();
        });

        $(".initial-cost").change(function () {
            calculatePricePer();
        });

        $(".initial-duration").change(function () {
            calculatePricePer();
        });

        $(".cancel-plan-creation").click(function () {
            window.location.href = "/";
        });
    });
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Search Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", this.ViewData.Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
