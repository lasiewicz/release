﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SubAdmin.PlanDisplayList>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	PlanList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="float:right;width:85%;">
    <br />
    <h2>PlanList</h2>

    <%=Html.DropDownListFor(m => m.BrandId, Model.BrandIdChoices, new {@class="selected-brandid"}) %>
    <input type="button" class="refresh-plan-list" value="Refresh"/>
    <br/>

    <% if (Model.PlansToDisplays != null)
       { %>
    <table class="plan-list-table">
        <tr>
            <th>Plan ID</th>
            <th>Initial</th>
            <th>duration</th>
            <th>Initial price</th>
            <th>Incremental price</th>
            <th>Renewal price</th>
            <th>Renewal</th>
            <th>duration</th>
            <th>Free Trial Duration</th>
            <th>Free Trial Duration Type</th>
            <th>Installment?</th>
            <th>Auth only?</th>
            <th>One time only?</th>
            <th>Plan type</th>
            <th>Payment type</th>
            <th>Base subscription price</th>
            <th>Member Spotlight</th>
            <th>Highlight Profile</th>
            <th>Read Receipts</th>
            <th>J-Meter</th>
            <th>All Access</th>
            <th>All Access Email</th>
            <th>Start Date</th>
            <th>End Date</th>
        </tr>

        <%=Html.DisplayFor(m => m.PlansToDisplays)%>
    </table>
    <% } %>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<%--take the style out. i only included so that it's easier for me to validate certain things--%>

 

<style type="text/css">
    tr { font-size: small; }
    tr.evenRow { background-color: lightgoldenrodyellow; }
    tr.plan-with-end-date { background-color: lightgrey; }
    th { font-size: x-small; padding: 5px; background-color: peru; }
    td { padding: 5px; }
</style>

<script src="<%= ResolveUrl("~/Scripts/scrollbarTable-0.1.min.js") %>" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".refresh-plan-list").click(function () {
            window.location.href = "/SubAdmin/PlanList/" + $(".selected-brandid").val();
        });

        $(".plan-list-table").scrollbarTable(600);

        $(".plan-list-table tr:even").addClass("evenRow");
    });
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Search Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", this.ViewData.Model.TopNav); %>
</asp:Content>


<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
