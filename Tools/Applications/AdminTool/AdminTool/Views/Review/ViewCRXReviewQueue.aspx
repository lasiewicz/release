﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.TextApprovalReviewModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	CRX Second Review Queue
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%Html.BeginForm("SubmitCRXReview", "Review"); %>
<% if (Model.ExpandedUserInfo != null)
   { %>
    <% Html.RenderPartial("../Review/Controls/ReviewContent", Model); %>  
    <div class="review-notes">
         <input id="btnUndoEdits" type="button" value="Undo Edits" />&nbsp;&nbsp;
         <input id="btnApprove" type="submit" value="Approve" />&nbsp;&nbsp;
         <input id="btnSuspend" type="button" value="Suspend/Terminate" />
         <br />
         <br />
         <span class="smallText approval-notes">(Notes added on Approve <br /> Or Suspend)</span>
         <%=Html.TextArea("AdminNotes", "", new { style = "width: 250px;height: 100px" })%>
         <div id="SuspendReasons" class="suspend-reasons" visible="false">
            <%=Html.DropDownList("AdminActionReasonId", Model.AdminSuspendReasons ) %><br />
          <div>
            <input  id="btnConfirmSuspend" type="submit" value="Confirm Suspend" />
          </div>
         </div> 
         <%=Html.Hidden("Suspend", "false")%>               
    </div>
<% } 
    else
    { %>
    <p class="no_more_item">No more items to review</p>
<% } %>
<%Html.EndForm(); %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="/Content/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="/Scripts/tiny_mce/tiny_mce.js"></script>
    <script src="/Scripts/jquery.multiselect.js" type="text/javascript" ></script>
    <script src="/Scripts/review.js" type="text/javascript" ></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
<%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
<% Html.RenderPartial("Controls/ReviewLeft", Model.Left); %>
</asp:Content>
