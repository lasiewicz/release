﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.TextApprovalReviewModelView>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Review" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%= Model.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
<%Html.BeginForm("SubmitTextReview", "Review"); %>
<% if (Model.ExpandedUserInfo != null)
   { %>
    <% Html.RenderPartial("../Review/Controls/ReviewContent", Model); %>  
    
    <div class="review-notes">
        <h3>Additional Training Notes</h3><br/>
        <%=Html.TextAreaFor(m => m.TrainingNotes)%>
        <br/><br/>
        <input type="submit" name="ReviewAction" value="Approve Agent Actions"/>
        <input id="btnSendTrainingReport" type="submit" name="ReviewAction" value="Send Training Report" />
    </div>
    <% }
       else
       { %>
    <p class="no_more_item">No more items to review</p>
    <% } %>
<%Html.EndForm(); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="/Content/jquery.multiselect.css" rel="stylesheet" type="text/css" />
     <script src="/Scripts/jquery.multiselect.js" type="text/javascript" ></script>
     <script src="/Scripts/review.js" type="text/javascript" ></script>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <% Html.RenderPartial("Controls/ReviewLeft", Model.Left); %>
</asp:Content>
