﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.SODBReviewModelView>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Review" %>
<%@ Import Namespace="Spark.SODBQueue.ValueObjects" %>

<asp:Content ID="Content6" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript" src="/Scripts/jquery.gallery.0.3.min.js"></script>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%=Model.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<style>
  .gallery{
  position: relative;
  overflow: hidden;
  background-color: #666;
  padding: 12px;
  height: 450px;
}
.galleryBar {
  position: absolute;
  left: 0;
  margin: 0;
  padding: 0 7px;
  display:none;
}

.galleryBar li {
  position: relative;
  margin: 0 7px 0 0;
  padding: 0;
  overflow: hidden;
  float: left;
  list-style: none;
  height:100px;
}
.galleryScreen, .galleryInfo {
  position: absolute;
}

.galleryScreen {

}
.galleryInfo {
 background-color: transparent;
 color: #fff;
 padding: 12px;
}
.galleryTitle {
 font-size: 135%;
 font-weight: bold;
}
  
    
#hor-minimalist-b
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	background: #fff;
	 
	width: 280px;
	border-collapse: collapse;
	text-align: left;
}
#hor-minimalist-b th
{
	font-size: 12px;
	font-weight: bold;
	color: #039;
	padding: 10px 8px;
	border-bottom: 2px solid #6678b1;
	text-align:left;
}
#hor-minimalist-b td
{
	border-bottom: 1px solid #ccc;
	color: #669;
	padding: 6px 8px;
}
#hor-minimalist-b tbody tr:hover td
{
	color: #009;
}

.hor-minimalist-label
{
     
	color: #669;
	padding: 6px 8px;
	font-size: 12px;
}

.hor-minimalist-header
{
    margin: 45px;
    font-size: 12px;
	font-weight: normal;
	color: #039;
	padding: 10px 8px;
	text-align:left;
}
 
 
  .sodbUL
 {
    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	background: #fff;
	border-collapse: collapse;
	color: #669;
	
      
 }
 
 .sodbUL li
 {
    padding-bottom:10px;
      
 }
 
 .admin-actions { clear: both;font-size: 11px; }
 .admin-actions tr th { text-align: left;}
 .admin-actions tr td { padding-right: 5px;}
 
 
 .escalate
 { 
     resize:none; 
     max-width:250px;
     max-height:150px;
 }
 
</style>



<script type="text/javascript">
    $(document).ready(function () {

        $('#soError').hide();

        $('.admin-note').hide();

        $('#agreebtn').click(function () {
            $('#closeprompt').lightbox_me();
            $('#closeprompt').trigger('close');
            $('#reviewform').submit();
            return true;
        });

        $('#cancelbtn').click(function () {
            $('#closeprompt').lightbox_me();
            $('#closeprompt').trigger('close');
            return false;
        });

        $('#btnSubmit').click(function () {


            if ($('input[name=sodbDecision]').is(':checked')) {

                if ($('input[name=sodbDecision]:checked').val() == '1') { //1 - positive match

                    $('#sodbprompt').lightbox_me({
                        centered: true
                    });
                    return false;
                }
                else {
                    return true;
                }

            }
            else {
                $('#soError').show();
                return false;
            }


        });

        $('input[name=sodbDecision]').change(function () {
            var decision = $(this).val();
            $('.admin-note').hide();
            if (decision == 5) { //5 - escalate
                //show textbox to type notes
                $('.admin-note').show();
            }
        });

        $('#ssp_blue_info_id_of_gallery').gallery({
            height: '500px',
            width: '500px',
            slideshow: false,
            thumbHeight: 70,
            thumbWidth: 70
        });

        $('#spark_info_id_of_gallery').gallery({
            height: '500px',
            width: '500px',
            slideshow: false,
            thumbHeight: 70,
            thumbWidth: 70

        });

    });
</script>


<div style="float:right;width:97%; position:relative;">
 
<%Html.BeginForm("SubmitSODBDecision", "Review", FormMethod.Post, new { id = "reviewform" }); %>
<%if (Model != null && Model.SparkInfo != null && Model.SODBInfo != null)
  {%>
    <%=Html.HiddenFor(m => m.MemberId)%>
    <%=Html.HiddenFor(m => m.SiteId)%>
    <%=Html.HiddenFor(m => m.SSPBlueId)%>
    <%=Html.HiddenFor(m => m.SodbImageUrls)%>
    <%=Html.HiddenFor(m => m.IsSecondReview)%>
    <%=Html.HiddenFor(m => m.IsWaitingforSSP)%>
    <%=Html.HiddenFor(m => m.Email)%>
    <%=Html.HiddenFor(m => m.IsBHMember)%>
    <div style=" min-height:400px;  margin-top:20px; margin-bottom:20px; margin-left:10px; float:left;  ">
        <div style="float:left;">
            <%Html.RenderPartial("../Review/Controls/SODBUserInfo", Model.SparkInfo,new ViewDataDictionary { { "IsSparkInfo", true } });%>
         </div>
         <div style="float:right;margin-left:10px;">
            <%Html.RenderPartial("../Review/Controls/SODBUserInfo", Model.SODBInfo,new ViewDataDictionary { { "IsSparkInfo", false }, { "Level", Model.Level } });
      %>
        </div>
    </div>
    <%if(Model.IsSecondReview) { %>
    <div style="clear:both;margin-left:10px;padding-top:10px;font-size:13px; color:#666699; font-family:Lucida Sans Unicode,Lucida Grande,Sans-Serif">   
        <strong>Admin Notes: </strong> <%=Model.AdminNotes %>
    </div>
    <% } %>
    <%if(Model.AdminActions != null && Model.AdminActions.Count > 0) {%>
    <table class="admin-actions">
        <tr>
            <th>Admin</th><th>Action</th><th>Date</th>
        </tr>
        <% foreach (SodbAdminAction adminAction in Model.AdminActions)
        {%>
        <tr>
        <td><%=ReviewManager.GetMemberEmail(adminAction.AdminId) %></td><td><%= (SODBDecision)adminAction.AdminActionId %></td><td><%=adminAction.ActionDate %></td>
        </tr>
        <%} %>
    </table>
    <% } %>
    <div style=" position:absolute; top:5px; right:2%; float:right; ">
        <ul class="sodbUL">
            <li id="soError">
                <span class="error" >Please select an option.</span>
            </li>
            <li>
                <%=Html.RadioButton("sodbDecision", (int)SODBDecision.PositiveMatch,
                                         false)%><span>Positive Match</span> 
            </li>
            <li>
                <%=Html.RadioButton("sodbDecision", (int)SODBDecision.NegativeMatch,
                                                             false)%><span>100% Not a Match</span>
            </li>
            <% if (!Model.IsWaitingforSSP) { %>
            <li>
                <%=Html.RadioButton("sodbDecision", (int) SODBDecision.RequestMoreInfo,
                                        false)%><span>Request more info (from SSP) </span>
            </li>
            <% } %>
            <li>
                <%=Html.RadioButton("sodbDecision", (int)SODBDecision.RequestPhoto, Model.SelectedDecision == (int)SODBDecision.RequestPhoto) %><span>Request Photo from Member</span>
            </li>
            <% if (!Model.IsSecondReview) { %>
            <li>
                <%=Html.RadioButton("sodbDecision", (int)SODBDecision.Escalate,
                                                           false)%><span>Escalate </span>
                                                           <span class="admin-note"> <%=Html.TextAreaFor(m => m.AdminNotes, new {rows = 3, cols = 20, @class ="escalate"})%></span>
            </li>
            <% } %>
            <li>
                <input id="btnSubmit" type="submit" value="Submit Decision" class="button" />
            </li>
        </ul>
    </div>

    <div class="modal" id="sodbprompt" >
        <div style="text-align:center;width:400px;padding-left:200px;">
            <table id="hor-minimalist-b" style="text-align:left;width:400px;">
             <thead>
                <tr>
                    <th> Have you completed the following ?</th>
                </tr>
             </thead>
             <tbody>
                <tr>
                   <td>1) Suspend member <br /></td>
                </tr>
                <tr>
                    <td>2) Terminate autorenewals & prorate refund </td>
                </tr>
            </tbody>
            </table>
            <br />
              <input type="button" id="agreebtn" value="I have completed the required actions" class="button" /> 
               <span style="padding-left:30px;"><input type="button" id="cancelbtn" value="Cancel" class="button" /></span>        
        </div> 
    </div>
     <input type="hidden" id="closeprompt" />  
    <% }
      else
      { %>
     <div> Queue is empty</div>
     <% } %>
<%Html.EndForm(); %>
</div>
</asp:Content>

 

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <% Html.RenderPartial("Controls/ReviewLeft", Model.Left); %>
</asp:Content>
