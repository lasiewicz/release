﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.OnDemandQueueCountsModelView>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Review" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	On Demand Review QueueManagement
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br/>
    <h2>On-Demand Agent Review Queues</h2>
    <br/><br/>
    <table class="ondemand-review-count">
        <tr>
            <th>Agent</th>
            <th>Date Added to Queue</th>
            <th>Added By</th>
            <th>Items Remaining</th>
            <th>Oldest Item</th>
        </tr>
        <% if (Model.OnDemandQueueCounts.Count > 0) {
               for (int i =0; i<Model.OnDemandQueueCounts.Count; i++)
               { %>
               <tr>
                    <td><%= Model.OnDemandQueueCounts[i].AdminEmail %></td>
                    <td><%= Model.OnDemandQueueCounts[i].DateQueueAdded.ToShortDateString() %></td>
                    <td><%= Model.OnDemandQueueCounts[i].SupervisorEmail %></td>
                    <td><%= Model.OnDemandQueueCounts[i].TotalItems %></td>
                    <td><%= Model.OnDemandQueueCounts[i].OldestItem.ToShortDateString()%></td>
                    <td>
                        <input class="btnPurge" type="button" value="Purge" data-adminmemberid= "<%= Model.OnDemandQueueCounts[i].AdminMemberId %>" 
                        data-supervisorid = "<%= Model.OnDemandQueueCounts[i].SupervisorId %>"/>
                    </td>
                    <td>
                        <input class="btnProcess" type="button" value="Process"  data-adminmemberid= "<%= Model.OnDemandQueueCounts[i].AdminMemberId %>" 
                        data-supervisorid = "<%= Model.OnDemandQueueCounts[i].SupervisorId %>"/>
                     </td>
               </tr>
        
               <% } %>
        <% } %>
    </table>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
<script src="/Scripts/OnDemandReview.js" type="text/javascript" ></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
<%Html.RenderPartial("../Shared/Controls/TopNav", Model.SharedTopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
<%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
