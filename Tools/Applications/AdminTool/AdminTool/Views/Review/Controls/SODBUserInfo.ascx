﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Review.SODBMemberInfo>" %>


<div  >
<% string prefix = "spark_info"; string textAlign = "left"; %>
 <table id="hor-minimalist-b">
        <thead>
    	    <tr>
                <th scope="col" colspan="2">
                <% if ((bool)ViewData["IsSparkInfo"])
                   {  %> 
                        Spark Profile Info
                 <% }
                   else
                   {
                       prefix = "ssp_blue_info";
                       textAlign = "right";
                  %>
                        SSP Blue Info   <span style="text-align:right;padding-left:140px;"> (<%=ViewData["Level"] %>)</span>
                 <%} %>
                 </th>
            </tr>
            </thead>
        <tbody>
        <tr>
            <td>First Name:</td><td><%=Html.DisplayTextFor(m=>m.FirstName) %></td>
        </tr>
        <tr>
            <td>Last Name:</td><td><%=Html.DisplayTextFor(m => m.LastName)%></td>
        </tr>
        <tr>
            <td>Date of Birth:</td><td><%=Model.DateOfBirth.ToShortDateString()%></td>
        </tr>
        <tr>
            <td>Zip Code:</td><td><%=Html.DisplayTextFor(m => m.ZipCode)%></td>
        </tr>
        <tr>
            <td>State:</td><td><%=Html.DisplayTextFor(m => m.State)%></td>
        </tr>
        <tr>
            <td>Gender:</td><td><%=Html.DisplayTextFor(m => m.Gender)%></td>
        </tr>
        <tr>
            <td colspan="2">
                <% if (Model.ShowViewMemberLink)
                   { %>
                   <a href="<%=Model.ViewMemberLink %>" target="_blank">Review Member Account</a>
                <% }
                   else
                   { %>
                    <span><br /></span>
                <%} %>
            </td>
        </tr>
    </tbody>
    </table>
      
    

</div>
 
    <%if (Model.PhotoUrls != null && Model.PhotoUrls.Count > 0)
      { %>
       
          <div id="<%=prefix%>_id_of_gallery" class="gallery"> 
          <ul class="galleryBar">
    <%
          foreach (string srcUrl in Model.PhotoUrls)
          {  
    %>
        <li><a href="<%=srcUrl%>" ><img src="<%=srcUrl%>" style="width:100px; height:100px;" /></a> </li>
    <% } %>
         </ul>
            </div>
        <div style="text-align:<%=textAlign%>;  padding-top:20px;">
            <span>Number of photos: <%=Model.PhotoUrls.Count.ToString() %></span><br />
            <span class="sodbUL">(hover over image to see additional photos)</span>
        </div>
        
          
     <% } %>

     





