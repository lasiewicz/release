﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Review.TextApprovalReviewModelView>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Review" %>

<% Html.RenderPartial("../Member/Controls/ExpandedUserInfo", Model.ExpandedUserInfo); %>  
      <%=Html.HiddenFor(m => m.MemberTextApprovalId)%>
      <%=Html.HiddenFor(m => m.AdminMemberId)%>
      <%=Html.HiddenFor(m => m.CommunityId)%>
      <%=Html.HiddenFor(m => m.LanguageId)%>
     <%=Html.HiddenFor(m => m.IsOnDemand)%>
     <%=Html.HiddenFor(m => m.MemberId)%>
     <%=Html.HiddenFor(m => m.ProcessSingleAdmin)%>
     <%=Html.HiddenFor(m => m.SupervisorId)%>
<div style="float:right;width:85%;">
<div class="review_attr">

<% for (int i = 0; i < Model.Attributes.Count; i++)
{
    TextAttribute attribute = Model.Attributes[i]; %>
        <%=Html.HiddenFor(m => Model.Attributes[i].AttributeGroupId)%>
        <%=Html.HiddenFor(m => Model.Attributes[i].StatusMask)%>
        <%=Html.HiddenFor(m => Model.Attributes[i].AttributeName)%>
        <%=Html.HiddenFor(m => Model.Attributes[i].TextContent)%>
        <%=Html.HiddenFor(m => Model.Attributes[i].AdminTextContent)%>

        <script  type="text/javascript">
            attributeIDArray.push(<%= attribute.AttributeGroupId.ToString() %>);
        </script>

            <div class="old">
                <div class="attr-name"> <%=attribute.AttributeName%><br/>(original): </div>
                <div class="attr-value" style="width: 615px;"> <%=attribute.TextContentHtml%> </div>
                <div class="review_status"> 
                    <div class="review_option">
                        <%=Html.RadioButton("OldTextAcceptable" + attribute.AttributeGroupId.ToString(), "true",
                                        attribute.StatusMask == 0, new {disabled = "disabled"})%>Good 
                    </div>
                    <div class="review_option">
                        <%=Html.RadioButton("OldTextAcceptable" + attribute.AttributeGroupId.ToString(), "false",
                                        attribute.StatusMask > 0, new {disabled = "disabled"})%>Bad 
                    </div>
                    <br/>
                     <div class="<%=(attribute.StatusMask == 0) ? "hideMe":""%> options crx_reasons">
                        <%=Html.ListBox("Reasons" + attribute.AttributeGroupId.ToString(),
                                    attribute.IndividualTextApprovalStatuses, new {@class = "reasonsDisabledStyle"})%>
                    </div>
                </div>
            </div>       
            <div class="new">
                <div class="attr-name"> <%=attribute.AttributeName%><br/>(agent): </div>
                <% if (Model.ModifyTextContent) {%>
                 <div class="attr-text-value" id="<%=attribute.AttributeGroupId.ToString()%>" style="width:615px;"> 
                    <%=Html.TextArea("Attribute" + attribute.AttributeGroupId.ToString(), attribute.AdminTextContentHtml, new { style = "width: 615px; font-size:11pt;font-family:arial,sans-serif;" })%>
                    <%--<%=Html.Hidden("AttributeName" + attribute.Number.ToString(), attribute.AttributeName)%>
                    <%=Html.Hidden("AttributeGroupID" + attribute.Number.ToString(), attribute.AttributeGroupID.ToString())%>
                    <%=Html.Hidden("LanguageID" + attribute.Number.ToString(), attribute.LanguageID.ToString())%>--%>
                    <%=Html.Hidden("OriginalValue" + attribute.AttributeGroupId.ToString(), attribute.AdminTextContentHtml)%>
                    <%=Html.Hidden("MaxLength" + attribute.AttributeGroupId.ToString(), attribute.MaxLength.ToString())%>
                    <div id="ValidationMessage<%=attribute.AttributeGroupId.ToString() %>" visible="false" style="display: none; color:Red"><br />This text can only be <%=attribute.MaxLength.ToString()%> characters long - it has <span id="CharactersTooLong<%=attribute.AttributeGroupId.ToString() %>"></span> too many characters.
                    </div>
                </div>
                <% } else { %>
                <div class="attr-value"> <%=attribute.AdminTextContentHtml%> </div>
                <% } %>
                
                <div class="review_status">
                 <!-- Show this option only for CRX review queue --> 
                  <% if (Model.ModifyTextContent) {%>
                  <table>
                    <tr>
                        <td>
                            <div id="CrxReview<%=attribute.AttributeGroupId.ToString() %>" style="width:170px; ">
                                <%=Html.CheckBoxFor(m=> m.Attributes[i].CRXTie) %><span style="color:Green">CRX assessment okay</span><br/>
                                <input type="checkbox" id="crx_approval"  /><span>CRX wrong, standard</span><br/>
                                <%=Html.CheckBoxFor(m => m.Attributes[i].ToolCorrectionRequested)%><span style="color:#990000;">CRX very wrong, attn required</span>
                            </div>
                        </td>
                        <td>
                            <div style="width:100px;display:none; clear" id="validateCopy<%=attribute.AttributeGroupId.ToString() %>">
                                <p style="color:#FF0000;"><b>*required</b></p>
                            </div>
                            <div style="width:100px;display:none;" id="redCopy<%=attribute.AttributeGroupId.ToString() %>">
                            <p style="color:#990000;">
                                Your selection suggests the CRX Tool incorrectly analyzed this essay. It is required to check the checkbox that applies.
                            </p>
                            </div>
                            <div style="width:100px;display:none;" id="blueCopy<%=attribute.AttributeGroupId.ToString() %>">
                                <p style="color:blue;">
                                    Your selection suggests the CRX Tool correctly analyzed this essay. You may still select one of the checkbox's if any portion was missed by CRX.
                                </p>
                            </div>
                        </td>
                    </tr>
                   </table>
                    <% } %>
                    <div class="review_option">
                        <%=Html.RadioButton("TextAcceptable" + attribute.AttributeGroupId.ToString(), "true",
                                        attribute.StatusMask == 0)%>Good 
                    </div>
                    <div class="review_option">
                        <%=Html.RadioButton("TextAcceptable" + attribute.AttributeGroupId.ToString(), "false",
                                        attribute.StatusMask > 0)%>Bad 
                    </div>
                    <br/>
                     <div id="divTextUnacceptableReasons<%=attribute.AttributeGroupId.ToString()%>" class="<%=(attribute.StatusMask == 0) ? "hideMe" : ""%> options">
                        <span id="TextUnAcceptableValidation<%=attribute.AttributeGroupId.ToString()%>"  visible="false" style="display: none; color:Red">Please select at least one option from the list</span>
                        <br/>
                        <%=Html.ListBox("TextUnacceptableReasons" + attribute.AttributeGroupId.ToString(),
                                    attribute.IndividualTextApprovalStatuses, new {@class = "reasonsStyle"})%>
                    </div>
                </div>
                <%if (!string.IsNullOrEmpty(attribute.CRXNotes))
                { %>
                <div>
                <div class="attr-name" style="padding-top:6px; margin-top:0px;">CRX Hints:</div>
                <div style="width:715px;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:13px;">
                    <%=attribute.CRXNotes %>
                </div>
                </div>
                <% } %>
            </div>  
               
    <% } %>
</div>
</div>
