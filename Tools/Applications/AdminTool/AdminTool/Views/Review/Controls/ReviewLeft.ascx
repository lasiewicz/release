﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Review.ReviewLeft>" %>
<div id="management-sidebar" align="center">
    <% if (Model.SiteId != (int) SiteIDs.None)
       { %>
        <p class="site-logo">
            <img src="/Images/Logos/<%= Model.SiteId.ToString() %>.png"/>
        </p>
    <% } %>
    <a href="/" class="button" id="return-to-search">Return to Search</a>
    <div class="review_left">
        <div class="review_info">
            <%if(!string.IsNullOrEmpty(Model.ApprovalStatus)) { %>
            <strong>Status:</strong>
            <span style="color:Green;"><strong><%= Model.ApprovalStatus %></strong></span>
            <% } %>
        </div>
        <div class="review_info">
            <%if(!string.IsNullOrEmpty(Model.ApprovalProcessedByEmail)) { %>
                <strong>Processed by:</strong><br/>
                <%=Model.ApprovalProcessedByEmail%>
            <% } %>
        </div>
        <div class="review_info">
         <%if (Model.ApprovalDate > DateTime.MinValue) { %>
            <strong>Date/Time:</strong><br/>
            <%=Model.ApprovalDate.ToString("G") %>
         <% } %>
        </div>
        <%if(Model.MemberMessages != null && Model.MemberMessages.Count > 0 ){ %>
    	    <p class="approval_box">
	        <%foreach (ApprovalMemberMessage message in Model.MemberMessages) { %>
	            <%if(message.ShowInRed){ %>
	                <span><%=message.Message %></span><br />
	            <%} else {%>
	                <span style="color:Red" class="red"><%=message.Message %></span><br />
	            <%} %>
	        <%} %>
    	    </p>
    	<%} %>
        <div class="approval_queue">
    	    <strong>
                <%if (Model.IsOnDemand) { %>
                    On Demand <br/>
                  <% } %>
                    Review Queue
            </strong><br />
            <span><%=Model.QueueCount.ToString() %></span><div>Items in queue</div><br />
            <span><%=Model.ItemsProcessed.ToString() %></span><div>Processed this session</div>
        </div>
    </div>
</div>
