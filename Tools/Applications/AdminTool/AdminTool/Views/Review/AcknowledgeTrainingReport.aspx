﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.TrainingAcknowledgeModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Acknowledge Training Report
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
    <br/>
    <%if (Model.Success)
      {%>
    <p style="font-size:14px;"> Training report acknowledged! </p>
    <%} else {%>
    <p style="font-size:14px;"> Error occured while acknowledging training report. </p>
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
     <%Html.RenderPartial("../Shared/Controls/TopNav", Model.SharedTopNav); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
     <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
