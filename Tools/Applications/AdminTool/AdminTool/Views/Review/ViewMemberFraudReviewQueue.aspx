﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.MemberFraudReviewModelView>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Spark Networks Web Admin - Fraud Review Queue
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LeftContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
   
    <% using(Html.BeginForm("SubmitFraudReview","Review")) { %>
        <div style="float:right; width:97%;">
            <br/> 
            <h2>Possible Fraud Review</h2>
            <br />
           <%=Html.DropDownListFor(m => m.SiteId, Model.Sites)%>
            <input id="GetQueueItems" name="FraudReview" type="submit" value="View" />
            <br /><br />
            <%if (Model.MemberItems.Count > 0)
              { %>
                <table id="search-results-table" style="float:left;width:50%;margin-right:50%;">
                    <tr>
                        <th align="left">Member ID</th>
                        <th align="left">Site</th>
                        <th align="left">Admin Email</th>
                        <th align="left">Insert Date</th>
                        <th></th>
                    </tr>
                    <% for (int i = 0; i < Model.MemberItems.Count; i++)
                       {
                           var queueItem = Model.MemberItems[i];%>
                        <tr>
                            <td>
                                <a target="_blank" href="/Member/View/<%=queueItem.SiteId.ToString()%>/<%=queueItem.MemberId.ToString() %>"><%= Html.DisplayFor(m => m.MemberItems[i].MemberId)%></a>
                                <%= Html.HiddenFor(m => m.MemberItems[i].MemberId)%>
                                <%= Html.HiddenFor(m => m.MemberItems[i].SiteId)%>
                                <%= Html.HiddenFor(m => m.MemberItems[i].AdminMemberId)%>
                            </td>
                            <td><%= queueItem.Site%></td>
                            <td><%= Html.DisplayFor(m => m.MemberItems[i].AdminEmail)%></td>
                            <td><%= queueItem.InsertDate.ToShortDateString()%></td>
                            <td><%=Html.RadioButtonFor(m => m.MemberItems[i].IsFraud, "true")%> Fraud
                                <%=Html.RadioButtonFor(m => m.MemberItems[i].IsFraud, "false")%>Not Fraud</td> 
                        </tr>
                    <% } %>
                </table>
                 
                 <input id="Save" name="FraudReview" type="submit" value="Submit" style="float:right;margin-right:50%;margin-top:20px;" />
            <% }
              else if(Model.SiteId.HasValue)
            { %>
                <span class="no_more_item" style="margin-left:0px; ">Queue is empty.</span>
            <% } %>
        </div>
    <% } %>
</asp:Content>
