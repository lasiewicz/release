﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master"Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.AdminSampleRatesModelView>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Review" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	FTA Review Sample Rates
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%Html.BeginForm("AdminSampleRates", "Review"); %>
    <br/>
    <h2>FTA Review Sample Rates</h2>
    <br/>
    <div class="samplerate" id="samplerateresults">
    <% if(Model.AdminSampleRates.Count > 0) { %>
        <ul>
        <% for (int i = 0; i < Model.AdminSampleRates.Count; i++ )
        { %>
            <li>
                <div>
                    <div style="width: 100px; float: left"><%=Model.AdminSampleRates[i].AdminMemberId%></div>
                    <%= Html.HiddenFor(model => Model.AdminSampleRates[i].AdminMemberId)%>
                    <div style="width: 300px; float: left"><%=Model.AdminSampleRates[i].AdminEmail%></div>
                    <div style="float: left"><%= Html.TextBoxFor(model => model.AdminSampleRates[i].DailySampleRate)%> %</div>
                    <%= Html.HiddenFor(model => Model.AdminSampleRates[i].OldDailySampleRate)%>
                </div>
            </li>
        <% } %>
        </ul>
    <%}%>
    <br/>
    <input type="submit" value="Update Rates for Next Sample"/>
   </div>
<%Html.EndForm(); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
