﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.SodbSearchModelView>" %>
<%@ Import Namespace="Spark.SODBQueue.ValueObjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	SODB Search 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<style>
       .sodb-results { font-size:12px; }
       .sodb-results tr th, .sodb-results tr td { padding:0px 5px 5px 0px; }
       input[type=text] { width:174px; }
</style>
<%Html.BeginForm("SODBSearchMember", "Review"); %>
<div>
    <br/>
    <h2>SODB Search</h2>
    <br/>
    <table class="sodb-results">
        <tr>
            <td>Member ID:</td>
            <td>
                <%= Html.TextBoxFor(model => model.MemberId)%>
                <%= Html.ValidationMessageFor(model => model.MemberId)%>
            </td>
        </tr>
         <tr>
            <td>SSPBlue ID:</td>
            <td>
                <%= Html.TextBoxFor(model => model.SSPBlueId)%>
            </td>
        </tr>
        <tr>
            <td>Site:</td>
            <td>
                <%=Html.DropDownListFor(model => model.SiteId, Model.SodbSites)%>
	        </td>
        </tr>
    </table>
    <br/>
    <input type="submit" id="btnGetSodbMember" value="Go"/><br /><br />
    <% if(Model.SodbMembers.Count > 0)
   { %>
     <table class="sodb-results">
        <tr>
            <th>SSP Blue ID</th>
            <th>Customer ID</th>
            <th>Calling System ID</th>
            <th>Level</th>
        </tr>
        <% foreach(SodbItem record in Model.SodbMembers) { %>
            <tr>
                <td><a href="/review/GetSodbMember?memberId=<%=record.MemberId %>&sspblueId=<%=record.SSPBlueId %>&siteId=<%=record.SiteId %>"><%= record.SSPBlueId%></a></td>
                <td><%= record.MemberId%></td>
                <td><%= record.SiteId%></td>
                <td><%= record.Level%></td>
            </tr>
        <% } %>
     </table>  
  <% }%>
</div>
<%Html.EndForm(); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
 <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
 <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
