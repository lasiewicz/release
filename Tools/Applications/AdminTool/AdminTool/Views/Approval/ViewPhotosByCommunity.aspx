﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<PhotoApprovalModelView>" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<link href="../../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
<link href="../../../Content/confirm.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="ContentLeft" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/ApprovalLeft", Model.Left); %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script src="../../Scripts/approval.js" language="javascript" type="text/javascript"></script>
<script src="../../Scripts/jquery.simplemodal.js" language="javascript" type="text/javascript"></script>

<%if (Model.ApprovalItems.Count > 0) { %>
    <div style="display: inline">
    <table>
        <tr>
            <%foreach (PhotoApprovalData photoApprovalData in Model.ApprovalItems)
              { %>
                <td>
                    <%Html.RenderPartial("Controls/FlashApproval", photoApprovalData); %><br />
                    <%Html.RenderPartial("Controls/AdditionalApprovalInfo", photoApprovalData); %>
                </td>    
            <%} %>
       </tr>
    </table>
    
    <%=Html.Hidden("PhotoCount", Model.ApprovalItems.Count.ToString()) %>
    <input type="button" onclick="submitPhotoApprovals();" value="Next" style="width: 130px; margin-left: <%=330 + (((Model.ApprovalItems.Count > 3)? 2: Model.ApprovalItems.Count -1) * 340) %>px;"/>
    </div>
<%} else { %>
    <p class="no_more_item">No more items to approve. </p>
<%} %> 

<%--<script type="text/javascript">
    $(document).ready(function () {
        $('div.divFraudScore').each(function () {
            var theColor = $(this).find('span.hexColor').html();
            $(this).css({ background: theColor });
        });

        resetApprovalForm();
    });
</script>--%>

<div id='confirm'>
    <div class='header'></div>
    <div class='message'></div>
    <div class='buttons'>
	    <div class='no simplemodal-close'>No</div><div class='yes'>Yes</div>
    </div>
</div>
</asp:Content>




