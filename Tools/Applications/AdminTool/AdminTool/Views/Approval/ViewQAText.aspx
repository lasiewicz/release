﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Approval.TextApprovalQAModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewQATextByCommunity
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="/Scripts/tiny_mce/tiny_mce.js"></script>
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        theme_advanced_buttons1: "undo,redo",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "bottom",
        theme_advanced_toolbar_align: "left"
    });

    function undoTextEdits() {
        var index = 1;
        var originalValue = $('#OriginalValue' + index).val();

        while (originalValue) {
            tinyMCE.getInstanceById('Answer' + index).execCommand('mceSetContent', false, originalValue);
            index++;
            originalValue = $('#OriginalValue' + index).val();
        }
    }

    function toggleTextSuspendPanel() {
        if ($('#SuspendReasons').is(':visible')) {
            $('#SuspendReasons').hide();
        }
        else {
            $('#SuspendReasons').show();
        }
    }

    function setSuspendTrue() {
        $('#Suspend').val('true');
    }
</script>

<%Html.BeginForm("CompleteQATextApproval", "Approval"); %>
<% if (Model.ApprovalQuestions != null && Model.ApprovalQuestions.Count > 0) { %>
    <%=Html.Hidden("MemberID", Model.MemberID.ToString())%>
    <%=Html.Hidden("MemberTextApprovalID", Model.MemberTextApprovalID.ToString())%>
    <%=Html.Hidden("LanguageID", Model.LanguageID.ToString())%>
    <%=Html.Hidden("CommunityID", Model.CommunityID.ToString())%>
    <%=Html.Hidden("SiteID", Model.SiteID.ToString())%>
    <%=Html.Hidden("Suspend", "false")%>
    <%=Html.Hidden("ApprovalType", Model.ApprovalType.ToString("d"))%>
    
    <table class="info_table">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Delete?</td>
        </tr>
        
        <%foreach (ApprovalQuestion question in Model.ApprovalQuestions) { %>
            <tr>
                <td><img src="/Images/<%=question.StatusIconSource %>" alt="" /></td>
                <td class="attributename"><%=question.Question%>  </td>
                <td>
                    <%=Html.TextArea("Answer" + question.Number.ToString(), question.Answer, new { style = "width: 700px;height:" + question.TextboxHeight.ToString() + "px" })%>
                    <%=Html.Hidden("AnswerID" + question.Number.ToString(), question.AnswerID.ToString())%><%=Html.Hidden("OriginalValue" + question.Number.ToString(), question.Answer)%>
                </td>
                <td>
                    <%=Html.CheckBox("Delete" + question.Number.ToString())%>
                </td>
            </tr>
        <%} %>
        
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <input type="button" value="Undo Edits" onclick="undoTextEdits();" />&nbsp;&nbsp;<input type="submit" value="Approve" />&nbsp;&nbsp;
                <input type="button" value="Suspend/Terminate" onclick="toggleTextSuspendPanel();" />
                <div id="SuspendReasons" visible="false" style="display: none; margin-left: 300px; border: 1px solid #666666; padding: 10px; background-color: #dddddd; width: 200px;">
                <%=Html.DropDownList("AdminActionReasonID", Model.AdminSuspendReasons ) %><br />
                <input type="submit" value="Confirm Suspend" onclick="setSuspendTrue();" />
                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
        
    </table>
    
<% } else { %>
    <p class="no_more_item">No more items to approve</p>
<% } %>

<%Html.EndForm(); %>

</asp:Content>


<asp:Content ID="Content5" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/ApprovalLeft", Model.Left); %>
</asp:Content>
