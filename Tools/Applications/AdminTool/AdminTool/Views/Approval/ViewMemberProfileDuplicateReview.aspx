﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Approval.MemberProfileDuplicateReviewModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewMemberProfileDuplicateByLanguage
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<style>

div.hr {
        background-color:#444;
        border-width:0;
        color:#ff000;
        height:2px;
        line-height:0;
        margin:-.5em 10px 1.8571em 10px;
        page-break-after:always;
        text-align:center;
        
}
 
</style>

<%Html.BeginForm("CompleteMemberProfileDuplicateReview", "Approval"); %>
    <%=Html.Hidden("LanguageID", Model.LanguageID.ToString()) %>
    <%=Html.Hidden("MemberProfileDuplicateHistoryID", Model.QueueItem == null ? Matchnet.Constants.NULL_INT : Model.QueueItem.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryId)%>
    <h2></h2>
    <%if (Model.QueueItem == null)
         { %>
             <table class="info_table" width="600" >
             <tr>
                <td>
                    <%if (string.IsNullOrEmpty(Model.ErrorMessage))
                      {%>
                        No more items in the queue.
                    <% }
                      else
                      {%>
                        <%=Model.ErrorMessage %>  
                    <% } %>
                </td>
            </tr>
            </table>
    <% } else
         {%>
         <table class="info_table" width="600" >
             <tr>
                <td colspan="2">
                    <span style=" font-size:large; color:Black;"> Site: <%= Model.SiteName %></span>
                </td> 
             </tr>
        </table>
        <br/>
        <% if (!string.IsNullOrEmpty(Model.ErrorMessage))
           {%>
            <%=Model.ErrorMessage %><br/>
           <%}%>
        <div>
        <div style="text-align:right;padding-bottom:20px;">
            <input type="submit" value="Process Page and Proceed to Next" onclick="return ValidateCrxDuplicateReview();"/>
        </div>
        <div style="float:left;width:975px;">&nbsp;</div>
        <div style="float:left;text-align:right;padding-left:20px;padding-right:20px;border:2px solid;background-color:gray;">
            <input type="checkbox" name="EnableEditing" id="EnableEditing" />Correct Agent Action
        </div>
         <table class="info_table" width="1000"    > 
        <tr>
            <td colspan="3">
                <b>STR Queue ID:&nbsp;<%=Model.QueueItem.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryId%></b>
            </td>
        </tr>

        <% 
          int i = 0;
          foreach (MemberProfileDuplicateHistoryEntryExtended dup in Model.MemberProfileDuplicateHistoryEntryExtended)
           {
               %>
               <tr align="left">
                <td   class="attributename2">
                        <div style="float:left;width:100px;"></div>
                        <div style="float:left;padding-bottom:5px; font-size:12px;">Mbr:&nbsp;<%= dup.MemberId %></div>
                        <div style="float:left;padding-bottom:5px;"><%= dup.FieldValueOriginationDate.ToString("g")%></div><br />
                        <div style="float:left;padding-bottom:8px;"><a href="/Member/View/<%=dup.LastLogonSiteID%>/<%=dup.MemberId %>" target="blank">View Account</a></div>
                        <div style="float:left;padding-bottom:5px;"><a href="/Approval/ViewTextByMember/<%=dup.MemberId%>/<%=dup.LastLogonSiteID%>" target="_blank">View All Essays</a></div>
                        <div style="float:left;padding-bottom:5px;"><a href="http://<%= Model.g.DefaultHost ?? "www" %>.jdate.com/Applications/MemberProfile/ViewProfile.aspx?MemberID=<%=dup.MemberId%>&impmid=<%=dup.MemberId%>&imppid=<%=dup.LastBrandID%>&EntryPoint=8&LayoutTemplateID=<%=dup.TemplateID%>&admin=true" target="_blank">View Profile</a></div>
                </td>
                <td>
		            <div>
                        <%=Html.TextArea("CRXDuplicate" + dup.CRXResponseId, dup.TextContent, new { style = "width: 700px;height:" + dup.TextboxHeight.ToString() + "px", @class = " " })%>
                    </div>
                </td>
                <td valign="top">
                    <table  width="100px">
                        <tr>
                            <td>
                              <label><%=Html.CheckBox("Matching" + dup.CRXResponseId, dup.IsMatch, new { @class = "matching match_" + dup.CRXResponseId, data = dup.CRXResponseId, data2 = dup.MemberId })%> Match</label>
                            </td>
                            <td align="left">
                                <label><%=Html.CheckBox("Suspend" + dup.MemberId, dup.CurrentlySuspended, new { @class = "suspended suspend_" + dup.CRXResponseId, disabled = "", data = dup.CRXResponseId, data2 = dup.MemberId })%>Suspend</label>
                                <%=Html.Hidden("AlreadySuspended" + dup.MemberId, dup.CurrentlySuspended, new { @class = "alreadySuspended", data2 = dup.MemberId, data = dup.CRXResponseId })%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span style="color:#660000;font-weight:700;">
                                <% if (dup.Suspended)
                                   {%>
                                    Suspended on 1st review
                                <% }
                                   else if (dup.AlreadySuspended)
                                   {%>
                                    Suspended prior to 1st review
                                <% }
                                   else if (dup.CurrentlySuspended)
                                   {%>
                                    Suspended since 1st review
                                <% } %>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div  class="hideMe">
                                    <span visible="false" style="display: none; color:Red"></span>
                                    <span id="<%=dup.MemberId%>"  style="display: none; color:Red">Please specify a suspend reason</span>
                                        <%=Html.DropDownList("SuspendReason" + dup.MemberId, dup.AdminSuspendReasons, new {@class = "suspendReasonSelection"})%>
                                        <%=Html.Hidden("SelectedSuspendReason" + dup.MemberId, dup.SuspendReasonValue, new { @class="selectedSuspendReason", data2 = dup.MemberId}) %>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% 
                if (i == 0)
                {
                    %>
                        <tr>
                            <td colspan="3">
                                <div class="hr"><hr /></div>
                            </td>
                        </tr>
                    <%
                }
            %>
    <% 
        i++;
           
           } %>
    </table>
        <div  style="text-align:right;padding-top:20px;">
            <input type="submit" value="Process Page and Proceed to Next" onclick="return ValidateCrxDuplicateReview();"/>
        </div>

        </div>

    <% } %>
<%Html.EndForm(); %>

<script language="javascript" type="text/javascript">
    document.forms[0].reset();

    $(document).ready(function () {


        //replace option with "UNKNOWN" if no alternative has been selected

        if ($('select.suspendReasonSelection option:selected').val() <= 0) {
            $('option[value="9999"]').attr('selected', true);
        }

        // bind the selected suspend reason if there is one
        BindSelectedSuspendReason();

        // disable most of the UI starting out
        LockUI();

        $('#EnableEditing').click(function () {
            if ($(this).is(':checked')) {
                $('.matching').each(function () {
                    $(this).removeAttr('disabled');
                    ReactToMatchingChange($(this).attr("data"), $(this).attr("data2"));
                });
            } else {
                this.form.reset();
                BindSelectedSuspendReason();
                if ($('select.suspendReasonSelection option:selected').val() <= 0) {
                    $('option[value="9999"]').attr('selected', true);
                }
                LockUI();
                
            }
        });

        $('.matching').click(function () {
            var crxResponseID = $(this).attr("data");
            var memberID = $(this).attr("data2");

            ReactToMatchingChange(crxResponseID, memberID);
        });

        $('.suspended').click(function () {
            var crxResponseID = $(this).attr("data");
            var memberID = $(this).attr("data2");

            ReactToSuspendChange(crxResponseID, memberID);
        });

    });
    
    function BindSelectedSuspendReason() {
        $('.selectedSuspendReason').each(function () {
            if ($(this).val() > 0) {
                var memberID = $(this).attr('data2');
                $('#SuspendReason' + memberID).val($(this).val());
            }
        });   
    }

    function LockUI() {
        $('.matching').attr('disabled', 'disabled');
        $('.suspended').attr('disabled', 'disabled');
        $('.suspendReasonSelection').attr('disabled', 'disabled');
    }

    function ReactToMatchingChange(crxResponseID, memberID) {
        var matching = $('#Matching' + crxResponseID);
        var theSuspend = $("#Suspend" + memberID);
        var alreadySuspended = $("#AlreadySuspended" + memberID);
        
        if(matching.is(':checked')) {
            if (alreadySuspended.val().toLowerCase() == "false") {
                theSuspend.removeAttr('disabled');
            }
        }
        else {
            // without selecting the Matching checkbox suspend option is not allowed
            if (alreadySuspended.val().toLowerCase() == "false") {
                theSuspend.removeAttr("checked");
                theSuspend.attr("disabled", "disabled");
            }
        }

        ReactToSuspendChange(crxResponseID, memberID);
    }
    
    function ReactToSuspendChange(crxResponseID, memberID) {
        var theSuspend = $("#Suspend" + memberID);
        var theSuspendReason = $("#SuspendReason" + memberID);
        var alreadySuspended = $("#AlreadySuspended" + memberID);

        if (theSuspend.is(':checked')) {
            if (alreadySuspended.val().toLowerCase() == "false") {
                theSuspendReason.removeAttr("disabled");
            }
        }
        else {
            if (alreadySuspended.val().toLowerCase() == "false") {
                theSuspendReason.val('0');
                theSuspendReason.attr("disabled", "disabled");
            }
        }
    }

    function ValidateCrxDuplicateReview() {
        var validated = true;

        $('.suspended').each(function () {
            if ($(this).is(':checked')) {
                var theId = $(this).attr("data2");
                if ($("#SuspendReason" + theId).val() == '0') {
                    validated = false;
                    $("#" + theId).show();
                }
            }
        });

        // UI control being disabled, the server takes on the default value rather than the selected value
        // so let's enable the UI elements here before the submit happens.
        if (validated) {
            $('.matching').each(function () {
                $(this).removeAttr('disabled');
            });
            $('.suspended').each(function () {
                $(this).removeAttr('disabled');
            });
            $('.alreadySuspended').each(function () {
                $(this).removeAttr('disabled');
            });
            $('.suspendReasonSelection').each(function () {
                $(this).removeAttr('disabled');
            });
        }

        return validated;
    }

</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/ApprovalLeft", Model.Left); %>
</asp:Content>
