﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" ValidateRequest="false" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Approval.TextApprovalModelView>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="/Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="/Scripts/approval.js"></script>
    
    <script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js" type="text/javascript" ></script>
    <script src="/Scripts/MicrosoftMvcJQueryValidation.js" type="text/javascript" ></script>
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    <script src="/Scripts/jquery.multiselect.js" type="text/javascript" ></script>
    
    <link href="/Content/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	
	<link href="/Content/prettify.css" rel="stylesheet" type="text/css" />
	<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewTextByLanguageAndType
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<style > 
		span{
				font-size:12px;
			
			}
		
        ol, ul
        {
            list-style: none;
        }
        
        .gOut
        {
            /*
            background-color: white;
            filter:alpha(opacity=50); 
            opacity: 0.5; 
            -moz-opacity:0.50; 
            z-index: 9999999999;
            background-repeat:no-repeat;
            background-position:center;
            disabled:disabled;
            */
        }
        		
	</style>

<script type="text/javascript">

    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        theme_advanced_buttons1: "undo,redo",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "bottom",
        theme_advanced_toolbar_align: "left"
    });



    function undoTextEdits() {
        var index = 1;
        var originalValue = $('#OriginalValue' + index).val();
        
        while(originalValue)
        {
            tinyMCE.getInstanceById('Attribute' + index).execCommand('mceSetContent', false, originalValue);
           
            index++;
            originalValue = $('#OriginalValue' + index).val();
        }
    }

    function toggleTextSuspendPanel() {
        if ($('#SuspendReasons').is(':visible')) {
            $('#SuspendReasons').hide();
        }
        else {
            $('#SuspendReasons').show();
        }
    }

    function setSuspendTrue() {
        $('#Suspend').val('true');
    }

    //globals
    var DIV_PREFIX = "#divTextUnacceptableReasons";
    var DD_PREFIX = "#multiselect_TextUnacceptableReasons";
    var IsEnglish = false;
    // end of globals
    
    


    $(document).ready(function () {

        $("#ftaMainDiv").addClass("gOut");
        setTimeout('greyouttextareas()', 3000);


        $('select.reasonsStyle').multiselect();

        $(':radio').click(function () {


            var radioId = $(this).attr("id");
            var ddId = radioId.substring(14);

            if ($(this).val() == 'false') {

                $(DIV_PREFIX + ddId).show();

                enableTextArea(ddId);

            }
            else {
                $(DIV_PREFIX + ddId).hide();

                disAbleTextArea(ddId);

            }

            

        });


        $('div.hideMe').hide();

        $('div.approval_member_info').hide();

        //$('div.divFraudScore').each(function () {
        //    var theColor = $(this).find('span.hexColor').html();
        //    $(this).css({ background: theColor });
        //});


        //var theColor = $('#hexColor').html();
        //$('#divFraudScore').css({ background: theColor });
        //$('#divFraudScore').css({ width: '40%' });


    });


    function enableTextArea(id) {

        $('#div_'+id).removeClass('gOut');

        tinymce.getInstanceById('Attribute' + id).getBody().setAttribute('contenteditable', true);

    }


    function disAbleTextArea(id) {

        $('#div_' + id).addClass('gOut');

        tinymce.getInstanceById('Attribute' + id).getBody().setAttribute('contenteditable', false);

    }



    function newMemberWindow(siteID,memberID) {
        window.open('../../Member/View/' +siteID + '/' + memberID);
    }

    function greyouttextareas() {

        
        for (edId in tinyMCE.editors) {

            $(tinyMCE.get(edId).getBody().setAttribute('contenteditable', false));

        }

        tinyMCE.get('AdminNotes').getBody().setAttribute('contenteditable', true);

        $(':radio').each(function (index) {

            if ($(this).is(':checked')) {

                //call the undo function
                theId = $(this).attr("id").substring(14);
                enableTextArea(theId);

            }

        });

        
    }
    
</script>
<%Html.BeginForm("CompleteTextApproval", "Approval"); %>
<% if (Model.AttributesToApprove != null && Model.AttributesToApprove.Count > 0) { %>
    <%=Html.Hidden("MemberID", Model.MemberID.ToString())%>
    <%=Html.Hidden("LanguageID", Model.LanguageID.ToString())%>
    <%=Html.Hidden("CommunityID", Model.CommunityID.ToString())%>
    <%=Html.Hidden("TextType", Model.TextType.ToString("d"))%>
    <%=Html.Hidden("MemberTextApprovalID", Model.MemberTextApprovalID.ToString())%>
    <%=Html.Hidden("Suspend", "false")%>
    <%=Html.Hidden("ApprovalType", RegularApprovalType.QueuedApproval.ToString("d"))%>
    <%=Html.Hidden("IP", Model.ExpandedUsrInfo.IPAddress) %>
    <%=Html.Hidden("ProfileRegionID", Model.ExpandedUsrInfo.ProfileRegionID) %>
    <%=Html.Hidden("GenderMask", Model.ExpandedUsrInfo.GenderMask) %>
    <%=Html.Hidden("MaritalStatus", Model.ExpandedUsrInfo.MaritalStatusIntValue) %>
    <%=Html.Hidden("FirstName", Model.ExpandedUsrInfo.FirstName) %>
    <%=Html.Hidden("LastName", Model.ExpandedUsrInfo.LastName) %>
    <%=Html.Hidden("Email", Model.ExpandedUsrInfo.Email) %>
    <%=Html.Hidden("OccupationDescription", Model.ExpandedUsrInfo.Occupation) %>
    <%=Html.Hidden("EducationLevel", Model.ExpandedUsrInfo.EducationIntValue) %>
    <%=Html.Hidden("Religion", Model.ExpandedUsrInfo.ReligionIntValue) %>
    <%=Html.Hidden("Ethnicity", Model.ExpandedUsrInfo.EthnicityIntValue) %>
    <%=Html.Hidden("RegistrationDate", Model.ExpandedUsrInfo.RegistrationDate.ToString())%>
    <%=Html.Hidden("SubscriberStatus", Model.ExpandedUsrInfo.SubscriberStatus)%>
    <%=Html.Hidden("DaysSinceFirstSubscription", Model.ExpandedUsrInfo.DaysSinceFirstSubscription.ToString())%>
    <%=Html.Hidden("BillingPhoneNumber", Model.ExpandedUsrInfo.BillingPhoneNumber)%>

    <%Html.RenderPartial("../Member/Controls/ExpandedUserInfo", Model.ExpandedUsrInfo); %>  
    
    <table class="info_table" width="600" >
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <%foreach (TextApprovalAttribute attribute in Model.AttributesToApprove) { %>
            <tr align="left">
                <td ><img src="/Images/<%=attribute.StatusIconSource %>" alt="" /></td>
                <td  class="attributename"><div style="max-width:100px;"><%=attribute.AttributeName%></div></td>
                <td class="<%=(Model.IsNewAgent && attribute.CRXApproval.HasValue && !attribute.CRXApproval.Value ? "highlightText" : "") %> ">
                    <div id="div_<%=(attribute.Number.ToString()) %>" class="gOut">
                        <%=Html.TextArea("Attribute" + attribute.Number.ToString(), attribute.DisplayValue, new { style = "width: 700px;height:" + attribute.TextboxHeight.ToString() + "px", @class = " " })%>
                        <%=Html.Hidden("AttributeName" + attribute.Number.ToString(), attribute.AttributeName)%>
                        <%=Html.Hidden("AttributeGroupID" + attribute.Number.ToString(), attribute.AttributeGroupID.ToString())%>
                        <%=Html.Hidden("LanguageID" + attribute.Number.ToString(), attribute.LanguageID.ToString())%>
                        <%=Html.Hidden("OriginalValue" + attribute.Number.ToString(), attribute.DisplayValue)%>
                        <%=Html.Hidden("MaxLength" + attribute.Number.ToString(), attribute.MaxLength.ToString())%>
                        <div id="ValidationMessage<%=attribute.Number.ToString() %>" visible="false" style="display: none; color:Red"><br />This text can only be <%=attribute.MaxLength.ToString()%> characters long - it has <span id="CharactersTooLong<%=attribute.Number.ToString() %>"></span> too many characters.</div>
                        <% if (attribute.ShowNoCRXResponseMessage)
                           {%>
                        <div style="padding:2px 0 0 0;color:grey;">CRX Response not available.</div>
                        <% } %>
                    </div>
                </td>
                <td valign="top">
                    <table  width="100px">
                        <tr>
                            <td>
                                <label>
                                    <%=Html.RadioButton("TextAcceptable" +  attribute.Number.ToString(), "true", !Model.IsEnglish)%> Good
                                </label>
                            </td>
                            <td align="left">
                                <label>
                                    <%=Html.RadioButton("TextAcceptable" + attribute.Number.ToString(), "false", false)%> Bad
                                </label>
                                <%= Html.ValidationMessage("TextApprovalStatus" + attribute.Number.ToString(), "*")%>
                                <span id="RadioValidation<%=attribute.Number.ToString() %>"  visible="false" style="display: none; color:Red">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="divTextUnacceptableReasons<%=attribute.Number.ToString()%>" class="hideMe">
                                <span id="TextUnAcceptableValidation<%=attribute.Number.ToString() %>"  visible="false" style="display: none; color:Red">Please select at least one option from the list</span>
                                 <%=Html.ListBox("TextUnacceptableReasons" + attribute.Number.ToString(), Model.IndividualTextApprovalStatuses, new { @class = "reasonsStyle" })%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <%} %>
        
        <% if (Model.IsEnglish)
           {%>
        <script type="text/javascript">
                IsEnglish = true;
         </script>   
        <tr>
            <td colspan="2">Warnings:</td>
            <td colspan="2">

                <div id="divWarning">
                <span id="warningValidation" style="display: none; color:Red">Please select a warning</span>    <br />
                    <%= Html.CheckBox("WarnContactInfo", Model.WarningReasons.Contains(WarningReason.ContactInfo))%>Contact Information<br/>
                    <%= Html.CheckBox("WarnInappropriateContent",
                                                    Model.WarningReasons.Contains(WarningReason.InappropriateContent))%>Inappropriate Content<br/>
                    <%= Html.CheckBox("WarnGeneralViolation",
                                                    Model.WarningReasons.Contains(WarningReason.GeneralViolation))%>General Violation of Terms of Use<br />
                    <%= Html.CheckBox("WarnNoWarning", Model.WarningReasons.Contains(WarningReason.NoWarning))%>Send No Warning<br/>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" valign="top" style="text-align: left;"><%=Html.CheckBox("PossibleFraud")%><span style="color:Red; font-size:16px;">Possible Fraud</span></td>
        </tr>
        <% }
           else
           {%>
        <tr>
            <td colspan="4" valign="top" style="text-align: right;"><%=Html.CheckBox("PossibleFraud")%> <span style="color:Red; font-size:16px;"> Possible Fraud</span></td>
        </tr>
        <%} %>
        
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <input type="button" value="Undo Edits" onclick="undoTextEdits();" />&nbsp;&nbsp;
                <input type="submit" value="Approve" onclick="return ValidateFTA();" />&nbsp;&nbsp;
                <input type="button" value="Suspend/Terminate" onclick="toggleTextSuspendPanel();" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td valign="top" align="left">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td nowrap width="10%"><span class="smallText">(Notes added on Approve <br /> Or Suspend)</span></td>
                        <td valign="top" width="35%">
                            <%=Html.TextArea("AdminNotes", "", new { style = "width: 250px;height: 100px" })%>
                        </td>
                        <td valign="top" align="left" width="55%"> 
                            <div id="SuspendReasons" visible="false" style="display: none; border: 1px solid #666666; padding: 10px; background-color: #dddddd; width: 200px;">
                                <%=Html.DropDownList("AdminActionReasonID", Model.AdminSuspendReasons ) %>
                                <span id="AdminActionReasonIDValidation"  visible="false" style="display: none; color:Red">*</span>
                                <br/>
                                <span id="AtLeastOneBadFTASelected"  visible="false" style="display: none; color:Red">At least one field must be marked as bad<br/></span>
                                <div>
                                    <input type="submit" value="Confirm Suspend" onclick="return ValidateFTAForSuspend();" />
                                </div>
                            </div>                
                        </td>
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
     
     
    
<% } else { %>
    <p class="no_more_item">No more items to approve</p>
<% } %>

<%Html.EndForm(); %>



</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/ApprovalLeft", Model.Left); %>
</asp:Content>
