﻿<%@ Control Language="C#"  Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Approval.PhotoApprovalData>" %>
<%@ Import Namespace="Matchnet.Configuration.ServiceAdapters" %>

<div class="theinfo" style="" id="photoDiv_<%=Model.MemberId.ToString() %>">

    <div>
        <h1>Member Identification</h1>
        <table>
            <tr>
                <td>UserName:</td>  <td><a id="view-member-<%= Model.MemberId %>" href="/Member/View/<%=((int)Model.SiteId).ToString()%>/<%=Model.MemberId.ToString() %>" target="_blank"><%=Model.Username %></a></td>
                <td>Age:</td>       <td><%=Model.Age.ToString() %></td>
            </tr>
            <tr>
                <td>Gender:</td>    <td><%=Model.Gender %></td>
                <td>Site:</td>      <td><%=Model.SiteName.Substring(0, Model.SiteName.LastIndexOf('.')) %></td>
            </tr>
            <tr>
                <td>Ethnicity:</td> <td><%=Model.Ethnicity %></td>
                <td>Hair:</td>      <td><%=Model.HairColor %></td>
            </tr>
            <tr>
                <td>BodyStyle:</td>      <td><%=Model.BodyStyle %></td>
                <td>Height:</td>      <td><%=Model.Height %></td>
            </tr>
            <tr>
                <td>Billing phone #:</td>      <td><%=Model.BillingPhoneNumber %></td>
            </tr>
        </table>
    </div>

    <div style="margin-top: 5px;">
        <h1>Fraud Identification</h1>
        <table>
            <tr>
                <td>Profile Location:</td>  <td><%=Model.Location %></td>
            </tr>
            <tr>
                <td>IP Address:</td>        <td><%=Model.RegIpAddress %></td>
            </tr>
            <tr>
                <td>Sub Status:</td>        <td><%=Model.SubscriptionStatus %></td>
            </tr>
            <tr>
                <td>Passed Fraud:</td>      <td><%=Model.PassedFraud.ToYesNoString() %></td>
            </tr>
            <tr>
                <td>Fraud Score:</td>      
                <td width="50%" valign="top" style="border:solid 1px black; text-align:center;">
                        <div class="divFraudScore">
                            <%=Model.FraudResult.RiskInquiryScore %>
                            <%--<span  class="hexColor" style="display:none"><%=Model.FraudResult.ColorHex%></span>--%>
                        </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <script type="text/javascript" src="<%= RuntimeSettings.GetSetting("MINGLE_ROLF_IOVATION_INFO_URL") %>?memberid=<%= Model.MemberId %>&siteid=<%= (int)Model.SiteId %>"></script>
                </td>
            </tr>
        </table>
        <br />
        About Me: <br />
        <%=Model.AboutMe %><br /><br />
        <input type="button" onclick="delPhoto(<%=Model.Id.ToString() %>,<%=Model.MemberId.ToString() %>);" value="Delete From Queue" />
    </div>

    <input type="checkbox" onclick="changePossibleFraud(<%=Model.Id.ToString() %>);"  name="PossibleFraud<%=Model.Id.ToString() %>" id="PossibleFraud<%=Model.Id.ToString() %>" class="possible-fraud-cb" />Possible Fraud
</div>

