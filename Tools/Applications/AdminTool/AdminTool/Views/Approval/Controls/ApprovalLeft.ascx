﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Approval.ApprovalLeft>" %>
<!-- Sidebar -->
	<div id="management-sidebar" align="center">
	    <% if (Model.SiteID != SiteIDs.None)
	       { %>
	        <p class="site-logo">
	            <img src="/Images/Logos/<%= ((int) Model.SiteID).ToString() %>.png"/>
	        </p>
	    <% } %>
    	<a href="/" class="button" id="return-to-search">Return to Search</a>
    
        <%if (Model.IsTextApproval && Model.HasContent){  %>
            <div class="approval_member_info">
            <p class="profile-image">
    	        <%if (Model.PhotoURL != null && Model.PhotoURL != "" ) { %>
        	        <img src="http://<%=Model.PhotoURL %>" />
    	        <%} else { %>
    	            <!--no photo, Tom we'll need some default no photo image here-->
    	            No Photo Available
    	        <%} %>
    	        <br />
    	        
    	            <div><span>MemberID:</span><a id="view-member-<%= Model.MemberID %>" href="/Member/View/<%=((int)Model.SiteID).ToString()%>/<%=Model.MemberID.ToString() %>" target="_blank"><%=Model.MemberID%></a></div>
    	            <div><span>Community:</span><%=Model.SiteID.ToString() %></div>
    	            <div><span>Age:</span><%=Model.Age.ToString() %></div>
    	            <div><span>Gender:</span><%=Model.Gender %></div>
    	            <div><span>FraudScore:</span><%=Model.FraudResult.RiskInquiryScore %></div>
    	    </p>
    	    </div>
    	<% } %>
    	
    	<%if(Model.MemberMessages != null && Model.MemberMessages.Count > 0 ){ %>
    	    <p class="approval_box">
	        <%foreach (ApprovalMemberMessage message in Model.MemberMessages) { %>
	            <%if(message.ShowInRed){ %>
	                <span><%=message.Message %></span><br />
	            <%} else {%>
	                <span style="color:Red" class="red"><%=message.Message %></span><br />
	            <%} %>
	        <%} %>
    	    </p>
    	<%} %>
    	
    	<div class="approval_queue">
    	        <strong>Approval Queue</strong><br />
        	    <span><%=Model.QueueCount.ToString() %></span><div>Items in queue</div><br />
        	    <span><%=Model.ItemsProcessed.ToString() %></span><div>Processed this session</div>
    	</div>
    	<br /><br />
    	<%if (Model.IsTextApproval) {%>
    	<a href="/Content/XML/FreeTextApproval.xml" target="_blank">Banned/Suspect Regular Expressions</a>
    	<%} %>
    </div>	