﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Approval.PhotoApprovalData>" %>

<script type="text/javascript">
    $(document).ready(function () {
        if ($('.photoA').length > 0) {
            $('.photoA').attr('yep', 'yep');
            var theID = $('.photoA').attr('name');
            var newID = "yep_" + theID;
            $('.photoA').attr('name', '');
            $('.photoA').attr('name', newID);
         }
    });
</script>

<% if (Model.PhotoAge > -1)
   { %>
    <div class="theinfo photoage">
    <% if (Model.PhotoAge == 0) { %>
        Photo Age: <%="<"%> 1 hr
    <% } else if (Model.PhotoAge == 1) {%>
        Photo Age: <%=Model.PhotoAge%> hr
    <% } else {%>
   Photo Age: <%=Model.PhotoAge%> hrs
   <% } %>
    </div>
<% } %>

<span class="theinfo">Approve as Main Photo</span>
<input type="checkbox" name="FlashApprove<%=Model.Id.ToString()%>_IsApprovedForMain" id="FlashApprove<%=Model.Id.ToString()%>_IsApprovedForMain" class="approved-for-main-cb" <%= Model.IsApprovedForMain ? "checked" : string.Empty %> />
<a href="https://www.google.com/searchbyimage?&image_url=<%=Url.Encode(Model.PhotoUrl) %>" target="_blank">[Search Google]</a>

<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="340" height="320" id="FlashApprove<%=Model.Id.ToString()%>" align="middle" wmode="transparent">
<param name="allowScriptAccess" value="sameDomain" />
<param name="menu" value="false" />
<param name="wmode" value="transparent" />
<param name="movie" value="/Content/Flash/mnApprove.swf?photoUrl=<%=Url.Encode(Model.PhotoUrl) %>&link=<%=Url.Encode("<a href='' target=_new>" + Model.Username + "</a>") %>&age=<%=Model.Age.ToString() %>&sex=<%=Model.Gender %>&pldom=<%=Url.Encode(Model.SiteName) %>&priv=<%=Model.PrivateFlag.ToString()%>&ok=<%=Model.ApprovedFlag.ToString()%>&okc=<%=Model.CaptionApproved.ToString()%>&caption=<%=Model.Caption%>" />
<param name="quality" value="high" />
<param name="bgcolor" value="#ffffff" />


<embed src="/Content/Flash/mnApprove.swf?photoUrl=<%=Url.Encode(Model.PhotoUrl) %>&link=<%=Url.Encode("<a href='' target=_new>" + Model.Username + "</a>") %>&age=<%=Model.Age.ToString() %>&sex=<%=Model.Gender %>&pldom=<%=Url.Encode(Model.SiteName) %>&priv=<%=Model.PrivateFlag.ToString()%>&ok=<%=Model.ApprovedFlag.ToString()%>&okc=<%=Model.CaptionApproved.ToString()%>&caption=<%=Model.Caption%>" quality="high" bgcolor="#ffffff" menu="false"
width="340" height="320" name="FlashApprove<%=Model.Id.ToString()%>" swLiveConnect="true" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" class="photoA"/>
</object>

<script type="text/javascript" language="JavaScript">
function FlashApprove<%=Model.Id.ToString()%>_DoFSCommand(command, args)
{
   
    if (command=="setVars")
    {
        var a=args.split(","); 
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_rb").value=a[0];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_tx").value=a[5];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_ty").value=a[6];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_tw").value=a[7];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_th").value=a[8];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_cx").value=a[1];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_cy").value=a[2];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_cw").value=a[3];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_ch").value=a[4];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_rot").value=a[9];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_cd").value=a[10];
        document.getElementById("FlashApprove<%=Model.Id.ToString()%>_rb_original").value=a[0];
    }
}
</script>


<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_rb" name="FlashApprove<%=Model.Id.ToString()%>_rb" value="8" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_rot" name="FlashApprove<%=Model.Id.ToString()%>_rot" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_tx" name="FlashApprove<%=Model.Id.ToString()%>_tx" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_ty" name="FlashApprove<%=Model.Id.ToString()%>_ty" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_tw" name="FlashApprove<%=Model.Id.ToString()%>_tw" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_th" name="FlashApprove<%=Model.Id.ToString()%>_th" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_cx" name="FlashApprove<%=Model.Id.ToString()%>_cx" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_cy" name="FlashApprove<%=Model.Id.ToString()%>_cy" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_cw" name="FlashApprove<%=Model.Id.ToString()%>_cw" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_ch" name="FlashApprove<%=Model.Id.ToString()%>_ch" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_cd" name="FlashApprove<%=Model.Id.ToString()%>_cd" value="0" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_rb_original" name="FlashApprove<%=Model.Id.ToString()%>_rb_original" value="8" />
 
 
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_MemberPhotoID" name="FlashApprove<%=Model.Id.ToString()%>_MemberPhotoID" value="<%=Model.MemberPhotoId.ToString() %>" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_FileID" name="FlashApprove<%=Model.Id.ToString()%>_FileID" value="<%=Model.FileId.ToString() %>" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_AdminMemberID" name="FlashApprove<%=Model.Id.ToString()%>_AdminMemberID" value="<%=Model.g.AdminID %>" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_CommunityID" name="FlashApprove<%=Model.Id.ToString()%>_CommunityID" value="<%=Model.CommunityId.ToString("d") %>" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_SiteID" name="FlashApprove<%=Model.Id.ToString()%>_SiteID" value="<%=Model.SiteId.ToString("d") %>" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_MemberID" name="FlashApprove<%=Model.Id.ToString()%>_MemberID" value="<%=Model.MemberId.ToString() %>" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_Caption" name="FlashApprove<%=Model.Id.ToString()%>_Caption" value="<%=Model.Caption %>" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_PhotoPrevApproved" name="FlashApprove<%=Model.Id.ToString()%>_PhotoPrevApproved" value="<%=Model.ApprovedFlag.ToString() %>" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_PhotoPrevApproved_Original" name="FlashApprove<%=Model.Id.ToString()%>_PhotoPrevApproved_Original" value="<%=Model.ApprovedFlag.ToString() %>" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_FraudScore" name="FlashApprove<%=Model.Id.ToString()%>_FraudScore" value="" />
<input type="hidden" id="FlashApprove<%=Model.Id.ToString()%>_Deleted" name="FlashApprove<%=Model.Id.ToString()%>_Deleted" value="false" />

<script type="text/vbscript" language="VBScript">on error resume next
sub FlashApprove<%=Model.Id.ToString()%>_FSCommand(ByVal cmd, ByVal args)
call FlashApprove<%=Model.Id.ToString()%>_DoFSCommand(cmd, args)
end sub
</script>
