﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Approval.ApprovalFacebookLikeModelView>" %>
<%@ Import Namespace="Spark.FacebookLike.ValueObjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewFacebookLikesByCommunity
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="/Scripts/tiny_mce/tiny_mce.js"></script>
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Scripts/FacebookLikes.js"></script>
    <script type="text/javascript" src="/Scripts/mustache.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.blockui.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">

        function toggleTextSuspendPanel() {
            if ($('#SuspendReasons').is(':visible')) {
                $('#SuspendReasons').hide();
            }
            else {
                $('#SuspendReasons').show();
            }
        }

        function setSuspendTrue() {
            $('#Suspend').val('true');
        }

        function newMemberWindow(siteID, memberID) {
            window.open('../../Member/View/' + siteID + '/' + memberID);
        }
    </script>

    <div id="likesWidgetControlContainer">
    <%Html.BeginForm("CompleteFacebookLikeApproval", "Approval"); %>
    <% if (Model.FacebookLikes != null && Model.FacebookLikes.Count > 0) { %>
        <%=Html.Hidden("MemberID", Model.MemberID.ToString())%>
        <%=Html.Hidden("CommunityID", Model.CommunityID.ToString())%>
        <%=Html.Hidden("SiteID", Model.SiteID.ToString())%>
        <%=Html.Hidden("Suspend", "false")%>
        <%=Html.Hidden("ApprovalType", Model.ApprovalType.ToString("d"))%>
        <%=Html.Hidden("FBLikesSubmitData", "")%>
        <%=Html.Hidden("FBLikesRemoveAll", "false")%>
        <%=Html.Hidden("FBLikesDisplayedCount", Model.FacebookLikes.Count.ToString())%>

        <%Html.RenderPartial("../Member/Controls/ExpandedUserInfo", Model.ExpandedUsrInfo); %> 

        <div id="blockDiv">
            <div id="fbDataEdit" class="fb-likes-widget-wrapper clearfix">
                <h3>Member Profile Facebook Likes to Review</h3>
                <div class="inline-edit">
                    <!--Edit Likes goes here-->
                    <ul id="user-info-all-edit" class="fb-likes-widget edit clearfix"></ul>

                    <%--Submit--%>
                    <input id="btnUndoEdit" type="button" value="Select All" onclick="fbLikeManager.selectAll();" />&nbsp;&nbsp;
                    <input id="btnRejectAll" type="button" value="Unselect All" onclick="fbLikeManager.unselectAll();" />&nbsp;&nbsp;
                    <input id="btnSubmit" type="submit" value="Approve" onclick="fbLikeManager.prepareSaveLikesBySiteCommunity();" />&nbsp;&nbsp;
                    <input id="btnSuspend" type="button" value="Suspend/Terminate" onclick="toggleTextSuspendPanel();" />
                    
                    <div class="suspend-box clearfix">
                        <h3 class="smallText">(Notes added on Approve Or Suspend)</h3>
                        <div class="float-inside"><%=Html.TextArea("AdminNotes", "", new { style = "width: 250px;height: 100px" })%></div>
                        <div id="SuspendReasons" visible="false" class="float-inside dropdown-box" style="display: none;">
                            <%=Html.DropDownList("AdminActionReasonID", Model.AdminSuspendReasons ) %><br />
                            <input type="submit" value="Confirm Suspend" onclick="setSuspendTrue();fbLikeManager.prepareSaveLikesBySiteCommunity();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <% } else { %>
        <p class="no_more_item">No more items to approve</p>
    <% } %>

    <%Html.EndForm(); %>
    </div>

    <script type="text/javascript">
        //initialize objects
        var fbLikeManager = new FBLikeManager();
        fbLikeManager.fbHasSavedLikes = <%=FacebookLikesHelper.HasFacebookLikes(Model.FacebookLikes).ToString().ToLower()%>;
        fbLikeManager.memberID = <%=Model.MemberID.ToString()%>
        fbLikeManager.fbConnectStatusID = 0;
        fbLikeManager.likesWidgetControlContainerID = 'likesWidgetControlContainer';
        fbLikeManager.blockDivId = 'blockDiv';
        fbLikeManager.scrollToDiv = 'likesWidgetControlContainer';
        fbLikeManager.widgetHasDataEditContainerID = 'fbDataEdit';
        fbLikeManager.likesContainerEditID = 'user-info-all-edit';
        fbLikeManager.fbSubmitDataID = 'FBLikesSubmitData';
        fbLikeManager.fbRemoveAllDataID = 'FBLikesRemoveAll';
        
        //generated likes category groups
        <%=FacebookLikesHelper.GenerateCategoryJS()%>

        //generated saved likes items
        <%=FacebookLikesHelper.GenerateSavedLikesJS(Model.FacebookLikes)%>

        $(function () {
            //Try rendering saved likes items on client side, may move this to server side if needed
            if (fbLikeManager.fbHasSavedLikes == true){
                fbLikeManager.loadEdit();
            }
        });
    </script>

</asp:Content>


<asp:Content ID="Content5" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/ApprovalLeft", Model.Left); %>
</asp:Content>
