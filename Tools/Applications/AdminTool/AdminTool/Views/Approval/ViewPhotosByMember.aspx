﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<PhotoApprovalMemberModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<link href="../../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Member/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="ContentLeft" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Member/Controls/MemberLeft", Model.Left); %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script src="/Scripts/approval.js" language="javascript" type="text/javascript"></script>
<%if (Model.ApprovalItems.Count > 0) {
      int i = 0; %>
    <div style="display: inline">
    <table>
        <tr>
            <%foreach (PhotoApprovalData photoApprovalData in Model.ApprovalItems)
              {
                  if (i == 3 || i == 6 || i == 9)
                  {
                      %>
                      </tr>
                      <tr>
                      <%
                  }
                  i++;
                %>
                <td>
                    <%Html.RenderPartial("Controls/FlashApproval", photoApprovalData); %><br />
                    <%Html.RenderPartial("Controls/MemberAdditionalApprovalInfo", photoApprovalData); %>
                </td>    
            <%} %>
       </tr>
    </table>
    
    <%=Html.Hidden("PhotoCount", Model.ApprovalItems.Count.ToString()) %>
    <input type="button" onclick="submitPhotoApprovals();" value="Next" style="width: 130px; margin-left: <%=330 + (((Model.ApprovalItems.Count > 3)? 2: Model.ApprovalItems.Count -1) * 340) %>px;"/>
    </div>
<%} else { %>
    <p>No more items to approve. </p>
<%} %> 
<%--<script type="text/javascript">
    $(document).ready(function() {
        $('div.divFraudScore').each(function() {
            var theColor = $(this).find('span.hexColor').html();
            $(this).css({ background: theColor });
        });
    });
</script>--%>
</asp:Content>
