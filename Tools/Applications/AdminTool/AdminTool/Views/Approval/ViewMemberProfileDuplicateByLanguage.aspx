﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Approval.MemberProfileDuplicateApprovalModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewMemberProfileDuplicateByLanguage
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<style>

div.hr {
        background-color:#444;
        border-width:0;
        color:#ff0000;
        height:2px;
        line-height:0;
        margin:-.5em 10px 1.8571em 10px;
        page-break-after:always;
        text-align:center;
        
}
 
</style>

<%Html.BeginForm("CompleteMemberProfileDuplicate", "Approval"); %>
    <%=Html.Hidden("LanguageID", Model.LanguageID.ToString()) %>
    <%=Html.Hidden("MemberProfileDuplicateQueueId", Model.QueueItem == null ? -1 : Model.QueueItem.QueueItemMemberProfileDuplicateID)%>
    <h2></h2>
    <%if (Model.QueueItem == null)
         { %>
             <table class="info_table" width="600" >
             <tr>
                <td>
                    <%if (string.IsNullOrEmpty(Model.ErrorMessage))
                      {%>
                        No more items in the queue.
                    <% }
                      else
                      {%>
                        <%=Model.ErrorMessage %>  
                    <% } %>
                </td>
            </tr>
            </table>
    <% } else
         {%>
         <table class="info_table" width="600" >
             <tr>
                <td colspan="2">
                    <span style=" font-size:large; color:Black;"> Site: <%= Model.SiteName %></span>
                </td> 
             </tr>
        </table>
        <br/>
        <% if (!string.IsNullOrEmpty(Model.ErrorMessage))
           {%>
            <%=Model.ErrorMessage %><br/>
           <%}%>
        <div>
        <div style="text-align:right;padding-bottom:20px;">
            <input type="submit" value="Process Page and Proceed to Next" onclick="return ValidateCrxDublicates();"/>
        </div>
         <table class="info_table" width="1000"    > 
        <% 
          int i = 0; 
          foreach (MemberProfileDuplicateExtended dup in Model.MemberProfileDuplicateExtended)
           {
               %>
               <tr align="left">
                <td   class="attributename2">
                        <div style="float:left;width:100px;"></div>
                        <div style="float:left;padding-bottom:5px; font-size:12px;">Mbr:&nbsp;<%= dup.MemberId %></div>
                        <div style="float:left;padding-bottom:5px;"><%= dup.FieldValueOriginationDate.ToString("g")%></div><br />
                        <div style="float:left;padding-bottom:8px;"><a href="/Member/View/<%=dup.LastLogonSiteID%>/<%=dup.MemberId %>" target="blank">View Account</a></div>
                        <div style="float:left;padding-bottom:5px;"><a href="/Approval/ViewTextByMember/<%=dup.MemberId%>/<%=dup.LastLogonSiteID%>" target="_blank">View All Essays</a></div>
                        <div style="float:left;padding-bottom:5px;"><a href="http://<%= Model.g.DefaultHost ?? "www" %>.jdate.com/Applications/MemberProfile/ViewProfile.aspx?MemberID=<%=dup.MemberId%>&impmid=<%=dup.MemberId%>&imppid=<%=dup.LastBrandID%>&EntryPoint=8&LayoutTemplateID=<%=dup.TemplateID%>&admin=true" target="_blank">View Profile</a></div>
                </td>
                <td>
		            <div>
                        <%=Html.TextArea("CRXDuplicate" + dup.CRXResponseId, dup.FieldValue, new { style = "width: 700px;height:" + dup.TextboxHeight.ToString() + "px", @class = " " })%>
                    </div>
                </td>
                <td valign="top">
                    <table  width="100px">
                        <tr>
                            <td>
                              <label><%=Html.CheckBox("Matching" + dup.CRXResponseId, false, new { @class = "matching match_" + dup.CRXResponseId, data = dup.CRXResponseId, data2 = dup.MemberId })%> Match</label>
                            </td>
                            <td align="left">
                                <label><%=Html.CheckBox("Suspend" + dup.MemberId, dup.AlreadySuspended, new { @class = "suspended suspend_" + dup.CRXResponseId, disabled = "", data = dup.CRXResponseId, data2 = dup.MemberId })%>Suspend</label>
                                <%=Html.Hidden("AlreadySuspended" + dup.MemberId, dup.AlreadySuspended, new { @class = "alreadySuspended", data2 = dup.MemberId, data = dup.CRXResponseId })%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div  class="hideMe">
                                    <span visible="false" style="display: none; color:Red"></span>
                                    <span id="<%=dup.MemberId%>"  style="display: none; color:Red">Please specify a suspend reason</span>
                                        <%=Html.DropDownList("SuspendReason" + dup.MemberId, dup.AdminSuspendReasons, new { disabled = "" , @class = "SuspendReason" })%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% 
                if (i == 0)
                {
                    %>
                        <tr>
                            <td colspan="3">
                                <div class="hr"><hr /></div>
                            </td>
                        </tr>
                    <%
                }
            %>
    <% 
        i++;
           
           } %>
    </table>
        <div  style="text-align:right;padding-top:20px;">
            <input type="submit" value="Process Page and Proceed to Next" onclick="return ValidateCrxDublicates();"/>
        </div>

        </div>



    <% } %>



<script language="javascript" type="text/javascript">

    $(document).ready(function () {

        //replace option with "UNKNOWN" if no alternative has been selected

        if ($('select.SuspendReason option:selected').val() <= 0) {
            $('option[value="9999"]').attr('selected', true);
        }

        //check to see if people are already suspended 

        $('.alreadySuspended').each(function () {
            var alreadySuspended = $("#AlreadySuspended" + $(this).attr("data2"));

            if (alreadySuspended.val() == "True") {

                //disable dropdown
                $("#SuspendReason" + $(this).attr("data2")).attr("disabled", "disabled");
                //checkbox checked and disabled
                $("#Suspend" + $(this).attr("data")).attr("checked", "checked");
                $("#Suspend" + $(this).attr("data")).attr("disabled", "disabled");


            }

        });


        $('.matching').click(function () {

            var theSuspend = $(".suspend_" + $(this).attr("data"));
            var theSuspendReason = $("#SuspendReason" + $(this).attr("data2"));

            var alreadySuspended = $("#AlreadySuspended" + $(this).attr("data2"));

            if (alreadySuspended.val() != "True") {

                if ($(this).is(':checked')) {

                    theSuspend.removeAttr('disabled');
                    theSuspendReason.removeAttr('disabled');

                }
                else {

                    theSuspend.removeAttr("checked");
                    theSuspend.attr("disabled", "disabled");
                    theSuspendReason.val('0');
                    theSuspendReason.attr("disabled", "disabled");
                }
            }

        });


    });


    function ValidateCrxDublicates() {

        var validated = true;

        $('.suspended').each(function () {

            if ($(this).is(':checked')) {

                if ($("#AlreadySuspended" + $(this).attr("data2")).val() != "True") {

                    var theId = $(this).attr("data2");

                    if ($("#SuspendReason" + theId).val() == '0') {

                        validated = false;
                        $("#" + theId).show();

                    }

                }
                else {
                    return true;
                }

            }

        });

        return validated;
    }

</script>











</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/ApprovalLeft", Model.Left); %>
</asp:Content>
