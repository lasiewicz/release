﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Approval.TextApprovalMemberModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="/Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="/Scripts/approval.js"></script>
    
      <script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js" type="text/javascript" ></script>
    <script src="/Scripts/MicrosoftMvcJQueryValidation.js" type="text/javascript" ></script>
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    <script src="/Scripts/jquery.multiselect.js" type="text/javascript" ></script>
    
    <link href="/Content/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	
	<link href="/Content/prettify.css" rel="stylesheet" type="text/css" />
	<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    
    
    
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
       <%--Member Top navigation--%>
    <%Html.RenderPartial("../Member/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        theme_advanced_buttons1: "undo,redo",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "bottom",
        theme_advanced_toolbar_align: "left"
    });

    function undoTextEdits() {
        var index = 1;
        var originalValue = $('#OriginalValue' + index).val();
        
        while (originalValue!=null) {
            tinyMCE.getInstanceById('Attribute' + index).execCommand('mceSetContent', false, originalValue);
            index++;
            originalValue = $('#OriginalValue' + index).val();
        }
    }

    //globals
    var DIV_PREFIX = "#divTextUnacceptableReasons";
    var DD_PREFIX = "#multiselect_TextUnacceptableReasons";
    // end of globals

    $(document).ready(function() {

        $('select.reasonsStyle').multiselect();


        $(':radio').click(function() {

            var radioId = $(this).attr("id");
            var ddId = radioId.substring(14);
            if ($(this).val() == 'false') {

                $(DIV_PREFIX + ddId).show();

                //  alert(DD_PREFIX + ddId);
                /*
                $(DD_PREFIX + ddId).rules("add", {
                required: true,
                minlength: 2,
                messages: {
                required: "please select at lest one reason",
                minlength: jQuery.format("Please, at least {0} characters are necessary")
                }
                });
                */
            }
            else {
                $(DIV_PREFIX + ddId).hide();
                //  $(DD_PREFIX + ddId).rules("remove");

            }

        });

        $('div.hideMe').hide();


    });


    
    
    
</script>
<%Html.BeginForm("CompleteTextApproval", "Approval"); %>
<% if (Model.AttributesToApprove.Count > 0) { %>
    <%=Html.Hidden("MemberID", Model.MemberID.ToString())%>
    <%=Html.Hidden("LanguageID", Model.LanguageID.ToString())%>
    <%=Html.Hidden("CommunityID", Model.CommunityID.ToString())%>
    <%=Html.Hidden("TextType", Model.TextType.ToString("d"))%>
    <%=Html.Hidden("Suspend", "false")%>
    <%=Html.Hidden("ApprovalType", RegularApprovalType.MemberApproval.ToString("d"))%>
    <%=Html.Hidden("SiteID", Model.Left.MemberSiteInfo.SiteID.ToString("d")) %>
    <table class="info_table">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        
        <%foreach (TextApprovalAttribute attribute in Model.AttributesToApprove) { %>
            <tr>
                <td><img src="/Images/<%=attribute.StatusIconSource %>" alt="" /></td>
                <td class="attributename"><%=attribute.AttributeName%>  </td>
                <td>
                    <%=Html.TextArea("Attribute" + attribute.Number.ToString(), attribute.DisplayValue, new { style = "width: 700px;height:" + attribute.TextboxHeight.ToString() + "px" })%>
                    <%=Html.Hidden("AttributeName" + attribute.Number.ToString(), attribute.AttributeName)%>
                    <%=Html.Hidden("AttributeGroupID" + attribute.Number.ToString(), attribute.AttributeGroupID.ToString())%>
                    <%=Html.Hidden("LanguageID" + attribute.Number.ToString(), attribute.LanguageID.ToString())%>
                    <%=Html.Hidden("AttributeStatus" + attribute.Number.ToString(), attribute.Status.ToString("d"))%>
                    <%=Html.Hidden("OriginalValue" + attribute.Number.ToString(), attribute.DisplayValue)%><%=Html.Hidden("MaxLength" + attribute.Number.ToString(), attribute.MaxLength.ToString())%>
                    <div id="ValidationMessage<%=attribute.Number.ToString() %>" visible="false" style="display: none; color:Red"><br />This text can only be <%=attribute.MaxLength.ToString()%> characters long - it has <span id="CharactersTooLong<%=attribute.Number.ToString() %>"></span> too many characters.</div>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <label>
                                    <%=Html.RadioButton("TextAcceptable" + attribute.Number.ToString(), "true", true)%>Good
                                </label>
                            </td>
                            <td align="left">
                                <label>
                                    <%=Html.RadioButton("TextAcceptable" + attribute.Number.ToString(), "false", false)%>Bad
                                </label>
                                <%= Html.ValidationMessage("TextApprovalStatus" + attribute.Number.ToString(), "*")%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="divTextUnacceptableReasons<%=attribute.Number.ToString()%>" class="hideMe">
                                <span id="TextUnAcceptableValidation<%=attribute.Number.ToString() %>"  visible="false" style="display: none; color:Red">Please select at least one option from the list</span>
                                 <%=Html.ListBox("TextUnacceptableReasons" + attribute.Number.ToString(), Model.IndividualTextApprovalStatuses, new { @class = "reasonsStyle" })%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <%} %>
        
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="button" value="Undo Edits" onclick="undoTextEdits();" />&nbsp;&nbsp;<input type="submit" value="Approve" onclick="return ValidateFTA();" /></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    
<% } else { %>
    <p>No more items to approve</p>
<% } %>

<%Html.EndForm(); %>
    

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Member/Controls/MemberLeft", Model.Left); %>
</asp:Content>
