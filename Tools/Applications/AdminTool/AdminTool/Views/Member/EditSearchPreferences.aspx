﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Member.SearchPreferencesData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	SearchPreferences
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<script language="javascript" type="text/javascript">
  $(document).ready(function() {

    $(':checkbox').change(function() {
      if ($(this).attr('checked')) {
        if ($(this).val() == "-2147483647") {
          $(':checkbox:checked[name="' + $(this).attr('name') + '"]').each(function() {
            if ($(this).val() != "-2147483647")
              $(this).removeAttr('checked');
          });
        }
        else {
          $(':checkbox:checked[name="' + $(this).attr('name') + '"][value="-2147483647"]').removeAttr('checked');
        }
      }
    });

    if ($('[name="UIMessage"]').val() != "") {
      if ($('[name="UIMessage"]').val() == "success") {
        alertMessage("Update successful", 2);
      }
      else {
        alertMessage($('[name="UIMessage"]').val());
      }
      
      $('[name="UIMessage"]').val("");
    }

  });
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%Html.BeginForm("CompleteEditSearchPreferences", "Member"); %>
  <%=Html.Hidden("SiteID", ((int)Model.MemberSiteInfo.SiteID).ToString("d"))%>
  <%=Html.Hidden("MemberID", Model.MemberID.ToString())%>
  <%=Html.Hidden("UIMessage", Model.UIMessage)%>
  

 <h2>Search Preferences</h2> 
<ul>
<li class="list-item-edit">
    <label>Min Age: </label><span style="padding-left:20px;"><%=Html.TextBox("MinAge", Model.MinAge) %> </span>
</li> 
<li class="list-item-edit">  
    <label>Max Age: </label><span style="padding-left:20px;"><%=Html.TextBox("MaxAge", Model.MaxAge) %></span>
</li> 
</ul>
<%Html.RenderPartial("../Shared/Controls/GenderPicker", Model.GenderData); %><br />

<ul>
<li class="list-item-edit">
    <label>With in:</label><span style="padding-left:20px;"><%=Html.DropDownList("SearchDistance", Model.DistanceOptions) %></span> &nbsp; &nbsp; of
</li>
</ul>

<ul>
<li class="list-item-edit">
    <label></label><span style="padding-left:20px;"><%Html.RenderPartial("../Shared/Controls/SearchRegionPicker", Model.SearchRegionPicker); %></span>
</li>
</ul>



<ul>
<li class="list-item-edit">
    <label>Has Photos Only</label>
    <span style="padding-left:20px;"><%=Html.CheckBox("HasPhotos", Model.HasPhotos) %></span>
</li>
<li class="list-item-edit">
    <label>Min Height</label>
    <span style="padding-left:20px;"><%=Html.DropDownList("MinHeight", Model.MinHeightOptions) %></span>
</li>
<li class="list-item-edit">
    <label>Max Height</label>
    <span style="padding-left:20px;"><%=Html.DropDownList("MaxHeight", Model.MaxHeightOptions) %></span>
</li>    
</ul>
<ul>
    <li class="list-item-edit">
        <label>Keyword(s)</label>
    <span style="padding-left:20px;"><%=Html.TextBox("KeywordSearch", Model.KeywordSearch, new { style = "width: 500px" })%></span>
    </li>
</ul>
<br /><br />
<table class="main-table" style="border:soild 1px red;">
<% foreach (var sp in Model.BasicSearchPrefs)
 { %>  
   <tr >
      <td valign="top"><span style=" padding-bottom:100px;" class="defaultFontSize"><%=sp.Name %> :</span> </td>
      <td>
            <table class="main-table">
                <% foreach(var spo in sp.Options) { %>
                    <tr>
                        <td>
                        <% if(spo.Selected) { %>
                            <input type="checkbox" name="<%=sp.Name %>" value="<%=spo.Value %>" checked="checked" /><span class="defaultFontSize" ><%=spo.Text %></span>
                        <%} else { %>
                            <input type="checkbox" name="<%=sp.Name %>" value="<%=spo.Value %>" /><span class="defaultFontSize"> <%=spo.Text %></span> 
                        <%} %>
                        </td>
                    </tr>
                <%} %>
            </table>
            
       </td> 
   </tr>   
<%} %>
</table>

<% foreach (var sp in Model.AdditionalSearchPrefs)
{ %> 
<ul>
 <li class="list-item-edit"> 
 <label><span class="defaultFontSize"><%=sp.Name %> :</span></label>
 <span style="padding-left:20px;">
        <table>
            <% foreach(var spo in sp.Options) { %>
             <tr><td>
                <% if(spo.Selected) { %>
                    <input type="checkbox" name="<%=sp.Name %>" value="<%=spo.Value %>" checked="checked" /><span class="defaultFontSize"><%=spo.Text %></span> <br />
                <%} else { %>
                    <input type="checkbox" name="<%=sp.Name %>" value="<%=spo.Value %>" /><span class="defaultFontSize"><%=spo.Text %></span><br />
                <%} %>
            </td></tr>
            <%} %>
        </table>
  </span>
</li> 
</ul>   
<%} %>

        
 <table width="30%">
    <tr>
        <td align="center">
        <br />
            <input type="submit" value="Update" />
        </td>
    </tr>
 </table>

    
  
  
<%Html.EndForm(); %>  
</asp:Content>

