﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Member.QASupport>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Spark Networks Web Admin - QA Support
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    &nbsp;<br />
    <h2>QA Support</h2>
    <p>&nbsp;</p>
    <% using (Html.BeginForm()) { %>
    <table class="qaSupportTable">
        <tr>
            <td><b>MemberID:</b></td>
            <td colspan="2"><%=Model.MemberID.ToString() %></td>
        </tr>
        <tr>
            <td><b>Registered:</b></td>
            <td colspan="2"><%=Model.RegisteredDate.ToString() %></td>
        </tr>
        <tr>
            <td><b>SiteID:</b></td>
            <td colspan="2"><%=Model.MemberSiteInfo.SiteID.ToString() %></td>
        </tr>
        <tr>
            <td><b>TrackingRegApplication:</b></td>
            <td colspan="2"><%=Model.TrackingRegApplication %></td>
        </tr>
        <tr>
            <td><b>TrackingRegOS:</b></td>
            <td colspan="2"><%=Model.TrackingRegOS %></td>
        </tr>
        <tr>
            <td><b>TrackingRegFormFactor:</b></td>
            <td colspan="2"><pre><%=Model.TrackingRegFormFactor %></pre></td>
        </tr>
        <tr>
            <td><b>TrackingRegDevice:</b></td>
            <td colspan="2"><pre><%=Model.TrackingRegDevice%></pre></td>
        </tr>
        <tr>
            <td><b>RegistrationScenarioID:</b></td>
            <td colspan="2"><pre><%=Model.RegistrationScenarioID %></pre></td>
        </tr>
        <tr>
            <td><b>RegistrationSessionID:</b></td>
            <td colspan="2"><pre><%=Model.RegsistrationSessionID %></pre></td>
        </tr>
        <tr>
            <td><b>BBE_PROMOTION_EXPIRATION:</b></td>
            <td colspan="2"><%=Model.BBE_PROMOTION_EXPIRATION.ToString()%></td>
        </tr>
        <tr>
            <td><b>BlackBoardEatsPromo1:</b></td>
            <td colspan="2"><pre><%=Model.BlackBoardEatsPromo1 %></pre></td>
        </tr>
        <tr>
            <td><b>IOS_IAP_Receipt_Original_TransactionID:</b></td>
            <td><%=Model.IOS_IAP_Receipt_Original_TransactionID %></td>
            <%if (string.IsNullOrEmpty(Model.IOS_IAP_Receipt_Original_TransactionID)){ %>
            <td><button type="submit" name="button" value="addiaptrandid">Add IAP TranID</button></td>
            <%}else{ %>
            <td><button type="submit" name="button" value="removeiaptranid">Remove IAP TranID</button></td>
            <%} %>
        </tr>
         <tr>
            <td><b>Ramah Alum Info</b></td>
            <td>
                Has Ramah ? <%=Model.HasRamahAlumBadge %>, 
                <br />
                Camp ID:  <%=Model.RamahCamp %>
                <br />
                Start Year: <%=Model.RamahStartYear %>
                <br />
                End Year: <%=Model.RamahEndYear %>

            </td>
            <%if (!Model.HasRamahAlumBadge){ %>
            <td><button type="submit" name="button" value="addRamahAlum">Add Ramah Alum</button></td>
            <%}else{ %>
            <td><button type="submit" name="button" value="removeRamahAlum">Remove Ramah Alum</button></td>
            <%} %>
        </tr>
    </table>
    <%} %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .qaSupportTable td{border:1px solid #999999;padding: 10px;}
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/MemberLeft", Model.MemberLeft); %>
</asp:Content>
