﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.PremiumServiceSettings>" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<style type="text/css">
    fieldset {
        border: 1px solid gray;
        }
</style>
<div class="modal" id="promptPSS" >

    <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverLayPSS()"><b>[X]</b></a></div>
    <span><b>Premium Service Settings</b><br /><br /></span> 

    <%if (Model.HasHighlightProfile) {%>
    <div style="width:100%;"  id="highlight-enabled">
    <fieldset>
        <legend>Highlight Profile</legend>
           <table width="100%" cellpaddin="20px;">
                <tr>
                    <td  style="padding:5px 5px 5px 300px;"><%= Html.RadioButtonFor(model => model.HiglightEnabled, false) %> Disabled <%= Html.RadioButtonFor(model => model.HiglightEnabled, true)%> Enabled</td>
                </tr>
            </table>
            
    </fieldset>
    </div> 
  
    <%} %>
    <%if (Model.HasSpotlightProfile) {%>
   <div style="width:100%;"  id="spotlight-enabled">
     <fieldset>
        <legend>Spotlight Profile</legend>
        <table width="100%">
            <tr>
                 <td  align="left" style="padding:7px 7px 7px 300px;"><%= Html.RadioButtonFor(model => model.SpotlightEnabled, false) %> Disabled <%= Html.RadioButtonFor(model => model.SpotlightEnabled, true)%> Enabled</td>
            </tr>
            <tr>
                <td   align="left" style="padding:7px 7px 7px 300px;">
                    <%=Html.DropDownList("DDLGenderPSS", GeneralHelper.GetGenderListItems(Model.SpotlightGender)) %> Seeking <%=Html.DropDownList("DDLSeekingGenderPSS", GeneralHelper.GetSeekingGenderListItems(Model.SpotlightSeekingGender))%>
	            </td>
            </tr>
            <tr>
                <td align="left" style="padding:7px 7px 7px 300px;">Age: <%=Html.TextBoxFor(model => model.SpotlightAgeMin, new { style = "width: 50px;" })%> to <%=Html.TextBoxFor(model => model.SpotlightAgeMax, new { style = "width: 50px;" })%>   </td>
            </tr>
            <tr>
                <td  align="center" style="padding:10px 7px 7px 0px;">
                    located within: <%=Html.DropDownList("DistancePSS", Model.SpotlightDistanceOptions)%> from 
                    <%Html.RenderPartial("../Shared/Controls/SearchRegionPicker", Model.SpotlightRegionPicker); %>
                </td>
            </tr>
        </table>
         <br /><br />
    </fieldset>  
	   
	</div>
    <%} %>
    <div style="width:100%; padding-left:300px; padding-top:10px;">
        <input type="button" id="btnUpdatePremiumServiceSettings" class="button" value="Save Changes" onclick="UpdatePremiumServiceSettings(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" /> 
        <input type="button" class="button"  onclick="closeOverLay();" value="Cancel" /> 
  </div>
    

</div>
<input type="hidden" id="closeOverlayPSS" />

<script type="text/javascript">

    $("#premiumservicesettings").css('color', '#ffffff');
    $("#premiumservicesettings").css('cursor', 'pointer');


    $('#premiumservicesettings').click(function() {
        $('#promptPSS').lightbox_me({
            centered: true
        });
        return false;
    });

    function closeOverLayPSS() {
        $('#closeOverlayPSS').lightbox_me();
        $('#closeOverlayPSS').trigger('close');
    }
    
</script>
