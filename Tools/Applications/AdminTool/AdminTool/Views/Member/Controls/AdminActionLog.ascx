﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.AdminActionLog>" %>
<div class="section-title">
    <h1>Admin Notes &amp; Action Log</h1>
</div>
<div style="float:right; padding-top:3px;">
    <a href="#" style="text-decoration:none; text-align:right;" id="expandALL">Expand All Notes</a> | <a href="#" style="text-decoration:none;text-align:right;" id="contractALL">Contract</a>
</div>
<div id="admin-notes-subcontainer">
<%=Html.TextArea("Note")%><br />
<%if (Model.CheckUIPermission(Enums.Operations.AddAdminNote)) { %>
    <input id="admin-add-note-btn" type="submit" class="button" value="Add Note" onclick="AddAdminNote(event, <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)"/>
<% }%>

<table id="admin-notes-table">
    <tr>
        <th>Date</th>
        <th>Admin Email</th>
        <th>Action</th>
        <th id="suspend-reason-column"></th>
    </tr>
    <%foreach (AdminActivityContainer activityContainer in Model.ActivityContainerCollection)
      {
          if (activityContainer.Type == ActivityType.Action)
          {%>
            <tr>
                <td><%=activityContainer.AdminActionLog.InsertDate.ToString()%></td>
                <td><%=activityContainer.AdminActionLog.EmailAddress%></td>
                <td><%=activityContainer.AdminActionLog.ActionDescription%></td>
                <td><% if (activityContainer.AdminActionLog.ReasonDescription != "")
                       {%>
                    <a href="#" class="more-link">[more]</a>
                    <% } %>
                </td>
            </tr>
            <% if (activityContainer.AdminActionLog.ReasonDescription != "")
               {%>
               <tr class="more-text hide">
                   <td colspan="4">
                       <div>
                            <%= Regex.Replace(activityContainer.AdminActionLog.ReasonDescription, @"<[^>]*>", String.Empty) %>
                        </div>
                    </td>
            </tr>
             <% }

          }
          else
          {%>
            <tr>
                <td><%=activityContainer.AdminNote.InsertDate.ToString()%></td>
                <td><%=activityContainer.AdminNote.AdminEmailAddress%> </td>
                <td>Admin Note</td>
                <td>
                    <% if (activityContainer.AdminNote.Note != ""){ %>
                    <a href="#" class="more-link">[more]</a>
                    <% } %>
                </td>
            </tr>  
            <% if (activityContainer.AdminNote.Note != "") { %>
            <tr class="more-text hide">
                <td colspan="4">
                    <div>
                        <%= Regex.Replace(activityContainer.AdminNote.Note, @"<[^>]*>", String.Empty) %>
                    </div>
                </td>
            </tr>
            <%}
          }
      
     }%>
</table>
</div>

<script id="new-note" type="text/html">
<tr>
    <td>{%= timestamp %}</td>
    <td>not available</td>
    <td>Admin Note</td>
    <td><a class="more-link" href="#">[less]</a></td>
</tr>
<tr class="more-text">
    <td colspan="4"><div> {%= note %} </div></td>
</tr>
</script>