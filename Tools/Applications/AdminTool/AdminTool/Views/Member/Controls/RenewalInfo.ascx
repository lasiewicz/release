﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.RenewalInfo>" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<style>
    fieldset {
        border: 1px solid gray;
        }
</style>
<div class="modal" id="promptRenewal" > 
    <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverLayRenewal()"><b>[X]</b></a></div>
    
    <table cellpadding="5px" cellspacing="5px">
            <tr>
                <td colspan="2">
                    <strong>Adjust Subscription Expiration Date</strong>            
                </td>
            </tr>
            <tr><td><br /></td></tr>
            <tr>
                <td>
                    <%if(Model.CanAdjustRenewalDate) {%>
                        <input id="update-renewal-date" type="submit" class="button" value="Update" onclick="UpdateRenewalDate(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)"/>
                    <%} else { %>
                        <p><span style="font-size:10pt; font-family:Arial; color:#555555;">The Adjust Subscription Expiration Date feature is unavailable because the subscription expiration and renew dates are in sync.</span></p>
                    <%} %>
                </td>
            </tr>
            <tr>
                <td><br /></td>
            </tr>
            <tr>
                <td><br /></td>
            </tr>
    </table>
     
    <% if(Model.HasActiveFreeTrialSubscription) { %>
    <table>
        <tr>
            <td colspan="2">
                    <strong>Cancel Free Trial</strong>
            </td>
        </tr>
        <tr><td><br /></td></tr>
        <tr>
            <td>
                <input id="cancelTrialForMember" type="submit" class="button" value="Cancel Free Trial Subscription" onclick="cancelTrialForMember(event, $(this), <%=Model.MemberID.ToString()%>, <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>)"/>
            </td>
        </tr>
    </table>
    <%} else { %>
        <table cellpadding="5px" cellspacing="5px">
        <tr>
            <td colspan="2">
                    <strong>Manage Auto-Renewal</strong>
            </td>
        </tr>
        <tr>
            <td><br /></td>
        </tr>
        <%if(Model.CanReopen){ %>
            <tr>
                <td><span style="font-size:11pt; font-family:Arial;">Re-open Subscription Plan: </span></td>
                <td><%=Html.CheckBox("ReopenSub") %></td>
            </tr>
            <tr>
                <td><br /></td>
            </tr>
            <tr>
                <td><span style="font-size:10pt; font-family:Arial; color:#555555;">Checking this box will turn on autorenewal and / or reinstate this member's subscription.</span></td>
            </tr>
            <%if(Model.HasPremiumSpotlight ||Model.HasPremiumHighlight || Model.HasPremiumJMeter || Model.HasPremiumAllAccess || Model.HasPremiumReadReceipts) { %>
                    <tr>
                        <td colspan="2">
                            <span style="font-size:10pt; font-family:Arial; color:#555555;">Checking the following box(es) will turn on autorenewal for this member's a la carte premium service plans.</span>
                        </td>
                    </tr>
                <%if(Model.HasPremiumHighlight) {%>
                    <tr>
                        <td>Profile Highlight:</td>
                        <td><%=Html.CheckBox("ReopenPremiumHighlight") %></td>
                    </tr>
                <%} %>
                <%if(Model.HasPremiumSpotlight) {%>
                    <tr>
                        <td>Member Spotlight:</td>
                        <td><%=Html.CheckBox("ReopenPremiumSpotlight") %></td>
                    </tr>
                <%} %>
                <%if(Model.HasPremiumJMeter) {%>
                    <tr>
                        <td>JMeter:</td>
                        <td><%=Html.CheckBox("ReopenPremiumJMeter") %></td>
                    </tr>
                <%} %>
                <%if(Model.HasPremiumAllAccess) {%>
                    <tr>
                        <td>All Access:</td>
                        <td><%=Html.CheckBox("ReopenPremiumAllAccess") %></td>
                    </tr>
                <%} %>
                <%if(Model.HasPremiumReadReceipts) {%>
                    <tr>
                        <td>Read Receipt:</td>
                        <td><%=Html.CheckBox("ReopenPremiumReadReceipt") %></td>
                    </tr>
                <%} %>
            <%} %>
            <tr>
                <td>&nbsp;<br /></td>
            </tr>
            <tr>
                <td><input id="update_renewal_info" type="submit" class="button" value="Save" onclick="UpdateRenewalInfo(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)"/></td>
                <td><input type="button" class="button"  onclick="closeOverLayRenewal();" value="Cancel" /> </td>
            </tr>
        <%}else if (Model.HasValidPlan){ %>
            <tr>
               <td colspan="2"><span style="font-size:10pt; font-family:Arial; color:#555555;">Click "End" to turn off auto-renewal for this account. Any premium services will also no longer renew.</span></td> 
            </tr>
            <tr>
                <td><br /></td>
            </tr>
            <tr>
                <td><input id="end-auto-renewal" type="submit" class="button" value="End" onclick="EndAutoRenewalStart(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)"/></td>
                <td><input type="button" class="button"  onclick="closeOverLayRenewal();" value="Cancel" /> </td>
            </tr>
            <tr>
                <td><br /></td>
            </tr>
            <%if (Model.HasValidPlan && (Model.RenewalEnabledPremiumHighlight || Model.RenewalEnabledPremiumSpotlight || Model.RenewalEnabledPremiumJMeter || Model.RenewalEnabledPremiumAllAccess || Model.RenewalEnabledReadReceipts))
              {%>
                <tr>
                    <td colspan="2">
                        <span style="font-size:10pt; font-family:Arial; color:#555555;">Checking the following box(es) will turn OFF autorenewal for this member's a la carte premium service plans.</span>
                    </td>
                </tr>
                 <%if(Model.RenewalEnabledPremiumHighlight) {%>
                    <tr>
                        <td>Profile Highlight:</td>
                        <td><%=Html.CheckBox("StopPremiumHighlight") %></td>
                    </tr>
                <%} %>
                <%if(Model.RenewalEnabledPremiumSpotlight) {%>
                    <tr>
                        <td>Member Spotlight:</td>
                        <td><%=Html.CheckBox("StopPremiumSpotlight") %></td>
                    </tr>
                <%} %>
                <%if(Model.RenewalEnabledPremiumJMeter) {%>
                    <tr>
                        <td>JMeter:</td>
                        <td><%=Html.CheckBox("StopPremiumJMeter") %></td>
                    </tr>
                <%} %>
                <%if(Model.RenewalEnabledPremiumAllAccess) {%>
                    <tr>
                        <td>All Access:</td>
                        <td><%=Html.CheckBox("StopPremiumAllAccess") %></td>
                    </tr>
                <%} %>
                <%if(Model.RenewalEnabledReadReceipts) {%>
                    <tr>
                        <td>Read Receipts:</td>
                        <td><%=Html.CheckBox("StopPremiumReadReceipts") %></td>
                    </tr>
                <%} %>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                </tr>
                <tr>
                <td><input id="stopALaCartePremServices" type="submit" class="button" value="Stop AR" onclick="UpdateALaCarteRenewal(event, $(this), <%=Model.MemberID.ToString()%>, <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>, false)"/></td>
                <td><input type="button" class="button"  onclick="closeOverLayRenewal();" value="Cancel" /> </td>
            </tr>
            <% } %>

             <% if(Model.TerminationReasons != null && Model.TerminationReasons.Count > 0) { %>
            <tr>
                <td colspan="2">
                <div id="terminationReasonPanel">
                <% foreach(var tr in Model.TerminationReasons) { %>
                    <input type="radio" name="terminationreason" value="<%=tr.Value %>" /><%=tr.Text %><br/>
                <%}%>
                </div>
                </td>
            </tr>
            <% } %>
        <%}else{ %>
            <tr>
               <td colspan="2"><span style="font-size:10pt; font-family:Arial; color:#555555;">This reactivation feature is unavailable because the member's plan is no longer available.</span></td> 
            </tr>
        <%} %>
    </table>
    <%} %>
    
</div>
<input type="hidden" id="closeOverlayRenewal" />

<script type="text/javascript">
    $("#updateRenewal").css('cursor', 'pointer');

    $('#updateRenewal').click(function() {
    $('#promptRenewal').lightbox_me({
            centered: true
        });
        return false;
    });

    function EndAutoRenewalStart(e, target, memberID, siteID) {
        // if terminationReasonPanel is already visible, check to see if the admin selected a reason for termination
        // if terminationReasonPanel is not visible, it means it's the first time clicking on the button, so show it
        if($('#terminationReasonPanel').is(':visible')) {
            // validate to see if admin selected a reason
            if($('input[name=terminationreason]:checked').length == 0) {
                alert("Please select a termination reason");
                return;
            }

            EndAutoRenewalWithReason(e, target, memberID, siteID, $('input[name=terminationreason]:checked').val());
        } else {
            $('#terminationReasonPanel').show();
        }
        
    }

    function closeOverLayRenewal() {
        $('input[name=terminationreason]').each(function (index) {
            if ($(this).is(":checked")) {
                $(this).attr("checked", false);
            }
        });
        $('#terminationReasonPanel').hide();
        $('#closeOverlayRenewal').lightbox_me();
        $('#closeOverlayRenewal').trigger('close');
    }

    $(document).ready(function () {
        $('#terminationReasonPanel').hide();
    });
    
</script>

