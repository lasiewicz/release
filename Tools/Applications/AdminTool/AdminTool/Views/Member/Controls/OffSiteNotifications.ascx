﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.OffSiteNotifications>" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>

<div class="modal" id="prompt" >
<div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverLay()"><b>[X]</b></a></div>
<span><b>Notifications Management</b><br /><br /></span>

<div style="width:100%;">

    <div style="float:left; width:40%"> 
        <ul>
            <li class="list-item-wider" id="click-alerts"><label>Secret Admirer Alerts</label><%= Html.CheckBoxFor(model => model.ClickAlert)%></li>
            <li class="list-item-wider" id="email-alerts"><label>EMail Alerts</label><%= Html.CheckBoxFor(model => model.EmailAlert)%></li>
            <li class="list-item-wider" id="ecard-alerts"><label>E-Card Alerts </label><%= Html.CheckBoxFor(model => model.EcardAlert)%></li>
        
            <li class="list-item-wider" id="hotlist-alerts"><label>Hot List Alerts </label><%= Html.CheckBoxFor(model => model.HotlistAlert)%></li>

            <li class="list-item-wider" id="profileviewed-alerts"><label>Profile Viewed Alerts </label><%= Html.CheckBoxFor(model => model.ProfileViewedAlert)%></li>
        
            <li class="list-item-wider" id="requestessay-alerts"><label>Missing Essay Request </label><%= Html.CheckBoxFor(model => model.RequestEsssayAlert)%></li>
            <!--
            <li class="list-item-wider" id=""><label>Profile Viewed Alerts and Offers </label><%= Html.CheckBox("temp") %></li>
            -->
            <li class="list-item-wider" id="newsletter-alerts"><label>Newsletters and Offers</label><%= Html.CheckBoxFor(model => model.NewsletterAlert)%></li>
            <li class="list-item-wider" id="travel-alerts"><label>Invitations by Email </label> <%= Html.CheckBoxFor(model => model.TravelEventsAlert)%></li>
        </ul>
    </div> 

    <div style="float:right; width:60%">
        <div style="width:100%;float:left;padding-bottom:10px;" id="matchmail-frequency">
            <dl class="over-lay-pop_plus">
                <dt>MatchMail</dt>
                <dd>   
	                <%= Html.RadioButtonFor(model => model.MatchMailFrequency, 1)%> Daily  
                    <%= Html.RadioButtonFor(model => model.MatchMailFrequency, 0)%>Never   <br />
                    <%if (Model.MemberSiteInfo.SiteID.ToString() != "JDate")
                    {%>
                        <%= Html.RadioButtonFor(model => model.MatchMailFrequency, 7)%> Once Weekly
                        <%= Html.RadioButtonFor(model => model.MatchMailFrequency, 3)%> Twice Weekly 
                    <%}%>
                </dd>
	        </dl>
         </div>   
         <div style="width:100%;float:left;" id="clickmail-frequency">
             <dl  class="over-lay-pop_plus">
                <dt> Secret Admirer Email</dt>
                <dd>
                     <%= Html.RadioButtonFor(model => model.ClickMailFrequency, 1)%> Daily
                     <%= Html.RadioButtonFor(model => model.ClickMailFrequency, 0)%>  Never <br />  
                     <%= Html.RadioButtonFor(model => model.ClickMailFrequency, 7)%>  Once Weekly 
                     <%= Html.RadioButtonFor(model => Model.ClickMailFrequency, 3)%> Twice Weekly
                     <br /><br />
                </dd>
             </dl>
        </div>

        <div style="width: 100%; float: left; margin-left: 175px; margin-top: 40px;">
            <input type="button" id="Button1" class="button" value="Save Changes" onclick="UpdateOffSiteNotifications(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" />
            <input type="button" class="button" onclick="closeOverLay();" value="Cancel" style="margin-left: 18px;"/>
        </div>
    </div>

</div>

</div>
<input type="hidden" id="closeOverlay" />

<script type="text/javascript">

    $("#offSite").css('color', '#ffffff');
    $("#offSite").css('cursor', 'pointer');


    $('#offSite').click(function() {
        $('#prompt').lightbox_me({
            centered: true
        });
        return false;
    });


    function closeOverLay() {

        $('#closeOverlay').lightbox_me();
        $('#closeOverlay').trigger('close');


    }
    
</script>