﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.JMeterEditable>" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<style type="text/css">
    fieldset {
        border: 1px solid gray;
        }
</style>
<div class="modal" id="promptJMeter">
<div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverLayJMeter()"><b>[X]</b></a></div>

<b>Reset Personality Test?</b><br/>
(Clicking 'Yes" will reset all the J-meter answers)<br/>
<input type="button" id="btnResetJMeter" class="button" value="Yes" onclick="ResetJMeter(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" />
<br/>

<b>Synchronize Match Test Status With Zoozamen</b><br/>
<input type="button" id="btnSyncWithZoozamen" class="button" value="Sync Now!" onclick="SyncWithZoozamen(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" /> 
<br/>
</div>

<input type="hidden" id="closeOverLayJMeter" />

<script type="text/javascript">

    $("#jmetereditable").css('color', '#0000FF');
    $("#jmetereditable").css('cursor', 'pointer');


    $('#jmetereditable').click(function () {
        $('#promptJMeter').lightbox_me({
            centered: true
        });
        return false;
    });

    function closeOverLayJMeter() {
        $('#closeOverLayJMeter').lightbox_me();
        $('#closeOverLayJMeter').trigger('close');
    }
</script>