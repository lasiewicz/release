﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.ExpandedUserInfo>" %>
<%@ Import Namespace="Matchnet.Configuration.ServiceAdapters" %>


<table style="width:1000px;padding-top:25px; margin-left:0;">
    <tr valign="top">
        <td width="20%" valign="top" align="left">
            <br />
            <p class="profile-image_no_pad">
            <% if (Model.ThumbnailUrl == null || Model.ThumbnailUrl == "" || Model.ThumbnailUrl == " ")
               { %>
                 <img src="../../../Images/no-photo.gif" />
            <%}
               else
               { %>
            <img src=<%=Model.ThumbnailUrl %> />
            <%} %>
            </p>
		</td>
        <td width="80%" valign="top">
             <table width="100%">
                    <tr>
                        <td><br /></td>
                        <td><br /></td>
                        <td><br /></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td class="memberLabel">Member:</td>
                                    <td style=" padding-bottom:10px;"><a class="memberLink" onclick="newMemberWindow('<%=Model.SiteId%>','<%=Model.MemberID %>');" > <%= Model.MemberID %></a></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel">Email:</td>
                                    <td class="regularLabel">
                                        <b>
                                            <% if (Model.GetConnectSiteName() == string.Empty)
                                            {%> <a href="mailto: <%=Model.Email %>"><%}
                                                else
                                            {%><a href="https://www.securemingle.com/admin/sites/<%=Model.GetConnectSiteName() %>/support/send_message.html?search=<%=Model.Email %>" target="_blank"><%} %>
                                            <span><%=Model.Email %></span></a>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>First Name: </b></td>
                                    <td class="regularLabel"><%=Model.FirstName %></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Last Name:</b></td>
                                    <td class="regularLabel"><%=Model.LastName %></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Registration Date:</b></td>
                                    <td class="regularLabel"> <%=Model.RegistrationDate.ToString() %></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Subscriber Status:</b></td>
                                    <td class="regularLabel"> <%=Model.SubscriberStatus %></td>
                                </tr>
                                <%if (Model.DaysSinceFirstSubscription >= 0)
                                  { %>
                                     <tr>
                                        <td class="memberLabel"><b>Days since first subscription: </b></td>
                                        <td class="regularLabel"><%=Model.DaysSinceFirstSubscription.ToNullableString()%></td>
                                    </tr>
                                <%}
                                  else
                                  { %>
                                    <tr>
                                        <td class="memberLabel"><br /></td>
                                        <td class="regularLabel"><br /></td>
                                    </tr>
                                <%} %>
                            </table>
                        </td>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td class="memberLabel"><b>Username:</b></td>
                                    <td class="regularLabel"><%=Model.Username %></td> 
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Status: </b></td>
                                    <td class="regularLabel"><%=Model.MaritalStatus %></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Basics:</b></td>
                                    <td class="regularLabel"> <%=Model.GenderSeekingGender %></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Occupation: </b></td>
                                    <td class="regularLabel"><%=Model.Occupation %></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Education: </b></td>
                                    <td class="regularLabel"><%=Model.Education%></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Religion: </b></td>
                                    <td class="regularLabel"><%=Model.Religion %></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Ethnicity: </b></td>
                                    <td class="regularLabel"><%=Model.Ethnicity %></td>
                                </tr>
                             </table>
                         </td>
                       
                        <td valign="top">
                            <table>
                                <tr valign="top">
                                    <td class="memberLabel"><b>Profile Location: </b></td>
                                    <td class="regularLabel"><%=Model.ProfileLocation%></td>
                                </tr>
                                <tr>
                                    <td class="memberLabel"><b>Billing phone #: </b></td>
                                    <td class="regularLabel"><%=Model.BillingPhoneNumber%></td>
                                </tr>
                                <%if (Model.IPAddress != null) { %>
                                    <tr>
                                        <td class="memberLabel"><b>IP Address: </b></td>
                                        <td class="regularLabel"><%=Model.IPAddress%></td>
                                    </tr>
                                <%}else { %>
                                    <tr>
                                        <td class="memberLabel"><b>IP Address: </b></td>
                                        <td class="regularLabel">&nbsp;</td>
                                    </tr>
                                <%} %>
                                <%if (Model.IPLocation != null) { %>
                                    <tr>
                                        <td class="memberLabel"><b>IP Location: </b></td>
                                        <td class="regularLabel"><%=Model.IPLocation.countryName%><br />
                                            <%=Model.IPLocation.regionName%>
                                            <% if (Model.IPLocation.city != string.Empty)
                                               {%>
                                                <br /><%=Model.IPLocation.city%>
                                            <%} %>
                                    
                                        </td>
                                    </tr>
                                <%} else { %>
                                    <tr>
                                        <td class="memberLabel"><b>IP Location: </b></td>
                                        <td class="regularLabel">&nbsp;</td>
                                    </tr>
                                <% }  %>
                                    <tr>
                                        <td class="memberLabel" width="50%" align="left" colspan="2"> 
                                            <div class="divFraudScore" style="float:left; width:100%; height:100%; text-align:center;width:40%; border-bottom: solid 1px black; border-left: solid 1px black; border-right: solid 1px black; border-top: solid 1px black;">
                                                <span class="score"><%=Model.FraudResult.RiskInquiryScore%></span>
                                                <%--<span  class="hexColor" style="display:none"><%=Model.FraudResult.ColorHex%></span>--%>
                                               <%-- <input type="button" style="float:right;" id="btnViewFraud" value="Details"> 
                                                <div id="dialog" title="Fraud Details" class="modal-close">
                                                    <ul>
                                                  <li>Status :  <%=Model.FraudResult.RiskInquiryStatusDescription %> </li>
                                                 <li>Rules: </li>
                                                 <li><% Model.FraudResult.FraudRules.Aggregate("", (current, rule) => current + "\n"+ rule); %></li>
                                                  </ul>      
                                                </div>--%>
                                            </div> 
                                        </td>
                                    </tr>   
                             </table>
                        </td>
                         <td valign="top" class="expandinfo">
                            <script type="text/javascript" src="<%= RuntimeSettings.GetSetting("MINGLE_ROLF_IOVATION_INFO_URL") %>?more_info=1&memberid=<%=Model.MemberID %>&siteid=<%= Model.SiteId %>"></script>
                        </td>
                    </tr>
                </table>
        </td>
    </tr>
</table>          
   
<script type="text/javascript">
$(document).ready(function () {
    $(document).on('click', '#btnViewFraud', function () {
        $( "#dialog" ).dialog();
        return false;
    });
}); 
 </script>




