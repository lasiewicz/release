﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.ILAuthenticated>" %>

<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<style type="text/css">
    fieldset {
        border: 1px solid gray;
        }
</style>

<div class="modal" id="promptILAuthenticated">
<div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverLayILAuthenticated()"><b>[X]</b></a></div>

Username: <%=Model.Username + " - " + Model.MemberID %><br/>
Email: <%=Html.DisplayFor(m=>m.MemberEmail) %><br/>
Site: <%=Html.DisplayFor(m => m.MemberSiteInfo.Name)%><br/>

<div class="il-auth-status"><%=Html.DisplayFor(m => m.Status)%></div><br/>

Set Expiration Date: <%=Html.TextBox("ILAuthExpDate", Model.ExpirationDate == DateTime.MinValue ? string.Empty : Model.ExpirationDate.ToString("MM/dd/yyyy"), new { @class = "date-field" })%><br/>
<input type="checkbox" id="DisableILAuth"/>Disable<br/>

<input type="button" id="btnDisableILAuth" class="button" value="Save" onclick="UpdateILAuthentication(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" />

</div>

<input type="hidden" id="closeOverLayILAuthenticated" />

<script type="text/javascript">

    $("#ilauthenticated").css('color', '#0000FF');
    $("#ilauthenticated").css('cursor', 'pointer');


    $('#ilauthenticated').click(function () {
        $('#promptILAuthenticated').lightbox_me({
            centered: true
        });
        return false;
    });

    function closeOverLayILAuthenticated() {
        $('#closeOverLayILAuthenticated').lightbox_me();
        $('#closeOverLayILAuthenticated').trigger('close');
    }

    $(document).ready(function () {
        // date fields
        $(".date-field").datepicker({ dateFormat: 'mm/dd/yy', changeMonth: true, changeYear: true });
    });
</script>