﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.MemberLeft>" %>
<%@ Import Namespace="Matchnet.Configuration.ServiceAdapters" %>


<!-- Sidebar -->
	<div id="management-sidebar" align="center">

	    <p class="site-logo">
	        <img src="/Images/Logos/<%= ((int) Model.MemberSiteInfo.SiteID).ToString() %>.png"/>
	    </p>
    
    	<a href="/" class="button" id="return-to-search">Return to Search</a>

    	<p class="profile-image">
    	    <%if (Model.PhotoURL != "")
           { %>
          
    	    <img src="<%=Model.PhotoURL %>" />
    	    <%}
           else
           { %>
    	    <!--no photo, Tom we'll need some default no photo image here-->
    	    No Photo Available
    	    <%} %>
    	</p>
               
	    	<div class="sidebar-boxes">        
    	    	<p class="side-headers">Admin Pages</p>
				<div class="side-content">
                    <%foreach (AdminTool.Models.ModelViews.MemberSiteInfo siteInfo in Model.OtherMemberSites)
                    { %>
                    <a id="view-member-<%= Model.MemberID.ToString() %>" href="/Member/View/<%=((int)siteInfo.SiteID).ToString()%>/<%=Model.MemberID.ToString() %>"><%= siteInfo.Name + (siteInfo.IsSubscriber ? " $" : "")%></a><br />
                    <%} %>      
            	</div>                    
        	</div>

            <!-- Iovation -->
            <div class="sidebar-boxes">        
    	    	<p class="side-headers">Iovation</p>
				<div class="side-content">
                    <script type="text/javascript" src="<%= RuntimeSettings.GetSetting("MINGLE_ROLF_IOVATION_INFO_URL") %>?more_info=1&memberid=<%=Model.MemberID %>&siteid=<%= (int)Model.MemberSiteInfo.SiteID %>"></script>
            	</div>                    
        	</div>
            
            <%if (Model.UserCapabilities.InformationCapturedDate != DateTime.MinValue) { %>
    		    <div class="sidebar-boxes">
	                <p class="side-headers">Computer Information</p>                
    	            <div class="side-content">
            	    <p class="side-headers">Captured: </p>
				    <p><%=Model.UserCapabilities.InformationCapturedDate.ToString()%></p>                
                    <p class="side-headers">Flash Version:</p>
				    <p><%=Model.UserCapabilities.FlashVersion %></p>                               
				    <p class="side-headers">Connect Speed:</p>
				    <p></p>                
                    <p class="side-headers">User Agent:</p>
				    <p> <%=Model.UserCapabilities.UserAgent%></p>                       
        		    </div>                    
                </div>
            <% } else {%>            
       		    <div class="sidebar-boxes">
	                <p class="side-headers">Computer Information</p>                
    	            <div class="side-content">
            	    <p class="side-headers">Captured: </p>
				    <p></p>                
                    <p class="side-headers">Flash Version:</p>
				    <p></p>                               
				    <p class="side-headers">Connect Speed:</p>
				    <p></p>                
                    <p class="side-headers">User Agent:</p>
				    <p></p>                       
        		    </div>                    
                </div>         
            <%} %>
	    	<div class="sidebar-boxes">
	            <p class="side-headers">Marketing Info</p>
    	        <div class="side-content">
        	    <p class="side-marketing-content">PRM: <%=Model.PRM%></p>
            	<p class="side-marketing-content">LGID: <%=Model.LuggageID %></p>
	            <p class="side-marketing-content">Banner ID: <%=Model.BannerID %></p>
    	        <p class="side-marketing-content">Landing Page: <%=Model.LandingPageID %></p>
            	</div>                        
            </div>
	</div>
