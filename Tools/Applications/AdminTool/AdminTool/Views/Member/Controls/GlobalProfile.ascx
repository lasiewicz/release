﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.GlobalProfile>" %>


<ul>

<li id="AdminSuspendedDiv" class="list-item"><label>Admin Suspended:</label><span>Yes</span> <%= Html.RadioButtonFor(model => model.AdminSuspended, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.AdminSuspended, "false")%></li>
<li class="list-item">
    <% if (Model.AdminSuspended.ToString() == "True") {%>
        <%=Html.DropDownList("AdminActionReasonID", Model.AdminSuspendReasons ) %>
    <% }  else{ %>
        <%=Html.DropDownList("AdminActionReasonID", Model.AdminSuspendReasons, new {disabled="disabled"}) %>
   
  <% }%>
</li>
<li id="BadEmailDiv" class="list-item"><label>Bad Email:</label> <span>Yes</span> <%= Html.RadioButtonFor(model => model.BadEmail, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.BadEmail, "false")%></li>
<li id="EmailVerifiedDiv" class="list-item"><label>Email Verified:</label> <span>Yes</span> <%= Html.RadioButtonFor(model => model.EmailVerified, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.EmailVerified, "false")%></li>
<li class="sendverification">
    <a id="send-verification" href="#" class="trigger-overlay">Send Verification Letter</a>
</li>
<li id="DneDiv" class="list-item"><label>On DNE List:</label> <span>Yes</span> <%= Html.RadioButtonFor(model => model.OnDNE, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.OnDNE, "false")%></li>
</ul>
<%if (Model.CheckUIPermission(Enums.Operations.GlobalProfileUpdate)) { %>
    <input type="button" value="Save Changes" id="btnSaveChanges" class="button" onclick="UpdateGlobalProfileInfo(event, $(this), false, <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" />   
<% }%>

<div id="send-verification-over" class="main-gray-boxes overlay">
    <a href="" class="close-x"><img src="/images/close.png" width="15" /></a>
    <p class="send-content">Send Verification Email to:</p>
    <p class="send-email"><%=Model.MemberEmail %></p>
    <input type="button" value="Send" class="button" onclick="SendVerificationEmail(event,<%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" />
    <input type="button" value="Cancel" class="cancel-overlay button" />
</div>

<%--<div id="admin-suspend-over" class="main-gray-boxes overlay">
                
                    <a href="" class="close-x"><img src="/images/close.png" width="15" /></a>
                    <p>Warning: Suspending this account will also end the automatic renewals for the current subscription!</p>
                    
                    
                     <div style="width:75px;float:left;">                                     
                         <input type="button" value="Continue" class="button" onclick="UpdateGlobalProfileInfo(event, $(this), 'true', <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" style="width:65px"  /> &nbsp;
                     </div>
                    <input type="button" value="Cancel" class="cancel-overlay button" />

                    <br /><br />                    
</div>--%>
