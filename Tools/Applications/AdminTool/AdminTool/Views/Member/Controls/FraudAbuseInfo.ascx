﻿    <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.FraudAbuseInfo>" %>    <%@ Import Namespace="Matchnet.Configuration.ServiceAdapters" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<h2>Fraud &amp; Abuse</h2>
<ul>
<li id="subscription-fraud" class="list-item"><label>Subscription Fraud:</label> <span>Yes</span> <%= Html.RadioButtonFor(model => model.SubscriptionFraud, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.SubscriptionFraud, "false")%></li>
<li id="chat-banned" class="list-item"><label>Chat Banned:</label> <span>Yes</span> <%= Html.RadioButtonFor(model => model.ChatBanned, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.ChatBanned, "false")%></li>
<li id="email-banned" class="list-item"><label>Sent Mail:</label> <span>Hide</span> <%= Html.RadioButtonFor(model => model.EmailBanned, "true") %> <span>Show</span> <%= Html.RadioButtonFor(model => model.EmailBanned, "false")%></li> 
<%if (Model.IPAddress != string.Empty) { %>
    <li id="join-ip-address"  class="list-item"><label>Join IP Address: </label><%=Model.IPAddress%></li>
    <%if (Model.IPLocation != null)
      {%>
        <li id="ip-address-location"  class="list-item"><label>IP Location: </label><%=Model.IPLocation.countryName%>
            <br /><%=Model.IPLocation.regionName%>
            <% if (Model.IPLocation.city != string.Empty)
               {%>
                <br /><%=Model.IPLocation.city%>
        <%} %>
        </li>
    <%} %>
<%} %>
    <li id="fraud-score"  class="list-item">
        <label>Fraud Score: </label>
            <span style="border:solid 1px black; text-align:center;" id="divFraudScore"><%=Model.KountFraudResult.RiskInquiryScore %> 
            <%--</span><span id="hexColor" style="display:none"><%=Model.FraudResult.ColorHex%></span>--%>
                </span>
      <input type="button" style="float:right;" id="btnViewFraud" value="Details"> 
        <div id="dialog" title="Fraud Details" class="modal" hidden>
            
          <label style="font-weight:bold;">Status : </label>
                 <%=Model.KountFraudResult.RiskInquiryStatusDescription %> 
                <br>
         <label style="font-weight:bold;">Rules:   </label>
            <% if (Model.KountFraudResult.FraudRules.Any())
               {%>
                <% foreach (var rule in Model.KountFraudResult.FraudRules) 
                { %>
                <%: rule %>
                <br>
                <% } %>
            <%} %>
            
        </div>
    </li>
    <li class="list-item"><label>Signed Pledge: </label> <%=Model.SignedPledge.ToYesNoString() %></li>
<%if (Model.TimeBetweenRegAndFirstSub.TotalMilliseconds > 0)
  {%>
<li id="reg-sub-timespan"  class="list-item"><label>Reg to Sub Timespan: </label><%=Model.TimeBetweenRegAndFirstSub.TotalMinutes.ToString("F0")%> mins</li>
<%} %>
</ul>
<input type="hidden" id="hdnSiteID" value="<%=Model.MemberSiteInfo.SiteID.ToString() %>"/>
<%if (Model.CheckUIPermission(Enums.Operations.UpdateFraudStatus)) { %>
    <input type="button" id="btnUpdateFraud" class="button" value="Save Changes" onclick="UpdateFraudInfo(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" />
<% } %>

<div id="admin-suspend-over" class="main-gray-boxes overlay">
                
                    <a href="" class="close-x"><img src="/images/close.png" width="15" /></a>
                    <p>Warning: Suspending this account will also end the automatic renewals for the current subscription!</p>
                    
                    
                     <div style="width:75px;float:left;">                                     
                         <input type="button" value="Continue" class="button" onclick="UpdateGlobalProfileInfo(event, $(this), 'true', <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" style="width:65px"  /> &nbsp;
                     </div>
                    <input type="button" value="Cancel" class="cancel-overlay button" />

                    <br /><br />                    
</div>
 

<script type="text/javascript">
$(document).ready(function () {
    $(document).on('click', '#btnViewFraud', function () {
        $( "#dialog" ).dialog();
        return false;
    });
}); 
 </script>


