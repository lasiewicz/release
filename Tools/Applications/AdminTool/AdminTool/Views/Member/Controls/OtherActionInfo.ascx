﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.OtherActioninfo>" %>
<h2>Other Available Actions</h2>
<%if (Model.CheckUIPermission(Enums.Operations.ColorCodeReset)) { %>
    <ul id="clear-color-code">

    <li class="list-item">
        <label>Color Code Reset</label>
        <input type="button" class="button" id="btn-clear-color-code" value="Clear Results" onclick="ClearColorCodeResults(event, $(this), false, <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" /><br />
    <%=Html.HiddenFor(model => model.CompletedColorQuiz) %>
    </li>
    </ul>
<% }%>
<ul id="color-analysis">
<li class="list-item"> <label>Color Analysis Purch: </label> 
   <span>Yes</span><%= Html.RadioButtonFor(model => model.ColorAnalysisPurchase, "true", new { disabled = (!Model.CompletedColorQuiz).ToString()} )%>
   <span>No</span><%= Html.RadioButtonFor(model => model.ColorAnalysisPurchase, "false", new { disabled = (!Model.CompletedColorQuiz).ToString() })%></li>
</ul>
<ul id="JDateCoILDiv" style="<%=Html.GetElementVisibility(Model.MemberSiteInfo.SiteID == Spark.CommonLibrary.SiteIDs.JDateCoIL)%>">
<%if (Model.CheckUIPermission(Enums.Operations.ILAuthenticatedUpdate)) { %>
    <li class="list-item">
        <label>Authenticated:</label> <span><a id="ilauthenticated" rel="#promptILAuthenticated"><%=Model.Authenticated.ToYesNoString() %></a></span>
    </li>    
<% }%>
<%if (Model.CheckUIPermission(Enums.Operations.ILProfileHighlightUpdate)) { %>
    <li class="list-item">
        <label>Profile Highlight:</label> <span><a href="http://<%= Model.g.DefaultBedrockAdminHost %>/Applications/Admin/PremiumAuthentication/ProfileHighlight.aspx?impmid=<%= Model.MemberID.ToString() %>&imppid=<%= Model.BrandID.ToString() %>&LayoutTemplateID=2" class="pop-link"><%=Model.ProfileHighlight.ToYesNoString() %></a></span>
    </li>
<% }%>
<%if (Model.CheckUIPermission(Enums.Operations.ILJmeterUpdate )) { %>
    <li class="list-item">
    <label>J-Meter Editable:</label>
    <span>
    <% if (GeneralHelper.IsMatchMeterEnabled(Model.g.GetBrand((int)Model.MemberSiteInfo.SiteID))){ %>
        <a id="jmetereditable" rel="#promptJMeter"><%=Model.JMeterEditable.ToYesNoString() %></a>
    <% } else {%>
        N/A
    <%} %>
    </span>
    </li>
<% }%>
</ul>
<ul id="AllOtherSitesDiv" style="<%=Html.GetElementVisibility(Model.MemberSiteInfo.SiteID != Spark.CommonLibrary.SiteIDs.JDateCoIL)%>">
<li class="list-item">
    <div id="ramahDiv">
        <label>Ramah Alumni Badge:</label> 
       
                <% if(Model.HasRamahBadge) {%> 
                             <span>Yes</span><input type="radio" id="ramahDate" name="ramahDate" value="true" checked="checked" />
                              <span>No</span>  <input type="radio" id="ramahDate" name="ramahDate" value="false"/>
                <%} else {%> 
                             <span>Yes</span>   <input type="radio" id="ramahDate" name="ramahDate" value="true"  />
                             <span>No</span>   <input type="radio" id="ramahDate" name="ramahDate" value="false" checked="checked"/>
                <%} %>
    </div>
</li>
<li class="list-item">
    <label>Profile Highlight: </label> 
    <span>N/A</span>
</li>
<li class="list-item">
    <label>J-Meter Editable:</label> 
    <span> N/A</span>
</li>
</ul>
<% if (Model.CheckUIPermission(Enums.Operations.LifetimeMembershipUpdate))
   { %>
<ul id="lifetime-member">
    <li class="list-item">
        <label>Lifetime Member:</label> 
        <span><%= Model.IsLifetimeMember ? "Yes" : "No" %></span>
        <input type="button" class="button" id="BtnGiveLifetimeMembership" value="Give Lifetime Membership"
         onclick="GiveLifetimeMembership(event, $(this), <%= ((int) Model.MemberSiteInfo.SiteID).ToString() %>,<%= Model.MemberID.ToString() %>, false)"/>
    </li>
</ul>
<% } %>
<%if (Model.CheckUIPermission(Enums.Operations.ColorAnalysisPurchase)) { %>
    <input type="button" id="btn-update-other" class="button" value="Save Changes" onclick="UpdateOtherActions(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" />
<%} %>
<div id="clear-color-code-over" class="main-gray-boxes overlay">
                    <a href="" class="close-x"><img src="/images/close.png" width="15" /></a>
                    <p>Warning: you are about to delete all color code test data from this member's profile.</p>
                    
                     <div style="width:75px;float:left;">                                     
                         <input type="button" value="Continue" class="button" onclick="ClearColorCodeResults(event, $(this), 'true', <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" style="width:65px"  /> &nbsp;
                     </div>
                    <input type="button" value="Cancel" class="cancel-overlay button" />
                
</div>

<div id="DivGiveLifetime-over" class="main-gray-boxes overlay">
    <a href="" class="close-x"><img src="/images/close.png" width="15" /></a>
    <p>Warning: Lifetime Membership Reinstate – Are you sure?</p>
    
    <div style="width:75px;float:left;">                                     
        <input type="button" value="Yes" class="button" style="width:65px"
         onclick="GiveLifetimeMembership(event, $(this), <%= ((int) Model.MemberSiteInfo.SiteID).ToString() %>,<%= Model.MemberID.ToString() %>, true)" /> &nbsp;
    </div>

    <input type="button" value="No" class="cancel-overlay button" />
</div>