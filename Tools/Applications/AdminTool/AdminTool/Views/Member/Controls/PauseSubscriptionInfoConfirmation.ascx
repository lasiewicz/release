﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.PauseSubscriptionInfo>" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<style>
    fieldset {
        border: 1px solid gray;
        }
</style>
<div class="modal" id="promptPauseSubscriptionConfirmation">
    <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverlayPauseSubscriptionConfirmation()"><b>[X]</b></a></div>
    <input type="hidden" id="closeOverlayPauseSubscriptionConfirmation" />
    
    <p>
    <b>Are you sure you want to proceed with pausing this subscription?</b>
    </p>

    <% if (!Model.HasPausedSubscription) { %>

        <br />

        <!--
        <p>
        <b>This subscription will automatically resume on <%= Model.SubscriptionPauseEndsOn %></b>
        </p>
        -->

    <% } %>

    <% 
      string description = String.Empty;
      bool hasPausedSubscription = false;
      
      if (Model.HasPausedSubscription) 
      { 
        description = "Resume Subscription";
        hasPausedSubscription = true;
      }     
      else
      {
        description = "Pause Subscription";
        hasPausedSubscription = false;
      }

    %>

    <p>
        <input id="btnFreezeOrResumeSubscription" type="submit" class="button" value="<%= description %>" onclick="PauseOrResumeSubscription(event, $(this), <%= Model.MemberID.ToString() %>, <%= ((int)Model.MemberSiteInfo.SiteID).ToString() %>, <%= Convert.ToString(hasPausedSubscription).ToLower() %>)"/>    
    </p>
</div>

<input type="hidden" id="closeOverlayPauseSubscriptionConfirmation" />

<script type="text/javascript">

    $("#pauseSubscriptionConfirmation").css('cursor', 'pointer');
    //$("#resumeSubscription").css('cursor', 'pointer');

    $('#pauseSubscriptionConfirmation').click(function () {
        $('#promptPauseSubscriptionConfirmation').lightbox_me({
            centered: true
        });
        return false;
    });

    /*
    $('#resumeSubscription').click(function () {
    $('#pauseSubscription').lightbox_me({
    centered: true
    });
    return false;
    });
    */

    function closeOverlayPauseSubscriptionConfirmation() {
        $('#closeOverlayPauseSubscriptionConfirmation').lightbox_me();
        $('#closeOverlayPauseSubscriptionConfirmation').trigger('close');
    }
    
</script>