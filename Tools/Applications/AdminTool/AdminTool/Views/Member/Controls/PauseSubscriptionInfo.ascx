﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.PauseSubscriptionInfo>" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<style>
    fieldset {
        border: 1px solid gray;
        }
</style>
<div class="modal" id="promptPauseSubscription" > 
    <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverlayPauseSubscription()"><b>[X]</b></a></div>
    <div style="width:100%;float:left;height:500px;" >
        <input type="hidden" id="closeOverlayPauseSubscription" />

        <div style="float:left;width:40%;margin-bottom:20px; border-right:1px solid black;" >
           <span style="padding-left:45px; text-decoration:underline; font-weight:bold; text-align:center;">Subscription Information</span>
           <br />
           <br />
            <div style="float:left;margin-bottom:20px;">
                <span style="text-decoration:underline;">Remaining subscription duration:</span>
                <br /><br />
                <%= Model.RemainingBasicSubscriptionDays %>&nbsp;day(s)
                <br />
                <br />
                <br />
            </div>
           
            <div style="float:left;margin-bottom:20px;">
                <span style="margin-bottom:3px; text-decoration:underline;">Premium services:</span>
                <br /><br />
                <%= Model.CustomerPrivilegeDescriptionList %>
                <br />
                <br />
                <br />
            </div>
           
            <div style="float:left;margin-bottom:20px;">
                <span style="margin-bottom:3px; text-decoration:underline;">Renewal state:</span>
                <br /><br />
                <% if (Model.lastSavedRenewalIsOpen)
                   { %>
                Open
                <% }
                   else
                   { %>
                Closed
                <% } %>
                <br />
                <br />
                <br />
            </div>
           
        </div>
        <div style="float:right;width:55%; " >
           <span style="padding-left:35px; text-decoration:underline;font-weight:bold;">Pause / Resume history</span>
           <br /><br />
            <div style="float:left;margin-bottom:20px;">
                <span style="padding-bottom:10px; text-decoration:underline;">Initial subscription plan:</span>
                <br /><br />
                <span style="width:50px;"> <%= Model.CurrentRenewalPlanInformation %></span>
                <br />
                <br />
                <br />
            </div>
           

            <div style="height:300px;">

                <div>
                    <b><u>Plan duration</u></b>
                    <b><u>Pause date</u></b>
                    <b><u>Resume date</u></b>
                </div>

                <% foreach (AdminTool.Models.ModelViews.Member.PauseSubscriptionHistory history in Model.FrozenTransactionHistory) { %>

                    <div>
                        <%= history.PlanDescription  %>
                        <%= history.PauseDate  %>
                        <% if (history.ResumeDate == DateTime.MinValue)
                           { %>
                            N/A
                        <% }
                           else
                           { %>
                            <%= history.ResumeDate%>
                        <% } %>
                    </div>

                <% } %>

            </div>
        </div>
        
    </div>
    <div style="width:200px;">

        <div>
        <% 
          string description = String.Empty;
      
          if (Model.HasPausedSubscription) 
          { 
            description = "Resume Subscription";
          }     
          else
          {
            description = "Pause Subscription";                
          }      
        %>
        <a id="pauseSubscriptionConfirmation" rel="#promptPauseSubscriptionConfirmation" class="button"><%= description %></a>
        </div>

            <!--
            <input id="btnFreezeOrResumeSubscription" type="submit" class="button" value="Save" onclick="PauseOrResumeSubscription(event, $(this), <%=Model.MemberID.ToString()%>, <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>, <%= Model.HasPausedSubscription %>)"/>
            -->
            <input type="button" class="button"  onclick="closeOverlayPauseSubscription();" value="Cancel" />    
     </div>
</div>

<div id="pause-subscription-info-confirmation">
<% Html.RenderPartial("Controls/PauseSubscriptionInfoConfirmation", Model); %>  
</div>

<input type="hidden" id="closeOverlayPauseSubscription" />

<script type="text/javascript">

    $("#pauseSubscription").css('cursor', 'pointer');
    //$("#resumeSubscription").css('cursor', 'pointer');

    $('#pauseSubscription').click(function () {
        $('#promptPauseSubscription').lightbox_me({
            centered: true
        });
        return false;
    });

    /*
    $('#resumeSubscription').click(function () {
        $('#pauseSubscription').lightbox_me({
            centered: true
        });
        return false;
    });
    */

    function closeOverlayPauseSubscription() {
        $('#closeOverlayPauseSubscription').lightbox_me();
        $('#closeOverlayPauseSubscription').trigger('close');
    }
    
</script>
