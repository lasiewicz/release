﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.AdminAdjustInfo>" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<style>
    fieldset {
        border: 1px solid gray;
        }
</style>
<div class="modal" id="promptAdminAdjust" > 
    <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverlayAdminAdjust()"><b>[X]</b></a></div>
    
    <div id="AdminAdjustMessage" class="error"></div>

    <table cellpadding="10px;" cellspacing="10px;" width="100%" >
        <tr>
            <td ><span style="font-size:11pt; font-family:Arial;">Adjust Duration:</span></td>
            <td colspan="2"><%=Html.DropDownList("DDLDirection", GeneralHelper.GetAdminAdjustDirections())%>&nbsp;<%=Html.TextBox("Duration") %>&nbsp;<%=Html.DropDownList("DDLPeriod", GeneralHelper.GetAdminAdjustPeriods())%></td>
        </tr>
        <tr>
            <td colspan="2"><br /></td>
            <td><br /></td> 
        </tr>
        <tr>
            <td> <span style="font-size:11pt; font-family:Arial;">Add Premium Services:</span> </td>
            <td>
                <%=Html.CheckBox("PremiumHighlightedProfile", Model.HasPremiumHighlightedProfile)%><span style="font-size:10pt; font-family:Arial;">Highlighted Profile</span><br />
                <%=Html.CheckBox("PremiumMemberSpotlight", Model.HasPremiumMemberSpotlight)%><span style="font-size:10pt; font-family:Arial;">Member Spotlight</span><br />
                <%=Html.CheckBox("PremiumJMeter", Model.HasPremiumJMeter)%><span style="font-size:10pt; font-family:Arial;">J-Meter</span><br />
                <%=Html.CheckBox("PremiumAllAccess", Model.HasPremiumAllAccess)%><span style="font-size:10pt; font-family:Arial;">All Access</span><br />
                <%=Html.CheckBox("PremiumReadReceipts", Model.HasPremiumReadReceipts)%><span style="font-size:10pt; font-family:Arial;">Read Receipts</span><br />
            </td>
            <td align="center" style="text-align:center" valign="bottom">
                <br /><br /><br />
                <span style="padding-top:70px;padding-left:60px;">
                    <input id="admin-adjust" type="submit" class="button" value="Save" onclick="AdminAdjust(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)"/>
                    <input type="button" class="button"  onclick="closeOverlayAdminAdjust();" value="Cancel" />
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="2"><br /></td>
            <td><br /></td>
        </tr>
    </table>
    <table cellpadding="10px;" cellspacing="10px;" width="100%" >
        <tr>
            <td><hr /></td>
        </tr>
    </table>
    <table cellpadding="10px;" cellspacing="10px;" width="500px" >
        <tr>
            <td><span style="font-size:11pt; font-family:Arial;">Adjust All Access Email:</span></td>
            <td><%=Html.TextBox("AllAcessEmailAdjustCount") %></td>
            <td valign="bottom"> <span style="padding-left:50px;"><input id="admin-adjust-all-access-count" type="submit" class="button" value="Add" onclick="AdminAdjustAllAccessEmail(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)"/></span></td>
        </tr>
    </table>
</div>
<input type="hidden" id="closeOverlayAdminAdjust" />

<script type="text/javascript">

    $("#adminAdjust").css('cursor', 'pointer');


    $('#adminAdjust').click(function () {
        $('#promptAdminAdjust').lightbox_me({
            centered: true
        });
        return false;
    });

    function closeOverlayAdminAdjust() {
        $('#closeOverlayAdminAdjust').lightbox_me();
        $('#closeOverlayAdminAdjust').trigger('close');
    }
    
</script>


