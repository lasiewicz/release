﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.CommunicationHistory>" %>
<h2>Communication History</h2>
<ul>
<%if (Model.CheckUIPermission(Enums.Operations.ViewEmailActivity)) { %>
<li class="list-item">
  <a href="/Email/List/<%= ((int)(Model.MemberSiteInfo.SiteID)).ToString() %>/<%= Model.MemberID.ToString() %>/1/0" class="pop-link">[View Email Activity]</a>
</li>    
<% } %>

<li class="list-item">
Unread Message Count: <%=Model.UnreadMessageCount %>
</li>

<li class="list-item"><%if (Model.LastMessageSentDate > DateTime.MinValue) { %>
    Last Sent Date: <%=Model.LastMessageSentDate.ToString() %>
<%} else { %>
    Last Sent Date: N/A
<% } %>
</li>
<li class="list-item" style="display:none;"><%if (Model.LastMatchMailSentDate > DateTime.MinValue) { %>
    Last MatchMail Sent To Member: <%=Model.LastMatchMailSentDate.ToString()%>
<%} else { %>
    Last MatchMail Sent To Member: N/A
<% } %>
</li>
<li class="list-item" style="display:none;"><%if (Model.LastMatchMailAttemptDate > DateTime.MinValue) { %>
    Last MatchMail Attempted For Member: <%=Model.LastMatchMailAttemptDate.ToString()%>
<%} else { %>
    Last MatchMail Attempted For Member: N/A
<% } %>
</li>
<li class="list-item"><%if (Model.AllAcessExpireDate > DateTime.MinValue) { %>
  All Access Privilege End Date: <%=Model.AllAcessExpireDate%>
<%} else { %>
  All Access Privilege End Date: N/A
<%} %>
</li>
<li class="list-item"><%if (Model.AllAcessExpireDate > DateTime.MinValue) { %>
  All Access Emails Remaining: <%=Model.AllAccessRemainingCount%>
<%} else { %>
  All Access Emails Remaining: N/A                        
<%} %>
</li>
<li class="list-item"><br /></li>
<li class="list-item"><b>Free Text Warnings</b></li>
<li class="list-item">Contact Info: <%=Model.ContactInfoWarnings %></li>
<li class="list-item">Inappropriate Content: <%=Model.InappropriateContentWarnings %></li>
<li class="list-item">General Violation: <%=Model.GeneralViolationWarnings %></li>
</ul>
<br />
<!-- Hide workaround -->
<ul  style="display:none">
<li id="unfraud-emails">Clear fraud-marked emails? <input type="checkbox" /></li>
<li class="list-item">
    (check box and click Save Changes in the "Fraud & Abuse" section) 
</li>
</ul>

