﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.BillingInfo>" %>
<%@ Import Namespace="Matchnet.Purchase.ValueObjects" %>

<h2>Billing Information</h2>
<%if (Model.CheckUIPermission(Enums.Operations.BillingInformationView)) { %>
    <%=Html.HiddenFor(x => x.PaymentProfileId, new { @class = "pmt-profile-id-holder" })%>
    <%=Html.HiddenFor(x => x.MemberID, new { @class = "pmt-profile-memberid-holder"}) %>
    <%=Html.Hidden("PmtProfileSiteId", (int)Model.MemberSiteInfo.SiteID, new { @class = "pmt-profile-siteid-holder"}) %>
    <% 
        if (Model.UsedForDefaultPaymentProfile == true && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.CreditCard)
        {  
    %>
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
                <li class="list-item"><label>Phone Number:</label><span><%= Model.PhoneNumber %></span></li>
                <% if (Model.ShowGovernmentIssueID){%>
                     <li class="list-item"><label>ID:</label><span><%= Model.GovernmentIssuedID %></span></li>  
                 <%  } %>
                <li class="list-item"><label>Street Address:</label><span><%= Model.StreetAddress %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>City:</label><span><%= Model.City %></span></li>
                <li class="list-item"><label>State:</label><span><%= Model.State %></span></li>
                <li class="list-item"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
                <li class="list-item"><label>Country:</label><span><%= Model.Country %></span></li>
                <li class="list-item"><label>Card Last Four:</label><span><%= Model.LastFourDigitsCreditCardNumber %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Card Type:</label><span><%= Model.CardType %></span></li>
                <li class="list-item"><label>Exp Month:</label><span><%= Model.ExpMonth.ToString() %></span></li>
                <li class="list-item"><label>Exp Year:</label><span><%= Model.ExpYear.ToString() %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == true && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.CreditCardShortFormMobileSite)
        {    
    %>
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
                <li class="list-item"><label>Card Last Four:</label><span><%= Model.LastFourDigitsCreditCardNumber %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Card Type:</label><span><%= Model.CardType %></span></li>
                <li class="list-item"><label>Exp Month:</label><span><%= Model.ExpMonth.ToString() %></span></li>
                <li class="list-item"><label>Exp Year:</label><span><%= Model.ExpYear.ToString() %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == true && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.CreditCardShortFormFullWebSite)
        {    
    %>
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
                <li class="list-item"><label>Card Last Four:</label><span><%= Model.LastFourDigitsCreditCardNumber %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Card Type:</label><span><%= Model.CardType %></span></li>
                <li class="list-item"><label>Exp Month:</label><span><%= Model.ExpMonth.ToString() %></span></li>
                <li class="list-item"><label>Exp Year:</label><span><%= Model.ExpYear.ToString() %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == true && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.PayPalLitle)
        {    
    %>
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></asp:Literal></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></asp:Literal></span></li>
                <li class="list-item"><label>Country:</label><span><%= Model.Country %></asp:Literal></span></li>
                <li class="list-item"><label>Email:</label><span><%= Model.CardType %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Payer Status:</label><span><%= Model.PayerStatus %></span></li>
                <li class="list-item"><label>Paypal Token:</label><span><%= Model.PaypalToken %></span></li>
                <li class="list-item"><label>Paypal BillingAgreementID Last Four:</label><span><%= Model.BillingAgreementID %></span></li>
            </ul>

    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == true && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.ElectronicFundsTransfer)
        {    
    %>

            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
                <li class="list-item no-border"><label>Phone Number:</label><span><%= Model.PhoneNumber %></span></li>
                <li class="list-item"><label>Email:</label><span><%= Model.EmailAddress %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>Street Address:</label><span><%= Model.StreetAddress %></span></li>
                <li class="list-item no-border"><label>City:</label><span><%= Model.City %></span></li>
                <li class="list-item"><label>State:</label><span><%= Model.State %></span></li>
                <li class="list-item no-border"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Account Last Four:</label><span><%= Model.BankAccountNumberLastFour %></span></li>
                <li class="list-item"><label>Routing Last Four:</label><span><%= Model.BankRoutingNumberLastFour %></span></li>
                <li class="list-item no-border"><label>Account Type:</label><span><%= Model.BankAccountType %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == true && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.DebitCard)
        {    
    %>
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
                <li class="list-item"><label>Phone Number:</label><span><%= Model.PhoneNumber %></span></li>
                  <% if (Model.ShowGovernmentIssueID){%>
                     <li class="list-item"><label>ID:</label><span><%= Model.GovernmentIssuedID %></span></li>  
                 <%  } %>
                <li class="list-item"><label>Street Address:</label><span><%= Model.StreetAddress %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>City:</label><span><%= Model.City %></span></li>
                <li class="list-item"><label>State:</label><span><%= Model.State %></span></li>
                <li class="list-item"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
                <li class="list-item"><label>Country:</label><span><%= Model.Country %></span></li>
                <li class="list-item"><label>Card Last Four:</label><span><%= Model.LastFourDigitsCreditCardNumber %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Card Type:</label><span><%= Model.CardType %></span></li>
                <li class="list-item"><label>Exp Month:</label><span><%= Model.ExpMonth.ToString() %></span></li>
                <li class="list-item"><label>Exp Year:</label><span><%= Model.ExpYear.ToString() %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == true && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.None)
        {    
    %>

            <p>Default Billing Information Does Not Exist</p>            
                
    <% 
        }
    %>

    <% 
        if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.CreditCard)
        {  
    %>
            
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
                <li class="list-item"><label>Street Address:</label><span><%= Model.StreetAddress %></span></li>
                <li class="list-item"><label>Phone Number:</label><span><%= Model.PhoneNumber %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>City:</label><span><%= Model.City %></span></li>
                <li class="list-item no-border"><label>State:</label><span><%= Model.State %></span></li>
                <li class="list-item"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
                <li class="list-item"><label>Country:</label><span><%= Model.Country %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                  <% if (Model.ShowGovernmentIssueID){%>
                     <li class="list-item"><label>ID:</label><span><%= Model.GovernmentIssuedID %></span></li>  
                 <%  } %>
                <li class="list-item"><label>Card Type:</label><span><%= Model.CardType %></span></li>
                <li class="list-item"><label>Card Last Four:</label><span><%= Model.LastFourDigitsCreditCardNumber %></span></li>
                <li class="list-item"><label>Exp Month:</label><span><%= Model.ExpMonth.ToString() %></span></li>
                <li class="list-item"><label>Exp Year:</label><span><%= Model.ExpYear.ToString() %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.CreditCardShortFormMobileSite)
        {    
    %>
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Card Type:</label><span><%= Model.CardType %></span></li>
                <li class="list-item"><label>Card Last Four:</label><span><%= Model.LastFourDigitsCreditCardNumber %></span></li>
                <li class="list-item"><label>Exp Month:</label><span><%= Model.ExpMonth.ToString() %></span></li>
                <li class="list-item"><label>Exp Year:</label><span><%= Model.ExpYear.ToString() %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.CreditCardShortFormFullWebSite)
        {    
    %>
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Card Type:</label><span><%= Model.CardType %></span></li>
                <li class="list-item"><label>Card Last Four:</label><span><%= Model.LastFourDigitsCreditCardNumber %></span></li>
                <li class="list-item"><label>Exp Month:</label><span><%= Model.ExpMonth.ToString() %></span></li>
                <li class="list-item"><label>Exp Year:</label><span><%= Model.ExpYear.ToString() %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.Check)
        {    
    %>
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
                <li class="list-item no-border"><label>Phone Number:</label><span><%= Model.PhoneNumber %></span></li>
                <li class="list-item"><label>Email:</label><span><%= Model.EmailAddress %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>Bank Name:</label><span><%= Model.BankName %></span></li>
                <li class="list-item"><label>Bank State:</label><span><%= Model.BankState %></span></li>
                <li class="list-item"><label>Street Address:</label><span><%= Model.StreetAddress %></span></li>
                <li class="list-item"><label>City:</label><span><%= Model.City %></span></li>
                <li class="list-item no-border"><label>State:</label><span><%= Model.State %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item no-border"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
                <li class="list-item"><label>Account Last Four:</label><span><%= Model.BankAccountNumberLastFour %></span></li>
                <li class="list-item"><label>Routing Last Four:</label><span><%= Model.BankRoutingNumberLastFour %></span></li>
                <li class="list-item no-border"><label>Account Type:</label><span><%= Model.BankAccountType %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.PayPalLitle)
        {    
    %>

            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></asp:Literal></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></asp:Literal></span></li>
                <li class="list-item"><label>Country:</label><span><%= Model.Country %></asp:Literal></span></li>
                <li class="list-item"><label>Email:</label><span><%= Model.CardType %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Payer Status:</label><span><%= Model.PayerStatus %></span></li>
                <li class="list-item"><label>Paypal Token:</label><span><%= Model.PaypalToken %></span></li>
                <li class="list-item"><label>Paypal BillingAgreementID Last Four:</label><span><%= Model.BillingAgreementID %></span></li>
            </ul>

    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.Manual)
        {    
    %>

            <ul>
            <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
            <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
            </ul>

    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.ElectronicFundsTransfer)
        {    
    %>

            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
                <li class="list-item no-border"><label>Phone Number:</label><span><%= Model.PhoneNumber %></span></li>
                <li class="list-item"><label>Email:</label><span><%= Model.EmailAddress %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>Street Address:</label><span><%= Model.StreetAddress %></span></li>
                <li class="list-item no-border"><label>City:</label><span><%= Model.City %></span></li>
                <li class="list-item"><label>State:</label><span><%= Model.State %></span></li>
                <li class="list-item no-border"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                <li class="list-item"><label>Account Last Four:</label><span><%= Model.BankAccountNumberLastFour %></span></li>
                <li class="list-item"><label>Routing Last Four:</label><span><%= Model.BankRoutingNumberLastFour %></span></li>
                <li class="list-item no-border"><label>Account Type:</label><span><%= Model.BankAccountType %></span></li>
            </ul>

    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.PaymentReceived)
        {    
    %>

            <ul>
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.DebitCard)
        {    
    %>
            <ul class="transaction-details">
                <li class="list-item"><label>First Name:</label><span><%= Model.FirstName %></span></li>
                <li class="list-item"><label>Last Name:</label><span><%= Model.LastName %></span></li>
                <li class="list-item"><label>Phone Number:</label><span><%= Model.PhoneNumber %></span></li>
                <li class="list-item"><label>Street Address:</label><span><%= Model.StreetAddress %></span></li>
            </ul>
            <ul class="transaction-details">
                <li class="list-item"><label>City:</label><span><%= Model.City %></span></li>
                <li class="list-item"><label>State:</label><span><%= Model.State %></span></li>
                <li class="list-item"><label>Postal Code:</label><span><%= Model.PostalCode %></span></li>
                <li class="list-item"><label>Country:</label><span><%= Model.Country %></span></li>
                <li class="list-item"><label>Card Last Four:</label><span><%= Model.LastFourDigitsCreditCardNumber %></span></li>
            </ul>
            <ul class="transaction-details no-border">
                  <% if (Model.ShowGovernmentIssueID){%>
                     <li class="list-item"><label>ID:</label><span><%= Model.GovernmentIssuedID %></span></li>  
                 <%  } %>
                <li class="list-item"><label>Card Type:</label><span><%= Model.CardType %></span></li>
                <li class="list-item"><label>Exp Month:</label><span><%= Model.ExpMonth.ToString() %></span></li>
                <li class="list-item"><label>Exp Year:</label><span><%= Model.ExpYear.ToString() %></span></li>
            </ul>
    <% 
        }
        else if (Model.UsedForDefaultPaymentProfile == false && Model.PaymentType == Spark.Common.PurchaseService.PaymentType.None)
        {    
    %>

            <p>Billing Information Does Not Exist</p>            
                
    <% 
        }
    %>

   
    
    <% if (Model.PaymentType != Spark.Common.PurchaseService.PaymentType.None)
       { %> 
        
        <br />
        <input type="button" id="archiveSetting" class="button" value="Archive" />

        <div class="modal" id="promptArc" >
            <div style="text-align:center;width:700px; padding-left:50px;">
                    <b>Wait! <br /><br />
                    Are you sure you want to delete this member's credit card number from the system ?</b>
                    <ul  style="text-align:left; padding-left:50px;">
                        <li>
                            <br />
                            •	No further charges can be made without entering new credit card info.
                        </li>
                        <li>
                            <br />
                            •	If the current member has open renewals <u>make sure</u> to Terminate the billing as well.
                        </li>
                        <li >
                            <br />
                            •   This action cannot be undone.
                        </li>
                        <li>
                            <br />
                          <span style="padding-left:200px;"> <input type="button" class="archive-pmt-profile "  value="Confirm" />  <input type="button" class="cancel-overlay" onclick="closeOverLayPSS()" value="Cancel" /></span>
                        </li>
                    </ul>
                    <br />
                   
            </div> 
        </div>
        <input type="hidden" id="closeOverlayPSS" />

        <script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" />
        
        <script language="javascript" type="text/javascript">
            $(document).ready(function () {

                $('.archive-pmt-profile').click(function () {

                    ArchivePaymentProfile($('.pmt-profile-siteid-holder').val(), $('.pmt-profile-memberid-holder').val(), $('.pmt-profile-id-holder').val());

                    closeOverLayPSS();

                });


                $('#archiveSetting').click(function () {
                    $('#promptArc').lightbox_me({
                        centered: true
                    });
                    return false;
                });

                function closeOverLayPSS() {
                    $('#closeOverlayPSS').lightbox_me();
                    $('#closeOverlayPSS').trigger('close');
                }



            });
    </script>

      <% }%>

<% } %>