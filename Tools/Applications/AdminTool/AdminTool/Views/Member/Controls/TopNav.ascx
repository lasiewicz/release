﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.TopNav>" %>
<div id="management-nav" >
    <ul class="sf-menu">
    <!-- Navigation list items -->
        <li><a href="#">Member Profile</a>
	        <ul >
			    <li>
			        <a href="http://<%= Model.g.DefaultHost ?? "www" %>.jdate.com/Applications/MemberProfile/ViewProfile.aspx?MemberID=<%= Model.MemberID.ToString() %>&impmid=<%= Model.MemberID.ToString() %>&imppid=<%= Model.BrandID.ToString() %>&EntryPoint=8&LayoutTemplateID=<%= Model.GetTemplateID() %>&admin=true" class="pop-link">View Profile</a>
                </li>
                <li>
                    <a href="/Member/EditProfile/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>/<%=Model.MemberID.ToString() %>/1" class="pop-link">Change Profile</a>
                </li>
                <%if (Model.UseAdminToolPhotoUpload)
                  { %>
                <li><a href="/UploadPhotos/UploadMemberPhotos/<%=Model.MemberID.ToString()%>/<%=Model.MemberSiteInfo.SiteID.ToString()%>"
                    class="pop-link">Upload Photos</a> </li>
                <%}
                  else
                  { %>
                <li><a href="http://<%= Model.g.DefaultBedrockAdminHost %>/Applications/MemberProfile/MemberPhotoUpload.aspx?impmid=<%= Model.MemberID.ToString() %>&imppid=<%= Model.BrandID.ToString() %>&LayoutTemplateID=2"
                    class="pop-link">Upload Photos</a></li>
                <%} %>
            </ul>           
        </li>
        
        <li style="text-align:center; width:150px;"><a href="#">Content Approval</a>
	        <ul>
			    <li>
			        <a href="/Approval/ViewPhotosByMember/<%=Model.MemberID.ToString() %>/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>">Approve Photos</a>
			    </li>
                <li>
                    <a href="/Approval/ViewTextByMember/<%=Model.MemberID.ToString() %>/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>">Approve Free Text</a>
                </li>
                <li>
                    <a href="/Approval/ViewQATextByMember/<%=Model.MemberID.ToString() %>/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>">Approve Q & A</a>
                </li>
                <li>
                    <a href="/Approval/ViewFacebookLikesByMember/<%=Model.MemberID.ToString() %>/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>">Approve Facebook Likes</a>
                </li>
            </ul>           
        </li>
        <li style="text-align:center; width:150px;"><a href="#">Matches</a>
	        <ul>
			    <li>
                    <a href="/Tools/ViewBulkMailSimulator/<%=Model.MemberID.ToString() %>/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>">MatchMail</a>
                </li>
                <li>
                    <a href="/Member/EditSearchPreferences/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>/<%=Model.MemberID.ToString() %>/" class="pop-link">Search Preferences</a>
                </li>
            </ul>           
        </li>
        <li style="width:157px; text-align:center;"><a href="#">Member Settings</a> 
	        <ul>
	            <li>
	                <a id="profileDisplaySettings" rel="#promptProfileDisplaySettings" style="color:Gray;">Profile Display Settings</a>
	            </li>
			    <li>
			        <a id="premiumservicesettings" rel="#promptPSS" style="color:Gray;">Premium Svc Settings</a>
			    </li>
                 <li>
                    <a id="offSite" rel="#prompt" style="color:Gray;"> Off-Site Notifications </a> 
                 </li>
            </ul>           
        </li>
        <% if (Model.GetConnectSiteName() != "")
           {%>
        <li><a href="https://www.securemingle.com/admin/sites/<%= Model.GetConnectSiteName() %>/support/send_message.html?search=<%= Model.Email %>"
            target="_blank">Send Email</a></li>
        <% } %>
        
        <li>
            <a href="/Member/Print/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>/<%=Model.MemberID.ToString() %>/" class="pop-link">Charge Back</a>
        </li>
    </ul>
</div>
