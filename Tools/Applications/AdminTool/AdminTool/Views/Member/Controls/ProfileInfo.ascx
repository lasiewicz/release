﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.ProfileInfo>" %>
<h2>Profile Information</h2>

<ul>
<li class="list-item"><label>First Name:</label> <span><%=Html.TextBox("FirstName", null, new{maxlength="100"}) %></span></li>
<li class="list-item"><label>Last Name:</label>  <span><%=Html.TextBox("LastName", null, new{maxlength="100"}) %></span></li>
<li class="list-item"><label>Location:</label>  <span><%=Model.RegionDisplay %></span></li>
<li class="list-item"><label>Birthdate:</label> <span><%=Html.TextBox("Birthdate") %></span></li>
<li class="list-item"><label>Gender:</label>  <span><%=Html.DropDownList("DDLGender", GeneralHelper.GetGenderListItems(Model.Gender)) %></span></li>
<li class="list-item"><label>Seeking Gender:</label>  <span><%=Html.DropDownList("DDLSeekingGender", GeneralHelper.GetSeekingGenderListItems(Model.SeekingGender)) %></span></li>
</ul>
<%if (Model.CheckUIPermission(Enums.Operations.ProfileInformationSave)) { %>
    <input type="button" value="Save Changes" id="btn-save-profile-info" class="button" onclick="UpdateProfileInfo(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" />   
<% } %>

<script type="text/javascript">
    $(function () {
        var $datePicker = $("#Birthdate"),
            minYear = '1900',
            minAge = 18,
            oldValue = $datePicker.val().split(' '); // mm/dd/yyyy ...

        $datePicker.datepicker({
            changeMonth: true,
            changeYear: true,
            minDate: new Date(minYear),
            maxDate: '-18y',
            yearRange: minYear + ':-18', // set dropdown year list from minYear to current year - 18
            onClose: function (date) {
                var givenDate = $(this).datepicker('getDate'),
                    givenValue = $datePicker.val().split(' '),
                    givenValueDate = givenValue[0].split('/'), // mm/dd/yyyy ...
                    year = givenValueDate[2]; 

                if(year.length <= 2){
                    // if given year value is 'yy' or 'y' convert to 'yyyy'
                    var prepix = year.length > 1 ? '19' : '190';
                     
                    givenValueDate[2] = prepix + year;

                    $datePicker.datepicker('setDate', givenValueDate[0] + '/' + givenValueDate[1] + '/' + givenValueDate[2]);
                    
                    givenDate = $(this).datepicker('getDate'),
                    givenValue = $datePicker.val().split(' '),
                    givenValueDate = givenValue[0].split('/'),
                    year = givenValueDate[2];
                }

                if(year < minYear){
                    $datePicker.datepicker('setDate', givenValueDate[0] + '/' + givenValueDate[1] + '/' + minYear);
                }

                if (getAge(givenValue[0]) < 18) {
                    $datePicker.datepicker('setDate', oldValue[0]);
                    alertMessage('Please Select an Age Between 18 and 99');
                }
            }
        });
    });
	</script>

