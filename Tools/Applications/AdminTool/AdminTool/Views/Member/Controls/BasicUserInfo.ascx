﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.BasicUserInfo>" %>


<style>
    #oChangedEmailHistory div,span{
        color:#000000;
        font-size:12px;
    }

    #oChangedEmailHistory span{
        color:#000000;
        font-size:14px;
    }
</style>

    <div class="basic-info-left">Name:</div><div class="basic-info-right"><%= Model.TruncateString(Model.FirstName, 30) + " " + Model.TruncateString(Model.LastName, 30) %></div>
    <div class="basic-info-left">Username:</div><div class="basic-info-right"><%= Model.UserName %>&nbsp;</div>
    <div class="basic-info-left">MemberID:</div><div class="basic-info-right"><%=Model.MemberID.ToString() %></div>    
    <div class="basic-info-left">Administrator:</div><div class="basic-info-right"><%= (Model.IsAdmin ? "Yes" : "No") %></div>
    <div class="basic-info-left">Profile Status:</div><div class="basic-info-right"><span id="profile-status"></span></div>
    <script type="text/javascript">
        $(document).ready(function() {
            GetProfileStatusDisplayText('<%=Model.ProfileStatus.ToString()%>', '<%=Model.SuspendReason %>');
        });
    </script>

    <div style="float:left">Login:<%=Html.TextBox("Email", Model.Email, new{@class="basic-login"})%></div>
         
           
    <%if (Model.CheckUIPermission(AdminTool.Models.Enums.Operations.EmailEdit))
      { %>
        <input type="button" value="Update" class="button" id="btnUpdateEmail"  onclick="UpdateEmail(event, $(this),  <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" />

    <%} %>


<div class="basic-links">
    
    <%if (Model.CheckUIPermission(AdminTool.Models.Enums.Operations.PasswordSend))
      { %>
    <a href="#" id="send-pass" class="trigger-overlay" >[Send Password Reset]</a>
    <%} %>
    
    <%if (Model.CheckUIPermission(Enums.Operations.StealthLogin))
      { %>
        <a href="#" onclick="GenerateStealthLoginLink(event,<%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>,<%=Model.g.AdminID.ToString()%>)">[Enter Stealth Login]</a>
      <%}%>
 
    <a href="#" id="changeEmailHistory"  >[Changed Email History]</a>

    <a href="#" class="change-norbert"><%=Model.IsNorbert ? "[Norbert]" : "[Bedrock]"%></a>
       
</div>



<div id="send-pass-over" class="main-gray-boxes overlay">
                 
                    <a href="" class="close-x"><img src="/images/close.png" width="15" /></a>
                    <p class="send-content">Send Password to:</p>
                    
                    <p class="send-email"><%=Model.Email %></p>
                    
                    <input type="button" value="Send" class="button" onclick="SendPassword(event,<%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" /> <input type="button" value="Cancel" class="cancel-overlay button" />
</div>


<div class="modal" id="oChangedEmailHistory" >
    <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverLay()"><b>[X]</b></a></div>
    <span><b>Member Changed Email History</b><br /><br /></span>
    <div style="width:100%;">
        <table id="admin-notes-table">
            <tr>
                <td><b>Member Email Address</b></td>
                
                <td><b>Since</b></td>
                 
                <td><b>Updated By</b></td>
            </tr>
        <%foreach(var le in Model.LastEmails) {%>
             <tr>        
                 <td>
                     <%= le.LastEmailAddress%>
                 </td>
                  
                  <td>
                    <%= le.LastEmailDate %>
                 </td>
                 
                  <td>
                     <% if (le.AdminMemberId > 0) { %>
                         Admin
                        <%} else {%>
                        User
                      <%} %>
                 </td>
                 
            </tr>
         <%}%>
        </table>
    </div>
</div> 

<input type="hidden" id="cChangedEmailHistory" />

<div class="modal" id="oChangeNorbert" >
    <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeChangeNorbert()"><b>[X]</b></a></div>
    <span><b>Update Norbert Beta Opt-in</b><br /><br /></span>
    <table cellpadding="10px" cellspacing="20px" width="100%" >
        <tr>
            <td align="right"> 
                <span style="padding:10px;width:100px;"> Show full website using:</span>
            </td>
            <td>
                <div id="divShowNorbert" style="width:100%;">
                    <span style="padding:10px;">    
                    <%= Html.RadioButtonFor(model => model.IsNorbert, "true") %><span>Norbert</span> 
                    </span>

                    <span style="padding:10px;"> 
                     <%= Html.RadioButtonFor(model => model.IsNorbert, "false")%><span>Bedrock</span> 
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <br />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <span style="padding:10px;"><input id="update-norbert-setting" type="submit" class="button" value="Save" onclick="UpdateNorbertSetting(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)"/>
                <input type="button" class="button"  onclick="closeChangeNorbert()" value="Cancel" />
                </span>
            </td>
        </tr>
    </table>
</div> 
<input type="hidden" id="cChangeNorbert" />

<script type="text/javascript">

    //changed email history
    $("#oChangedEmailHistory").css('color', '#ffffff');
    $("#oChangedEmailHistory").css('cursor', 'pointer');


    $('#changeEmailHistory').click(function() {

        $('#oChangedEmailHistory').lightbox_me({
            centered: true
        });
        return false;
    });

    function closeOverLay() {
        $('#cChangedEmailHistory').lightbox_me();
        $('#cChangedEmailHistory').trigger('close');
    }

    //change norbert opt-in
    $('.change-norbert').click(function() {

        $('#oChangeNorbert').lightbox_me({
            centered: true
        });
        return false;
    });

    function closeChangeNorbert() {
        $('#cChangeNorbert').lightbox_me();
        $('#cChangeNorbert').trigger('close');
    }
    
</script>

  
  
  



