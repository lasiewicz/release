﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.DateTimeStampInfo>" %>
<%@ Import Namespace="Matchnet.Member.ValueObjects" %>

<div id="DateTimeStampInfoControl">
<ul>
    <li>Registered: <%= Model.GetShortDisplayDate(Model.RegisteredDate) %><%if (Model.IsMobileRegistration)
                                                                            {%> (M)<% } %></li>
    <li>Last Update: <%= Model.GetDisplayDate(Model.LastUpdateDate)   %></li>
    <li>Last Login: <%= Model.GetDisplayDate(Model.LastLoginDate) %></li>
    <li>Last 5 Logins:</li>
    <% if(Model.LastLogons != null) { %>
    <% foreach (LastLogon lastLogon in Model.LastLogons) { %>
      <li><%= Model.GetLastLogonDisplay(lastLogon)%></li>
    <%}%>
    <%}%>

    <li>
        <a href="https://www.securemingle.com/admin/sites/<%= Model.SiteNameForLink%>/admin_login_history.html?memberid=<%= Model.MemberID.ToString()%>&email=<%= Url.Encode(Model.MemberEmail)%>" target="_blank" id=" " rel="" class="button">Login History Details</a>
    </li>
</ul>
</div>