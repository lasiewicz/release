﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.ProfileDisplaySettings>" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<style>
    fieldset {
        border: 1px solid gray;
        }
</style>
<div class="modal" id="promptProfileDisplaySettings" >
    <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverLayProfileDisplaySettings()"><b>[X]</b></a></div>
    <table cellpadding="10px" cellspacing="20px" width="100%" >
        <tr>
            <td align="right"> 
                <span style="padding:10px;width:100px;"> Show when member is online:</span>
            </td>
            <td>
                <div id="divProfileSettingsShowOnline" style="width:100%;">
                    <span style="padding:10px;">    
                    <%= Html.RadioButtonFor(model => model.ShowOnline, "true") %><span>Show</span> 
                    </span>

                    <span style="padding:10px;"> 
                     <%= Html.RadioButtonFor(model => model.ShowOnline, "false")%><span>Hide</span> 
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <br />
            </td>
        </tr>
        <tr>
            <td align="right"> 
                    <span style="padding:10px;width:100px;">Show in searches:</span>
            </td>
            <td>
                <div id="divProfileSettingsShowinSearches" style="width:100%;">
                    <span style="padding:10px;">
                        <%= Html.RadioButtonFor(model => model.ShowInSearches, "true") %><span>Show</span>
                    </span>

                    <span style="padding:10px;"> 
                        <%= Html.RadioButtonFor(model => model.ShowInSearches, "false")%><span>Hide</span>
                    </span>
                
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <br />
            </td>
        </tr>
        <tr>
            <td align="right"> 
                <span style="padding:10px;">Show when member views or hotlists others:</span>
            </td>
            <td>
                <div id="divProfileSettingsShowActivityTrail" style="width:100%;">
                    <span style="padding:10px;">
                        <%= Html.RadioButtonFor(model => model.ShowActivityTrail, "true") %><span>Show</span> 
                    </span>
                    
                    <span style="padding:10px;"> 
                        <%= Html.RadioButtonFor(model => model.ShowActivityTrail, "false")%><span>Hide</span>       
                    </span>
               </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <br />
            </td>
        </tr>
        <tr>
            <td align="right"> 
                <span style="padding:10px;width:100px;">Display photos to everyone: </span>
            </td>
            <td>
                <div id="divProfileSettingsShowPhotosToNonMembers">
                    
                    <span style="padding:10px;">
                        <%= Html.RadioButtonFor(model => model.ShowPhotosToNonMembers, "true") %><span>Show to members and non-members</span> 
                    </span>     
                      <br />
                    <span style="padding:10px;">
                        <%= Html.RadioButtonFor(model => model.ShowPhotosToNonMembers, "false")%><span>Show to members only</span> 
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <br />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <span style="padding:10px;"><input id="update-profile-display-settings" type="submit" class="button" value="Save" onclick="UpdateProfileDisplaySettings(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)"/>
                <input type="button" class="button"  onclick="closeOverLayProfileDisplaySettings()" value="Cancel" />
                </span>
            </td>
        </tr>
    </table>
</div>
<input type="hidden" id="closeOverlayPDS" />    

<script type="text/javascript">

    $("#profileDisplaySettings").css('color', '#ffffff');
    $("#profileDisplaySettings").css('cursor', 'pointer');


    $('#profileDisplaySettings').click(function () {
        $('#promptProfileDisplaySettings').lightbox_me({
            centered: true
        });
        return false;
    });

    function closeOverLayProfileDisplaySettings() {

        $('#closeOverlayPDS').lightbox_me();
        $('#closeOverlayPDS').trigger('close');
    }
    
</script>
