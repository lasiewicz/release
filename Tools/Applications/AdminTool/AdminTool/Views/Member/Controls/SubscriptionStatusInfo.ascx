﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.SubscriptionStatusInfo>" %>


<div id="sub-status-info">
<ul>
<li>Subscription Status: <b><%=Model.SubscriberStatus %></b></li>
<li>End of Term: <b><%=Model.RenewalDate.ToString()%></b></li> 
<% if (Model.HasValidSubsciption)
   { %>
   <li id="renewal-status" class="<%=((int)Model.MemberSiteInfo.SiteID).ToString()%> <%=Model.MemberID.ToString()%>)">Renewal Status:
    <% if (Model.ActiveRenewal)
       { %>
         <b>Open</b>
    <% }
       else
       {%>
        <b>No Renewal</b>
    <% } %>
    </li>
    <li>Renewal Amount: <b><%=string.Format("{0:C}", Model.RenewalAmount) %></b></li>
    <li>Bundled Premium Services: <b><%=Model.BundledPremiumServices %></b></li>
    <li>ALC Premium Services: <b><%=Model.ALCPremiumServices %></b></li>
    <!--<li>Active Premium Services: <%=Model.ActivePremiumServices %></li>-->
<% }
   else
   {%>  
     <li>Renewal Status: N/A</li>
     <li>Renewal Amount: N/A</li>
     <li>Bundled Premium Services: N/A</li>
     <li>ALC Premium Services: N/A</li>
     <!--<li>Active Premium Services: <%=Model.ActivePremiumServices %></li>-->
<% } %>
</ul>
</div>

<% 
    string disableElementTextForPausedSubscription = String.Empty;
    string showIAP = String.Empty;

    if (Model.HasPausedSubscription) 
    {
        disableElementTextForPausedSubscription = "style=visibility:hidden;";
    }
    else if (Model.IsIAP)
    {
        disableElementTextForPausedSubscription = "disabled=true style='opacity: 0.6;position:relative; cursor:none;'";
        showIAP = "style='position:absolute;height:250px;font-size:25px;font-weight:bold;text-align:center;'";
    }
    else if(!Model.IsIAP)
    {
        showIAP = "style='display:none;'";
    }
%>

<div id="sub-status-buttons" <%= disableElementTextForPausedSubscription %>>
    <%if (Model.CheckUIPermission(Enums.Operations.BuySubscription))
      { %>
        <a class="button" <%= disableElementTextForPausedSubscription %> href="javascript:var newwin = window.open('/Subscription/Credit?memberID=<%= Model.MemberID.ToString() %>&siteID=<%=((int)Model.MemberSiteInfo.SiteID).ToString()%>&brandID=<%= Model.BrandID.ToString() %>'); if(newwin) { newwin.focus(); }">Buy Credit</a>
        <a id="btn-buy-subscription-check" <%= disableElementTextForPausedSubscription %> class="button" href="javascript:var newwin = window.open('http://credittool.matchnet.com/ManualPayment?memberID=<%= Model.MemberID.ToString() %>&siteID=<%=((int)Model.MemberSiteInfo.SiteID).ToString()%>'); if(newwin) { newwin.focus(); }">Manual Payment</a>
        <a id="btn-buy-subscription-admin" <%= disableElementTextForPausedSubscription %> class="button" href="javascript:var newwin = window.open('/Subscription/AdminOnly?memberID=<%= Model.MemberID.ToString() %>&siteID=<%=((int)Model.MemberSiteInfo.SiteID).ToString()%>&brandID=<%= Model.BrandID.ToString() %>'); if(newwin) { newwin.focus(); }">Buy Admin Only</a>
    <% } %>
    <%if (Model.CheckUIPermission(Enums.Operations.BuyAlaCarte))
      { %>
        <a id="btn-ala-carte" <%= disableElementTextForPausedSubscription %> class="button" href="javascript:var newwin = window.open('/Subscription/ALaCarte?memberID=<%= Model.MemberID.ToString() %>&siteID=<%=((int)Model.MemberSiteInfo.SiteID).ToString()%>&brandID=<%= Model.BrandID.ToString() %>'); if(newwin) { newwin.focus(); }">A la Carte</a>
    <% } %>
    <%if (Model.CheckUIPermission(Enums.Operations.AdminAdjust))
      { %>
         <a id="adminAdjust" <%= disableElementTextForPausedSubscription %> rel="#promptAdminAdjust" class="button" >Admin Adjust</a>
    <% } %>
    <%if (Model.CheckUIPermission(Enums.Operations.ChangePaymentType))
      { %>
        <a id="btn-change-paymenttype" <%= disableElementTextForPausedSubscription %> class="button" href="javascript:var newwin = window.open('/Subscription/PaymentProfile?memberID=<%= Model.MemberID.ToString() %>&siteID=<%=((int)Model.MemberSiteInfo.SiteID).ToString()%>&brandID=<%= Model.BrandID.ToString() %>'); if(newwin) { newwin.focus(); }">Change Payment Type</a>
    <% } %>
    <%if (Model.CheckUIPermission(Enums.Operations.AdminAdjust))
      { %>
          <a id="updateRenewal" rel="#promptRenewal" class="button">Update Renewal</a>
    <% } %>

    <div <%=showIAP%>>
    Subscription Adjustments Unavailable
        <br /><br />
    User subscribed
    through Apple
         store
    </div>

</div>



<%if (Model.CheckUIPermission(Enums.Operations.PasswordEdit))
  { %>
    <div  >
    <%if (Model.SubscriptionLastInitialPurchaseDate.HasValue)
      {%>
        <input type="text" id="SubscriptionLastInitialPurchaseDate" value="<%=Model.SubscriptionLastInitialPurchaseDate.Value.ToShortDateString() %>" <%= disableElementTextForPausedSubscription %> />
    <%}
      else
      { %>
        <input type="text" id="SubscriptionLastInitialPurchaseDate" <%= disableElementTextForPausedSubscription %> />
    <%} %>
        <br />
        <input type="button"   <%= disableElementTextForPausedSubscription %> class="button-mini" value="Update Last Initial Purchase Date" onclick="UpdateSubscriptionLastInitialPurchaseDate(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)"/>
    </div>
    <script type="text/javascript">
        $(function() {
            $('#SubscriptionLastInitialPurchaseDate').datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
<%} %>

<!-- NEED TO CHANGE ACTIVE DIRECTORY PERMISSION -->

<%if(!Model.IsIAP) { %>
<%if (Model.CheckUIPermission(Enums.Operations.PasswordEdit))
  { %>
    
    <% 
      string description = String.Empty;
      
      if (Model.HasPausedSubscription) 
      { 
        description = "Resume Subscription";
      }     
      else
      {
        description = "Pause Subscription";                
      }

      string disableElementTextForIsPausedSubscriptionAllowed = String.Empty;

      if (!Model.IsPauseAllowed
          && !Model.HasPausedSubscription)
      {
         disableElementTextForIsPausedSubscriptionAllowed = "disabled=true";
      }                
    %>
    <div style="width:140px;float:left;margin-top:10px;">
        <input type="button" id="pauseSubscription" <%= disableElementTextForIsPausedSubscriptionAllowed %> rel="#promptPauseSubscription" class="button" value="<%= description %>" />
    </div>
<% } %>
<% } %>

<div id="reopen-renewals-over" class="main-gray-boxes overlay">
        <a href="" class="close-x"><img src="/images/close.png" width="15" /></a>
        <p>
        This will turn on autorenewal and/or reinstate this member's subscription.<br />
        <% if (Model.HasHighlightALCService || Model.HasSpotlightALCService || Model.HasAllAccessALCService || Model.HasReadReceiptALCService)
           {%>
        
            <p>Checking the following box(es) will turn on autorenewal for this member's a la carte premium service plans. <br />
            Profile Highlight: <input type="checkbox" id="chkReopenHighlight" disabled="<%=Model.HasHighlightALCService.ToString() %>"/><br />
            Member Spotlight: <input type="checkbox" id="chkReopenSpotlight" disabled="<%=Model.HasSpotlightALCService.ToString() %>"/><br />
            All Access: <input type="checkbox" id="chkReopenAllAccess" disabled="<%=Model.HasAllAccessALCService.ToString() %>"/><br />
            Read Receipts: <input type="checkbox" id="chkReopenReadReceipt" disabled="<%=Model.HasReadReceiptALCService.ToString() %>"/>
            </p>
        <% } %>
        <div style="width:140px;float:left"><input type="button" class="button" onclick="ReopenAutoRenewal(event, $(this), <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" value="Save" /> &nbsp;</div>
        <input type="button" class="button cancel-overlay" value="Cancel" />
</div>

<div id="turnoff-renewal-over" class="main-gray-boxes overlay">
        <a href="" class="close-x"><img src="/images/close.png" width="15" /></a>
        <p>Click "Confirm" to turn off auto-renewal for this account.<br />
            Any premium services will also no longer renew.</p>
        <div style="width:140px;float:left"><input type="button" class="button" onclick="EndAutoRenewal(event, $(this), true, <%=((int)Model.MemberSiteInfo.SiteID).ToString()%>,<%=Model.MemberID.ToString()%>)" value="Confirm" /> &nbsp;</div>
        <input type="button" class="button cancel-overlay" value="Cancel" />
</div>
 

