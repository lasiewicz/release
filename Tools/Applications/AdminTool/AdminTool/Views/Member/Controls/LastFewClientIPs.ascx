﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.LastFewClientIPs>" %>
<script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
<style>
    fieldset {
        border: 1px solid gray;
        }
        /* ------------------
 styling for the tables 
   ------------------   */


#hor-minimalist-b
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	background: #fff;
	margin: 45px;
	width: 480px;
	border-collapse: collapse;
	text-align: left;
}
#hor-minimalist-b th
{
	font-size: 14px;
	font-weight: normal;
	color: #039;
	padding: 10px 8px;
	border-bottom: 2px solid #6678b1;
	text-align:left;
}
#hor-minimalist-b td
{
	border-bottom: 1px solid #ccc;
	color: #669;
	padding: 6px 8px;
}
#hor-minimalist-b tbody tr:hover td
{
	color: #009;
}

.hor-minimalist-label
{
     
	color: #669;
	padding: 6px 8px;
	font-size: 12px;
}

.hor-minimalist-header
{
    margin: 45px;
    font-size: 16px;
	font-weight: normal;
	color: #039;
	padding: 10px 8px;
	text-align:left;
}


</style>

<div class="modal" id="promptLastFewClientIPs" >
    <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverlayLastFewClientIPs()"><b>[X]</b></a></div>
 

     <% if(Model.SessionTrackings != null) { %>

       <span class="hor-minimalist-header">Last 5 Recorded IP Addresses <span class="hor-minimalist-label">(May not match-up exactly to Last 5 Logins)</span> </span> 
      
      

        <table id="hor-minimalist-b">
             <thead>
    	    <tr>
        	    <th scope="col">Session Started</th>
                <th scope="col">ClientIP</th>
            </tr>
            </thead>
            <tbody>
            <%foreach(var st in Model.SessionTrackings) {%>
            <tr>
                <td><%= st.SessionStart.ToString()%></td>
                <td><%= st.ClientIP %></td>
            </tr>
            <%}%>
             </tbody>
        </table>
    <%}%>

</div>

<input type="hidden" id="closeOverlayLastFewClientIPs" />
<script type="text/javascript">
    $("#viewLastFewClientIPs").css('cursor', 'pointer');

    $('#viewLastFewClientIPs').click(function () {
        $('#promptLastFewClientIPs').lightbox_me({
            centered: true
        });
        return false;
    });

    function closeOverlayLastFewClientIPs() {
        $('#closeOverlayLastFewClientIPs').lightbox_me();
        $('#closeOverlayLastFewClientIPs').trigger('close');
    }
</script>