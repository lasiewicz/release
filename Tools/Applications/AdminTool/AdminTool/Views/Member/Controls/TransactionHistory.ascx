﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Member.TransactionHistory>" %>
<script src="/Scripts/jquery.ellipsis.js" type="text/javascript" ></script>               
<style type="text/css">
.ellipsis {
    white-space: nowrap;
    overflow: hidden;
}

.ellipsis.multiline {
    white-space: normal;
    word-wrap: break-word;
    width: 160px;
    height: 63px;
}
</style>
       
<div class="section-title transaction">
    <h1>Transaction History</h1>
</div>
    
<div id="transaction-subcontainer"> 

<% foreach (AdminTool.Models.AccountHistory transaction in Model.AllAccountHistory) { %>
     <div class="transaction-history-item tabs-bottom">
            <ul>
                <li><a href="#tabs1-<%= transaction.OrderID %>">Details</a></li>
                <% if (transaction.TransactionType == TransactionType.GiftRedeem && transaction.RedeemedGift != null)
                   { %>
                    <li><a href="/MemberAPI/BillingInfoForGiftRedemption/<%= transaction.RedeemedGift.Code %>/<%= (int)Model.MemberSiteInfo.SiteID %>/<%= Model.MemberID %>"><span>Billing Info</span></a></li>
                <% }
                   else if (transaction.TransactionType == TransactionType.Authorization)
                   { %>
                    <li><a href="/MemberAPI/BillingInfoForFreeTrial/<%= transaction.UserPaymentGUID %>/<%= (int)Model.MemberSiteInfo.SiteID %>/<%= Model.MemberID %>"><span>Billing Info</span></a></li>
                <% }
                   else
                   { %>
                    <li><a href="/MemberAPI/BillingInfo/<%= transaction.OrderID %>/<%= (int)Model.MemberSiteInfo.SiteID %>/<%= Model.MemberID %>"><span>Billing Info</span></a></li>
                <% } %>
                
                <li><a href="#tabs3-<%= transaction.OrderID %>">Credit</a></li>
            </ul>
    
        <div class="transaction-header <%= transaction.AccountHistoryType %> <%= transaction.OrderStatus %>">
            <div class="transaction-date"><%= Convert.ToString(transaction.InsertDateInPST) %></div>                    
            <% 
                if (String.IsNullOrEmpty(transaction.AdminDisplay))
                { 
            %>
                <div class="admin-display">&nbsp;</div>
            <%
                }
                else
                {                    
            %>                       
                <div class="admin-display">admin:<%= transaction.AdminDisplay %></div>
            <%
                }                 
            %>                                
            <div class="transaction-initial"><%= transaction.TransactionTypeDisplay %>:$<%= String.Format("{0:#0.00}", transaction.TotalAmount)%></div>
        </div>

        <div id="tabs-<%= transaction.OrderID %>" class="transaction-details-container"  sizcache="1" sizset="161">

            
            <div id="tabs1-<%= transaction.OrderID %>" class="transaction-details-subcontainer ui-tabs-panel">
                <% if (transaction.PurchasedGift != null) { %>
                     <div id="giftInfo_<%= transaction.PurchasedGift.Code %>" class="giftInfo" >
                        <div style="float:right; width:5%;padding-top:2px;">
                            <span class="regularLink"><b>[X]</b></span>
                        </div>
                        <div style="float:left" >
                            <ul class="transaction-details">
                                <li>Gift Type: <%= transaction.PurchasedGift.GiftType %></li>
                                <li>Begin Valid Date: <%= transaction.PurchasedGift.BeginValidDate.ToShortDateString()%></li>
                                <li>ExpirationDate: <%= transaction.PurchasedGift.ExpirationDate.ToShortDateString()%></li>
                                <li>IsValid: <%= transaction.PurchasedGift.IsValid.ToYesNoString() %></li>
                                <li>Status: <%= transaction.PurchasedGift.Status%></li>
                                <li><a id="mangepurchased" href="/SubAdmin/GiftSearch/<%= transaction.PurchasedGift.Code %>">Manage</a></li>
                            </ul>
                            <ul class="transaction-details">
                                <% if (transaction.PurchasedGift.GiftRedemption != null && transaction.PurchasedGift.GiftRedemption.CustomerID > 0)
                                   { %>
                                    <li>RedeemerID: <a id="A1" href="/Member/View/<%= transaction.PurchasedGift.GiftRedemption.CallingSystemID %>/<%= transaction.PurchasedGift.GiftRedemption.CustomerID%>"><%= transaction.PurchasedGift.GiftRedemption.CustomerID%></a><%= int.MinValue %></li>
                                <% } %>
                            </ul>
                        </div>
                                                                   
                    </div>
                    <script language="javascript" type="text/javascript">
                        $(document).ready(function () {
                            $("#gift_<%= transaction.PurchasedGift.Code %>").click(function () {
                                $("#giftInfo_<%= transaction.PurchasedGift.Code %>").show("slide", { direction: "top" }, 500);
                            });

                            $("#giftInfo_<%= transaction.PurchasedGift.Code %>").click(function () {
                                $(this).hide("slide", { direction: "bottom" }, 500);
                            });

                        });
                    </script>
                <% } %>
                <% if (transaction.RedeemedGift != null) { %>
                     <div id="giftInfo_<%= transaction.RedeemedGift.Code %>" class="giftInfo" >
                        <div style="float:right; width:5%;padding-top:2px;">
                            <span class="regularLink"><b>[X]</b></span>
                        </div>
                        <div style="float:left" >
                            <ul class="transaction-details">
                                <li>Gift Type: <%= transaction.RedeemedGift.GiftType %></li>
                                <li><a id="A2" href="/SubAdmin/GiftSearch/<%= transaction.RedeemedGift.Code %>">Manage</a></li>
                            </ul>
                            <ul class="transaction-details">
                                   <li>Purchaser: <a id="A1" href="/Member/View/<%= transaction.RedeemedGift.CallingSystemID%>/<%= transaction.RedeemedGift.CustomerID%>"><%= transaction.RedeemedGift.CustomerID%></a><%= int.MinValue %></li>
                            </ul>
                        </div>
                                                                   
                    </div>
                    <script language="javascript" type="text/javascript">
                        $(document).ready(function () {
                            $("#gift_<%= transaction.RedeemedGift.Code %>").click(function () {
                                $("#giftInfo_<%= transaction.RedeemedGift.Code %>").show("slide", { direction: "top" }, 500);
                            });

                            $("#giftInfo_<%= transaction.RedeemedGift.Code %>").click(function () {
                                $(this).hide("slide", { direction: "bottom" }, 500);
                            });

                        });
                    </script>
                <% } %>
                    <ul class="transaction-details">
                        <% if (transaction.TransactionType != TransactionType.Adjustment)
                           { %>
                            <li>Initial Cost: $<%= String.Format("{0:#0.00}", transaction.TotalAmount)%></li>

                            <% 
                            if (transaction.Duration == 0)
                            {
                            %>
                                <li>Initial Duration: <%= transaction.Duration%></li>
                            <%  
                            }
                            else
                            {
                            %>
                                <li>Initial Duration: <%= transaction.Duration%>&nbsp;<%= transaction.DurationTypeDisplay %></li>
                            <% 
                            }        
                            %>

                            <% 
                            if (transaction.PrimaryPlan != null)
                            {
                            %>
                                <li>Renewal Rate: $<%= String.Format("{0:#0.00}", transaction.PrimaryPlan.RenewCost)%></li>
                                <% 
                            if (transaction.PrimaryPlan.RenewDuration == 0)
                            {
                                %>
                                    <li>Renewal Duration: <%= transaction.PrimaryPlan.RenewDuration%></li>
                                <%  
                            }
                            else
                            {
                                %>
                                    <li>Renewal Duration: <%= transaction.PrimaryPlan.RenewDuration%>&nbsp;<%= transaction.RenewalDurationTypeDisplay%></li>
                                <% 
                            }        
                                %>
                            <%  
                            }
                            else
                            {
                            %>
                                <li>Renewal Rate: N/A</li>
                            <% 
                            }        
                            %>
                        <%}
                       else
                       { %>
                           <li>Duration: <%= transaction.Duration%> <%=transaction.DurationType.ToString() %></li>
                        <%} %>

                        <li>Includes: <%= transaction.IncludedAlaCarteItemsDisplay %></li>
                        <% if (transaction.PurchasedGift != null) { %>
                            <li>
                                Gift Code: <span id="gift_<%= transaction.PurchasedGift.Code %>" class="regularLink" ><%= transaction.PurchasedGift.Code %></span>
                            </li>
                        <% } %>
                        <%if (transaction.RedeemedGift != null) { %>
                            <li>
                                Gift Code: <span id="gift_<%= transaction.RedeemedGift.Code %>" class="regularLink" ><%= transaction.RedeemedGift.Code %></span>
                            </li>
                        <% } %> 
                        <li>&nbsp;</li> 
                    </ul>
                    <ul class="transaction-details-no-border">
                        <%if (transaction.TransactionType != TransactionType.Adjustment)
                          { %>
                        <li>Plan ID: <%= transaction.PrimaryPackageID%></li>
                        <li>PromoID: <%= transaction.PromoID < 0 ? "" : Convert.ToString(transaction.PromoID)%></li>
                        <li  class="ellipsis multiline">Promo Description: <%= transaction.PromoDescription %> </li>
                        <%} %>             
                    </ul>
              
                    <ul class="transaction-details-left-border">
                        <li>Status: <%= transaction.OrderStatusDisplay %></li>
                        <li>Payment Type: <%= transaction.PaymentTypeDisplay %></li>
                        <li>Account: <%= transaction.LastFourAccountNumber %></li>
                        <li>Confirmation: <%= transaction.OrderID %></li>
                        <li>Auth ID: <%= transaction.AuthorizationPaymentID %></li>
                        <li>Charge ID: <%= transaction.BillingPaymentID %></li>
                    </ul>              
            </div>
                   
            <div id="tabs3-<%= transaction.OrderID %>" class="transaction-details-subcontainer ui-tabs-panel">
            <br />
            <a style="cursor:pointer; text-decoration:underline;" onclick="javascript:window.open('<%=transaction.CreditQueueURL %>');" value="" >Add to credit queue</a>
            </div>
        </div>        
         
    </div>    
<% } %>

    </div>
