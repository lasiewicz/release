﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master"
    Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Member.MemberData>" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="TitleContent" runat="server">
    <%--Member Title--%>
    Spark Networks Web Admin - Account Manager
</asp:Content>
<asp:Content ID="ContentHead" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <script src="/Scripts/mustache.js" type="text/javascript" ></script>    
</asp:Content>
<asp:Content ID="ContentHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("Controls/TopNav", Model.TopNav); %>
</asp:Content>
<asp:Content ID="ContentLeft" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/MemberLeft", Model.MemberLeft); %>
</asp:Content>
<asp:Content ID="ContentMain" ContentPlaceHolderID="MainContent" runat="server">
<!-- Content Area Right -->
<div id="management-content-right">
    <!-- Main -->
    <div id="management-main">

        <div class="main-containers row1 clearfix">
            <!-- Basic Info -->
            <div id="basic-info" class="main-gray-boxes">
                <%Html.RenderPartial("Controls/BasicUserInfo", Model.BasicUserInfo); %>
            </div>
            <!-- Global Profile -->
            <div id="global-profile" class="main-gray-boxes">
                <%Html.RenderPartial("Controls/GlobalProfile", Model.GlobalProfile); %>
            </div>
            <!-- Date/Time Stamps -->
            <div id="date-time-stamps" class="main-gray-boxes">
                <%Html.RenderPartial("Controls/DateTimeStampInfo", Model.DateTimeStampInfo); %>
            </div>
            <!-- Current Sub Status -->  
			<div id="sub-status" class="main-gray-boxes ">
                <%Html.RenderPartial("Controls/SubscriptionStatusInfo", Model.SubscriptionStatusInfo); %>            			                     
            </div>
        </div>

		<!-- Second Row -->  
		<div class="main-containers row2 clearfix">
        	<!-- Communication History -->  
        	<div id="comm" class="main-gray-boxes">
                <%Html.RenderPartial("Controls/CommunicationHistory", Model.CommunicationHistory); %>
            </div>
            <!-- Fraud and Abuse -->  
            <div id="fraud-abuse" class="main-gray-boxes">
                <%Html.RenderPartial("Controls/FraudAbuseInfo", Model.FraudAbuseInfo); %>
            </div>
            <!-- Profile Information -->  
            <div id="profile-info" class="main-gray-boxes">
           		<%Html.RenderPartial("Controls/ProfileInfo", Model.ProfileInfo); %>            			                                             
            </div>
    		<!-- Billing Information --> 
            <div id="billing-info" class="main-gray-boxes loading-bg">

            </div>
            <!-- Other Available Actions --> 
            <div id="other-actions" class="main-gray-boxes">
                <%Html.RenderPartial("Controls/OtherActionInfo", Model.OtherActionInfo); %>            			                                             
            </div>
        </div>

        <div class="main-containers row3 clearfix">
            <!-- Admin Notes & Action Log -->
            <div id="admin-notes-container">
                <%Html.RenderPartial("Controls/AdminActionLog", Model.AdminActionLog); %>
            </div>
            <!-- Transaction History -->
            <div id="transaction-container" class="loading-bg">

            </div>
        </div>

    </div>
</div>
<!-- End Content Area Right --> 

<div id="offsite-notifications">
    <% Html.RenderPartial("Controls/OffSiteNotifications", Model.OffSiteNotifications); %>  
</div>

<div id="premium-service-settings">
    <% Html.RenderPartial("Controls/PremiumServiceSettings", Model.PremiumServiceSettings); %>  
</div>

<div id="admin-adjust">
    <% Html.RenderPartial("Controls/AdminAdjustInfo", Model.AdminAdjustInfo); %>  
</div>

<div id="renewal-info">
    <% Html.RenderPartial("Controls/RenewalInfo", Model.RenewalInfo); %>  
</div>

<div id="profile-display-settings">
    <% Html.RenderPartial("Controls/ProfileDisplaySettings", Model.ProfileDisplaySettings); %>  
</div>

<div id="pause-subscription-info">
    <% Html.RenderPartial("Controls/PauseSubscriptionInfo", Model.PauseSubscriptionInfo); %>  
</div>

<div id="jmeter-editable">
    <% Html.RenderPartial("Controls/JMeterEditable", Model.JMeterEditable); %>
</div>

<div id="il-authenticated">
    <% Html.RenderPartial("Controls/ILAuthenticated", Model.IlAuthenticated); %>
</div>

<div id="last-few-clientips">
    <% Html.RenderPartial("Controls/LastFewClientIPs", Model.LastFewClientIPs); %>
</div>

<% Response.WriteFile(("/Views/Member/Template/TransactionHistory.htm")); %>
<% Response.WriteFile(("/Views/Member/Template/BillingInfo.htm")); %>

<script type="text/javascript">
$(document).ready(function() {
        //GetBillingInfo(<%=Model.MemberSiteInfo.SiteID.ToString("d") %>, <%=Model.MemberSiteInfo.MemberID.ToString() %>);
    GetTransactionHistory(<%=Model.MemberSiteInfo.SiteID.ToString("d") %>, <%=Model.MemberSiteInfo.MemberID.ToString() %>);
    
    //Attach Events
    $('#transaction-subcontainer .giftTransShow').live("click",function () {
        var id = $(this).attr('id');
        var index = id.indexOf("_");
        var code = id.slice(index + 1);
        $("#giftInfo_" + code).show("slide", { direction: "top" }, 500);
    });

    $("#transaction-subcontainer .giftTransHide").live("click",function () {
        $(this).hide("slide", { direction: "bottom" }, 500);
    });
        
    $('.archive-profile').live("click", function () {
        ArchivePaymentProfile($('.pmt-profile-siteid').val(), $('.pmt-profile-memberid').val(), $('.pmt-profile-id').val());
        closeOverLayPSS();
    });
                
    $('#archiveSetting').live("click",function () {
        $('#promptArc').lightbox_me({
            centered: true
        });
        $('#promptArc .cancel-overlay').parent().css('display','block');
        return false;
    });
});
</script>
</asp:Content>
