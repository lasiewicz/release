﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master"
 Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Member.EditProfileData>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	EditProfile
</asp:Content>
 
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript" src="/Scripts/tiny_mce/tiny_mce.js"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<script type="text/javascript">
    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        theme_advanced_buttons1: "undo,redo",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "bottom",
        theme_advanced_toolbar_align: "left"
    });

    function undoTextEdits() {
        var index = 1;
        var originalValue = $('#OriginalValue' + index).val();
        
        while(originalValue)
        {
            tinyMCE.getInstanceById('Attribute' + index).execCommand('mceSetContent', false, originalValue);
            index++;
            originalValue = $('#OriginalValue' + index).val();
        }
    }

    $(function() {
    $('#sdt_70').datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
    
 </script>   
 
 <script language="javascript" type="text/javascript">
     function ValidateEditProfileForm() {
         // check for numeric fields to be all numeric
         var isValid = true;
         $(".numeric-text-field").each(function(index) {
             if (isNaN($(this).val())) {
                 $(this).prev().prev().css('background-color', 'red');
                 isValid = false;
             }
             else {
                 $(this).prev().prev().css('background-color', '');
             }
         });

         if (isValid) {
             // fields that have both max and min
             $(".min-max-validate").each(function() {
                 if (parseInt($(this).val()) >= parseInt($(this).next().val()) && parseInt($(this).val()) <= parseInt($(this).next().next().val())) {
                     $(this).parent().css('color', '');
                     $(this).parent().next().hide();
                     $(this).parent().next().html("");
                 }
                 else {
                     $(this).prev().css('color', 'red');
                     $(this).parent().next().show();
                     $(this).parent().next().html("The allowed range is " + $(this).next().val() + "-" + $(this).next().next().val());
                     isValid = false;
                 }
             });
         }

         if (isValid) {
             // fields with min only
             $(".min-validate").each(function () {
                 if (parseInt($(this).val()) < parseInt($(this).next().val())) {
                     $(this).parent().prev().css('color', 'red');
                     $(this).parent().next().show();
                     $(this).parent().next().html("The minimum age allowed is " + $(this).next().val());
                     isValid = false;
                 }
                 else if (parseInt($(this).val().length) == 0) {
                     $(this).parent().prev().css('color', 'red');
                     $(this).parent().next().show();
                     $(this).parent().next().html("A value must be entered for this field.");
                     isValid = false;
                 }
                 else if (parseInt($(this).val()) > 99) {
                     $(this).parent().prev().css('color', 'red');
                     $(this).parent().next().show();
                     $(this).parent().next().html("The maximum age allowed is 99");
                     isValid = false;
                 }
                 else if (parseInt($(this).val()) > parseInt($(".max-validate").val())) {
                     $(this).parent().prev().css('color', 'red');
                     $(this).parent().next().show();
                     $(this).parent().next().html("The minimum age cannot be greater than the maximum age");
                     isValid = false;
                 }
                 else {
                     $(this).parent().prev().css('color', '');
                     $(this).parent().next().hide();
                     $(this).parent().next().html("");
                 }
             });
         }

         if (isValid) {
             // fields with max only
             $(".max-validate").each(function () {
                 if (parseInt($(this).val()) > parseInt($(this).next().val())) {
                     $(this).parent().prev().css('color', 'red');
                     $(this).parent().next().show();
                     $(this).parent().next().html("The maximum age allowed is " + $(this).next().val());
                     isValid = false;
                 }
                 else if (parseInt($(this).val().length) == 0) {
                     $(this).parent().prev().css('color', 'red');
                     $(this).parent().next().show();
                     $(this).parent().next().html("A value must be entered for this field.");
                     isValid = false;
                 }
                 else {
                     $(this).parent().prev().css('color', '');
                     $(this).parent().next().hide();
                     $(this).parent().next().html("");
                 }
             });
         }

         return isValid;
     }

     $(document).ready(function() {
       $(".date-field").datepicker({ dateFormat: 'mm/dd/yy' });

       if ($(".hidden-message-to-user").val() != "") {
         alertMessage($(".hidden-message-to-user").val(), 2, null);
       }

       $("form").submit(function() {
         return ValidateEditProfileForm();
       });
     });
</script>
 
 <input type="hidden" class="hidden-message-to-user" value="<%= Model.Message %>" />
 
<div id="management-nav">
    <a href="/Member/EditProfile/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>/<%=Model.MemberID.ToString() %>/1"><span class="profile-nav-label">Text Boxes</span></a>
    <a href="/Member/EditProfile/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>/<%=Model.MemberID.ToString() %>/3""><span class="profile-nav-label">Single Selects</span></a> 
    <a href="/Member/EditProfile/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>/<%=Model.MemberID.ToString() %>/4"><span class="profile-nav-label">Multi Selects</span></a> 
    <a href="/Member/EditProfile/<%=Model.MemberSiteInfo.SiteID.ToString("d") %>/<%=Model.MemberID.ToString() %>/2"><span class="profile-nav-label">Boolean CheckBoxes</span></a>
</div>
<%Html.BeginForm("CompleteEditProfile", "Member"); %>
  <%=Html.Hidden("SiteID", ((int)Model.MemberSiteInfo.SiteID).ToString("d"))%>
  <%=Html.Hidden("MemberID", Model.MemberID.ToString())%>
  <%=Html.Hidden("ControlType", Model.ControlType.ToString())%>
   <div style="width:100%; padding-left:5px">
    <h2>EditProfile</h2>
    <% if (Model.DisplayRegionPicker)
       {%>
            <%Html.RenderPartial("../Shared/Controls/RegionPicker", Model.RegionPickerData); %>
    <%} %>
    
    <br />
    
    <% if (Model.DisplayGenderPicker)
       {%>
            <%Html.RenderPartial("../Shared/Controls/GenderPicker", Model.GenderPickerData); %>
    <%} %>
    
    <br />
 
 
    <%--TextBoxes here--%>
   
    <ul>
    <% foreach (var ft in Model.FreeTextAttributes)
       {
           string theLabel = String.Empty;
           bool isTextAread = false;
           switch (ft.AttributeName.ToLower())
           {
               case "aboutme":
                   theLabel = "About Me";
                   isTextAread = true;
                   break;
               case "username":
                   theLabel = "User Name";
                   break;
               case "perfectmatchessay":
                   theLabel = "Perfect Match Essay";
                   isTextAread = true;
                   break;
               case "idealrelationshipessay":
                   theLabel = "Ideal Relationship Essay";
                   isTextAread = true;
                   break;
               case "occupationdescription":
                   theLabel = "Occupation Description";
                   break;
               case "grewupin":
                   theLabel = "Grew Up In";
                   break;
               case "studiesemphasis":
                   theLabel = "Studies Emphasis";
                   break;
               case "perfectfirstdateessay":
                   theLabel = "Perfect First Date Essay";
                   isTextAread = true;
                   break;
               case "learnfromthepastessay":
                   theLabel = "Learn From The Past Essay";
                   isTextAread = true;
                   break;
               case "desiredminage":
                   theLabel = "Desired Min Age";
                   break;
               case "desiredmaxage":
                   theLabel = "Desired Max Age";
                   break;
               default:
                   theLabel = ft.AttributeName;
                   break;   
           }
           
           
           %>
        <li class="list-item-edit"><label><%=theLabel%>:</label>
            <% if (isTextAread == true)
               { %>
                <span style="padding-left:20px;"><%=Html.TextArea("sft_" + ft.AttributeID, ft.AttributeValue, new { style = "width:400px;height:100px;" })%></span>
                 <%=Html.Hidden("OriginalValue" + "sft_" + ft.AttributeID, ft.AttributeValue)%>
             <% }
               else
               { %>
                <span style="padding-left:20px;"><%=Html.TextBox("sft_" + ft.AttributeID, ft.AttributeValue, new { maxlength = ft.MaxLength })%></span>
             <%} %>
         </li>
    <% } %>
  
    <%--Numeric TextBoxes here--%>
    <% foreach (var nt in Model.NumberTextAttributes)
       {

           string theLabel = String.Empty;

           switch (nt.AttributeName.ToLower())
           {
               case "desiredminage":
                   theLabel = "Desired Min Age";
                   break;
               case "desiredmaxage":
                   theLabel = "Desired Max Age";
                   break;
               default:
                   theLabel = nt.AttributeName;
                   break;
           } 
           
           
           %>
         <li class="list-item-edit"><label><%=theLabel%>:</label>
        <%if (nt.MinValue > 0 && nt.MaxValue > 0) {%>
          <span style="padding-left:35px;"><input type="text" name="<%= "snt_" + nt.AttributeID %>" class="numeric-text-field min-max-validate" value="<%= nt.AttributeValue %>" /></span>
          <input type="hidden" value="<%= nt.MinValue %>" />
          <input type="hidden" value="<%= nt.MaxValue %>" />
        <%}
          else if (nt.MinValue < 0 && nt.MaxValue < 0) { %>
        <span style="padding-left:35px;"><input type="text" name="<%= "snt_" + nt.AttributeID %>" class="numeric-text-field" value="<%= nt.AttributeValue %>" /></span>
        <%}
          else if (nt.MinValue > 0) { %>
          <span style="padding-left:20px;">
            <input type="text" name="<%= "snt_" + nt.AttributeID %>" class="numeric-text-field min-validate" value="<%= nt.AttributeValue %>" />
            <input type="hidden" value="<%= nt.MinValue %>" />
          </span>
        <%} else if (nt.MaxValue > 0) { %>
          <span style="padding-left:20px;">
            <input type="text" name="<%= "snt_" + nt.AttributeID %>" class="numeric-text-field max-validate" value="<%= nt.AttributeValue %>" />
            <input type="hidden" value="<%= nt.MaxValue %>" />
          </span>
        <%} %>

        <span class="error-message" style="margin-left: 40px; border: solid 1px #c00; background-color: rgb(255, 228, 288); font-size: 16px; font-weight: bold; color: #ec5100; padding: 3px; display: none;"></span>
        </li>
       
    <% } %>
    
    
    
    <%--DateTime TextBoxes here--%>
    <% foreach (var dt in Model.DateTimeAttributes)
       { %>
         <li class="list-item-edit"><label><%=dt.AttributeName %>:</label>
         <span style="padding-left:20px;"><input type="text" name="<%= "sdt_" +  dt.AttributeID %>" class="date-field" value="<%=dt.AttributeValue.ToString("d") %>" /></span></li>
    <% } %>
    
      </ul>
    </div>
    
    
    <br />
    
    <%--Boolean CheckBoxes here--%>
    <ul>
    <% foreach (var bcb in Model.BooleanAttributes)
       { %>
            <li class="list-item-edit">
                <label><%=bcb.AttributeName %>:</label>
                <span style="padding-left:20px;"><%=Html.CheckBox("sbcb_" + bcb.AttributeID, bcb.AttributeValue)%></span>
            </li>
    <% } %>
    </ul>
   
    
    <%--Single selects here--%>
    <% if (Model.SingleSelectAttributes.Count > 0)
       { %>
    <div style="width:100%; padding-left:5px">
    
        <ul>
            <% foreach (var ss in Model.SingleSelectAttributes)
               {%>
                   <li class="list-item-edit"><label><%=ss.AttributeName%>:</label>
                   <span style="padding-left:10px;"><%=Html.DropDownList("sss_" + ss.AttributeID, ss.OptionChoices)%></span></li>
            <%} %>
        </ul>
   
    </div>
    <%} %>
    <br />
    
    <%--Multi selects here--%>
    <ul>
    <% foreach (var ms in Model.MultiSelectAttributes)
       {%>
           <li class="list-item-edit">
               <label><%=ms.AttributeName %>: </label>
               <span style="padding-left:10px;">
                <table>
                  <% foreach (var oc in ms.OptionChoices)
                     { %>
                     <% if (oc.Selected)
                        { %>
                            <tr>
                                <td><input type="checkbox" name= "sms_<%=ms.AttributeID %>" value="<%=oc.Value %>" checked="checked" /></td><td><%=oc.Text%></td>
                            </tr>
                     <%}
                        else
                        { %>
                            <tr>
                                <td><input type="checkbox" name= "sms_<%=ms.AttributeID %>" value="<%=oc.Value %>" /></td><td><%=oc.Text%></td>
                            </tr>      
                     <%} %>
                     
                  <%} %>
                 </table>
                </span>
           </li>
    <%} %>
    </ul>
    <br />

<div style="width:100%; text-align:center; padding-left:300px;">
  <input type="submit"  class="button" value="Make Update" />
</div>



<%Html.EndForm(); %>    
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>

