﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.MemberDataForPrint>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Email" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PrintableMemberData2</title>
    <style>
           ul {
             font-family: Verdana, Arial, Helvetica, sans-serif;
             font-size: 9px;
             font-style: normal;
             line-height: 1.5em;
             font-weight: normal;
             font-variant: normal;
             text-transform: none;
             color: #000000;
             text-decoration: none;
                   list-style-type: none;
 
             }
  
              li {
                   list-style-type: none;
              }
    
            label {
              text-align:left;
              font-weight:bold;
 
              }
  
              li span {
                  padding-left: 5px;
              }
  
              .headerLabel {
                  font-size: 14px;
                  font-weight: bold;
                  text-decoration: underline;
                  padding-bottom: 15px;
              }
             
         </style>
</head>
<body>
 <div style="width: 100%; padding-bottom: 20px; border: solid 1px #cccccc">
        <div>
            <div style="display: inline-block; padding-left: 5px; padding-bottom: 10px;">
                <%=Model.MDPBrandInfo%>
            </div>
        </div>
        <div style="display: inline-block; padding-left: 5px;">
            Activity Data for user: <%=Model.UserName%>
        </div>
    </div>
    <div style="display: inline-block;float:left; width:300px;">
        <table>
            <tr>
                <td>
                    <span class="headerLabel">Login History</span>
                </td>
            </tr>
            <tr>
                <td>Date</td>
                <td>Count</td>
            </tr>
            <% if(Model.LastLogons != null) { %>
                <% foreach (MemberDataForPrint.LoginData logonData in Model.LastLogons) { %>
                  <tr>
                      <td><%=logonData.Login.ToShortDateString()%></td>
                      <td><%=logonData.count%> </td>
                  </tr>
                <%}%>
            <%}%>
        </table>
        <br />
        <table>
            <tr>
                <td colspan="3">
                       <span class="headerLabel">Message History (Sent)</span>
                </td>
            </tr>
            <tr>
                <td>Date</td>
                <td>Message</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3">
                    <% if(Model.AllSentEmails != null) { %>
                        <% foreach (Email email in Model.AllSentEmails) { %>
                         <tr>
                             <td><%=email.MessageDate.ToShortDateString()%></td>
                             <td><%=email.ViewText%></td>
                             <td></td>
                        </tr>
                        <%}%>
                    <%}%>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
