﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.ListMigrationModelView>" %>
<%@ Import Namespace="Matchnet.List.ValueObjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Spark Networks Web Admin - List Migration Support 0.1
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    &nbsp;<br />
    <h2>List Migration Support</h2>
    <p>&nbsp;</p>
    <h3>List items</h3>
<%--    <h3>GetListMembers(category, communityId, siteId, 1, 10000, out rowCount)</h3>--%>
    <table class="listMigrationTable">
        <tr>
            <td>Category</td>
            <td>Target MemberId</td>
            <td>Update Date</td>
            <td>Tease Id</td>
            <td>Comment</td>
        </tr>
        
        <% foreach(var category in Model.ListData) { %>
            <% if (category.RowCount >0 ) {%>
        
                    <% foreach(var targetMemberDeatils in category.MemberIds) { %>
                        <tr>
                            <td><%=(HotListCategory)category.CategoryId%> (<%=category.CategoryId%>)</td>
                            <td><%=targetMemberDeatils.Key%></td>
                            <td><%=targetMemberDeatils.Value.ActionDate%></td>
                            <td><%=targetMemberDeatils.Value.TeaseID >0 ? targetMemberDeatils.Value.TeaseID: 0 %></td>
                            <td><%=targetMemberDeatils.Value.Comment%></td>
                        </tr>
                    <%} %> 
            <%} %> 
        <%} %> 
    </table>    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        /*font-family: "Courier New", Courier, monospace; font-size: 8px*/
         .listMigrationTable td{border:1px solid #999999;padding: 10px; }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("Controls/TopNav", Model.TopNav); %>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/MemberLeft", Model.MemberLeft); %>
</asp:Content>
