﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.MigrationSupport>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Spark Networks Web Admin - Migration Support
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    &nbsp;<br />
    <h2>Migration Support</h2>
    <p>&nbsp;</p>
    <h3>Integers</h3>
    <table class="migrationSupportTable">
        <tr>
            <td><b>Name</b></td>
            <td><b>Mingle Name</b></td>
            <td><b>Value</b></td>
            <td><b>Option Description</b></td>
        </tr>
        <% foreach(var attribute in Model.IntegerAttributes) { %>
            <tr>
                <td><%=attribute.BHAttributeName %></td>
                <td><%=attribute.MingleAttributeName %></td>
                <td><%=attribute.Value %></td>
                <td><%=attribute.Description %></td>
            </tr>
        <%} %> 
    </table>
    <br/><br />
    <h3>Text</h3>
    <table class="migrationSupportTable">
        <tr>
            <td><b>Name</b></td>
            <td><b>Mingle Name</b></td>
            <td><b>Value</b></td>
            <td><b>Approved Status</b></td>
        </tr>
        <% foreach(var attribute in Model.TextAttributes) { %>
            <tr>
                <td><%=attribute.BHAttributeName %></td>
                <td><%=attribute.MingleAttributeName %></td>
                <td><%=attribute.Value %></td>
                <td><%=attribute.ApprovedStatus %></td>
            </tr>
        <%} %> 
    </table>
    <br/><br />
    <h3>Dates</h3>
        <table class="migrationSupportTable">
            <tr>
            <td><b>Name</b></td>
            <td><b>Mingle Name</b></td>
            <td><b>Value</b></td>
        </tr>
        <% foreach(var attribute in Model.DateAttributes) { %>
            <tr>
                <td><%=attribute.BHAttributeName %></td>
                <td><%=attribute.MingleAttributeName %></td>
                <td><%=attribute.Value %></td>
            </tr>
        <%} %> 
    </table>
    <br/><br />
    <h3>SearchPreferences</h3>
        <table class="migrationSupportTable">
            <tr>
            <td><b>Name</b></td>
            <td><b>Mingle Name</b></td>
            <td><b>Value</b></td>
            <td><b>Weight</b></td>
            <td><b>Description</b></td>
        </tr>
        <% foreach(var preference in Model.SearchPreferences) { %>
            <tr>
                <td><%=preference.BHPreferenceName%></td>
                <td><%=preference.MinglePreferenceName%></td>
                <td><%=preference.Value %></td>
                <td><%=preference.Weight %></td>
                <td><%=preference.Description %></td>
            </tr>
        <%} %> 
    </table>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .migrationSupportTable td{border:1px solid #999999;padding: 10px;}
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("Controls/MemberLeft", Model.MemberLeft); %>
</asp:Content>
