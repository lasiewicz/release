﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Migration.PhotoMigrationModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Photo Migration Support
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" class="init">
        $(document).ready(function () {
            // initialize jquery datatable 
            var table = $('#photoMigrationTable').DataTable(
                {
                    //change the # of entries drop down values
                    "lengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]]
                });

            
            $('#photoMigrationTable').dataTable();

            // hook an event to show full and thumb photos when a row is clicked on
            $('#photoMigrationTable tbody').on('click', 'tr', function() {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                } else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    //console.log(table.row(this).data());
                    $('#fullCloud').attr('src', table.row(this).data()[1]);
                    $('#thumbCloud').attr('src', table.row(this).data()[2]);
                }
            });
        });
    </script>
    &nbsp;<br/>
    <h2>Photo Migration Support</h2><br/>
    <div>
        <p>Click on a row to fetch and view the photos!</p>
    </div>
    &nbsp;<br/>
    <table id="photoMigrationTable" class="display">
        <thead>
        <tr>
            <th>MemberPhotoId</th>
            <th>Full Cloud Path</th>
            <th>Thumb Cloud Path</th>
            <th>Approved</th>
            <th>AdminMemberId</th>
        </tr>
        </thead>
        <tbody>
        <% foreach (var photo in Model.Photos)
           { %>
            <tr>
                <td> <%= photo.MemberPhotoId %> </td>
                <td> <%= Html.Encode(photo.FullCloudPath) %> </td>
                <td> <%= Html.Encode(photo.ThumbCloudPath) %> </td>
                <td> <%= photo.IsApproved %></td>
                <td> <%= photo.AdminMemberId %></td>
            </tr>
        <% } %>
        </tbody>
    </table>
    <style>
        #photosViewer {
            border-color: black;
            border-width: 2px;
            margin: 30px;
        }
         #leftPhoto {
             float: left;
             width: 50%;
         }

        #rightPhoto {
            float: right;
            width: 50%;
        }
    </style>
    <div id="photosViewer">
        <div id="leftPhoto">
            <img id="fullCloud" src="" alt="No Full Cloud Photo"/>
        </div>
        <div id="rightPhoto">
            <img id="thumbCloud" src="" alt="No Thumb Cloud Photo"/>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../Content/superfish-members-search.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        /*font-family: "Courier New", Courier, monospace; font-size: 8px*/
        .listMigrationTable td {
            border: 1px solid #999999;
            padding: 10px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <% Html.RenderPartial("Controls/TopNav", Model.TopNav); %>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <% Html.RenderPartial("Controls/MemberLeft", Model.MemberLeft); %>
</asp:Content>