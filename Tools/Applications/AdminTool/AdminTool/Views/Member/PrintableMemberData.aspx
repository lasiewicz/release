﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.MemberDataForPrint>" %>

<%@ Import Namespace="AdminTool.Models.ModelViews.Email" %>

<html>
<head>
    <script src="<%= ResolveUrl("~/Scripts/jquery-1.4.2.min.js")%>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/Scripts/jquery.lightbox_me.js")%>" type="text/javascript"></script>
    <style>
        ul {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 9px;
            font-style: normal;
            line-height: 1.5em;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            color: #000000;
            text-decoration: none;
            list-style-type: none;
        }

        li {
            list-style-type: none;
            line-height: 17px;
        }

        label {
            text-align: left;
            font-weight: bold;
        }

        li span {
            padding-left: 5px;
        }

        .headerLabel {
            font-size: 12px;
            font-weight: bold;
            text-decoration: underline;
            padding-bottom: 15px;
        }
    </style>
</head>
<body>
    <div style="width: 100%; padding-bottom: 20px; border: solid 1px #cccccc">
        <div>
            <div style="display: inline-block; padding-left: 5px; padding-bottom: 10px;">
                <%=Model.MDPBrandInfo%>
            </div>
            <div style="display: inline-block; float: right;">
                Case: <span id="case"></span>
            </div>
        </div>
        <div style="display: inline-block; padding-left: 5px;">
            Profile Data for user: <%=Model.UserName%>
        </div>
        <div style="display: inline-block; float: right;">
            <a href="/Member/Print2/<%=Model.TheSiteID%>/<%=Model.MemberID%>">Page 2</a>
        </div>
    </div>
    <table width="100%">
        <tr valign="top">
            <td width="60%">

                <ul>
                    <li>
                        <span class="headerLabel">Basic Information</span>
                    </li>
                    <li>
                        <label>User has an approved Photo ?</label>
                        <span><%=Model.HasPhoto.ToString()%></span>
                    </li>
                    <li>
                        <label>Email:</label>
                        <span><%=Model.MDPEmailAddress%></span></li>
                    <li>
                        <label>ProfileStatus:</label><span><%=Model.ProfileStatus.ToString()%></span></li>
                    <% foreach (var dt in Model.DateTimeAttributes)
                       { %>
                    <li>
                        <label><%=dt.AttributeName %>:</label>
                        <span><%=dt.AttributeValue.ToString("d") %></span>
                    </li>
                    <% } %>
                    <li>
                        <label>Location: </label>
                        <span><%=Model.ProfileInfo.RegionDisplay.ToString()%></span></li>
                    <li>
                        <label>Gender: </label>
                        <span><%=Model.ProfileInfo.Gender.ToString()%></span></li>
                    <li>
                        <label>Seeking Gender:</label><span><%=Model.ProfileInfo.SeekingGender.ToString()%></span></li>
                </ul>

                <ul>
                    <li><span class="headerLabel">About Me</span></li>
                    <% foreach (var ft in Model.FreeTextAttributes)
                       {
                           if (ft.AttributeValue != string.Empty)
                           {
                               string theLabel = String.Empty;
                               bool addME = false;
                               switch (ft.AttributeName.ToLower())
                               {
                                   case "username":
                                       theLabel = "User Name";
                                       addME = true;
                                       break;
                                   case "occupationdescription":
                                       theLabel = "Occupation Description";
                                       addME = true;
                                       break;
                                   case "grewupin":
                                       theLabel = "Grew Up In";
                                       addME = true;
                                       break;
                                   case "studiesemphasis":
                                       theLabel = "Studies Emphasis";
                                       addME = true;
                                       break;
                                   case "desiredminage":
                                       theLabel = "Desired Min Age";
                                       if (ft.AttributeValue == "-2147483647")
                                           addME = false;
                                       else
                                           addME = true;
                                       break;
                                   case "desiredmaxage":
                                       theLabel = "Desired Max Age";
                                       if (ft.AttributeValue == "-2147483647")
                                           addME = false;
                                       else
                                           addME = true;
                                       break;
                                   default:
                                       theLabel = ft.AttributeName;
                                       break;
                               }
           
           
                    %>

                    <% if (addME)
                       { %>
                    <li>
                        <label><%=theLabel%>:</label>
                        <span><%=ft.AttributeValue%></span>
                    </li>
                    <%} %>

                    <% }
       } %>
                    <% foreach (var bcb in Model.BooleanAttributes)
                       { %>
                    <li>
                        <label><%=bcb.AttributeName %>:</label>
                        <span><%=bcb.AttributeValue%></span>
                    </li>
                    <% } %>


                    <% if (Model.SingleSelectAttributes.Count > 0)
                       { %>

                    <% foreach (var ss in Model.SingleSelectAttributes)
                       {
                           foreach (var os in ss.OptionChoices)
                           {
                               if (os.Selected)
                               {
                    %>
                    <li>
                        <label><%=ss.AttributeName%>:</label>
                        <span><%=os.Text %></span></li>
                    <% 
                       }

                   }
                       } %>


                    <%} %>

                    <% foreach (var nt in Model.NumberTextAttributes)
                       {

                           string theLabel = String.Empty;
                           bool addME = true;
                           switch (nt.AttributeName.ToLower())
                           {
                               case "desiredminage":
                                   theLabel = "Desired Min Age";
                                   if (nt.AttributeValue == -2147483647)
                                       addME = false;
                                   else
                                       addME = true;
                                   break;
                               case "desiredmaxage":
                                   theLabel = "Desired Max Age";
                                   if (nt.AttributeValue == -2147483647)
                                       addME = false;
                                   else
                                       addME = true;
                                   break;
                               default:
                                   theLabel = nt.AttributeName;
                                   break;
                           } 
           
           
                    %>
                    <% if (addME)
                       { %>
                    <li>
                        <label><%=theLabel%>:</label>
                        <span>
                            <%= nt.AttributeValue %>
                        </span>
                    </li>
                    <%} %>

                    <% } %>


                    <% foreach (var ms in Model.MultiSelectAttributes)
                       {
                           bool addMe = false;
                           foreach (var oc in ms.OptionChoices)
                           {
                               if (oc.Selected)
                               {
                                   addMe = true;
                                   break;
                               }
                           }
                           if (addMe)
                           { 
                    %>

                    <li style="padding-top: 7px;">
                        <label><%=ms.AttributeName %>: </label>
                        <span>
                            <% foreach (var oc in ms.OptionChoices)
                               { %>
                            <% if (oc.Selected)
                               { %>
                            <%=oc.Text%>
                            <%} %>
                            <%} %>
                        </span>
                    </li>
                    <%}
       }%>
                </ul>
            </td>
            <td>
                <ul>
                    <% foreach (var ft in Model.FreeTextAttributes)
                       {
                           bool addMe = false;

                           if (ft.AttributeValue != "")
                               addMe = true;

                           string theLabel = String.Empty;
                           bool isTextAread = false;
                           switch (ft.AttributeName.ToLower())
                           {
                               case "aboutme":
                                   theLabel = "About Me";
                                   isTextAread = true;
                                   break;
                               case "perfectmatchessay":
                                   theLabel = "Perfect Match";
                                   isTextAread = true;
                                   break;
                               case "idealrelationshipessay":
                                   theLabel = "Ideal Relationship";
                                   isTextAread = true;
                                   break;
                               case "perfectfirstdateessay":
                                   theLabel = "Perfect First Date";
                                   isTextAread = true;
                                   break;
                               case "learnfromthepastessay":
                                   theLabel = "Learn From The Past";
                                   isTextAread = true;
                                   break;
                               default:
                                   theLabel = ft.AttributeName;
                                   break;
                           }
                    %>
                    <% if (isTextAread == true && addMe == true)
                       { %>
                    <li style="padding-top: 7px;">
                        <label><%=theLabel%>:</label>

                        <% if (ft.AttributeValue.Length > 150)
                           { %>
                        <%=ft.AttributeValue.Substring(0,150)%> ...
                        <%}
                           else
                           { %>
                        <%=ft.AttributeValue%>
                        <%} %>
                    </li>
                    <%} %>

                    <% } %>
                </ul>

                <ul>
                    <li>
                        <span class="headerLabel">Admin Notes</span>
                    </li>
                    <%foreach (AdminTool.Models.MemberDataForPrint.AdminActionNotesMiniForPrint note in Model.Notes)
                      { %>
                                <li style="padding-top: 7px;">
                                    <span style="font-weight: bold;"><%=note.InsertDate.ToString()%></span>
                                    <%= note.Note %>
                                </li>
                      <%
                       }
                       %>
                        
                </ul>

            </td>
        </tr>
    </table>

    <div id="prompt">

        <input type="text" id="theCaseNumber" /> <input type="button" onclick="updateCaseNumber();" value="Add Case" />
    </div>


    <input type="hidden" id="closeOverlay" />

    <script type="text/javascript">

        $('#prompt').lightbox_me({
            centered: true
        });

        function updateCaseNumber()
        {

            $('#case').html($('#theCaseNumber').val());
            closeOverlay();

        }

        function closeOverLay() {

            $('#closeOverlay').lightbox_me();
            $('#closeOverlay').trigger('close');


        }

    </script>






</body>
</html>
