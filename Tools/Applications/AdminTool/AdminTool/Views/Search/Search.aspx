﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Search.SearchData>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Search" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="TitleContent" runat="server">
	<%--Page title--%>
	Spark Networks Web Admin - Member Search
</asp:Content>

<asp:Content ID="ContentHead" ContentPlaceHolderID="HeadContent" runat="server">
<link href="../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Search Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", this.ViewData.Model.TopNav); %>
</asp:Content>


<asp:Content ID="ContentMain" ContentPlaceHolderID="MainContent" runat="server">

<div id="member-search-container">

<script type="text/javascript">

    $(document).ready(function () {


        if ($("#search-results").length > 0) {
            $(".searchDocumentationBlock").hide();
        }
        else
            $(".searchDocumentationBlock").show();


        $('#clearMe').click(function (event) {

            $("#CommunityID").val('0');


        });

        if ($('#EndOfSearchResultsReached').val().toLowerCase() == "true") {
            alertMessage("End of search results reached");
        }

    });
</script>



<div id="member-search-sidebar">
	<%=Html.Hidden("VirtualTerminalUrl", Model.VirtualTerminalUrl) %>
    <p class="header">Search Criteria</p>
	<p class="instruction">You may enter the leading portion of the email address or username, using "%" where a wildcard is needed to represent one or more charac- <br>ters. "%" cannot be the first character of the the email or username. </p>

	<div id="search-by-billing">
        <input type="button" class="button" value="Search By Billing" />
    </div>   
    
</div>


<div id="member-search-main">
    <%=Html.Hidden("EndOfSearchResultsReached", Model.SearchResult.SearchResultEndReached) %>

<!-- Search Left -->
<dl class="search-forms left">
    <% if (Model.LegacyEnabled)
       { %>
	<dt>Search Mode:</dt>
    <dd><%= Html.CheckBoxFor(model => model.PerformLegacySearch, new {@class = "form-size-small"}) %><span class="info">Legacy</span></dd>
    <% } %>
    
    <dt>Email:</dt>
	    <dd><%= Html.TextBox("Email", Model.Email, new{@class="form-size"})%></dd>

	<dt>Username:</dt>
	    <dd>
	        <%= Html.TextBox("UserName", Model.UserName, new{@class="form-size"})%>
	        <%= Html.CheckBoxFor(model => model.ExactUsernameOnly, new { @class = "form-size-small" })%> <span class="info"> check to search exact username </span>
	    </dd>
	    

	<dt>Member ID:</dt>
	    <dd><%= Html.TextBox("MemberID", (Model.MemberID > 0) ? Model.MemberID.ToString() : "", new{@class="form-size"})%></dd>	                 

	<dt>Phone:</dt>
	    <dd><%= Html.TextBox("PhoneNumber", Model.PhoneNumber, new{@class="form-size"})%></dd>

    <dt>IP Address:</dt>
    <dd class="ips"><%= Html.TextBox("IP1", Model.IP1, new { @class = "form-size-medium" })%> - <%= Html.TextBox("IP2", Model.IP2, new { @class = "form-size-medium" })%>> - <%= Html.TextBox("IP3", Model.IP3, new { @class = "form-size-medium" })%> - <%= Html.TextBox("IP4", Model.IP4, new { @class = "form-size-medium" })%></dd>                       
    

</dl>
    
<!-- Search Mid -->    
<dl class="search-forms mid">
	<dt>First Name:</dt>
    <dd><%= Html.TextBox("FirstName", Model.FirstName, new{@class="form-size"})%></dd>

	<dt>Last Name:</dt>
    <dd><%= Html.TextBox("LastName", Model.LastName, new{@class="form-size"})%></dd>	         

	<dt>Zip Code:</dt>
    <dd class="zip"><%= Html.TextBox("ZipCode", Model.ZipCode, new{@class="form-size"})%></dd>	                 

	<dt>City Name:</dt>
    <dd><%= Html.TextBox("CityName", Model.CityName, new{@class="form-size"})%></dd>
    
        <%if (Model.CheckUIPermission(AdminTool.Models.Enums.Operations.Searching))
      { %>
    <dt>&nbsp;</dt>
	    <dd><input type="submit" class="button_big" value="Search" id="search" name=""/> 
	        <input type="button" class="button_big"  value="Clear" id="clearMe" name=""/></dd>
	    
	   <%} %>

</dl>


<!-- Search Right -->           
<div class="search-forms right">
	<img src="images/spark-logo.gif" />

	<p class="header">Filter By:</p>

	<p class="search-filter">
        <%= Html.RadioButton("SearchFilter", (int)AdminSearchFilterType.None, Model.FilterValue == AdminSearchFilterType.None)%>No filter<br /><br />
        <%= Html.RadioButton("SearchFilter", (int)AdminSearchFilterType.Active, Model.FilterValue == AdminSearchFilterType.Active)%>Active<br /><br />
       <%-- <%= Html.RadioButton("SearchFilter", (int)AdminSearchFilterType.Subscriber, Model.FilterValue == AdminSearchFilterType.Subscriber)%>Subscriber<br /><br />
        <%= Html.RadioButton("SearchFilter", (int)AdminSearchFilterType.NonSubscriber, Model.FilterValue == AdminSearchFilterType.NonSubscriber)%>Non-Subscriber<br /><br />--%>
        <%= Html.RadioButton("SearchFilter", (int)AdminSearchFilterType.SelfSuspended, Model.FilterValue == AdminSearchFilterType.SelfSuspended)%>Self-Suspended<br /><br />
        <%= Html.RadioButton("SearchFilter", (int)AdminSearchFilterType.AdminSuspended, Model.FilterValue == AdminSearchFilterType.AdminSuspended)%>Admin-Suspended<br /><br />
	</p>

     <dt> <b>Community: </b><br /><br />
     <%= Html.DropDownListFor(x => x.CommunityID, Model.CommunityIDChoices, new { @class = "form-size" })%>
     </dt>
	    <dd></dd> 
    
</div>
<div style="float:left; border-top:solid 2px #ccc;width:100%; margin-top:40px;">
<div class="searchDocumentationBlock" >

<p>The Admintool Search has been upgraded to include several new search fields and filters to improve the ability to locate members.  Results have also been expanded from 100 to 1000. Search fields appear in 3 columns.  Column 3 filters apply to any search but here are a few notes to be aware of when using various search fields in Columns 1 & 2.</p>

<p style="padding-top:25px;padding-bottom:7px;">
<span style="font-weight:bolder; text-decoration:underline;">Column 1 - Single Field Searches, Filter-Only</span> <br />
<span style="font-weight:bolder;">Email – Username – Member ID – Phone – IP Address </span> (from registration) <br /><br />
<span style="font-weight:bolder; color:#003366; font-style:italic;">Column 1 fields cannot be combined with other search fields but can be filtered by status and community in Column 3.</span> <br /><br />
<span style="padding-left:5px; word-spacing:2px;">For example, a search can include:</span><br />
<span style="padding-left:5px; word-spacing:2px;">• Username + Community + Status</span><br />
<span style="padding-left:5px; word-spacing:2px;">• “NYCGirl” + Spark + Admin Suspended = good results based on all 3 parameters</span><br /><br />
<span style="padding-left:5px; word-spacing:2px;color:#660000;">However, a search cannot combine Column 1 fields with Column 2 fields.  Column 1 takes priority:</span> <br />
<span style="padding-left:5px; word-spacing:2px;color:#660000;">• Username + First Name</span> <br />
<span style="padding-left:5px; word-spacing:2px;color:#660000;">• “NYCGirl” + “Ellen” = same results as if only Username “NYCGirl” was included</span><br />
</p>


<p>
<span style="font-weight:bolder; text-decoration:underline;">Column 2 – Multi Field Searches + Filter</span><br />
<span style="font-weight:bolder;">First Name – Last Name – Zip Code – City Name  </span> <br /><br />
<span style="font-weight:bolder;  color:#003366; font-style:italic;">Search fields in the second column can be combined with each other as well as filters in Column 3.</span> <br /><br />

<span style="padding-left:5px; word-spacing:2px;">For example, a search can include:</span><br />
<span style="padding-left:5px; word-spacing:2px;">• First Name + Last Name + City + Community + Status</span><br />
<span style="padding-left:5px; word-spacing:2px;">• “Tom” + “Smith” + “Beverly Hills” + JDate + Active = good results based on all 5 parameters</span><br /><br />
<span style="padding-left:5px; word-spacing:2px;color:#660000;">However, if Username or another field from Column 1 is added it will take priority and cancel out any search terms included in Column 2.</span>
</p>

</div>
</div>



</div>
<!-- End of member-search-main -->  

<div class="clearfix"></div>

</div>
<!-- End of member-search-container -->   


<br clear="all" />


<% if (Model.SearchResult.isSearched)
   {
       if (Model.SearchResult.TotalPages <= 0)
       {%>
                <div class="search-no-results">No Results found.</div>
            <% }
       else
       { %>
                <div class="search-pagination">
                     <span class="search-pagination-totalpages">
                     <% if (Model.PageNumber != 1)
                        {%>
                        <a href="/?<%= Model.GetSearchParameterQueryString(Model.PageNumber - 1) %>">< Previous</a> 
                     <% }%>
                     <% if (Model.SearchResult.TotalRows >= Model.ResultsPerPage)
                        {%>
                     <a href="/?<%= Model.GetSearchParameterQueryString(Model.PageNumber + 1) %>">Next ></a> 
                     <% } %>
                     </span>
                </div>
    <% }%>
<!--Results-->
<div id="search-results">

<table id="search-results-table">

    <thead>
        <tr>
        <th>Email Address:</th>
        <th>Username:</th>
        <th>Member ID:</th>
        <th>First Name:</th>
        <th>Last Name:</th>
        <th>Gender:</th>
        <th>Age:</th>
        <th>Location:</th>
        <th>Site: <a href="#" id="legend">(click for legend)</a>
            <div id="legend-container">
                <a href="" class="close-x"><img src="/images/close.png" width="15" /></a><br />
                <img src="../../Images/legend.png" />
            </div>
            </th>
        </tr>
    </thead>

    <tbody>
    
     <%foreach (AdminTool.Models.ModelViews.Search.SearchMember member in Model.SearchResult.SearchMembers)
                          {%>
                             
                            
        <tr>
        <td class="member-search-email-container"><%=member.EmailAddress %></td>
        <td><%= member.UserName %></td>
        <td><%= member.MemberID %></td>
        <td><%= Model.TruncateString(member.FirstName, 20)%></td>
        <td><%= Model.TruncateString(member.LastName, 20) %></td>
        <td><%=member.Gender %></td>
        <td><%=member.Age.ToString() %></td>       
        <td><%=member.Location %></td>
        <td>
        <%if (member.MemberSites.Count > 0)
          { %>
        <select id="site-list-<%= member.MemberID %>" class="site-list <%= member.MemberID %>">
            <%foreach (AdminTool.Models.ModelViews.MemberSiteInfo site in member.MemberSites)
              {%>
              
            <option value="<%= ((int)site.SiteID).ToString() %>" class="site-status-<%= site.ProfileStatus %>" >
            <%= site.Name%>
            <%if (site.IsSubscriber)
              { %>
            $
            <%} %>
           </option>
            <%} %>
        </select>
        <%}
          else
          { %>
        None
        <%} %>
        </td>
        <td>
            <%if (member.MemberSites.Count > 0)
              { %>
            <a id="view-member-<%= member.MemberID %>" href="/Member/View/<%=((int)member.MemberSites[0].SiteID).ToString()%>/<%=member.MemberID.ToString() %>">View</a></td>
            <%} %>
        </tr>
     <%} %>

    </tbody>
</table>


<br /><br />

</div>

         <% if (Model.SearchResult.TotalPages <= 0)
            {%>
                <div class="search-no-results">No Results found.</div>
            <%}else{ %>
                <div class="search-pagination">
                     <span class="search-pagination-totalpages">
                     <% if (Model.PageNumber != 1)
                        {%>
                        <a href="/?<%= Model.GetSearchParameterQueryString(Model.PageNumber - 1) %>">< Previous</a> 
                     <% }%>
                     <% if (Model.SearchResult.TotalRows >= Model.ResultsPerPage)
                        {%>
                     <a href="/?<%= Model.GetSearchParameterQueryString(Model.PageNumber + 1) %>">Next ></a>
                     <% } %>
                     </span>
                </div>
            <%}
        } %>


</asp:Content>
