﻿<%@ Page Language="C#" AutoEventWireup="true"Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.UPS.SubscriptionData>" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="mboxDefault"></div><script language="JavaScript1.2"><%=Model.MBoxScript%></script>
        <h1><%=Model.HeaderText %></h1>  
        <p>A confirmation email has been sent to your personal email address. Now you can start contacting other members with all the great features on our site.</p>
        <h2 style="margin: 1em 2em .5em;">Keep this information for your records</h2>
        <div class="key-value-pair" style="margin: 0 0 1em 6em;">
	        <span class="key">Username:</span> <%=Model.UserName %> <br>
	        <span class="key">Confirmation Number:</span> <%=Model.ConfirmationNumber %> <br>
	        <span class="key">Card Type:</span> <%=Model.CardType %> <br>
	        <span class="key">Card Number:</span> <%=Model.CardNumber %> <br>
	        <span class="key">Transaction Date:</span> <%= Model.TransactionDate %>
        </div>
        <%if (Model.ShowColorCodeConfirmation) { %>
            <p>A confirmation email has been sent to your personal email address. Now you can start contacting other members with all the great features on our site.</p>
            <h2 style="margin: 1em 2em .5em;">Keep this information for your records</h2>
            <div class="key-value-pair"  style="margin: 0 0 1em 6em;">
	            <span class="key">Username:</span> <%=Model.UserName %> <br />
	            <span class="key">Confirmation Number:</span> <%=Model.ConfirmationNumber %><br />
	            <span class="key">Product Name:</span> Free Trial Subscription<br />	
                <span class="key">Cost:</span> $0.00<br />
	            <span class="key">Transaction Date:</span> <%= Model.TransactionDate %>
            </div>
        <% } %>

         <%if (Model.ShowFreeTrialConfirmation) { %>
            <p>At the end of your Free Trial period, the credit card information you entered will be used to bill you for the subscription plan you selected. A confirmation email has been sent to email address you provided.</p>
            <h2 style="margin: 1em 2em .5em;">Keep this information for your records</h2>
            <div class="key-value-pair"  style="margin: 0 0 1em 6em;">
	            <span class="key">Username:</span>  <%=Model.UserName %> <br/>
	            <span class="key">Confirmation Number:</span> <%=Model.ConfirmationNumber %> <br/>
	            <span class="key">Product Name:</span> Free Trial Subscription<br />	
                <span class="key">Cost:</span> <%=Model.Cost %><br/> 
	            <span class="key">Card Type:</span> <%=Model.CardType %> <br/>
	            <span class="key">Card Number:</span> <%=Model.CardNumber %> <br/>
	            <span class="key">Transaction Date:</span> <%= Model.TransactionDate %>
            </div>
        <% } %>

        <%if (Model.ShowOtherStuff) { %>
            <%if (Model.ShowFreeTrialConfirmationAdditional) { %>
                Please note that at any time during the course of your Free Trial Subscription you may choose to end your Free Trial and decline automatic subscription to the site. This may be done on the Auto Renewal page of your account.
            <% } %>
        <% } %>
    </div>
    </form>
</body>
</html>
