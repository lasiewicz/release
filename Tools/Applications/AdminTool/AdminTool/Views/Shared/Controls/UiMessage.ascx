﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.ModelViewBase>" %>

<%=Html.HiddenFor(m=>m.BigUiMessage) %>

<div class="modal" id="BigUIMessageContainer">
        <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverlayBigUIMessage()"><b>[X]</b></a></div>
        <div id="BigUIMessageContent"></div>
    </div>
<input type="hidden" id="closeOverlayBigUIMessage" />


<%if (!string.IsNullOrEmpty(Model.BigUiMessage))
  {%>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            if ($('#BigUiMessage').val().length > 0) {
                $('#BigUIMessageContent').html($('#BigUiMessage').val());
                $('#BigUIMessageContainer').lightbox_me({
                    centered: true
                });
            }
        });

        function closeOverlayBigUIMessage() {
            $('#BigUIMessageContainer').lightbox_me();
            $('#BigUIMessageContainer').trigger('close');
        }
    </script>
  <%}%>