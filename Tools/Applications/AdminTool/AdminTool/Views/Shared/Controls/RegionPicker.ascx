﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.RegionPicker>" %>

<script language="javascript" type="text/javascript">

  function ResetVisibility() {
    $(".state-dropdown-container").addClass("hide");
    $(".zipcode-text-container").addClass("hide");
    $(".city-text-container").addClass("hide");
    $(".multi-city-zip-container").addClass("hide");

    $(".city-prefix-pagination").html("");
    $(".city-names-list").html("");    
    $(".city-click-select-container").addClass("hide");
  }

  function ResetCityList() {
    $(".city-prefix-pagination").html("");
    $(".city-names-list").html("");    
   }

  function ClearUIValues() {
    $(".zipcode-text").val("");
    $(".city-text").val("");
  }

  function SetZipcodeStatus(valid) {
    if (valid == 1) {
      $(".zipcode-status-indicator").html("Zipcode valid");
     }
    else {
      $(".zipcode-status-indicator").html("Zipcode INVALID");
     }
   }
   
   function PopulateCityText(cityName) {
     //alert("PopulateCityText fired");
    $(".city-text").val(cityName);
    var stateRegionID = GetStateRegionID();
    GetRegionIDByCityName($(".hidden-siteid-holder").val(), $(".country-select-dropdown option:selected").val(), stateRegionID, $(".city-text").val());
  }

   function GetCountries(siteID, countryRegionID) {
     $.ajax({
       type: "POST",
       url: "/RegionAPI/GetCountries/" + siteID,
       success: function(result) {
         BuildDropdownOptions(result.SelectListItems, $(".country-select-dropdown"), true);
         if (countryRegionID > 0) {
           $(".country-select-dropdown").val(countryRegionID);
         }
       }
     });
   }

   function GetStateRegionID() {
     var stateRegionID = -1;

     var stateCheck = $(".state-select-dropdown option:selected").val();

     if (stateCheck != null && stateCheck != "")
       stateRegionID = stateCheck;

     return stateRegionID;
   }

   function GetStates(siteID, countryRegionID, selectedValue) {

     showHideLoadingAnimation($(".country-select-dropdown"), "show");

     $.ajax({
       type: "POST",
       url: "/RegionAPI/GetStates/" + siteID,
       data: { CountryRegionID: countryRegionID },
       success: function(result) {
         // determine which control to display next None = 0, Zip = 1, State = 2, City = 3
         if (result.NextToDisplay == 1) {
           $(".zipcode-text-container").removeClass("hide");
         }
         else if (result.NextToDisplay == 2) {
           $(".state-dropdown-container").removeClass("hide");
           $(".city-text-container").removeClass("hide");
           $(".city-click-select-container").removeClass("hide");
           BuildDropdownOptions(result.SelectListItems, $(".state-select-dropdown"), true);
           
           if (selectedValue > 0) {
             $(".state-select-dropdown").val(selectedValue);
           }
         }
         else if (result.NextToDisplay == 3) {
           $(".city-text-container").removeClass("hide");
           $(".city-click-select-container").removeClass("hide");
         }

         showHideLoadingAnimation($(".country-select-dropdown"), "hide");
       }
     });
   }

   function GetRegionIDByZip(siteID, countryRegionID, zipcode, selectedRegionID) {
     $.ajax({
       type: "POST",
       url: "/RegionAPI/GetRegionIDByZip/" + siteID,
       data: { CountryRegionID: countryRegionID, ZipCode: zipcode, IgnoreMultiCity: false },
       success: function(result) {
         if (result.Status == 2) {
           // if regionID came back we are done here
           if (result.RegionID > 0) {
             $(".hidden-regionid-holder").val(result.RegionID);
             SetZipcodeStatus(1);
           }
           else if (result.SelectListItems.length > 0) {
             // multiple cities for a zip
             $(".multi-city-zip-container").removeClass("hide");
             BuildDropdownOptions(result.SelectListItems, $(".multi-city-zip-city-selector"), true);

             if (selectedRegionID > 0) {
               $(".multi-city-zip-city-selector").val(selectedRegionID);
             }
           }
         }
       }

     });
   }

   function GetRegionIDByCityName(siteID, countryRegionID, stateRegionID, cityName) {
     $.ajax({
       type: "POST",
       url: "/RegionAPI/GetRegionID/" + siteID,
       data: { CountryRegionID: countryRegionID, StateRegionID: stateRegionID, CityName: cityName },
       success: function(result) {
       if (result.Status == 2) {
         if (result.RegionID > 0) {
           $(".hidden-regionid-holder").val(result.RegionID);
           alertMessage("City has been validated", result.Status, $(".city-text"));
         }
        }
         else {
           alertMessage(result.StatusMessage, result.Status, $(".city-text"));
          }
       }

     });
   }

   function GetCityPrefixes(siteID, countryRegionID, stateRegionID) {
     $.ajax({
       type: "POST",
       url: "/RegionAPI/GetCityPrefix/" + siteID,
       data: { CountryRegionID: countryRegionID, StateRegionID: stateRegionID },
       success: function(result) {
         if (result.Status == 2) {
           var html = "";
           var stateRegionID = GetStateRegionID();
           for (i = 0; i < result.SelectListItems.length; i++) {
             html += "<a class='city-prefix-click' onclick='GetCitiesForCityPrefix(" + $(".hidden-siteid-holder").val() +
                    ", " + $(".country-select-dropdown").val() +
                    ", " + stateRegionID + ", \"" +
                    result.SelectListItems[i].Text + "\")'>" + result.SelectListItems[i].Text + "</a>, ";
           }
           $(".city-prefix-pagination").html(html);
         }
         else {
           alertMessage(result.StatusMessage, result.Status, $(".city-prefix-pagination"));
         }
       }

     });
   }

   function GetCitiesForCityPrefix(siteID, countryRegionID, stateRegionID, cityPrefix) {
     $.ajax({
       type: "POST",
       url: "/RegionAPI/GetCityList/" + siteID,
       data: { CountryRegionID: countryRegionID, StateRegionID: stateRegionID, CityPrefix: cityPrefix },
       success: function(result) {
         if (result.Status == 2) {
           var html = "";
           for (i = 0; i < result.SelectListItems.length; i++) {
             html += "<a class='city-name-clicked' onclick='PopulateCityText(\"" + result.SelectListItems[i].Text + "\")'>" +
           result.SelectListItems[i].Text + "</a><br/>";
           }
           $(".city-names-list").html(html);
         }
         else {
           alertMessage(result.StatusMessage, result.Status, $(".city-names-list"));
          }
       }

     });
    }

    function BindRegionPicker(siteID, regionID) {
      $.ajax({
        type: "POST",
        url: "/RegionAPI/GetRegionBindInfo/" + siteID,
        data: { RegionID: regionID },
        success: function(result) {
          if (result.Status == 2) {
            // creation of countries dropdown and binding have to happen in one method to ensure the
            // dropdown is created
            GetCountries($(".hidden-siteid-holder").val(), result.CountryRegionID);

            // state could be present or not so check for that
            if (result.StateRegionID > -1) {
              GetStates($(".hidden-siteid-holder").val(), result.CountryRegionID, result.StateRegionID);
            }

            // zipcode
            if (result.ZipCode != "")
              $(".zipcode-text").val(result.ZipCode);

            // cityname
            if (result.CityName != "")
              $(".city-text").val(result.CityName);

            // multi city zip dropdown
            if (result.IsMutiCityZip) {
              GetRegionIDByZip($(".hidden-siteid-holder").val(), result.CountryRegionID, result.ZipCode, result.RegionID);
            }
          }
          else {
            alertMessage(result.StatusMessage, result.Status, $(".country-select-dropdown"));
          }
        }

      });
     }

     $(document).ready(function() {

         ResetVisibility();

         GetCountries($(".hidden-siteid-holder").val());

         if ($(".hidden-regionid-holder").val() > 0) {
             // bind the information here
             BindRegionPicker($(".hidden-siteid-holder").val(), $(".hidden-regionid-holder").val());
         }
         else {
             GetCountries($(".hidden-siteid-holder").val(), -1);
         }

         $(".country-select-dropdown").change(function() {
             ResetVisibility();
             ClearUIValues();
             GetStates($(".hidden-siteid-holder").val(), $(".country-select-dropdown option:selected").val(), -1);
         });

         $(".state-select-dropdown").change(function() {
             ResetCityList();
         });

         $(".zipcode-text").keyup(function() {

             $(".multi-city-zip-container").addClass("hide");

             if ($(".zipcode-text").val().length >= 5 && $(".zipcode-text").val().length <= 7) {
                 var countryRegionID = $(".country-select-dropdown option:selected").val();
                 if (countryRegionID == 223 && $(".zipcode-text").val().length == 5) // USA
                 {
                     GetRegionIDByZip($(".hidden-siteid-holder").val(), $(".country-select-dropdown option:selected").val(), $(".zipcode-text").val(), -1);
                 }
                 else if (countryRegionID == 38 && ($(".zipcode-text").val().length == 7 || $(".zipcode-text").val().length == 6)) {  // Canada
                     GetRegionIDByZip($(".hidden-siteid-holder").val(), $(".country-select-dropdown option:selected").val(), $(".zipcode-text").val(), -1);
                 }
             }
             else {
                 SetZipcodeStatus(0);
             }
         });

         $(".city-text").change(function() {
             var stateRegionID = GetStateRegionID();
             GetRegionIDByCityName($(".hidden-siteid-holder").val(), $(".country-select-dropdown option:selected").val(), stateRegionID, $(".city-text").val());

         });

         $(".multi-city-zip-city-selector").change(function() {
             $(".hidden-regionid-holder").val($(".multi-city-zip-city-selector option:selected").val());
             SetZipcodeStatus(1);
         });

         $(".lookup-city-click").click(function() {
             var stateRegionID = GetStateRegionID();
             if (stateRegionID < 0 && $(".state-dropdown-container").is(":visible")) {
                 alertMessage("Please select a state to retrieve the list of cities", 3, $(".lookup-city-click"));
             }
             else {
                 GetCityPrefixes($(".hidden-siteid-holder").val(), $(".country-select-dropdown option:selected").val(), stateRegionID);
             }

             //get the position of the placeholder element
             var pos = $(this).offset();
             var width = $(this).width();

             //show the menu directly over the placeholder
             //$("#regionControl").css({ "left": (pos.left) + "px", "top": pos.top + "px" });

             //show the hidden div.
             $("#regionControl").show();

         });

         $("#closeRegionSelector").click(function() {

            $("#regionControl").hide();

         });

     });
</script>

    <input type="hidden" class="hidden-siteid-holder" value="<%= Model.SiteID %>" />
    <input type="hidden" class="hidden-regionid-holder" name="<%= Model.FormFieldName %>" value="<%= Model.RegionID %>" />

    <ul>
         <li class="list-item-edit">
            <label>Country:</label>
            <span style="padding-left:20px;"><select class="country-select-dropdown"></select></span>
         </li>   
     </ul>
    
    <div class="state-dropdown-container">
    <ul>
        <li class="list-item-edit">
            <label>State:</label>
            <span style="padding-left:20px;"><select class="state-select-dropdown"></select></span>
        </li>
    </ul>
    </div>

    <div class="zipcode-text-container">
    <ul>
        <li class="list-item-edit">
            <label>Zip:</label>
            <span style="padding-left:20px;"><input type="text" class="zipcode-text" /></span> 
            <span class="zipcode-status-indicator"  style="padding-left:5px; padding-top:5px;" ></span>
        </li>
    </ul>
    </div>

    <div class="city-text-container">
    <ul>
        <li class="list-item-edit">
            <label>City:</label>
            <span style="padding-left:20px;">
                <div style=" text-align:left">
                    <div class="city-click-select-container" style="width:50%">
                        <span class="lookup-city-click"> Look up city starting with: </span>
                    </div>
                    <div style="float:left">
                        <input type="text" id="city-text" class="city-text" />
                    </div>
                     <div style="float:left;text-align:left">
                          <div id="regionControl" style="display:none; padding-left:5px;">
                                <div style=" z-index:10000; border:solid 1px grey; width:300px;">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                <div id="closeRegionSelector" style=" text-align:right; text-decoration:underline; color:Blue; cursor:pointer;">close</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="city-prefix-pagination" style=" border-bottom: solid 1px grey; padding-top:5px; padding-bottom:5px;"></div> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="city-names-list" style="overflow:scroll; height:300px;  padding-top:5px; padding-bottom:5px;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
            </span>
            
        </li>
     </ul>   
    </div>

    <div class="multi-city-zip-container">
    <ul>
        <li class="list-item-edit">
            <label>City:</label>
             <span style="padding-left:20px;"><select class="multi-city-zip-city-selector"></select></span>
         </li>
    </ul>     
    </div>



   