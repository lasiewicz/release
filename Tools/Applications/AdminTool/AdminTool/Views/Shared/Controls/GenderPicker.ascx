﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.GenderPicker>" %>

<script language="javascript" type="text/javascript">
  function UpdateGenderMaskValueHolder() {
    var gender = $("input[name='gpGender']:checked").val();
    var genderSeeking = $("input[name='gpSeekingGender']:checked").val();

    $(".hidden-gendermask-value-holder").val(gender | genderSeeking);
   }

   $(document).ready(function() {
   $("input[name='gpGender']").change(function() { UpdateGenderMaskValueHolder(); });
   $("input[name='gpSeekingGender']").change(function() { UpdateGenderMaskValueHolder(); });
   });
</script>
 
<input type="hidden" class="hidden-gendermask-value-holder" name="<%= Model.FormFieldName %>" value="<%= Model.GenderMaskValue %>" />

<ul>
    <li class="list-item-edit">
     <label>Gender:</label>
         <span style="padding-left:10px;">
            <table><tr>
                <%foreach(var item in Model.GenderOptions) 
                  { %>
                  <td>
                    <%if(item.Selected) { %>
                    <input type="radio" name="gpGender" class="genderpicker-gender-selection" value="<%=item.Value %>" checked="checked" /><%=item.Text %> 
                    <%} else { %>
                    <input type="radio" name="gpGender" class="genderpicker-gender-selection" value="<%=item.Value %>" />   <%=item.Text %>
                    <%} %>
                   &nbsp;</td>
                <%} %>
            </tr></table>
        </span>
    </li>
</ul>

<ul>
    <li class="list-item-edit">
     <label>Seeking Gender:</label>
     <span style="padding-left:10px;">
            <table><tr>
                <%foreach(var item in Model.SeekingGenderOptions) { %>
                <td>
                <%if(item.Selected) { %>
                <input type="radio" name="gpSeekingGender" class="genderpicker-seeking-gender-selection" value="<%=item.Value %>" checked="checked" /><%=item.Text %>
                <%} else { %>
                <input type="radio" name="gpSeekingGender" class="genderpicker-seeking-gender-selection" value="<%=item.Value %>" /><%=item.Text %>
                <%} %>
                &nbsp; </td>
                <%} %>
            </tr></table>
            </span>
     </li>
</ul>

