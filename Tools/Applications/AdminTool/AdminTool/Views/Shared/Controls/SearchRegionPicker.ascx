﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.SearchRegionPicker>" %>
<script language="javascript" type="text/javascript">

  var SearchTypeEnum = { "PostalCode": "PostalCode", "AreaCode": "AreaCode", "Region": "Region", "College": "College" }

  function ResetVisibility() {
    $(".zipcode-tab-content").addClass("hide");
    $(".areacode-tab-content").addClass("hide");
    $(".region-city-tab-content").addClass("hide");
    $(".search-region-selector-general-controls").addClass("hide");
  }

  function BuildDropdownOptions(listItems, dropdownToAppendTo, insertBlank) {
    // wipe everything
    dropdownToAppendTo.html("");

    // insert blank as the first choice if wanted
    if (insertBlank) {
      var blank = new Option("", "");
      $(blank).html("");
      $(dropdownToAppendTo).append(blank);
    }

    for (i = 0; i < listItems.length; i++) {
      var o = new Option(listItems[i].Text, listItems[i].Value);
      $(o).html(listItems[i].Text);
      $(dropdownToAppendTo).append(o);
    }

  }
  
  function GetCountries(siteID, countryRegionID) {
    $.ajax({
      type: "POST",
      url: "/RegionAPI/GetCountries/" + siteID,
      success: function(result) {
        BuildDropdownOptions(result.SelectListItems, $(".country-select-dropdown"), true);
        if (countryRegionID > 0) {
          $(".country-select-dropdown").val(countryRegionID);
        }
      }
    });
  }

  function GetStates(siteID, countryRegionID, stateRegionID) {

    showHideLoadingAnimation($(".country-select-dropdown"), "show");

    $.ajax({
      type: "POST",
      url: "/RegionAPI/GetStates/" + siteID,
      data: { CountryRegionID: countryRegionID, Caller: "editsearch" },
      success: function(result) {
        // determine which control to display next None = 0, Zip = 1, State = 2, City = 3 / we shouldn't get 1 back for this control
        if (result.NextToDisplay == 2) {
          $(".state-dropdown-container").removeClass("hide");
          $(".city-text-container").removeClass("hide");
          BuildDropdownOptions(result.SelectListItems, $(".state-select-dropdown"), true);
          $(".city-click-select-container").removeClass("hide");
          if (stateRegionID > 0) {
            $(".state-select-dropdown").val(stateRegionID);
           }
        }
        else if (result.NextToDisplay == 3) {
          $(".city-text-container").removeClass("hide");
          $(".city-click-select-container").removeClass("hide");
        }

        showHideLoadingAnimation($(".country-select-dropdown"), "hide");
      }
    });
  }

  function BindRegionOrZipTab(siteID, regionID) {
    $.ajax({
      type: "POST",
      url: "/RegionAPI/GetRegionBindInfo/" + siteID,
      data: { RegionID: regionID },
      success: function(result) {
        if (result.Status == 2) {
          if ($(".hidden-searchtype-holder").val() == SearchTypeEnum.Region) {
//            $(".region-city-tab-content").removeClass("hide");
            // creation of countries dropdown and binding have to happen in one method to ensure the
            // dropdown is created
            GetCountries($(".hidden-siteid-holder").val(), result.CountryRegionID);

            // state could be present or not so check for that
            if (result.StateRegionID > -1) {
              GetStates($(".hidden-siteid-holder").val(), result.CountryRegionID, result.StateRegionID);
            }

            // cityname
            if (result.CityName != "")
              $(".city-text").val(result.CityName);
          }
          else if ($(".hidden-searchtype-holder").val() == SearchTypeEnum.PostalCode) {
//            $(".zipcode-tab-content").removeClass("hide");

            $(".zipcode-country-select-dropdown").val(result.CountryRegionID);

            if (result.ZipCode != "")
              $(".zipcode-input-text").val(result.ZipCode);
          }

        } // end of success if
        else {
          alertMessage(result.StatusMessage, result.Status, $(".country-select-dropdown"));
        }
      }

    });
  }

  function BindAreaCodes() {
//    $(".areacode-tab-content").removeClass("hide");
    $(".areacode-country-select-dropdown").val($(".hidden-countryregionid-holder").val());

    var areaCodes = $(".hidden-regionid-holder").val();
    var s = areaCodes.split(",");

    $(".areacode-input-text").each(function(index) {
      if (s[index]) {
        $(this).val(s[index]);
      }
    });

   }

   function BindSearchRegionPicker() {
      if ($(".hidden-searchtype-holder").val == "AreaCode") {
        BindAreaCodes();
       }
      else {
        BindRegionOrZipTab($(".hidden-siteid-holder").val(), $(".hidden-regionid-holder").val());
      }

      CaptureLocationString();
   }

   function ChangeTab(tabID) {
     ResetVisibility();
     if (tabID == 1) {
       $(".zipcode-tab-content").removeClass("hide");
     }
     else if (tabID == 2) {
       $(".areacode-tab-content").removeClass("hide");
     }
     else if (tabID == 3) {
       $(".region-city-tab-content").removeClass("hide");
       if ($(".country-select-dropdown option").size() < 1) {
         GetCountries($(".hidden-siteid-holder").val(), -1);
       }
     }

     if ($(".hidden-lock-search-type").val().toLowerCase() == "true") {
       $(".save-region-button").removeClass("hide");
     }
     else {
       $(".search-region-selector-general-controls").removeClass("hide");
     }
    
    }

    function GetAreaCodeString() {
      var areaCodes = "";
      $(".areacode-input-text").each(function() {
        if ($(this).val() != "") {
          areaCodes += $(this).val() + ",";
        }
      });

      if (areaCodes.length > 0) {
        areaCodes = areaCodes.substring(0, areaCodes.length-1);
      }

      return areaCodes;
     }

     function CaptureLocationString() {
      if ($(".hidden-searchtype-holder").val() == SearchTypeEnum.AreaCode) {
        var areaCodes = GetAreaCodeString();
        $(".selected-location-name-holder").html(areaCodes);
        ResetVisibility();
      }
      else {
        var regionID = $(".hidden-regionid-holder").val();
        var siteID = $(".hidden-siteid-holder").val();
        $.ajax({
          type: "POST",
          url: "/RegionAPI/GetRegionString/" + siteID,
          data: { RegionID: regionID },
          success: function(result) {
            if (result.Status == 2) {
              $(".selected-location-name-holder").html(result.RegionDisplayString);
              ResetVisibility();
            }
          }
        });  
       }  
    }

    function SaveZip() {
      var siteID = $(".hidden-siteid-holder").val();
      var countryRegionID = $(".zipcode-country-select-dropdown option:selected").val();
      var zipcode = $(".zipcode-input-text").val();

      $.ajax({
        type: "POST",
        url: "/RegionAPI/GetRegionIDByZip/" + siteID,
        data: { CountryRegionID: countryRegionID, ZipCode: zipcode, IgnoreMultiCity: true },
        success: function(result) {
          if (result.Status == 2) {
            SetFormFields(SearchTypeEnum.PostalCode, countryRegionID, result.RegionID);
            CaptureLocationString();
          }
          else {
            alertMessage(result.StatusMessage, result.Status, $(".save-region-button"));
          }
        }
      });
     }

     function SaveAreaCode() {
       var siteID = $(".hidden-siteid-holder").val();
       var countryRegionID = $(".areacode-country-select-dropdown option:selected").val();
       var areaCodes = GetAreaCodeString();

       $.ajax({
         type: "POST",
         url: "/RegionAPI/ValidateAreaCodes/" + siteID,
         data: { AreaCodes: areaCodes, CountryRegionID: countryRegionID },
         success: function(result) {
           if (result.Status == 2) {
             SetFormFields(SearchTypeEnum.AreaCode, countryRegionID, areaCodes); // we are actually using the RegionID holder to store the comma separated area codes in this case
             CaptureLocationString();
           }
           else {
             alertMessage(result.StatusMessage, result.Status, $(".save-region-button"));
           }
         }
       });
     }

     function SaveRegionCity() {
       var siteID = $(".hidden-siteid-holder").val();
       var countryRegionID = $(".country-select-dropdown option:selected").val();
       var stateRegionID = $(".state-select-dropdown option:selected").val();
       var cityName = $(".city-text").val();

       $.ajax({
         type: "POST",
         url: "/RegionAPI/GetRegionID/" + siteID,
         data: { CountryRegionID: countryRegionID, StateRegionID: stateRegionID, CityName: cityName },
         success: function(result) {
         if (result.Status == 2) {
           SetFormFields(SearchTypeEnum.Region, countryRegionID, result.RegionID);
             CaptureLocationString();
           }
           else {
             alertMessage(result.StatusMessage, result.Status, $(".save-region-button"));
           }
         }
       });
     }

    function SetFormFields(searchType, countryRegionID, regionID) {
      $(".hidden-searchtype-holder").val(searchType);
      $(".hidden-countryregionid-holder").val(countryRegionID);
      $(".hidden-regionid-holder").val(regionID);
     }

    function SaveRegion() {
      if ($(".zipcode-tab-content").is(":visible")) {
        SaveZip();
       }
      else if ($(".areacode-tab-content").is(":visible")) {
        SaveAreaCode();
       }
      else if ($(".region-city-tab-content").is(":visible")) {
        SaveRegionCity();
       }
     }

     function GetStateRegionID() {
       var stateRegionID = -1;

       var stateCheck = $(".state-select-dropdown option:selected").val();

       if (stateCheck != null && stateCheck != "")
         stateRegionID = stateCheck;

       return stateRegionID;
     }

     function GetCityPrefixes(siteID, countryRegionID, stateRegionID) {
       $.ajax({
         type: "POST",
         url: "/RegionAPI/GetCityPrefix/" + siteID,
         data: { CountryRegionID: countryRegionID, StateRegionID: stateRegionID },
         success: function(result) {
           if (result.Status == 2) {
             var html = "";
             var stateRegionID = GetStateRegionID();
             for (i = 0; i < result.SelectListItems.length; i++) {
               html += "<a class='city-prefix-click' onclick='GetCitiesForCityPrefix(" + $(".hidden-siteid-holder").val() +
                    ", " + $(".country-select-dropdown").val() +
                    ", " + stateRegionID + ", \"" +
                    result.SelectListItems[i].Text + "\")'>" + result.SelectListItems[i].Text + "</a>, ";
             }
             $(".city-prefix-pagination").html(html);
           }
           else {
             alertMessage(result.StatusMessage, result.Status, $(".city-prefix-pagination"));
           }
         }

       });
     }

     function GetCitiesForCityPrefix(siteID, countryRegionID, stateRegionID, cityPrefix) {
       $.ajax({
         type: "POST",
         url: "/RegionAPI/GetCityList/" + siteID,
         data: { CountryRegionID: countryRegionID, StateRegionID: stateRegionID, CityPrefix: cityPrefix },
         success: function(result) {
           if (result.Status == 2) {
             var html = "";
             for (i = 0; i < result.SelectListItems.length; i++) {
               html += "<a class='city-name-clicked' onclick='PopulateCityText(\"" + result.SelectListItems[i].Text + "\")'>" +
           result.SelectListItems[i].Text + "</a><br />";
             }
             $(".city-names-list").html(html);
           }
           else {
             alertMessage(result.StatusMessage, result.Status, $(".city-names-list"));
           }
         }

       });
     }

     function PopulateCityText(cityName) {
       $(".city-text").val(cityName);
     }

     function ResetRegionCityControls() {
       $(".state-dropdown-container").addClass("hide");
       $(".city-text-container").addClass("hide");
       $(".city-click-select-container").addClass("hide");
       ResetCityClickSelect(); 
      }

     function ResetCityClickSelect() {
       $(".city-text").val("");
       $(".city-prefix-pagination").html("");
       $(".city-names-list").html("");
     }

     function ActivateCurrentTab() {
       var searchType = $(".hidden-searchtype-holder").val();

       if (searchType == SearchTypeEnum.PostalCode) {
         ChangeTab(1);
       }
       else if (searchType == SearchTypeEnum.AreaCode) {
         ChangeTab(2);
       }
       else if (searchType == SearchTypeEnum.Region) {
         ChangeTab(3);
       }
     }

     $(document).ready(function() {

         ResetVisibility();

         BindSearchRegionPicker();

         $(".save-region-button").click(function() {
             SaveRegion();
         });

         $(".country-select-dropdown").change(function() {
             ResetRegionCityControls();
             GetStates($(".hidden-siteid-holder").val(), $(".country-select-dropdown option:selected").val(), -1);
         });

         $(".state-select-dropdown").change(function() {
             ResetCityClickSelect();
         });

         $(".lookup-city-click").click(function() {
             var stateRegionID = GetStateRegionID();
             if (stateRegionID < 0 && $(".state-dropdown-container").is(":visible")) {
                 alertMessage("Please select a state to retrieve the list of cities", 3, $(".lookup-city-click"));
             }
             else {
                 GetCityPrefixes($(".hidden-siteid-holder").val(), $(".country-select-dropdown option:selected").val(), stateRegionID);
             }

             //show the hidden div.
             $("#regionControl").show();

         });

         $(".selected-location-name-holder").click(function() {

             ActivateCurrentTab();
             $("#searchRegionSelector").show();
             $("#mainDiv").show();

         });


         $("#closeRegionSelector").click(function() {

         $("#regionControl").hide();
         $("#mainDiv").hide();

         });

     });
</script>
<style>
    legend {
         font-size:small;
        }
    .fieldsetSmaller
    {
        width: 500px;
        padding-top:10px;
       margin-top:5px;
    }    
    .city-click-select-container
    {
        padding-top:5px;
    }
    .city-text-container
    {
        padding-bottom : 20px;
    }
    
</style>
<input type="hidden" class="hidden-siteid-holder" value="<%= Model.SiteID %>" />
<input type="hidden" class="hidden-searchtype-holder" name="<%= Model.SearchTypeFormFieldName %>" value="<%= Model.SearchType %>" />
<input type="hidden" class="hidden-countryregionid-holder" name="<%= Model.CountryRegionIDFormFieldName %>" value="<%= Model.CountryRegionID %>" />
<input type="hidden" class="hidden-regionid-holder" name="<%= Model.FormFieldName %>" value="<%= Model.RegionID %>" />
<input type="hidden" class="hidden-lock-search-type" value="<%= Model.LockSearchType %>" />

<a class="selected-location-name-holder"></a>

<div id="mainDiv" style="display:none;" class="main-div">

<a onclick="ChangeTab(1)" class="search-region-selector-general-controls search-region-selector-tabs">Zip Code</a>
<a onclick="ChangeTab(2)" class="search-region-selector-general-controls search-region-selector-tabs">Area Code</a>
<a onclick="ChangeTab(3)" class="search-region-selector-general-controls search-region-selector-tabs">Region/City</a> 

<div id="searchRegionSelector" style="display:none">
<fieldset class="fieldsetSmaller"> 
<legend>  </legend>

<%--Zipcode tab--%>
<div class="zipcode-tab-content">
<ul>
    <li class="list-item-edit">
        <label>Country:</label>
        <span style="padding-left:10px;">
            <select class="zipcode-country-select-dropdown">
                <option value="223">USA</option>
                <option value="38">Canada</option>
            </select>
        </span>
    </li>
    <li class="list-item-edit">
         <label>Zip Code:</label>
        <span style="padding-left:10px;"><input type="text" class="zipcode-input-text" /></span>
    </li>
</ul>

</div>

<%--Area codes tab--%>
<div class="areacode-tab-content">
<ul>
    <li class="list-item-edit">
        <label>Conntry:</label>
        <span style="padding-left:10px;">
            <select class="areacode-country-select-dropdown">
                <option value="223">USA</option>
                <option value="38">Canada</option>
            </select>
        </span>
    </li>
    <li class="list-item-edit">
        <label>
            Area Code:
        </label>
       <span style="padding-left:10px;">
            <input type="text" class="areacode-input-text" />
        </span>
    </li>
</ul>
</div>

<%--Region/City tab--%>
<div class="region-city-tab-content">

<ul>
    <li class="list-item-edit">
        <label>Country:</label>
        <span style="padding-left:20px;"><select class="country-select-dropdown"></select></span>
    </li>
</ul>

<div class="state-dropdown-container">
    <ul>
        <li class="list-item-edit">
            <label>State:</label>
            <span style="padding-left:20px;"><select class="state-select-dropdown"></select></span>
        </li>
    </ul>
</div>

<div class="city-text-container">
    <ul>
        <li class="list-item-edit">
            <label>City:</label>
            <span style="padding-left:20px;"><input type="text" class="city-text" /> <span class="lookup-city-click">Lookup City</span></span>
        </li>
    </ul>
</div>

<div class="city-click-select-container">
  <div id="regionControl" style="display:none; padding-left:5px;">
    <div style="width:300px;">
        <table width="100%">
            <tr>
                <td align="right">
                    <div id="closeRegionSelector" style=" text-align:right; text-decoration:underline; color:Blue; cursor:pointer;">close</div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="city-prefix-pagination" style=" border-bottom: solid 1px grey; padding-top:5px; padding-bottom:5px;"></div> 
                </td>
            </tr>
            <tr>
                <td>
                    <div class="city-names-list" style="overflow:scroll; height:300px;  padding-top:5px; padding-bottom:5px;">
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
  
  
</div>
</div>
<div style="padding-left:200px; padding-top:10px; float:left;">
<input type="button" class="save-region-button search-region-selector-general-controls button" value="Update Location" />
</div>
</fieldset>


</div>

</div>