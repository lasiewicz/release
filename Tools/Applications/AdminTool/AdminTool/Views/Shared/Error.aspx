﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<System.Web.Mvc.HandleErrorInfo>" %>

<asp:Content ID="errorTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Error
</asp:Content>

<asp:Content ID="errorHead" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="errorContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Sorry, an error occurred while processing your request.
    </h2>
    
    <%= ViewData.ContainsKey(AdminTool.Models.WebConstants.ERROR_KEY) ? ViewData[AdminTool.Models.WebConstants.ERROR_KEY].ToString() : ""%>
</asp:Content>
