﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.TrainingTool.TTTestResult>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.TrainingTool" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	TTTestResults
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .bordered-table tr td {
            border: 1px solid black;
            padding: 5px;
        }
        .bordered-table th {
            border: 1px solid black;
            padding: 5px;
        }
        .ttTestResults div {
            float: right;
            margin: 10px;
        }
    </style>

<% if (Model.TestAnswerGroups == null)
   { %>
    NO TEST RESULT
<% }
   else
   { %>
    
<div class="ttTestResults">
    
    
    <h2>FTA Text Training</h2>
    <table>
        <tr>
            <td>Agent:</td>
            <td><%= Model.AgentEmail %><br/><a href="/TrainingTool/TTTraineeStart/<%= Model.AgentMemberID %>">[see training history]</a></td>
        </tr>
        <tr>
            <td>Time started:</td>
            <td><%= Model.TimeStarted.ToString() %></td>
        </tr>
        <tr>
            <td>Time completed:</td>
            <td><%= Model.TimeEnded.ToString() %></td>
        </tr>
        <tr>
            <td>Total test time:</td>
            <td><%=Model.TotalTestTime %>&nbsp;<%= Model.TotalTestTimeMinutes %>min&nbsp;<%= Model.TotalTestTimeSeconds %>sec</td>
        </tr>
        <tr>
            <td>Bucket:</td>
            <td><%= Model.GroupingName %></td>
        </tr>
        <tr>
            <td>Total items:</td>
            <td><%= Model.TotalTestItemCount.ToString() %></td>
        </tr>
        <tr>
            <td>Profiles per Hour:</td>
            <td><%= Model.ProfilesPerHour.ToString("F")%></td>
        </tr>
    </table>
    
    <br/>
    <ul>
        <li>Suspend:&nbsp;<%= Model[Constants.GROUP_NAME_SUSPENSION].PointsCorrect%>/<%= Model[Constants.GROUP_NAME_SUSPENSION].PossiblePoints%></li>
        <li>Email Warnings:&nbsp;<%= Model[Constants.GROUP_NAME_EMAIL_WARNING].PointsCorrect%>/<%= Model[Constants.GROUP_NAME_EMAIL_WARNING].PossiblePoints%></li>
        <li>Content Review:&nbsp;<%= Model[Constants.GROUP_NAME_CONTENT_REVIEW].PointsCorrect%>/<%= Model[Constants.GROUP_NAME_CONTENT_REVIEW].PossiblePoints%></li>
    </ul>
    

   <%-- <div>
    <h2>Approve or Suspend</h2>
    <table class="bordered-table">
        <tr>
            <th></th>
            <th>Correct</th>
            <th>Incorrect</th>
            <th colspan="2"></th>
        </tr>
        <tr>
            <td>Approved</td>
            <td><%= Model[Constants.GROUP_NAME_APPROVESUSPEND].GetCount(Constants.SUBGROUP_NAME_APPROVE, true) %></td>
            <td><%= Model[Constants.GROUP_NAME_APPROVESUSPEND].GetCount(Constants.SUBGROUP_NAME_APPROVE, false) %></td>
            <td><%= Model[Constants.GROUP_NAME_APPROVESUSPEND][Constants.SUBGROUP_NAME_APPROVE].CorrectPercent %>%</td>
            <td rowspan="2"><%= Model[Constants.GROUP_NAME_APPROVESUSPEND].TotalCorrectPercent %>%</td>
        </tr>
        <tr>
            <td>Suspended</td>
            <td><%= Model[Constants.GROUP_NAME_APPROVESUSPEND].GetCount(Constants.SUBGROUP_NAME_SUSPEND, true) %></td>
            <td><%= Model[Constants.GROUP_NAME_APPROVESUSPEND].GetCount(Constants.SUBGROUP_NAME_SUSPEND, false) %></td>
            <td><%= Model[Constants.GROUP_NAME_APPROVESUSPEND][Constants.SUBGROUP_NAME_SUSPEND].CorrectPercent %>%</td>
        </tr>
    </table>
    <br/>
    
    <h2>Send Warnings</h2>
    <table class="bordered-table">
         <tr>
            <th></th>
            <th>Correct</th>
            <th>Incorrect</th>
            <th colspan="2"></th>
        </tr>
        <% for (int i = 0; i < Model[Constants.GROUP_NAME_SENDWARNINGS].TestResultLineItems.Count(); i++)
           {
               TestResultLineItem item = Model[Constants.GROUP_NAME_SENDWARNINGS][i];
        %>
              <tr>
                  <td><%= item.DisplayName %></td>
                  <td><%= item.CorrectCount %></td>
                  <td><%= item.IncorrectCount %></td>
                  <td><%= item.CorrectPercent %>%</td>
                  
                  <% if (i == 0)
                     { %>
                     <td rowspan="<%= Model[Constants.GROUP_NAME_SENDWARNINGS].TestResultLineItems.Count() %>">
                         <%= Model[Constants.GROUP_NAME_SENDWARNINGS].TotalCorrectPercent %>%
                     </td>    
                  <% } %>
              </tr> 
           <% } %>
    </table>
    <br/>

    <h2>Good from Bad Essays</h2>
    <table class="bordered-table">
         <tr>
            <th></th>
            <th>Correct</th>
            <th>Incorrect</th>
            <th colspan="2"></th>
        </tr>
        <% for (int i = 0; i < Model[Constants.GROUP_NAME_BADGOODESSAY].TestResultLineItems.Count(); i++)
           {
               TestResultLineItem item = Model[Constants.GROUP_NAME_BADGOODESSAY][i];
        %>
              <tr>
                  <td><%= item.DisplayName %></td>
                  <td><%= item.CorrectCount %></td>
                  <td><%= item.IncorrectCount %></td>
                  <td><%= item.CorrectPercent %>%</td>
                  
                  <% if (i == 0)
                     { %>
                     <td rowspan="<%= Model[Constants.GROUP_NAME_BADGOODESSAY].TestResultLineItems.Count() %>">
                         <%= Model[Constants.GROUP_NAME_BADGOODESSAY].TotalCorrectPercent %>%
                     </td>    
                  <% } %>
              </tr> 
           <% } %>
    </table>
    <br />
    
    <h2>Essay Violations</h2>
    <table class="bordered-table">
         <tr>
            <th></th>
            <th>Correct</th>
            <th>Incorrect</th>
            <th colspan="2"></th>
        </tr>
        <% for (int i = 0; i < Model[Constants.GROUP_NAME_ESSAYVIOLATION].TestResultLineItems.Count(); i++)
           {
               TestResultLineItem item = Model[Constants.GROUP_NAME_ESSAYVIOLATION][i];
        %>
              <tr>
                  <td><%= item.DisplayName %></td>
                  <td><%= item.CorrectCount %></td>
                  <td><%= item.IncorrectCount %></td>
                  <td><%= item.CorrectPercent %>%</td>
                  
                  <% if (i == 0)
                     { %>
                     <td rowspan="<%= Model[Constants.GROUP_NAME_ESSAYVIOLATION].TestResultLineItems.Count() %>">
                         <%= Model[Constants.GROUP_NAME_ESSAYVIOLATION].TotalCorrectPercent %>%
                     </td>    
                  <% } %>
              </tr> 
           <% } %>
    </table>
    <br />
    </div>--%>
</div>

<% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
