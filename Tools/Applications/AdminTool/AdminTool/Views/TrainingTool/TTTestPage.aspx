﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.TrainingTool.TTTestPage>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.TrainingTool" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	TTTestPage
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style> 
        
        #main 
        {
            margin-left: 0 !important;
        }
	span{
			font-size:12px;
			
		}
		
    ol, ul
    {
        list-style: none;
    }
        
    .ttCreateGrouping
       {
           border: 1px solid black;
           margin:10px;
           padding:5px;
           -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            width:500px; 
            float:left;
            font-weight:600;
           font-size:12px;
           width:100%; 
           clear:both; 
           height: auto;
       }
       
     
    
    .mainDivTT
    {
        float: left; 
        width:1000px; 
    } 
    
    .mainDivTT .ttCreateGrouping:nth-child(odd) 
    {
         background-color:#F2F2F2;
    }
    
       
</style>

<script type="text/javascript"> 

    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        theme_advanced_buttons1: "undo,redo",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "bottom",
        theme_advanced_toolbar_align: "left"
    });


    $(document).ready(function () {
        $('select.reasonsStyle').multiselect();

        //attach validation event
        attachValidationEvent();

        //if include is not selected uncheck 
        uncheckRadionForNonIncluded();

        checklistClear();

        $('#MoveToGroupingIDButton').click(function (event) {
            MoveTTMemberProfile(event, $(this), $('#TTMemberProfileID').val(), $('#MemberID').val(), $('#MoveToGroupingID').val());
        });

        $('#btnPauseTest').click(function (event) {
            PauseTestSession(event, $(this), $('#TTTestInstanceID').val(), $('#AdminID').val());
        });

    });

    function checklistClear() {
        $("#sendNoWarning").click(function () {
            if ($(this).is(":checked")) {
                $("#WarningContactInfo,#WarningInappropriateContent,#WarningGeneralViolation").attr("checked", false);
            }
        });

        $("#WarningContactInfo,#WarningInappropriateContent,#WarningGeneralViolation").click(function () {
            if ($(this).is(":checked") && $("#sendNoWarning").is(":checked")) {
                $("#sendNoWarning").attr("checked", false);
            }
        });

        $('#MoveToGroupingIDButton').click(function (event) {
            MoveTTMemberProfile(event, $(this), $('#TTMemberProfileID').val(), $('#MemberID').val(), $('#MoveToGroupingID').val());
        });
    }

    function uncheckRadionForNonIncluded() {


        $('input:radio[id^="TextAcceptable"]').each(function () {

            var index = $(this).attr('id').substring(14);

            if (!$('#IncludeInTest' + index).is(':checked')) {

                if ($(this).is(':checked')) {

                    $(this).removeAttr('checked');
                }
            }

        });



    }


    function attachValidationEvent() {

        $('#testApprove, #testSuspend').click(function (e) {
            
            var valid = true;
            var selectWarning = false;
            var isBadSelectedCounter = 0;
            var includeErrorCounter = 0;

            $("#warningWarning").hide();


            $('input:radio[id^="TextAcceptable"]').each(function () {

                var index = $(this).attr('id').substring(14);
                $('#warningGoodBad' + index).hide();
                $('#TextUnAcceptableValidation' + index).hide();

                    if ($('input[name=TextAcceptable' + index + ']:checked').length) {

                        if ($(this).is(':checked') && $(this).val() == 'true') {
                            //make sure either a good or a bad is selected. 
                        }

                        if ($(this).is(':checked') && $(this).val() == "false") {
                            // if a bad is selected make sure a reason is selected.

                            if ($('input[name=multiselect_TextUnacceptableReasons' + index + '][aria-selected=true]').length == 0) {
                                $('#TextUnAcceptableValidation' + index).show();
                                valid = false;
                                window.scrollTo(0, 0);
                                e.preventDefault();
                            }

                            isBadSelectedCounter++;
                        }


                    } else {

                        $('#warningGoodBad' + index).show();
                        includeErrorCounter++;
                    }

                

            });

            if (includeErrorCounter > 0) {
                valid = false;
                window.scrollTo(0, 0);
                e.preventDefault();
            }
            else {

                if (isBadSelectedCounter > 0) { //lets make sure a warning is selected. 

                    var isSelectedWarning = false;

                    $("#warnings").find(":input[type=checkbox]").each(function () {

                        if ($(this).is(":checked")) {
                            isSelectedWarning = true;
                        }

                    });

                    if (!isSelectedWarning) {
                        $("#warningWarning").show();
                    }

                    valid = isSelectedWarning;
                }
            }
            return valid;
        });
    }

    var int = self.setInterval(function () { callKeepalivePulse() }, 300000);

    function callKeepalivePulse() {
        SendKeepalivePulse($('#TTTestInstanceID').val());
    }

    function toggleTextSuspendPanel() {
        if ($('#SuspendReasons').is(':visible')) {
            $('#SuspendReasons').hide();
        }
        else {
            $('#SuspendReasons').show();
        }
    }

    function validateTTTest() {
        return true;
    }

    function validateTTTestForSuspend() {
        $('#Suspend').val('true');
        return true;
    }


</script>


<h2>TTTestPage</h2>

<% if (Model.TTMemberAttributeTexts == null || Model.TTMemberAttributeTexts.Count == 0)
   { %><a href="/TrainingTool/TTTestHistory/<%=Model.g.AdminID %>">View My Test Results</a>
<% }
   else
   { %>

<% Html.BeginForm("SaveAdminAnswer", "TrainingTool"); %>
<%--This string of MemberAttributeTextIDs will come in handy if you need to know which attributes are displayed on the page. Can be used for validation and etc.--%>
<%= Html.HiddenFor(m => m.TTMemberAttributeTextIDsString) %>
<%= Html.HiddenFor(m => m.TTMemberProfileID) %>
<%= Html.HiddenFor(m => m.Suspend) %>
<%= Html.HiddenFor(m => m.TTTestInstanceID) %>
<%= Html.HiddenFor(m => m.IsLastQuestion) %>
<%= Html.Hidden("AdminID", Model.g.AdminID) %>
<%= Html.HiddenFor(m=>m.TTAnswerMemberProfileVersion) %>
<%= Html.HiddenFor(m=>m.TTAnswerSuspendReasonsVersion) %>

<div class="mainDivTT">
<input type="button" value="Pause" id="btnPauseTest" />

<% Html.RenderPartial("Controls/TTExpanedUserInfo", Model.ExpandedUserInfo); %>

 
    <% foreach (TTMemberAttributeTextUI text in Model.TTMemberAttributeTexts)
       { %>
        <%= Html.Hidden("TextAnswerVersion" + text.TTMemberAttributeTextID, text.AnswerVersion) %>
        <div class="ttCreateGrouping">
          
            <div style="display:inline; float:inherit;">
                   <span style="font-size:12px; margin-bottom:10px;"><%= text.MemberAttributeName %>:</span>
                        <%= Html.TextArea("Attribute" + text.TTMemberAttributeTextID, text.FieldValue, new {style = "width: 700px;height:" + text.TextboxHeight + "px", @class = " "}) %>
             </div>
            <div style=" float:right; display:inline;">
                <div style="width:100%; font-size:16px;padding-bottom:5px; font-size:11px;">       
                   <span id="warningGoodBad<%= text.TTMemberAttributeTextID.ToString() %>"  visible="false" style="display: none;color:Red;width:5px;">Select an option.</span>
                        <%= Html.RadioButton("TextAcceptable" + text.TTMemberAttributeTextID, "true", text.IsGood) %>Good
                        <%= Html.RadioButton("TextAcceptable" + text.TTMemberAttributeTextID, "false", !text.IsGood) %>Bad
                </div>
                <div>
                        <%= Html.ListBox("TextUnacceptableReasons" + text.TTMemberAttributeTextID, text.TextUnacceptableReasons, new {@class = "reasonsStyle"}) %>
                         <br />
                         <span id="TextUnAcceptableValidation<%= text.TTMemberAttributeTextID.ToString() %>"  visible="false" style="display: none;color:Red;width:5px;">Select at least one option from the list.</span>
                    </div>
            </div>
        
        </div>
        
       
        

    
    <% } %>
    

<div style="display:inline; width:200px; float:left;">
            <span id="warningWarning"  style="display: none;color:Red;"> Please select a Warning.</span>
            <span style="display:inline; float:left; font-weight:bold;" >Warnings: <br /></span>
            <span style="display:inline; text-align:left; float:right;" id="warnings">
                <%=Html.CheckBoxFor(m=>m.WarningContactInfo) %>Contact Info<br/>
                <%=Html.CheckBoxFor(m=>m.WarningInappropriateContent) %>Inappropriate Content<br/>
                <%=Html.CheckBoxFor(m=>m.WarningGeneralViolation) %>General Violation<br /> 
                <input type="checkbox" id="sendNoWarning"   />Send no warning      
            </span>
        </div>

<br/>

<%=Html.CheckBoxFor(x=>x.PossibleFraud) %>Possible Fraud

    <div style="text-align:center;">
        <input type="button" value="Undo Edits"/>
        <%--validation must be added for this submit--%>
        <input type="submit" value="Approve" id="testApprove"/>
        <div style="float: right; margin-right: 300px;">
            <input type="button" value ="Suspend/Terminate" id="testSuspend" onclick="toggleTextSuspendPanel();"/>
            <div id="SuspendReasons" visible="false" style="display: none; border: 1px solid #666666; background-color: #dddddd; width: 200px;">
                <%= Html.DropDownList("AdminActionReasonID", Model.AdminSuspendReasons) %>
                <%--validation must be added for this submit--%>
                <input type="submit" value="Confirm Suspend" onclick="return validateTTTestForSuspend();" />
            </div>
        </div>
      </div>

</div>
<% Html.EndForm(); %>
<% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js" type="text/javascript" ></script>
    <script src="/Scripts/jquery.multiselect.js" type="text/javascript" ></script>
    <script type="text/javascript" src="/Scripts/tiny_mce/tiny_mce.js"></script>
    <link href="/Content/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	<link href="/Content/prettify.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
