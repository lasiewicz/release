﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.TrainingTool.TTEmailList>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%--EmailList Title--%>
    Spark Networks Web Admin - Email List for Training Tool
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">

    //SETTING UP OUR POPUP
    //0 means disabled; 1 means enabled;
    var emailListPopupStatus = 0;

    //User clicked on a message to view
    function LoadEmailMessage(ttMessageID) {
    
        $.ajax({
            type: "POST",
            url: "/TrainingToolAPI/RetrieveEmailMessage/" + ttMessageID,
            data: { },
            success: function (result) {
                if (result.Status == 2) {
                    $("#popupEmailMessage").find("#emailMessageArea").html(result.HTML);
                }
                else {
                    $("#popupEmailMessage").find("#emailMessageArea").html(result.StatusMessage);
                }
            }
        });

        //centering with css
        //centerPopup("#popupEmailMessage", "#backgroundPopup");
        //load popup
        emailListPopupStatus = loadPopup("#popupEmailMessage", "#backgroundPopup", emailListPopupStatus);
    }

    $(document).ready(function () {

        //Load individual email message 
        $("div.view-message-click-1").click(function () {
            var ttMessageID = $(this).find("input:hidden").val();
            LoadEmailMessage(ttMessageID);
        });

        //FolderID dropdown changes, then we need to refresh the list of emails
        $("span.folder-selection").change(function () {
            var folderID = $(this).find("select option:selected").val();
            var ttMemberProfileID = $("#ttMemberProfileID").val();
            var theNewLocation = "/TrainingTool/TTEmailList/" + ttMemberProfileID + "/" + folderID + "/0";
            window.location.href = theNewLocation;
        });

        //Click the x event!
        $("#popupClose").click(function () {
            emailListPopupStatus = disablePopupfast("#popupEmailMessage", "#backgroundPopup", emailListPopupStatus);
        });

        //Click out event!
        $("#backgroundPopup").click(function () {
            emailListPopupStatus = disablePopupfast("#popupEmailMessage", "#backgroundPopup", emailListPopupStatus);
        });

    });
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%--Hidden input to hold some required parameters--%>
<input type="hidden" id="ttMemberProfileID" value="<%= Model.TTMemberProfileID %>" />

<div style="width:100%;text-align:center;margin:5px;padding-top:10px;">
 Email activity for <a href="mailto:<%=Model.EmailAddress %>"><%=Model.EmailAddress %></a>: 
 <span class="folder-selection">
 <%= Html.DropDownListFor(m => m.FolderID, Model.FolderIDChoices)%>
 </span>
</div>

<%if (Model.TotalReturnedInPage > 0)
  { %>

<style>

#hor-minimalist-b
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	background: #fff;
	border-collapse: collapse;
	text-align: left;
	margin:7px 0;
	width:730px;
}
#hor-minimalist-b th
{
	font-size: 14px;
	font-weight: normal;
	color: #039;
	padding: 10px 8px;
	border-bottom: 2px solid #6678b1;
	text-align:left;
}
#hor-minimalist-b td
{
	border-bottom: 1px solid #ccc;
	color: #669;
	padding: 6px 8px;
}
#hor-minimalist-b tbody tr:hover td
{
	color: #009;
}

.hor-minimalist-label
{
     
	color: #669;
	padding: 6px 8px;
	font-size: 12px;
}

.hor-minimalist-header
{
    margin: 45px;
    font-size: 16px;
	font-weight: normal;
	color: #039;
	padding: 10px 8px;
	text-align:left;
}

.view-message-click-1
{
    text-decoration:underline;
    cursor:pointer;
}

.hor-minimalist-label
{
    color: #669;
	font-size: 12px;
}

.hor-minimalist-header
{
    font-size: 16px;
	font-weight: normal;
	color: #039;
	 
}
</style>
 
<table id="hor-minimalist-b">
<thead>
<tr>
   <th scope="col">View</th>
   <th scope="col"></th>
   <th scope="col">Date</th>
   <th scope="col">From</th>
   <th scope="col">To</th> 
</tr>
</thead>
<tbody>
<% foreach (var item in Model.Emails)
   { %>
<tr>
   <td>
        <div class="view-message-click-1"><a>View&nbsp;<%=item.ViewText%></a><input type="hidden" value="<%=item.MessageID %>" /></div>
   </td>
  <td>
    <% if (item.IsVIP) { %> *AA* <% } %>
  </td>
  <td>
    <%=item.MessageDate%>
  </td>
  <td>
    <%=item.FromMemberString%>
  </td>
  <td>
    <%=item.ToMemberString%>
  </td>
</tr>
<% } %>
</tbody>
</table>
 
<div id="tnt_pagination" style="padding-top:10px;">
<% for (int i = 0; i < Model.TotalPageCount; i++)
   {
   if (i * Model.PageSize == Model.RowIndex)
       {
         %>
            <span class="disabled_tnt_pagination"><%= (i + 1)%></span>
        <% 
       }
    else
       {
        %>
            <span><a  href="/TrainingTool/TTEmailList/<%= (Model.TTMemberProfileID) %>/<%= Model.FolderID %>/<%= i * Model.PageSize %>"><%= (i + 1)%></a> </span>
        <% 
      }
   }
 %>
<span class="hor-minimalist-label">Showing: <%=Model.RowIndex + 1%>&nbsp;to&nbsp;<%=Model.RowIndex + Model.TotalReturnedInPage%>&nbsp;of&nbsp;<%=Model.TotalEmailCount%></span>
</div>
 <br /><br /><br />
 <div id="popupEmailMessage" style="width:70%; height:150px; overflow:auto; margin-left:100px; margin-top:5px;">
  <a id="popupClose">x</a>
  <p id="emailMessageArea"></p>
</div>

<% }
  else
  { %>
  No email to view
<%} %>

</asp:Content>
