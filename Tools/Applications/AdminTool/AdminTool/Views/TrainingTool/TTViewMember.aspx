﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.TrainingTool.TTViewMember>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	TTViewMember
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <!-- Content Area Right -->
    <div id="management-content-right">
        <!-- Main -->
        <div id="management-main">
            <div class="main-containers row1">
                <!-- Basic Info -->
                <div id="basic-info" class="main-gray-boxes">
                    <%Html.RenderPartial("Controls/TTBasicUserInfo", Model.TtBasicUserInfo); %>
                </div>
                <!-- Global Profile -->
                <div id="global-profile" class="main-gray-boxes">
                    <%Html.RenderPartial("Controls/TTGlobalProfile", Model.TtGlobalProfile); %>
                </div>
                <!-- Date/Time Stamps -->
                <div id="date-time-stamps" class="main-gray-boxes">
                    <%Html.RenderPartial("Controls/TTDateTimeStampInfo", Model.TtDateTimeStampInfo); %>
                </div>
                <!-- Current Sub Status -->  
			    <div id="sub-status" class="main-gray-boxes ">
                    PLACEHOLDER
                </div>
                <div class="clearfix">
                </div>
            </div>

		<!-- Second Row -->  
		<div class="main-containers row2">
        	<!-- Communication History -->  
        	<div id="comm" class="main-gray-boxes">
                <%Html.RenderPartial("Controls/TTCommunicationHistory", Model.TtCommunicationHistory); %>
            </div>
            <!-- Fraud and Abuse -->  
            <div id="fraud-abuse" class="main-gray-boxes">
            PLACEHOLDER
            </div>
            <!-- Profile Information -->  
            <div id="profile-info" class="main-gray-boxes">
           		PLACEHOLDER
            </div>
    		<!-- Billing Information --> 
            <div id="billing-info" class="main-gray-boxes">
                PLACEHOLDER     
            </div>
            <!-- Other Available Actions --> 
            <div id="other-actions" class="main-gray-boxes">
            PLACEHOLDER
            </div>       
            <div class="clearfix"></div>
        </div>

        <div class="main-containers row3">
            <!-- Admin Notes & Action Log -->
            <div id="admin-notes-container">
                PLACEHOLDER
            </div>
            <!-- Transaction History -->
            <div id="transaction-container" class="">
                PLACEHOLDER       
            </div>
        </div>
    
    </div>
</div>
<!-- End Content Area Right --> 

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <% if (Model.SuperTopNav != null)
       {
           Html.RenderPartial("Controls/TTSuperTopNav", Model.SuperTopNav);
       }%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%Html.RenderPartial("Controls/TTMemberLeft", Model.TtMemberLeft); %>
</asp:Content>
