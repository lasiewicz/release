﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.TrainingTool.TTManageGrouping>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	TTGroupingManagement
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<style>
    
        .ttManageGrouping
        {
            padding-left:20px;
        }
    
       .ttManageGrouping li 
       {
            padding-bottom:10px;
            padding-left:20px;
            text-align:left;
            font-size:11px;
       }
       
       .ttManageGrouping ul 
       {
           margin-top:20px;
       }
       
       .ttManageGrouping div
       {
           border: 1px solid #444;
           margin:10px;
           padding:5px;
           -webkit-border-radius: 10px;
           -moz-border-radius: 10px;
           border-radius: 10px;
           height:150px;
           width:250px;
           float: left;
       }
       
       .ttManageGrouping div:nth-child(1)
       {
           background-color:#A6D785;
       }
       
       .ttManageGrouping div:nth-child(2)
       {
           background-color:#FFEC8B;
       }
       
       .ttManageGrouping div:nth-child(3)
       {
           background-color:#B19CD9;
       }
       
       .ttManageGrouping div:nth-child(4)
       {
           background-color:#F0F8FF;
       }
       
       
       .ttManageGrouping span,.ttCreateGrouping span
       {
           font-weight:600;
           font-size:12px;
           text-align:center;
           width:100%;
       }
       
       .ttManageGrouping input[type="button"]
       {
           width:90%;
       }
       
       .tt_button_lrg
       {
           width:190px;
       }
       
       .ttCreateGrouping 
       {
           padding:20px;
           width: 88%;
            max-width:1066px;
           margin-left:30px;
           border: 1px solid black;
           -webkit-border-radius: 10px;
           -moz-border-radius: 10px;
           border-radius: 10px;
           height:55px;
           clear: both;
       }
       
       
       .ttCreateGrouping div
       {
            width:50%;
            float:left;
       }     
       
       .ttCreateGrouping li 
       {
            padding-bottom:10px;
            text-align:left;
       }

       .bucketDisplay {
           padding-left: 20px;    
       } 
       
       .bucketDisplay div {
           margin:10px;
           padding:5px;
           clear:both;
       } 
       
       .bucketDisplay table 
       {
           width:25%;
       }
       
       .bucketDisplay table tr:nth-child(odd)
       {
           background:#F0F8FF;
       }
       
       #GroupingID 
       {
           width:33%;
       }
       #NewGroupingName
       {
           margin-top:13px;
       }
       th 
       {
           font-size: 14px;
       }
       td 
       {
            font-family: Trebuchet MS, Tahoma, Sans-Serif;
            padding: 3px;
            text-align: center;
            font-size: 12px;
       }
       td a 
       {
           text-decoration: none;
       }
       
       td a:visited {
            color: #B19CD9;
       }
</style>

<%=Html.HiddenFor(m=>m.PageNumber) %>
<%=Html.HiddenFor(m=>m.RowsPerPage) %>

    <div style="float: left; width:85%; ">
    
    <h2 style="padding-top: 10px; padding-left: 200px;">Free Text Training Profiles</h2>
    <br />


<% Html.BeginForm("Upload", "TrainingTool", FormMethod.Post, new {enctype = "multipart/form-data"});%>   
     <div class="ttCreateGrouping">
            <div>
                <h2>Bucket Selector:</h2>
                <span style="font-weight:100;">For adding members below or displaying bucket contents</span><br />
                <span><%=Html.DropDownList("GroupingID", Model.GroupingList,string.Empty) %> <input type="button" id="RefreshGroupingMembers" value="View Bucket Contents"/></span>
            </div>
            <div>
                <h2>Create New Bucket:</h2>
               <span><%=Html.TextBox("NewGroupingName") %> <input type="button" id="CreateNewGroupingButton" value="Create Bucket"/></span>
                <%--<span id="GroupingMemberProfiles"></span> --%>
            </div>
    </div> 
    <div class="ttManageGrouping clear-both">
                <div>
                    <span>Modify existing training profile</span><br />
                    <ul>
                        <li>
                            Member ID : <br />
                            <%=Html.TextBox("ModifyMemberID") %>
                        </li>
                        <li><input type="button" id="ModifyMemberButton" value="Modify" class="tt_button"/></li>
                    </ul>
                </div>
                
             <div>
                    <span>Add a single member ID to the bucket selected below</span>
                    <ul>
                        <li><%=Html.DropDownList("SingleAddSiteID", Model.SingleAddSiteList, new { style = "width:172px;" })%></li>
                        <li>Member ID: <%=Html.TextBox("SingleAddMemberID", "", new { style= "width:113px;" })%></li>
                        <li><input type="button" id="SingleAddButton" value="Add Member" class="tt_button" /></li>
                    </ul>
                </div>

                <div>
                    <span>Insert multiple profiles by .csv file upload to the bucket selected below</span>
                    <ul>
                        <%--<li><%=Html.DropDownList("MultiAddSiteID", Model.MultiAddSiteList, new { style = "width:172px;" })%></li>--%>
                        <li><input type="file" name="csvFile" id="csvFile" /></li>
                        <li><input type="submit" id="btnUploadCsv" value="Upload Member ID's" style="width:225px;"/></li>
                    </ul>
                </div>

                <div>
                    <span>View Agent Training History</span>
                    <ul>
                        <li>Member Email: <%=Html.TextBox("AgentEmail", "", new { style = "width:113px;" })%> </li>
                        <li><input type="button" id="SearchAgentTrainingHistoryButton" value="View" class="tt_button"/> </li>
                    </ul>
                </div>                         
    </div>
    

<% Html.EndForm(); %>

    <div class="bucketDisplay">
        <div id="MemberProfilesContainer"></div>    
        <div id="MemberProfilesPagination"></div>
    </div>
    
    <%-- This is referenced inside admin.js GetTTMemberProfilesForGroupingID. This is used to build the list of TTMemberProfiles for a givent GroupingID. --%>
    <script id="ttMemberProfileTemplate" type="text/template">
        <tr>
        <td><a href="/TrainingTool/TTSetAnswers/{{MemberID}}/{{GroupingID}}">{{MemberID}}</a></td>
        <td>{{AnswerCompleted}}</td>
        </tr>
    </script>

    </div> <%--main div close--%>
    
    <%-- When modifying an existing member profile, a single MemberID can belong to multiple buckets, so this overlay provides the list. --%>
    <div class="modal" id="MemberIDSearchResult">
        <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverlayMemIDSrchResult()"><b>[X]</b></a></div>
        <div id="MemberIDSearchResultContent"></div>
    </div>
    <input type="hidden" id="closeOverlayMemIDSrchResult" />
    
    <%-- Overlay for display the list of emails for a email substring search --%>
    <div class="modal" id="EmailSubstringSearchResult">
        <div style="float:right;"><a style="cursor: pointer; color:Blue;" onclick="closeOverlayEmailSubstringSearchResult()"><b>[X]</b></a></div>
        <div id="EmailSubstringSearchResultContent"></div>
    </div>
    <input type="hidden" id="closeOverlayEmailSubstringSearchResult" />
    
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            $('#SingleAddButton').click(function (event) {
                if ($('#GroupingID option:selected').val() <= 0) {
                    alertMessage('Please Select a Bucket below');
                    return false;
                }
                AddSingleMemberToTTGrouping(event, $(this), $('#GroupingID').val(), $('#GroupingID option:selected').text(), $('#SingleAddMemberID').val(), $('#SingleAddSiteID').val());
            });

            $('#RefreshGroupingMembers').click(function (event) {
                if ($("#GroupingID option:selected").val() <= 0) {
                    alertMessage('Please Select a Bucket');
                    $('#MemberProfilesContainer, #MemberProfilesPagination').empty();
                    return false;
                }
                $('h3.bucketWarning').css("display", "none");
                GetTTMemberProfilesForGroupingID(event, $(this), $('#GroupingID').val(), 1, $('#RowsPerPage').val());

            });

            $('#CreateNewGroupingButton').click(function (event) {
                //prevents blank names from being submitted
                if ($.trim($('#NewGroupingName').val()) === "") {
                    alertMessage("Please give the bucket a name");
                    return false;
                } else {
                    CreateNewTTGrouping(event, $(this), $('#NewGroupingName').val());
                }
            });

            $('#ModifyMemberButton').click(function (event) {
                GetTTMemberProfilesByMemberID(event, $(this), $('#ModifyMemberID').val(), memberIDSearchCallBack);
            });

            $('#SearchAgentTrainingHistoryButton').click(function (event) {
                GetMemberEmailsByEmailSubstring(event, $(this), $('#AgentEmail').val(), agentSearchCallBack, true);
            });

            $('#btnUploadCsv').click(function () {
                if ($('input:file').val().length == 0) {
                    alertMessage('Please select a file to upload');
                    return false;
                }
                return true;
            });
        });

        var agentSearchCallBack = function (listOfEmailAddresses) {
            var html = "";
            for (var i = 0; i < listOfEmailAddresses.Array.length; i++) {
                html += '<a href="/TrainingTool/TTTestHistory/' + listOfEmailAddresses.Array[i].MemberID + '">' + listOfEmailAddresses.Array[i].EmailAddress + '</a><br>';
            }

            $('#EmailSubstringSearchResultContent').html(html);

            $('#EmailSubstringSearchResult').lightbox_me({
                centered: true
            });
        };

        var memberIDSearchCallBack = function (memberIDs, found) {
            if (!found) {
                $('#MemberIDSearchResultContent').html("MemberID not found");
                $('#MemberIDSearchResult').lightbox_me({
                    centered: true
                });
                return;
            }

            if (memberIDs.length == 1) {
                window.location = "/TrainingTool/TTSetAnswers/" + memberIDs[0].MemberID + "/" + memberIDs[0].TTGroupingID;
            } else {
                var html = memberIDs[0].MemberID + " belongs to multiple buckets:<br>";
                for (var i = 0; i < memberIDs.length; i++) {
                    html += '<a href="/TrainingTool/TTSetAnswers/' + memberIDs[i].MemberID + '/' + memberIDs[i].TTGroupingID + '">' + memberIDs[i].GroupingName + '</a><br>';
                }
                $('#MemberIDSearchResultContent').html(html);

                $('#MemberIDSearchResult').lightbox_me({
                    centered: true
                });
            }
        };

        function closeOverlayMemIDSrchResult() {
            $('#closeOverlayMemIDSrchResult').lightbox_me();
            $('#closeOverlayMemIDSrchResult').trigger('close');
        }
        
        function closeOverlayEmailSubstringSearchResult() {
            $('#closeOverlayEmailSubstringSearchResult').lightbox_me();
            $('#closeOverlayEmailSubstringSearchResult').trigger('close');
        }
    
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="/Scripts/mustache.js"></script>
    <script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
    <style>
        table, th, td {
            border: 1px solid black;
        }
        td {
            padding: 15px;
        }    
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
    <%Html.RenderPartial("../Shared/Controls/UiMessage", Model); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
