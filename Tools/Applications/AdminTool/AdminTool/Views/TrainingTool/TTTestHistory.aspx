﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.TrainingTool.TTTestHistory>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.TrainingTool" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	TTTestHistory
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h2 
        {
            margin:10px 0 30px 0;
        }
        .bordered-table 
        {
            margin: 0 auto;
        }
        .bordered-table tr td {
            border: 1px solid black;
            padding: 5px;
            height: 25px;
            vertical-align: middle;
        }
        .bordered-table th {
            border: 1px solid black;
            padding: 5px;
            font-family: Arial;
            font-variant: small-caps;
            text-transform: lowercase;
            letter-spacing: 2px;
            font-weight: 600;
        }
        
        tr.topRow th:nth-child(1) 
        {
            background-color: #A6D785;
        }
        tr.topRow th:nth-child(2) 
        {
            background-color: #FFEC8B;
        }
        tr.topRow th:nth-child(3) 
        {
            background-color: #C2E3FD;
        }
        .bordered-table tr.titleRow 
        {
            font-family: arial;
            text-transform: lowercase;
            font-variant: small-caps;
            text-align: center;
        }
        .bordered-table tr.resultsRow 
        {
            text-align: center;
            font-size: 14px;
        }
        .bordered-table tr.titleRow td:nth-of-type(-n+4),.bordered-table tr.resultsRow:nth-child(even) td:nth-of-type(-n+4)
        {
            background-color: #DFFCCC;
        }
        .bordered-table tr.titleRow td:nth-of-type(n+5),.bordered-table tr.resultsRow:nth-child(even) td:nth-of-type(n+5)
        {
            background-color: #FFFAE0;
        }
        .bordered-table tr.titleRow td:nth-of-type(n+9),.bordered-table tr.resultsRow:nth-child(even) td:nth-of-type(n+9)
        {
            background-color: aliceblue;
        }
        
        
    </style>
    
    <% Html.RenderPartial("Controls/TTTestResults", Model.TestResults); %>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>
