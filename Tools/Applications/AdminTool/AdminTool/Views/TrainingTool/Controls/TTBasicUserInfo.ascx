﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.TrainingTool.TTBasicUserInfo>" %>

<div class="basic-info-left">Name:</div><div class="basic-info-right"><%= Model.TruncateString(Model.FirstName, 30) + " " + Model.TruncateString(Model.LastName, 30) %></div>
<div class="basic-info-left">Username:</div><div class="basic-info-right"><%= Model.UserName %>&nbsp;</div>
<div class="basic-info-left">MemberID:</div><div class="basic-info-right"><%=Model.MemberId.ToString() %></div>    
<div class="basic-info-left">Administrator:</div><div class="basic-info-right"><%= (Model.IsAdmin ? "Yes" : "No") %></div>
<div class="basic-info-left">Profile Status:</div><div class="basic-info-right"><span id="profile-status"></span></div>
<script type="text/javascript">
    $(document).ready(function () {
        GetProfileStatusDisplayText('<%=Model.ProfileStatus.ToString()%>', '<%=Model.SuspendReason %>');
    });
</script>

<div style="float:left">Login:<%=Html.TextBox("Email", Model.Email, new{@class="basic-login"})%></div>
<input type="button" value="Update" class="button" />
<div class="basic-links">
    [Send Password Reset][Enter Stealth Login]
</div>