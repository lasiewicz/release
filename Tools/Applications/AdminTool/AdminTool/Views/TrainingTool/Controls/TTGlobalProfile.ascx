﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.TrainingTool.TTGlobalProfile>" %>

<ul>

<li id="AdminSuspendedDiv" class="list-item"><label>Admin Suspended:</label><span>Yes</span> <%= Html.RadioButtonFor(model => model.AdminSuspended, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.AdminSuspended, "false")%></li>
<li class="list-item">
    <% if (Model.AdminSuspended.ToString() == "True") {%>
        <%=Html.DropDownList("AdminActionReasonID", Model.AdminSuspendReasons ) %>
    <% }  else{ %>
        <%=Html.DropDownList("AdminActionReasonID", Model.AdminSuspendReasons, new {disabled="disabled"}) %>
   
  <% }%>
</li>
<li id="BadEmailDiv" class="list-item"><label>Bad Email:</label> <span>Yes</span> <%= Html.RadioButtonFor(model => model.BadEmail, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.BadEmail, "false")%></li>
<li id="EmailVerifiedDiv" class="list-item"><label>Email Verified:</label> <span>Yes</span> <%= Html.RadioButtonFor(model => model.EmailVerified, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.EmailVerified, "false")%></li>
<li class="sendverification">
    <a id="send-verification" href="#" class="trigger-overlay">Send Verification Letter</a>
</li>
<li id="DneDiv" class="list-item"><label>On DNE List:</label> <span>Yes</span> <%= Html.RadioButtonFor(model => model.OnDNE, "true") %> <span>No</span> <%= Html.RadioButtonFor(model => model.OnDNE, "false")%></li>
</ul>

<input type="button" value="Save Changes" id="btnSaveChanges" class="button" />   
