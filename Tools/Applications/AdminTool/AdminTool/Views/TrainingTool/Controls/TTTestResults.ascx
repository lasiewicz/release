﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.TrainingTool.TTTestResults>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.TrainingTool" %>

<script type="text/javascript">
    $(document).ready(function () {
        letterGrade();
    });

    function letterGrade() {
        $(".suspendGrade, .emailGrade, .contentGrade").each(function () {
            var correct = $(this).find("span.correct").html();
            var total = $(this).find("span.total").html();
            var letter = $(this).find("span.letterGrade");
            if (total <= 0) {
                letter.text("N/A");
            } else {
                var grade = correct / total;

                if (grade < .6) {
                    letter.text("F").css("color", "red");
                } else if (grade >= .9) {
                    letter.text("A");
                } else if (grade >= .8) {
                    letter.text("B");
                } else if (grade >= .7) {
                    letter.text("C");
                } else if (grade >= .6) {
                    letter.text("D");
                }
            }
        });
    }

    </script>

<% if (Model.TTTestResultList == null)
       { %>
       No test taken yet.
    <% }
       else
       { %>
       
       <table class="bordered-table">
           <tr class="topRow">
               <th colspan="4">Training Details</th>
               <th colspan="4">Letter Grades</th>
               <th>Final Grade</th>
           </tr>
           <tr class="titleRow">
               <td>Date</td>
               <td>Bucket</td>
               <td>Profiles</td>
               <td>Total Time</td>
               <td>Profiles Per Hour</td>
               <td>Suspend Accuracy</td>
               <td>Email Warning Accuracy</td>
               <td>Content Review Accuracy</td>
               <td>Overall</td>
           </tr>
           <% foreach (var tResult in Model.TTTestResultList)
              { %>
              <tr class="resultsRow">
                  <td><%= tResult.TimeStarted %></td>
                  <td><%= tResult.GroupingName %></td>
                  <td><%= tResult.TotalTestItemCount %></td>
                  <td><%= tResult.TotalTestTimeMinutes %>&nbsp;min&nbsp;<%= tResult.TotalTestTimeSeconds %>&nbsp;sec&nbsp;</td>
                  <td><%= ((int)tResult.ProfilesPerHour).ToString() %></td>
                  <td class="suspendGrade"><span class="correct"><%= tResult[Constants.GROUP_NAME_SUSPENSION].PointsCorrect%></span>&nbsp;/&nbsp;<span class="total"><%= tResult[Constants.GROUP_NAME_SUSPENSION].PossiblePoints%></span>&nbsp;&nbsp;(<span class="letterGrade"></span>)</td>
                  <td class="emailGrade"><span class="correct"><%= tResult[Constants.GROUP_NAME_EMAIL_WARNING].PointsCorrect%></span>&nbsp;/&nbsp;<span class="total"><%= tResult[Constants.GROUP_NAME_EMAIL_WARNING].PossiblePoints%></span>&nbsp;&nbsp;(<span class="letterGrade"></span>)</td>
                  <td class="contentGrade"><span class="correct"><%= tResult[Constants.GROUP_NAME_CONTENT_REVIEW].PointsCorrect%></span>&nbsp;/&nbsp;<span class="total"><%= tResult[Constants.GROUP_NAME_CONTENT_REVIEW].PossiblePoints%></span>&nbsp;&nbsp;(<span class="letterGrade"></span>)</td>
                  <td>TBD</td>
              </tr>
           <% } %>
       </table>

    <% } %>