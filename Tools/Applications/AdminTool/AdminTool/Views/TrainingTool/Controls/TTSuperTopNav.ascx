﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.TrainingTool.TTSuperTopNav>" %>

<div id="member-search-nav" class="clearfix">
<script type="text/javascript">

    /*
    * DEFAULT OPTIONS
    *
    options: {
    template:"yourMenuVoiceTemplate",--> the url that returns the menu voices via ajax. the data passed in the request is the "menu" attribute value as "menuId"
    additionalData:"",								--> if you need additional data to pass to the ajax call
    menuSelector:".menuContainer",		--> the css class for the menu container
    menuWidth:150,										--> min menu width
    openOnRight:false,								--> if the menu has to open on the right insted of bottom
    iconPath:"ico/",									--> the path for the icons on the left of the menu voice
    hasImages:true,									--> if the menuvoices have an icon (a space on the left is added for the icon)
    fadeInTime:100,									--> time in milliseconds to fade in the menu once you roll over the root voice
    fadeOutTime:200,									--> time in milliseconds to fade out the menu once you close the menu
    menuTop:0,												--> top space from the menu voice caller
    menuLeft:0,											--> left space from the menu voice caller
    submenuTop:0,										--> top space from the submenu voice caller
    submenuLeft:4,										--> left space from the submenu voice caller
    opacity:1,												--> opacity of the menu
    shadow:false,										--> if the menu has a shadow
    shadowColor:"black",							--> the color of the shadow
    shadowOpacity:.2,								--> the opacity of the shadow
    openOnClick:true,								--> if the menu has to be opened by a click event (otherwise is opened by a hover event)
    closeOnMouseOut:false,						--> if the menu has to be cloesed on mouse out
    closeAfter:500,									--> time in millisecond to whait befor closing menu once you mouse out
    minZindex:"auto", 								--> if set to "auto" the zIndex is automatically evaluate, otherwise it start from your settings ("auto" or int)
    hoverInted:0, 										--> if you use jquery.hoverinted.js set this to time in milliseconds to delay the hover event (0= false)
    onContextualMenu:function(o,e){} --> a function invoked once you call a contextual menu; it pass o (the menu you clicked on) and e (the event)
    },
    */

    $(function () {
        $(".myMenu").buildMenu(
    {
        template: "menuVoices.html",
        additionalData: "pippo=1",
        menuWidth: 200,
        openOnRight: false,
        menuSelector: ".menuContainer",
        iconPath: 'http://' + location.host + "/Images/",
        hasImages: true,
        fadeInTime: 100,
        fadeOutTime: 300,
        adjustLeft: 2,
        minZindex: "auto",
        adjustTop: 10,
        opacity: .95,
        shadow: false,
        shadowColor: "#ccc",
        hoverIntent: 0,
        openOnClick: true,
        closeOnMouseOut: false,
        closeAfter: 1000,
        submenuHoverIntent: 200
    });

        $(document).buildContextualMenu(
    {
        menuWidth: 200,
        overflow: 2,
        menuSelector: ".menuContainer",
        iconPath: 'http://' + location.host + "/Images/",
        hasImages: true,
        fadeInTime: 100,
        fadeOutTime: 100,
        adjustLeft: 0,
        adjustTop: 0,
        opacity: .99,
        closeOnMouseOut: false,
        onContextualMenu: function (o, e) { }, //params: o = the contextual menu,e = the event
        shadow: false
    });
    });



    function recallcMenu(el) {
        if (!el) el = $.mbMenu.lastContextMenuEl;
        var cmenu = +$(el).attr("cmenu");
        $(cmenu).remove();
    }



    function showMessage(msg) {
        var msgBox = $("<div>").addClass("msgBox");
        $("body").append(msgBox);
        msgBox.append("You click on: <br>" + msg);
        setTimeout(function () { msgBox.fadeOut(500, function () { msgBox.remove(); }) }, 3000)
    }

</script> 
 
   <div class="myMenu" style="background-color:#003366;">
               <table class="rootVoices" cellspacing='0' cellpadding='0' border='0' width="100%">
                   <tr>
                    <td class="rootVoice  " ><a href="/TrainingTool/TTManageGrouping" style=" text-decoration:none; color:#fff">FTT Management Home</a></td>
                    <td class="rootVoice  " ><a href="/TrainingTool/TTViewMember/<%=Model.TargetAgentID %>/<%=Model.GroupingID %>" style=" text-decoration:none; color:#fff">Profile Account Page</a></td>
                    <td class="rootVoice  " ><a href="/TrainingTool/TTEditProfile/<%=Model.TargetAgentID %>/<%=Model.GroupingID %>" style=" text-decoration:none; color:#fff">Change Profile</a></td>
                    <td class="rootVoice  " ><a href="/TrainingTool/TTSetAnswers/<%=Model.TargetAgentID %>/<%=Model.GroupingID %>" style=" text-decoration:none; color:#fff">Set Answer Key</a></td>
                  </tr>
              </table>
    </div>
  
  <!-- menues hold over from main menu incase we need the functionality in the future -->
   <div id="menu_2" class="mbmenu">
    <a class="{menu: 'sub_menu_1',img: 'FTA.png'}">Free Text Approval</a>
    <a class="{menu: 'sub_menu_2',img: 'PhotoApproval.png'}">Photo Approval</a>
    <a class="{menu: 'sub_menu_3',img: 'QnA_Approval.png'}">Q&A Approval</a>
	<a class="{menu: 'sub_menu_4',img: 'FB_Likes.png'}">FB Likes Approval</a>
    <a class="{menu: 'sub_menu_5',img: 'Message_Boards.png'}" >Message Boards</a>
    <a class="{img: 'DuplicateText.png'}" href="/Approval/ViewMemberProfileDuplicateByLanguage/2">Similar Text Review</a>
  </div>


  <div id="menu_5" class="mbmenu">
    <a   class="{img: '2.png'}" href="/Reports/ViewQueueCounts/">Approval Queue Stats</a>
    <a   class="{img: '1-6-7-8.png'}" href="/Reports/ViewApprovalCounts/">Admin Approval Stats and Review</a>
    <a   class="{img: 'DuplicateText.png'}" href="/Reports/ViewMemberProfileDuplicateCounts/">Similar Text Agent Stats</a>
    <a class="{img: 'DuplicateText.png'}" href="/Approval/ViewMemberProfileDuplicateReview/2">Similar Text Second Review</a>
    <a   class="{img: '1-6-7-8.png'}" href="/Review/OnDemandReviewQueueManagement">On-Demand Review Queue</a>
    <a   class="{img: '1-6-7-8.png'}" href="/Review/CRXReviewQueue">CRX Second Review</a>
    <a   class="{img: '1-6-7-8.png'}" href="/TrainingTool/TTManageGrouping">FTA Training Management</a>
    <a   class="{img: '4-5.png'}" href="/Reports/ViewPhotosQueue/">Stuck Photo Finder</a>
    <a   class="{img: '4-5.png'}" href="/review/viewmemberfraudreviewqueue/">Possible Fraud Review</a>
	<a   class="{img: '3.png'}" href="/Reports/ViewBulkMailBatchReports/">BulkMail Batch Report</a>
	<a   class="{img: '9.png'}" href="/Review/SODBReviewQueue">SODB Queue</a>
    <a   class="{img: '10.png'}" href="/Review/SODBSecondReviewQueue">SODB Second Review</a>
    <a   class="{img: '11.png'}" href="/reports/ViewSodbQueueCounts">SODB Queue Stats</a>
    <a   class="{img: '12.png'}" href="/reports/ViewSodbReviewStats">SODB Review Agent Stats</a>
    <a   class="{img: '13.png'}" href="/review/SODBSearch">SODB Search</a>
 </div>

  <div id="menu_6" class="mbmenu">
    <a class="{img: 'SearchPromo.png'}"  href="/SubAdmin/PromoSearch/">Promo Search</a>
	<a class="{img: 'CreatePlan.png'}"  href="/SubAdmin/CreatePlan/">Create Plan</a>
	<a class="{img: 'SearchGifts.jpg'}"  href="/SubAdmin/GiftSearch/">Search Gifts</a>
	<a class="{img: 'CreatePromo.png'}"  href="/SubAdmin/CreatePromo/">Create Promo</a>
	<a class="{img: 'ReorderPromo.png'}"  href="/SubAdmin/PromoPriority/">Promo Ordering</a>
    <a class="{img: 'PlanList.png'}"  href="/SubAdmin/PlanList/0">Plan List</a>
    <a class="{img: 'ResourceTemplateMgr.png'}"  href="/SubAdmin/ResourceManagerTemplates">Resource Template Manager</a>
  </div>

    <div id="menu_7" class="mbmenu">
        <a  class="{img: 'Search.png'}" href="/DNE/Search">Search</a>
	    <a  class="{img: 'Download.png'}" href="/DNE/Download">Download</a>
	    <a  class="{img: 'Upload.png'}" href="/DNE/Upload"  target="_blank">Upload</a>
    </div>
   
  <div id="sub_menu_1" class="mbmenu">
    <a class="{img: 'EN.png'}"  href="/Approval/ViewTextByLanguage/2">English</a>
	<a class="{img: 'FR.png'}"  href="/Approval/ViewTextByLanguage/8">French</a>
	<a class="{img: 'HE.png'}"  href="/Approval/ViewTextByLanguage/262144">Hebrew</a>
  </div>
  
  <div id="sub_menu_2" class="mbmenu">
	<a rel="separator">Communities</a>
    <a  class="{img: '/Icons/Spark.png'}" href="/Approval/ViewPhotosByCommunity/1">Spark</a>
	<a  class="{img: '/Icons/Cupid.png'}" href="/Approval/ViewPhotosByCommunity/10">Cupid</a>
	<a  class="{img: '/Icons/BBW.png'}" href="/Approval/ViewPhotosByCommunity/23">BBW</a>
	<a  class="{img: '/Icons/BlackSingles.png'}" href="/Approval/ViewPhotosByCommunity/24">BlackSingles</a>
	<a rel="separator">Sites</a>
	<a  class="{img: 'JDate.png'}" href="/Approval/ViewPhotosBySite/103">JDate.com</a>
	<a  class="{img: 'JDateIL.png'}" href="/Approval/ViewPhotosBySite/4">JDate.co.il</a>
	<a  class="{img: 'JDateUK.png'}" href="/Approval/ViewPhotosBySite/107">JDate.co.uk</a>
	<a  class="{img: 'JDateFR.png'}" href="/Approval/ViewPhotosBySite/105">JDate.fr</a>
  </div>
  
  <div id="sub_menu_3" class="mbmenu">
    <a rel="separator">Communities</a>
    <a class="{img: 'EN.png'}" href="/Approval/ViewQATextByLanguage/2">English</a>
	<a class="{img: 'FR.png'}" href="/Approval/ViewQATextByLanguage/8">French</a>
	<a class="{img: 'HE.png'}" href="/Approval/ViewQATextByLanguage/262144">Hebrew</a>
	<a class="{img: '/Icons/Spark.png'}" href="/Approval/ViewQATextByCommunity/1">Spark</a>
	<a class="{img: '/Icons/Cupid.png'}" href="/Approval/ViewQATextByCommunity/10">Cupid</a>
	<a class="{img: '/Icons/BBW.png'}" href="/Approval/ViewQATextByCommunity/23">BBW</a>
	<a class="{img: '/Icons/BlackSingles.png'}" href="/Approval/ViewQATextByCommunity/23">BlackSingles</a>
	<a class="{img: 'JDate.png'}" href="/Approval/ViewQATextBySite/103">JDate.com</a>
	<a class="{img: 'JDateIL.png'}" href="/Approval/ViewQATextBySite/4">JDate.co.il</a>
	<a class="{img: 'JDateUK.png'}" href="/Approval/ViewQATextBySite/107">JDate.co.uk</a>
	<a class="{img: 'JDateFR.png'}" href="/Approval/ViewQATextBySite/105">JDate.fr</a>
  </div>

   <div id="sub_menu_4" class="mbmenu">
    <a rel="separator">Communities</a>
    <a  class="{img: '/Icons/Spark.png'}" href="/Approval/ViewFacebookLikesByCommunity/1">Spark</a>
	<a  class="{img: '/Icons/Cupid.png'}" href="/Approval/ViewFacebookLikesByCommunity/10">Cupid</a>
	<a  class="{img: '/Icons/BBW.png'}" href="/Approval/ViewFacebookLikesByCommunity/23">BBW</a>
	<a  class="{img: '/Icons/BlackSingles.png'}" href="/Approval/ViewFacebookLikesByCommunity/24">BlackSingles</a>
    <a rel="separator">Sites</a>
	<a  class="{img: 'JDate.png'}"   href="/Approval/ViewFacebookLikesBySite/103">JDate.com</a>
	<a  class="{img: 'JDateIL.png'}"   href="/Approval/ViewFacebookLikesBySite/4">JDate.co.il</a>
	<a  class="{img: 'JDateUK.png'}"  href="/Approval/ViewFacebookLikesBySite/107">JDate.co.uk</a>
	<a  class="{img: 'JDateFR.png'}"   href="/Approval/ViewFacebookLikesBySite/105">JDate.fr</a>
 </div>
  

  <div id="sub_menu_5" class="mbmenu">
    <a class="{img: '/Icons/BBW.png'}"  href="http://connect.bbwpersonalsplus.com/admin_login.html" target="_blank">BBW</a>
    <a class="{img: '/Icons/JDate.png'}"  href="http://connect.jdate.com/admin_login.html" target="_blank">JDate</a>
	<a class="{img: '/Icons/Spark.png'}"  href="http://connect.spark.com/admin_login.html" target="_blank">Spark</a>
    <a  class="{img: '/Icons/BlackSingles.png'}" href="http://connect.blacksingles.com/admin_login.html">BlackSingles</a>
 </div>
   

    
 
 

 
</div>
