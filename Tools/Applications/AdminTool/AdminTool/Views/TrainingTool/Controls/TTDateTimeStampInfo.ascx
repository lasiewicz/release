﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.TrainingTool.TTDateTimeStampInfo>" %>

<div id="DateTimeStampInfoControl">
<ul>
    <li>Registered: <%= Model.GetShortDisplayDate(Model.RegisteredDate) %><%if (Model.IsMobileRegistration)
                                                                            {%> (M)<% } %></li>
    <li>Last Update: <%= Model.GetDisplayDate(Model.LastUpdateDate) %></li>
    <li>Last Login: <%= Model.GetDisplayDate(Model.LastLoginDate) %></li>
    <li>Last 5 Logins:</li>
    <% if(Model.LastLogons != null) { %>
    <% foreach (DateTime lastLogon in Model.LastLogons) { %>
      <li><%= Model.GetDisplayDate(lastLogon)%></li>
    <%}%>
    <%}%>
</ul>
</div>