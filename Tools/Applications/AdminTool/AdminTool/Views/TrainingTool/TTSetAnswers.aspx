﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.TrainingTool.TTSetAnswers>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.TrainingTool" %>
<%@ Import Namespace="Matchnet.ApproveQueue.ValueObjects.TrainingTool" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> 
	TTSetAnswers
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <style> 
	span{
			font-size:12px;
		}
		
    ol, ul
    {
        list-style: none;
    }
        
    .ttCreateGrouping
       {
           border: 1px solid black;
           margin:10px;
           padding:5px;
           -webkit-border-radius: 10px;
           -moz-border-radius: 10px;
           border-radius: 10px;
           width:500px; 
           height: auto;
           float:left;
           font-weight:600;
           font-size:12px;
           width:100%; 
           clear:both; 
       }
       
       .ttCreateGrouping div 
       {
           width: auto;
       }
       
     
    
    .mainDivTT
    {
        float: left; 
        width:1000px; 
    } 
    
    .mainDivTT .ttCreateGrouping:nth-child(odd) 
    {
         background-color:#F2F2F2;
    }
    
    
      
       .headerFTT li 
       {
            padding-bottom:10px;
            padding-left:20px;
            text-align:left;
            font-size:11px;
       }
       
       .headerFTT ul 
       {
           margin-top:20px;
       }
       
       .headerFTT div
       {
           border: 1px solid black;
           margin:10px;
           padding:5px;
           -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            height:150px;
            width:250px;
           float: left;
       }
       
       .headerFTT div:nth-child(1)
       {
           background-color:#A6D785;
       }
       
       .headerFTT div:nth-child(2)
       {
           background-color:#FFEC8B;
       }
       
       .headerFTT div:nth-child(3)
       {
           background-color:#B19CD9;
       }
       
       .headerFTT span
       {
           font-weight:600;
           font-size:12px;
           text-align:center;
           padding-left:10px;
           width:100%;
       }
       
       .headerFTT input[type="button"]
       {
           width:177px;
       }
       
       .headerFTT
       {
           width:auto;
       }
       
       
       
       .headerFTT div
       {
           margin:10px;
           padding:5px;
           padding-top: 10px;
           -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            height:50px;
            width:950px;
           clear: both;
       }
       
       .headerFTT span
       {
           font-weight:600;
           font-size:12px;
           text-align:center;
           padding-left:10px;
           width:100%;
       }
       
       .headerFTT li 
       {
            padding-bottom:10px;
            text-align:left;
       }
       
       .headerFTT li h2 
       {
           font-size: 12px;
        margin: 5px 0 10px 0;
       }
       
       .headerFTT ul ul 
       {
           margin-left: 1.5%;
       }
       
       .headerFTT ul ul li 
       {
           font-size: 12px;
           padding-left:0;
           list-style-type: disc;
       }
       
       #saveAnswerKey 
       {
           margin-top: 7px;
           width: 15%;
           height: 50px;
       }
       #SelectedSuspendReasons 
       {
           display: block;
           margin-top: 10px;
             font-size: 9px;
           text-transform: uppercase;
           font-family: sans-serif;
           margin-left: -120px;
       }
       #SelectedSuspendReasons option:nth-child(even) 
       {
           background-color: aliceblue;
       }
       
          
</style>

<script type="text/javascript">

    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        theme_advanced_buttons1: "undo,redo",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "bottom",
        theme_advanced_toolbar_align: "left"
    });


    $(document).ready(function () {

        $("#warningWarning, span[id^='warningGoodBad']").hide();
        $('select.reasonsStyle').multiselect();

        //attach validation event
        attachAnswerKeyValidationEvent();

        //if include is not selected uncheck 
        uncheckRadionForNonIncluded();

        //Disables and enables the Suspend Reason List when buttons are clicked
        suspendListActivate();

        //Checklist logic
        checklistClear();



    });

    function checklistClear() {
        $("#sendNoWarning").click(function () {
            if ($(this).is(":checked")) {
                $("#WarningContactInfo,#WarningInappropriateContactInfo,#WarningGeneralViolation").attr("checked", false);
            }
        });

        $("#WarningContactInfo,#WarningInappropriateContactInfo,#WarningGeneralViolation").click(function () {
            if ($(this).is(":checked") && $("#sendNoWarning").is(":checked")) {
                $("#sendNoWarning").attr("checked", false);
            }
        });

        $('#MoveToGroupingIDButton').click(function (event) {
            MoveTTMemberProfile(event, $(this), $('#TTMemberProfileID').val(), $('#MemberID').val(), $('#MoveToGroupingID').val());
        });
    }

    function suspendListActivate() {
        var isSuspend = $('input[name="Suspend"]').val().toLowerCase() === 'true';

        $("input#allowBox").attr("checked", !isSuspend);
        $("#suspendUIBox").attr("checked", isSuspend);

        if ($("input#allowBox").is(":checked")) {
            $("#SelectedSuspendReasons").attr("disabled", true);
        }

        $("#suspendUIBox").click(function () {
            $("#SelectedSuspendReasons").removeAttr("disabled");
            $("#SelectedSuspendReasons option[value='0']").attr('selected', 'selected');
            $("input#allowBox").attr("checked", false);
            $('input[name="Suspend"]').val('true');
        });

        $("input#allowBox").click(function () {
            $("#SelectedSuspendReasons").attr("disabled", true);
            $("#suspendUIBox").attr("checked", false);
            $("#SelectedSuspendReasons option").attr('selected', false);
            $('input[name="Suspend"]').val('false');
        });

    }

    function uncheckRadionForNonIncluded() {


        $('input:radio[id^="TextAcceptable"]').each(function () {

            var index = $(this).attr('id').substring(14);

            if (!$('#IncludeInTest' + index).is(':checked')) {

                if ($(this).is(':checked')) {

                    $(this).removeAttr('checked');
                }
            }

        });
    }

    function attachAnswerKeyValidationEvent() {

        $('#saveAnswerKey').click(function (e) {

            $("#warningWarning").hide();
            $("#suspendWarning").hide();

            var valid = true;
            var selectWarning = false;
            var isBadSelectedCounter = 0;
            var includeErrorCounter = 0;



            $('input:radio[id^="TextAcceptable"]').each(function () {

                var index = $(this).attr('id').substring(14);
                $('#warningGoodBad' + index).hide();
                $('#TextUnAcceptableValidation' + index).hide();

                if ($('#IncludeInTest' + index).is(':checked')) {

                    if ($('input[name=TextAcceptable' + index + ']:checked').length) {

                        if ($(this).is(':checked') && $(this).val() == 'true') { //make sure either a good or a bad is selected. 

                            // do nothing. 

                        }

                        if ($(this).is(':checked') && $(this).val() == "false") { // if a bad is selected make sure a reason is selected.

                            if ($('input[name=multiselect_TextUnacceptableReasons' + index + '][aria-selected=true]').length == 0) {
                                $('#TextUnAcceptableValidation' + index).css("display", "inline-block");
                                alertMessage('Please review the errors on the page');
                                valid = false;
                                window.scrollTo(0, 0);
                                e.preventDefault();

                            }

                            isBadSelectedCounter++;
                        }


                    } else {

                        $('#warningGoodBad' + index).show();
                        alertMessage('Please review the errors on the page');
                        includeErrorCounter++;
                    }

                }

            });



            if (includeErrorCounter > 0) {
                valid = false;
                window.scrollTo(0, 0);
                e.preventDefault();
            }

            else if ($("#suspendUIBox").is(":checked")) {
                if ($("#SelectedSuspendReasons option:selected").val() <= 0) {
                    alertMessage("Please review the errors on the page");
                    $("#suspendWarning").css("display", "block");
                    valid = false;
                }
            }

            else {

                if (isBadSelectedCounter > 0) { //lets make sure a warning is selected. 

                    var isSelectedWarning = false;

                    $("#warnings").find("input[type=checkbox]").each(function () {

                        if ($(this).is(":checked")) {
                            isSelectedWarning = true;
                        }

                    });

                    if (!isSelectedWarning) {
                        $("#warningWarning").show();
                        alertMessage('Please review the errors on the page');
                    }

                    valid = isSelectedWarning;
                }

            }






            return valid;
        });

    }



</script>


<div class="mainDivTT">
<h2 style="padding-top: 10px; padding-left: 5px;">Training Sample Answer Key</h2>
<% Html.BeginForm("SaveTTAnswer", "TrainingTool"); %>
<%=Html.HiddenFor(m=>m.TTMemberAttributeTextIDsString) %>
<%=Html.HiddenFor(m=>m.TTMemberProfileID) %>
<%=Html.HiddenFor(m=>m.MemberID) %>
<%=Html.HiddenFor(m=>m.Suspend) %>

<div class="headerFTT clear-both">
    
    <ul>
        <li>
            Member ID:  <%=Html.DisplayTextFor(m => m.MemberID)%>
        </li>
        <li>
            Current bucket: <%=Html.DisplayTextFor(m=>m.CurrentBucketName) %>        
        </li>
        <li>
                Move to:&nbsp;<%=Html.DropDownList("MoveToGroupingID", Model.GroupingList) %>
            <span>
                <input type="button" id="MoveToGroupingIDButton" value="Move" style="width: 7%;"/>
            </span>
        </li>
        <li>
           <h2 id="answerKeyRules"> To Set Answer Key:</h2>
           <ul>
                <li>Check the “Include” checkbox for each essay to include in the training sample</li>
                <li>Select the “Good” or “Bad” radio button for each essay included in the sample</li>
                <li>For all essays marked “Bad” you must choose a reason from the “Select options” dropdown</li>
                <li>A “Warnings:” email must also be checked off if at least one included essay is marked “Bad”</li>
                <li>If trainee should suspend this member, select "Suspend" and ALL acceptable suspend reasons from multi-select list (hold CTRL to select multiple reasons)</li>
                <li>Trainees will only pick 1 suspend reason that will be "Correct" if included here as an acceptable suspend reason</li>
                <li>Finally, to save changes and set the answer key click “Save Answer Key”</li>
            </ul>
        </li>    
    </ul>
</div>
  <div style="width:100%; margin-bottom: 30px;">
       
        <div style="display:inline; width:200px; float:left;">
            <span id="warningWarning"  style="display: none;color:Red;"> Please select a Warning.</span>
            <span style="display:inline; float:left; font-weight:bold;">Warnings: <br /></span>
            <span style="display:inline; text-align:left; float:right;" id="warnings">
                <%=Html.CheckBoxFor(m=>m.WarningContactInfo) %>Contact Info<br/>
                <%=Html.CheckBoxFor(m=>m.WarningInappropriateContactInfo) %>Inappropriate Content<br/>
                <%=Html.CheckBoxFor(m=>m.WarningGeneralViolation) %>General Violation<br /> 
                <input type="checkbox" id="sendNoWarning"   />Send no warning      
            </span>
        </div>
        <div style="display:inline; float:left; margin-left: 40px;">
        <span id="suspendWarning"  style="display: none;color:Red;"> Please select a resason for suspension.</span>
            <span style="display:inline; float:left; font-weight:bold;"> Approve or Suspend:<br /></span>
            <span style="display:inline; text-align:left; float:right;" id="approveSuspend">
                <input type="checkbox" id="allowBox" checked="checked"/> Approve <br />
                <input type="checkbox" id="suspendUIBox"/>Suspend<br/>
                <%--<%=Html.CheckBoxFor(m=>m.Suspend) %>Suspend--%>
                <% /*=Html.RadioButton("approveSuspend", "Approve", !Model.Suspend) Approve <br />
                <%=Html.RadioButton("approveSuspend", "Suspend", Model.Suspend)Suspend */%> 
                <%=Html.ListBoxFor(x=>x.SelectedSuspendReasons, Model.SuspendReasons) %>
            </span>
        </div>
        <div style="text-align:right;">
            <input type="submit" value="Save Answer Key" id="saveAnswerKey"/>
        </div>
    </div>
 
    <%foreach (TTMemberAttributeTextUI text in Model.TTMemberAttributeTexts)
      {%>
        <div class="ttCreateGrouping">
            <div style="width:55px; display:inline; float:left; margin-right:10px;">
                <span style="font-size:11px;"><%= Html.CheckBox("IncludeInTest" + text.TTMemberAttributeTextID, text.IncludeInTest) %> Include ? </span>
            </div> 
            <div style="display:inline; float:inherit;">
                <span style="font-size:12px; margin-bottom:10px;"><%= text.MemberAttributeName %>:</span>
                <%=Html.TextArea("Attribute" + text.TTMemberAttributeTextID, text.FieldValue, new { style = "width: 700px;height:" + text.TextboxHeight + "px", @class = " " })%>
            </div>
            <div style=" float:right; display:inline;">
                <div style="width:100%; font-size:16px;padding-bottom:5px; font-size:11px;">
                    <span id="warningGoodBad<%= text.TTMemberAttributeTextID.ToString() %>"  visible="false" style="display: none;color:Red;width:5px;">Select an option.</span>
                    <%=Html.RadioButton("TextAcceptable" + text.TTMemberAttributeTextID, "true", text.IsGood) %>Good
                    <%=Html.RadioButton("TextAcceptable" + text.TTMemberAttributeTextID, "false", !text.IsGood)%>Bad
                </div>
                <div>
                    <%=Html.ListBox("TextUnacceptableReasons" + text.TTMemberAttributeTextID, text.TextUnacceptableReasons, new { @class = "reasonsStyle" })%>
                    <br />
                    <span id="TextUnAcceptableValidation<%= text.TTMemberAttributeTextID.ToString() %>"  visible="false" style="display: none;color:Red;width:195px;">Select at least one option from the list.</span>
                </div>
            </div>
        </div>
    <% } %>
    

 
<% Html.EndForm();%>
 <a href="#answerKeyRules" style="float: left; clear: both;">Back to top</a>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    
    <script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js" type="text/javascript" ></script>
    <script src="/Scripts/jquery.multiselect.js" type="text/javascript" ></script>
    <script type="text/javascript" src="/Scripts/tiny_mce/tiny_mce.js"></script>
    <link href="/Content/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	<link href="/Content/prettify.css" rel="stylesheet" type="text/css" />
	
    
    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <% if (Model.SuperTopNav != null)
       {
           Html.RenderPartial("Controls/TTSuperTopNav", Model.SuperTopNav);
       }%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
