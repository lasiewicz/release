﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.TrainingTool.TTTraineeStart>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	TTTraineeStart
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h2 
        {
            margin:10px 0 30px 0;
        }
        .bordered-table 
        {
            margin: 0 auto;
        }
        .bordered-table tr td {
            border: 1px solid black;
            padding: 5px;
            height: 25px;
            vertical-align: middle;
        }
        .bordered-table th {
            border: 1px solid black;
            padding: 5px;
            font-family: Arial;
            font-variant: small-caps;
            text-transform: lowercase;
            letter-spacing: 2px;
            font-weight: 600;
        }
        
        tr.topRow th:nth-child(1) 
        {
            background-color: #A6D785;
        }
        tr.topRow th:nth-child(2) 
        {
            background-color: #FFEC8B;
        }
        tr.topRow th:nth-child(3) 
        {
            background-color: #C2E3FD;
        }
        .bordered-table tr.titleRow 
        {
            font-family: arial;
            text-transform: lowercase;
            font-variant: small-caps;
            text-align: center;
        }
        .bordered-table tr.resultsRow 
        {
            text-align: center;
            font-size: 14px;
        }
        .bordered-table tr.titleRow td:nth-of-type(-n+4),.bordered-table tr.resultsRow:nth-child(even) td:nth-of-type(-n+4)
        {
            background-color: #DFFCCC;
        }
        .bordered-table tr.titleRow td:nth-of-type(n+5),.bordered-table tr.resultsRow:nth-child(even) td:nth-of-type(n+5)
        {
            background-color: #FFFAE0;
        }
        .bordered-table tr.titleRow td:nth-of-type(n+9),.bordered-table tr.resultsRow:nth-child(even) td:nth-of-type(n+9)
        {
            background-color: aliceblue;
        }
        
        .ttTraineeStartPreviousSessions {
            clear: both;
        }
        
       .ttTraineeStart div:nth-child(1), .ttTraineeStart div:nth-child(2)
       {
           border: 1px solid black;
           margin: 10px;
           padding: 5px;
           -webkit-border-radius: 10px;
           -moz-border-radius: 10px;
           border-radius: 10px;
           height: 30px;
           width: 250px; 
           float: left;
       }
       
       .ttTraineeStart div:nth-child(1)
       {
           background-color:#F7F8E0;
       }
       
       .ttTraineeStart div:nth-child(2)
       {
           width: 400px;
           background-color:#A9F5F2;
          
       }
       
    </style>

<div style="float: left; ">
    
    <input type="hidden" id="HiddenTestInstanceID"/>

    <h2>Agent Training Home</h2>
    <div class="ttTraineeStart">
        <div><%=Html.DisplayTextFor(m=>m.TraineeEmail) %></div>
        <div>
            Start New Training: <%=Html.DropDownList("GroupingID", Model.GroupingList) %>
            <input type="button" id="StartNewTrainingButton" value="Start" />
        </div>
        <div id="StartNewTrainingLink"></div>
        <% if (Model.LastPausedTTTestInstanceID > -1)
           {%>
            <a href="/TrainingTool/TTTestPage/<%=Model.LastPausedTTTestInstanceID%>">Continue the paused test</a>
           <%} %>
    </div>

    <div class="ttTraineeStartPreviousSessions">
        <div>
            <% Html.RenderPartial("Controls/TTTestResults", Model.TestResults); %>
        </div>
    </div>

</div> <%--Whole page div--%>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('#StartNewTrainingButton').click(function (event) {
            $('#StartNewTrainingLink').hide();
            CreateNewTTTestInstance(event, $(this), $('#GroupingID').val(), <%=Model.g.AdminID %>);
        });

        $('#HiddenTestInstanceID').change(function() {
            $('#StartNewTrainingLink').html('<a href=\"/TrainingTool/TTTestPage/' + $(this).val() + '\">Click to enter test</a>');
            $('#StartNewTrainingLink').show();
        });
    });
</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
