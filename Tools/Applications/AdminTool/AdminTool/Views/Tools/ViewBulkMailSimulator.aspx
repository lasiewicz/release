﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Tools.BulkMailSimulatorModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
       .headerLabel
       {
           font-family:Calibri;
           padding-left:20px;
           font-weight:bold;
           font-size:20px;
           
       }
       .label
       {
           font-weight:bold;
           font-size:larger;
       }
       h2
       {
          color:#C00000;
          font-size:large;
       }
       body
       {
           font-size:14px;
           font-weight:normal;
       }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%> 
    <%Html.RenderPartial("../Member/Controls/TopNav", Model.TopNav); %>
   <script src="/Scripts/jquery.lightbox_me.js" type="text/javascript" ></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div style="padding-left:35px; padding-top:10px;">

<%if (Model.Errored) { %>
    <p>Error Encountered: <%=Model.ErrorMessage %></p>
<%} else { %>

    <div>
        <div>
            <span class="headerLabel">On Record: </span><br />
        </div>
        <div style="padding-left:143px;">
            Last MatchMail Sent To Member: <%=Model.ScheduleRecord.LastSentDate.ToString() %><br />
            Last MatchMail Attempted For Member: <%=Model.ScheduleRecord.LastAttemptDate.ToString() %><br />
            Total Matches Sent To Member:  <%=Model.ScheduleRecord.SentCount.ToString() %><br />
        </div>
    </div>
    
    
    <br />
    
 
    <div>
         <div>
            <span class="headerLabel">This Simulation:  </span><br />
         </div>
         <div style="padding-left:143px;">
            Matches Found: <%=Model.MatchesFound.ToString()%><br />
            <%if(Model.UseAgeFilter) { %>
                Strict Search With Age Filter Sufficient: <%=Model.InitialSearchSufficientWithAgeFilter.ToYesNoString() %><br />
            <% } %>
            <%if(!Model.InitialSearchSufficientWithAgeFilter){ %>
            Strict Search Sufficient: <%=Model.InitialSearchSufficient.ToYesNoString() %><br />
            <%if(!Model.InitialSearchSufficient){ %>
                    <%if(Model.UseAgeFilter) { %>
                        Lax Search With Age Filter Sufficient: <%=Model.LaxSearchSufficientWithAgeFilter.ToYesNoString() %><br/>
                    <% } %>
                    <%if(!Model.LaxSearchSufficientWithAgeFilter){ %>
                Lax Search Sufficient: <%=Model.LaxSearchSufficient.ToYesNoString() %>
            <% } %>
                <% } %>
            <% } %>
            <br />
            Keyword Search Enabled: <%=Model.KeywordSearchEnabled.ToYesNoString() %><br />
            Reverse Age Filter Enabled: <%=Model.UseAgeFilter.ToString()%><br />
            SearchPrefType (70_MM3Generic): <%=Model.SearchPrefType.ToString()%><br />
        </div>
    </div>
 
    <%if(Model.UseAgeFilter) { %>
    <hr/>
    <div style="padding-left:200px; padding-top:10px;">
        <div>
            <h2>Strict Search With Age Filter</h2><br />
        </div>
        <div>
            <b>Search Preferences:</b><br />
            <%foreach (MatchSearchPreference preference in Model.InitialSearchWithAgeFilter.Preferences)
              { %>
                <span class="label"><%=preference.Name %></span>: <%=preference.Value %><br />
            <%} %><br />
            Number of Search Results: <%=Model.InitialSearchWithAgeFilter.TotalSearchResults.ToString()%><br />
            Number of Results Already on ScrubList: <a href="#" id="show-scrublist1"><%=Model.InitialSearchWithAgeFilter.AlreadyOnScrubList.ToString()%></a><br />
            <div class="modal" id="show-scrublist1-overlay">                
                <br />
                <h2>Strict Search With Age Filter</h2><br /><br />
                <p class="send-content">Scrublist Ids: <span><%=Model.InitialSearchWithAgeFilter.AlreadyOnScrubListIdsToString()%></span></p>
                <br /><br />                    
            </div>
            Number of Results Ineligible: <%=Model.InitialSearchWithAgeFilter.IneligibleMatches.ToString()%><br />
            Number of Matches Found: <%=Model.InitialSearchWithAgeFilter.Matches.Count.ToString()%><br />
        </div> 



      </div>
       <div style="padding-left:250px; padding-top:10px;">
            <%if (Model.InitialSearchWithAgeFilter.Matches.Count > 0) { %>
                <br /><h2>Matches Found</h2>:
                <%foreach (MatchMember member in Model.InitialSearchWithAgeFilter.Matches) {%>
                    <br />MemberID: <%=member.MemberID.ToString() %><br />
                    UserName: <%=member.UserName%><br />
                    Gender/Seeking: <%=member.GenderAndSeeking%><br />
                    Age: <%=member.Age.ToString()%><br />
                    Location: <%=member.Location%><br />
                    Preferred Min Age: <%=member.PreferredMinAge%><br />
                    Preferred Max Age: <%=member.PreferredMaxAge%><br />
                    Distance: <%=member.Distance.ToString() %> miles<br />
                <%} %>
            <%} %>
        </div>
    <% } %>
    <%if (!Model.InitialSearchSufficientWithAgeFilter) { %>
    <hr/>
    <div style="padding-left:200px; padding-top:10px;">
        <div>
            <h2>Strict Search</h2><br />
        </div>
        <div>
            <b>Search Preferences:</b><br />
            <%foreach (MatchSearchPreference preference in Model.InitialSearch.Preferences)
              { %>
                <span class="label"><%=preference.Name %></span>: <%=preference.Value %><br />
            <%} %><br />
            Number of Search Results: <%=Model.InitialSearch.TotalSearchResults.ToString()%><br />
            Number of Results Already on ScrubList: <a href="#" id="show-scrublist2"><%=Model.InitialSearch.AlreadyOnScrubList.ToString()%></a><br />
            <div class="modal" id="show-scrublist2-overlay">                                       
                    <br />
                    <h2>Strict Search</h2><br /><br />
                    <p class="send-content">Scrublist Ids: <span><%=Model.InitialSearch.AlreadyOnScrubListIdsToString()%></span>                                                                         
                    <br /><br />                    
            </div>
            Number of Results Ineligible: <%=Model.InitialSearch.IneligibleMatches.ToString()%><br />
            Number of Matches Found: <%=Model.InitialSearch.Matches.Count.ToString()%><br />
        </div>
      </div>
       <div style="padding-left:250px; padding-top:10px;">
            <%if (Model.InitialSearch.Matches.Count > 0) { %>
                <br /><h2>Matches Found</h2>:
                <%foreach (MatchMember member in Model.InitialSearch.Matches) {%>
                    <br />MemberID: <%=member.MemberID.ToString() %><br />
                    UserName: <%=member.UserName%><br />
                    Gender/Seeking: <%=member.GenderAndSeeking%><br />
                    Age: <%=member.Age.ToString()%><br />
                    Location: <%=member.Location%><br />
                    Preferred Min Age: <%=member.PreferredMinAge%><br />
                    Preferred Max Age: <%=member.PreferredMaxAge%><br />
                    Distance: <%=member.Distance.ToString() %> miles<br />
                <%} %>
            <%} %>
        </div>
      
      <%if (!Model.InitialSearchSufficient) { %>
            <%if(Model.UseAgeFilter) { %>
              <hr/>
              <div style="padding-left:300px; padding-top:10px;">
          
                    <div>
                        <h2>Lax Search With Age Filter</h2><br />
                    </div>
                    <div>
                        <b>Search Preferences:</b>
                        <br />
                        <%foreach (MatchSearchPreference preference in Model.LaxSearchWithAgeFilter.Preferences){ %>
                            <b><%=preference.Name %></b>: <%=preference.Value %><br />
                        <%} %><br />
                        Number of Search Results: <%=Model.LaxSearchWithAgeFilter.TotalSearchResults.ToString() %><br />
                        Number of Results Already on ScrubList: <a href="#" id="show-scrublist3"><%=Model.LaxSearchWithAgeFilter.AlreadyOnScrubList.ToString() %></a><br />
                        <div class="modal" id="show-scrublist3-overlay">                                       
                                <br />
                                <h2>Lax Search With Age Filter</h2><br /><br />
                                <p class="send-content">Scrublist Ids: <span><%=Model.LaxSearchWithAgeFilter.AlreadyOnScrubListIdsToString()%></span>                                                                         
                                <br /><br />                    
                        </div>
                        Number of Results Ineligible: <%=Model.LaxSearchWithAgeFilter.IneligibleMatches.ToString() %><br />
                        Number of Matches Found: <%=Model.LaxSearchWithAgeFilter.Matches.Count.ToString() %><br />
                    </div>
              </div> 
              <div style="padding-left:350px; padding-top:10px;">
                         <%if (Model.LaxSearchWithAgeFilter.Matches.Count > 0) { %>
                                <br /><h2>Matches Found</h2>:
                                <%foreach(MatchMember member in Model.LaxSearchWithAgeFilter.Matches) {%>
                                    <br />MemberID: <%=member.MemberID.ToString() %><br />
                                    UserName: <%=member.UserName%><br />
                                    Gender/Seeking: <%=member.GenderAndSeeking%><br />
                                    Age: <%=member.Age.ToString()%><br />
                                    Location: <%=member.Location%><br />
                                    Preferred Min Age: <%=member.PreferredMinAge%><br />
                                    Preferred Max Age: <%=member.PreferredMaxAge%><br />
                                    Distance: <%=member.Distance.ToString() %> miles<br />
                                <%} %>
                            <%} %>
               </div>
            <% } %>
              <%if (!Model.LaxSearchSufficientWithAgeFilter) { %>
              <hr/>
      <div style="padding-left:300px; padding-top:10px;">
          
            <div>
                <h2>Lax Search</h2><br />
            </div>
            <div>
                <b>Search Preferences:</b>
                <br />
                <%foreach (MatchSearchPreference preference in Model.LaxSearch.Preferences){ %>
                    <b><%=preference.Name %></b>: <%=preference.Value %><br />
                <%} %><br />
                Number of Search Results: <%=Model.LaxSearch.TotalSearchResults.ToString() %><br />
                Number of Results Already on ScrubList: <a href="#" id="show-scrublist4"><%=Model.LaxSearch.AlreadyOnScrubList.ToString() %></a><br />
                <div class="modal" id="show-scrublist4-overlay">                                       
                        <br />
                        <h2>Lax Search</h2><br /><br />
                        <p class="send-content">Scrublist Ids: <span><%=Model.LaxSearch.AlreadyOnScrubListIdsToString()%></span>                                                                         
                        <br /><br />                    
                </div>
                Number of Results Ineligible: <%=Model.LaxSearch.IneligibleMatches.ToString() %><br />
                Number of Matches Found: <%=Model.LaxSearch.Matches.Count.ToString() %><br />
            </div>
      </div> 
      <div style="padding-left:350px; padding-top:10px;">
                 <%if (Model.LaxSearch.Matches.Count > 0) { %>
                        <br /><h2>Matches Found</h2>:
                        <%foreach(MatchMember member in Model.LaxSearch.Matches) {%>
                            <br />MemberID: <%=member.MemberID.ToString() %><br />
                            UserName: <%=member.UserName%><br />
                            Gender/Seeking: <%=member.GenderAndSeeking%><br />
                            Age: <%=member.Age.ToString()%><br />
                            Location: <%=member.Location%><br />
                                    Preferred Min Age: <%=member.PreferredMinAge%><br />
                                    Preferred Max Age: <%=member.PreferredMaxAge%><br />
                            Distance: <%=member.Distance.ToString() %> miles<br />
                        <%} %>
                    <%} %>
       </div>
      <%} %>
          <%} %>
      <%} %>
       
  <br />
    
    
  
<%} %>

</div>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('#show-scrublist1,#show-scrublist2,#show-scrublist3,#show-scrublist4').click(function () {
            $('#show-scrublist1-overlay,#show-scrublist2-overlay,#show-scrublist3-overlay,#show-scrublist4-overlay').lightbox_me({
                centered: true
            });
            return false;
        });
    });
</script>


</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Member/Controls/MemberLeft", Model.Left); %>
</asp:Content>
