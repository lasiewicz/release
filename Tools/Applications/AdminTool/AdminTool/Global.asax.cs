﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Spark.Common.Adapter;
using Spark.CommonLibrary.Logging;
using AdminTool.Models;

namespace AdminTool
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.aspx/{*pathInfo}");
            routes.IgnoreRoute("{resource}.swf/{*pathInfo}");
            routes.IgnoreRoute("{resource}.fla/{*pathInfo}");

            #region Routes with constraints
            
            routes.MapRoute(
                "Member", // Route name
                "{controller}/{action}/{siteID}/{memberID}", // URL with parameters
                null,
                new { controller = "Member" }
            );

            routes.MapRoute(
                "Member/EditProfile", // Route name
                "{controller}/{action}/{siteID}/{memberID}/{controlType}", // URL with parameters
                null, // Parameter defaults
                new { controller = "Member"}
            );

            // Must put this before the route for MemberAPI/BillingInfo
            // Otherwise it matches on the first set of characters and uses the route for MemberAPI/BillingInfo 
            routes.MapRoute(
                "MemberAPI/BillingInfoForFreeTrial", // Route name
                "{controller}/{action}/{userPaymentGUID}/{siteID}/{memberID}", // URL with parameters
                null, // Parameter defaults
                new { controller = "MemberAPI", action = "BillingInfoForFreeTrial" }
            );

            routes.MapRoute(
                "MemberAPI/BillingInfo", // Route name
                "{controller}/{action}/{orderID}/{siteID}/{memberID}", // URL with parameters
                null, // Parameter defaults
                new { controller = "MemberAPI", action = "BillingInfo" }
            );

            routes.MapRoute(
                "RegionAPI", // Route name
                "{controller}/{action}/{siteID}", // URL with parameters
                null, // Parameter defaults
                new { controller = "RegionAPI" } 
            );

            routes.MapRoute(
                "SubAdminWithParameters", // Route name
                "{controller}/{action}/{promoId}/{brandId}", // URL with parameters
                null, // Parameter defaults
                new { controller = "SubAdmin" }
            );
            
            routes.MapRoute(
            "TrainingToolTestPage", // Route name
            "{controller}/{action}/{testInstanceID}/{ttMemberProfileIDToExclude}", // URL with parameters
            new { ttMemberProfileIDToExclude = -1},   // parameter defaults
            new { controller = "TrainingTool", action = "TTTestPage" } // constraints
            );
            
            routes.MapRoute(
            "TrainingToolEmailList", // Route name
            "{controller}/{action}/{ttMemberProfileID}/{folderID}/{rowIndex}", // URL with parameters
            null,   // parameter defaults
            new { controller = "TrainingTool", action = "TTEmailList" } // constraints
            );

            routes.MapRoute(
             "TrainingTool", // Route name
             "{controller}/{action}/{memberID}/{groupingID}", // URL with parameters
             null,  // parameter defaults
             new { controller = "TrainingTool" }    // constraints
             );

            //routes.MapRoute(
            // "TrainingToolGroupingIdOnly", // Route name
            // "{controller}/{action}/{ID}", // URL with parameters
            // null,  // parameter defaults
            // new { controller = "TrainingTool" }    // constraints
            // );
            
            #endregion

            #region Routes with no default and no constraint

            routes.MapRoute(
              "Email",    // route name
              "{controller}/{action}/{siteID}/{memberID}/{folderID}/{rowIndex}",  // URL with parameters,
              null    // parameter defaults
              );

            routes.MapRoute(
                "DefaultOneID", // Route name
                "{controller}/{action}/{ID}", // URL with parameters
                null // Parameter defaults
            );

            // Mike: I don't think this will ever hit, so commenting it out
            //routes.MapRoute(
            //   "TextApproval", // Route name
            //   "{controller}/{action}/{languageID}", // URL with parameters
            //   null // Parameter defaults
            //   );

            routes.MapRoute(
               "MemberTextApproval", // Route name
               "{controller}/{action}/{memberID}/{siteID}", // URL with parameters
               null // Parameter defaults
               );

            routes.MapRoute(
                "SubAdmin",
                "{controller}/{action}",
                null
                );
            #endregion

            // Mike: I don't think this will ever hit, so commenting it out
            //default
            //routes.MapRoute(
            //    "Default", // Route name
            //    "{controller}/{action}/{id}", // URL with parameters
            //    new { controller = "Search", action = "Index", id = "" } // Parameter defaults
            //);
            
            //handles application root
            routes.MapRoute(
                "Root",
                "",
                new { controller = "Search", action = "Index", id = "" }
                );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);
            //RouteDebug.RouteDebugger.RewriteRoutesForTesting(RouteTable.Routes);

            Logger.Configure(Server.MapPath("/") + "log4net.config");

        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            PurchaseServiceWebAdapter.CloseProxyInstance();
            RenewalServiceWebAdapter.CloseProxyInstance();
            AccessServiceWebAdapter.CloseProxyInstance();
            PaymentProfileServiceWebAdapter.CloseProxyInstance();
            AuthorizationServiceWebAdapter.CloseProxyInstance();
            OrderHistoryServiceWebAdapter.CloseProxyInstance();
            DiscountServiceWebAdapter.CloseProxyInstance();
            CatalogServiceWebAdapter.CloseProxyInstance();
        }

        protected void Application_End()
        {
            
        }
    }
}