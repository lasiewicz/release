﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminTool
{
    /*
	This page is called by the middle tier when it needs to expire a cache item.
	*/
    public partial class CacheManager : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string key = Request.QueryString["DeleteKey"];

            if (key != null)
            {
                Matchnet.Caching.Cache.Instance.Remove(key);
            }
            
        }
    }
}
