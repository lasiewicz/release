﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.ModelViews.SubAdmin;
using AdminTool.Models.ModelViews;
using Matchnet.Content.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Purchase.ValueObjects;
using Newtonsoft.Json;
using Plan = AdminTool.Models.ModelViews.SubAdmin.Plan;
using Promo = AdminTool.Models.ModelViews.SubAdmin.Promo;

namespace AdminTool.Controllers
{
    public class SubAdminController : BaseController
    {
        private const string PlanState = "PlanState";
        private const string PromoStep = "PromoStep";
        private const string PageMessage = "PageMessage";
        private const string PageMode = "PageMode";

        public ActionResult PromoSearch()
        {
            base.CheckActionPermission(Enums.Operations.PromoSearch, true);

            var manager = new PromoManager();
            var data = manager.SetupPromoSearch();

            return View("PromoSearch", data);
        }

        public ActionResult GiftSearch(string giftCode)
        {
            return View(new GiftSearchModelView(new SharedTopNav(), giftCode));
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SubmitPromoSearch(FormCollection collection)
        {
            base.CheckActionPermission(Enums.Operations.PromoSearch, true);

            var manager = new PromoManager();
            var data = manager.PerformPromoSearch(collection);

            return View("PromoSearch", data);
        }

        public ActionResult PromoPriority()
        {
            base.CheckActionPermission(Enums.Operations.PromoSearch, true);

            var manager = new PromoManager();
            var promoPriority = new PromoPriority();
            promoPriority.BrandId = 1003;
            promoPriority.PaymentType = (int) PaymentType.CreditCard;
            promoPriority.PromoType = (int) PromoType.Member;

            manager.GetPromoList(promoPriority);
            SubAdminHelper.PopulateSelectListItems(promoPriority);

            return View("PromoPriority", promoPriority);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdatePromoPriority(PromoPriority promoPriority, FormCollection collection)
        {
            base.CheckActionPermission(Enums.Operations.CreatePromo, true);

            var manager = new PromoManager();
            if(promoPriority.IsRefresh)
            {
                // just refresh the promo list
                manager.GetPromoList(promoPriority);
            }
            else
            {
                // update the promos
                manager.UpdatePromoPriority(promoPriority);
                // get the updated list
                manager.GetPromoList(promoPriority);
                ViewData[PageMessage] = "Promo ordering has been updated";
            }

            SubAdminHelper.PopulateSelectListItems(promoPriority);

            ModelState.Clear();

            return View("PromoPriority", promoPriority);
        }

        public ActionResult CreatePromo()
        {
            base.CheckActionPermission(Enums.Operations.CreatePromo, true);

            var manager = new PromoManager();
            var data = manager.GetBlankPromo();

            ViewData[PromoStep] = 1;

            return View("CreatePromo", data);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteCreatePromo(Promo promo, FormCollection collection)
        {
            base.CheckActionPermission(Enums.Operations.CreatePromo, true);

            var manager = new PromoManager();
            var promoStep = int.Parse(collection[PromoStep]);

            // Do not disable this. This is not done for testing or just for fun. If you don't clear, mvc will bind things wrong
            // in a 2 dimensional data situation, e.g., plan selectors.
            ModelState.Clear();

            var hasError = false;
            var pageMessage = manager.SavePromoStep(promo, promoStep, out hasError);

            if (promoStep == 5 && !hasError)
            {
                if (promo.PromoType == PromoType.Member)
                {
                    return RedirectToAction("PromoPriority");
                }
                else
                {
                    if (promo.PromoType == PromoType.EmailLink || promo.PromoType == PromoType.EmailLinkWithTargeting)
                    {
                        // pageMessage comes back with the promoId if there was a successful insert
                        var promoURL = string.Format("http://{0}.{1}/Applications/Subscription/Subscribe.aspx?promoplan={2}",
                                                            "www",
                                                            BrandConfigSA.Instance.GetBrandByID(promo.BrandId).Uri.ToLower(),
                                                            GeneralHelper.Encrypt(int.Parse(pageMessage)));

                        pageMessage = string.Format("PromoID: {0} has been created.<br />Promo URL: {1}", pageMessage, promoURL);

                    }
                    else
                    {
                        pageMessage = string.Format("PromoID: {0} has been created.", pageMessage);
                    }
                }
            }

            ViewData[PageMessage] = pageMessage;
            ViewData[PromoStep] = hasError ? promoStep : ++promoStep;

            TempData["PrePromo"] = promo; //Store promo to use in preview
            TempData["PrePromoStep"] = promoStep;

            return View("CreatePromo", promo);
        }

        public ActionResult PreviewPromo(string planSelectors)
        {
            if (!string.IsNullOrEmpty(planSelectors))
            {
                string[] plansInfo = planSelectors.Split(',');
                var selectors = new List<PlanSelector>();
                foreach (var s in plansInfo)
                {
                    string[] planDetails = s.Split(':');
                    var selector = new PlanSelector();
                    selector.ColumnNumber = Convert.ToInt32(planDetails[0]);
                    selector.RowNumber = Convert.ToInt32(planDetails[1]);
                    selector.PlanId = Convert.ToInt32(planDetails[2]);
                    selector.ResourceTemplateId = Convert.ToInt32(planDetails[3]);
                    selectors.Add(selector);
                }
               
                var promo = TempData["PrePromo"] as Promo;
                //var promoStep = (int)TempData["PrePromoStep"];
                if (promo != null)
                {
                    promo.PlanSelectors.Clear();
                    foreach (var planSelector in selectors)
                    {
                        if (planSelector.RowNumber > 0 && planSelector.ColumnNumber > 0)
                        {
                            promo.PlanSelectors.Add(new PlanSelector
                                                        {
                                                            BrandID = promo.BrandId,
                                                            ColumnNumber = planSelector.ColumnNumber,
                                                            RowNumber = planSelector.RowNumber,
                                                            PlanId = planSelector.PlanId,
                                                            ResourceTemplateId = planSelector.ResourceTemplateId
                                                        });
                        }
                    }
                    TempData["PrePromo"] = promo;
                    var manager = new PromoManager();
                    string errorMsg = manager.ValidatePromoStep(promo, 3);
                    if (!string.IsNullOrEmpty(errorMsg))
                        return Content("<html><body><h3>" + errorMsg + "</body></html>");
                    return Content(manager.GeneratePromoPUIHtml(promo));
                }
            }
            return Content("<html><body><h3>Error occured!</h3></body></html>");
        }

        public ActionResult ClonePromo(int promoId, int brandId)
        {
            base.CheckActionPermission(Enums.Operations.CreatePromo, true);

            // Without clearing this, even if we set the promoId to constants.null_int on the model it still
            // retains the original promoId of the promo we are cloning from.
            ModelState.Clear();

            var manager = new PromoManager();
            var data = manager.ClonePromo(promoId, brandId);

            ViewData[PromoStep] = 1;

            return View("CreatePromo", data);
        }

        public ActionResult PromoDetail(int promoId, int brandId)
        {
            base.CheckActionPermission(Enums.Operations.CreatePromo, true);

            var manager = new PromoManager();
            var data = manager.GetPromoDetail(promoId, brandId);

            return View("PromoDetail", data);
        }

        public ActionResult EditPromoEndDate(int promoId, int brandId)
        {
            base.CheckActionPermission(Enums.Operations.CreatePromo, true);

            var manager = new PromoManager();
            var data = manager.GetPromoDetail(promoId, brandId);

            ArrayList errors;
            bool isEditable = manager.IsPromoEditable(data, out errors);
            
            if(isEditable)
            {
                ViewData[PageMode] = "edit";    
            }
            else
            {
                ViewData[PageMessage] = string.Join("<br/>", (string[])errors.ToArray(typeof(string)));
            }

            return View("PromoDetail", data);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteEditPromoEndDate(int promoId, int brandId, DateTime newEndDate)
        {
            base.CheckActionPermission(Enums.Operations.CreatePromo, true);

            var manager = new PromoManager();
            
            ArrayList errors;
            bool updateSuccessful = manager.UpdatePromoEndDate(promoId, brandId, newEndDate, out errors);

            if(updateSuccessful)
            {
                ViewData[PageMessage] = string.Format(
                    "Promo end date is updated PromoID: {0}, New Promo End Date: {1}", promoId,
                    newEndDate.ToString("d"));
            }
            else
            {
                ViewData[PageMode] = "edit";    
                ViewData[PageMessage] = string.Join("<br/>", (string[])errors.ToArray(typeof(string)));
            }

            // we have to get the model to render the view regardless of the error status
            var data = manager.GetPromoDetail(promoId, brandId);

            return View("PromoDetail", data);
        }

        public ActionResult CreatePlan()
        {
            base.CheckActionPermission(Enums.Operations.CreatePlan, true);

            var manager = new PlanManager();
            var data = manager.GetBlankPlan(true);

            return View("CreatePlan", data);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteCreatePlan(Plan plan, FormCollection collection)
        {
            base.CheckActionPermission(Enums.Operations.CreatePlan, true);

            var manager = new PlanManager();

            if (!manager.Validate(plan, ModelState))
            {
                return View("CreatePlan", plan);
            }

            PlanState pState = Models.ModelViews.SubAdmin.PlanState.EnterInfo;

            if (!string.IsNullOrEmpty(collection[PlanState]))
            {
                pState = (PlanState)Enum.Parse(typeof(PlanState), collection[PlanState]);
            }

            switch (pState)
            {
                case Models.ModelViews.SubAdmin.PlanState.EnterInfo: // validation passed and we just need to spit the data back out
                    ViewData[PlanState] = Models.ModelViews.SubAdmin.PlanState.ViewInfo;
                    break;
                case Models.ModelViews.SubAdmin.PlanState.ViewInfo:    // after viewing the read only review, user confirmed
                    ViewData["PageMessage"] = manager.CompleteCreatePlan(plan);
                    ViewData[PlanState] = Models.ModelViews.SubAdmin.PlanState.Inserted;
                    break;
            }

            return View("CreatePlan", plan);
        }
        public ActionResult PlanList(int id)
        {
            // id in this case is brandId
            base.CheckActionPermission(Enums.Operations.CreatePlan, true);

            var manager = new PlanManager();
            var plansDisplay = new PlanDisplayList();

            if (id > 0)
            {
                plansDisplay.BrandId = id;
                manager.GetPlanList(plansDisplay);
            }

            SubAdminHelper.PopulateSelectListItems(plansDisplay);

            return View("PlanList", plansDisplay);
        }

        [HttpPost]
        public ActionResult SendGiftNotification(int giftID, int purchaserMemberID, int purchaserSiteID, string giftCode, string emailAddress)
        {
            GiftManager giftManager = new GiftManager();
            giftManager.SendNotification(giftID,  purchaserMemberID, purchaserSiteID, emailAddress, g.AdminID);
            GiftSearchModelView modelView= new GiftSearchModelView(new SharedTopNav(), giftCode);
            return View("GiftSearch", modelView);
        }

        public ActionResult ResourceManagerTemplates(int? brandId, int? templateTypeId)
        {
            var manager = new ResourceTemplatesManager();
            var data = new ResourceManagerTemplates { Brands = manager.PopulateBrands(),
                                                      TemplateTypes = manager.PopulateTemplateTypes(true)
            };
            if(brandId.HasValue && templateTypeId.HasValue)
            {
                data.BrandId = brandId.Value;
                data.TemplateType = templateTypeId.Value;
            }
            return View(data);
        }

        public ActionResult TemplateDetail(int? templateId, int? brandId, int? templateType)
        {
            ResourceTemplateDetail data;
            var manager = new ResourceTemplatesManager();
            if (templateId.HasValue)
            {
                data = new ResourceTemplateDetail(templateId.Value)
                           {
                               Brands = manager.PopulateBrands(),
                               TemplateTypes = manager.PopulateTemplateTypes(false),
                               IsNew = false
                           };
            }
            else
            {
                data = new ResourceTemplateDetail()
                {
                    Brands = manager.PopulateBrands(),
                    TemplateTypes = manager.PopulateTemplateTypes(false),
                    IsNew = true,
                    BrandId = brandId.HasValue ? brandId.Value : 1003,
                    TemplateTypeId = templateType.HasValue ? (templateType.Value == 0 ? 1 : templateType.Value) : 1
                };
            }
            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult TemplateDetail(ResourceTemplateDetail detail)
        {
            var manager = new ResourceTemplatesManager();
            if (ModelState.IsValid)
            {
                bool status = manager.SaveResourceTemplate(detail);
                ViewData["Message"] = detail.IsNew
                                          ? (status ? string.Format("Resource template {0} created.", detail.TemplateId) : "Error creating template.")
                                          : (status ? "Resource template saved." : "Error saving template.");

                var data = new ResourceManagerTemplates
                               {
                                   Brands = manager.PopulateBrands(),
                                   TemplateTypes = manager.PopulateTemplateTypes(true),
                                   BrandId = detail.BrandId,
                                   TemplateType = detail.TemplateTypeId
                               };

                return View("ResourceManagerTemplates", data);
            }
            detail.Brands = manager.PopulateBrands();
            detail.TemplateTypes = manager.PopulateTemplateTypes(false);
            return View(detail);
        }
    }
}
