﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using AdminTool.Models.JSONResult;
using AdminTool.Models;
using AdminTool.Models.ModelViews;
using Matchnet.Content.ValueObjects.Admin;
using AdminTool.Models.ModelViews.Member;
using Spark.CommonLibrary;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Common.PaymentProfileService;
using Matchnet.ExternalMail.ValueObjects;
using Spark.Common.Adapter;
using Spark.Common.RenewalService;
using Matchnet.Purchase.ServiceAdapters;
using Purchase = Matchnet.Purchase.ValueObjects;
using RestSharp;
using System.Configuration;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Matchnet.Configuration.ServiceAdapters;

namespace AdminTool.Controllers
{
    public class MemberAPIController : BaseController
    {
        public JsonResult UpdateEmail(int siteID, int memberID)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            string email = "";
            try
            {
                //check permission
                base.CheckActionPermission(Enums.Operations.EmailEdit, true);

                email = Request["Email"].Trim();
                if (siteID > 0 && memberID > 0 && !String.IsNullOrEmpty(email))
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    //update email
                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    string errorMessage;

                    if (memberManager.UpdateEmail(email, out errorMessage))
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "Login Email has been saved.";
                    }
                    else
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = errorMessage;
                    }
                    
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateEmail().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating email";
            }

            return Json(result);

        }

        public JsonResult AdjustAllAccessEmails(int siteID, int memberID, int emailCount)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            Brand brand = g.GetBrand(siteID);

            List<int> PrivilegeTypeIDs = new List<int>();
            PrivilegeTypeIDs.Add((int)Spark.Common.AccessService.PrivilegeType.AllAccessEmails);

            try
            {
                bool updateStatus = MemberSA.Instance.AdjustUnifiedAccessCountPrivilege(memberID, g.AdminID, "",
                    PrivilegeTypeIDs.ToArray(), brand.BrandID, siteID, brand.Site.Community.CommunityID, emailCount);
                if (!updateStatus) throw new Exception("Update not successful");
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.AdminAdjust(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error on admin adjust all access email.";
            }
            
            return Json(result);
        }

        public JsonResult PauseOrResumeSubscription(int memberID, int siteID, bool hasPausedSubscription)
        {
            MemberUpdateResult result = new MemberUpdateResult();

            try
            {
                if (!hasPausedSubscription)
                {
                    // CallingSystemTypeID of 8 is the administrator tool 
                    // Need a ReasonID for freezing the subscription 
                    Spark.Common.PurchaseService.FreezeSubscriptionAccountResponse freezeSubscriptionAccountResponse = PurchaseServiceWebAdapter.GetProxyInstance().FreezeSubscriptionAccount(memberID, siteID, 8, g.AdminID, String.Empty, 4);
                    //Spark.Common.PurchaseService.FreezeSubscriptionAccountResponse freezeSubscriptionAccountResponse = PurchaseServiceWebAdapter.GetProxyInstance().FreezeSubscriptionAccount(memberID, siteID, 8, g.AdminID, String.Empty, 4);

                    if (freezeSubscriptionAccountResponse.Code != "0")
                    {
                        throw new Exception(string.Format("Pausing the subscription was unsuccessful. Code: {0} Message: {1}", freezeSubscriptionAccountResponse.Code, freezeSubscriptionAccountResponse.Message));
                    }
                    else
                    {
                        result.StatusMessage = "The subscription was successfully paused.";
                    }
                }
                else
                {
                    Spark.Common.PurchaseService.FreezeSubscriptionAccountResponse freezeSubscriptionAccountResponse = PurchaseServiceWebAdapter.GetProxyInstance().UnFreezeSubscriptionAccount(memberID, siteID, 8, g.AdminID, String.Empty);
                    //Spark.Common.PurchaseService.FreezeSubscriptionAccountResponse freezeSubscriptionAccountResponse = PurchaseServiceWebAdapter.GetProxyInstanceForBedrock().UnFreezeSubscriptionAccount(memberID, siteID, 8, g.AdminID, String.Empty);

                    if (freezeSubscriptionAccountResponse.Code != "0")
                    {
                        throw new Exception(string.Format("Resumeing the frozen subscription was unsuccessful. Code: {0} Message: {1}", freezeSubscriptionAccountResponse.Code, freezeSubscriptionAccountResponse.Message));
                    }
                    else
                    {
                        result.StatusMessage = "The subscription was successfully resumed.";
                    }
                }

                result.Status = (int)AdminTool.Models.Enums.Status.Success;
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.PauseOrResumeSubscription(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                if (!hasPausedSubscription)
                {
                    result.StatusMessage = "Error on pausing the subscription.";
                }
                else
                {
                    result.StatusMessage = "Error on resuming the subscription.";
                }
            }

            return Json(result);
        }
        
        public JsonResult UpdateProfileDisplaySettings(int siteID, int memberID,
            bool showOnline, bool showInSearches, bool showPhotosToNonMembers, bool showActivityTrail)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            result.Status = (int)AdminTool.Models.Enums.Status.Success;

            try
            {
                int hideMask = 0;
                int displayPhotosToGuests = 0;

                if (showPhotosToNonMembers)
                {
                    displayPhotosToGuests = 1;
                }
                if (!showOnline)
                {
                    hideMask += (int)Enums.HideMask.ShowOnline;
                }
                if (!showInSearches)
                {
                    hideMask += (int)Enums.HideMask.ShowInSearches;
                }
                if (!showActivityTrail)
                {
                    hideMask += (int)Enums.HideMask.ShowActivityTrail;
                }

                Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);
                Brand brand = g.GetBrand(siteID);

                member.SetAttributeInt(brand, "HideMask", hideMask);
                member.SetAttributeInt(brand, "DisplayPhotosToGuests", displayPhotosToGuests);
                MemberSaveResult memberSaveResult = MemberSA.Instance.SaveMember(member);

                if (memberSaveResult.SaveStatus != MemberSaveStatusType.Success)
                {
                    throw new Exception("Profile settings update unsuccessful because member save failed.");
                }
                
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateProfileDisplaySettings(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating profile settings.";
            }

            return Json(result);
        }

        public JsonResult UpdateNorbertSetting(int siteID, int memberID, bool isNorbert)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            result.Status = (int)AdminTool.Models.Enums.Status.Success;

            try
            {
                Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);
                Brand brand = g.GetBrand(siteID);

                member.SetAttributeInt(brand, "REDRedesignBetaParticipatingFlag", isNorbert ? 1 : 0);
                member.SetAttributeInt(brand, "REDRedesignBetaOfferedFlag", 1);
                MemberSaveResult memberSaveResult = MemberSA.Instance.SaveMember(member);

                if (memberSaveResult.SaveStatus != MemberSaveStatusType.Success)
                {
                    throw new Exception("Norbert Opt-in update unsuccessful because member save failed.");
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateNorbertSetting(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating Norbert opt-in.";
            }

            return Json(result);
        }

        public JsonResult UpdateRenewalDate(int siteID, int memberID)
        {
            MemberUpdateResult result = new MemberUpdateResult();

            try
            {
                Brand brand = g.GetBrand(siteID);
                Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);

                // Transaction type is set to adjustment 
                Spark.Common.RenewalService.RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().AdjustRenewalDate(
                    memberID, siteID, 1, member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, siteID, brand.Site.Community.CommunityID).EndDateUTC,
                    Constants.NULL_INT, Constants.NULL_INT, g.AdminID, "", 1007, DateTime.MinValue, DateTime.MinValue);

                if (renewalResponse.InternalResponse.responsecode != "0")
                {
                    throw new Exception(string.Format("Renewal date update unsuccessful. Code: {0} Message: {1}", renewalResponse.InternalResponse.responsecode, renewalResponse.InternalResponse.responsemessage));
                }

                member.SetAttributeInt(brand,
                    "SubscriptionStatus",
                    Convert.ToInt16(PurchaseSA.Instance.GetMemberSubStatus(memberID, siteID,
                    member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, siteID, brand.Site.Community.CommunityID).EndDatePST, false).Status));

                MemberSaveResult memberSaveResult = MemberSA.Instance.SaveMember(member);

                if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                {
                    throw new Exception("Renewal date update unsuccessful because member save failed.");
                }
            }

            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateRenewalDate(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error on update renewal date.";
            }

            return Json(result);
        }

        public JsonResult UpdateRenewalInfo(int siteID, int memberID, bool reopenRenewal, bool reopenPremiumHighlight,
            bool reopenPremiumSpotlight, bool reopenPremiumJMeter, bool reopenPremiumAllAccess, bool reopenReadReceipt)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            result.Status = (int)AdminTool.Models.Enums.Status.Success;
            result.StatusMessage = "Error on update renewal.";

            List<int> secondaryPackageIDList = new List<int>();

            try
            {
                RenewalSubscription renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(memberID, siteID);
                Brand brand = g.GetBrand(siteID);

                if (reopenRenewal && renewalSub != null && renewalSub.RenewalSubscriptionID > 0 && renewalSub.PrimaryPackageID > 0)
                {
                    Purchase.Plan memberPlan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, brand.BrandID);
                    if (memberPlan != null)
                    {
                        if (renewalSub.RenewalSubscriptionDetails != null && renewalSub.RenewalSubscriptionDetails.Length > 1)
                        {
                            Purchase.Plan rsePlan = null;
                            foreach (Spark.Common.RenewalService.RenewalSubscriptionDetail rse in renewalSub.RenewalSubscriptionDetails)
                            {
                                if(rse.IsPrimaryPackage)
                                    continue;

                                rsePlan = PlanSA.Instance.GetPlan(rse.PackageID, brand.BrandID);
                                if (reopenPremiumHighlight && (rsePlan.PremiumTypeMask & Purchase.PremiumType.HighlightedProfile) == Purchase.PremiumType.HighlightedProfile)
                                {
                                    secondaryPackageIDList.Add(rse.PackageID);
                                }
                                else if (reopenPremiumSpotlight && (rsePlan.PremiumTypeMask & Purchase.PremiumType.SpotlightMember) == Purchase.PremiumType.SpotlightMember)
                                {
                                    secondaryPackageIDList.Add(rse.PackageID);
                                }
                                else if (reopenPremiumJMeter && (rsePlan.PremiumTypeMask & Purchase.PremiumType.JMeter) == Purchase.PremiumType.JMeter)
                                {
                                    secondaryPackageIDList.Add(rse.PackageID);
                                }
                                else if (reopenPremiumAllAccess && (rsePlan.PremiumTypeMask & Purchase.PremiumType.AllAccess) == Purchase.PremiumType.AllAccess)
                                {
                                    secondaryPackageIDList.Add(rse.PackageID);
                                }
                                else if (reopenReadReceipt && (rsePlan.PremiumTypeMask & Purchase.PremiumType.ReadReceipt) == Purchase.PremiumType.ReadReceipt)
                                {
                                    secondaryPackageIDList.Add(rse.PackageID);
                                }
                            }
                        }

                        RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().EnableAutoRenewal(secondaryPackageIDList.ToArray(),
                             memberID, siteID, 1, g.AdminID, "", Constants.NULL_INT);

                        if (renewalResponse.InternalResponse.responsecode != "0")
                        {
                            throw new Exception(string.Format("Renewal unsuccessful. Code: {0} Message: {1}", renewalResponse.InternalResponse.responsecode, renewalResponse.InternalResponse.responsemessage));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateRenewalInfo(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error on update renewal.";
            }

            return Json(result);
        }

        public JsonResult UpdateALaCarteRenewal(int siteId, int memberId, List<int> premiumTypes, bool enable)
        {
            MemberUpdateResult result = new MemberUpdateResult();

            try
            {
                result.Status = (int) AdminTool.Models.Enums.Status.Success;
                result.StatusMessage = "A La Carte Renewal Info Updated.";

                var memberManager = new MemberManager(memberId, siteId, MemberLoadFlags.None);
                string errorMessage = memberManager.UpdateALaCarteRenewal(premiumTypes, enable);

                if (errorMessage != string.Empty)
                {
                    result.Status = (int) Enums.Status.Failed;
                    result.StatusMessage = errorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateALaCarteRenewal(): ", ex);
                result.Status = (int) AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating a la carte renewal info.";
            }

            return Json(result);
        }

        public JsonResult CancelFreeTrial(int memberId, int siteId)
        {
            MemberUpdateResult result = new MemberUpdateResult();

            try
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
                result.StatusMessage = "Tiral Subscription canceled.";

                var memberManager = new MemberManager(memberId, siteId, MemberLoadFlags.None);
                string errorMessage = memberManager.CancelFreeTrial();

                if (errorMessage != string.Empty)
                {
                    result.Status = (int)Enums.Status.Failed;
                    result.StatusMessage = errorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.CancelFreeTrial(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating free trial renewal info.";
            }

            return Json(result);
        }

        public JsonResult AdminAdjust(int siteID, int memberID,
            Enums.AdminAdjustDirection adjustDirection, DurationType adjustPeriod, int duration, bool includePremiumHighlight,
            bool includePremiumSpotlight, bool includePremiumJMeter, bool includePremiumAllAccess, bool includePremiumReadReceipts)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            
            bool hasSubscription = false;
            bool alreadyHasPremiumHighlightedProfile = false;
            bool alreadyHasPremiumMemberSpotlight = false;
            bool alreadyHasPremiumJMeter = false;
            bool alreadyHasPremiumAllAccess = false;
            bool alreadyHasPremiumReadReceipts = false;

            try
            {
                RenewalSubscription renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(memberID, siteID);

                if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0 && renewalSub.PrimaryPackageID > 0)
                {
                    Matchnet.Purchase.ValueObjects.Plan basePlan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, g.GetBrand(siteID).BrandID);
                    if (basePlan != null)
                    {
                        if ((basePlan.PlanTypeMask & Purchase.PlanType.PremiumPlan) == Purchase.PlanType.PremiumPlan)
                        {
                            if ((basePlan.PremiumTypeMask & Purchase.PremiumType.HighlightedProfile) == Purchase.PremiumType.HighlightedProfile)
                            {
                                alreadyHasPremiumHighlightedProfile = true;
                            }

                            if ((basePlan.PremiumTypeMask & Purchase.PremiumType.SpotlightMember) == Purchase.PremiumType.SpotlightMember)
                            {
                                alreadyHasPremiumMemberSpotlight = true;
                            }

                            if ((basePlan.PremiumTypeMask & Purchase.PremiumType.JMeter) == Purchase.PremiumType.JMeter)
                            {
                                alreadyHasPremiumJMeter = true;
                            }

                            if ((basePlan.PremiumTypeMask & Purchase.PremiumType.AllAccess) == Purchase.PremiumType.AllAccess)
                            {
                                alreadyHasPremiumAllAccess = true;
                            }

                            if ((basePlan.PremiumTypeMask & Purchase.PremiumType.ReadReceipt) == Purchase.PremiumType.ReadReceipt)
                            {
                                alreadyHasPremiumReadReceipts = true;
                            }
                        }
                    }
                }

                if (adjustDirection == Enums.AdminAdjustDirection.Subtract)
                {
                    duration = (-duration);
                }

                if (duration > 0)
                {
                    // complimentary limit to two year
                    DateTime oldRenewDate;
                    if (hasSubscription == true)
                    {
                        oldRenewDate = Matchnet.Lib.Util.Util.CDateTime(renewalSub.RenewalDatePST);
                    }
                    else
                    {
                        oldRenewDate = DateTime.Now;
                    }
                    if (GetNewRenewDate(adjustPeriod, duration, oldRenewDate) > DateTime.Now.AddYears(2))
                    {

                    }
                }

                //get adjust type override
                //TL 08212009 Members with bundled features or a la carte features should automatically have those time adjusted with base subscription
                List<int> PrivilegeTypeIDs = new List<int>();
                PrivilegeTypeIDs.Add((int)Spark.Common.AccessService.PrivilegeType.BasicSubscription);

                if (includePremiumHighlight || alreadyHasPremiumHighlightedProfile)
                {
                    PrivilegeTypeIDs.Add((int)Spark.Common.AccessService.PrivilegeType.HighlightedProfile);
                }
                if (includePremiumSpotlight || alreadyHasPremiumMemberSpotlight)
                {
                    PrivilegeTypeIDs.Add((int)Spark.Common.AccessService.PrivilegeType.SpotlightMember);
                }
                if (includePremiumJMeter || alreadyHasPremiumJMeter)
                {
                    PrivilegeTypeIDs.Add((int)Spark.Common.AccessService.PrivilegeType.JMeter);
                }
                if (includePremiumAllAccess || alreadyHasPremiumAllAccess)
                {
                    PrivilegeTypeIDs.Add((int)Spark.Common.AccessService.PrivilegeType.AllAccess);
                }
                if (includePremiumReadReceipts || alreadyHasPremiumReadReceipts)
                {
                    PrivilegeTypeIDs.Add((int)Spark.Common.AccessService.PrivilegeType.ReadReceipt);
                }

                //Update Unified Access
                Spark.Common.AccessService.AccessServiceClient accessServiceCient = AccessServiceWebAdapter.GetProxyInstanceForBedrock();
                Spark.Common.AccessService.AccessResponse accessResponse = accessServiceCient.AdjustTimePrivilege(memberID, PrivilegeTypeIDs.ToArray(), duration, (int)adjustPeriod, g.AdminID, "", Constants.NULL_INT, siteID, 1);

                result.Status = (int)AdminTool.Models.Enums.Status.Success;
                result.StatusMessage = "Admin adjust successful.";
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.AdminAdjust(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error on admin adjust.";
            }

            return Json(result);
        }

        private DateTime GetNewRenewDate(DurationType durationType, int duration, DateTime renewDate)
        {
            switch (durationType)
            {
                case DurationType.Minute:	//Minute
                    renewDate = renewDate.AddMinutes(duration);
                    break;
                case DurationType.Hour:	//Hour
                    renewDate = renewDate.AddHours(duration);
                    break;
                case DurationType.Day:	//Day
                    renewDate = renewDate.AddDays(duration);
                    break;
                case DurationType.Week:	//Weeks
                    renewDate = renewDate.AddDays(7 * duration);
                    break;
                case DurationType.Month:	//Month
                    renewDate = renewDate.AddMonths(duration);
                    break;
                case DurationType.Year: //Years
                    renewDate = renewDate.AddYears(duration);
                    break;
                default:
                    break;
            }

            return renewDate;
        }

        public JsonResult GetPassword(int siteID, int memberID)
        {
            MemberGetResult result = new MemberGetResult();
            try
            {
                //check permission
                base.CheckActionPermission(Enums.Operations.PasswordView, true);

                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    //get password
                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    string errorMessage;
                    result.Password = memberManager.GetPassword(out errorMessage);

                    if (errorMessage != string.Empty)
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = errorMessage;
                    }
                    else
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "Password retrieved.";
                    }
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.GetPassword(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error getting password.";
            }

            return Json(result);

        }

        public JsonResult UpdatePassword(int siteID, int memberID)
        {
            MemberGetResult result = new MemberGetResult();
            try
            {
                //check permission
                base.CheckActionPermission(Enums.Operations.PasswordEdit, true);
                //throw new Exception("Testing failure");

                string newPassword = Request["NewPassword"].Trim();
                if (siteID > 0 && memberID > 0 && !String.IsNullOrEmpty(newPassword))
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    //update password
                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    bool status = memberManager.UpdatePassword(newPassword);
                    if (status)
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "Password Updated.";
                    }
                    else
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = "Error Updating Password.";
                    }
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdatePassword(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating password.";
            }

            return Json(result);

        }

        public JsonResult UpdateSubscriptionLastInitialPurchaseDate(int siteID, int memberID, DateTime newDate)
        {
            MemberGetResult result = new MemberGetResult();
            try
            {
                //check permission
                base.CheckActionPermission(Enums.Operations.BuySubscription, true);

                if (siteID > 0 && memberID > 0 && newDate != DateTime.MinValue)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    //update password
                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    memberManager.UpdateSubscriptionLastInitialPurchaseDate(newDate);
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Subscription Last Initial Purchase Date Updated.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateSubscriptionLastInitialPurchaseDate(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating Last Initial Purchase Date.";
            }

            return Json(result);

        }

        public JsonResult SendPassword(int siteID, int memberID)
        {
            MemberGetResult result = new MemberGetResult();
            try
            {
                //check permission
                base.CheckActionPermission(Enums.Operations.PasswordSend, true);

                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    //send password
                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    memberManager.SendPassword();
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Password has been sent.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.SendPassword(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error sending password.";
            }

            return Json(result);

        }

        public JsonResult GenerateStealthLoginLink(int siteID, int memberID, int adminMemberID)
        {
            var result = new StealthLoginResult();
            try
            {
                //check permission
                base.CheckActionPermission(Enums.Operations.StealthLogin, true);

                if (siteID > 0 && memberID > 0)
                {
                    //send password
                    var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    var stealthLoginLink = memberManager.GenerateStealthLoginLink(adminMemberID);
                    result.StealthLoginLink = stealthLoginLink;
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Stealth login link generated.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.GenerateStealthLoginLink(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error generating a stealth login link.";
            }

            return Json(result);

        }

        public JsonResult AddAdminNote(int siteID, int memberID, string note)
        {
            MemberGetResult result = new MemberGetResult();

            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    memberManager.AddAdminNote(note);
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Changes have been saved.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.AddAdminNote(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error adding admin note.";
            }

            return Json(result);
        }

        public JsonResult ClearColorCodeResults(int siteID, int memberID)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    memberManager.ClearColorCodeResults();
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Color Code results have been reset.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.ResetColorCodeResults(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error resetting Color Code results.";
            }

            return Json(result);
        }

        public JsonResult GiveLifetimeMembership(int siteID, int memberID)
        {
            base.CheckActionPermission(Enums.Operations.LifetimeMembershipUpdate, true);

            MemberUpdateResult result = new MemberUpdateResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    var errorMessage = string.Empty;
                    var success = memberManager.GiveLifetimeMembership(out errorMessage);

                    if (success)
                    {
                        result.Status = (int) AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "Lifetime membership has been issued.";
                    }
                    else
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = errorMessage;
                    }
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.GiveLifetimeMembership(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error giving lifetime membership to this member.";
            }

            return Json(result);
        }

        private int AddMaskValue(int originalMaskValues, int maskBit, bool maskBitValue)
        {
            if (maskBitValue)
            {
                return originalMaskValues | maskBit;
            }
            else
            {
                return originalMaskValues & ~maskBit;
            }
        }

        public JsonResult UpdatePremiumServiceSettings(int siteID, int memberID, bool highlightEnabled, 
            bool spotlightEnabled, int spotlightGender, int spotlightSeekingGender, int spotlightMinAge, int spotlightMaxAge,
            int spotlightDistance, int spotlightRegionID)
        {
             MemberResultBase result = new MemberResultBase();

             try
             {
                 if (siteID > 0 && memberID > 0)
                 {
                     result.MemberID = memberID;
                     result.SiteID = siteID;
                     Brand brand = g.GetBrand((int)siteID);
                     MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);

                     memberManager.UpdatePremiumServiceSettings(highlightEnabled, spotlightEnabled, spotlightGender, 
                         spotlightSeekingGender, spotlightMinAge, spotlightMaxAge, spotlightDistance, spotlightRegionID);
                     result.Status = (int)AdminTool.Models.Enums.Status.Success;
                     result.StatusMessage = "Changes have been saved.";
                 }
                 else
                 {
                     throw new Exception("Missing parameters");
                 }
             }
             catch (Exception ex)
             {
                 _logger.Error("Error in MemberAPIController.UpdatePremiumServiceSettings(): ", ex);
                 result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                 result.StatusMessage = "Error updating information.";
             }

             return Json(result);
        }

        public JsonResult ResetJMeter(int siteID, int memberID)
        {
            var result = new MemberResultBase();

            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;
                    Brand brand = g.GetBrand((int)siteID);
                    var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);

                    if (memberManager.ResetJMeter())
                    {
                        result.Status = (int) AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "J-Meter data has been reset.";
                    }
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = "Error resetting J-Meter data.";
                    }
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.ResetJMeter(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error resetting J-Meter data.";
            }

            return Json(result);

        }

        public JsonResult SyncWithZoozamen(int siteID, int memberID)
        {
            var result = new MemberResultBase();

            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;
                    Brand brand = g.GetBrand((int)siteID);
                    var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);

                    memberManager.SyncWithZoozamen();
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Sync with Zoozamen complete.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.SyncWithZoozamen(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error sync'ing with Zoozamen.";
            }

            return Json(result);
        }

        public JsonResult UpdateILAuthentication(int siteID, int memberID, bool disableILAuth, DateTime iLAuthExpDate)
        {
            var result = new MemberILAuthResult();

            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;
                    Brand brand = g.GetBrand((int)siteID);
                    var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    string ilAuthStatus; 
                    result.Status = (int)memberManager.UpdateILAuthentication(disableILAuth, iLAuthExpDate, out ilAuthStatus);
                    result.StatusMessage = result.Status == (int)AdminTool.Models.Enums.Status.Success ? "IL authentication status updated." : "IL authentication status update failed.";
                    result.ILAuthStatus = ilAuthStatus;
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateILAuthentication(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating IL authentication.";
            }

            return Json(result);
        }

        public JsonResult UpdateOffSiteNotifications(int siteID, int memberID, int matchmailFrequency, int clickmailFrequency, bool clickAlert,
            bool emailAlert, bool ecardAlert, bool newsletterAlert, bool traveleventsAlert, bool qualifiedpartnersAlert, bool hotlistAlert, bool profileviewedAlert, bool essayRequestAlert)
        {
            MemberResultBase result = new MemberResultBase();

            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    // compile the mask values from the bool's passed in
                    int emailAlertMasks = 0;
                    int newsletterMasks = 0;

                    emailAlertMasks = AddMaskValue(emailAlertMasks, (int)EmailAlertMask.GotClickAlert, clickAlert);
                    emailAlertMasks = AddMaskValue(emailAlertMasks, (int)EmailAlertMask.NewEmailAlert, emailAlert);
                    emailAlertMasks = AddMaskValue(emailAlertMasks, (int)EmailAlertMask.ECardAlert, ecardAlert);
                    emailAlertMasks = AddMaskValue(emailAlertMasks, (int)EmailAlertMask.HotListedAlert, hotlistAlert);
                    emailAlertMasks = AddMaskValue(emailAlertMasks, (int)EmailAlertMask.ProfileViewedAlertOptOut, !profileviewedAlert);
                    emailAlertMasks = AddMaskValue(emailAlertMasks, (int)EmailAlertMask.NoEssayRequestEmail, essayRequestAlert);

                    newsletterMasks = AddMaskValue(newsletterMasks, (int)NewsEventOfferMask.News, newsletterAlert);
                    newsletterMasks = AddMaskValue(newsletterMasks, (int)NewsEventOfferMask.Events, traveleventsAlert);
                    newsletterMasks = AddMaskValue(newsletterMasks, (int)NewsEventOfferMask.Offers, qualifiedpartnersAlert);

                    Brand brand = g.GetBrand((int)siteID);
                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);

                    memberManager.UpdateOffSiteNotifications(matchmailFrequency, clickmailFrequency, emailAlertMasks, newsletterMasks);
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Changes have been saved.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateOffSiteNotifications(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating information.";
            }

            return Json(result);
        }

        public JsonResult UpdateFraudAbuseInfo(int siteID, int memberID, bool subscriptionFraud, bool chatBanned, bool emailBanned, bool unfraudEmails)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    Brand brand = g.GetBrand((int)siteID);
                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);

                    //first get original values
                    bool existingSubscriptionFraud = !(Conversion.CBool(memberManager.Member.GetAttributeInt(brand, "PassedFraudCheckSite", 1)));
                    bool existingChatBanned = Conversion.CBool(memberManager.Member.GetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, Constants.NULL_INT, "ChatBanned", 0));

                    memberManager.UpdateFraudInfo(subscriptionFraud, chatBanned, emailBanned, unfraudEmails);
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;

                    MingleClearCacheHelper mingleClearCacheHelper = new MingleClearCacheHelper(memberID);
                    string clearCacheResult = mingleClearCacheHelper.GetClearCacheResponse();

                    StringBuilder statusMessage = new StringBuilder();

                    if ((existingSubscriptionFraud != subscriptionFraud) && subscriptionFraud)
                    {
                        statusMessage.Append("Marked as fraud - IM/Mail Permissions have been blocked. ");
                    }
                    if ((existingSubscriptionFraud != subscriptionFraud) && !subscriptionFraud)
                    {
                        statusMessage.Append("Profile passed fraud check - IM/Mail Permissions are allowed. ");
                    }
                    if ((existingChatBanned != chatBanned) && !chatBanned)
                    {
                        statusMessage.Append("Member is now allowed to use the Chat Rooms.");
                    }
                    if ((existingChatBanned != chatBanned) && chatBanned)
                    {
                        statusMessage.Append("Member has been banned from Chat Rooms.");
                    }
                    if (statusMessage.Length == 0)
                    {
                        statusMessage.Append("Changes have been saved.");
                    }

                    statusMessage.AppendLine("clearCacheResult: " + clearCacheResult);

                    result.StatusMessage = statusMessage.ToString();
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateFraudAbuseInfo(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating information.";
            }

            return Json(result);
        }

        public JsonResult UpdateOtherActions(int siteID, int memberID, bool colorAnalysisPurchase)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    memberManager.UpdateOtherActions(colorAnalysisPurchase);
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Changes have been saved.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateOtherActions(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating information.";
            }

            return Json(result);
        }

        public JsonResult UpdateRamahAction(int siteID, int memberID, bool ramahDate)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    memberManager.UpdateRamahAction(ramahDate);
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Changes have been saved.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateOtherActions(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating information.";
            }

            return Json(result);
        }

        public JsonResult EndAutoRenewal(int siteID, int memberID)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    bool succecssfulUpdate = memberManager.EndAutoRenewal();
                    if (succecssfulUpdate)
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "Auto Renewal has been turned off.";
                    }
                    else
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = "Error turning off auto renewal.";
                    }
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.EndAutoRenewal(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error turning off auto renewal.";
            }

            return Json(result);
        }

        public JsonResult EndAutoRenewalWithReason(int siteID, int memberID, int terminationReasonID)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    bool succecssfulUpdate = memberManager.EndAutoRenewalWithReason(terminationReasonID);
                    if (succecssfulUpdate)
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "Auto Renewal has been turned off.";
                    }
                    else
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = "Error turning off auto renewal.";
                    }
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.EndAutoRenewal(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error turning off auto renewal.";
            }

            return Json(result);
        }

        public JsonResult ReopenAutoRenewal(int siteID, int memberID, bool reopenMainSubscription, bool reopenHighlightALC, bool reopenSpotlightALC, bool reopenAllAccessALC, bool reopenReadReceiptALC)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    bool succecssfulUpdate = memberManager.ReopenAutoRenewal(reopenMainSubscription, reopenHighlightALC, reopenSpotlightALC, reopenAllAccessALC, reopenReadReceiptALC);
                    if (succecssfulUpdate)
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "Auto Renewal has been turned on.";
                    }
                    else
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = "Error turning auto renewal on.";
                    }

                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.ReopenAutoRenewal(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error turning auto renewal on.";
            }

            return Json(result);
        }

        //ReopenAutoRenewal(bool reopenMainSubscription, bool reopenHighlightALC, bool reopenSpotlightALC)

        public JsonResult UpdateProfileInfo(int siteID, int memberID, string firstName, string lastName, DateTime birthdate,
                                        Enums.Gender gender, Enums.SeekingGender seekingGender)
        {
            MemberUpdateResult result = new MemberUpdateResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    memberManager.UpdateProfileInfo(firstName, lastName, birthdate, gender, seekingGender);
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Profile has been saved.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateProfileInfo(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating profile.";
            }

            return Json(result);
        }

        public JsonResult UpdateGlobalProfileInfo(int siteID, int memberID, bool adminSuspended, bool badEmail, bool emailVerified, bool onDNE, AdminActionReasonID adminActionReasonID)
        {
            bool alreadySuspended = false;
            MemberUpdateResult result = new MemberUpdateResult();
            result.Status = (int)AdminTool.Models.Enums.Status.Success;

            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    Brand brand = g.GetBrand(siteID);
                    Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.IngoreSACache);
                    int globalStatusMask = member.GetAttributeInt(brand, "GlobalStatusMask");
                    alreadySuspended = (globalStatusMask & WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND) == WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND;

                    if (!adminSuspended)
                    {
                        //reasons should only be used for an admin suspend
                        adminActionReasonID = AdminActionReasonID.None;
                    }
                    else
                    {
                        if (!alreadySuspended && adminSuspended && !GeneralHelper.IsSuspendReasonAllowed(adminActionReasonID))
                        {
                            result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                            result.StatusMessage = "Invalid suspension reason.";
                        }
                    }

                    if (result.Status == (int)AdminTool.Models.Enums.Status.Success)
                    {
                        MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);

                        memberManager.UpdateGlobalStatusMask(adminSuspended, badEmail, emailVerified, adminActionReasonID);
                        memberManager.UpdateDNEStatus(onDNE);

                        if (adminSuspended && adminActionReasonID == AdminActionReasonID.FraudScammer)
                        {
                            int currentEmailBanned = member.GetAttributeInt(brand, "EmailBan");
                            if (!Convert.ToBoolean(currentEmailBanned))
                            {
                                memberManager.HideEmails(brand);
                            }
                        }

                        //Notify iovation on member suspend and unsuspend
                        if ((adminSuspended && adminActionReasonID != AdminActionReasonID.None) || (alreadySuspended && !adminSuspended))
                            MemberHelper.NotifyIovation(member, adminSuspended, adminActionReasonID);
                        
                        result.StatusMessage = "Changes have been saved.";
                        //additional return values to update UI
                        result.ProfileStatus = MemberHelper.GetProfileStatus(memberManager.Member, g.GetBrand(siteID)).ToString();
                        result.SuspendReason = ""; //TODO - Talk to Mishkoff
                    }
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.UpdateGlobalProfileInfo(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error updating information.";
            }

            return Json(result);
        }

        public ActionResult GetTransactionHistory(int siteID, int memberID)
        {
            object result = null;
            TransactionHistory history = null;

            try
            {
                MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                bool hasAccess = base.CheckActionPermission(Enums.Operations.BillingInformationView, false);
                history = memberManager.GetTransactionHistory();

                if (Convert.ToBoolean(RuntimeSettings.GetSetting("USE_ORDER_HISTORY_JSON_SERVICE")))
                {
                    var client = new RestClient(ConfigurationManager.AppSettings["OrderHistoryRestAuthority"]);
                    var requestString = AdminTool.Models.SparkAPI.RestUrls.GETMEMBERORDERHISTORYBYMEMBERID + "?MemberID=" + memberID + "&CallingSystemID=" + siteID + "&PageSize=5000&PageNumber=1";
                    var request = new RestRequest(requestString, Method.GET);
                    request.AddHeader("Accept", "version=V2.1");
                    var jsonResponce = client.Execute(request).Content;
                    var fixedResponce = jsonResponce.Replace("-79228162514264337593543950335", "0.0");
                    var orderHistorys = JsonConvert.DeserializeObject<List<OrderHistory>>(fixedResponce);
                    List<DiscountTransaction> dts = new List<DiscountTransaction>();
                    DiscountTransaction dt = null;
                    foreach (OrderHistory oh in orderHistorys)
                    {
                        dt = AccountHistory.GetDiscountInfo(oh);
                        if (dt != null)
                            dts.Add(dt);
                    }

                    //lets update the discount code and discount amounts for all the AccountHistory
                    if (dts.Count > 0)
                    {
                        foreach (AccountHistory ah in history.AllAccountHistory)
                        {
                            var dtResult = dts.Where(n => n.OrderId == ah.OrderID).FirstOrDefault();
                            if (dtResult != null)
                            {
                                if(dtResult.DiscountType == 2) // discount type is a percentage 
                                    ah._discountAmountForDisplay = (dtResult.DiscountAmount * 100).ToString() + "%";
                                else
                                    ah._discountAmountForDisplay =  "$" + dtResult.DiscountAmount.ToString();
                                
                                
                                ah.DiscountCode = dtResult.DiscountCode;
                                ah.HasDiscount = true;
                            }
                        }
                    }

                }

                 

                var billingInfo = (hasAccess) ? memberManager.GetDefaultBillingInfo() : null;

                result = new
                {
                    AllAccountHistory = history.AllAccountHistory.Select(x => new
                    {
                        x.OrderID,
                        url = x.BillingInfoURL,
                        actHisType = x.AccountHistoryTypeString,
                        ostatus = x.OrderStatusString,
                        insDate = x.InsertDateInPSTHtml,
                        admin = x.AdminDisplayHtml,
                        transType = x.TransactionTypeDisplay,
                        amt = x.TotalAmountHtml,
                        x.IsGiftPurchase,
                        pgCode = x.PurchasedGiftCode,
                        pgType = x.PurchasedGiftType,
                        pgBeginDate = x.PurchasedGiftBeginValidDate,
                        pgExpireDate = x.PurchasedGiftExpirationDate,
                        pgIsValid = x.PurchasedGiftIsValid,
                        pgrCallingSystem = x.PurchasedGiftRedemptionCallingSystemId,
                        pgrCustomerId = x.PurchasedGiftRedemptionCustomerId,
                        pgr = x.PurchasedGiftRedemption,
                        pgStatus = x.PurchasedGiftStatus,
                        x.MinValue,
                        x.IsGiftRedemption,
                        rgCode = x.RedeemedGiftCode,
                        rgType = x.RedeemedGiftType,
                        rgCallingSystem = x.RedeemedGiftCallingSystemId,
                        rgCustomerId = x.RedeemedGiftCustomerId,
                        x.IsNotAdminAdjust,
                        x.RenewalRate,
                        duration = x.DurationHtml,
                        x.HasPrimaryPlan,
                        rduration = x.RenewalDurationHtml,
                        x.IsAdminAdjust,
                        includes = x.IncludedAlaCarteItemsDisplay,
                        x.PrimaryPackageIDHTML,
                        x.PromoIDHtml,
                        x.PromoDescription,
                        orderstatus = x.OrderStatusDisplay,
                        paymentType = x.PaymentTypeDisplay,
                        account = x.LastFourAccountNumber,
                        x.CreditQueueURL,
                        internalStatusMessage = (x.OrderStatus == OrderStatus.Failed ? x.InternalStatusMessage : x.OrderStatusDisplay),
                        authorizationPaymentID = (x.AuthorizationPaymentID > 0 ? Convert.ToString(x.AuthorizationPaymentID) : String.Empty),
                        billingPaymentID = (x.BillingPaymentID > 0 ? Convert.ToString(x.BillingPaymentID) : String.Empty),
                        x.DiscountCode, 
                        x.DiscountAmountForDisplay,
                        x.HasDiscount,
                        fraudDetailedResponse = x.FraudDetailedResponse
                    }),
                    billing = billingInfo
                };
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.GetTransactionHistory(): ", ex);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("~/Views/Member/Controls/TransactionHistory.ascx", history);
        }

        public ActionResult GetBillingInfo(int siteID, int memberID)
        {
            MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
            BillingInfo billingInfo = memberManager.GetDefaultBillingInfo();
            return PartialView("~/Views/Member/Controls/BillingInfo.ascx", billingInfo);
        }

        public ActionResult ArchivePaymentProfile(int siteID, int memberID, string paymentProfileId)
        {
            MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
            BillingInfo billingInfo = memberManager.ArchivePaymentProfile(paymentProfileId);
            return PartialView("~/Views/Member/Controls/BillingInfo.ascx", billingInfo);
        }

        public ActionResult BillingInfoForGiftRedemption(string orderID, int siteID, int memberID)
        {
            //orderid == giftcode
            BillingInfo billingInfoModel = new BillingInfoHelper().GetBillingInfoFromGift(orderID, siteID, memberID);
            billingInfoModel.MemberSiteInfo = MemberHelper.GetMemberSiteInfo(MemberHelper.GetMember(memberID, MemberLoadFlags.None), siteID);
            return PartialView("~/Views/Member/Controls/BillingInfo.ascx", billingInfoModel);
        }

        public ActionResult BillingInfoForFreeTrial(string userPaymentGUID, int siteID, int memberID)
        {
            BillingInfo billingInfoModel = new BillingInfoHelper().GetBillingInfoFromFreeTrial(userPaymentGUID, siteID, memberID);
            billingInfoModel.MemberSiteInfo = MemberHelper.GetMemberSiteInfo(MemberHelper.GetMember(memberID, MemberLoadFlags.None), siteID);
            return PartialView("~/Views/Member/Controls/BillingInfo.ascx", billingInfoModel);
        }

        public ActionResult BillingInfo(int orderID, int siteID, int memberID)
        {
            BillingInfo billingInfoModel = new BillingInfoHelper().GetBillingInfoFromOrder(orderID, siteID, memberID);
            billingInfoModel.MemberSiteInfo = MemberHelper.GetMemberSiteInfo(MemberHelper.GetMember(memberID, MemberLoadFlags.None), siteID);
            return PartialView("~/Views/Member/Controls/BillingInfo.ascx", billingInfoModel);

            //billingInfoModel.PaymentType = Purchase.PaymentType.ElectronicFundsTransfer;
        }

        public JsonResult SendVerificationEmail(int siteID,int memberID)
        {
            MemberGetResult result = new MemberGetResult();
            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    result.MemberID = memberID;
                    result.SiteID = siteID;

                    //send verification email
                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    bool status = memberManager.SendVerificationEmail();
                    result.Status = (status) ? (int)AdminTool.Models.Enums.Status.Success : (int)AdminTool.Models.Enums.Status.Failed;
                    result.StatusMessage = (status) ? "Verification Email has been sent." : "Error sending Verification Email";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in MemberAPIController.SendVerificationEmail(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error sending Verification Email.";
            }

            return Json(result);
        }

    }
}
