﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using Spark.CommonLibrary.Logging;
using System.Configuration;
using AdminTool.Models.JSONResult;
using Matchnet.Member.ServiceAdapters;

namespace AdminTool.Controllers
{
    public class APIController : Controller
    {
        public Logger _logger;

        public ContextGlobal g
        {
            get { return HttpContext.Items["g"] as ContextGlobal; }
        }


        public APIController()
        {
            _logger = new Logger(this.GetType());
        }
        
        [HttpPost]
        public JsonResult AddAdminNote(int memberID, int siteID, string adminName, string note)
        {
            APIResult result = new APIResult();

            try
            {
                InitializeGlobalContext(adminName);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in APIController.AddAdminNote():", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.APIAdminMappingFailed;
                result.StatusMessage = "Error mapping admin.";
                return Json(result);
            }

            try
            {
                if (siteID > 0 && memberID > 0)
                {
                    MemberManager memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                    memberManager.AddAdminNote(note);
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                    result.StatusMessage = "Changes have been saved.";
                }
                else
                {
                    throw new Exception("Missing parameters");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in APIController.AddAdminNote(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error adding admin note.";
            }

            return Json(result);
        }

        private void InitializeGlobalContext(string adminName)
        {
            //initialize context global
            if (!HttpContext.Items.Contains("g"))
            {
                HttpContext.Items["g"] = new ContextGlobal(GetFullAdminName(adminName));
            }

            //ensure admin's domain/bedrock mapping exists
            if (ConfigurationManager.AppSettings["isADEnabled"] == "true" && g.AdminID <= 0)
            {
                ViewData[WebConstants.ERROR_KEY] = "Admin " + g.AdminDomainAccount + " is missing domain/bedrock mapping.";
                throw new Exception("Error in OnActionExecuting(). Admin is missing domain/bedrock mapping.");
            }
        }

        private string GetFullAdminName(string adminName)
        {
            string prepend = "Matchnet\\";
            string env = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENVIRONMENT_TYPE");
            if (env != null)
            {
                if (env.Contains("stgv"))
                {
                    prepend = "Sparkstage\\";
                }
            }
            return prepend + adminName;
        }

    }
}
