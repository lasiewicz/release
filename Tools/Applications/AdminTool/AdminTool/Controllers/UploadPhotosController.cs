﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Spark.CommonLibrary;
using AdminTool.Models;

namespace AdminTool.Controllers
{
    public class UploadPhotosController : BaseController
    {
        //
        // GET: /UploadPhotos/

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UploadMemberPhotos(int memberID, SiteIDs siteID)
        {
            PhotoUploadManager manager = new PhotoUploadManager(memberID, siteID);
            var memeberPhotos = manager.GetMemberPhotos();
            return View(memeberPhotos);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UploadMemberPhotos(int memberID, SiteIDs siteID, FormCollection formData)
        {
            PhotoUploadManager manager = new PhotoUploadManager(memberID, siteID);
            string validationMessage = manager.CheckFilesValidation();
            if (validationMessage == string.Empty)
            {
                manager.UpdateChanges(formData);

            }
            var memeberPhotos = manager.GetMemberPhotos(validationMessage, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);

            return View(memeberPhotos);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public RedirectToRouteResult DeleteMemberPhoto(int memberID, SiteIDs siteID, int memberPhotoID, byte listOrder)
        {
            PhotoUploadManager manager = new PhotoUploadManager(memberID, siteID);

            manager.DeleteMemberPhoto(memberPhotoID, listOrder);

            return RedirectToAction("UploadMemberPhotos", new RouteValueDictionary()
                                                       {
                                                           {"memberID", memberID},
                                                           {"siteID", siteID}
                                                       });
        }
    }
}
