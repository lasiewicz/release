﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.ModelViews.Email;

namespace AdminTool.Controllers
{
    public class EmailController : BaseController
    {
        //
        // GET: /Email/List/103/1234234/1

        public ActionResult List(int siteID, int memberID, int folderID, int rowIndex)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.ViewEmailActivity, true);

            EmailList emailList = new EmailList();
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                EmailManager emailManager = new EmailManager(siteID, memberID);
                emailList = emailManager.GetEmails(rowIndex, folderID);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in EmailController.List(" + siteID.ToString() + ", " + memberID.ToString() + ").", ex);
            }

            return View("EmailList", emailList);
        }

    }
}
