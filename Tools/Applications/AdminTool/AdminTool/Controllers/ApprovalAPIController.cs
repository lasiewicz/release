﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.Member;
using Spark.CommonLibrary;
using Matchnet.Member.ServiceAdapters;
using AdminTool.Models;
using AdminTool.Models.ModelViews;
using AdminTool.Models.JSONResult;
using AdminTool.Models.ModelViews.Approval;
using System.Drawing;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace AdminTool.Controllers
{
    public class ApprovalAPIController : BaseController
    {
        private const Int16 APPROVE_STATUS_IGNORE = 1;
        private const Int16 APPROVE_STATUS_INVALID = 8;

        public JsonResult ApprovePhoto(int disposition, 
            int tx, int ty, int tw, int th,
            int cx, int cy, int cw, int ch,
            int captionDelete, int photoPrevApproved, int memberPhotoID, int fileID, int rot,
            int memberID, int adminMemberID, int communityID, int siteID, bool deleted, bool isApprovedForMain)
        {
            ApprovalResult result = new ApprovalResult();

            try
            {
                System.Diagnostics.Trace.WriteLine("Start ApprovalAPIController.ApprovePhoto()");
                bool success = true; ;
                if (!deleted)
                {
                    ApprovalManager manager = new ApprovalManager();
                    success = manager.CompletePhotoApproval(disposition,
                        tx, ty, tw, th,
                        cx, cy, cw, ch,
                        captionDelete, photoPrevApproved, memberPhotoID, fileID, rot,
                        memberID, adminMemberID, communityID, siteID, isApprovedForMain);
                }
                if (success)
                {
                    result.Status = (int)AdminTool.Models.Enums.Status.Success;
                }
                else
                {
                    result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                }
            }
            catch (Exception ex)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                _logger.Error("Error in ApprovalAPIController.ApprovePhoto().", ex);
                System.Diagnostics.Trace.WriteLine("Error in ApprovalAPIController.ApprovePhoto(): " + ex.Message);
            }

            return Json(result); 
        }

        public JsonResult DeletePhotoQueueItem(int memberID, int communityID, int memberPhotoID)
        {
            ApprovalResult result = new ApprovalResult();
            try
            {
                new ApprovalManager().DeletePhotoQueueItem(memberID, communityID, memberPhotoID);
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
            }
            catch (Exception ex)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                _logger.Error("Error in ApprovalAPIController.DeletePhotoQueueItem().", ex);
            }

            return Json(result); 
        }
    }
}
