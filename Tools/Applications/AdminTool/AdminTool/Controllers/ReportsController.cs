﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.ModelViews;
using AdminTool.Models.ModelViews.Approval;
using AdminTool.Models.ModelViews.Review;
using Matchnet;
using Matchnet.ApproveQueue.ServiceAdapters;

namespace AdminTool.Controllers
{
    public class ReportsController : BaseController
    {
        //
        // GET: /Reports/

        public ActionResult ViewBulkMailBatchReports()
        {
            return View(new SharedTopNav());
        }

        public ActionResult ViewApprovalCounts()
        {
            var textApprovalReviewCounts = DBApproveQueueSA.Instance.GetTextApprovalReviewCounts(false);
            var reviewCounts = new TextApprovalReviewCountsModelView
            {
                SharedTopNav = new SharedTopNav(),
                TotalFTASamples = textApprovalReviewCounts.TotalItems,
                LatestFTASample = textApprovalReviewCounts.LatestRecordDate,
                OldestFTASample = textApprovalReviewCounts.OldRecordDate
            };
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            return View(reviewCounts);
        }

        public ActionResult ViewMemberProfileDuplicateCounts()
        {
            CheckActionPermission(Enums.Operations.CRXDuplicatesSupervisor, true);

            var dupCounts = new MemberProfileDuplicateCountsModelView { SharedTopNav = new SharedTopNav() };
            return View(dupCounts);
        }

        public ActionResult ViewQueueCounts()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            return View(new SharedTopNav());
        }

        public ActionResult ViewPhotosQueue()
        {
            CheckActionPermission(Enums.Operations.PhotoApprovalQueue, true);
            return View(new PhotoApprovalModelView(null, new ApprovalLeft(), new SharedTopNav()));
        }

        public ActionResult ViewTextReviewCounts()
        {
            var modelView = new TextApprovalReviewReportModelView { Communities = ReviewManager.GetCommunities(0) };
            return View(modelView);
        }

        [HttpPost]
        public ActionResult ViewTextReviewCounts(TextApprovalReviewReportModelView model)
        {
            if (ModelState.IsValid)
            {
                var manager = new ReviewManager();
                var modelView = manager.GetTextApprovalReviewReportCounts(model);
                return View("ViewTextReviewCounts", modelView);
            }
            return View(new TextApprovalReviewReportModelView { Communities = ReviewManager.GetCommunities(model.CommunityId) });
        }

        public ActionResult ViewSodbQueueCounts()
        {
            base.CheckActionPermission(Enums.Operations.SODBAccess, true);

            var manager = new ReviewManager();
            var modelView = manager.GetSodbQueueCounts();
            return View(modelView);
        }

        public ActionResult ViewSodbReviewStats()
        {
            base.CheckActionPermission(Enums.Operations.SODBAccess, true);

            var modelView = new SodbReviewStatsModelView();
            return View(modelView);
        }

        [HttpPost]
        public ActionResult ViewSodbReviewStats(SodbReviewStatsModelView model)
        {
            if (ModelState.IsValid)
            {
                var reviewManager = new ReviewManager();
                var modelView = reviewManager.GetSodbReviewStats(model);
                return View("ViewSodbReviewStats", modelView);
            }
            return View(new SodbReviewStatsModelView());
        }

    }
}
