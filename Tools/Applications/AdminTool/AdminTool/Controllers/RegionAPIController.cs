﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.JSONResult;
using AdminTool.Models;
using Matchnet.Lib;
using Matchnet.Content.ServiceAdapters;

namespace AdminTool.Controllers
{
    public class RegionAPIController : BaseController
    {
        //
        // GET: /Region/

        public JsonResult GetCountries(int siteID)
        {
            RegionResult result = new RegionResult();

            RegionManager manager = new RegionManager(siteID);
            result.SelectListItems = manager.GetCountries();

            result.Status = (int)AdminTool.Models.Enums.Status.Success;
            result.StatusMessage = "Countries retrieved";

            return Json(result);
        }

        public JsonResult GetStates(int siteID, int countryRegionID, string caller)
        {
            RegionResult result = new RegionResult();

            RegionManager manager = new RegionManager(siteID);
            result.NextToDisplay = manager.GetNextDataToCapture(countryRegionID, caller);

            if (result.NextToDisplay == RegionDisplay.State)
            {
                result.SelectListItems = manager.GetStates(countryRegionID);
            }

            result.Status = (int)AdminTool.Models.Enums.Status.Success;
            result.StatusMessage = "States retrieved";

            return Json(result);
        }

        public JsonResult GetChildRegions(int siteId, int parentRegionId)
        {
            RegionResult result = new RegionResult();

            var manager = new RegionManager(siteId);

            result.SelectListItems = manager.GetChildRegions(parentRegionId);
            result.Status = (int)AdminTool.Models.Enums.Status.Success;
            result.StatusMessage = "Child regions retrieved";

            return Json(result);
        }

        public JsonResult GetRegionIDByZip(int siteID, int countryRegionID, string zipCode, bool ignoreMultiCity)
        {
            RegionResult result = new RegionResult();

            RegionManager manager = new RegionManager(siteID);
            int regionID = -1;
            result.SelectListItems = manager.GetRegionIDByZip(countryRegionID, zipCode, out regionID, ignoreMultiCity);
            result.RegionID = regionID;

            if (regionID < 0 && result.SelectListItems.Count == 0)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Zipcode not found";
            }
            else
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
                result.StatusMessage = "Zipcode lookup complete";
            }

            return Json(result);
        }

        public JsonResult GetRegionID(int siteID, int countryRegionID, int stateRegionID, string cityName)
        {
            RegionResult result = new RegionResult();

            RegionManager manager = new RegionManager(siteID);
            result.RegionID = manager.GetRegionID(countryRegionID, stateRegionID, cityName);

            if (result.RegionID < 0)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "RegionID not found for that city";
            }
            else
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
                result.StatusMessage = "RegionID found";
            }

            return Json(result);
        }

        public JsonResult GetCityList(int siteID, int countryRegionID, int stateRegionID, string cityPrefix)
        {
            RegionResult result = new RegionResult();

            RegionManager manager = new RegionManager(siteID);
            result.SelectListItems = manager.GetCityList(countryRegionID, stateRegionID, cityPrefix);

            if (result.SelectListItems.Count() > 0)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
                result.StatusMessage = "Cities found";
            }
            else
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "No city beginning with that letter";
            }

            return Json(result);
        }

        public JsonResult GetCityPrefix(int siteID, int countryRegionID, int stateRegionID)
        {
            RegionResult result = new RegionResult();

            RegionManager manager = new RegionManager(siteID);
            result.SelectListItems = manager.GetCityPrefix(countryRegionID, stateRegionID);

            if (result.SelectListItems.Count() > 0)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
                result.StatusMessage = "Cities found";
            }
            else
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "No cities found";
            }

            return Json(result);
        }

        public JsonResult GetRegionBindInfo(int siteID, int regionID)
        {
            
            RegionManager manager = new RegionManager(siteID);
            RegionBindInfo result = manager.GetRegionBindInfo(regionID);

            if (result.RegionID > -1)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
                result.StatusMessage = "Binding info retrieved";
            }
            else
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Failed to get binding info";
            }

            return Json(result);
        }

        public JsonResult GetRegionString(int siteID, int regionID)
        {
            RegionManager manager = new RegionManager(siteID);
            RegionString result = manager.GetRegionString(regionID);

            if (result.RegionDisplayString == null || result.RegionDisplayString == string.Empty)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Invalid regionID";
            }
            else
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
                result.StatusMessage = "Success";
            }

            return Json(result);
        }

        public JsonResult ValidateAreaCodes(int siteID, string areaCodes, int countryRegionID)
        {
            RegionManager manager = new RegionManager(siteID);
            ResultBase result = manager.ValidateAreaCodes(areaCodes, countryRegionID);

            if (result.Status == (int)AdminTool.Models.Enums.Status.Success)
            {
                result.StatusMessage = "Success";
            }

            return Json(result);
        }
    }
}
