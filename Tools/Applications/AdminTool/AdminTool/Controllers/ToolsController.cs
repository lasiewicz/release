﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.ModelViews.Tools;
using Spark.CommonLibrary;

namespace AdminTool.Controllers
{
    public class ToolsController : BaseController
    {
        //
        // GET: /Tools/

        public ActionResult ViewBulkMailSimulator(int memberID, int siteID)
        {
            BulkMailSimulatorManager manager = new BulkMailSimulatorManager();
            BulkMailSimulatorModelView modelView = manager.GetSimulatorModelView(memberID, siteID);
            return View(modelView);
        }
    }
}
