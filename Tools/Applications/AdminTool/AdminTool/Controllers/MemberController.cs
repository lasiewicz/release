﻿#region

using System;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.ModelViews;
using AdminTool.Models.ModelViews.Member;
using Matchnet.Member.ServiceAdapters;
using AdminTool.Models.ModelViews.Migration;

#endregion

namespace AdminTool.Controllers
{
    public class MemberController : BaseController
    {
        //
        // GET: /Member/View/101/1000101
        /// <summary>
        ///     Displays member management page
        /// </summary>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public ActionResult View(int siteID, int memberID)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.MemberPageView, true);

            var memberData = new MemberData();
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.IngoreSACache);
                memberData = memberManager.GetMemberData();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.View(" + siteID + ", " + memberID + ").", ex);
            }

            return View("Member", memberData);
        }

        public ActionResult EditSearchPreferences(int siteID, int memberID)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.ProfileInformationSave, true);

            var data = new SearchPreferencesData();

            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                data = memberManager.GetEditSearchPreferences();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.EditSearchPreferences(" + siteID + ", " + memberID + ").", ex);
            }

            return View("EditSearchPreferences", data);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteEditSearchPreferences(FormCollection collection, int siteID, int memberID)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.ProfileInformationSave, true);
            SearchPreferencesData data = null;
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                // Update the attributes here and then reload the page with the new values.
                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                data = memberManager.UpdateMemberSearchPreferences(collection);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.CompleteEditSearchPreferences(" + siteID + ", " + memberID + ").", ex);
            }

            return View("EditSearchPreferences", data);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteEditProfile(FormCollection collection, int siteID, int memberID, int controlType)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.ProfileInformationSave, true);
            EditProfileData data = null;
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                // Update the attributes here and then reload the page with the new values.
                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                data = memberManager.UpdateMemberProfileAttributes(collection, controlType);
                data.Message = "Profile update was successful";
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.CompleteEditProfile(" + siteID + ", " + memberID + ").", ex);
            }

            return View("EditProfile", data);
        }

        public ActionResult EditProfile(int siteID, int memberID, int controlType)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.ProfileInformationSave, true);

            var data = new EditProfileData();
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.IngoreSACache);
                data = memberManager.GetEditProfileData(controlType);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.EditProfile(" + siteID + ", " + memberID + ").", ex);
            }

            return View("EditProfile", data);
        }

        public ActionResult Print(int siteID, int memberID)
        {
            var data = new MemberDataForPrint();
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                data = memberManager.GetMemberDataForPrint();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.Print(" + siteID + ", " + memberID + ").", ex);
            }

            return View("PrintableMemberData", data);
        }

        public ActionResult Print2(int siteID, int memberID)
        {
            var data = new MemberDataForPrint();
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                data = memberManager.GetMemberDataForPrint();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.EditProfile(" + siteID + ", " + memberID + ").", ex);
            }

            return View("PrintableMemberData2", data);
        }

        /// <summary>
        ///     Displays supporting member informatio to support QA
        ///     GET: /Member/QASupport/101/1000101
        /// </summary>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public ActionResult QASupport(int siteID, int memberID)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.MemberPageView, true);

            var qaSupport = new QASupport();
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                qaSupport = memberManager.GetQASupport();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.QASupport(" + siteID + ", " + memberID + ").", ex);
            }

            return View("QASupport", qaSupport);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("QASupport")]
        public ActionResult QASupportPost(int siteID, int memberID)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.MemberPageView, true);

            var qaSupport = new QASupport();

            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
                switch (Request["button"])
                {
                    case "addiaptrandid":
                        //add test iap tranID
                        memberManager.QAOnlyUpdateIAPTranIDAttribute(false);
                        break;
                    case "removeiaptranid":
                        memberManager.QAOnlyUpdateIAPTranIDAttribute(true);
                        break;
                    case "addRamahAlum":
                        memberManager.QAOnlyUpdateRamahAlumAttribute(false);
                        break;
                    case "removeRamahAlum":
                        memberManager.QAOnlyUpdateRamahAlumAttribute(true);
                        break;
                }


                qaSupport = memberManager.GetQASupport();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.QASupport(" + siteID + ", " + memberID + ").", ex);
            }

            return View("QASupport", qaSupport);
        }

        public ActionResult MigrationSupport(int siteID, int memberID)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.MemberPageView, true);

            var migrationSupport = new MigrationSupport();
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None, true);
                migrationSupport = memberManager.GetMigrationSupportModelView();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.MigrationSupport(" + siteID + ", " + memberID + ").", ex);
            }

            return View("MigrationSupport", migrationSupport);
        }

        public ActionResult ListMigrationSupport(int siteID, int memberID)
        {
            base.CheckActionPermission(Enums.Operations.MemberPageView, true);

            ListMigrationModelView migrationModelView;
            try
            {
                if (siteID <= 0 || memberID <= 0)
                {
                    Redirect("/Search/Index");
                }

                var memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None, true);
                migrationModelView = memberManager.GetListMigrationSupportModelView();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberController.MigrationSupport(" + siteID + ", " + memberID + ").", ex);
            }

            return View("ListMigrationSupport", migrationModelView);
        }

        public ActionResult PhotoMigrationSupport(int siteId, int memberId)
        {
            if (siteId <= 0 || memberId <= 0)
            {
                Redirect("/Search/Index");
            }

            CheckActionPermission(Enums.Operations.MemberPageView, true);

            PhotoMigrationModelView migrationModelView;
            try
            {
                // passing in false for migrate member intentionally
                // Spark.MingleMigration.Service's getCachedMemberBytes BL method does not populate photos for the cached member object it returns
                // todo: update the service code to returns photos?
                var memberManager = new MemberManager(memberId, siteId, MemberLoadFlags.None, false);
                migrationModelView = memberManager.GetPhotoMigrationSupportModelView();
            }
            catch (Exception exception)
            {
                throw new Exception("Error in MemberController.MigrationSupport(" + siteId + ", " + memberId + ").",
                    exception);
            }

            return View("PhotoMigrationSupport", migrationModelView);
        }

        #region BillingInfo commented out
        /*
        public ActionResult BillingInfo(int orderID, int siteID, int memberID)
        {
            BillingInfo billingInfoModel = new BillingInfo();
            billingInfoModel.OrderID = orderID;

            ObsfucatedPaymentProfileResponse paymentProfileResponse = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderID, memberID, siteID);

            if (paymentProfileResponse != null
                && paymentProfileResponse.Code == "0"
                && paymentProfileResponse.PaymentProfile != null)
            {
                if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.CreditCard;
                    ObsfucatedCreditCardPaymentProfile creditCardPaymentProfile = (ObsfucatedCreditCardPaymentProfile)paymentProfileResponse.PaymentProfile;
                    billingInfoModel.CardType = creditCardPaymentProfile.CardType;
                    billingInfoModel.City = creditCardPaymentProfile.City;
                    billingInfoModel.Country = creditCardPaymentProfile.Country;
                    billingInfoModel.ExpMonth = creditCardPaymentProfile.ExpMonth;
                    billingInfoModel.ExpYear = creditCardPaymentProfile.ExpYear;
                    billingInfoModel.FirstName = creditCardPaymentProfile.FirstName;
                    billingInfoModel.LastFourDigitsCreditCardNumber = creditCardPaymentProfile.LastFourDigitsCreditCardNumber;
                    billingInfoModel.LastName = creditCardPaymentProfile.LastName;
                    billingInfoModel.PhoneNumber = creditCardPaymentProfile.PhoneNumber;
                    billingInfoModel.PostalCode = creditCardPaymentProfile.PostalCode;
                    billingInfoModel.State = creditCardPaymentProfile.State;
                    billingInfoModel.StreetAddress = creditCardPaymentProfile.StreetAddress;

                }
                else if (paymentProfileResponse.PaymentProfile is ObsfucatedCheckPaymentProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.Check;
                    ObsfucatedCheckPaymentProfile checkPaymentProfile = (ObsfucatedCheckPaymentProfile)paymentProfileResponse.PaymentProfile;
                    billingInfoModel.BankAccountNumberLastFour = checkPaymentProfile.BankAccountNumberLastFour;
                    billingInfoModel.BankAccountType = checkPaymentProfile.BankAccountType;
                    billingInfoModel.BankName = checkPaymentProfile.BankName;
                    billingInfoModel.BankState = checkPaymentProfile.BankState;
                    billingInfoModel.City = checkPaymentProfile.City;
                    billingInfoModel.EmailAddress = checkPaymentProfile.EmailAddress;
                    billingInfoModel.FirstName = checkPaymentProfile.FirstName;
                    billingInfoModel.LastName = checkPaymentProfile.LastName;
                    billingInfoModel.PhoneNumber = checkPaymentProfile.Phone;
                    billingInfoModel.PostalCode = checkPaymentProfile.Zip;
                    billingInfoModel.BankRoutingNumberLastFour = checkPaymentProfile.BankRoutingNumberLastFour;
                    billingInfoModel.State = checkPaymentProfile.State;
                    billingInfoModel.StreetAddress = checkPaymentProfile.Street;
                }
                else if (paymentProfileResponse.PaymentProfile is ObsfucatedPaypalPaymentProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.PayPalLitle;
                    ObsfucatedPaypalPaymentProfile paypalPaymentProfile = (ObsfucatedPaypalPaymentProfile)paymentProfileResponse.PaymentProfile;
                    billingInfoModel.FirstName = paypalPaymentProfile.FirstName;
                    billingInfoModel.LastName = paypalPaymentProfile.LastName;
                    billingInfoModel.EmailAddress = paypalPaymentProfile.Email;
                    billingInfoModel.Country = paypalPaymentProfile.Country;
                    billingInfoModel.PayerStatus = paypalPaymentProfile.PayerStatus;
                    billingInfoModel.PaypalToken = paypalPaymentProfile.PaypalToken;
                    billingInfoModel.BillingAgreementID = paypalPaymentProfile.BillingAgreementID;
                }
                else if (paymentProfileResponse.PaymentProfile is ObfuscatedManualPaymentProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.PaypalDirect;
                    ObfuscatedManualPaymentProfile manualPaymentProfile = (ObfuscatedManualPaymentProfile)paymentProfileResponse.PaymentProfile;
                    billingInfoModel.FirstName = manualPaymentProfile.FirstName;
                    billingInfoModel.LastName = manualPaymentProfile.LastName;
                }
                else if (paymentProfileResponse.PaymentProfile is ObfuscatedElectronicFundsTransferProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.ElectronicFundsTransfer;
                    ObfuscatedElectronicFundsTransferProfile electronicFundsTransferProfile = (ObfuscatedElectronicFundsTransferProfile)paymentProfileResponse.PaymentProfile;
                    billingInfoModel.BankAccountNumberLastFour = electronicFundsTransferProfile.BankAccountNumberLastFour;
                    billingInfoModel.BankAccountType = electronicFundsTransferProfile.BankAccountType;
                    billingInfoModel.City = electronicFundsTransferProfile.City;
                    billingInfoModel.EmailAddress = electronicFundsTransferProfile.EmailAddress;
                    billingInfoModel.FirstName = electronicFundsTransferProfile.FirstName;
                    billingInfoModel.LastName = electronicFundsTransferProfile.LastName;
                    billingInfoModel.PhoneNumber = electronicFundsTransferProfile.Phone;
                    billingInfoModel.PostalCode = electronicFundsTransferProfile.Zip;
                    billingInfoModel.BankRoutingNumberLastFour = electronicFundsTransferProfile.BankRoutingNumberLastFour;
                    billingInfoModel.State = electronicFundsTransferProfile.State;
                    billingInfoModel.StreetAddress = electronicFundsTransferProfile.Street;
                }
                else if (paymentProfileResponse.PaymentProfile is ObfuscatedPaymentReceivedProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.PaymentReceived;
                    ObfuscatedPaymentReceivedProfile paymentReceivedProfile = (ObfuscatedPaymentReceivedProfile)paymentProfileResponse.PaymentProfile;
                    billingInfoModel.FirstName = paymentReceivedProfile.FirstName;
                    billingInfoModel.LastName = paymentReceivedProfile.LastName;
                }
                else
                {
                    billingInfoModel.PaymentType = PaymentType.None;
                }

                return PartialView("~/Views/Member/Controls/BillingInfo.ascx", billingInfoModel);
            }
            else
            {
                billingInfoModel.PaymentType = PaymentType.None;
                return PartialView("~/Views/Member/Controls/BillingInfo.ascx", billingInfoModel);
            }
        }
        */

        #endregion
    }
}