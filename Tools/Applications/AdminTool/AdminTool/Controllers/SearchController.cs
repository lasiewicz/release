﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.Search;
using AdminTool.Models;
using System.Collections.Specialized;

namespace AdminTool.Controllers
{
    public class SearchController : BaseController
    {
        // GET: /Search/
        /// <summary>
        /// Default empty search page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //check for search pagination
            if (!String.IsNullOrEmpty(Request.QueryString["Page"]))
            {
                return Index(new FormCollection(Request.QueryString));
            }

            return View("Search", new SearchData());
        }

        // POST: /Search/
        /// <summary>
        /// Processes a search request
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(FormCollection formData)
        {
            //note: For this initial implementation, we will only support existing BH Admin search fields and restrict to one field at a time
            //Future phase will extend to support additional search fields and multiple criterias

            //check permission
            base.CheckActionPermission(Enums.Operations.Searching, true);

            SearchData searchDataModel = new SearchData();

            try
            {
                //get posted data; validation already happened in js and should happen in service code, so there's no need for it here
                searchDataModel.PopulateSearchData(formData);

                //perform search
                SearchManager searchManager = new SearchManager();
                searchDataModel.SearchResult = searchManager.ProcessSearch(searchDataModel);
                if (searchDataModel.SearchResult.OnlyOneResultReturned && searchDataModel.SearchResult.SearchMembers[0].MemberSites.Count > 0)
                {
                    //for single member results, we will send user to account management page
                    Response.Redirect("/Member/View/" + ((int)searchDataModel.SearchResult.SearchMembers[0].MemberSites[0].SiteID).ToString() + "/" + searchDataModel.SearchResult.SearchMembers[0].MemberID.ToString());
                }

                //throw new Exception("Toma's big error!");
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SearchController.Index()", ex);
            }

            return View("Search", searchDataModel);
            
        }

    }
}
