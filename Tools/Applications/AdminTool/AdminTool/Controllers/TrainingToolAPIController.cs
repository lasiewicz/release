﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.JSONResult;
using Matchnet.ApproveQueue.ServiceAdapters;

namespace AdminTool.Controllers
{
    public class TrainingToolAPIController : BaseController
    {
        //
        // GET: /TrainingToolAPI/

        public JsonResult RetrieveEmailMessage(int ID)
        {
            // ID = TTMessageID
            var result = new AdminTool.Models.JSONResult.PartialViewResult() { Status = (int)AdminTool.Models.Enums.Status.Success, StatusMessage = "Success" };

            try
            {
                var manager = new TrainingToolManager();
                var ttMessage = manager.RetrieveEmailMessage(ID);

                if (ttMessage != null)
                {
                    if (!string.IsNullOrEmpty(ttMessage.MessageBody) ||
                        !string.IsNullOrEmpty(ttMessage.MessageHeader))
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "Success";

                        var sb = new StringBuilder();
                        sb.Append("<b>" + ttMessage.MessageHeader + "</b><br>");
                        sb.Append(ttMessage.MessageBody);
                        result.HTML = sb.ToString();
                    }
                    else
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = "Message contains no header or body (Most likely deleted).";
                    }
                }
                else
                {
                    result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                    result.StatusMessage = "Email message was not found.";
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.RetrieveEmailMessage().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error retrieving the single training email specified.";
            }

            return Json(result);
        }

        public JsonResult AddSingleMember(int groupingID, string groupingName, int memberID, int siteID)
        {
            var result = new APIResult() { Status = (int)AdminTool.Models.Enums.Status.Success, StatusMessage = "Success" };

            try
            {
                var manager = new TrainingToolManager();
                var errorMessage = manager.AddSingleMember(groupingID, memberID, siteID);

                if (errorMessage == string.Empty)
                {
                    result.StatusMessage = string.Format("Member {0} has been added to {1} training tool profile.",
                                                         memberID,
                                                         groupingName);
                }
                else
                {
                    result.Status = (int) AdminTool.Models.Enums.Status.Failed;
                    result.StatusMessage = errorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.AddSingleMember().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error adding a single member to a training tool profile.";
            }

            return Json(result);
        }

        public JsonResult PauseTestSession(int testInstanceID)
        {
            var result = new APIResult() { Status = (int)AdminTool.Models.Enums.Status.Success, StatusMessage = "Test session is paused. You will be navigated away from this page shortly." };

            try
            {
                var manager = new TrainingToolManager();
                manager.PauseTestSession(testInstanceID);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.PauseTestSession().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error while trying to pause a test session.";
            }

            return Json(result);
        }

        public JsonResult GetMemberProfiles(int groupingID, int pageNumber, int rowsPerPage)
        {
            var result = new TTMemberProfileResult() { Status = (int)AdminTool.Models.Enums.Status.Success, StatusMessage = "Success" };

            try
            {
                var manager = new TrainingToolManager();
                var totalRows = 0;
                result.MemberProfiles = manager.GetMemberProfiles(groupingID, pageNumber, rowsPerPage, out totalRows);
                result.TotalRows = totalRows;
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.GetMemberProfiles().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error retrieving a list of member profiles for a given groupingID.";
            }

            return Json(result);
        }

        public JsonResult GetMemberProfilesByMemberID(int memberID)
        {
            var result = new TTMemberProfileResult() { Status = (int)AdminTool.Models.Enums.Status.Success, StatusMessage = "Success" };

            try
            {
                var manager = new TrainingToolManager();
                result.MemberProfiles = manager.GetMemberProfiles(memberID);
                result.TotalRows = result.MemberProfiles == null ? 0 : result.MemberProfiles.Count();
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.GetMemberProfiles().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error retrieving a list of member profiles for a given memberID.";
            }

            return Json(result);
        }

        public JsonResult GetMemberEmailsByEmailSubstring(string emailString, bool adminsOnly)
        {
            var result = new ArrayResult()
                {
                    Status = (int) AdminTool.Models.Enums.Status.Success,
                    StatusMessage = "Success"
                };

            try
            {
                var manager = new TrainingToolManager();
                result.Array = manager.GetMemberEmailsByEmailSubstring(emailString, adminsOnly);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.GetMemberEmailsByEmailSubstring().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error retrieving a list of member emails for a given email substring.";
            }
            
            return Json(result);
        }
        
        public JsonResult CreateNewGrouping(string groupingName)
        {
            var result = new APIResult() { Status = (int)AdminTool.Models.Enums.Status.Success, StatusMessage = "Success" };

            try
            {
                var manager = new TrainingToolManager();
                var returnMessage = manager.CreateNewGrouping(groupingName);

                if(returnMessage != string.Empty)
                {
                    result.Status = (int) AdminTool.Models.Enums.Status.Failed;
                    result.StatusMessage = returnMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.CreateNewGrouping().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error creating a new training tool profile.";
            }

            return Json(result);
        }

        public JsonResult CreateNewTestInstance(int groupingID, int adminID)
        {
            var result = new TTTestInstanceResult() {Status = (int) Enums.Status.Success, StatusMessage = "Success"};

            try
            {
                var manager = new TrainingToolManager();
                var testInstanceID = manager.CreateNewTestInstance(groupingID, adminID);
                result.TTTestInstanceID = testInstanceID;
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.CreateNewTestInstance().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error creating a new training tool test instance.";
            }

            return Json(result);
        }

        public JsonResult SendKeepalivePulse(int testInstanceId)
        {
            var result = new APIResult() { Status = (int)AdminTool.Models.Enums.Status.Success, StatusMessage = "Success" };
            try
            {
                DBApproveQueueSA.Instance.TrainingToolKeepalivePulse(testInstanceId);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.SendKeepalivePulse().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error while send a keepalive pulse.";
            }

            return Json(result);
        }

        public JsonResult MoveMemberProfile(int ttMemberProfileID, int memberID, int ttGroupingID)
        {
            var result = new APIResult() { Status = (int)Enums.Status.Success, StatusMessage = "Success. You will be automatically taken away from page. Please don't update anything because the values on this page are no longer valid." };
            try
            {
                var memberProfile = DBApproveQueueSA.Instance.GetTTMemberProfile(memberID, ttGroupingID);
                if (memberProfile != null)
                {
                    result.Status = (int) Enums.Status.Failed;
                    result.StatusMessage = string.Format("MemberID {0} already exists in the target bucket.", memberID);
                }

                if (result.Status == (int) Enums.Status.Success)
                {
                    DBApproveQueueSA.Instance.UpdateTTMemberProfileTTGroupingID(ttMemberProfileID, ttGroupingID);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in TrainingToolAPI.MoveMemberProfile().", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error while moving the member profile to a different grouping.";
            }

            return Json(result);
        }

    }
}
