﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AdminTool.Models;
using AdminTool.Models.ModelViews.DNE;
using AdminTool.Models.ModelViews.Search;
using Matchnet.ExternalMail.ServiceAdapters;

namespace AdminTool.Controllers
{
    public class DNEController : BaseController
    {
        private const int PageSize = 20;
        // GET: DNE/Search/
        /// <summary>
        /// Default empty search page
        /// </summary>
        /// <returns></returns>
        public ActionResult Search(int? pageNumber, int? siteId, string email, string rEmail)
        {
            var manager = new DNEManager();
            var data = new Search { Sites = manager.GetSites()};
            int totalRows = 0;
            email = string.IsNullOrEmpty(email) ? "" : email;
            if(pageNumber != null && siteId != null) //Paging
            {
                int startRow = (pageNumber.Value - 1) * PageSize + 1;
                data.SearchResults = manager.Search(siteId.Value, email, startRow, ref totalRows);
                data.StartRow = startRow;
                data.SiteId = siteId.Value;
                data.EmailAddress = email;
                data.TotalRows = totalRows;
                data.PageNumber = pageNumber.Value;
            }
            else if(!string.IsNullOrEmpty(rEmail) && siteId != null) //Remove email
            {
                manager.RemoveMember(siteId.Value, rEmail);
                data.SiteId = siteId.Value;
                data.EmailAddress = email;
                data.SearchResults = manager.Search(siteId.Value, email, 1, ref totalRows);
                data.TotalRows = totalRows;
            }
            return View("Search", data);
        }

         // POST: DNE/Search/
        /// <summary>
        /// Search request
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Search(FormCollection formData)
        {
            var manager = new DNEManager();
            string email = (!String.IsNullOrEmpty(formData["EmailAddress"])) ? formData["EmailAddress"] : "";
            int siteId = (!String.IsNullOrEmpty(formData["SiteId"])) ? Int32.Parse(formData["SiteId"]) : 0;
            int totalRows = 0;
            var data = new Search { EmailAddress = email,
                                    SiteId = siteId,
                                    Sites = manager.GetSites()
                                  };
            
            data.SearchResults = manager.Search(siteId,email, 1, ref totalRows );
            data.TotalRows = totalRows;
            return View("Search", data);
        }

        // GET: DNE/Downlaod/
        /// <summary>
        /// Download page
        /// </summary>
        /// <returns></returns>
        public ActionResult Download()
        {
            var manager = new DNEManager();
            var data = new Download { SiteSummary = manager.GetSitesSummary() };
            foreach (var emailSummary in data.SiteSummary)
                data.IsChecked.Add(false);
            return View("Download", data);
        }

        // POST: DNE/Search/
        /// <summary>
        /// Download request
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Download(int[] dneDownload)
        {
            byte[] bytes = null;
            bool hasDownloadContent = false;
            if (dneDownload != null && dneDownload.Length > 0)
            {
                var fileText = new StringBuilder();
                foreach (var siteId in dneDownload)
                {
                    ArrayList entries = DoNotEmailSA.Instance.GetEntriesBySite(siteId);
                    foreach (var entry in entries)
                    {
                        fileText.Append(entry + Environment.NewLine);
                    }
                }

                if (!string.IsNullOrEmpty(fileText.ToString()))
                {
                    var encoding = new System.Text.ASCIIEncoding();
                    bytes = encoding.GetBytes(fileText.ToString());
                    hasDownloadContent = true;
                }
            }

            if(hasDownloadContent)
                return File(bytes, "text/plain", "DoNotEmail.csv");

            var manager = new DNEManager();
            var data = new Download { SiteSummary = manager.GetSitesSummary() };
            foreach (var emailSummary in data.SiteSummary)
                data.IsChecked.Add(false);
            return View("Download", data);
        }

        // GET: DNE/Upload/
        /// <summary>
        /// Upload page
        /// </summary>
        /// <returns></returns>
        public ActionResult Upload()
        {
            var manager = new DNEManager();
            var data = new Search { Sites = manager.GetSites() };
            return View("Upload", data);
        }

         // POST: DNE/Upload/
        /// <summary>
        /// Uploads the file
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Upload(FormCollection formData)
        {
            var manager = new DNEManager();
            int count = 0;
            int siteId = (!String.IsNullOrEmpty(formData["SiteId"])) ? Int32.Parse(formData["SiteId"]) : 0;
            HttpPostedFileBase file = Request.Files[0];
            if (file != null)
            {
                var bytes = new byte[file.ContentLength];
                file.InputStream.Read(bytes, 0, file.ContentLength);
                var encoding = new ASCIIEncoding();
                string fileContent = encoding.GetString(bytes);
                count = manager.UploadEmailAddress(fileContent, siteId);
            }
            var data = new Search { Sites = manager.GetSites(), EmailUploadCount = count, SiteId = siteId };
            return View("Upload", data);
        }
    }
}