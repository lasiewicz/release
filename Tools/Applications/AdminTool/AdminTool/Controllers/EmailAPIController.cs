﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.JSONResult;
using System.Text;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using AdminTool.Models.ModelViews.Email;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace AdminTool.Controllers
{
    public class EmailAPIController : BaseController
    {
        //
        // GET: /EmailAPI/

        public JsonResult RetrieveEmailMessage(int memberID, int siteID, int messageID)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.ViewEmailActivity, true);
     
            AdminTool.Models.JSONResult.PartialViewResult result = new AdminTool.Models.JSONResult.PartialViewResult();

            try
            {
                Message message =  EmailMessageSA.Instance.RetrieveMessageByMessageID(memberID, messageID);
                if (message != null)
                {
                    if (!string.IsNullOrEmpty(message.MessageBody) ||
                        !string.IsNullOrEmpty(message.MessageHeader))
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Success;
                        result.StatusMessage = "Success";

                        var sb = new StringBuilder();
                        sb.Append("<b>" + message.MessageHeader + "</b><br>");
                        sb.Append(message.MessageBody);
                        result.HTML = sb.ToString();
                    }
                    else
                    {
                        result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                        result.StatusMessage = "Message contains no header or body (Most likely deleted).";
                    }
                }
                else
                {
                    result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                    result.StatusMessage = "Email message was not found.";
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error in EmailAPIController.RetrieveEmailMessage(): ", ex);
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error retrieving an email message.";
            }

            return Json(result);
        }


        public JsonResult GetImHistoryList(int memberID, int brandID)
        {
            var response = new InstantMessageHistoryMembersResponse();

            try
            {
                var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
                MessageMemberCollection messageMembers = EmailMessageSA.Instance.RetrieveInstantMessageMembers(memberID, brand.Site.Community.CommunityID, true, false);
                response.MatchesFound = messageMembers.TotalCount;


                if (messageMembers != null && messageMembers.Count > 0)
                {
                    List<MailMember> mailMemberList = new List<MailMember>();

                    foreach (MessageMember message in messageMembers)
                    {
                        mailMemberList.Add(EmailManager.ConvertMessageMemberToMailMember(memberID,message,brand));
                    }

                    response.MemberList = mailMemberList;
                    response.code = 200;
                }

                response.MemberId = memberID;
                response.GroupId = brandID;

            }
            catch (Exception ex)
            {
                _logger.Error("Error in EmailAPIController.GetImHistoryList(): ", ex);
                response.code = 500;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetImHistoryMessageList(int memberID, int targetMemberID, int brandID, int pageNumber, int pageSize, bool ignoreCache)
        {
            var response = new InstantMessageHistoryResponse();

            try
            {
                var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
                int startRow = ((pageNumber - 1) * pageSize) + 1;

                EmailMessageList messagesPage = EmailMessageSA.Instance.RetrieveInstantMessages(memberID, targetMemberID, brand.Site.Community.CommunityID, startRow, pageSize, ignoreCache);
                response.MatchesFound = messagesPage.TotalCount;

                response.MessageList = EmailManager.ConvertEmailMessageListToMailMessageList(memberID,messagesPage);
                 response.MemberId = memberID;
                 response.GroupId = brandID;
                 response.code = 200;
            }
            catch (Exception ex)
            {
                _logger.Error("Error in EmailAPIController.GetImHistoryMessageList(): ", ex);
                response.code = 500;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        






    }
}
