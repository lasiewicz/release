﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.ModelViews.TrainingTool;

namespace AdminTool.Controllers
{
    public class TrainingToolController : BaseController
    {
        //
        // GET: /TrainingTool/

        public ActionResult TTEmailList(int ttMemberProfileID, int folderID, int rowIndex)
        {
            var manager = new TrainingToolManager();
            var viewModel = manager.GetTTEmailList(ttMemberProfileID, folderID, rowIndex);

            return View(viewModel);
        }

        public ActionResult TTTestPage(int testInstanceID, int ttMemberProfileIDToExclude)
        {
            var manager = new TrainingToolManager();
            var viewModel = manager.GetTTTestPage(testInstanceID, ttMemberProfileIDToExclude);

            return View(viewModel);
        }

        public ActionResult TTEditProfile(int memberID, int groupingID)
        {
            CheckActionPermission(Enums.Operations.FTATrainingToolSupervisor, true);

            var manager = new TrainingToolManager();
            var viewModel = manager.GetTTEditProfile(memberID, groupingID);
            
            return View(viewModel);
        }

        public ActionResult TTSetAnswers(int memberID, int groupingID)
        {
            CheckActionPermission(Enums.Operations.FTATrainingToolSupervisor, true);

            var manager = new TrainingToolManager();
            var viewModel = manager.GetTTSetAnswers(memberID, groupingID);

            return View(viewModel);
        }

        public ActionResult TTViewMember(int memberID, int groupingID)
        {
            var isFTTSupervisor = CheckActionPermission(Enums.Operations.FTATrainingToolSupervisor, false);
            var manager = new TrainingToolManager();

            var viewModel = manager.GetTTViewMember(memberID, groupingID, isFTTSupervisor);
            
            return View(viewModel);
        }

        public ActionResult TTTraineeStart(int ID)
        {
            // ID = MemberID

            // Trainees can only look at their own stuff, if the memberIDs do not match, make sure it's a supervisor who is trying to 
            // access this page.
            if (ID != g.AdminID)
            {
                CheckActionPermission(Enums.Operations.FTATrainingToolSupervisor, true);
            }

            var manager = new TrainingToolManager();
            TTTraineeStart viewModel = manager.GetTTTraineeStart(ID);

            return View(viewModel);
        }

        public ActionResult TTTestResults(int ID)
        {
            // ID = TestInstanceID

            var manager = new TrainingToolManager();
            var viewModel = manager.GetTTTestResults(ID);

            return View(viewModel);
        }

        public ActionResult TTManageGrouping()
        {
            CheckActionPermission(Enums.Operations.FTATrainingToolSupervisor, true);

            var manager = new TrainingToolManager();
            var viewModel = manager.GetTTManageGrouping();

            return View(viewModel);
        }

        public ActionResult TTTestHistory(int ID)
        {
            // ID = MemberID
            if (g.AdminID != ID)
            {
                CheckActionPermission(Enums.Operations.FTATrainingToolSupervisor, true);
            }

            var manager = new TrainingToolManager();
            var viewModel = manager.GetTTTestHistory(ID);
            return View(viewModel);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SaveTTAnswer(TTSetAnswers model, FormCollection collection)
        {
            CheckActionPermission(Enums.Operations.FTATrainingToolSupervisor, true);

            var manager = new TrainingToolManager();
            manager.SaveTTAnswer(model, collection);
            
            return RedirectToAction("TTManageGrouping");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SaveAdminAnswer(TTTestPage model, FormCollection collection)
        {
            var manager = new TrainingToolManager();
            manager.SaveAdminAnswer(model, collection);

            return RedirectToAction("TTTestPage", new { TestInstanceID = model.TTTestInstanceID, TTMemberProfileIDToExclude  = model.TTMemberProfileID});
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Upload(FormCollection formData, HttpPostedFileBase csvFile)
        {
            CheckActionPermission(Enums.Operations.FTATrainingToolSupervisor, true);
            
            var manager = new TrainingToolManager();
            var groupingId = Convert.ToInt32(formData["GroupingID"]);
            var errorArray = manager.AddMemberProfilesFromCsv(csvFile.InputStream, groupingId);

            var viewModel = manager.GetTTManageGrouping();

            if (errorArray.Count == 0)
            {
                viewModel.BigUiMessage =
                    "You CSV file request has been submitted. The total execution time will depend on the size of the CSV file.";
            }
            else
            {
                var sb = new StringBuilder();
                foreach (string errorMsg in errorArray)
                {
                    sb.Append(errorMsg + "<br>");
                }
                viewModel.BigUiMessage = sb.ToString();
            }

            return View("TTManageGrouping", viewModel);
        }
    }

}
