﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.UPS;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using Spark.Common.Adapter;
using Spark.Common.AuthorizationService;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;
using  Spark.CommonLibrary.Logging;
using PaymentType = Matchnet.Purchase.ValueObjects.PaymentType;

namespace AdminTool.Controllers
{
    public class SubscriptionController : BaseController
    {
        private Logger _logger = new Logger(typeof(SubscriptionController));
        public ActionResult Credit(int memberID, int siteID, int brandID)
        {
            try
            {
                Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID)))) 
                {
                    var connector = new PaymentUIConnector();
                    var page = new CreditCardPage(memberID, brandID,false);
                    ((IPaymentUIJSON)page).PaymentType = ((Request["PaymentTypeID"] != null) && (Request["PaymentTypeID"] == "2")) ? PaymentType.Check : PaymentType.CreditCard;
                    ((IPaymentUIJSON)page).SaveLegacyData = true;
                    page.Context = System.Web.HttpContext.Current;

                    string html = connector.RedirectToPaymentUI(page, memberID, brandID);
                    return Content(html);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error SubscriptionController Credit page", ex);
            }
            return Content("<html><body>UPS Integration disabled!</body></html>");
        }

        public ActionResult ALaCarte(int memberID, int siteID, int brandID)
        {
            try
            {
                Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID))))
                {
                    // if member doesn't have a sub, don't even bother with the ups redirect
                    var renewalSub = MemberHelper.GetRenewalSubscription(memberID, siteID);
                    if(renewalSub == null)
                    {
                        return Content("<html><body>Subscription is required for this action.</body></html>");
                    }

                    var connector = new PaymentUIConnector();
                    var page = new PremiumServicesPage(memberID, brandID);
                    ((IPaymentUIJSON)page).PaymentType = ((Request["PaymentTypeID"] != null) && (Request["PaymentTypeID"] == "2")) ? PaymentType.Check : PaymentType.CreditCard;
                    ((IPaymentUIJSON)page).SaveLegacyData = true;
                    page.Context = System.Web.HttpContext.Current;

                    string html = connector.RedirectToPaymentUI(page, memberID, brandID);
                    return Content(html);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error SubscriptionController Premium Service/A La Carte page", ex);
            }
            return Content("<html><body>UPS Integration disabled!</body></html>");
        }

        public ActionResult PaymentProfile(int memberID, int siteID, int brandID)
        {
            try
            {
                Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID))))
                {
                    if(!MemberHelper.HasPaymentProfile(memberID, siteID))
                    {
                        return Content("<html><body>Subscription or payment profile is required for this action.</body></html>");
                    }

                    var connector = new PaymentUIConnector();
                    var page = new PaymentProfilePage(memberID, brandID);
                    ((IPaymentUIJSON)page).PaymentType = ((Request["PaymentTypeID"] != null) && (Request["PaymentTypeID"] == "2")) ? PaymentType.Check : PaymentType.CreditCard;
                    ((IPaymentUIJSON)page).SaveLegacyData = true;

                    string html = connector.RedirectToPaymentUI(page, memberID, brandID);
                    return Content(html);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error SubscriptionController Payment Profile page", ex);
            }
            return Content("<html><body>UPS Integration disabled!</body></html>");
        }
        
        public ActionResult Upsale(int impmid, int imppid)
        {
            try
            {
                Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(imppid);
                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", brand.Site.Community.CommunityID,
                   brand.Site.SiteID, brand.BrandID))))
                {
                    var connector = new PaymentUIConnector();
                    UpsalePage page = new UpsalePage(impmid, imppid);

                    ((IPaymentUIJSON)page).PaymentType = PaymentType.CreditCard;
                    ((IPaymentUIJSON)page).SaveLegacyData = true;
                    page.Context = System.Web.HttpContext.Current;

                    string html = connector.RedirectToPaymentUI(page, impmid, imppid);
                    return Content(html);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error SubscriptionController Upsale page", ex);
            }
            return Content("<html><body>UPS Integration disabled!</body></html>");
        }

        public ActionResult AdminOnly(int memberID, int siteID, int brandID)
        {
            try
            {
                Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID)))) 
                {
                    // get admin only or supervisor only promo
                    Promo adminPromo = base.CheckActionPermission(Enums.Operations.SupervisorPromo, false) ? PromoEngineSA.Instance.GetSupervisorPromo(memberID, brand, PaymentType.CreditCard) : PromoEngineSA.Instance.GetAdminPromo(memberID, brand, PaymentType.CreditCard);

                    // if supervisor promo is not read and the user is a supervisor, get the admin promo at least
                    if (adminPromo == null && base.CheckActionPermission(Enums.Operations.SupervisorPromo, false))
                        adminPromo = PromoEngineSA.Instance.GetAdminPromo(memberID, brand, PaymentType.CreditCard);

                    if(adminPromo == null)
                        return Content("<html><body><p style='color:red; font-weight:bold;'> No admin promos found for this brand.</p></body></html>");
                    
                    var connector = new PaymentUIConnector();
                    var page = new CreditCardPage(memberID, brandID, true);
                    ((IPaymentUIJSON)page).PaymentType = ((Request["PaymentTypeID"] != null) && (Request["PaymentTypeID"] == "2")) ? PaymentType.Check : PaymentType.CreditCard;
                    ((IPaymentUIJSON)page).SaveLegacyData = true;
                    page.Context = System.Web.HttpContext.Current;
                    page.PromoID = adminPromo.PromoID;

                    string html = connector.RedirectToPaymentUI(page, memberID, brandID);
                    return Content(html);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error SubscriptionController Buy AdminOnly page", ex);
            }
            return Content("<html><body>UPS Integration disabled!</body></html>");
        }

        public ActionResult Confirmation(int impmid, int imppid)
        {
            int orderID = Constants.NULL_INT;
            OrderInfo orderInfo = null;

            Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(imppid);
            Member TargetMember = MemberHelper.GetMember(impmid, MemberLoadFlags.IngoreSACache);

            SubscriptionHelper.ClearMingleMemberCache(TargetMember, brand);
            bool tryToUpsale = false;
            if (System.Web.HttpContext.Current.Request["upsale"] != null && System.Web.HttpContext.Current.Request["upsale"] == "true")
            {
                tryToUpsale = true;
                _logger.Debug("Sent to the purchase confirmation page where upsale was set to [" + System.Web.HttpContext.Current.Request["upsale"] + "]");
            }

            if (Convert.ToBoolean((RuntimeSettings.GetSetting("UPS_DIRECT_TO_UPSALE_WITH_NO_CONFIRMATION_PAGE", brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID))) && tryToUpsale)
            {
                if (System.Web.HttpContext.Current.Request["oid"] == null)
                {
                    throw new Exception("Order ID is missing");
                }
                string href = "/Subscription/Upsale";
                href += string.Format("?oid={0}&upsale=true&impmid={1}&imppid={2}",
                                      System.Web.HttpContext.Current.Request["oid"], impmid, imppid);
                _logger.Debug("Redirect to the upsale page from the order confirmation page to the following URL: " + href);
                return Redirect(href);
            }
            _logger.Debug("Sent to the purchase confirmation page without redirecting to the upsale page");
            Spark.Common.OrderHistoryService.PaymentType paymentType = Spark.Common.OrderHistoryService.PaymentType.CreditCard;

            var data = new SubscriptionData();
            data.HeaderText = "Congratulations on Your Subscription!";
            bool isFreeTrial = false, isColorCode = false;
            ObsfucatedPaymentProfileResponse paymentProfileResponse = null;
            AuthorizationSubscription freeTrialSubscription = null;

            if (System.Web.HttpContext.Current.Request["freetrial"] != null)
            {
                try
                {
                    int accessTransactionID = Convert.ToInt32(System.Web.HttpContext.Current.Request["freetrial"]);
                    freeTrialSubscription = AuthorizationServiceWebAdapter.GetProxyInstance().GetAuthorizationSubscriptionByAccessTransactionID(accessTransactionID);
                }
                catch (Exception ex)
                {
                    _logger.Debug("Error in getting the free trial subscription details, Error message: " + ex.Message);
                }
                finally
                {
                    AuthorizationServiceWebAdapter.CloseProxyInstance();
                }
                isFreeTrial = true;
            }
            else if (System.Web.HttpContext.Current.Request["oid"] == null)
            {
                throw new Exception("OrderID cannot be null.");
            }
            else
            {
                orderID = Convert.ToInt32(System.Web.HttpContext.Current.Request["oid"]);
                //get UPS order info
                orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(orderID);
                OrderHistoryServiceWebAdapter.CloseProxyInstance();

                if (orderInfo != null)
                {
                    //set duration
                    foreach (OrderDetailInfo odi in orderInfo.OrderDetail)
                    {
                        //current business rule requires all package items in an order to have same duration
                        if (odi.Duration > 0)
                        {
                            orderInfo.Duration = odi.Duration;
                            orderInfo.DurationTypeID = odi.DurationTypeID;
                            break;
                        }
                    }
                    //get payment profile info
                    paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderID, orderInfo.CustomerID, orderInfo.CallingSystemID);
                    PaymentProfileServiceWebAdapter.CloseProxyInstance();
                }
                else
                {
                    throw new Exception("OrderID: " + orderID.ToString() + ", did not return a valid order from UPS.");
                }
            }
            string mBoxNamePrefix = RuntimeSettings.GetSetting("T_N_T_SUB_CONFIRM_MBOX_NAME_PREFIX", brand.Site.Community.CommunityID, brand.Site.SiteID);
            if (!string.IsNullOrEmpty(mBoxNamePrefix))
            {
                if (isFreeTrial && freeTrialSubscription != null)
                {
                    data.MBoxScript = "mboxCreate('{0}_sub_complete','productPurchasedId={1}','orderTotal={2}','orderId={3}');";
                    data.MBoxScript = string.Format(data.MBoxScript, mBoxNamePrefix, Convert.ToString(freeTrialSubscription.PrimaryPackageID), Convert.ToString(freeTrialSubscription.AuthorizationAmount), Convert.ToString(freeTrialSubscription.AuthorizationSubscriptionID));
                }
                else
                {
                    data.MBoxScript = "mboxCreate('{0}_sub_complete','productPurchasedId={1}','orderTotal={2}','orderId={3}');";
                    data.MBoxScript = string.Format(data.MBoxScript, mBoxNamePrefix, SubscriptionHelper.UPSGetPrimaryPlan(orderInfo, brand).PlanID.ToString(), orderInfo.TotalAmount.ToString(), orderID.ToString());
                }
            }
            else
            {
                data.ShowOmnitureMboxSubscriptionConfirmation = false;
            }
            if (isFreeTrial)
            {
                data.ShowFreeTrialConfirmation = true;
                data.ShowFreeTrialConfirmationAdditional = true;
                data.ShowOtherStuff = true;
                string[] args = SubscriptionHelper.GetFreeTrialArgs(TargetMember,brand, freeTrialSubscription);
                data.UserName = args[0];
                data.ConfirmationNumber = args[1];
                data.CardType = args[2];
                data.CardNumber = args[3];
                data.TransactionDate = args[4];
                data.ShowFreeTrialConfirmationButton = false;
                data.HeaderText = "Congratulations on Your Subscription!";
                return View(data);
            }
            //Check ColorAnalysis Purchase
            if (orderInfo.OrderDetail.Any(orderDetailInfo => orderDetailInfo.ItemID == 1003))
            {
                isColorCode = true;
            }
            if (isColorCode)
            {
                data.ShowColorCodeConfirmation = true;
                data.ShowOtherStuff = false;
                string[] args = SubscriptionHelper.GetColorCodeArgs(TargetMember,brand,orderID);
                data.UserName = args[0];
                data.ConfirmationNumber = args[1];
                data.TransactionDate = args[2];
            }
                //Matchnet.Web.Framework.Txt txt = new Txt();
                //txtRecordInfo.ExpandImageTokens = true;
                //Continue.Attributes.Add("onclick", string.Format("javascript:document.location=\"{0}\";", SubscriptionHelper.GetContinueLink(g)));

            data.UserName = TargetMember.GetUserName(brand);
            data.ConfirmationNumber = orderID.ToString();
            data.TransactionDate = DateTime.Now.ToString();

            if (paymentProfileResponse != null && paymentProfileResponse.PaymentProfile != null)
            {
                var ccPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                if (ccPaymentProfile != null)
                {
                    data.CardType = ccPaymentProfile.CardType;
                    data.CardNumber = ccPaymentProfile.LastFourDigitsCreditCardNumber;
                }
            }

            return View(data);
        }
        
    }
}
