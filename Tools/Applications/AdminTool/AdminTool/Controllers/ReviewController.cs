﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using AdminTool.Models.ModelViews.Review;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using AdminTool.Models.ModelViews.Member;
using Matchnet.ExternalMail.ServiceAdapters;
using Spark.CommonLibrary;

namespace AdminTool.Controllers
{
    public class ReviewController : BaseController
    {
        public ActionResult ViewMemberFraudReviewQueue()
        {
            var data = new MemberFraudReviewModelView{ Sites = ReviewManager.GetSites(null)};
            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SubmitFraudReview(MemberFraudReviewModelView model, string fraudReview, int? siteId)
        {
            if(fraudReview.ToLower() == "submit")
            {
                var reviewManager = new ReviewManager();
                var data = reviewManager.CompleteFraudReview(model);
                ModelState.Clear();
                return View("ViewMemberFraudReviewQueue", data);
            }
            else
            {
                ModelState.Clear();
                if (siteId.HasValue)
                {
                    var reviewManager = new ReviewManager();
                    var data = reviewManager.GetFraudReviewQueueItems(siteId.Value);
                    return View("ViewMemberFraudReviewQueue", data);
                }
            }
            return View("ViewMemberFraudReviewQueue", new MemberFraudReviewModelView());    
        }

        public ActionResult TextReview(bool? isOnDemand, int? supervisorId, int? adminMemberId, bool? perAdmin)
        {
            var reviewManager = new ReviewManager();
            var textApprovalReviewModelView = (isOnDemand.HasValue && isOnDemand.Value) ? 
                reviewManager.GetTextApprovalReview(true, supervisorId, adminMemberId, perAdmin) :
                reviewManager.GetTextApprovalReview(false, null, null, null);
            TempData["UserInfo"] = textApprovalReviewModelView.ExpandedUserInfo;
            return View("ViewTextApprovalReviewQueue", textApprovalReviewModelView);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SubmitTextReview(FormCollection collection, TextApprovalReviewModelView textApprovalReviewModelView, string reviewAction)
        {
            string action = reviewAction.ToLower();
            var reviewManager = new ReviewManager();
            textApprovalReviewModelView.ExpandedUserInfo = (ExpandedUserInfo)TempData["UserInfo"];
            reviewManager.CompleteAdminTextApprovalReview(collection, textApprovalReviewModelView, action);
            ModelState.Clear();
            if(textApprovalReviewModelView.ProcessSingleAdmin)
                return RedirectToAction("TextReview", new { isOnDemand = textApprovalReviewModelView.IsOnDemand, 
                                                            supervisorId = textApprovalReviewModelView.SupervisorId, 
                                                            adminMemberId = textApprovalReviewModelView.AdminMemberId, 
                                                            perAdmin = textApprovalReviewModelView.ProcessSingleAdmin });
            return RedirectToAction("TextReview", new { isOnDemand = textApprovalReviewModelView.IsOnDemand });
        }

        public ActionResult AcknowledgeTrainingReport(int mtaid, int adminId, int supervisorId, bool isOnDemand)
        {
            var reviewManager = new ReviewManager();
            var acknowledgeModelView = reviewManager.AcknowledgeTrainingReport(mtaid, adminId, supervisorId, isOnDemand);
            return View("AcknowledgeTrainingReport", acknowledgeModelView);
        }

        public ActionResult AdminSampleRates()
        {
            var reviewManager = new ReviewManager();
            var sampleRatesModelView = reviewManager.GetAdminSampleRates();
            return View("AdminSampleRates", sampleRatesModelView);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult AdminSampleRates(AdminSampleRatesModelView model)
        {
            var reviewManager = new ReviewManager();
            var sampleRatesModelView = reviewManager.SaveAdminSampleRates(model);
            return View("AdminSampleRates", sampleRatesModelView);
        }

        public ActionResult CRXReviewQueue()
        {
            var reviewManager = new ReviewManager();
            var crxReviewTextModelView = reviewManager.GetCRXSecondReviewText();
            return View("ViewCRXReviewQueue", crxReviewTextModelView);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SubmitCRXReview(FormCollection collection, TextApprovalReviewModelView textApprovalReviewModelView)
        {
            var reviewManager = new ReviewManager();
            reviewManager.CompleteCRXSecondReview(textApprovalReviewModelView,collection);
            return RedirectToAction("CRXReviewQueue");
        }

        public ActionResult SODBReviewQueue()
        {
            base.CheckActionPermission(Enums.Operations.SODBAccess, true);

            var reviewManager = new ReviewManager();
            var modelView = reviewManager.GetItemFromSODBQueue(false);
            return View("SODBReviewQueue", modelView);
        }

        public ActionResult SODBSecondReviewQueue()
        {
            base.CheckActionPermission(Enums.Operations.SODBAccess, true);

            var reviewManager = new ReviewManager();
            var modelView = reviewManager.GetItemFromSODBQueue(true);
            return View("SODBReviewQueue", modelView);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SubmitSODBDecision(FormCollection collection, SODBReviewModelView sodbReviewModelView)
        {
            base.CheckActionPermission(Enums.Operations.SODBAccess, true);

            var reviewManager = new ReviewManager();
            sodbReviewModelView.SelectedDecision = Convert.ToInt16(collection["sodbDecision"]);
            reviewManager.CompleteSODBReview(sodbReviewModelView);
            if(sodbReviewModelView.IsSecondReview)
                return RedirectToAction("SODBSecondReviewQueue");
            if(sodbReviewModelView.IsWaitingforSSP)
                return RedirectToAction("SODBSearch");
            return RedirectToAction("SODBReviewQueue");
        }

        public ActionResult SODBSearch()
        {
            base.CheckActionPermission(Enums.Operations.SODBAccess, true);

            var reviewManager = new ReviewManager();
            var modelView = new SodbSearchModelView();
            return View("SODBSearchMember", modelView);
        }

         [HttpPost, ValidateInput(false)]
        public ActionResult SODBSearchMember(SodbSearchModelView model)
        {
            base.CheckActionPermission(Enums.Operations.SODBAccess, true);
            if (ModelState.IsValid)
            {
                var reviewManager = new ReviewManager();
                var modelView = reviewManager.SearchSodbMember(model.MemberId.Value, (model.SiteId > 0 ? model.SiteId : null), (model.SSPBlueId > 0 ? model.SSPBlueId : null));
                return View(modelView);
            }
            return View(new SodbSearchModelView());
        }

         public ActionResult GetSodbMember(int memberId, int sspBlueId, int siteId)
         {
             base.CheckActionPermission(Enums.Operations.SODBAccess, true);

             var reviewManager = new ReviewManager();
             var modelView = reviewManager.GetSodbMember(memberId, sspBlueId, siteId);
             return View("SODBReviewQueue", modelView);
         }

        public ActionResult OnDemandTextReview(DateTime startDate, DateTime endDate, CommunityIDs communityId,string rates)
        {
            //Populate OnDemand Queue
            var reviewManager = new ReviewManager();

            if (communityId == CommunityIDs.None)
                reviewManager.PopulateOnDemandTextApprovalReview(startDate, endDate, null, rates);
            else
                reviewManager.PopulateOnDemandTextApprovalReview(startDate, endDate, (int)communityId, rates);
            return RedirectToAction("OnDemandReviewQueueManagement");
            //TempData["UserInfo"] = textApprovalReviewModelView.ExpandedUserInfo;
            //return View("ViewTextApprovalReviewQueue", textApprovalReviewModelView);
        }

        public ActionResult OnDemandReviewQueueManagement()
        {
            var reviewManager = new ReviewManager();
            var modelView = reviewManager.GetOnDemandQueueCounts();
            return View("OnDemandReviewQueueManagement", modelView);
        }

        public JsonResult PurgeOnDemandQueue(int adminMemberId, int supervisorId)
        {
            var reviewManager = new ReviewManager();
            bool success = reviewManager.PurgeOndemandReviewQueue(adminMemberId, supervisorId);
            return Json(new { status = success}, JsonRequestBehavior.AllowGet); 
        }
    }
}
