﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.JSONResult;
using AdminTool.Models;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Spark.Common.DiscountService;
using AdminTool.Models.ModelViews.SubAdmin;

namespace AdminTool.Controllers
{
    public class SubAdminAPIController : BaseController
    {
        //
        // GET: /SubAdminAPI/

        public JsonResult GetCurrencyString(int brandId)
        {
            var result = new SingleStringResult();

            result.StringResult = GeneralHelper.GetCurrencyString(brandId);
            result.Status = (int)AdminTool.Models.Enums.Status.Success;

            return Json(result);
        }

        public ActionResult GetGiftDetails(string giftCode)
        {
            GiftManager giftManager = new GiftManager();
            GiftDetailsModelView modelView = giftManager.GetGiftDetailsModelView(giftCode);
            return PartialView("~/Views/SubAdmin/Controls/GiftDetails.ascx", modelView);
        }

        public JsonResult CancelGift(string giftCode)
        {
            APIResult result = new APIResult();
            result.Status = (int)AdminTool.Models.Enums.Status.Success;
            result.StatusMessage = "Gift Cancelled";

            GiftManager giftManager = new GiftManager();
            bool cancelled = giftManager.CancelGift(giftCode, g.AdminID);
            if(!cancelled)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error cancelling gift";
            }

            return Json(result);
        }

        public JsonResult ReopenGift(string giftCode)
        {
            APIResult result = new APIResult();
            result.Status = (int)AdminTool.Models.Enums.Status.Success;
            result.StatusMessage = "Gift Reopened";

            GiftManager giftManager = new GiftManager();
            bool reopened = giftManager.ReopenGift(giftCode);
            if (!reopened)
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Error reopening gift";
            }

            return Json(result);
        }

        public JsonResult SendGiftNotification(int memberID, int siteID, int giftID, string giftCode, string emailAddress)
        {
            APIResult result = new APIResult();
            result.Status = (int)AdminTool.Models.Enums.Status.Success;
            result.StatusMessage = "Notification sent.";
            
            GiftManager giftManager = new GiftManager();
            if (!giftManager.IsEmailAddressEligibleForGift(emailAddress, siteID))
            {
                result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                result.StatusMessage = "Email address belongs to an existing subscriber";
            }
            else
            {
                bool sendSuccess = giftManager.SendNotification(giftID, memberID, siteID, emailAddress, g.AdminID);    
                if(!sendSuccess)
                {
                    result.Status = (int)AdminTool.Models.Enums.Status.Failed;
                    result.StatusMessage = "Unable to send notification.";
                }
            }

            return Json(result);
        }

        /// <summary>
        /// retuns a plandisplay for the passed in planId, and brandId
        /// </summary>
        /// <param name="planId"></param>
        /// <param name="brandId"></param>
        /// <returns></returns>
        public JsonResult GetPlan(int brandId, int planId)
        {
            var manager = new PlanManager();

            PlanDetailedDisplay pdd = null;
            PlanDisplay pd = manager.GetPlan(planId, brandId);

            if (pd != null && pd.PurchasePlan != null)
            {
                pdd = new PlanDetailedDisplay();

                pdd.BasicCost = pd.BasicCost;
                pdd.PlanTypeDescription = pd.PlanTypeDescription;
                pdd.RenewCost = pd.PurchasePlan.RenewCost;
                pdd.InitialDuration = pd.PurchasePlan.InitialDuration.ToString() + " " + GeneralHelper.GetDurationStringForDurationType((int)pd.PurchasePlan.InitialDurationType);
                pdd.RenewDuration = pd.PurchasePlan.RenewDuration.ToString() + " " + GeneralHelper.GetDurationStringForDurationType((int)pd.PurchasePlan.RenewDurationType);
                pdd.PlanType = GeneralHelper.GetPlanTypeString(pd.PurchasePlan);

            }

            return Json(pdd);
        }


        public JsonResult GetResourceTemplateCollection(int brandId, int templateTypeId)
        {
            ResourceTemplateCollection templateCollection = ResourceTemplateSA.Instance.GetResourceTemplateCollection();
            var tempType = (ResourceTemplateType)templateTypeId;
            var templates = (from ResourceTemplate resTemplate in templateCollection
                                                where resTemplate.GroupID == brandId && resTemplate.ResourceTemplateType == tempType
                                                orderby resTemplate.ResourceTemplateID
                                                select new {resTemplate.ResourceTemplateID, resTemplate.ResourceTemplateType, resTemplate.Content, resTemplate.Description, resTemplate.GroupID }).ToList();

            templates.Sort((x, y) => string.Compare(x.Description, y.Description));

            return Json(templates);
        }
    }
}
