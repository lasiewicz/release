﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.Review;
using Matchnet;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.BulkMail.ServiceAdapters;
using AdminTool.Models;
using AdminTool.Models.ModelViews.Reports;

using AdminTool.Models.ModelViews.Member;
using Spark.CommonLibrary;
using Matchnet.Member.ServiceAdapters;
using AdminTool.Models.ModelViews;
using AdminTool.Models.JSONResult;
using AdminTool.Models.ModelViews.Approval;
using System.Drawing;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace AdminTool.Controllers
{
    public class ReportsAPIController : BaseController
    {
        //
        // GET: /ReportsAPI/

        public ActionResult GetBulkMailBatchForDate(DateTime batchDate)
        {
            BulkMailBatchModelView modelView = new ReportsManager().GetBulkMailBatchForDate(batchDate);
            return PartialView("~/Views/Reports/Controls/BulkMailBatchDetail.ascx", modelView);
        }

        public ActionResult GetApprovalCountTotals(DateTime startDate, DateTime endDate, string adminMemberID, CommunityIDs communityID, SiteIDs siteID, Enums.ApprovalCountReportSortType sortType, Enums.SortDirection sortDirection)
        {
            ApprovalReportsManager manager = new ApprovalReportsManager();
            ApprovalCountTotalsModelView modelView = manager.GetCountTotals(startDate, endDate, adminMemberID, communityID, siteID, sortType, sortDirection);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            return PartialView("~/Views/Reports/Controls/ApprovalCountTotals.ascx", modelView);
        }

        public ActionResult GetMemberProfileDuplicateHistoryCount(DateTime startDate, DateTime endDate, string adminMemberID)
        {
            CheckActionPermission(Enums.Operations.CRXDuplicatesSupervisor, true);

            ApprovalReportsManager manager = new ApprovalReportsManager();
            MemberProfileDuplicateCountsResult partialView = manager.GetMemberProfileDuplicateHistoryCount(startDate,
                                                                                                           endDate,
                                                                                                           string.IsNullOrEmpty(adminMemberID) ? Constants.NULL_INT : int.Parse(adminMemberID));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            return PartialView("~/Views/Reports/Controls/MemberProfileDuplicateCountsResult.ascx", partialView);
        }

        public ActionResult GetQueueCountTotals(QueueItemType itemType)
        {
            ApprovalQueueCountTotalsModelView modelView = null;
            ApprovalReportsManager manager = new ApprovalReportsManager();

            switch (itemType)
            {
                case QueueItemType.Photo:
                    modelView = manager.GetPhotoApprovalQueueCounts();
                    break;
                case QueueItemType.Text:
                    modelView = manager.GetTextApprovalQueueCounts();
                    break;
                case QueueItemType.QuestionAnswer:
                    modelView = manager.GetQAApprovalQueueCounts();
                    break;
                case QueueItemType.FacebookLike:
                    modelView = manager.GetFacebookLikesApprovalQueueCounts();
                    break;
                case QueueItemType.CrxSecondReview:
                    modelView = manager.GetCrxReviewQueueCounts();
                    break;
                case QueueItemType.MemberProfileDuplicate:
                    modelView = manager.GetMemberProfileDuplicateQueueCounts();
                    break;
                case QueueItemType.MemberProfileDuplicateReview:
                    modelView = manager.GetMemberProfileDuplicateReviewQueueCounts();
                    break;
            }

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            return PartialView("~/Views/Reports/Controls/QueueCountTotals.ascx", modelView);
        }

        public ActionResult GetPhotosQueue(int siteID)
        {
            CheckActionPermission(Enums.Operations.PhotoApprovalQueue, true);
            var manager = new ApprovalReportsManager();
            var photoItems = manager.GetPhotoQueueItems(siteID);
            return Json(photoItems.Select(x => new { MemberID = x.MemberId, x.SiteName, x.InsertDate, x.PhotoAge }).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
