﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using Spark.CommonLibrary.Logging;
using System.Configuration;

namespace AdminTool.Controllers
{
    /// <summary>
    /// Base Controller to allow for common initializations that may be needed later for all requests.
    /// </summary>
    public class BaseController : Controller
    {
        public Logger _logger;

        public ContextGlobal g
        {
            get { return HttpContext.Items["g"] as ContextGlobal; }
        }

        public BaseController()
        {
            _logger = new Logger(this.GetType());
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            //initialize context global
            if (!HttpContext.Items.Contains("g"))
            {
                HttpContext.Items["g"] = new ContextGlobal();
            }

            //ensure admin's domain/bedrock mapping exists
            if (ConfigurationManager.AppSettings["isADEnabled"] == "true" && g.AdminID <= 0)
            {
                ViewData[WebConstants.ERROR_KEY] = "Admin " + g.AdminDomainAccount + " is missing domain/bedrock mapping.";
                throw new Exception("Error in OnActionExecuting(). Admin is missing domain/bedrock mapping.");
            }
            
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            //add logging
            string adminInfo = "";
            if (g != null)
            {
                adminInfo = "Admin User: " + g.AdminDomainAccount + ", " + g.AdminID.ToString() + ". ";
            }
            _logger.Error("Admin Tool error. " + adminInfo, filterContext.Exception);

            //display friendly error view
            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                if (!ViewData.ContainsKey(WebConstants.ERROR_KEY))
                {
                    //if apis that threw error did not provide a error page friendly error message, we'll display the full exception
                    ViewData[WebConstants.ERROR_KEY] = ExceptionHelper.GetExceptionMessage(filterContext.Exception, ExceptionHelper.ExceptionMode.Html);
                }
                filterContext.HttpContext.Response.Clear();
                filterContext.ExceptionHandled = true;
                filterContext.Result = this.View("Error", new HandleErrorInfo(filterContext.Exception, (string)filterContext.RouteData.Values["controller"], (string)filterContext.RouteData.Values["action"]));

            } 

        }

        protected bool CheckActionPermission(AdminTool.Models.Enums.Operations operation, bool throwExceptionOnNoAccess)
        {
            ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
            if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, (int)operation))
            {
                if (throwExceptionOnNoAccess)
                {
                    ViewData[WebConstants.ERROR_KEY] = "Your admin account " + g.AdminDomainAccount + " does not have permissions for operation: " + operation.ToString();
                    throw new Exception("Your admin account does not have permissions for operation: " + operation.ToString());
                }

                return false;
            }

            return true;
        }

    }
}
