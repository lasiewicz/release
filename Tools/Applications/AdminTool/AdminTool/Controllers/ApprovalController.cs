﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.Member;
using Matchnet.ApproveQueue.ServiceAdapters;
using Spark.CommonLibrary;
using Matchnet.Member.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Member.ValueObjects;
using AdminTool.Models;
using AdminTool.Models.ModelViews;
using AdminTool.Models.ModelViews.Approval;
using Matchnet;
using Spark.FacebookLike.ValueObjects;

namespace AdminTool.Controllers
{
    public class ApprovalController : BaseController
    {
        //
        // GET: /Approval/

        public ActionResult ViewBySite(SiteIDs ID)
        {
            return View();
        }

        public ActionResult ViewFacebookLikesByCommunity(CommunityIDs ID)
        {
            ApprovalManager manager = new ApprovalManager();
            ApprovalFacebookLikeModelView facebookLikesApprovalByCommunity = (ApprovalFacebookLikeModelView)manager.GetFacebookLikesApprovalByCommunity(ID);

            return View(facebookLikesApprovalByCommunity);
        }

        public ActionResult ViewFacebookLikesBySite(SiteIDs ID)
        {
            CommunityIDs communityID = GeneralHelper.GetCommunityIDForSiteID(ID);
            ApprovalManager manager = new ApprovalManager();
            ApprovalFacebookLikeModelView facebookLikesApprovalBySite = (ApprovalFacebookLikeModelView)manager.GetFacebookLikesApprovalBySite(ID, communityID);

            return View(facebookLikesApprovalBySite);
        }

        public ActionResult ViewFacebookLikesByMember(int memberID, SiteIDs siteID)
        {
            CommunityIDs communityID = GeneralHelper.GetCommunityIDForSiteID(siteID);
            ApprovalManager manager = new ApprovalManager();
            ApprovalFacebookLikeMemberModelView facebookLikesApprovalByMember = (ApprovalFacebookLikeMemberModelView)manager.GetFacebookLikesApprovalByMember(memberID, siteID, communityID);

            return View(facebookLikesApprovalByMember);
        }

        public ActionResult ViewPhotosByCommunity(CommunityIDs ID)
        {
            ApprovalManager manager = new ApprovalManager();
            PhotoApprovalModelView photoApprovalModelView = (PhotoApprovalModelView)manager.GetPhotoApprovalByCommunity(3, ID);

            return View(photoApprovalModelView);
        }

        public ActionResult ViewPhotosBySite(SiteIDs ID)
        {
            CommunityIDs communityID = GeneralHelper.GetCommunityIDForSiteID(ID);
            ApprovalManager manager = new ApprovalManager();
            PhotoApprovalModelView photoApprovalModelView = (PhotoApprovalModelView)manager.GetPhotoApprovalBySite(3, ID, communityID);

            return View(photoApprovalModelView);
        }

        public ActionResult ViewPhotosByMember(int memberID, SiteIDs siteID)
        {
            ApprovalManager manager = new ApprovalManager();
            CommunityIDs communityID = GeneralHelper.GetCommunityIDForSiteID(siteID);
            PhotoApprovalMemberModelView photoApprovalModelView = (PhotoApprovalMemberModelView)manager.GetPhotoApprovalByMember(memberID, communityID);

            return View(photoApprovalModelView);
        }

        public ActionResult ViewMemberProfileDuplicateByLanguage(int ID)
        {
            TextApprovalManager manager = new TextApprovalManager(ID, TextType.Regular);
            bool crxTextNotFound = false;
            var modelView = manager.GetMemberProfileDuplicate(ID, Constants.NULL_INT, out crxTextNotFound);

            if(crxTextNotFound)
                return RedirectToAction("ViewMemberProfileDuplicateByLanguage", new { ID = ID });

            return View(modelView);
        }

        public ActionResult ViewMemberProfileDuplicateReview(int ID)
        {
            CheckActionPermission(Enums.Operations.CRXDuplicatesSupervisor, true);

            TextApprovalManager manager = new TextApprovalManager(ID, TextType.Regular);
            var modelView = manager.GetMemberProfileDuplicateReview(ID, Constants.NULL_INT);
            return View(modelView);
        }

        public ActionResult ViewTextByLanguage(int ID)
        {
            TextApprovalManager manager = new TextApprovalManager(ID, TextType.Regular);
            TextApprovalModelView modelView = manager.GetTextApprovalByLanguage();
            return View(modelView);
        }

        public ActionResult ViewTextByMember(int memberID, SiteIDs siteID)
        {
            CommunityIDs communityID = GeneralHelper.GetCommunityIDForSiteID(siteID);
            TextApprovalManager manager = new TextApprovalManager(TextType.Regular, memberID, communityID);
            TextApprovalMemberModelView modelView = manager.GetMemberApproval(siteID);
            return View(modelView);
        }

        public ActionResult ViewQATextByCommunity(CommunityIDs ID)
        {
            TextApprovalManager manager = new TextApprovalManager();
            TextApprovalQAModelView modelView = manager.GetQAApprovalByCommunity(ID);
            return View("ViewQAText", modelView);
        }

        public ActionResult ViewQATextByLanguage(int ID)
        {
            TextApprovalManager manager = new TextApprovalManager();
            TextApprovalQAModelView modelView = manager.GetQAApprovalByLanguage(ID);
            return View("ViewQAText", modelView);
        }

        public ActionResult ViewQATextBySite(SiteIDs ID)
        {
            TextApprovalManager manager = new TextApprovalManager();
            TextApprovalQAModelView modelView = manager.GetQAApprovalBySite(ID);
            return View("ViewQAText", modelView);
        }

        public ActionResult ViewQATextByMember(int memberID, SiteIDs siteID)
        {
            TextApprovalManager manager = new TextApprovalManager();
            TextApprovalQAMemberModelView modelView = manager.GetQAApprovalByMember(memberID, siteID);
            return View("ViewQATextByMember", modelView);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteMemberProfileDuplicate(FormCollection collection, int languageID, int memberProfileDuplicateQueueId)
        {
            int i = 0;
            var manager = new TextApprovalManager(languageID, TextType.Regular);
            var invalidQueueItem = false;
            var errorMessage = manager.CompleteMemberProfileDuplicate(collection, languageID, memberProfileDuplicateQueueId, out invalidQueueItem);

            if(string.IsNullOrEmpty(errorMessage))
            {
                return RedirectToAction("ViewMemberProfileDuplicateByLanguage", new { ID = languageID });
            }
            else
            {
                bool crxTextNotFound = false;
                var modelView = invalidQueueItem
                                    ? manager.GetEmptyMemberProfileDuplicateApprovalModelView(languageID)
                                    : manager.GetMemberProfileDuplicate(languageID, memberProfileDuplicateQueueId, out crxTextNotFound);

                modelView.ErrorMessage = errorMessage;
                return View("ViewMemberProfileDuplicateByLanguage", modelView);
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteMemberProfileDuplicateReview(FormCollection collection, int languageID, int memberProfileDuplicateHistoryID)
        {
            CheckActionPermission(Enums.Operations.CRXDuplicatesSupervisor, true);

            int i = 0;
            var manager = new TextApprovalManager(languageID, TextType.Regular);
            var invalidQueueItem = false;
            var errorMessage = manager.CompleteMemberProfileDuplicateReview(collection, languageID, memberProfileDuplicateHistoryID, out invalidQueueItem);

            if(string.IsNullOrEmpty(errorMessage))
            {
                return RedirectToAction("ViewMemberProfileDuplicateReview", new {ID = languageID});
            }
            else
            {
                var modelView = invalidQueueItem
                                    ? manager.GetEmptyMemberProfileDuplicateReviewModelView(languageID)
                                    : manager.GetMemberProfileDuplicateReview(languageID, memberProfileDuplicateHistoryID);
                modelView.ErrorMessage = errorMessage;
                return View("ViewMemberProfileDuplicateReview", modelView);
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteQATextApproval(FormCollection collection, QAApprovalType approvalType,
            int memberID, int languageID, int communityID, int siteID, bool suspend)
        {
            if (collection["Answer1"] != null)
            {
                int index = 1;
                List<ApprovalQuestionUpdate> approvalUpdates = new List<ApprovalQuestionUpdate>();

                while (collection["Answer" + index.ToString()] != null)
                {
                    bool delete = false;
                    int answerID = Convert.ToInt32(collection["AnswerID" + index.ToString()]);
                    string answer = collection["Answer" + index.ToString()];
                    if (collection["Delete" + index.ToString()] != "false") { delete = true; }
                    approvalUpdates.Add(new ApprovalQuestionUpdate(answerID, answer, delete));
                    index++;
                }

                TextApprovalManager approvalManager = new TextApprovalManager();

                if (suspend)
                {
                    AdminActionReasonID adminActionReasonID = (AdminActionReasonID)Convert.ToInt32(collection["AdminActionReasonID"]);
                    approvalManager.CompleteQATextApprovalWithSuspend(approvalUpdates, memberID, (CommunityIDs)communityID, (SiteIDs) siteID, languageID, adminActionReasonID);
                }
                else
                {
                    approvalManager.CompleteQATextApproval(approvalUpdates, memberID, (CommunityIDs)communityID, (SiteIDs)siteID, languageID);
                }
            }

            switch (approvalType)
            {
                case QAApprovalType.QueuedApprovalByLanguage:
                    return RedirectToAction("ViewQATextByLanguage", new { ID = languageID });
                    break;
                case QAApprovalType.QueuedApprovalByCommunity:
                    return RedirectToAction("ViewQATextByCommunity", new { ID = communityID });
                    break;
                case QAApprovalType.QueuedApprovalBySite:
                    return RedirectToAction("ViewQATextBySite", new { ID = siteID });
                    break;
                case QAApprovalType.MemberApproval:
                    return RedirectToAction("View", "Member", new { siteID = siteID, memberID = memberID });
                    break;
                default:
                    return RedirectToAction("View", "Member", new { siteID = siteID, memberID = memberID });
                    break;
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteTextApproval(FormCollection collection, RegularApprovalType approvalType,
            int memberID, int languageID, TextType textType, bool suspend, int communityID, string adminNotes, int? siteID,
            string ip, int? profileRegionID, int? genderMask, int? maritalStatus, string firstName, string lastName,
            string email, string occupationDescription, int? educationLevel, int? religion, int? ethnicity,
            DateTime? registrationDate, string subscriberStatus, int? daysSinceFirstSubscription, string billingPhoneNumber, int? memberTextApprovalId)
        {
            List<WarningReason> warnings = new List<WarningReason>();

            if (collection["Attribute1"] != null)
            {
                int approvedCount = 0;
                int rejectedCount = 0;

                int index = 1;
                List<TextApprovalAttributeUpdate> attributeUpdates = new List<TextApprovalAttributeUpdate>();

                bool isvalid = true;
                while (collection["Attribute"+ index.ToString()] != null)
                {
                    //Check if either good or bad option is selected on the Aprroval status radio button
                    if (collection["TextAcceptable" + index.ToString()] == null)
                    {
                        isvalid = false;
                        ModelState.AddModelError("TextApprovalStatus" + index.ToString(), "*");
                        index++;
                        continue;
                    }

                    string attributeName = collection["AttributeName" + index.ToString()];
                    string value = collection["Attribute" + index.ToString()];
                    string originalValue = collection["OriginalValue" + index.ToString()];
                    int attributeGroupID = Convert.ToInt32(collection["AttributeGroupID" + index.ToString()]);
                    int attributeLanguageID = Convert.ToInt32(collection["LanguageID" + index.ToString()]);
                    bool textAcceptable = Convert.ToBoolean(collection["TextAcceptable" + index.ToString()]);

                    bool attributeUnapproved = true;

                    if (approvalType == RegularApprovalType.MemberApproval)
                    {
                        TextStatusType attributeStatus = (TextStatusType)Convert.ToInt32(collection["AttributeStatus" + index.ToString()]);
                        if (attributeStatus != TextStatusType.None && attributeStatus != TextStatusType.Pending)
                        {
                            attributeUnapproved = false;
                        }
                    }
                    
                    int statusMask;
                    if(textAcceptable) 
                    {
                        statusMask = (int) IndividualTextApprovalStatusMask.Approve;
                        approvedCount++;
                    }
                    else
                    {
                        statusMask = GetTextApprovalStatusMask(collection["TextUnacceptableReasons" + index.ToString()]);
                        rejectedCount++;
                    }
                    
                    int notNullProfileRegionID, notNullGenderMask, notNullMaritalStatus, notNullEducationLevel, notNullReligion, notNullEthnicity, notNullDaysSinceFirstSubscription;

                    notNullProfileRegionID = profileRegionID == null ? Constants.NULL_INT : (int)profileRegionID;
                    notNullGenderMask = genderMask == null ? Constants.NULL_INT : (int)genderMask;
                    notNullMaritalStatus = maritalStatus == null ? Constants.NULL_INT : (int)maritalStatus;
                    notNullEducationLevel = educationLevel == null ? Constants.NULL_INT : (int)educationLevel;
                    notNullReligion = religion == null ? Constants.NULL_INT : (int)religion;
                    notNullEthnicity = ethnicity == null ? Constants.NULL_INT : (int)ethnicity;
                    notNullDaysSinceFirstSubscription = daysSinceFirstSubscription == null ? Constants.NULL_INT : (int)daysSinceFirstSubscription;

                    if(registrationDate != null)
                        attributeUpdates.Add(new TextApprovalAttributeUpdate(attributeName, attributeGroupID, originalValue, value, textAcceptable, statusMask, attributeLanguageID, attributeUnapproved, ip, notNullProfileRegionID, notNullGenderMask, notNullMaritalStatus, firstName, lastName, email, occupationDescription, notNullEducationLevel, notNullReligion, notNullEthnicity, registrationDate.Value, subscriberStatus, notNullDaysSinceFirstSubscription, billingPhoneNumber));
                    else
                        attributeUpdates.Add(new TextApprovalAttributeUpdate(attributeName, attributeGroupID, originalValue, value, textAcceptable, statusMask, attributeLanguageID, attributeUnapproved, ip, notNullProfileRegionID, notNullGenderMask, notNullMaritalStatus, firstName, lastName, email, occupationDescription, notNullEducationLevel, notNullReligion, notNullEthnicity)); 
                    
                    index++;
                }

                //Determine the warnings selection.
                if (approvalType == RegularApprovalType.QueuedApproval)
                {
                    var enumNames = Enum.GetNames(typeof (WarningReason));
                    for (int i = 0; i < enumNames.Count(); i++)
                    {
                        var selected = collection["Warn" + enumNames[i]] != null && collection["Warn" + enumNames[i]].ToLower().Contains("true");
                        if (selected)
                        {
                            warnings.Add((WarningReason) Enum.Parse(typeof (WarningReason), enumNames[i]));
                        }
                    }
                }

                //If not all radio buttons are selected with atleast one option, return error
                //Also check to see if at least 1 warning reason was selected if there was at least 1 "bad" fta (this is the queue approval page only and does not apply if it's a suspend request).
                if (!isvalid
                        || (approvalType == RegularApprovalType.QueuedApproval && !suspend && (rejectedCount > 0 && warnings.Count == 0)))
                {
                    if (approvalType == RegularApprovalType.MemberApproval)
                    {
                        SiteIDs siteIds = (SiteIDs) Enum.Parse(typeof (SiteIDs), siteID.Value.ToString());
                        CommunityIDs communityIDs = GeneralHelper.GetCommunityIDForSiteID(siteIds);
                        TextApprovalManager manager = new TextApprovalManager(TextType.Regular, memberID, communityIDs);
                        TextApprovalMemberModelView modelView = manager.GetMemberApproval(siteIds);
                        return View("ViewTextByMember", modelView);
                    }
                    else
                    {
                        TextApprovalManager manager = new TextApprovalManager(languageID, TextType.Regular);
                        //Generate ModelView from existing data. Do not grab next item from the queue
                        TextApprovalModelView modelView = manager.GenerateModelView(memberID, communityID,languageID,memberTextApprovalId);
                        return View("ViewTextByLanguage", modelView);
                    }
                }

                TextApprovalManager approvalManager = new TextApprovalManager(memberID, languageID, (CommunityIDs)communityID);

                if (suspend)
                {
                    AdminActionReasonID adminActionReasonID = (AdminActionReasonID)Convert.ToInt32(collection["AdminActionReasonID"]);
                    approvalManager.CompleteTextApprovalWithSuspend(attributeUpdates, adminActionReasonID, memberTextApprovalId);
                }
                else
                {
                    if (approvalType == RegularApprovalType.QueuedApproval)
                        approvalManager.CompleteTextApproval(attributeUpdates, memberTextApprovalId);
                    else
                        approvalManager.CompleteTextApprovalMemberApproval(attributeUpdates, languageID, communityID);

                    // if at least one free text attribute has been approved, let's update the member attribute
                    if (approvedCount > 0)
                    {
                        Matchnet.Member.ServiceAdapters.Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);
                        if (member != null)
                        {
                            int ftaApproved = member.GetAttributeInt(communityID, Constants.NULL_INT, Constants.NULL_INT, "FTAApproved", Constants.NULL_INT);

                            // enable this member in our search now if we went from 0 to 1
                            if (ftaApproved != 1)
                            {
                                member.SetAttributeInt(communityID, Constants.NULL_INT, Constants.NULL_INT, "FTAApproved", 1);
                                Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
                                MemberSA.Instance.UpdateSearch(member.MemberID, communityID);
                            }
                        }

                    }
                }
            }

            // send the communication warning email to the member and log the warning count(s)
            // before we do that, let's remove the NoWarning enum value
            warnings.Remove(WarningReason.NoWarning);
            if(warnings.Count > 0)
            {
                // log to a new table location
                // we need to obtain the siteID to increment the warning counter
                var member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);
                int brandId = Constants.NULL_INT;
                member.GetLastLogonDate(communityID, out brandId);
                var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandId);

                if (brand != null)
                {
                    // queue up the external mail impulse
                    var resoureFilePath = GeneralHelper.DetermineResourceFileName(brand);
                    var resourceBag = GeneralHelper.GetResourceCollection(resoureFilePath);
                    List<string> warningDesc = new List<string>();
                    foreach (WarningReason wr in warnings)
                    {
                        warningDesc.Add(GeneralHelper.GetResource(resourceBag,
                                                                  "WarningReason" +
                                                                  Enum.GetName(typeof (WarningReason), wr)));
                    }
                    Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendFTAWarnings(memberID, brandId,
                                                                                                  warningDesc.ToArray());

                    // increment our counter here
                    int[] warningsValues = warnings.Cast<int>().ToArray();
                    MemberSA.Instance.IncrementCommunicationCounter(memberID, brand.Site.SiteID, warningsValues);
                }
            }

            // process admin notes
            if (adminNotes != null && adminNotes != string.Empty)
            {
                Matchnet.Content.ServiceAdapters.AdminSA.Instance.UpdateAdminNote(adminNotes.Replace("<p>", string.Empty).Replace("</p>", string.Empty),
                    memberID, g.AdminID, communityID, Matchnet.Lib.ConstantsTemp.ADMIN_NOTE_MAX_LENGTH);
            }

            if (approvalType == RegularApprovalType.QueuedApproval)
            {
                // Check for the PossibleFraud checkbox value
                if (collection["PossibleFraud"] != null && collection["PossibleFraud"].Contains("true"))
                {
                    var sID = siteID.HasValue
                                  ? siteID.Value
                                  : MemberHelper.GetMemberLastLogonSite(MemberHelper.GetMember(memberID, MemberLoadFlags.None));

                    var queueItemMember = new QueueItemMember(communityID, sID, Constants.NULL_INT, memberID,
                                                              g.AdminID);
                    DBApproveQueueSA.Instance.Enqueue(queueItemMember);
                }

                return RedirectToAction("ViewTextByLanguage", new { ID = languageID});
            }
            else
            {
                return RedirectToAction("View", "Member", new { siteID = siteID.Value, memberID = memberID });
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CompleteFacebookLikeApproval(FormCollection collection, QAApprovalType approvalType,
            int memberID, int communityID, int siteID, bool suspend, string FBLikesSubmitData, bool FBLikesRemoveAll, int FBLikesDisplayedCount, string AdminNotes)
        {
            bool isReady = true;
            List<FacebookLikeParams> facebookLikeParamsList = new List<FacebookLikeParams>();

            if (isReady)
            {
                System.Diagnostics.Trace.WriteLine("CompleteFacebookLikeApproval - approvalType: " + approvalType.ToString() + ", FBLikesSubmitData: " + FBLikesSubmitData);
                string[] fbLikesList = !string.IsNullOrEmpty(FBLikesSubmitData) ? FBLikesSubmitData.Split(new string[] { "sparkobj&&" }, StringSplitOptions.RemoveEmptyEntries) : null;
                if (fbLikesList != null && fbLikesList.Length > 0)
                {
                    foreach (string fbLike in fbLikesList)
                    {
                        FacebookLikeParams facebookLikeParams = new FacebookLikeParams();
                        facebookLikeParams.MemberId = memberID;
                        facebookLikeParams.SiteId = siteID;

                        string[] fbLikeParams = fbLike.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string fbLikeParam in fbLikeParams)
                        {
                            string[] param = fbLikeParam.Split(new string[] { "==" }, StringSplitOptions.RemoveEmptyEntries);
                            if (param.Length == 2)
                            {
                                switch (param[0].ToLower())
                                {
                                    case "id":
                                        facebookLikeParams.FacebookId = Convert.ToInt64(param[1]);
                                        break;
                                    case "name":
                                        facebookLikeParams.FbName = param[1];
                                        break;
                                    case "categoryid":
                                        facebookLikeParams.FbLikeCategoryId = Convert.ToInt32(param[1]);
                                        break;
                                    case "categorygroupid":
                                        break;
                                    case "picture":
                                        facebookLikeParams.FbImageUrl = param[1];
                                        break;
                                    case "link":
                                        facebookLikeParams.FbLinkHref = param[1];
                                        break;
                                    case "type":
                                        facebookLikeParams.FbType = param[1];
                                        break;
                                    case "approved":
                                        if (param[1] == "true")
                                        {
                                            facebookLikeParams.FbLikeStatus = Spark.FacebookLike.ValueObjects.ServiceDefinitions.FacebookLikeStatus.Approved;
                                        }
                                        else
                                        {
                                            facebookLikeParams.FbLikeStatus = Spark.FacebookLike.ValueObjects.ServiceDefinitions.FacebookLikeStatus.Rejected;
                                        }
                                        break;
                                }
                            }
                        }

                        if (facebookLikeParams.FacebookId > 0)
                        {
                            facebookLikeParamsList.Add(facebookLikeParams);
                        }
                    }
                }

                if (facebookLikeParamsList.Count > 0 || (approvalType == QAApprovalType.MemberApproval && FBLikesRemoveAll && facebookLikeParamsList.Count == 0) || suspend)
                {
                    ApprovalManager approvalManager = new ApprovalManager();

                    if (suspend)
                    {
                        AdminActionReasonID adminActionReasonID = (AdminActionReasonID)Convert.ToInt32(collection["AdminActionReasonID"]);
                        approvalManager.CompleteFacebookLikesApproval(facebookLikeParamsList, memberID, (CommunityIDs)communityID, (SiteIDs)siteID, approvalType, FBLikesDisplayedCount, true, adminActionReasonID);
                    }
                    else
                    {
                        approvalManager.CompleteFacebookLikesApproval(facebookLikeParamsList, memberID, (CommunityIDs)communityID, (SiteIDs)siteID, approvalType, FBLikesDisplayedCount, false, AdminActionReasonID.None);
                    }
                }

                // process admin notes
                if (!string.IsNullOrEmpty(AdminNotes))
                {
                    Matchnet.Content.ServiceAdapters.AdminSA.Instance.UpdateAdminNote(AdminNotes.Replace("<p>", string.Empty).Replace("</p>", string.Empty),
                        memberID, g.AdminID, communityID, Matchnet.Lib.ConstantsTemp.ADMIN_NOTE_MAX_LENGTH);
                }
            }

            switch (approvalType)
            {
                case QAApprovalType.QueuedApprovalByCommunity:
                    return RedirectToAction("ViewFacebookLikesByCommunity", new { ID = communityID });
                case QAApprovalType.QueuedApprovalBySite:
                    return RedirectToAction("ViewFacebookLikesBySite", new { ID = siteID });
                default:
                    return RedirectToAction("View", "Member", new { siteID = siteID, memberID = memberID });
            }
        }
        private int GetTextApprovalStatusMask(string rawFormValue)
        {
            int statusMask = 0;

            string[] statuses = rawFormValue.Split(',');

            foreach (string status in statuses)
            {
                statusMask = statusMask + Convert.ToInt32(status);
            }

            return statusMask;
        }
    }
}
