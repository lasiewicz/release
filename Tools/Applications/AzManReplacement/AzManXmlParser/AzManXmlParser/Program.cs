﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzManXmlParser
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 3)
                Console.WriteLine("Usage: AzManXmlParser inputFileName appName outputFileName");
            
            var inputFileName = args[0]; 
            var appName = args[1];
            var outputFileName = args[2];                
                        
            var res = FileProcessor.Process(inputFileName, appName);
            System.IO.File.WriteAllText(outputFileName,res);
            Console.WriteLine(res);
            Console.ReadLine();
        }
    }
}
