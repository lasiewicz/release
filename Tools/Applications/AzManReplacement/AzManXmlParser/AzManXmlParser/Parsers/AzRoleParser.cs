﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AzManXmlParser.ValueObjects;

namespace AzManXmlParser.Parsers
{
    class AzRoleParser
    {

        public static AzRole Parse(XmlNode node)
        {
            if (node == null || node.Attributes == null)
                return null;

            var ret = new AzRole
            {
                Guid = node.Attributes["Guid"].Value,
                Name = node.Attributes["Name"].Value,
                Description = node.Attributes["Description"].Value,
                Members = new List<string>()
            };
            ProcessChildren(ret, node);
            return ret;
        }

        static void ProcessChildren(AzRole target, XmlNode node)
        {
            foreach (XmlNode childNode in node.ChildNodes)
            {
                switch (childNode.Name)
                {
                    case "Member":
                        target.Members.Add(childNode.InnerText);
                        break;
                    case "TaskLink":
                        if (target.TaskLink != null)
                            throw new Exception("Multiple task links from roles. Was: " + target.TaskLink + " Attempted to assing: " + childNode.InnerText);
                        target.TaskLink  = childNode.InnerText;
                        break;
                    default:
                        Console.WriteLine("Unexpected tag " + childNode.Name + " in " + node.Name);
                        break;
                }
            }
        }
    }
}
