﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AzManXmlParser.ValueObjects;

namespace AzManXmlParser.Parsers
{
    class AzAdminManagerParser
    {
        public static AzAdminManager Parse(XmlNode node)
        {
            var ret = new AzAdminManager();
            ret.Guid = node.Attributes["Guid"].Value;
            ret.Description = node.Attributes["Description"].Value;
            ret.AzApplications = new List<AzApplication>();
            ProcessChildren(ret, node);
            return ret;
        }

        static void ProcessChildren(AzAdminManager target, XmlNode node)
        {
            foreach (XmlNode childNode in node.ChildNodes)
            {
                switch (childNode.Name)
                {
                    case "AzApplication":
                        target.AzApplications.Add(AzApplicationParser.Parse(childNode));
                        break;
                    default:
                        Console.WriteLine("Unexpected tag " + childNode.Name + " in " + node.Name);
                        break;
                }
            }
        }
    }
}
