﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AzManXmlParser.ValueObjects;

namespace AzManXmlParser.Parsers
{
    class AzApplicationParser
    {
        public static AzApplication Parse(XmlNode node)
        {
            var ret = new AzApplication();
            ret.Guid = node.Attributes["Guid"].Value;
            ret.Name = node.Attributes["Name"].Value;
            ret.Description = node.Attributes["Description"].Value;
            ret.AzOperations = new List<AzOperation>();
            ret.AzTasks = new List<AzTask>();
            ret.AzRoles = new List<AzRole>();
            ProcessChildren(ret, node);
            return ret;
        }

        static void ProcessChildren(AzApplication target, XmlNode node)
        {
            foreach (XmlNode childNode in node.ChildNodes)
            {
                switch (childNode.Name)
                {
                    case "AzOperation":
                        target.AzOperations.Add(AzOperationParser.Parse(childNode));
                        break;
                    case "AzTask":
                        target.AzTasks.Add(AzTaskParser.Parse(childNode));
                        break;
                    case "AzRole":
                        target.AzRoles.Add(AzRoleParser.Parse(childNode));
                        break;
                    default:
                        Console.WriteLine("Unexpected tag " + childNode.Name + " in " + node.Name);
                        break;
                }
            }
        }
    }
}
