﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AzManXmlParser.ValueObjects;

namespace AzManXmlParser.Parsers
{
    class AzTaskParser
    {

        public static AzTask Parse(XmlNode node)
        {
            var ret = new AzTask();
            ret.Guid = node.Attributes["Guid"].Value;
            ret.Name = node.Attributes["Name"].Value;
            ret.RoleDefinition = node.Attributes["RoleDefinition"].Value == "True" ? true : false;
            ret.Description = node.Attributes["Description"].Value;
            ret.OperationLinks = new List<string>();
            ret.TaskLinks = new List<string>();
            ProcessChildren(ret, node);
            return ret;
        }

        static void ProcessChildren(AzTask target, XmlNode node)
        {
            foreach (XmlNode childNode in node.ChildNodes)
            {
                switch (childNode.Name) 
                {
                    case "OperationLink":
                        target.OperationLinks.Add(childNode.InnerText);
                        break;
                    case "TaskLink":
                        target.TaskLinks.Add(childNode.InnerText);
                        break;
                    default:
                        Console.WriteLine("Unexpected tag " + childNode.Name + " in " + node.Name);
                        break;
                }
            }
        }
    }
}
