﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AzManXmlParser.ValueObjects;

namespace AzManXmlParser.Parsers
{
    class AzOperationParser
    {

        public static AzOperation Parse(XmlNode node)
        {
            var ret = new AzOperation();
            ret.Guid = node.Attributes["Guid"].Value;
            ret.Name = node.Attributes["Name"].Value;
            ret.Description = node.Attributes["Description"].Value;
            ProcessChildren(ret, node);
            return ret;
        }

        static void ProcessChildren(AzOperation target, XmlNode node)
        {
            foreach (XmlNode childNode in node.ChildNodes)
            {
                switch (childNode.Name)
                {
                    case "OperationID":
                        target.OperationId = int.Parse(childNode.InnerText);
                        break;
                    default:
                        Console.WriteLine("Unexpected tag " + childNode.Name + " in " + node.Name);
                        break;
                }
            }
        }
    }
}
