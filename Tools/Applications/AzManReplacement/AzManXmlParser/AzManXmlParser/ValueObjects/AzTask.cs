﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzManXmlParser.ValueObjects
{
    class AzTask:AzObject
    {
        public bool RoleDefinition { get; set; }
        public List<string> OperationLinks { get; set; }
        public List<string> TaskLinks { get; set; }
    }
}
