﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzManXmlParser.ValueObjects
{
    class AzApplication:AzObject
    {
        public List<AzOperation> AzOperations { get; set; }
        public List<AzTask> AzTasks { get; set; }
        public List<AzRole> AzRoles { get; set; } 
    }
}
