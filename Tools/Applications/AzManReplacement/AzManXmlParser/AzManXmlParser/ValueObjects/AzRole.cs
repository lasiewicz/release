﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzManXmlParser.ValueObjects
{
    class AzRole:AzObject
    {
        public List<string> Members { get; set; }
        public string TaskLink { get; set; } 
    }
}
