﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AzManXmlParser.ValueObjects
{
    class AzObject
    {
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
