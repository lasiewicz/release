﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzManXmlParser.Parsers;
using AzManXmlParser.ValueObjects;

namespace AzManXmlParser
{
    class SqlGenerator
    {
        public static string Generate(AzAdminManager root, string appName)
        {
            return Generate(root.AzApplications.FirstOrDefault(app => app.Name == appName));
        }

        public static string INITIAL_DATA_SEED = Quote("InitialDataSeed");

        static string Generate(AzApplication root)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("use mnInternalSecurity");
            GenerateStoredProcedures(stringBuilder);

            stringBuilder.AppendLine("BEGIN TRY");
            stringBuilder.AppendLine("BEGIN TRANSACTION");
            GeneratePrivileges(root, stringBuilder);

            GenerateRoles(root, stringBuilder);
            GenerateRolesInRoles(root, stringBuilder);

            GenerateRole2AdObjects(root,stringBuilder);

            GenerateRolePrivileges(root, stringBuilder);
            stringBuilder.AppendLine("COMMIT");
            stringBuilder.AppendLine("END TRY");
            stringBuilder.AppendLine("BEGIN CATCH");
            stringBuilder.AppendLine("IF @@TRANCOUNT > 0");
            stringBuilder.AppendLine("ROLLBACK");
            stringBuilder.AppendLine("END CATCH");
            return stringBuilder.ToString();
        }




        static string Quote(string quote)
        {
            return "'" + quote.Replace("'","''") + "'";
        }

        static void GeneratePrivileges(AzApplication root, StringBuilder builder)
        {
            foreach (var operation in root.AzOperations)
            {
                var command = string.Format("EXEC #up_Import_Privilege_add {0}, {1}", operation.OperationId, Quote(operation.Name));
                builder.AppendLine(command);
            }
        }

        static void GenerateStoredProcedures(StringBuilder builder)
        {
            var ret = @"
if OBJECT_ID('tempdb..#up_Import_Privilege_add') is not null
	drop procedure #up_Import_Privilege_add
go
create procedure #up_Import_Privilege_add @operationId int, @name varchar(255)
as 
begin
    declare @now datetime
    declare @who varchar(255)
    set @now = getdate()
    set @who = 'initial_seed'   

    if not exists(select 1 from Privilege where PrivilegeID = @operationId)
        insert Privilege(PrivilegeID,Active,AppDomainID,[Action],[Resource],UpdatedBy,UpdateDate,InsertedBy,InsertDate) 
        values(@operationId,1,1, @name,'AdminTool', @who,@now,@who,@now)
    else
        update Privilege
            set [Action] = @name where PrivilegeID = @operationId
end            
go
--drop table RoleGuidMapping
--create table RoleGuidMapping(id int unique not null, [guid] varchar(255) unique not null)


if OBJECT_ID('tempdb..#up_Import_Role_add') is not null
	drop procedure #up_Import_Role_add
go
create procedure #up_Import_Role_add @guid varchar(255), @name varchar(255)
as
begin
    declare @now datetime
    declare @who varchar(255)
    set @now = getdate()
    set @who = 'initial_seed'   
    declare @id int

    select @id = id from RoleGuidMapping where [guid] = @guid
    if @id is null
		select @id = Max(RoleId) + 1 from [Role]
	if @id is null
		set @id = 1

    if not exists(select 1 from RoleGuidMapping where @guid = [guid])
        insert into RoleGuidMapping(id,[guid]) values(@id,@guid)

    if not exists(select 1 from [Role] where RoleID = @id)
        insert [Role](RoleID,Active, RoleName, AppDomainID,UpdatedBy,UpdateDate,InsertedBy,InsertDate) 
        values(@id,1,@name, 1, @who,@now,@who,@now)
    else
        update [Role]
            set RoleName = @name where RoleID= @id
end
go
if OBJECT_ID('tempdb..#up_Import_Role_to_Role_add') is not null
	drop procedure #up_Import_Role_to_Role_add
go
create procedure #up_Import_Role_to_Role_add @guidParent varchar(255), @guidChild varchar(255)
as 
begin
    declare @now datetime
    declare @who varchar(255)
    set @now = getdate()
    set @who = 'initial_seed'   

	declare @parentId int
	declare @childId int
	select @parentId = id from RoleGuidMapping where [guid] = @guidParent
	select @childId = id from RoleGuidMapping where [guid] = @guidChild
	
    if @parentId is null
		raiserror (1000,-1,-1, 'parentId is not found' )
		
	if @parentId is null
		raiserror (1000,-1,-1,'childId is not found' )
	
	if not exists(select 1 from RoleHierarchy where ParentRoleID = @parentId and ChildRoleID = @childId)
		insert into RoleHierarchy(ParentRoleID,ChildRoleID,InsertedBy,InsertDate)
		values(@parentId,@childId,@who,@now)	
end	
go
if OBJECT_ID('tempdb..#up_Import_Role_to_AD_Add') is not null
	drop procedure #up_Import_Role_to_AD_Add
go
create procedure #up_Import_Role_to_AD_Add @guid varchar(255), @objectSid varchar(255)
as 
	begin
	declare @now datetime
    declare @who varchar(255)
    set @now = getdate()
    set @who = 'initial_seed'   

	declare @id int
	select @id = id from RoleGuidMapping where [guid] = @guid
	if @id is null
		raiserror (1000, -1,-1, 'role Id is not found')
		
	if not exists(select 1 from RoleActiveDirectory where RoleID = @id and ObjectSid = @objectSid)
		insert into RoleActiveDirectory(RoleID,ObjectSid,InsertedBy,InsertDate)
		values(@id,@objectSid,@who,@now)	
end
go
if OBJECT_ID('tempdb..#up_Import_Role_Privilege_Add') is not null
	drop procedure #up_Import_Role_Privilege_Add
go
create procedure #up_Import_Role_Privilege_Add @guid varchar(255), @operationId int
as 
	begin
	    declare @now datetime
    declare @who varchar(255)
    set @now = getdate()
    set @who = 'initial_seed'   

	declare @id int
	select @id = id from RoleGuidMapping where [guid] = @guid
	if @id is null
		raiserror (1000, -1, -1, 'role Id is not found')
		
	if not exists(select 1 from RolePrivilege where RoleID = @id and PrivilegeID = @operationId)
		insert into RolePrivilege(RoleID,PrivilegeID,InsertedBy,InsertDate)
		values(@id,@operationId,@who,@now)	
end
go

";
            builder.Append(ret);
        }


        static void GenerateRoles(AzApplication root, StringBuilder builder)
        {
            foreach (var role in root.AzRoles)
            {
                var command = string.Format("EXEC #up_Import_Role_add {0}, {1}", Quote(role.Guid), Quote(role.Name));
                builder.AppendLine(command);
            }
        }

        static void GenerateRolesInRoles(AzApplication root, StringBuilder builder)
        {
            foreach (var role in root.AzRoles)
            {
                
                var task = root.AzTasks.FirstOrDefault(x => x.Guid == role.TaskLink);
                if (task != null)
                {
                    foreach (var childTaskStr in task.TaskLinks)
                    {
                        
                        var childRole = root.AzRoles.FirstOrDefault(x => x.TaskLink == childTaskStr);
                        if (childRole != null)
                        {
                            var command = string.Format("EXEC #up_Import_Role_to_Role_add {0}, {1}", Quote(role.Guid),
                                Quote(childRole.Guid));
                            builder.AppendLine(command);
                        }
                    }
                }
            }            
        }

        static void GenerateRole2AdObjects(AzApplication root, StringBuilder builder)
        {
            foreach (var role in root.AzRoles)
            {
                foreach (var member in role.Members)
                {
                    if(member == null)
                        throw  new Exception("member cannot be null. role: " + role.Guid);
                    var command = string.Format("EXEC #up_Import_Role_to_AD_Add {0}, {1}", Quote(role.Guid), Quote(member));
                    builder.AppendLine(command);
                }
            }
        }

        static void GenerateRolePrivileges(AzApplication root, StringBuilder builder)
        {
            foreach (var role in root.AzRoles)
            {
                var task = root.AzTasks.FirstOrDefault(x => role.TaskLink == x.Guid);
                if (task != null)
                {
                    foreach (var operationStr in task.OperationLinks)
                    {
                        var operation = root.AzOperations.FirstOrDefault(x => x.Guid == operationStr);
                        if (operation != null)
                        {
                            var command = string.Format("EXEC #up_Import_Role_Privilege_Add {0}, {1}", Quote(role.Guid),
                                operation.OperationId);
                            builder.AppendLine(command);
                        }
                        else
                        {
                            throw new Exception("Operation cannot be null. role: " + role.Guid + " operationStr: " + operationStr);
                        }

                    }
                }
                else
                {
                    throw  new Exception("Task cannot be null. role: " + role.Guid );
                }
            }
        }

    }
}
