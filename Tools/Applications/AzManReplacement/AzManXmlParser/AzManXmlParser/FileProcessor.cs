﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AzManXmlParser.Parsers;

namespace AzManXmlParser
{
    internal class FileProcessor
    {

        public static string Process(string fileName, string appName)
        {
            var doc = new XmlDocument();
            doc.Load(fileName);
            return SqlGenerator.Generate(AzAdminManagerParser.Parse(doc.ChildNodes[1]), appName);
        }
    }
}
