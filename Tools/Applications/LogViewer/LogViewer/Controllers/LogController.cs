﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;

using LogViewer.Models.ModelViews;
using Spark.Monitoring.ValueObjects;
using Spark.CommonLibrary.Logging;

namespace LogViewer.Controllers
{
    public class LogController : Controller
    {
        public const int PageSize = 50;
        public Logger logger = new Logger(typeof(LogController));
        //
        // GET: /QueryLog/

        public ActionResult Index()
        {
            try
            {
                if (!String.IsNullOrEmpty(Request.QueryString["page"]))
                {
                    return Index(new FormCollection(Request.QueryString));
                }
                return View(new QueryResult());
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Log Viewer error: {0} ", ex.Message), ex);
                return View("Error");
            }
        }

        /// <summary>
        /// Return query results
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(FormCollection formData)
        {
            try
            {
                int startRow = 1; int pagenumber = 1;
                if (!string.IsNullOrEmpty(formData["page"]))
                {
                    int.TryParse(formData["page"], out pagenumber);
                    startRow = PageSize * (pagenumber - 1) + 1;
                }

                string machineName = formData["MachineName"] != "Select" ? formData["MachineName"] : "";
                string source = formData["Source"] != "Select" ? formData["Source"] : "";
                string category = formData["Category"] != "Select" ? formData["Category"] : "";
                string entryType = formData["Type"] != "Select" ? formData["Type"] : "";
                DateTime? startDate = null, endDate = null;

                startDate = ValidateDate(formData["StartDate"], formData["StartTime"],"StartDate");
                endDate = ValidateDate(formData["EndDate"], formData["EndTime"],"EndDate");
                if(string.IsNullOrEmpty(formData["EndTime"]) && endDate.HasValue)
                    endDate = new DateTime(endDate.Value.Year, endDate.Value.Month, endDate.Value.Day, 23, 59, 59);
                
                string message = Server.HtmlEncode(!string.IsNullOrEmpty(formData["Message"]) ? formData["Message"].Trim() : "");

                QueryResult results = new QueryResult();
                if (ModelState.IsValid)
                {
                    SearchQuery query = new SearchQuery(machineName,
                                                       category,
                                                       source,
                                                       entryType,
                                                       message,
                                                       startDate,
                                                       endDate,
                                                       startRow,
                                                       PageSize);
                    results.PopulateData(query);
                    results.QueryLog(query);
                    if (pagenumber == 1)
                        results.PageNumber = 1;
                }
                else//Invlaid inputs
                    results.PopulateData(formData);
                
                return View(results);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Log Viewer error: {0} ",ex.Message), ex);
                return View("Error");
            }
        }

        /// <summary>
        /// Method to validate date and time
        /// </summary>
        /// <param name="date"></param>
        /// <param name="time"></param>
        /// <param name="key"></param>
        /// <returns>Valid date or null</returns>
        private DateTime? ValidateDate(string date, string time, string key)
        {
            if (!string.IsNullOrEmpty(date))
            {
                DateTime validDate;
                if (!DateTime.TryParse(date, out validDate))
                {
                    ModelState.AddModelError(key, "Invalid date");
                    return null;
                }
                else
                {
                    if (validDate.Year < 1753 || validDate.Year > 9999) // Avoid SQL Overflow exception
                    {
                        ModelState.AddModelError(key, "Invalid date");
                        return null;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(time))
                        {
                            //Validate time
                            if (!System.Text.RegularExpressions.Regex.IsMatch(time, "^(?:0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"))
                            {
                                ModelState.AddModelError(key, "Invalid time");
                                return null;
                            }
                            else
                            {
                                string[] timeHHMM = time.Split(':');
                                int hour = 0, minute = 0, second = 0;
                                if (timeHHMM != null && timeHHMM.Length > 1)
                                {
                                    Int32.TryParse(timeHHMM[0], out hour);
                                    Int32.TryParse(timeHHMM[1], out minute);
                                }
                                validDate = new DateTime(validDate.Year, validDate.Month, validDate.Day, hour, minute, second);
                            }
                        }
                    }
                }
                return validDate;   
            }
            return null;
        }

    }
}
