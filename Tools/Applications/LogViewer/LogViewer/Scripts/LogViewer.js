﻿
$(document).ready(function() {
    $('#submitButton').focus();

    //Bind enter key press event to submit button
    $('body').keypress(function(e) {

        if (e.which == 13) {
            var target = $(e.target);
            if (target.attr('id') == 'Page') {
                //Validate page number
                var page = $('#Page').val();
                if (page == '' || isNaN(page)) {
                    $('.pageNumerror').css('display', 'block');
                    return false;
                }
                else {
                    var pageNum = parseInt($('#Page').val());
                    var totalPages = parseInt($('#TotalPages').val());
                    if (pageNum > totalPages) {
                        return false; // If pagenumber is greater than total pages, do not submit the form
                    }
                    else {
                        $('#submitQuery').click();
                    }
                }
                return false;
            }
            else {
                $('#submitButton').click();
            }
        }
    });

    //Bind click event to submit button
    $('#submitButton').click(function(e) {
        $('#Page').val("1");
        $('#submitQuery').click();
    });

    //Date picker
    $('#StartDate').datepicker();
    $('#EndDate').datepicker();

    //Time picker
    $('#StartTime').timepicker();
    $('#EndTime').timepicker();
});
