﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Spark.Monitoring.ServiceAdapters;
using Spark.Monitoring.ValueObjects;

namespace LogViewer
{
    public partial class LogViewerTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SearchQuery query = new SearchQuery("BHD-JRHO","","","","Logon",null,null,1,10);
            int totalrows = 0;
            DateTime? val = query.StartDate;
            //List<LogEntry> result = Spark.Monitoring.BusinessLogic.MonitoringBL.Instance.QueryEventLog(query, ref totalrows);
            List<LogEntry> result = MonitoringSA.Instance.QueryEventLog(query, ref totalrows);
        }
    }
}
