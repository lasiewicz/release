﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LogViewer.Models.ModelViews.QueryResult>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Query Log
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   
   <form id="queryform" runat="server">
   
    <div class="search">
        <ul>
            <li>
                <span>Machine Name: </span>
                <div><%= Html.DropDownList("MachineName")%></div>
            </li>
            <li>
                <span>Source: </span>
                <div><%= Html.DropDownList("Source")%></div>
            </li>
            <li>
                <span>Category: </span>
                <div><%= Html.DropDownList("Category")%></div>
            </li>
            <li>
                <span>Entry Type: </span>
                <div><%= Html.DropDownList("Type")%></div>
            </li>
            <li>
                <span>Start Date: </span>
                <div><%= Html.TextBox("StartDate", Model.StartDate)%>
                <%= Html.TextBox("StartTime", Model.StartTime)%>
                <%= Html.ValidationMessageFor(model => model.StartDate) %>
                </div>
            </li>
            <li>
                <span>End Date: </span>
                <div>
                    <%= Html.TextBox("EndDate", Model.EndDate)%>
                     <%= Html.TextBox("EndTime", Model.EndTime)%>
                    <%= Html.ValidationMessageFor(model => model.EndDate) %>
                </div>
            </li>
            <li>
                <span>Message: </span>
                <div><%= Html.TextBox("Message", Model.Message)%></div>
            </li>
        </ul>
        <%--<%= Html.Hidden("Page", "1")%>--%>
    </div>
    
    <div class="submit">
        <!-- Invisible submit button to do actual form submit. This is to differentiate between submit button click and enter key press. -->
        <input id="submitQuery" tabindex="0" type="submit" value=""/>
        <input id="submitButton" tabindex="0" type="button" value="Submit"/>
    </div>

    <% if (Model.Results.Count > 0)
       { %>
    <div class="pager">
        <% if (Model.TotalPages > 1)
           { %>
           <% if (Model.PrevPage == 0)
              { %>
                <a href='#' class="disable">Prev</a>
           <% }
              else
              { %>
                <a href='/Log?<%= Model.GetSearchQueryString(Model.PrevPage) %>' title='Page <%= Model.PrevPage %>'>Prev</a>
            <% } %>
             <div class="goto"> 
                Page: <%= Html.TextBox("Page", Model.PageNumber)%> of <%= Model.TotalPages %>
                
            </div>
            <% if (Model.NextPage > Model.TotalPages)
               { %>
               <a href='#' class="disable">Next</a>
            <% }
               else
               { %>
                <a href='/Log?<%= Model.GetSearchQueryString(Model.NextPage)%>' title='Page <%= Model.NextPage %>'>Next</a>
            <% } %>
            <span class="pageNumerror">Invalid page number</span>
        <% } %>
    </div>
    
    <div class="results">
        <ul>
            <li>
               <div>Entry Date</div>
                <div class="small">Machine Name</div>
                <div class="small">Source</div>
                <div class="small">Category</div>
                <div class="small">Entry Type</div>
                <div>Insert Date</div>
                <div class="msg">Message</div>
            </li>
            <%for (int i = 0; i < Model.Results.Count; i++ )
              {
                  if (i % 2 != 0)
                  { %> <li class="even"> <% }
                  else
                  { %>
                <li> <% } %>
                     <div><%= Model.Results[i].EntryDate%></div>
                    <div class="small"><%=Model.Results[i].MachineName%></div>
                    <div class="small"><%=Model.Results[i].Source%></div>
                    <div class="small"><%=Model.Results[i].Category%></div>
                    <div class="small"><%=Model.Results[i].EntryType%></div>
                    <div><%=Model.Results[i].RecordInsertDate%></div>
                    <div class="msg"><%=Model.Results[i].Message%></div>
                </li>
            <% } %>
        </ul>
    </div>
    
    <div class="pager">
        <% if (Model.TotalPages > 1)
           { %>
           <% if (Model.PrevPage == 0)
              { %>
                <a href='#' class="disable">Prev</a>
           <% }
              else
              { %>
                <a href='/Log?<%= Model.GetSearchQueryString(Model.PrevPage) %>' title='Page <%= Model.PrevPage %>'>Prev</a>
            <% } %>
            <% if (Model.NextPage > Model.TotalPages)
               { %>
               <a href='#' class="disable">Next</a>
            <% }
               else
               { %>
                <a href='/Log?<%= Model.GetSearchQueryString(Model.NextPage)%>' title='Page <%= Model.NextPage %>'>Next</a>
            <% } %>
            <div class="total">Page <%= Model.PageNumber%> of <%= Model.TotalPages %></div>
        <% } %>
        <%= Html.Hidden("TotalPages",Model.TotalPages) %>
    </div>
    
    <% } %>
    
</form>
</asp:Content>
