﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.Monitoring.ValueObjects;
using Spark.Monitoring.ServiceAdapters;

namespace LogViewer.Models.ModelViews
{
    public class QueryResult
    {
        public enum EntryType
        {
            Error, 
            FailureAudit, 
            Information, 
            SuccessAudit, 
            Warning
        }

        public const int PageSize = 50;
        public List<LogEntry> Results { get; set; }
       
        private int totalRows;
        public int TotalRows
        {
            get { return totalRows; }
            set { totalRows = value; }
        }

        public int TotalPages
        {
            get { return (totalRows % PageSize == 0) ? totalRows / PageSize : ((totalRows / PageSize) + 1); }
        }

        private int pageNumber;
        public int PageNumber
        {
            get 
            {
                if (pageNumber == 0) return 1;
                //pageNumber = (HttpContext.Current.Request.QueryString["page"] != null) ? Convert.ToInt32(HttpContext.Current.Request.QueryString["page"]) : 1; 
                return pageNumber; 
            }
            set { pageNumber = value; }
        }

        public int PrevPage
        {
            get { return PageNumber - 1; }
        }
        public int NextPage
        {
            get { return PageNumber + 1; }
        }

        /// <summary>
        /// Unique list of machine names
        /// </summary>
        private List<string> machineNameList;
        public List<SelectListItem> MachineName
        {
            get
            {
                List<SelectListItem> machines = new List<SelectListItem>();
                machines.Add(new SelectListItem() { Text = "Select", Value = "Select" });
                foreach (string machine in machineNameList)
                {
                    machines.Add(new SelectListItem() { Text = machine, Value = machine, Selected = (machine == MachineNameVal) });
                }
                return machines;
            }
        }

        /// <summary>
        /// Unique list of categories
        /// </summary>
        private List<string> categoryList;
        public List<SelectListItem> Category
        {
            get
            {
                List<SelectListItem> categories = new List<SelectListItem>();
                categories.Add(new SelectListItem() { Text = "Select", Value = "Select" });
                foreach (string category in categoryList)
                {
                    categories.Add(new SelectListItem() { Text = category, Value = category, Selected = (category == CategoryVal) });
                }
                return categories;
            }
        }

        /// <summary>
        /// Unique list of Source 
        /// </summary>
        private List<string> sourceList;
        public List<SelectListItem> Source
        {
            get
            {
                List<SelectListItem> sources = new List<SelectListItem>();
                sources.Add(new SelectListItem() { Text = "Select", Value = "Select" });
                foreach (string src in sourceList)
                {
                    sources.Add(new SelectListItem() { Text = src, Value = src, Selected = (src == SourceVal) });
                }
                return sources;
            }
        }

        /// <summary>
        /// ListItem collection of Entry type enum
        /// </summary>
        public List<SelectListItem> Type
        {
            get 
            {
                List<SelectListItem> types = new List<SelectListItem>();
                Array values = Enum.GetValues(typeof(EntryType));
                Array names = Enum.GetNames(typeof(EntryType));
                types.Add(new SelectListItem() { Text = "Select", Value = "Select" });
                for (int i = 0; i < values.Length; i++)
                {
                    types.Add(new SelectListItem() { Text = names.GetValue(i).ToString(), Value = values.GetValue(i).ToString(), Selected = (names.GetValue(i).ToString() == EntryTypeVal) });
                }
                return types;
            }
        }

        // Form input parameter properties
        public string MachineNameVal { get; set; }
        public string CategoryVal { get; set; }
        public string SourceVal { get; set; }
        public string EntryTypeVal { get; set; }
        public string Message { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string EndDate { get; set; }
        public string EndTime { get; set; }
        public int StartRow { get; set; }

        /// <summary>
        /// Constructor 
        /// </summary>
        public QueryResult()
        {
            Results = new List<LogEntry>();
            GetDistinctValues();
            //Set default startdate to one hour less than current datetime
            //StartDate = DateTime.Now.AddHours(-1).ToString("MM/dd/yyyy HH:mm:ss");
        }

        /// <summary>
        /// Method to call Spark.Monitoring service 
        /// </summary>
        /// <param name="inputquery">Search query</param>
        /// <returns>Search log results</returns>
        public List<LogEntry> QueryLog(SearchQuery inputquery)
        {
            Results = MonitoringSA.Instance.QueryEventLog(inputquery, ref totalRows);
            return Results;
        }

        /// <summary>
        /// Method to build querystring for 'Prev' and 'Next' links
        /// </summary>
        /// <param name="pagenumber"></param>
        /// <returns></returns>
        public string GetSearchQueryString(int pagenumber)
        {
            string qs = "page=" + pagenumber;
            qs = qs + ((!string.IsNullOrEmpty(MachineNameVal)) ? "&MachineName=" + HttpContext.Current.Server.UrlEncode(MachineNameVal) : "");
            qs = qs + ((!string.IsNullOrEmpty(CategoryVal)) ? "&Category=" + HttpContext.Current.Server.UrlEncode(CategoryVal) : "");
            qs = qs + ((!string.IsNullOrEmpty(SourceVal)) ? "&Source=" + HttpContext.Current.Server.UrlEncode(SourceVal) : "");
            qs = qs + ((!string.IsNullOrEmpty(EntryTypeVal)) ? "&Type=" + HttpContext.Current.Server.UrlEncode(EntryTypeVal) : "");
            qs = qs + ((!string.IsNullOrEmpty(Message)) ? "&Message=" + HttpContext.Current.Server.UrlEncode(Message) : "");
            qs = qs + ((!string.IsNullOrEmpty(StartDate)) ? "&StartDate=" + HttpContext.Current.Server.UrlEncode(StartDate.ToString()) : "");
            qs = qs + ((!string.IsNullOrEmpty(StartTime)) ? "&StartTime=" + HttpContext.Current.Server.UrlEncode(StartTime.ToString()) : "");
            qs = qs + ((!string.IsNullOrEmpty(EndDate)) ? "&EndDate=" + HttpContext.Current.Server.UrlEncode(EndDate.ToString()) : "");
            qs = qs + ((!string.IsNullOrEmpty(EndTime)) ? "&EndTime=" + HttpContext.Current.Server.UrlEncode(EndTime.ToString()) : "");
            return qs;
        }

        /// <summary>
        /// Method to populate Model state to retain input values 
        /// </summary>
        /// <param name="query"></param>
        public void PopulateData(SearchQuery query)
        {
            MachineNameVal = query.MachineName;
            CategoryVal = query.Category;
            SourceVal = query.Source;
            EntryTypeVal = query.EntryType;
            Message = HttpContext.Current.Server.HtmlDecode(query.Message);
            StartDate = (query.StartDate.HasValue) ? query.StartDate.Value.ToString("MM/dd/yyyy") : ""; //ToString("M/d/yyyy HH:mm:ss")
            EndDate = (query.EndDate.HasValue) ? query.EndDate.Value.ToString("MM/dd/yyyy") : "";
            PageNumber = ((query.StartRow - 1) / PageSize) + 1;
            StartTime = (query.StartDate.HasValue) ? query.StartDate.Value.ToString("HH:mm") : "";
            EndTime = (query.EndDate.HasValue) ? query.EndDate.Value.ToString("HH:mm") : "";
        }

        /// <summary>
        /// Method to populate Model state to retain input values 
        /// </summary>
        /// <param name="query"></param>
        public void PopulateData(FormCollection data)
        {
            MachineNameVal = data["MachineName"];
            CategoryVal = data["Category"];
            SourceVal = data["Source"];
            EntryTypeVal = data["Type"];
            Message = data["Message"];
            StartDate = data["StartDate"];
            EndDate = data["EndDate"];
            pageNumber = data["page"] != null ? Int32.Parse(data["Page"].ToString()) : 1;
            StartTime = data["StartTime"];
            EndTime = data["EndTime"];
        }

        /// <summary>
        /// Method to get uniques values of machine names, source and categories from DB
        /// </summary>
        private void GetDistinctValues()
        {
            Spark.Monitoring.ValueObjects.CachedDistinctValues value = MonitoringSA.Instance.GetCachedDistinctValues();
            machineNameList = value.MachineNames;
            sourceList = value.Source;
            categoryList = value.Category;
        }
    }
}
