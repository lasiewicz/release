﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LandingPageEngine.Models;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
//using Spark.CommonLibrary.Logging;
using log4net;
using System.Configuration;

namespace LandingPageEngine.Controllers
{
    /// <summary>
    /// Base Controller to allow for common initializations that may be needed later for all requests.
    /// </summary>
    public class BaseController : Controller
    {
        //public Logger _logger;
        private static readonly ILog Log = LogManager.GetLogger(typeof(BaseController));

        public BaseController()
        {
            //_logger = new Logger(this.GetType());
        }

        protected string GetHomePageForSite()
        {
            return @"http://www." + GetSiteURI();
        }

        protected string GetErrorPageForSite()
        {
            return @"http://www." + GetSiteURI() + "/Applications/Home/Error.aspx";
        }

        protected string GetSiteURI()
        {
            return HttpContext.Request.Url.Host.Substring(HttpContext.Request.Url.Host.IndexOf(".") + 1);
        }

        protected Brand GetBrandFromSite()
        {
            //Looking for '.' occurrences in host, to avoid 404 error when the url is 'http://jdate.com/LP/jdate-jewish-singles2'
            //Correct URL: 'http://www.jdate.com/LP/jdate-jewish-singles2'
            string host = HttpContext.Request.Url.Host;
            string uri = host.Substring(host.IndexOf(".") + 1); //host string/value must be <subdomain>.<domain>.<tld>, cannot use local.LandingPageEngineV2.jdate.com
            Brand brand = BrandConfigSA.Instance.GetBrandByURI(uri);
            if(brand == null)
            {
                brand = BrandConfigSA.Instance.GetBrandByURI(host);
            }
            return brand;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            //_logger.Error("Landing Page Engine error. ", filterContext.Exception);
            Log.Error("Landing Page Engine error. ", filterContext.Exception);

            //display friendly error view
            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                if (!ViewData.ContainsKey(Constants.ERROR_KEY))
                {
                    //if apis that threw error did not provide a error page friendly error message, we'll display the full exception
                    ViewData[Constants.ERROR_KEY] = ExceptionHelper.GetExceptionMessage(filterContext.Exception, ExceptionHelper.ExceptionMode.Html);
                }
                filterContext.HttpContext.Response.Clear();
                filterContext.ExceptionHandled = true;
                filterContext.Result = this.Redirect(GetErrorPageForSite());
            }

        }
    }
}
