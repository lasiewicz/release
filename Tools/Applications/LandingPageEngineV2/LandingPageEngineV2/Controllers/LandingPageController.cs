﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LandingPageEngine.Models;
using LandingPageEngine.Models.ModelViews;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.LandingPage;
using log4net;
namespace LandingPageEngine.Controllers
{
    public class LandingPageController : BaseController
    {
        //
        // GET: /LandingPage/
        private static readonly ILog Log = LogManager.GetLogger(typeof(BaseController));

        public ActionResult Index(string url)
        {
            string uri = base.GetHomePageForSite();
            LandingPageService service = new LandingPageService();
            TemplateBasedLandingPage page = service.GetLandingPageByURL(url);

            if (page == null)
            {
                return Redirect(base.GetHomePageForSite());
            }

            Brand brand = GetBrandFromSite();
            if (!page.Sites.Contains(brand.Site.SiteID))
            {
                return Redirect(base.GetHomePageForSite());
            }

            PageOutput output = new PageOutput();
            output.Output = service.GetPageContentFromLandingPage(page);
            output.Page = page;
            output.OmnitureAccountName = OmnitureHelper.GetAccountName(brand);
            output.OmnitureInternalFilter = OmnitureHelper.GetInternalFilter(brand);


            int? unifiedLandingPageID = page.UnifiedLandingPageID;

            if (!unifiedLandingPageID.HasValue) //lets get the unified landing page id from the third party service. 
            {
                try
                {
                    unifiedLandingPageID = service.GetUnifiedLandingPageID(brand.Site.SiteID, url);
                    LandingPageSA.Instance.SaveUnifiedLandingPageIDForLandingPage(page.LandingPageID, unifiedLandingPageID);

                }
                catch (Exception ex)
                {
                    //_logger.Error(ex.Message);
                    Log.Error(ex.Message);
                }
            }

            //only add the unfiedLandingPageID if we have a real one.
            int landingPageID = Constants.BAD_LANDING_PAGE_ID;

            if (unifiedLandingPageID.HasValue)
                landingPageID = (int)unifiedLandingPageID;

            SetBedrockCookie(HttpContext.Request.QueryString, landingPageID, unifiedLandingPageID);
            return View(output);
        }

        private void SetBedrockCookie(NameValueCollection querystringCollection, int landingPageID, int? unifiedLandingPageID)
        {
            string lgid = querystringCollection.AllKeys.Contains("lgid") ? querystringCollection["lgid"] : string.Empty;
            string prm = querystringCollection.AllKeys.Contains("prm") ? querystringCollection["prm"] : string.Empty;
            string lgid2 = querystringCollection.AllKeys.Contains("lgid2") ? querystringCollection["lgid2"] : string.Empty;
            string bid = querystringCollection.AllKeys.Contains("bid") ? querystringCollection["bid"] : string.Empty;
            string refcd = querystringCollection.AllKeys.Contains("refcd") ? querystringCollection["refcd"] : string.Empty;

            HttpCookie cookie = new HttpCookie("landingPageTransfer");
            cookie.Values.Add("lgid", lgid);
            cookie.Values.Add("prm", prm);
            cookie.Values.Add("lgid2", lgid2);
            cookie.Values.Add("bid", bid);
            cookie.Values.Add("refcd", refcd);
            cookie.Values.Add("lpid", landingPageID.ToString());

            HttpContext.Response.AppendCookie(cookie);
        }

    }
}
