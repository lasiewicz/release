﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LandingPageEngineV2
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Engine",
                url: "LP/{url}",
                defaults: new { controller = "LandingPage", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
