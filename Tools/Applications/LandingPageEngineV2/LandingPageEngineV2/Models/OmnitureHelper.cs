﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;

namespace LandingPageEngine.Models
{
    public class OmnitureHelper
    {
        private const int SPARK_COMMUNITY_ID = 1;

        public static string GetAccountName(Brand brand)
        {
            string accountName = string.Empty;

            accountName = RuntimeSettings.GetSetting("OMNITURE_ACCOUNT_NAME", brand.Site.Community.CommunityID, brand.Site.SiteID);
            if (string.IsNullOrEmpty(accountName))
            {
                accountName = "spark" + brand.Site.Name.Replace(".", string.Empty);
            }

            return accountName;

        }
          
        public static string GetInternalFilter(Brand brand)
        {
            if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", brand.Site.Community.CommunityID,
                brand.Site.SiteID, brand.BrandID))))
            {
                return "javascript:," + brand.Site.Name + ",secure.spark.net";
            }
            else
            {
                return "javascript:," + brand.Site.Name;
            }
        }
        




    }
}
