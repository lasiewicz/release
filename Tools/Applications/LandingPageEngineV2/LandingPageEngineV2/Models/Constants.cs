﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LandingPageEngine.Models
{
    public class Constants
    {
        public static string LANDING_PAGES_CACHE_KEY = "~LandingPages";
        public static string ERROR_KEY = "ErrorPageMessage";
        public static string MM_SERVICE_SITEID_KEY = "siteid";
        public static string MM_SERVICE_URL_KEY = "url";

        public static string MM_SERVICE_RESPONSE_ERROR_KEY = "error";
        public static string MM_SERVICE_RESPONSE_LPID_KEY = "lpid";
        public static string MM_SERVICE_RESPONSE_SUCCESS_KEY = "success";

        public static int BAD_LANDING_PAGE_ID = -1;

    }
}
