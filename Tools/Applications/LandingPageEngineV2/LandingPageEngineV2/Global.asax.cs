﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;

namespace LandingPageEngineV2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MvcApplication));
        protected void Application_Start()
        {
            var configFile = new System.IO.FileInfo(Server.MapPath("/") + "log4net.config");
            log4net.Config.XmlConfigurator.ConfigureAndWatch(configFile);
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //Log.Error("Test Log");
        }
    }
}
