﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SettingsManager.Startup))]
namespace SettingsManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            
            
        }
    }
}
