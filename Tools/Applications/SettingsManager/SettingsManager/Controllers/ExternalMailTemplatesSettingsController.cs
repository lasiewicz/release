﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using SettingsManager.Models;
using Spark.InternalSecurity.ServiceAdapter;

namespace SettingsManager.Controllers
{
    public class ExternalMailTemplatesSettingsController : Controller
    {
        const string Prefix ="YESMAIL_PERCENT";
        private const string Prefix2 = "YESMAIL_";
        private const string Contains = "_PERCENT";
        public ActionResult Index()
        {

            ViewBag.CanEdit = CanEdit();
            var settingConstants = RuntimeSettings.GetSettings().Keys.Cast<string>().ToList().Where(x => x.StartsWith(Prefix) || x.StartsWith(Prefix2) && x.Contains(Contains)).OrderBy(x => x).Select(
                x => new SiteSettingModel() { Key = x, Value = RuntimeSettings.Instance.GetSettingFromSingleton(x)});
            
            return View(settingConstants);
        }

        public ActionResult ProcessSetting(FormCollection form)
        {
            ViewBag.CanEdit = CanEdit();
            var settingConstant = form["settingConstant"];
            
            var siteId = 0;
            int.TryParse(form[settingConstant + "_SITE_ID"],out siteId);
            var value = form[settingConstant + "_VALUE"] ?? string.Empty;
            var settingConstants = RuntimeSettings.GetSettings().Keys.Cast<string>().ToList().Where(x => x.StartsWith(Prefix) || x.StartsWith(Prefix2) && x.Contains(Contains)).OrderBy(x => x).Select(
                x =>
                {
                    var settingSiteId = 0;
                    int.TryParse(form[x + "_SITE_ID"], out settingSiteId);
                    return new SiteSettingModel() {Key = x, SiteId = settingSiteId, Value = GetValue(settingSiteId,x)}; 

                }).ToList();
            

            switch (form["action"])
            {
                case "GET":
                    value = GetValue(siteId, settingConstant);
                    break;
                case "UPDATE":
                    UpdateValue(siteId, settingConstant, value);
                    break;
                case "GET_ALL":
                    GetAllValues(settingConstants,form);
                    break;
            }
            if (!string.IsNullOrEmpty(settingConstant))
            {
                var setting = settingConstants.First(x => x.Key == settingConstant);
                setting.Value = value;
                setting.SiteId = siteId;
            }
            return View("Index", settingConstants);
        }

        void UpdateValue(int siteId, string settingConstant, string value)
        {
            if (!CanEdit())
                return;
            if (siteId == 0)
                RuntimeSettings.Instance.UpdateGlobalDefault(settingConstant, value);
            else
                RuntimeSettings.Instance.SetValueForSite(siteId, settingConstant, value);
        }

        public string GetValue(int siteId, string settingConstant)
        {
            if (siteId == 0)
            {
                return RuntimeSettings.Instance.GetSettingFromSingleton(settingConstant);
            }
            var site = BrandConfigSA.Instance.GetBrands().GetSite(siteId);
            var communityId = site.Community.CommunityID;
            return RuntimeSettings.Instance.GetSettingFromSingleton(settingConstant, communityId,siteId);                        
        }

        public void GetAllValues(List<SiteSettingModel> settingConstants, FormCollection form)
        {
            settingConstants.ForEach(x =>
            {
                var settingSiteID = 0;
                int.TryParse(form[x.Key + "_SITE_ID"], out settingSiteID);
                x.Value = GetValue(settingSiteID, x.Key);
            });
        }
        private const string applicationName = "SettingsManagerUI";
        private const string useNewSecurity = "UseInternalSecurityEndpoint";

        bool CanEdit()
        {
            try
            {
                var name = User.Identity.Name.ToLower();

                var useSecurity = false;

                bool.TryParse(ConfigurationManager.AppSettings[useNewSecurity], out useSecurity);
                if (useSecurity)
                {
                    var authorizationServiceAdapter = new AuthorizationServiceAdapter((ClaimsIdentity) User.Identity,
                        applicationName);
                    return authorizationServiceAdapter.CanDoAction("ExternalMailSetting", "Edit");
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}