﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SettingsManager.Models
{
    public class SiteSettingModel
    {
        public string Key { get; set; }
        public int SiteId { get; set; }
        public string Value { get; set; } 
    }
}