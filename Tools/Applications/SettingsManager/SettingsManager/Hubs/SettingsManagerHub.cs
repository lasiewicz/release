﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SettingsManager.Hubs
{
    public class SettingsManagerHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }
    }
}