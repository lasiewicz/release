﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml.Linq;
using Newtonsoft.Json;
using Spark.InternalAPI.Helpers;
using Spark.InternalAPI.Serialization;
using log4net;

namespace Spark.InternalAPI.Modules
{
    public enum HttpSub500StatusCode
    {
        InternalServerError=50005 
    }
    
    public class ErrorHandlerHttpModule : IHttpModule
    {
        private const string ErrorControllerRouteName = "Errors";
        public const string IS_PROCESSED_KEY = "IsProcessed";
        private static readonly ILog Log = LogManager.GetLogger(typeof(ErrorHandlerHttpModule));

        #region IHttpModule Members

        public void Init(HttpApplication context)
        {
            context.Error += Application_Error;
            context.EndRequest += Application_EndRequest;
        }

        public void Dispose()
        {
        }

        #endregion

        private static void Application_EndRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.Response.StatusCode == (int)HttpStatusCode.NotFound
                || HttpContext.Current.Response.StatusCode == (int)HttpStatusCode.BadRequest
                || HttpContext.Current.Response.StatusCode == (int)HttpStatusCode.Forbidden
                || HttpContext.Current.Response.StatusCode == (int)HttpStatusCode.InternalServerError
                || HttpContext.Current.Response.StatusCode == (int)HttpStatusCode.Unauthorized
                || HttpContext.Current.Response.StatusCode == (int)HttpStatusCode.BadRequest)
            {
                if (HttpContext.Current.Items != null && HttpContext.Current.Items.Contains(IS_PROCESSED_KEY)) return;
                if (HttpContext.Current.Response.ContentType.ToLower().Contains("json")) return;

                HttpContext.Current.Items[IS_PROCESSED_KEY] = Boolean.TrueString.ToLower();
                var contextWrapper = new HttpContextWrapper(((HttpApplication)sender).Context);

                ProcessHttpException(contextWrapper);
            }
        }

        private static void Application_Error(object sender, EventArgs e)
        {
            var contextWrapper = new HttpContextWrapper(((HttpApplication)sender).Context);
            ProcessHttpException(contextWrapper);
        }

        private static void ProcessHttpException(HttpContextWrapper contextWrapper)
        {
            // This block was preventing the error handling from working. Also commented out <customErrors> in web.config to lessen confusion.
            // The web.config's <customErrors> section's mode property. This setting indicates whether custom errors are enabled at all, and if so whether they are enabled only for remote requests.
            //if (!HttpContext.Current.IsCustomErrorEnabled)
            //{
            //    // Don't handle the error in this case.
            //    return;
            //}

            var exception = HttpContext.Current.Server.GetLastError();
            exception = exception == null ? exception : exception.GetBaseException();
            if (null == exception)
            {
                exception = new HttpException(contextWrapper.Response.StatusCode,
                                              contextWrapper.Response.StatusDescription,
                                              contextWrapper.Response.SubStatusCode);
            }
            else if (!(exception is HttpException))
            {
                var jsonError = GetExceptionMessage(exception);
                var he = new HttpException((int)HttpStatusCode.InternalServerError, jsonError.Message,
                                           (int)HttpSub500StatusCode.InternalServerError);
                he.Data["uniqueId"] = jsonError.UniqueId;
                exception = he;
            }

            var httpException = exception as HttpException;
            var appRelativePath = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;

            if (appRelativePath.StartsWith("~/" + ErrorControllerRouteName + "/", StringComparison.OrdinalIgnoreCase))
            {
                TransferToFatalErrorPage(httpException);
                return;
            }


            var httpStatusCode = httpException != null
                                     ? httpException.GetHttpCode()
                                     : (int)HttpStatusCode.InternalServerError;

            var errorRoute = GetHttpErrorPath(httpStatusCode);
            if (httpStatusCode == (int)HttpStatusCode.Unauthorized)
            {
                Log.Warn(httpException);
            }
            else
            {
                Log.Error(httpException);
            }

            try
            {
                if (HttpRuntime.UsingIntegratedPipeline)
                {
                    // For IIS 7+
                    //have to use the name value collection to transfer exception values to controller
                    var nameValueCollection = new NameValueCollection
                                                  {
                                                      {"subStatusCode", httpException.ErrorCode.ToString()},
                                                      {"errorMessage", httpException.Message}
                                                  };
                    var uniqueId = httpException.Data["uniqueId"] as string;
                    if (uniqueId != null) nameValueCollection.Add("uniqueId", uniqueId);
                    //add values used by logging module
                    var appID = contextWrapper.Items["tokenAppId"] ?? 0;
                    var memberID = contextWrapper.Items["tokenMemberId"] ?? 0;
                    nameValueCollection.Add("tokenAppId", appID.ToString());
                    nameValueCollection.Add("tokenMemberId", memberID.ToString());
                    //if not handling exception, send flag to not log on request transfer to prevent double logging
                    HttpContext.Current.Server.TransferRequest(errorRoute, false, "GET", nameValueCollection);
                }
                else
                {
                    // For IIS 6
                    var originalPath = HttpContext.Current.Request.Path;
                    HttpContext.Current.RewritePath(errorRoute);

                    IHttpHandler handler = new MvcHttpHandler();
                    handler.ProcessRequest(HttpContext.Current);

                    HttpContext.Current.RewritePath(originalPath);
                }
                HttpContext.Current.Server.ClearError();
            }
            catch (Exception e)
            {
                // Log the error here in some way.
                Log.Error(httpException, e);
                TransferToFatalErrorPage(httpException);
            }
        }

        private static JsonError GetExceptionMessage(Exception exception)
        {
            var jsonError = new JsonError();
            jsonError.UniqueId = ErrorHelper.LogException(exception);
            jsonError.Message = exception.Message;
            return jsonError;
        }

        private static void TransferToFatalErrorPage(HttpException httpException)
        {
            HttpContextWrapper contextWrapper = new HttpContextWrapper(HttpContext.Current.ApplicationInstance.Context);
            var statusCode = (int)HttpStatusCode.InternalServerError;
            var subStatusCode = 0;
            var statusDescription = HttpStatusCode.InternalServerError.ToString();
            var errorMessage = contextWrapper.Response.StatusDescription;
            if (null != httpException)
            {
                statusCode = httpException.GetHttpCode();
                subStatusCode = httpException.ErrorCode;
                statusDescription = Enum.Parse(typeof(HttpStatusCode), statusCode.ToString()).ToString();
                errorMessage = httpException.Message;
            }
            contextWrapper.Response.ClearHeaders();
            contextWrapper.Response.ClearContent();
            contextWrapper.Response.TrySkipIisCustomErrors = true;
            contextWrapper.Response.StatusCode = statusCode;
            contextWrapper.Response.SubStatusCode = subStatusCode;
            contextWrapper.Server.ClearError();
            contextWrapper.Response.AddHeader("content-type", "application/json");
            //            contextWrapper.Response.AddHeader("X-AspNetMvc-Version", "3.0");
            var jsonResult = new NewtonsoftJsonResult
            {
                Result = new
                {
                    code = statusCode,
                    status = statusDescription,
                    error = new
                    {
                        code = subStatusCode,
                        message = errorMessage
                    }
                }
            };
            contextWrapper.Response.Write(JsonConvert.SerializeObject(jsonResult));
        }

        private static string GetHttpErrorPath(int statusCode)
        {
            var conf = XDocument.Load(HostingEnvironment.ApplicationPhysicalPath + "web.config");
            var httpErrors = conf.Root.Element("system.webServer").Element("httpErrors");
            foreach (XElement elem in httpErrors.Elements())
            {
                if (elem.Name.ToString().ToLower() == "error")
                {
                    var code = Int32.Parse(elem.Attribute("statusCode").Value);
                    if (code == statusCode)
                    {
                        return elem.Attribute("path").Value;
                    }
                }
            }
            return "/errors/exception";
        }
    }
}