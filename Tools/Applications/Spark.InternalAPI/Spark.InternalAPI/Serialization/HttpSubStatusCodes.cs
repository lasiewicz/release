﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.InternalAPI.Models.Integration
{
    public enum HttpSub400StatusCode
    {
        ValidMemberIdRequired = 40000,
        MissingMemberId = 40001,
        InvalidMemberId = 40002,
        FailedUploadPhoto = 40003,
        MissingPhotoUploadData = 40004,
        FailedDeletePhoto = 40005,
        UnsupportedPhotoFormat = 40006,
        MaxPhotoSizeExceeded = 40007,
        MaxNumberOfPhotosExceeded = 40008,
        InvalidMemberPhotoId = 40009,
        FailedPhotoSave = 40010,
        FailedRemoveFromHotList = 40011,
        FailedAddToHotList = 40012,
        FailedSendMailMessage = 40018,
        FailedSendTease = 40019,
        InvalidBrandId = 40021,
        UnknownHotListCategory = 40022,
        UnsupportedHotListCategory = 40023,
        MissingEmail = 40024,
        MissingPassword = 40025,
        InvalidApplicationId = 40026,
        MissingApplicationId = 40027,
        MissingAttributesToUpdate = 40030,
        FailedSaveMatchPreferences = 40040,
        FailedAuthentication = 40050,
        FailedCreateSession = 40055,
        FailedSaveSession = 40056,
        FailedGetSession = 40057,
        FailedGetMemberFromSession = 40058,
        InvalidSessionProperty = 40059,
        FailedRegistration = 40060,
        InvalidPageSize = 40061,
        BlockedSenderIdForSendMail = 40062,
        InvalidPasswordResetToken = 40063,
        InvalidLocation = 40064,
        AttributeMappingFailure = 40065,
        SearchPreferenceMappingFailure = 40066,
        MingleRegistrationRequestMissingRequiredParameters = 40067,
        FailedMingleMemberUpdate = 40068,
        InvalidUserName = 40069,
        FailedSaveLogonCredentials = 40070,
        FailedUpdateLogonActiveFlag = 40071,
        FailedUpdateProfileEnabledStatus = 40072,
        InvalidAdminMemberId = 40073
    }

    public enum HttpSub401StatusCode
    {
        MissingAccessToken = 41000,
        InvalidAccessTokenVersion = 41002,
        ExpiredToken = 41003,
        TokenNotFoundForMember = 41004,
        MismatchedToken = 41005,
        InvalidApplicationId = 41007,
        InvalidMemberId = 41008,
        IPBlocked = 41011,
        InvalidEmailOrPassword = 41012,
        InvalidAccessToken = 41013,
        SubscriberIdRequired = 41020,
        ValidClientSecretRequired = 41021,
        MismatchClientSecret = 41022,
        InvalidAttributeToUpdate = 41023,
        MissingClientIP = 41024,
        InvalidClientIP = 41025,
        CredentialsAlreadyCreated = 41026,
        InvalidAdminMemberId = 41027
    }
}