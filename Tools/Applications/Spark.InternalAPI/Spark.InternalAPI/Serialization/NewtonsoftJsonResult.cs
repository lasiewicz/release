﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Spark.InternalAPI.Serialization
{
    public class NewtonsoftJsonResult : ActionResult
    {
        public object Result { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            string contentType = "application/json";
            if (context.Controller.TempData.ContainsKey("contentType"))
            {
                string s = context.Controller.TempData["contentType"] as string;
                if (!string.IsNullOrEmpty(s)) contentType = s;
            }
            context.HttpContext.Response.ContentType = contentType;
            Result = JsonConvert.SerializeObject(Result,
                                                 new JsonSerializerSettings
                                                 {
                                                     DateTimeZoneHandling = DateTimeZoneHandling.Utc
                                                 });

            if (context.Controller.TempData.ContainsKey("redirectUrl") && !string.IsNullOrEmpty((string)context.Controller.TempData["redirectUrl"]))
            {
                string redirectUrl = context.Controller.TempData["redirectUrl"] as string;
                string encodedJSON = HttpUtility.UrlEncode((string)Result);
                redirectUrl = redirectUrl.Replace("%s", "").Replace("?", "");
                context.HttpContext.Response.Redirect(redirectUrl + "?" + encodedJSON);
            }
            else
            {
                context.HttpContext.Response.Write(Result);
            }
        }
    }
}