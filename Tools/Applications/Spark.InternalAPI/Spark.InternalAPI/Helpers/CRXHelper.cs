﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Matchnet;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Spark.InternalAPI.Controllers;
using Spark.InternalAPI.Models.CRX;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using MCA = Matchnet.Content.ValueObjects.AttributeMetadata;
using log4net;

namespace Spark.InternalAPI.Helpers
{
    public class CRXHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(CRXController));
        private const string AdminVerifyPercentageEnglish = "CRX_ADMIN_VERIFY_PERCENTAGE_ENGLISH";

        public void CompleteTextAttributeApproval(QueueItemText attributeUpdate)
        {
            var textAttrInfo = DBApproveQueueSA.Instance.GetCRXTextAttributeInfo(attributeUpdate.CRXResponseID);
            if (textAttrInfo != null)
            {
                attributeUpdate.MemberID = textAttrInfo.MemberID;
                attributeUpdate.CommunityID = textAttrInfo.CommunityID;
                attributeUpdate.LanguageID = textAttrInfo.LanguageID;
                attributeUpdate.MemberAttributeGroupID = textAttrInfo.MemberAttributeGroupID;
                attributeUpdate.TextContent = textAttrInfo.TextContent;
                attributeUpdate.MemberTextApprovalID = textAttrInfo.MemberTextApprovalID;

                if (attributeUpdate.MemberID > 0)
                {
                    Member member = MemberSA.Instance.GetMember(attributeUpdate.MemberID, MemberLoadFlags.IngoreSACache);
                    int brandId = 0;
                    member.GetLastLogonDate(attributeUpdate.CommunityID, out brandId);
                    Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
                    bool updateUsername = false;
                    SaveMemberAttribute(attributeUpdate, member, brand, attributeUpdate.LanguageID, ref updateUsername);
                    SaveMemberAndCompleteTextApproval(attributeUpdate, member, updateUsername);
                }
            }
            else
            {
                Log.Debug(string.Format("Unable to retrieve member text attribute info. CRXResponseId : {0}", attributeUpdate.CRXResponseID));
            }
        }

        public bool ValidateCRXItem(CRXItem item, ref string responseId, out string itemError)
        {
            bool isValid = true;
            string reason = "";
            itemError = string.Empty;
            if (item.CRXResponseId <= 0)
            {
                isValid = false;
                reason += " Invalid CRXResponseID";
            }
            else
            {
                var lastResetDate = DBApproveQueueSA.Instance.GetLastCRXResponseResetTime(item.CRXResponseId);

                if (lastResetDate != item.CRXLastResetDate)
                {
                    isValid = false;
                    reason += string.Format(" Last CRXResetDate does not match PassedIn: {0} DBValue: {1}",
                                            item.CRXLastResetDate.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                                            lastResetDate.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                }
            }

            if (!isValid)
            {
                responseId += item.CRXResponseId.ToString() + reason + " ";
                itemError = reason;
            }

            return isValid;
        }

        public bool ValidateCRXDuplicates(List<CRXDuplicate> items, out string errorString)
        {
            var sb = new StringBuilder();
            foreach(CRXDuplicate dup in items)
            {
                if(dup.MemberId <= 0)
                {
                    sb.Append(" Invalid MemberID: " + dup.MemberId);
                }

                if(dup.CRXResponseId <= 0)
                {
                    sb.Append(" Invalid CRXResponseID: " + dup.CRXResponseId);
                }

                if(string.IsNullOrEmpty(dup.FieldName))
                {
                    sb.Append(" FieldName is not specified.");
                }
                else
                {
                    var fieldType = GetFieldType(dup.FieldName);
                    if(fieldType == FieldType.Unknown)
                    {
                        sb.Append(string.Format(" Unsupported field type/name passed in [{0}]", dup.FieldName));
                    }
                }

                if(string.IsNullOrEmpty(dup.FieldValue))
                {
                    sb.Append(" FieldValue is missing.");
                }
            }

            errorString = sb.ToString();
            return sb.Length == 0;
        }

        public string GetAttributeName(int memberId, int communityId, int memberAttributeGroupId)
        {
            Member member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
            int brandId = 0;
            member.GetLastLogonDate(communityId, out brandId);
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            string attributeName = "";
            MCA.AttributeCollection attributeCollection =
                    AttributeMetadataSA.Instance.GetAttributeCollections().GetCollection
                    ("FREETEXT", communityId, Constants.NULL_INT, Constants.NULL_INT);

            foreach (MCA.AttributeCollectionAttribute aca in attributeCollection)
            {
                Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(aca.AttributeID);
                if (attribute.DataType == MCA.DataType.Text)
                {
                    MCA.AttributeGroup attributeGroup = member.getAttributeGroup(brand.Site.Community.CommunityID,
                                        brand.Site.SiteID,
                                        brand.BrandID,
                                        attribute);
                    if (attributeGroup.ID == memberAttributeGroupId)
                    {
                        attributeName = attribute.Name;
                        break;
                    }
                }
            }
            return attributeName;
        }

        public List<CRXParam> GetCRXParams()
        {
            var crxParams = new List<CRXParam>();
            int languageId = 2;
            Brands brands = BrandConfigSA.Instance.GetBrandsByLanguageID(languageId);
            foreach (Brand brand in brands)
            {
                if (crxParams.Where(x => x.CommunityId == brand.Site.Community.CommunityID).Count() == 0)
                {
                    int adminVerifyPercentage = 0;
                    int.TryParse(RuntimeSettings.Instance.GetSettingFromSingleton(AdminVerifyPercentageEnglish, brand.Site.Community.CommunityID),
                                 out adminVerifyPercentage);
                    if (adminVerifyPercentage > 0)
                    {
                        crxParams.Add(new CRXParam
                        {
                            CommunityId = brand.Site.Community.CommunityID,
                            LanguageId = languageId,
                            AdminVerifyPercentage = adminVerifyPercentage
                        });
                    }
                }
            }
            return crxParams;
        }

        public List<CRXAttribute> GetAttributeIds()
        {
            var crxAttributes = new List<CRXAttribute>();
            MCA.Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            var attrGroupIds = attributes.GetAttributeGroups();
            crxAttributes = (from AttributeGroup attrGroupId in attrGroupIds.Values 
                    select new CRXAttribute {AttributeGroupId = attrGroupId.ID, AttributeId = attrGroupId.AttributeID}).ToList();
            crxAttributes.Sort((attr1, attr2) => attr1.AttributeGroupId.CompareTo(attr2.AttributeGroupId));
            return crxAttributes;
        }

        public List<CRXAttributeOption> GetAttributeValueDescription(string attrName)
        {
            AttributeOptionCollection collection = AttributeOptionSA.Instance.GetAttributes().Get(attrName);
            return (from AttributeOption attributeOption in collection select new CRXAttributeOption {AttributeValue = attributeOption.Value, Description = attributeOption.Description}).ToList();
        }

        public List<IndividualTextApprovalStatus> GetIndividualTextStatuses()
        {
            var statuses = DBApproveQueueSA.Instance.GetIndividualTextApprovalStatuses();
            return statuses.Where(x => x.Display).ToList();
        }

        public List<CRXAttribute> GetAttributeIdNames()
        {
            var crxAttributes = new List<CRXAttribute>();
            MCA.Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            var htAttributes = attributes.GetAllAttributes();
            foreach (int id in htAttributes.Keys)
            {
                var attribute = htAttributes[id] as Matchnet.Content.ValueObjects.AttributeMetadata.Attribute;
                if(attribute != null)
                {
                    crxAttributes.Add(new CRXAttribute { AttributeId =  id, AttributeName = attribute.Name});
                }
            }
            return crxAttributes;
        }

        public List<object> ConvertCRXResetDatesToString(List<DateTime> CRXResetDates)
        {
            var returnVal = new List<object>();
            if(CRXResetDates != null && CRXResetDates.Count > 0)
            {
                CRXResetDates.Sort((x, y) => x.CompareTo(y));
                for(int i=0; i < CRXResetDates.Count; i++)
                {
                    returnVal.Add(new { Id = i + 1, Time = CRXResetDates[i].ToString("yyyy-MM-dd HH:mm:ss.fff") });
                }
            }
            return returnVal;
        }

        private void SaveMemberAttribute(QueueItemText attributeUpdate, Member member, Brand brand, int languageId, ref bool updateUsername)
        {
            string attributeName = "";
            string attributeValue = "";

            //Get actual brand based on languageId where the attribute is updated. 
            if (brand.Site.LanguageID != languageId)
            {
                var updatedBrand = GetLastUpdatedBrand(member, languageId);
                if (updatedBrand != null)
                    brand = updatedBrand;
            }

            MCA.AttributeCollection attributeCollection =
                    AttributeMetadataSA.Instance.GetAttributeCollections().GetCollection
                    ("FREETEXT", brand.Site.Community.CommunityID, Constants.NULL_INT, Constants.NULL_INT);

            foreach (MCA.AttributeCollectionAttribute aca in attributeCollection)
            {
                Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(aca.AttributeID);
                if (attribute.DataType == MCA.DataType.Text)
                {
                    MCA.AttributeGroup attributeGroup = member.getAttributeGroup(brand.Site.Community.CommunityID,
                                        brand.Site.SiteID,
                                        brand.BrandID,
                                        attribute);
                    if (attributeGroup.ID == attributeUpdate.MemberAttributeGroupID)
                    {
                        attributeName = attribute.Name;
                        break;
                    }
                }
            }

            if (attributeUpdate.TextContent.Trim() == string.Empty)
            {
                if (attributeName.ToLower() == "username")
                {
                    attributeValue = member.MemberID.ToString();
                }
            }
            else
            {
                attributeValue = attributeUpdate.TextContent;
                if (attributeName.ToLower() == "username" && attributeValue.Trim() == string.Empty)
                {
                    attributeValue = member.MemberID.ToString();
                }
            }

            //update username 
            if (attributeName.ToLower() == "username")
            {
                TextStatusType textStatusType = TextStatusType.None;
                string oldValue = member.GetAttributeText(brand.Site.Community.CommunityID,
                            brand.Site.SiteID,
                            brand.BrandID,
                            attributeUpdate.LanguageID,
                            attributeName,
                            String.Empty,
                            out textStatusType);
                updateUsername = attributeValue != oldValue;
                member.SetUsername(attributeValue.Trim(), brand, TextStatusType.Human);
            }
            else
            {
                member.SetAttributeText(brand.Site.Community.CommunityID,
                    brand.Site.SiteID,
                    brand.BrandID,
                    attributeUpdate.LanguageID,
                    attributeName,
                    attributeValue.Trim(),
                    TextStatusType.Human);
            }
        }


        private void SaveMemberAndCompleteTextApproval(QueueItemText attributeUpdate, Member member, bool updateUsername)
        {
            if (updateUsername)
                MemberSA.Instance.SaveMember(member, attributeUpdate.CommunityID); //Use overloaded SaveMember method to update username
            else
                MemberSA.Instance.SaveMember(member);
            //Complete attribute approval
            DBApproveQueueSA.Instance.CompleteCRXTextAttributeArroval(attributeUpdate);
        }

        private Brand GetLastUpdatedBrand(Member member, int languageId)
        {
            //Get actual brand based on languageId where the attribute is updated. 
            //Brand info is retrieved based on 'lastlogondate' which is not same as actual brand where the member updated the attribute.
            Brand brand = null;
            //get all sites from member's community
            int[] siteIds = member.GetSiteIDList();
            foreach (var siteId in siteIds)
            {
                Brand updatedbrand = GetBrand(siteId);
                if (updatedbrand.Site.LanguageID == languageId)
                {
                    brand = updatedbrand;
                    break;
                }
            }
            return brand;
        }

        private Brand GetBrand(int siteID)
        {
            Brands brands = BrandConfigSA.Instance.GetBrandsBySite(siteID);

            //default brand should have the smallest brandID
            Brand defaultBrand = null;
            foreach (Brand b in brands)
            {
                if (defaultBrand == null)
                {
                    defaultBrand = b;
                }
                else if (b.BrandID < defaultBrand.BrandID)
                {
                    defaultBrand = b;
                }
            }
            return defaultBrand;
        }

        public FieldType GetFieldType(string fieldName)
        {
            var enumNames = Enum.GetNames(typeof (FieldType));
            var queryResult = (from string s in enumNames
                               where s.ToLower() == fieldName.ToLower()
                               select s).SingleOrDefault();

            if(string.IsNullOrEmpty(queryResult))
                return FieldType.Unknown;

            return (FieldType)Enum.Parse(typeof (FieldType), fieldName);
        }
    }
}