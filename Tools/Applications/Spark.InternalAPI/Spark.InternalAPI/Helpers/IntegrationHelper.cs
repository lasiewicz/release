﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Spark.API.ServiceAdapters;
using Spark.API.ValueObjects;
using Spark.Common.Security;
using Spark.InternalAPI.Models.Integration;
using log4net;

namespace Spark.InternalAPI.Helpers
{
    public class IntegrationHelper
    {
        // 4 - added AdminMemberID
        private const int TokenVersion = 4;
        
        private static readonly TimeSpan AccessTokenLifetime;
        private static readonly TimeSpan AccessTokenLongLifetime;
        private static readonly TimeSpan RefreshTokenLifetime;
        
        private static readonly ILog Log = LogManager.GetLogger(typeof(IntegrationHelper));

        static IntegrationHelper()
        {
            try
            {
                var lifeTimeString = ConfigurationManager.AppSettings["AccessTokenLifetimeMins"];
                var lifetimeMins = Int32.Parse(lifeTimeString);
                AccessTokenLifetime = new TimeSpan(0, lifetimeMins, 0);
                lifeTimeString = ConfigurationManager.AppSettings["AccessTokenLongLifetimeMins"];
                lifetimeMins = Int32.Parse(lifeTimeString);
                AccessTokenLongLifetime = new TimeSpan(0, lifetimeMins, 0);
                lifeTimeString = ConfigurationManager.AppSettings["RefreshTokenLifetimeMins"];
                lifetimeMins = Int32.Parse(lifeTimeString);
                RefreshTokenLifetime = new TimeSpan(0, lifetimeMins, 0);
            }
            catch (Exception ex)
            {
                Log.Error("failed to read token AccessTokenLifetimeMins, RefreshTokenLifetimeMins settings from webconfig", ex);
                throw;
            }
        }

        
        internal static TokenPairResponseV2 GetOrCreateAccessToken(Brand brand, int applicationId, string scope, int memberId, int adminMemberId)
        {
            if(brand == null)
            {
                return new TokenPairResponseV2
                {
                    ResponseCode = (int)HttpStatusCode.BadRequest,
                    ResponseSubCode = (int)HttpSub400StatusCode.InvalidBrandId,
                    Error = "Invalid Brand ID "
                };
            }
            
            var memberBasicInfo = MemberSA.Instance.GetMemberBasicLogonInfo(memberId, brand.Site.Community.CommunityID);
            if( memberBasicInfo == null)
            {
                return new TokenPairResponseV2
                {
                    ResponseCode = (int)HttpStatusCode.BadRequest,
                    ResponseSubCode = (int)HttpSub401StatusCode.InvalidMemberId,
                    Error = String.Format("Invalid Member ID {0}", memberId)
                };
            }

            var app = GetApplication(applicationId);
            if (app == null)
            {
                return new TokenPairResponseV2
                {
                    ResponseCode = (int)HttpStatusCode.BadRequest,
                    ResponseSubCode = (int)HttpSub400StatusCode.InvalidApplicationId,
                    Error = String.Format("Invalid application ID {0}", applicationId)
                };
            }

            if (adminMemberId < 0)
            {
                return new TokenPairResponseV2
                {
                    ResponseCode = (int)HttpStatusCode.BadRequest,
                    ResponseSubCode = (int)HttpSub400StatusCode.InvalidAdminMemberId,
                    Error = String.Format("Invalid admin member ID {0}", adminMemberId)
                };
            }

            var tokens = GetAccessRefreshToken(applicationId, memberId);

            if (tokens != null)
            {
                if (String.IsNullOrEmpty(tokens.RefreshToken.Token) ||
                    tokens.RefreshToken.ExpiresDateTime.AddMinutes(-1) < DateTime.Now)
                {
                    var createAccessTokenUsingPassword = CreateAccessTokenTrusted(brand, applicationId,
                                                                                  memberId, scope, adminMemberId);
                    
                    return TokenPairResponseV2.Parse(createAccessTokenUsingPassword);
                    
                }
                if (String.IsNullOrEmpty(tokens.AccessToken.Token) ||
                    tokens.AccessToken.ExpiresDateTime.AddMinutes(-1) < DateTime.Now)
                {
                    return GetOrCreateAccessTokenRefresh(brand, app, memberId, tokens.RefreshToken.Token, adminMemberId);
                    
                }
                var isPayingMember = MemberIsSubscriber(brand, memberId);
                var accessTokenUsingPassword = new TokenPairResponse(tokens)
                {
                    MemberId = memberId,
                    IsPayingMember = isPayingMember,
                    AdminMemberID = adminMemberId
                };
                
                if (!(accessTokenUsingPassword is TokenPairResponseV2))
                {
                    return TokenPairResponseV2.Parse(accessTokenUsingPassword);
                }
                
            }

            var tokenTrusted = CreateAccessTokenTrusted(brand, applicationId, memberId, scope, adminMemberId);
            return TokenPairResponseV2.Parse(tokenTrusted);
        }

        private static TokenPair GetAccessRefreshToken(int applicationId, int memberId)
        {
            var existingMember = APISA.Instance.GetAppMember(applicationId, memberId);
            if (existingMember == null)
            {
                Log.DebugFormat("no app member found for app id {0}, member id {1}", applicationId, memberId);
                return null;
            }
            if (String.IsNullOrEmpty(existingMember.AccessToken))
            {
                Log.DebugFormat("null or empty access token for app id {0}, member id {1}", applicationId, memberId);
            }
            if (String.IsNullOrEmpty(existingMember.RefreshToken))
            {
                Log.DebugFormat("null or empty refresh token for app id {0}, member id {1}", applicationId, memberId);
            }

            var tokenPair = new TokenPair
            {
                AccessToken = new TokenWithExpiration
                {
                    Token = existingMember.AccessToken,
                    ExpiresDateTime = existingMember.AccessTokenExpirationDateTime
                },
                RefreshToken = new TokenWithExpiration
                {
                    Token = existingMember.RefreshToken,
                    ExpiresDateTime = existingMember.RefreshTokenExpirationDateTime
                }
            };
            return tokenPair;
        }

        private static TokenPairResponseV2 GetOrCreateAccessTokenRefresh(Brand brand, Application app, int memberId,
                                                                       string refreshToken, int adminMemberId)
        {

            // validate refresh token
            var tokens = GetAccessRefreshToken(app.ApplicationId, memberId);
            if (tokens == null)
            {
                var error = string.Format("null tokens for app {0} member {1}", app.ApplicationId, memberId);
                Log.Debug(error);
                
                return new TokenPairResponseV2
                {
                    ResponseCode = (int)HttpStatusCode.Unauthorized,
                    ResponseSubCode = (int)HttpSub401StatusCode.TokenNotFoundForMember,
                    Error = error
                };
            }
            if (String.IsNullOrEmpty(refreshToken))
            {
                var error = string.Format("null refresh token for app {0} member {1}", app.ApplicationId, memberId);
                Log.Debug(error);
                
                return new TokenPairResponseV2
                {
                    ResponseCode = (int)HttpStatusCode.Unauthorized,
                    ResponseSubCode = (int)HttpSub401StatusCode.TokenNotFoundForMember,
                    Error = error
                };
            }
            if (tokens.RefreshToken.ExpiresDateTime < DateTime.Now)
            {
                var error = string.Format("expired refresh token for app {0} member {1}", app.ApplicationId, memberId);
                Log.Debug(error);
                
                return new TokenPairResponseV2
                {
                    ResponseCode = (int)HttpStatusCode.Unauthorized,
                    ResponseSubCode = (int)HttpSub401StatusCode.ExpiredToken,
                    Error = error
                };
                
            }

            if (tokens.RefreshToken.Token == refreshToken)
            {
                if (tokens.AccessToken.ExpiresDateTime < DateTime.Now)
                {
                    var expiresIn = AccessTokenLifetime;
                    tokens.AccessToken = CreateToken(app.ApplicationId, memberId, 0, expiresIn, adminMemberId);
                    SaveAppMember(app.ApplicationId, memberId, tokens, brand);
                }

                var isPayingMember = MemberIsSubscriber(brand, memberId);
                var tokenPairResponse = new TokenPairResponse(tokens) { MemberId = memberId, IsPayingMember = isPayingMember, AdminMemberID = adminMemberId};
                Log.DebugFormat("returning tokens for - app {0} member {1}", app.ApplicationId, memberId);

                return TokenPairResponseV2.Parse(tokenPairResponse);
            }
            var errorMessage = string.Format("Invalid refresh token: {0} expected token {1}", refreshToken,
                                             tokens.RefreshToken.Token);
            Log.Debug(errorMessage);
            
            return new TokenPairResponseV2
            {
                ResponseCode = (int)HttpStatusCode.Unauthorized,
                ResponseSubCode = (int)HttpSub401StatusCode.InvalidAccessToken,
                Error = errorMessage
            };
        }

        private static TokenWithExpiration CreateToken(int applicationId, int memberId, int cryptogrphicPaddingBytes,
            TimeSpan expiresIn, int adminMemberId)
        {
            // adding random bytes to the unencrypted string makes the data more secure from hackers, but increases token length
            var randomBytes = cryptogrphicPaddingBytes > 0 ? Encryption.GetCryptoBytes(cryptogrphicPaddingBytes) : null;

            var expirationDate = DateTime.Now + expiresIn;

            var appIdBytes = BitConverter.GetBytes(applicationId);
            var memberIdBytes = BitConverter.GetBytes(memberId);
            var adminMemberIdBytes = BitConverter.GetBytes(adminMemberId);
            var expirationBytes = BitConverter.GetBytes(expirationDate.Ticks);

            var bytes =
                new byte[appIdBytes.Length + memberIdBytes.Length + adminMemberIdBytes.Length + expirationBytes.Length + cryptogrphicPaddingBytes];
            var count = 0;
            appIdBytes.CopyTo(bytes, 0);
            count += appIdBytes.Length;
            memberIdBytes.CopyTo(bytes, count);
            count += memberIdBytes.Length;
            adminMemberIdBytes.CopyTo(bytes, count);
            count += adminMemberIdBytes.Length;
            expirationBytes.CopyTo(bytes, count);
            if (randomBytes != null)
            {
                count += expirationBytes.Length;
                randomBytes.CopyTo(bytes, count);
            }

            var encryptedToken = Encryption.EncryptBytes(bytes, true);

            // starting with version 3, use "-" instead of "/" to be url safe
            var versionedToken = string.Format("{0}-{1}", TokenVersion, encryptedToken);
            var tokenData = new TokenWithExpiration {Token = versionedToken, ExpiresDateTime = expirationDate};
            Log.Info(String.Format("Token Created in Internal.API: {0} ApplicationId: {1} MemberId: {2} AdminMemberId: {3}", versionedToken, applicationId, memberId, adminMemberId));
            return tokenData;
        }

        private static TokenPairResponse CreateAccessTokenTrusted(Brand brand, int applicationId, int memberId, string tokenLifeType, int adminMemberId)
        {
            var accessToken = CreateToken(applicationId, memberId, 0,
                                          tokenLifeType == "short" ? AccessTokenLifetime : AccessTokenLongLifetime, adminMemberId);

            var refreshtoken = CreateToken(applicationId, memberId, 30, RefreshTokenLifetime, adminMemberId);

            var tokenData = new TokenPair { AccessToken = accessToken, RefreshToken = refreshtoken };
            SaveAppMember(applicationId, memberId, tokenData, brand);
            var isPayingMember = MemberIsSubscriber(brand, memberId);
            var accessTokenResponse = new TokenPairResponse(tokenData) { MemberId = memberId, IsPayingMember = isPayingMember, AdminMemberID = adminMemberId};
            Log.DebugFormat("Created new tokens for member {0} application {1} refresh token {2} expires {3} access token {4} expires {5} adminMemberId {6}",
                memberId, applicationId, refreshtoken.Token, refreshtoken.ExpiresDateTime, accessToken.Token,
                accessToken.ExpiresDateTime, adminMemberId);

            return accessTokenResponse;
        }

        private static void SaveAppMember(int applicationId, int memberId, TokenPair tokenPair, Brand brand)
        {
            var appMember = new AppMember
            {
                AccessToken = tokenPair.AccessToken.Token,
                AccessTokenExpirationDateTime = tokenPair.AccessToken.ExpiresDateTime,
                AppId = applicationId,
                MemberId = memberId,
                RefreshToken = tokenPair.RefreshToken.Token,
                RefreshTokenExpirationDateTime = tokenPair.RefreshToken.ExpiresDateTime
            };
            var existingMember = APISA.Instance.GetAppMember(applicationId, memberId);
            if (existingMember == null)
            {
                Log.DebugFormat("Creating App member for app {0}, member {1} access token {2} refresh token {3}",
                                appMember.AppId, appMember.MemberId, appMember.AccessToken, appMember.RefreshToken);
                APISA.Instance.CreateAppMember(appMember);
            }
            else
            {
                Log.DebugFormat("Updating App member for app {0}, member {1} access token {2} refresh token {3}",
                                appMember.AppId, appMember.MemberId, appMember.AccessToken, appMember.RefreshToken);
                APISA.Instance.UpdateAppMember(appMember);
            }
        }

        private static Application GetApplication(int appId)
        {
            var apps = APISA.Instance.GetApps();
            var returnApp = apps != null
                                ? apps.Where(app => app != null).Where(app => app.AppId == appId).Select(
                                    TransformAppVoToApplication).FirstOrDefault()
                                : null;
            return returnApp;
        }

        private static Application TransformAppVoToApplication(App app)
        {
            var application = new Application
            {
                ApplicationId = app.AppId,
                AutoAuth = app.AutoAuth,
                BrandId = app.BrandId,
                ClientSecret = app.Secret,
                Title = app.Title,
                Description = app.Description,
                OwnerMemberId = app.MemberId,
                RedirectUrl = app.RedirectUrl,
            };
            return application;
        }

        private static bool MemberIsSubscriber(Brand brand, int memberId)
        {

            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            var isPaying = member.IsPayingMember(brand.Site.SiteID);
            return isPaying;
        }
    }
}