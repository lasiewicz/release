﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Matchnet;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Configuration.ServiceAdapters;
using Spark.InternalAPI.Models.Approval;

namespace Spark.InternalAPI.Helpers
{
    public class ApprovalHelper
    {
        private const string FTAExternalCommunities = "FTA_EXTERNAL_COMMUNITIES";

        public void AddItemsToTextQueue(FreeText freeText)
        {
            var texts = new List<QueueItemText>();
            foreach (var attribute in freeText.Attributes)
            {
                texts.Add(new QueueItemText
                {
                    MemberID = freeText.MemberId,
                    CommunityID = freeText.CommunityId,
                    SiteID = freeText.SiteId,
                    BrandID = Constants.NULL_INT,
                    LanguageID = freeText.LanguageId,
                    TextType =
                        (TextType)Enum.Parse(typeof(TextType), freeText.TextType.ToString()),
                    IPAddress = freeText.IP,
                    ProfileRegionId = (freeText.RegionId > 0) ? freeText.RegionId : Constants.NULL_INT,
                    GenderMask = (freeText.GenderMask > 0) ? freeText.GenderMask : Constants.NULL_INT,
                    MaritalStatus = (freeText.MaritalStatus > 0) ? freeText.MaritalStatus : Constants.NULL_INT,
                    FirstName = freeText.FirstName,
                    LastName = freeText.LastName,
                    Email = freeText.Email,
                    OccupationDescription = freeText.Occupation,
                    EducationLevel = (freeText.Education > 0) ? freeText.Education : Constants.NULL_INT,
                    Religion = (freeText.Religion > 0) ? freeText.Religion : Constants.NULL_INT,
                    Ethnicity = (freeText.Ethnicity > 0) ? freeText.Ethnicity : Constants.NULL_INT,
                    BillingPhoneNumber = freeText.BillingPhone,
                    RegistrationDate = freeText.RegDate,
                    SubscriptionStatus = freeText.SubStatus,
                    MingleTableColumnName = attribute.AttributeName,
                    TextContent = attribute.NewText,
                    IsExternal = true, //Non-BH member
                    OldTextContent = attribute.OldText,
                    PopulateMemberInfo = true
                });
            }
            
            string externalCommunityIds = RuntimeSettings.Instance.GetSettingFromSingleton(FTAExternalCommunities);
            //Add item to the queue, only if the communityId is listed in the setting
            if (!string.IsNullOrEmpty(externalCommunityIds) && externalCommunityIds.Contains(freeText.CommunityId.ToString()))
            {
                var baseItems = texts.Cast<QueueItemBase>().ToList();
                DBApproveQueueSA.Instance.Enqueue(baseItems);
                //Add text attribute info
                DBApproveQueueSA.Instance.SaveMemberTextApprovalAttributes(texts);
            }
        }

        public bool ValidateFreeText(FreeText freeText, out string invalidReason)
        {
            invalidReason = "";
            bool isValid = false;
            if(freeText != null)
            {
                if (freeText.MemberId <= 0)
                    invalidReason += string.Format("Invalid MemberId - {0} ", freeText.MemberId);
                if (freeText.CommunityId <= 0)
                    invalidReason += string.Format("Invalid CommunityId - {0} ", freeText.CommunityId);
                if (freeText.SiteId <= 0)
                    invalidReason += string.Format("Invalid SiteId - {0} ", freeText.SiteId);
                if (freeText.LanguageId <= 0)
                    invalidReason += string.Format("Invalid LanguageId - {0} ", freeText.LanguageId);
                if (freeText.TextType <= 0)
                    invalidReason += string.Format("Invalid TextType - {0} ", freeText.TextType);
                if(string.IsNullOrEmpty(freeText.Email) || (!string.IsNullOrEmpty(freeText.Email) && !Regex.IsMatch(freeText.Email,@"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"))) 
                    invalidReason += string.Format("Invalid Email - {0} ", freeText.Email);
                if (string.IsNullOrEmpty(freeText.SubStatus))
                    invalidReason += string.Format("Invalid SubStatus - {0} ", freeText.SubStatus);
                if (freeText.Attributes == null || (freeText.Attributes != null && freeText.Attributes.Count == 0))
                    invalidReason += "Atleast one attribute info is required";
                if (string.IsNullOrEmpty(invalidReason))
                    isValid = true;
            }
            return isValid;
        }
    }
}