﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Spark.InternalAPI.Serialization;
using log4net;

namespace Spark.InternalAPI.Helpers
{
    internal static class ErrorHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ErrorHelper));

        internal static void OnException(ExceptionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (context.ExceptionHandled)
            {
                return;
            }

            var guidId = LogException(context.Exception);
            var returnMessage = "Sorry, an error occurred while processing your request.";
            string exceptionMessage = context.Exception.ToString();
#if DEBUG
            returnMessage = exceptionMessage;
#endif
            context.Result = new NewtonsoftJsonResult
                                 {
                                     Result = new JsonError
                                                  {
                                                      Message = returnMessage,
                                                      UniqueId = guidId
                                                  }
                                 };
            context.ExceptionHandled = true;
            context.RequestContext.HttpContext.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
        }

        public static string LogException(Exception exception)
        {
            var guidId = Convert.ToString(Guid.NewGuid());
            Log.ErrorFormat("GuidId: {0} Exception Message: {1}", guidId, exception.ToString());
            return guidId;
        }
    }

    /// <summary>
    /// </summary>
    [Serializable]
    public class JsonError
    {
        public string Message { get; set; }
        public string UniqueId { get; set; }
    }
}