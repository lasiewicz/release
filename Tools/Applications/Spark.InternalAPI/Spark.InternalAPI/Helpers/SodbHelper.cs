﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Spark.InternalAPI.Controllers;
using Spark.InternalAPI.Models.Sodb;
using Spark.SODBQueue.SeviceAdapters;
using Spark.SODBQueue.ValueObjects;
using log4net;

namespace Spark.InternalAPI.Helpers
{
    public class SodbHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SodbController));
        public SodbItem GetSodbMemberToReview()
        {
            SodbItem sodbItem = null;
            try
            {
                SodbQueueSA.Instance.Environment = ConfigurationManager.AppSettings["Environment"];
                sodbItem = SodbQueueSA.Instance.GetSodbItemToReview();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return sodbItem;
        }

        public void CompleteSodbMemberReview(SodbReviewItem reviewItem)
        {
            try
            {
                var sodbItem = new SodbItem
                                   {
                                       SSPBlueId = reviewItem.SSPBlueId,
                                       MemberId = reviewItem.CustomerId,
                                       SiteId = reviewItem.CallingSystemId,
                                       AdminId = reviewItem.AdminId,
                                       AdminActionId = reviewItem.AdminActionId,
                                       AdminNotes = reviewItem.AdminNotes
                                   };
                SodbQueueSA.Instance.Environment = ConfigurationManager.AppSettings["Environment"];
                SodbQueueSA.Instance.CompleteSodbItemReview(sodbItem);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public List<SodbQueueCounts> GetReviewQueueStats()
        {
            var queueCounts = new List<SodbQueueCounts>();
            try
            {
                SodbQueueSA.Instance.Environment = ConfigurationManager.AppSettings["Environment"];
                queueCounts = SodbQueueSA.Instance.GetSodbReviewQueueStats();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return queueCounts;
        }

        public SodbItem GetSodbSecondReviewItem(int adminId)
        {
            SodbItem sodbItem = null;
            try
            {
                SodbQueueSA.Instance.Environment = ConfigurationManager.AppSettings["Environment"];
                sodbItem = SodbQueueSA.Instance.GetSodbSecondReviewItem(adminId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return sodbItem;
        }

        public List<SodbAgentReviewCounts> GetAgentReviewStats(DateTime startDate, DateTime endDate, int? siteId)
        {
            var queueCounts = new List<SodbAgentReviewCounts>();
            try
            {
                SodbQueueSA.Instance.Environment = ConfigurationManager.AppSettings["Environment"];
                queueCounts = SodbQueueSA.Instance.GetSodbAgentReviewStats(startDate, endDate, siteId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return queueCounts;
        }

        public List<SodbItem> SearchSodbMember(int memberId, int? siteId, int? sspBlueId)
        {
            var sodbMembers = new List<SodbItem>();
            try
            {
                SodbQueueSA.Instance.Environment = ConfigurationManager.AppSettings["Environment"];
                sodbMembers = SodbQueueSA.Instance.SearchSodbMember(memberId, siteId, sspBlueId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return sodbMembers;
        }

        public SodbItem GetSodbMember(int memberId, int siteId, int sspBlueId)
        {
            SodbItem sodbMember = null;
            try
            {
                SodbQueueSA.Instance.Environment = ConfigurationManager.AppSettings["Environment"];
                sodbMember = SodbQueueSA.Instance.GetSodbMemberById(memberId, siteId, sspBlueId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return sodbMember;
        }

        public List<SodbItem> GetSodbMembersWithNoPhoto()
        {
            var sodbMembers = new List<SodbItem>();
            try
            {
                SodbQueueSA.Instance.Environment = ConfigurationManager.AppSettings["Environment"];
                sodbMembers = SodbQueueSA.Instance.GetSodbMembersWithNoPhoto();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return sodbMembers;
        }

        public void UpdateMemberPhotoUploadStatus(int memberId, int siteId, int sspBlueId)
        {
            try
            {
                SodbQueueSA.Instance.Environment = ConfigurationManager.AppSettings["Environment"];
                SodbQueueSA.Instance.UpdatePhotoUploadStatus(memberId,siteId,sspBlueId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }
    }
}