﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace Spark.InternalAPI.Controllers
{
    public class BaseController : Controller
    {
        public ILog Logger;

        public BaseController()
        {
            Logger = LogManager.GetLogger(GetType());
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            Logger.Error("Spark InternalAPI error. ", filterContext.Exception);

        }
    }
}
