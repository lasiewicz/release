﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Configuration.ServiceAdapters;
using Spark.InternalAPI.Configuration;
using Spark.InternalAPI.Helpers;
using Spark.InternalAPI.Models.CRX;

namespace Spark.InternalAPI.Controllers
{
    public class CRXController : BaseController
    {
        private const string CRXEnabled = "ENABLE_CRX_APPROVAL";

        /// <summary>
        /// Gets member text attributes info for CRX approval
        /// </summary>
        [HttpGet]
        public ActionResult GetMemberTextAttributes(int? numberOfRows, int? communityId, int? languageId)
        {
            Logger.Info(string.Format("URL: {0}", HttpContext.Request.Url));
            var totalStopwatch = new Stopwatch();
            
            totalStopwatch.Start();

            var dbStopwatch = new Stopwatch();
            var crxHelper = new CRXHelper();
            //Default communityId = 3 languageId = 2
            if (!communityId.HasValue) communityId = 3;
            if (!languageId.HasValue) languageId = 2;

            dbStopwatch.Start();
            var textAttributes = DBApproveQueueSA.Instance.GetTextAttributesForCRXApproval(communityId, languageId,
                                                                                            null, numberOfRows);
            dbStopwatch.Stop();

            #region Serialize
            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
            var result = new ContentResult
            {
                Content = serializer.Serialize(textAttributes.Select(x => new
                {
                    x.MemberID,
                    x.MemberTextApprovalID,
                    x.MemberAttributeGroupID,
                    x.LanguageID,
                    x.CommunityID,
                    x.IPAddress,
                    x.ProfileRegionId,
                    x.GenderMask,
                    x.MaritalStatus,
                    x.FirstName,
                    x.LastName,
                    x.Email,
                    x.OccupationDescription,
                    x.EducationLevel,
                    x.Religion,
                    x.Ethnicity,
                    x.DaysSinceFirstSubscription,
                    x.BillingPhoneNumber,
                    x.SubscriptionStatus,
                    RegistrationDate = x.RegistrationDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    x.TextContent,
                    x.CRXResponseID,
                    CRXSubIds = crxHelper.ConvertCRXResetDatesToString(x.CRXResetDates),
                    InsertDate = x.InsertDate.ToString("yyyy-MM-dd HH:mm:ss")
                })), 
                ContentType = "application/json"
            };
            #endregion

            totalStopwatch.Stop();

            Logger.Info(string.Format("Leaving URL: {0} TotalElapsed: {1} DBElapsed: {2} TotalItemsReturned: {3}",
                                      HttpContext.Request.Url,
                                      string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                                    totalStopwatch.Elapsed.Hours, totalStopwatch.Elapsed.Minutes,
                                                    totalStopwatch.Elapsed.Seconds,
                                                    totalStopwatch.Elapsed.Milliseconds/10),
                                      string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                                    dbStopwatch.Elapsed.Hours, dbStopwatch.Elapsed.Minutes,
                                                    dbStopwatch.Elapsed.Seconds,
                                                    dbStopwatch.Elapsed.Milliseconds/10),
                                                    textAttributes == null ? 0 : textAttributes.Count));

            return result;
        }


        /// <summary>
        /// Update CRX Response fields from CRX tool
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //[JsonFilter(Param = "items", JsonDataType = typeof(List<CRXItem>))]
        public JsonResult UpdateMemberTextAttributes(List<CRXItem> items)
        {
            Logger.Info(string.Format("URL: {0}", HttpContext.Request.Url));
            var crxHelper = new CRXHelper();
            string invalidResponseIds = "";
            //var error = (List<string>)ViewData["JsonError"];
            if(items != null && items.Count > 0)
            {
                bool isCrxEnabled = false;
                bool.TryParse(RuntimeSettings.Instance.GetSettingFromSingleton(CRXEnabled), out isCrxEnabled);
                if (isCrxEnabled)
                {
                    foreach (var crxItem in items)
                    {
                        var itemError = string.Empty;
                        if (crxHelper.ValidateCRXItem(crxItem, ref invalidResponseIds, out itemError))
                        {
                            //Item stays in the queue for admin review, when item is marked as 'bad'
                            //or when item is marked as 'good' and still needs to be reviewed by admin
                            if (!crxItem.CRXApproval || (crxItem.CRXApproval && crxItem.CRXAdminVerify))
                            {
                                DBApproveQueueSA.Instance.CompleteCRXTextAttributeArroval(new QueueItemText
                                {
                                    UpdateCRXOnly = true,
                                    CRXResponseID = crxItem.CRXResponseId,
                                    CRXApproval = crxItem.CRXApproval,
                                    CRXApprovalDisapprovalWeight = crxItem.CRXApprovalDisapprovalWeight,
                                    CRXNotes = crxItem.CRXNotes,
                                    CRXAdminVerify = true
                                });
                            }
                            else //If CRX Approved, text goes live
                            {
                                crxHelper.CompleteTextAttributeApproval(new QueueItemText
                                {
                                    UpdateCRXOnly = false,
                                    CRXResponseID =
                                        crxItem.CRXResponseId,
                                    CRXApproval = crxItem.CRXApproval,
                                    CRXApprovalDisapprovalWeight =
                                        crxItem.CRXApprovalDisapprovalWeight,
                                    CRXNotes = crxItem.CRXNotes,
                                    CRXAdminVerify = crxItem.CRXAdminVerify
                                });
                            }
                        }
                        else
                        {
                            Logger.ErrorFormat(
                                "Invalid POST data. ValidationErrorReason: {6} CRXResponseID : {0} URL: {1} Date: {2} CRXApproval: {3} CRXApprovalDisapprovalWeight:{4} CRXNotes:{5} ",
                                crxItem.CRXResponseId, HttpContext.Request.Url,
                                DateTime.Now.ToLongTimeString(), crxItem.CRXApproval,
                                crxItem.CRXApprovalDisapprovalWeight, crxItem.CRXNotes, itemError);
                        }
                    }
                }
            }
            else
            {
                var hasItems = items == null;
                var itemCount = items != null ? items.Count : 0;
                
                Logger.ErrorFormat("Invalid POST data. URL: {0} HasItems {1} ItemCount {2}", HttpContext.Request.Url, hasItems, itemCount);
                return Json(string.Format("Invalid POST data. URL: {0}", HttpContext.Request.Url), JsonRequestBehavior.AllowGet);
            }
            return Json(string.Format("OK{0}", !string.IsNullOrEmpty(invalidResponseIds) ? " Invalid items: " + invalidResponseIds : ""), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //[JsonFilter(Param = "items", JsonDataType = typeof(List<CRXDuplicate>))]
        public JsonResult AddProfileDuplicate(List<CRXDuplicate> items)
        {
            Logger.Info(string.Format("URL: {0}", HttpContext.Request.Url));
            var crxHelper = new CRXHelper();
            string errorString = string.Empty;
            //var error = (List<string>)ViewData["JsonError"];
            if (items != null && items.Count > 0)
            {
                // despite its name this setting seems to indicate DB write should occur or not
                bool isCrxEnabled = false;
                bool.TryParse(RuntimeSettings.Instance.GetSettingFromSingleton(CRXEnabled), out isCrxEnabled);
                if (isCrxEnabled)
                {
                    errorString = CRXDuplicateManager.Instance.AddProfileDuplicate(items);
                }
            }

            if (errorString.Length > 0)
            {
                Logger.Error(string.Format("ERROR inside AddProfileDuplicate() -- {0}", errorString));
            }

            return Json(errorString.Length == 0 ? "OK" : string.Format("ERROR {0}", errorString), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get CRX Parameters
        /// </summary>
        [HttpGet]
        public JsonResult GetParams()
        {
            Logger.Info(string.Format("URL: {0}", HttpContext.Request.Url));
            var crxHelper = new CRXHelper();
            var crxparams = crxHelper.GetCRXParams();
            return Json(crxparams, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Get mnSystem Attributes
        /// </summary>
        [HttpGet]
        public JsonResult GetAttributeGroupIds()
        {
            Logger.Info(string.Format("URL: {0}", HttpContext.Request.Url));
            var crxHelper = new CRXHelper();
            var attributes = crxHelper.GetAttributeIds();
            return Json(attributes.Select(x=> new { x.AttributeGroupId, x.AttributeId }), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Attribute Value description
        /// </summary>
        [HttpGet]
        public JsonResult GetAttributeValueDescription(string attributeName)
        {
            Logger.Info(string.Format("URL: {0}", HttpContext.Request.Url));
            var crxHelper = new CRXHelper();
            var attributeOptions = crxHelper.GetAttributeValueDescription(attributeName);
            return Json(attributeOptions, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Individual Text Approval Status Mask and values
        /// </summary>
        [HttpGet]
        public JsonResult GetIndividualTextStatuses()
        {
            var crxHelper = new CRXHelper();
            var textApprovalStatus = crxHelper.GetIndividualTextStatuses();
            return Json(textApprovalStatus.Select(x=> new {x.StatusMask, x.Description } ), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get mnSystem AttributeId, Name mapping
        /// </summary>
        [HttpGet]
        public JsonResult GetAttributeIdNames()
        {
            Logger.Info(string.Format("URL: {0}", HttpContext.Request.Url));
            var crxHelper = new CRXHelper();
            var attributes = crxHelper.GetAttributeIdNames();
            return Json(attributes.Select(x => new { x.AttributeId, x.AttributeName }), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Revert CRX changes
        /// </summary>
        [HttpPost]
        public JsonResult Revert()
        {
            DBApproveQueueSA.Instance.RevertCRXResponse();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get CRX Response
        /// </summary>
        [HttpGet]
        public JsonResult GetResponse(int crxResponseId)
        {
            QueueItemText crxResponse = null;
            crxResponse = DBApproveQueueSA.Instance.GetCRXResponse(crxResponseId);
            if (crxResponse != null)
                return Json(new { crxResponseId, crxResponse.CRXAdminVerify, crxResponse.CRXApproval, crxResponse.CRXApprovalDisapprovalWeight, crxResponse.CRXNotes }, JsonRequestBehavior.AllowGet);
            return Json("CRX Response not available", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Verify CRX Response
        /// </summary>
        [HttpGet]
        public JsonResult VerifyCRXResponse(int? numberOfRows)
        {
            var crxHelper = new CRXHelper();
            var textAttributes = DBApproveQueueSA.Instance.VerifyCRXResponse(3, 2, null, numberOfRows);
            return Json(textAttributes.Select(x => new
            {
                x.MemberID,
                x.MemberTextApprovalID,
                x.MemberAttributeGroupID,
                AttributeName = crxHelper.GetAttributeName(x.MemberID, x.CommunityID, x.MemberAttributeGroupID),
                x.IPAddress,
                x.CommunityID,
                x.LanguageID,
                x.ProfileRegionId,
                x.GenderMask,
                x.MaritalStatus,
                x.FirstName,
                x.LastName,
                x.Email,
                x.OccupationDescription,
                x.EducationLevel,
                x.Religion,
                x.Ethnicity,
                x.DaysSinceFirstSubscription,
                x.BillingPhoneNumber,
                x.SubscriptionStatus,
                x.RegistrationDate,
                x.TextContent,
                x.CRXResponseID,
                x.CRXApproval,
                x.CRXApprovalDisapprovalWeight,
                x.CRXNotes
            }), JsonRequestBehavior.AllowGet);
        }
    }
}
