﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.InternalAPI.Configuration;
using Spark.InternalAPI.Helpers;
using Spark.InternalAPI.Models.Sodb;
using Spark.SODBQueue.SeviceAdapters;
using Spark.SODBQueue.ValueObjects;

namespace Spark.InternalAPI.Controllers
{
    public class SodbController : BaseController
    {
       
        /// <summary>
        /// Gets SODB member info to review
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSodbMember()
        {
            var sodbHelper = new SodbHelper();
            SodbItem sodbMember = sodbHelper.GetSodbMemberToReview();
            return Json(sodbMember, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Updates Admin's review decision
        /// </summary>
        /// <param name="sodbItem"></param>
        /// <returns></returns>
        [HttpPost]
        //[JsonFilter(Param = "sodbItem", JsonDataType = typeof(SodbReviewItem))]
        public JsonResult CompleteSodbReview(SodbReviewItem sodbItem)
        {
            //var error = (List<string>)ViewData["JsonError"];
            if (sodbItem != null)
            {
                var sodbHelper = new SodbHelper();
                sodbHelper.CompleteSodbMemberReview(sodbItem);
            }
            return Json("OK");
        }

        /// <summary>
        /// Gets SODB review queue stats
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSodbReviewQueueStats()
        {
            var sodbHelper = new SodbHelper();
            List<SodbQueueCounts> queueCounts = sodbHelper.GetReviewQueueStats();
            return Json(queueCounts, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets SODB member info from Second Review queue
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSodbSecondReviewMember(int adminId)
        {
            var sodbHelper = new SodbHelper();
            SodbItem sodbMember = sodbHelper.GetSodbSecondReviewItem(adminId);
            return Json(sodbMember, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets SODB agent review stats
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSodbAgentReviewStats(DateTime startDate, DateTime endDate, int? siteId)
        {
            var sodbHelper = new SodbHelper();
            List<SodbAgentReviewCounts> queueCounts = sodbHelper.GetAgentReviewStats(startDate, endDate, siteId);
            return Json(queueCounts, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Search SODB member
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //[JsonFilter(Param = "sodbItem", JsonDataType = typeof(SodbSearchItem))]
        public JsonResult SearchSodbMember(SodbSearchItem sodbItem)
        {
            var sodbHelper = new SodbHelper();
            List<SodbItem> searchResults = sodbHelper.SearchSodbMember(sodbItem.CustomerId, sodbItem.CallingSystemId, sodbItem.SSPBlueId);
            return Json(searchResults, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets SODB member by Id
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSodbMemberById(int memberId, int siteId, int sspBlueId)
        {
            var sodbHelper = new SodbHelper();
            SodbItem member = sodbHelper.GetSodbMember(memberId, siteId, sspBlueId);
            return Json(member, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get members who haven't uploaded a photo
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSodbMembersWithNoPhoto()
        {
            Logger.Info(string.Format("URL: {0}", HttpContext.Request.Url));
            var sodbHelper = new SodbHelper();
            List<SodbItem> searchResults = sodbHelper.GetSodbMembersWithNoPhoto();
            return Json(searchResults, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Update Member photo upload status
        /// </summary>
        /// <returns></returns>
        public void UpdatePhotoUploadStatus(int memberId, int siteId, int sspBlueId)
        {
            Logger.Info(string.Format("URL: {0}", HttpContext.Request.Url));
            var sodbHelper = new SodbHelper();
            sodbHelper.UpdateMemberPhotoUploadStatus(memberId, siteId, sspBlueId);
        }
    }
}
