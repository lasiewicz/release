﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Spark.InternalAPI.Serialization;
using log4net;

namespace Spark.InternalAPI.Controllers
{
    public class ErrorsController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ErrorsController));

        private enum HttpSub404StatusCode
        {
            InvalidResourcePath = 44000
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (filterContext.HttpContext.Response.StatusCode == (int)HttpStatusCode.OK)
            {
                object result = filterContext.Result;
                if (result is NewtonsoftJsonResult)
                {
                    result = ((NewtonsoftJsonResult)result).Result;
                }
                else if (result is JsonResult)
                {
                    result = ((JsonResult)result).Data;
                }
                else if (result is ContentResult)
                {
                    result = ((ContentResult)result).Content;
                }

                if (!(result is HttpStatusCodeResult)) //don't wrap status code results in success response
                {
                    filterContext.Result = new NewtonsoftJsonResult
                    {
                        Result =
                            new
                            {
                                code = (int)HttpStatusCode.OK,
                                status = HttpStatusCode.OK.ToString(),
                                data = result
                            }
                    };
                }
            }
        }
        
        public ActionResult General(Exception exception)
        {
            InitializeResponse((int)HttpStatusCode.InternalServerError);
            var result = new NewtonsoftJsonResult { Result = new { error = exception.Message } };
            return result;
            //return Content("General failure", "text/plain");
        }

        public ActionResult Http500()
        {
            InitializeResponse((int)HttpStatusCode.InternalServerError);
            object error = null;
            var uniqueId = GetUniqueId();
            if (!string.IsNullOrEmpty(uniqueId))
            {
                error = new { code = Response.SubStatusCode, message = GetErrorMessage(), uniqueId };
            }
            else
            {
                error = new { code = Response.SubStatusCode, message = GetErrorMessage() };
            }
            return new NewtonsoftJsonResult
            {
                Result =
                    new
                    {
                        code = (int)HttpStatusCode.InternalServerError,
                        status = HttpStatusCode.InternalServerError.ToString(),
                        error
                    }
            };
        }

        public ActionResult Http404()
        {
            InitializeResponse((int)HttpStatusCode.NotFound);
            return new NewtonsoftJsonResult
            {
                Result =
                    new
                    {
                        code = (int)HttpStatusCode.NotFound,
                        status = HttpStatusCode.NotFound.ToString(),
                        error =
                new { code = (int)HttpSub404StatusCode.InvalidResourcePath, message = GetErrorMessage() }
                    }
            };
        }

        public ActionResult Http403()
        {
            InitializeResponse((int)HttpStatusCode.Forbidden);
            return new NewtonsoftJsonResult
            {
                Result =
                    new
                    {
                        code = (int)HttpStatusCode.Forbidden,
                        status = HttpStatusCode.Forbidden.ToString(),
                        error = new { code = Response.SubStatusCode, message = GetErrorMessage() }
                    }
            };
        }

        public ActionResult Http401()
        {
            InitializeResponse((int)HttpStatusCode.Unauthorized);
            return new NewtonsoftJsonResult
            {
                Result =
                    new
                    {
                        code = (int)HttpStatusCode.Unauthorized,
                        status = HttpStatusCode.Unauthorized.ToString(),
                        error = new { code = Response.SubStatusCode, message = GetErrorMessage() }
                    }
            };
        }

        public ActionResult Http400()
        {
            InitializeResponse((int)HttpStatusCode.BadRequest);
            return new NewtonsoftJsonResult
            {
                Result =
                    new
                    {
                        code = (int)HttpStatusCode.BadRequest,
                        status = HttpStatusCode.BadRequest.ToString(),
                        error = new { code = Response.SubStatusCode, message = GetErrorMessage() }
                    }
            };
        }

        private void InitializeResponse(int statusCode)
        {
            Response.ClearHeaders();
            Response.ClearContent();
            Response.StatusCode = statusCode;
            if (Request.Headers.HasKeys())
            {
                if (!string.IsNullOrEmpty(Request.Headers["subStatusCode"]))
                    Response.SubStatusCode = Int32.Parse(Request.Headers["subStatusCode"]);
                if (!string.IsNullOrEmpty(Request.Headers["tokenAppId"]))
                {
                    HttpContext.Items["tokenAppId"] = Int32.Parse(Request.Headers["tokenAppId"]);
                }
                if (!string.IsNullOrEmpty(Request.Headers["tokenMemberId"]))
                {
                    HttpContext.Items["tokenMemberId"] = Int32.Parse(Request.Headers["tokenMemberId"]);
                }
            }

            Response.TrySkipIisCustomErrors = true;
        }

        private string GetErrorMessage()
        {
            var message = Response.StatusDescription;
            if (Request.Headers.HasKeys() && !string.IsNullOrEmpty(Request.Headers["errorMessage"]))
            {
                message = Request.Headers["errorMessage"];
            }
            return message;
        }

        private string GetUniqueId()
        {
            var uniqueId = string.Empty;
            if (Request.Headers.HasKeys() && !string.IsNullOrEmpty(Request.Headers["uniqueId"]))
            {
                uniqueId = Request.Headers["uniqueId"];
            }
            return uniqueId;
        }

    }
}
