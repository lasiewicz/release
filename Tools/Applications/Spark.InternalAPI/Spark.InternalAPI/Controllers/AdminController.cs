﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spark.InternalAPI.Controllers
{
    public class AdminController : Controller
    {
        const string MatchnetServerDown = "Matchnet Server Down";
        const string MatchnetServerEnabled = "Matchnet Server Enabled";

        public ActionResult GetHealthCheck()
        {
            var serverStatus = MatchnetServerDown;
            try
            {
                var filePath = Path.GetDirectoryName(Server.MapPath("/")) + @"\deploy.txt";
                var fileExists = System.IO.File.Exists(filePath);

                if (!fileExists)
                {
                    serverStatus = MatchnetServerEnabled;
                }

            }
            catch (Exception)
            {
                serverStatus = MatchnetServerDown;
            }
            finally
            {
                Response.AddHeader("Health", serverStatus);
                Response.Write(serverStatus);
                Response.End();
            }

            return Content(serverStatus);
        }

    }
}
