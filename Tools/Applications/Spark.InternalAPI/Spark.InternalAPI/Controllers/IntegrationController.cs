﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Spark.InternalAPI.Helpers;
using Spark.InternalAPI.Models.Integration;
using Spark.InternalAPI.Serialization;

namespace Spark.InternalAPI.Controllers
{
    public class IntegrationController : Controller
    {
        public ActionResult CreateAccessToken(int brandId, int applicationId, int memberId, string lifetime, int? adminMemberId)
        {
            
            string scope = lifetime ?? "short";
            
            var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandId);

            var accessToken = IntegrationHelper.GetOrCreateAccessToken(brand, applicationId, scope, memberId, adminMemberId ?? 0);

            if (accessToken.ResponseCode == (int)HttpStatusCode.OK)
            {
                return new NewtonsoftJsonResult { Result = accessToken };
            }

            return new SparkHttpStatusCodeResult(accessToken.ResponseCode, accessToken.ResponseSubCode,
                                                     accessToken.Error);
        }

    }
}
