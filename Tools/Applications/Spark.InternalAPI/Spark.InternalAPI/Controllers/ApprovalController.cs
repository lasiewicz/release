﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Spark.InternalAPI.Configuration;
using Spark.InternalAPI.Helpers;
using Spark.InternalAPI.Models.Approval;

namespace Spark.InternalAPI.Controllers
{
    public class ApprovalController : BaseController
    {
        /// <summary>
        /// Method to add an item into Free Text Approval Queue
        /// </summary>
        /// <param name="freeTextItem"></param>
        /// <returns></returns>
        [HttpPost]
        //[JsonFilter(Param = "freeTextItem", JsonDataType = typeof(FreeTextItem))]
        public JsonResult EnqueueFreeTextApprovalItem(FreeTextItem freeTextItem)
        {
            Logger.Info(string.Format("URL: {0} Params: {1}", HttpContext.Request.Url, freeTextItem));
            
            var approvalHelper = new ApprovalHelper();
            string invalidReasons = "";
            //var error = (List<string>)ViewData["JsonError"];

            if (freeTextItem != null && approvalHelper.ValidateFreeText(freeTextItem.FreeText, out invalidReasons))
            {
                approvalHelper.AddItemsToTextQueue(freeTextItem.FreeText);
            }
            else
            {
                Logger.ErrorFormat("Invalid POST data. URL: {0}", HttpContext.Request.Url);
                return Json(string.Format("Invalid POST data. URL: {0} {1} ", HttpContext.Request.Url, (!string.IsNullOrEmpty(invalidReasons) ? "Reasons: " + invalidReasons : "")), JsonRequestBehavior.AllowGet);
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
    }
}
