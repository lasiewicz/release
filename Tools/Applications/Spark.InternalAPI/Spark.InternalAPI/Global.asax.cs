﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Spark.InternalAPI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              "IntegrationGetToken",
              "integration/createaccesstoken/{brandid}/{applicationId}/{memberId}",
              new { controller = "Integration", action = "CreateAccessToken"},
              new { httpMethod = new HttpMethodConstraint("POST") }
                    );

            routes.MapRoute(
              "CRXTextAttributesGet",
              "crxget/{numberOfRows}/{communityId}/{languageId}",
              new { controller = "CRX", action = "GetMemberTextAttributes", numberOfRows = UrlParameter.Optional, communityId = UrlParameter.Optional, languageId = UrlParameter.Optional },
              new { httpMethod = new HttpMethodConstraint("GET") }
                    );

            routes.MapRoute(
              "CRXDuplicate",
              "crxduplicate",
              new { controller = "CRX", action = "AddProfileDuplicate" },
              new { httpMethod = new HttpMethodConstraint("POST") }
                    );

            routes.MapRoute(
              "CRXTextAttributesUpdate",
              "crxupdate",
              new { controller = "CRX", action = "UpdateMemberTextAttributes" },
              new { httpMethod = new HttpMethodConstraint("POST") }
                    );

            routes.MapRoute(
              "CRXParamsGet",
              "crxparams",
              new { controller = "CRX", action = "GetParams" },
              new { httpMethod = new HttpMethodConstraint("GET") }
                    );

            routes.MapRoute(
              "CRXAtrributesGet",
              "crxgetattrgroupids",
              new { controller = "CRX", action = "GetAttributeGroupIds" },
              new { httpMethod = new HttpMethodConstraint("GET") }
                    );

            routes.MapRoute(
              "CRXAtrributesValueGet",
              "crxgetattrvalue/{attributeName}",
              new { controller = "CRX", action = "GetAttributeValueDescription" },
              new { httpMethod = new HttpMethodConstraint("GET") }
                    );

            routes.MapRoute(
             "CRXIndividualTextApprovalStatusGet",
             "crxgettextapprovalstatus",
             new { controller = "CRX", action = "GetIndividualTextStatuses" },
             new { httpMethod = new HttpMethodConstraint("GET") }
                   );

            routes.MapRoute(
              "CRXAtrributeIdNamesGet",
              "crxgetattrids",
              new { controller = "CRX", action = "GetAttributeIdNames" },
              new { httpMethod = new HttpMethodConstraint("GET") }
                    );

            string env = ConfigurationManager.AppSettings["Environment"];
            //Do not expose below methods in PROD. Below methods are for testing purpose.
            if (!string.IsNullOrEmpty(env) && env != "prod")
            {
                routes.MapRoute(
                    "Revert",
                    "crxreset",
                    new { controller = "CRX", action = "Revert" },
                    new { httpMethod = new HttpMethodConstraint("POST") }
                    );

                routes.MapRoute(
                    "GetResponse",
                    "crxgetresponse/{crxResponseId}",
                    new { controller = "CRX", action = "GetResponse" },
                    new { httpMethod = new HttpMethodConstraint("GET") }
                    );

                routes.MapRoute(
                    "VerifyResponse",
                    "crxverify/{numberOfRows}",
                    new { controller = "CRX", action = "VerifyCRXResponse", numberOfRows = UrlParameter.Optional },
                    new { httpMethod = new HttpMethodConstraint("GET") }
                    );
            }

            //Sodb
            routes.MapRoute(
                    "GetSodbMember",
                    "getsodbmember",
                    new { controller = "Sodb", action = "GetSodbMember" },
                    new { httpMethod = new HttpMethodConstraint("GET") }
                    );

            routes.MapRoute(
                    "SendSodbDecision",
                    "completesodbreview",
                    new { controller = "Sodb", action = "CompleteSodbReview" },
                    new { httpMethod = new HttpMethodConstraint("POST") }
                    );

            routes.MapRoute(
                    "GetSodbQueueCount",
                    "sodbqueuestats",
                    new { controller = "Sodb", action = "GetSodbReviewQueueStats" },
                    new { httpMethod = new HttpMethodConstraint("GET") }
                    );

            routes.MapRoute(
                   "GetSodbSecondReviewMember",
                   "getsodbreview/{adminId}",
                   new { controller = "Sodb", action = "GetSodbSecondReviewMember", adminId = UrlParameter.Optional },
                   new { httpMethod = new HttpMethodConstraint("GET") }
                   );

            routes.MapRoute(
                   "GetSodbAgentReviewStats",
                   "getsodbagentstats",
                   new { controller = "Sodb", action = "GetSodbAgentReviewStats" },
                   new { httpMethod = new HttpMethodConstraint("GET") }
                   );

            routes.MapRoute(
                   "SearchSodbMember",
                   "sodbsearch",
                   new { controller = "Sodb", action = "SearchSodbMember" },
                   new { httpMethod = new HttpMethodConstraint("POST") }
                   );

            routes.MapRoute(
                    "GetSodbMemberById",
                    "getsodbmember/{memberId}/{siteId}/{sspblueId}",
                    new { controller = "Sodb", action = "GetSodbMemberById", memberId = UrlParameter.Optional, siteId = UrlParameter.Optional, sspBlueId = UrlParameter.Optional },
                    new { httpMethod = new HttpMethodConstraint("GET") }
                    );

            routes.MapRoute(
                   "GetSodbMembersWithNoPhoto",
                   "getmemberswithnophoto",
                   new { controller = "Sodb", action = "GetSodbMembersWithNoPhoto" },
                   new { httpMethod = new HttpMethodConstraint("GET") }
                   );

            routes.MapRoute(
                    "UpdateQueueItemStatus",
                    "updatephotouploadstatus/{memberId}/{siteId}/{sspblueId}",
                    new { controller = "Sodb", action = "UpdatePhotoUploadStatus", memberId = UrlParameter.Optional, siteId = UrlParameter.Optional, sspBlueId = UrlParameter.Optional },
                    new { httpMethod = new HttpMethodConstraint("GET") }
                    );


            //Free Text Approval
            //Commenting until provo is ready to use the api to add items into FTA queue
            //routes.MapRoute(
            //    "fta-addtoqueue", "fta/enqueue",
            //    new { controller = "Approval", action = "EnqueueFreeTextApprovalItem" },
            //    new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "get-healthcheck", "admin/healthcheck",
                new { controller = "admin", action = "gethealthcheck" },
                new { httpMethod = new HttpMethodConstraint("Get") });
        }

        public static void RegisterErrorRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "get-http500", "errors/exception",
                new { controller = "errors", action = "http500" },
                new { httpMethod = new HttpMethodConstraint("Get") });

            routes.MapRoute(
                "get-http404", "errors/notfound",
                new { controller = "errors", action = "http404" },
                new { httpMethod = new HttpMethodConstraint("Get") });

            routes.MapRoute(
                "get-http403", "errors/forbidden",
                new { controller = "errors", action = "http403" },
                new { httpMethod = new HttpMethodConstraint("Get") });

            routes.MapRoute(
                "get-http401", "errors/unauthorized",
                new { controller = "errors", action = "http401" },
                new { httpMethod = new HttpMethodConstraint("Get") });

            routes.MapRoute(
                "get-http400", "errors/badrequest",
                new { controller = "errors", action = "http400" },
                new { httpMethod = new HttpMethodConstraint("Get") });

            routes.MapRoute(
                "Catch-all", "{*url}",
                new { controller = "errors", action = "http404" },
                new { httpMethod = new HttpMethodConstraint("Get") });
        }

        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);
            RegisterErrorRoutes(RouteTable.Routes);
        }
    }
}