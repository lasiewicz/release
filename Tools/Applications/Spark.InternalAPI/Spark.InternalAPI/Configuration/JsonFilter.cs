﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Spark.InternalAPI.Models.CRX;
using log4net;

namespace Spark.InternalAPI.Configuration
{
    public class JsonFilter : ActionFilterAttribute
    {
        public string Param { get; set; }
        public Type JsonDataType { get; set; }

        ILog Logger = LogManager.GetLogger(typeof(JsonFilter));

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (filterContext.HttpContext.Request.ContentType.Contains("application/json"))
                {
                    string inputContent;
                    using (var sr = new StreamReader(filterContext.HttpContext.Request.InputStream))
                    {
                        inputContent = sr.ReadToEnd();
                    }
                    var error = new List<string>();
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject(inputContent, JsonDataType, new JsonSerializerSettings {
                        MissingMemberHandling = MissingMemberHandling.Error,
                        Error = delegate (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                                    {
                                        error.Add(args.ErrorContext.Error.Message);
                                        args.ErrorContext.Handled = true;
                                    }
                    });
                    filterContext.Controller.ViewData["JsonError"] = error;
                    filterContext.ActionParameters[Param] = result;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in POST data. " + ex.Message + ex.StackTrace);
            }
        }
    }
}