﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.InternalAPI.Models.CRX
{
    public class CRXAttribute
    {
        public int AttributeGroupId { get; set; }
        public int AttributeId { get; set; }
        public string AttributeName { get; set; }
    }

    public class CRXAttributeOption
    {
        public int AttributeValue { get; set; }
        public string Description { get; set; }
    }
}