﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.InternalAPI.Models.CRX
{
    [DataContract]
    public class CRXItem
    {
        [DataMember(IsRequired = true)]
        public int CRXResponseId { get; set; }
        [DataMember(IsRequired = true)]
        public bool CRXApproval { get; set; }
        [DataMember(IsRequired = true)]
        public float CRXApprovalDisapprovalWeight { get; set; }
        [DataMember(IsRequired = true)]
        public string CRXNotes { get; set; }
        [DataMember(IsRequired = true)]
        public bool CRXAdminVerify { get; set; }
        [DataMember(IsRequired = true)]
        public DateTime CRXLastResetDate { get; set; }
    }
}