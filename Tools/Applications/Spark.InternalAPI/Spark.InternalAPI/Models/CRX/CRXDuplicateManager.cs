﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Spark.InternalAPI.Helpers;
using log4net;

namespace Spark.InternalAPI.Models.CRX
{
    public class CRXDuplicateManager
    {
        private static readonly CRXDuplicateManager _instance = new CRXDuplicateManager();
        public ILog Logger;

        private CRXDuplicateManager()
        {
            Logger = LogManager.GetLogger(GetType());
        }
        
        public static CRXDuplicateManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public string AddProfileDuplicate(List<CRXDuplicate> items)
        {
            var crxHelper = new CRXHelper();
            var errorString = string.Empty;

            // let's validate the duplicates sent. we need all of them to be valid to process
            if (crxHelper.ValidateCRXDuplicates(items, out errorString))
            {
                var queueItem = new QueueItemMemberProfileDuplicate();
                // For now, we only support English so set it to 2. If we support other languages later, we need to ask Wil to
                // pass that in.
                queueItem.LanguageID = 2;
                queueItem.MemberProfileDuplicates = new List<MemberProfileDuplicate>();
                var ordinal = 1;
                var firstCRXResponseId = -1;
                var crxResponseIds = string.Empty;
                foreach (CRXDuplicate dup in items)
                {
                    if(ordinal == 1)
                    {
                        firstCRXResponseId = dup.CRXResponseId;
                    }

                    queueItem.MemberProfileDuplicates.Add(new MemberProfileDuplicate()
                    {
                        OrdinalNumber = ordinal++,
                        CRXResponseId = dup.CRXResponseId,
                        FieldType = crxHelper.GetFieldType(dup.FieldName),
                        FieldValueOriginationDate = dup.FieldUpdateDate,
                        FieldValue = dup.FieldValue,
                        DuplicateStatus = DuplicateStatus.Undetermined,
                        MemberId = dup.MemberId
                    });

                    crxResponseIds += dup.CRXResponseId.ToString() + ",";
                }

                Logger.InfoFormat("Item count : {0} CRXResponseIDs passed in: {1}", items.Count.ToString(), crxResponseIds);

                try
                {
                    DBApproveQueueSA.Instance.SaveMemberProfileDuplicate(queueItem);
                }
                catch (Exception ex)
                {
                    var exceptionMessage = CombineInnerExceptionMessages(ex);
                    Logger.ErrorFormat("Exception string: {0}", exceptionMessage);

                    if (exceptionMessage.ToLower().Contains("violation of primary key constraint"))
                    {
                        errorString +=
                            string.Format("CRXResponseID: {0} exists in the database already. Insert attempt failed.",
                                          firstCRXResponseId);
                    }
                    Logger.ErrorFormat("Error string: {0}", errorString);
                }
                
            }

            return errorString;
        }

        private string CombineInnerExceptionMessages(Exception ex)
        {
            if(ex.InnerException == null)
                return string.Empty;

            var innerExceptionMessage = CombineInnerExceptionMessages(ex.InnerException);
            return string.Format("{0} {1} {2}", ex.Message, innerExceptionMessage == string.Empty ? string.Empty : "*InnerException:", innerExceptionMessage);
        }
    }
}