﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.InternalAPI.Models.CRX
{
    public class CRXParam
    {
        public int CommunityId { get; set; }
        public int LanguageId { get; set; }
        public int AdminVerifyPercentage { get; set; }
    }
}