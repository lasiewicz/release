﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.InternalAPI.Models.CRX
{
    [DataContract]
    public class CRXDuplicate
    {
        [DataMember(IsRequired = true)]
        public int MemberId { get; set; }
        
        [DataMember(IsRequired = true)]
        public int CRXResponseId { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime FieldUpdateDate { get; set; }
        
        [DataMember(IsRequired = true)]
        public string FieldName { get; set; }
        
        [DataMember(IsRequired = true)]
        public string FieldValue { get; set; }
    }
}