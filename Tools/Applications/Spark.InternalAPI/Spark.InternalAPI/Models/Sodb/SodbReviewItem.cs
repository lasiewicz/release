﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.InternalAPI.Models.Sodb
{
    [DataContract]
    public class SodbReviewItem
    {
        [DataMember(IsRequired = true)]
        public int SSPBlueId { get; set; }
        [DataMember(IsRequired = true)]
        public int CustomerId { get; set; }
        [DataMember(IsRequired = true)]
        public int CallingSystemId { get; set; }
        [DataMember(IsRequired = true)]
        public int AdminId { get; set; }
        [DataMember(IsRequired = true)]
        public int AdminActionId { get; set; }
        [DataMember]
        public string AdminNotes { get; set; }
    }


    [DataContract]
    public class SodbSearchItem
    {
        public int? SSPBlueId { get; set; }
        [DataMember(IsRequired = true)]
        public int CustomerId { get; set; }
        public int? CallingSystemId { get; set; }
    }
}