﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.InternalAPI.Models.Approval
{
    [DataContract]
    public class FreeText
    {
        [DataMember(IsRequired = true)]
        public int MemberId { get; set; }
        
        [DataMember(IsRequired = true)]
        public int CommunityId { get; set; }
        
        [DataMember(IsRequired = true)]
        public int SiteId { get; set; }
        
        [DataMember(IsRequired = true)]
        public int LanguageId { get; set; }
        
        [DataMember(IsRequired = true)]
        public int TextType { get; set; }

        [DataMember(IsRequired = false)]
        public string FirstName { get; set; }

        [DataMember(IsRequired = false)]
        public string LastName { get; set; }
        
        [DataMember(IsRequired = true)]
        public string Email { get; set; }

        [DataMember(IsRequired = false)]
        public string Username { get; set; }

        [DataMember(IsRequired = false)]
        public string IP { get; set; }

        [DataMember(IsRequired = false)]
        public int RegionId { get; set; }

        [DataMember(IsRequired = false)]
        public int Gender { get; set; }

        [DataMember(IsRequired = false)]
        public int SeekingGender { get; set; }

        [DataMember(IsRequired = false)]
        public int MaritalStatus { get; set; }

        [DataMember(IsRequired = false)]
        public string Occupation { get; set; }

        [DataMember(IsRequired = false)]
        public int Education { get; set; }

        [DataMember(IsRequired = false)]
        public int Religion { get; set; }

        [DataMember(IsRequired = false)]
        public int Ethnicity { get; set; }

        [DataMember(IsRequired = false)]
        public string BillingPhone { get; set; }
        
        [DataMember(IsRequired = true)]
        public string SubStatus { get; set; }
        
        [DataMember(IsRequired = true)]
        public DateTime RegDate { get; set; }

        [DataMember(IsRequired = true)]
        public List<FreeTextAttribute> Attributes { get; set; }


        public int GenderMask
        {
            get { return Gender | SeekingGender; }
        }
    }

    [DataContract]
    public class FreeTextAttribute 
    {
        [DataMember(IsRequired = true)]
        public string AttributeName { get; set; }

        [DataMember(IsRequired = false)]
        public string OldText { get; set; }
        
        [DataMember(IsRequired = true)]
        public string NewText { get; set; }
    }

    public class FreeTextItem
    {
        public FreeText FreeText { get; set; }

        public override string ToString()
        {
            if(FreeText != null)
                return string.Format(
                "MemberId: {0} CommunityId: {1} SiteId: {2} LanguageId: {3} TextType: {4} Email: {5} ", FreeText.MemberId,
                FreeText.CommunityId, FreeText.SiteId, FreeText.LanguageId, FreeText.TextType, FreeText.Email);
            return "";
        }
    }
}