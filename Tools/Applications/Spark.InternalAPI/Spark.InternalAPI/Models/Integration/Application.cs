﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.InternalAPI.Models.Integration
{
    [DataContract(Name = "Application", Namespace = "")]
    public class Application
    {
        [DataMember(Name = "applicationId")]
        public int ApplicationId { get; set; }

        [DataMember(Name = "brandId")]
        public int BrandId { get; set; }

        [DataMember(Name = "ownerMemberId")]
        public int OwnerMemberId { get; set; }

        [DataMember(Name = "clientSecret")]
        public string ClientSecret { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "redirectUrl")]
        public string RedirectUrl { get; set; }

        [IgnoreDataMember]
        public bool AutoAuth { get; set; }
    }
}