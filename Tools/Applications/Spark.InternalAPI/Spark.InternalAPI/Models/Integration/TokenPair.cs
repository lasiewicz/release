﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.InternalAPI.Models.Integration
{
    public class TokenPair
    {
        [IgnoreDataMember]
        public TokenWithExpiration AccessToken { get; set; }
        [IgnoreDataMember]
        public TokenWithExpiration RefreshToken { get; set; }
        //add token_type "bearer"?
    }
}