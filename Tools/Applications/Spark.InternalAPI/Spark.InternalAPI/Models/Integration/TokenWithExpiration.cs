﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.InternalAPI.Models.Integration
{
    public class TokenWithExpiration
    {
        public string Token { get; set; }
        public DateTime ExpiresDateTime { get; set; }
        public int ExpiresIn
        {
            get
            {
                var diff = ExpiresDateTime - DateTime.Now;
                return (int)diff.TotalSeconds;
            }
        }
    }
}