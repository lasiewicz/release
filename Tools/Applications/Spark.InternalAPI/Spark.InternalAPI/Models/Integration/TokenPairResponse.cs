﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.InternalAPI.Models.Integration
{
    public class TokenPairResponse
    {
        public TokenPairResponse() {}
		public TokenPairResponse(TokenPair tokenPairData)
		{
			AccessToken = tokenPairData.AccessToken.Token;
		    ExpiresIn = tokenPairData.AccessToken.ExpiresIn;
		    AccessExpiresTime = tokenPairData.AccessToken.ExpiresDateTime.ToUniversalTime();
			RefreshToken = tokenPairData.RefreshToken.Token;
            RefreshTokenExpiresTime = tokenPairData.RefreshToken.ExpiresDateTime.ToUniversalTime();
		}
        
        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }
        
        [DataMember(Name = "accessToken")]
	    public string AccessToken { get;  set; }

        [DataMember(Name = "expiresIn")]
        public int ExpiresIn { get;  set; }

        [DataMember(Name = "expiresTime")]
        public DateTime AccessExpiresTime { get; set; }

        [DataMember(Name = "refreshToken")]
        public string RefreshToken { get; set; }

		/// <summary>
		/// Expiration time of the refresh token
		/// </summary>
		[DataMember(Name = "refreshTokenExpiresTime")]
		public DateTime RefreshTokenExpiresTime { get; set; }

		[DataMember(Name = "error")]
        public string Error { get; set; }

        [DataMember(Name = "isPayingMember")]
        public bool IsPayingMember { get; set; }

        [DataMember(Name = "adminMemberID")]
        public int AdminMemberID { get; set; }
	}

    public class TokenPairResponseV2 : TokenPairResponse
    {
        private int responseCode = (int) HttpStatusCode.OK;
        private int responseSubCode = 0;

        public TokenPairResponseV2():base(){}
        public TokenPairResponseV2(TokenPair tokenPair):base(tokenPair){}

        [IgnoreDataMember]
        public int ResponseCode
        {
            get { return responseCode; }
            set { responseCode = value; }
        }

        [IgnoreDataMember]
        public int ResponseSubCode
        {
            get { return responseSubCode; }
            set { responseSubCode = value; }
        }

        public static TokenPairResponseV2 Parse(TokenPairResponse tpr)
        {
            TokenPairResponseV2 tokenPairResponseV2 = new TokenPairResponseV2();
            tokenPairResponseV2.AccessExpiresTime = tpr.AccessExpiresTime;
            tokenPairResponseV2.AccessToken = tpr.AccessToken;
            tokenPairResponseV2.Error = tpr.Error;
            tokenPairResponseV2.ExpiresIn = tpr.ExpiresIn;
            tokenPairResponseV2.IsPayingMember = tpr.IsPayingMember;
            tokenPairResponseV2.MemberId = tpr.MemberId;
            tokenPairResponseV2.RefreshToken = tpr.RefreshToken;
            tokenPairResponseV2.RefreshTokenExpiresTime = tpr.RefreshTokenExpiresTime;
            tokenPairResponseV2.AdminMemberID = tpr.AdminMemberID;
            return tokenPairResponseV2;
        }
    }
}