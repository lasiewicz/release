﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.InternalAPI.Controllers;
using Spark.InternalAPI.Models.CRX;

namespace Spark.InternalAPI.Test
{
    public class CRXControllerTest
    {
        [TestFixtureSetUp]
        public void StartUp()
        {
        }

        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {
        }

        [SetUp]
        public void SetUp()
        {

        }

        [TearDown]
        public void Teardown()
        {

        }

        [Test]
        public void AddProfileDuplicateTest()
        {
            var duplicates = new List<CRXDuplicate>();
            duplicates.Add(new CRXDuplicate()
                               {
                                   CRXResponseId = 234234,
                                   FieldName = "AboutMe",
                                   FieldUpdateDate = DateTime.Now,
                                   FieldValue = "im mike",
                                   MemberId = 111
                               });
            duplicates.Add(new CRXDuplicate()
            {
                CRXResponseId = 234234,
                FieldName = "AboutMe",
                FieldUpdateDate = DateTime.Now,
                FieldValue = "im mike the fake mike",
                MemberId = 222
            });

            var ret = CRXDuplicateManager.Instance.AddProfileDuplicate(duplicates);
            Assert.IsTrue(ret == string.Empty);
        }
    }
}
