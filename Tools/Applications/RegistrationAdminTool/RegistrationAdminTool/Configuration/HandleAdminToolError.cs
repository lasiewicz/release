﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RegistrationAdminTool.Models;
using log4net;
using System.Web.Routing;

namespace RegistrationAdminTool.Configuration
{
    public class HandleAdminToolError : HandleErrorAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(HandleAdminToolError));

        public override void OnException(ExceptionContext filterContext)
        {
            Log.Error("Registration Admin Tool error. ", filterContext.Exception);

            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                var model = new  HandleErrorInfo(filterContext.Exception, (string)filterContext.RouteData.Values["controller"], (string)filterContext.RouteData.Values["action"]);
                var result = new ViewResult
                {
                    ViewName = "Error", 
                    ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
                    TempData = filterContext.Controller.TempData,
                };

                filterContext.HttpContext.Response.Clear();
                filterContext.ExceptionHandled = true;
                filterContext.Result = result;
                
              


            }
            base.OnException(filterContext);
        }
    }
}