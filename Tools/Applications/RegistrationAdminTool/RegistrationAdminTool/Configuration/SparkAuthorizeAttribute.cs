﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using RegistrationAdminTool.Helpers;
using RegistrationAdminTool.Models;

namespace RegistrationAdminTool.Configuration
{
    public class SparkAuthorizeAttribute : AuthorizeAttribute
    {
        public Enums.Operations Operation { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool isAuthorized = base.AuthorizeCore(httpContext);
            if (isAuthorized)
            {
                var securityHelper = new ActiveDirectoryHelper();
                isAuthorized = securityHelper.CheckUserAccess(httpContext.User.Identity, (int) Operation);
            }
            return isAuthorized;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //TODO: handle this more elegantly, perhaps with a route redirect
            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                throw new UnauthorizedAccessException(string.Format("The {0} account is not authorized to access this page.",filterContext.HttpContext.User.Identity.Name));
            }
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}