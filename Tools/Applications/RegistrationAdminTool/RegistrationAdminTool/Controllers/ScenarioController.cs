﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RegistrationAdminTool.Configuration;
using RegistrationAdminTool.Managers;
using RegistrationAdminTool.Models;
using RegistrationAdminTool.Models.JSONResult;
using Matchnet.Content.ValueObjects.Registration;
using System.Text;
using Newtonsoft.Json;
using RegistrationAdminTool.Models.Entities;


namespace RegistrationAdminTool.Controllers
{
    public class ScenarioController : BaseController
    {
        //
        // GET: /Scenario/
        public ActionResult Index(int? siteId, int? scenarioId)
        {
            //CheckActionPermission(Enums.Operations.Access, false);
            
            var scenarioManager = new ScenarioManager();
            return View("Scenarios", scenarioManager.GetScenariosModel(siteId, scenarioId));
        }

        public JsonResult GetScenariosForSite(int siteId)
        {
            var scenarioManager = new ScenarioManager();
            var scenarios = scenarioManager.GetScenariosBySite(siteId, "", "");
            return Json(scenarios, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetScenariosForSiteWithSort(int siteId, string sort, string direction)
        {
            var scenarioManager = new ScenarioManager();
            var scenarios = scenarioManager.GetScenariosBySite(siteId, sort, direction);
            return Json(scenarios, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetScenarioDetails(int scenarioId)
        {
            var scenarioManager = new ScenarioManager();
            Matchnet.Content.ValueObjects.Registration.Scenario scenario = (from s in scenarioManager.Scenarios where s.ID == scenarioId select s).FirstOrDefault();
            var scenarioViewModel = new ScenarioViewModel(scenario);
            return Json(scenarioViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateScenario(int siteId, int deviceTypeId)
        {
            var scenarioManager = new ScenarioManager();
            return View("NewScenario", scenarioManager.GetNewScenarioModel(siteId, deviceTypeId));
        }

        public ActionResult EditScenario(int scenarioId)
        {
            var scenarioManager = new ScenarioManager();
            var editScenarioViewModel = scenarioManager.GetEditScenarioViewModel(scenarioId,false);
            Log.InfoFormat("EditScenarioViewModel: " + JsonConvert.SerializeObject(editScenarioViewModel));
            return View("EditScenario", editScenarioViewModel);
        }

        public ActionResult CopyScenario(int scenarioId)
        {
            var scenarioManager = new ScenarioManager();
            return View("CopyScenario", scenarioManager.GetEditScenarioViewModel(scenarioId,true));
        }


        [HttpPost]
        public JsonResult IsScenarioNameTaken(int siteId, string name, int? existingScenarioId)
        {
            var scenarioNameCheckResult = new ScenarioNameCheckResult();
            scenarioNameCheckResult.IsNameTaken = true;
            try
            {
                var scenarioManager = new ScenarioManager();
                scenarioNameCheckResult.Status = (int) ResultStatus.Success;
                if (existingScenarioId.HasValue)
                {
                    scenarioNameCheckResult.IsNameTaken = scenarioManager.IsScenarioNameTaken(siteId, name, existingScenarioId.Value);
                }
                else
                {
                    scenarioNameCheckResult.IsNameTaken = scenarioManager.IsScenarioNameTaken(siteId, name);
                }
            }
            catch (Exception ex)
            {
                scenarioNameCheckResult.Status = (int)ResultStatus.Failed;
            }
            
            return Json(scenarioNameCheckResult);
        }

        public JsonResult GetRegControls(int siteId, int deviceTypeId, int? scenarioId)
        {
            var scenarioManager = new ScenarioManager();
            List<RegControlDisplay> controls;

            if(scenarioId.HasValue && scenarioId.Value > 0)
            {
                controls = scenarioManager.GetRegControlsBySiteAndScenario(siteId, deviceTypeId, scenarioId.Value);    
            }
            else
            {
                controls = scenarioManager.GetRegControlsBySiteAndDevice(siteId, deviceTypeId); 
            }
            
            return Json(controls, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveScenario(Scenario scenario, int splashTemplateId, int regTemplateId, int confirmationTemplateId)
        {
            Log.InfoFormat("Scenario: {0}, splashTemplateId: {1}, regTemplateId: {2}, confirmationTemplateId: {3}", JsonConvert.SerializeObject(scenario), splashTemplateId, regTemplateId, confirmationTemplateId);

            var scenarioManager = new ScenarioManager();
            var result = scenarioManager.SaveScenario(scenario, splashTemplateId, regTemplateId, confirmationTemplateId);

            if (result.ScenarioId > 0)
            {
                result.Status = (int)ResultStatus.Success;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveActiveAndWeights(int siteID, int deviceTypeID, int selectedScenarioID, bool activateSelectedScenario, List<ActiveWeight> activeWeights)
        {
            Log.InfoFormat("siteID: {0}, deviceTypeID: {1}, selectedScenarioID: {2}, activateSelectedScenario: {3}, activeWeights: {4}",  siteID, deviceTypeID, selectedScenarioID, activateSelectedScenario, JsonConvert.SerializeObject(activeWeights));
            var scenarioManager = new ScenarioManager();
            var result = scenarioManager.SaveActiveAndWeights(siteID, deviceTypeID, selectedScenarioID, activateSelectedScenario, activeWeights);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveField(RegControlDisplay regControlDisplay, int scenarioId)
        {
            var scenarioManager = new ScenarioManager();
            var result = new APIResult();

            var success = scenarioManager.SaveRegControlScenarioOverride(regControlDisplay, scenarioId);
            if (success)
            {
                result.Status = (int) ResultStatus.Success;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTemplates(int siteId, TemplateType type)
        {
            var templateManager = new TemplateManager();
            return Json(templateManager.GetTemplates(siteId, type), JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveStep(Step step, int scenarioId)
        {
            var scenarioManager = new ScenarioManager();
            var result = new APIResult();

            var success = scenarioManager.SaveStep(step, scenarioId);
            if (success)
            {
                result.Status = (int)ResultStatus.Success;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Error()
        {
            return View("Error");
        }

        //get the pre fix url based on ENV and Site. 
        public JsonResult GetPriviewURL(int siteID)
        {

            string thePrefix = "";

            switch (siteID)
	        {
                case 103:
                    thePrefix = ConfigurationManager.AppSettings["RegSiteUri-JDate"];
                break;
                case 101:
                    thePrefix = ConfigurationManager.AppSettings["RegSiteUri-Spark"];
                break;
                case 105:
                    thePrefix = ConfigurationManager.AppSettings["RegSiteUri-JDateFR"];
                break;
                case 107:
                    thePrefix = ConfigurationManager.AppSettings["RegSiteUri-JDateUK"];
                break;
                case 9041:
                    thePrefix = ConfigurationManager.AppSettings["RegSiteUri-BBW"];
                break;
                case 9051:
                    thePrefix = ConfigurationManager.AppSettings["RegSiteUri-Black"];
                break;
                case 4:
                    thePrefix = ConfigurationManager.AppSettings["RegSiteUri-JDateIL"];
                break;
                case 15:
                thePrefix = ConfigurationManager.AppSettings["RegSiteUri-Cupid"];
                break;
		        
	        }


            return Json(thePrefix + "?scenario=", JsonRequestBehavior.AllowGet); ;
        }

    }
}
