﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.Registration;
using RegistrationAdminTool.Configuration;
using RegistrationAdminTool.Managers;
using RegistrationAdminTool.Models;

namespace RegistrationAdminTool.Controllers
{
//    [SparkAuthorize(Operation = Enums.Operations.Access)]
    public class TemplateController : BaseController
    {
        //
        // GET: /Template/
        public ActionResult Index()
        {
            TemplateManager templateManager = new TemplateManager();
            return View("Templates", templateManager.GetTemplateViewModel());        
        }        

        public ActionResult CreateTemplate()
        {
            TemplateManager templateManager = new TemplateManager();
            return View("NewTemplate", templateManager.GetNewTemplateViewModel());            
        }

        [HttpPost]
        public ActionResult SaveTemplate(SaveTemplateModel saveTemplateModel)
        {
            TemplateManager templateManager = new TemplateManager();
            TemplateModel templateModel = null;
            if (ModelState.IsValid)
            {
                templateModel = templateManager.SaveTemplateModel(saveTemplateModel);
            }

            if (null != templateModel && templateModel.ID > 0)
            {
                TemplateViewModel templateViewModel = templateManager.GetTemplateViewModel();
                templateViewModel.SuccessMessage = templateModel.SuccessMessage;
                return View("Templates", templateViewModel);
            }
            else
            {
                var model = templateManager.GetNewTemplateViewModel();
                model.ErrorMessage = (null != templateModel)? templateModel.ErrorMessage : "Error saving template.";
                model.TemplateType = saveTemplateModel.TemplateType;
                model.Name = saveTemplateModel.Name;
                model.SiteId = saveTemplateModel.SiteId;
                return View("NewTemplate", model);
            }
        }

        public ActionResult RemoveTemplate(int regTemplateId, int siteId)
        {
            TemplateManager templateManager = new TemplateManager();
            TemplateViewModel templateViewModel=templateManager.RemoveTemplateModel(regTemplateId, siteId);
            return View("Templates", templateViewModel);
        }
    }
}
