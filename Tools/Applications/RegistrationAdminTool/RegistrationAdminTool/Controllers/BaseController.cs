﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RegistrationAdminTool.Helpers;
using RegistrationAdminTool.Models;
using log4net;
using System.Configuration;
using Matchnet;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.Member.ServiceAdapters;

namespace RegistrationAdminTool.Controllers
{
    public class BaseController : Controller
    {
        public ILog Log;

         

        public BaseController()
        {
            Log = LogManager.GetLogger(GetType());
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            //ensure admin's domain/bedrock mapping exists
            if (ConfigurationManager.AppSettings["isADEnabled"] == "true" && GetAdminMemberID(HttpContext.User.Identity.Name) <= 0)
            {
                throw new Exception("Error in OnActionExecuting(). " + HttpContext.User.Identity.Name + " Admin is missing domain/bedrock mapping.");
            }

            CheckActionPermission(Enums.Operations.Access, true);

        }  

        #region helpers
        protected bool CheckActionPermission(Models.Enums.Operations operation, bool throwExceptionOnNoAccess)
        {
            var securityHelper = new ActiveDirectoryHelper();
            if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, (int)operation))
            {
                if (throwExceptionOnNoAccess)
                {
                    ViewData[WebConstants.ERROR_KEY] = "Your account does not have permissions for operation: " + operation.ToString();
                    throw new Exception("Your account does not have permissions for operation: " + operation.ToString());
                }

                return false;
            }

            return true;
        }


        private int GetAdminMemberID(string adminName)
        {
            int adminMemberID = Constants.NULL_INT;

             
            AdminMemberDomainMapperCollection adminMapperList = MemberSA.Instance.GetAdminMemberDomainMappers();
            if (adminMapperList != null && adminMapperList.Count > 0)
            {
                AdminMemberDomainMapper mapper = adminMapperList.GetAdminMemberDomainMapper(adminName);
                if (mapper != null)
                {
                    adminMemberID = mapper.MemberID;
                }
            }

            return adminMemberID;
        
        }


        //protected override void OnException(ExceptionContext filterContext)
        //{
        //    base.OnException(filterContext);

        //    //add logging
        //    string adminInfo = "";
        //    if (g != null)
        //    {
        //        adminInfo = "Admin User: " + g.AdminDomainAccount + ", " + g.AdminID.ToString() + ". ";
        //    }
        //    _logger.Error("Admin Tool error. " + adminInfo, filterContext.Exception);

        //    //display friendly error view
        //    if (filterContext.HttpContext.IsCustomErrorEnabled)
        //    {
        //        if (!ViewData.ContainsKey(WebConstants.ERROR_KEY))
        //        {
        //            //if apis that threw error did not provide a error page friendly error message, we'll display the full exception
        //            ViewData[WebConstants.ERROR_KEY] = ExceptionHelper.GetExceptionMessage(filterContext.Exception, ExceptionHelper.ExceptionMode.Html);
        //        }
        //        filterContext.HttpContext.Response.Clear();
        //        filterContext.ExceptionHandled = true;
        //        filterContext.Result = this.View("Error", new HandleErrorInfo(filterContext.Exception, (string)filterContext.RouteData.Values["controller"], (string)filterContext.RouteData.Values["action"]));

        //    }

        //}


        #endregion

    }
}