﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RegistrationAdminTool.Helpers;
using RegistrationAdminTool.Managers;
using RegistrationAdminTool.Models.JSONResult;

namespace RegistrationAdminTool.Controllers
{
    public class CacheController : BaseController
    {
        
        public ActionResult ClearCache(int siteId)
        {
            var cacheManager = new CacheManager();
            var cacheType = SiteHelper.GetRegSiteCacheType(siteId);
            var cleared = cacheManager.ClearRegistrationMetadataCache(cacheType, Server.MapPath("~"));

            var result = new APIResult { Status = cleared ? (int)ResultStatus.Success : (int)ResultStatus.Failed };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
