﻿using Matchnet.Content.ValueObjects.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistrationAdminTool.Helpers
{
    public class TemplateHelper
    {
        public static List<SelectListItem> GetTemplateTypesAsSelectList(int? templateTypeId)
        {
            var listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = TemplateType.Splash.ToString(), Value = ((int)TemplateType.Splash).ToString(), Selected = templateTypeId == (int)TemplateType.Splash });
            listItems.Add(new SelectListItem { Text = TemplateType.Registration.ToString(), Value = ((int)TemplateType.Registration).ToString(), Selected = templateTypeId == (int)TemplateType.Registration });
            listItems.Add(new SelectListItem { Text = TemplateType.Confirmation.ToString(), Value = ((int)TemplateType.Confirmation).ToString(), Selected = templateTypeId == (int)TemplateType.Confirmation });
            return listItems;
        }
    }
}