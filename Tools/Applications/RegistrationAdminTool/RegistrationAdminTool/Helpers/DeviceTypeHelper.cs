﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistrationAdminTool.Helpers
{
    public class DeviceTypeHelper
    {
        public static string GetDeviceName(int deviceId)
        {
            switch (deviceId)
            {
                case 1:
                    return "FWS";
                case 2:
                    return "MOS";
                case 3:
                    return "TOS";
                default:
                    return string.Empty;
            }
        }

        public static List<SelectListItem> GetDeviceTypesAsSelectList(int? deviceTypeId)
        {
            var listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = GetDeviceName(1), Value = "1", Selected = deviceTypeId == 1 });
            listItems.Add(new SelectListItem { Text = GetDeviceName(2), Value = "2", Selected = deviceTypeId == 2 });
            listItems.Add(new SelectListItem { Text = GetDeviceName(3), Value = "3", Selected = deviceTypeId == 3 });
            return listItems;
        }
    }
}