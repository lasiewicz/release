﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistrationAdminTool.Helpers
{
    public class DisplayTypeHelper
    {
        public static string GetDisplayTypeName(int displayTypeId)
        {
            switch(displayTypeId)
            {
                case 1:
                    return "DDL";
                case 2:
                    return "ChkBox";
                case 3:
                    return "Captcha";
                case 4:
                    return "TxtBox";
                case 5:
                    return "TxtArea";
                case 6:
                    return "Pwrd";
                case 7:
                    return "Radio";
                case 8:
                    return "Rgn";
                case 9:
                    return "DOB";
                case 10:
                    return "File";
                case 11:
                    return "CCode";
                case 12:
                    return "LstBox";
                case 13:
                    return "RgnAjx";
                case 14:
                    return "Info";
                case 15:
                    return "ChkList";
                case 16:
                    return "T&C";
                case 17:
                    return "DatePick";
                default:
                    return string.Empty;
            }
        }
    }
}