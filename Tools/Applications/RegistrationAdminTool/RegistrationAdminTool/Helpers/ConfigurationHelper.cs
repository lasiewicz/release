﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace RegistrationAdminTool.Helpers
{
    public class ConfigurationHelper
    {
        public static string JDateRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-JDate"]; }
        }
        public static string JDateILRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-JDateIL"]; }
        }
        public static string JDateFrRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-JDateFR"]; } 
        }
        public static string JDateUkRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-JDateUK"]; }
        }
        public static string BBWRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-BBW"]; }
        }
        public static string BlackRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-Black"]; }
        }
        public static string SparkRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-Spark"]; }
        }
        public static string CupidRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-Cupid"]; }
        }
        public static string AdventistSinglesRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-AdventistSingles"]; }
        }
        public static string CatholicMingleRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-CatholicMingle"]; }
        }
        public static string ChristianMingleRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-ChristianMingle"]; }
        }
        public static string DeafSinglesRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-DeafSingles"]; }
        }
        public static string LDSMingleRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-LDSMingle"]; }
        }
        public static string MilitarySinglesRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-MilitarySingles"]; }
        }
        public static string SilverSinglesRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-SilverSingles"]; }
        }
        public static string LDSSinglesRegSiteURL
        {
            get { return ConfigurationManager.AppSettings["RegSiteUri-LDSSingles"]; }
        }
    }
}