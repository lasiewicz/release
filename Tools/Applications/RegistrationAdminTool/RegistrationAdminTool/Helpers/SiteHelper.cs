﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.CacheBusterClient;
using System.Web.Mvc;

namespace RegistrationAdminTool.Helpers
{
    public class SiteHelper
    {
        public static string GetSiteName(int siteId) 
        {
            switch(siteId)
            {
                case 103:
                    return "JDate";
                case 101:
                    return "Spark";
                case 9041:
                    return "BBW";
                case 9051:
                    return "BlackSingles";
                case 4:
                    return "JDate.co.il";
                case 107:
                    return "JDate.co.uk";
                case 105:
                    return "JDate.fr";
                case 15:
                    return "Cupid";
                case 9011:
                    return "AdventistSingles";
                case 9071:
                    return "CatholicMingle";
                case 9081:
                    return "ChristianMingle";
                case 9111:
                    return "DeafSingles";
                case 9191:
                    return "LDSMingle";
                case 9221:
                    return "MilitarySingles";
                case 9231:
                    return "SilverSingles";
                case 9281:
                    return "LDSSingles";
                default:
                    return String.Empty;

            }
        }

        public static string GetRegURL(int siteId)
        {
            switch (siteId) 
            {
                case 4:
                    return ConfigurationHelper.JDateILRegSiteURL;
                case 101:
                    return ConfigurationHelper.SparkRegSiteURL;
                case 103:
                    return ConfigurationHelper.JDateRegSiteURL;
                case 105:
                    return ConfigurationHelper.JDateFrRegSiteURL;
                case 107:
                    return ConfigurationHelper.JDateUkRegSiteURL;
                case 9041:
                    return ConfigurationHelper.BBWRegSiteURL;
                case 9051:
                    return ConfigurationHelper.BlackRegSiteURL;
                case 15:
                    return ConfigurationHelper.CupidRegSiteURL;
                case 9011:
                    return ConfigurationHelper.AdventistSinglesRegSiteURL;
                case 9071:
                    return ConfigurationHelper.CatholicMingleRegSiteURL;
                case 9081:
                    return ConfigurationHelper.ChristianMingleRegSiteURL;
                case 9111:
                    return ConfigurationHelper.DeafSinglesRegSiteURL;
                case 9191:
                    return ConfigurationHelper.LDSMingleRegSiteURL;
                case 9221:
                    return ConfigurationHelper.MilitarySinglesRegSiteURL;
                case 9231:
                    return ConfigurationHelper.SilverSinglesRegSiteURL;
                case 9281:
                    return ConfigurationHelper.LDSSinglesRegSiteURL; 



                default:
                    return String.Empty;
            }
        }

        public static CacheType GetRegSiteCacheType(int siteId)
        {
            switch (siteId)
            {
                case 4:
                    return CacheType.RegSiteScenarioMetadataJDateIL;
                case 101:
                    return CacheType.RegSiteScenarioMetadataSpark;
                case 103:
                    return CacheType.RegSiteScenarioMetadataJDate;
                case 105:
                    return CacheType.RegSiteScenarioMetadataJDateFR;
                case 107:
                    return CacheType.RegSiteScenarioMetadataJDateUK;
                case 9041:
                    return CacheType.RegSiteScenarioMetadataBBW;
                case 9051:
                    return CacheType.RegSiteScenarioMetadataBlack;
                case 15:
                    return CacheType.RegSiteScenarioMetadataCupid;
                default:
                    return CacheType.APIScenarioMetadata;
            }
        }

        public static List<SelectListItem> GetSitesAsSelectList(int? siteId)
        {
            var listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Pick a Site", Value = "", Selected = siteId == 0 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(103), Value = "103", Selected = siteId == 103 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(4), Value = "4", Selected = siteId == 4 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9081), Value = "9081", Selected = siteId == 9081 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(101), Value = "101", Selected = siteId == 101 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9041), Value = "9041", Selected = siteId == 9041 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9051), Value = "9051", Selected = siteId == 9051 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(107), Value = "107", Selected = siteId == 107 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(105), Value = "105", Selected = siteId == 105 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9011), Value = "9011", Selected = siteId == 9011 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9071), Value = "9071", Selected = siteId == 9071 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9111), Value = "9111", Selected = siteId == 9111 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9191), Value = "9191", Selected = siteId == 9191 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9221), Value = "9221", Selected = siteId == 9221 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9231), Value = "9231", Selected = siteId == 9231 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(9281), Value = "9281", Selected = siteId == 9281 });
            listItems.Add(new SelectListItem { Text = SiteHelper.GetSiteName(15), Value = "15", Selected = siteId == 15 });
            return listItems;
        }
    }
}

