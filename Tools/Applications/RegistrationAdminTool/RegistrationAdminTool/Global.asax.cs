﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using RegistrationAdminTool.Configuration;
using RegistrationAdminTool.Models;
using Spark.CommonLibrary.Logging;

namespace RegistrationAdminTool
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleAdminToolError());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "get-clearcache", "clearcache/{siteId}",
               new { controller = "Cache", action = "ClearCache" },
               new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
               "get-templates", "gettemplates",
               new { controller = "Template", action = "Index" },
               new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
               "create-template", "createnewtemplate",
               new { controller = "Template", action = "CreateTemplate" },
               new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
               "save-template", "savetemplate",
               new { controller = "Template", action = "SaveTemplate" },
               new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
              "remove-detail", "removetemplate/{regTemplateId}/{siteId}",
              new { controller = "Template", action = "RemoveTemplate" },
              new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
              "get-scenario-detail", "editscenario/{scenarioId}",
              new { controller = "Scenario", action = "EditScenario" },
              new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
               "get-copy-scenario", "copyscenario/{scenarioId}",
               new { controller = "Scenario", action = "CopyScenario" },
               new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
              "get-edit-scenario", "scenario/{scenarioId}",
              new { controller = "Scenario", action = "GetScenarioDetails" },
              new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
               "get-scenarios-site-sort", "scenarios/{siteId}/{sort}/{direction}",
               new { controller = "Scenario", action = "GetScenariosForSiteWithSort", direction = UrlParameter.Optional },
               new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
               "get-scenarios-site", "scenarios/{siteId}",
               new { controller = "Scenario", action = "GetScenariosForSite" },
               new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
               "get-scenarios-preview-url", "previewurl/{siteId}",
               new { controller = "Scenario", action = "GetPriviewURL" },
               new { httpMethod = new HttpMethodConstraint("GET") });


            routes.MapRoute(
              "get-controls", "controls/{siteId}/{deviceTypeId}/{scenarioId}",
              new { controller = "Scenario", action = "GetRegControls" },
              new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
              "get-new-scenario", "createscenario",
              new { controller = "Scenario", action = "CreateScenario" },
              new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
               "get-templates-site", "templates/{siteId}/{type}",
               new { controller = "Scenario", action = "GetTemplates" },
               new { httpMethod = new HttpMethodConstraint("GET") });
            
            routes.MapRoute(
              "get-toggle-scenario", "scenario/toggle/{scenarioId}",
              new { controller = "Scenario", action = "ToggleRegScenario" },
              new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
              "post-new-scenario", "savescenario",
              new { controller = "Scenario", action = "SaveScenario" },
              new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
              "post-save-active-weights", "saveactiveweights",
              new { controller = "Scenario", action = "SaveActiveAndWeights" },
              new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
              "get-is-scenario-name-taken", "IsScenarioNameTaken",
              new { controller = "Scenario", action = "IsScenarioNameTaken" },
              new { httpMethod = new HttpMethodConstraint("POST") });
            
            routes.MapRoute(
              "post-save-field", "SaveField",
              new { controller = "Scenario", action = "SaveField" },
              new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
              "post-save-page", "SaveStep",
              new { controller = "Scenario", action = "SaveStep" },
              new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "Root",
                "{siteId}/{scenarioId}",
                new { controller = "Scenario", action = "Index", siteId = UrlParameter.Optional, scenarioId = UrlParameter.Optional },
                new { httpMethod = new HttpMethodConstraint("GET") }
                );
        }

        protected void Application_Start()
        {
            var configFile = new System.IO.FileInfo(Server.MapPath("/") + "log4net.config");
            log4net.Config.XmlConfigurator.ConfigureAndWatch(configFile);

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            // Register your new model binder
            ModelBinders.Binders.DefaultBinder = new ModelBinderWithEnums();

           // Logger.Configure(Server.MapPath("/") + "log4net.config");
        }
    }
}
