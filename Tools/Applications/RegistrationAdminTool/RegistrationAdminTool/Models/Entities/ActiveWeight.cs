﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistrationAdminTool.Models.Entities
{
    public class ActiveWeight
    {
        public int ScenarioID { get; set; }
        public bool Active { get; set; }
        public double Weight { get; set; }
    }
}