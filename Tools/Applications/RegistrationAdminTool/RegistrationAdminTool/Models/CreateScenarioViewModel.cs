﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.Registration;

namespace RegistrationAdminTool.Models
{
    public class CreateScenarioViewModel: BaseViewModel
    {
        public List<RegControl> SiteControls;
        public int SiteId { get; set; }
        public int DeviceTypeId { get; set; }
        public string SiteName { get; set; }
        public string DeviceTypeName { get; set; }

        public CreateScenarioViewModel()
        {
        }

    }
}