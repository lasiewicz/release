﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.Registration;
using RegistrationAdminTool.Helpers;

namespace RegistrationAdminTool.Models
{
    public class RegControlDisplay
    {
        public int RegControlID { get; set; }
        public int RegControlSiteID { get; set; }
        public int DisplayType { get; set; }
        public string Name { get; set; }
        public bool Required { get; set; }
        public bool AutoAdvance { get; set; }
        public string ErrorMessage { get; set; }
        public string Label { get; set; }
        public string AdditionalText { get; set; }
        public string DisplayTypeName { get; private set; }
        public bool RegControlScenarioRequired { get; set; }

        public RegControlDisplay()
        {
            
        }

        public RegControlDisplay(RegControl control)
        {
            RegControlID = control.RegControlID;
            RegControlSiteID = control.RegControlSiteID;
            DisplayType = (int)control.DisplayType;
            Name = control.Name;
            Required = control.Required;
            AutoAdvance = control.EnableAutoAdvance;
            ErrorMessage = control.RequiredErrorMessage;
            Label = control.Label;
            AdditionalText = control.AdditionalText;
            
            if(AutoAdvance)
            {
                DisplayTypeName = DisplayTypeHelper.GetDisplayTypeName(DisplayType) + "-Auto";
            }
            else
            {
                DisplayTypeName = DisplayTypeHelper.GetDisplayTypeName(DisplayType);
            }

        }
    }
}