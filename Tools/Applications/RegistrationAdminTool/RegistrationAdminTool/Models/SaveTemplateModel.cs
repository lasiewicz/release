﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.Registration;

namespace RegistrationAdminTool.Models
{    
    public class SaveTemplateModel
    {
        [Required(ErrorMessage = "A Site ID is required.")]
        [Range(4,9999, ErrorMessage = "Site ID must be greater than 0.")]
        public int SiteId { get; set; }

        [Required(ErrorMessage = "A Template Name is required.")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Template Name must be greater than 1 character.")]
        public string Name { get; set; }

        public TemplateType TemplateType { get; set; }
    }
}