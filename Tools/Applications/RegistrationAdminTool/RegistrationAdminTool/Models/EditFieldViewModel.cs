﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.Registration;

namespace RegistrationAdminTool.Models
{
    public class EditFieldViewModel
    {
        public List<RegControlDisplay> Controls { get; set; }
        public string Label { get; set; }
        public string ErrorMessage { get; set; }
        public string AdditionalText { get; set; }
        public bool AutoAdvance { get; set; }
        public bool Required { get; set; }
        public int ScenarioID { get; set; }
        public int DisplayType { get; set; }
    }
}