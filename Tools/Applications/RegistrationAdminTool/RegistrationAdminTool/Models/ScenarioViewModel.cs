﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.Registration;
using RegistrationAdminTool.Helpers;

namespace RegistrationAdminTool.Models
{
    public class ScenarioViewModel : BaseViewModel
    {
        public int RegScenarioId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public bool IsAutoAdvance { get; set; }
        public string DeviceType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastEditedDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedDisplayInfo { get; set; }
        public string UpdatedDisplayInfo { get; set; }
        public string LastEditedBy { get; set; }
        public int SplitPercentage { get; set; }
        public string Url { get; set; }
        public int StepCount { get; set; }
        public int ControlCount { get; set; }
        public List<Step> Steps { get; set; }
      
        public ScenarioViewModel(Scenario scenario)
        {
            Steps = scenario.Steps;
            RegScenarioId = scenario.ID;
            Name = scenario.Name;
            IsActive = scenario.Active;
            Description = scenario.Description;
            IsAutoAdvance = false;
            foreach (Step step in scenario.Steps)
            {
                foreach (RegControl control in step.Controls.Where(control => control.EnableAutoAdvance))
                {
                    IsAutoAdvance = true;
                    break;
                }
            }
            DeviceType = scenario.DeviceType.ToString();
            switch(scenario.DeviceType)
            {
                case Matchnet.Content.ValueObjects.Registration.DeviceType.Desktop: 
                    DeviceType = "FWS";
                    break;
                case Matchnet.Content.ValueObjects.Registration.DeviceType.Handeheld:
                    DeviceType = "MOS";
                    break;
                case Matchnet.Content.ValueObjects.Registration.DeviceType.Tablet:
                    DeviceType = "TOS";
                    break;
            }
            CreatedBy = scenario.CreatedBy;
            CreateDate = scenario.CreateDate;
            LastEditedBy = scenario.UpdatedBy;
            LastEditedDate = scenario.UpdateDate;
            CreatedDisplayInfo = "";
            if (CreateDate.HasValue && CreateDate.Value > DateTime.MinValue)
            {
                CreatedDisplayInfo = CreateDate.Value.ToShortDateString();
                if (!string.IsNullOrEmpty(scenario.CreatedBy))
                {
                    CreatedDisplayInfo += " by " + scenario.CreatedBy; 
                }
            }
            UpdatedDisplayInfo = "";
            if (LastEditedDate.HasValue && LastEditedDate.Value > DateTime.MinValue)
            {
                UpdatedDisplayInfo = LastEditedDate.Value.ToShortDateString();
                if (!string.IsNullOrEmpty(scenario.UpdatedBy))
                {
                    UpdatedDisplayInfo += " by " + scenario.UpdatedBy;
                }
            }
            SplitPercentage = (int)(scenario.Weight*100);
            Url = string.Format("{0}?scenario={1}", SiteHelper.GetRegURL(scenario.SiteID), HttpUtility.HtmlEncode(scenario.Name));
            StepCount = scenario.Steps.Count;
            ControlCount = scenario.Steps.Sum(step => step.Controls.Count);
        }
    }
}