﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.Registration;
using RegistrationAdminTool.Helpers;

namespace RegistrationAdminTool.Models
{
    public class EditScenarioViewModel : BaseViewModel
    {
        public List<SelectListItem> SplashTemplates;
        public List<SelectListItem> RegistrationTemplates;
        public List<SelectListItem> ConfirmationTemplates;
        public int SplashTemplateId { get; set; }
        public int RegistrationTemplateId { get; set; }
        public int ConfirmationTemplateId { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public int DeviceTypeId { get; set; }
        public int RegScenarioId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public int SplitPercentage { get; set; }
        public List<Step> Steps { get; set; }
        public double Weight { get; set; }
        public string DeviceTypeName { get; set; }

        public EditScenarioViewModel(Scenario scenario)
        {            
            Steps = scenario.Steps;
            RegScenarioId = scenario.ID;
            Name = scenario.Name;
            IsActive = scenario.Active;
            Description = scenario.Description;
            DeviceTypeId = (int)scenario.DeviceType;
            SplitPercentage = (int)(scenario.Weight*100);
            SiteId = scenario.SiteID;
            SiteName = SiteHelper.GetSiteName(scenario.SiteID);
            SplashTemplateId = scenario.SplashTemplate.ID;
            RegistrationTemplateId = scenario.RegistrationTemplate.ID;
            ConfirmationTemplateId = scenario.ConfirmationTemplate.ID;
            Weight = scenario.Weight;
            DeviceTypeName = DeviceTypeHelper.GetDeviceName((int)scenario.DeviceType);
            SetControlsToOverrideValues();

        }

        private void SetControlsToOverrideValues()
        {
            if(Steps != null)
            {
                foreach(var step in Steps)
                {
                    if(step.Controls != null)
                    {
                        foreach(var control in step.Controls)
                        {
                            if(control.ScenarioOverrides != null && control.ScenarioOverrides.Count ==1)
                            {
                                control.AdditionalText = control.ScenarioOverrides[0].AdditionalText;
                                control.Label = control.ScenarioOverrides[0].Label;
                                control.RequiredErrorMessage = control.ScenarioOverrides[0].RequiredErrorMessage;
                                control.Required = control.ScenarioOverrides[0].Required;
                                control.EnableAutoAdvance = control.ScenarioOverrides[0].EnableAutoAdvance;
                                control.DisplayType = control.ScenarioOverrides[0].DisplayType;
                            }
                        }
                    }
                }
            }
        }
    }
}