﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistrationAdminTool.Models
{
    public class LayoutViewModel 
    {
        public bool CreateScenarioLinkActive { get; set; }
        public bool TemplateManagerLinkActive { get; set; }
        public bool ClearCacheLinkActive { get; set; }

        public LayoutViewModel()
        {
            ClearCacheLinkActive = true;
            CreateScenarioLinkActive = true;
            TemplateManagerLinkActive = true;
        }
    }
}
