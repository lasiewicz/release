﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistrationAdminTool.Models.JSONResult
{
    public enum ResultStatus : int
    {
        Success = 2,
        Failed = 3
    }
    
    /// <summary>
    /// Base class for returning JSON results for API calls
    /// </summary>
    [Serializable]
    public class ResultBase
    {
        public int Status { get; set; }
        public string StatusMessage { get; set; }

        public ResultBase()
        {
            Status = (int) ResultStatus.Failed;
            StatusMessage = "";
        }
    }
}