﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistrationAdminTool.Models.JSONResult
{
    public class ScenarioNameCheckResult : ResultBase
    {
        public bool IsNameTaken { get; set; }
    }
   
}
