﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistrationAdminTool.Models.JSONResult
{
    public class SaveScenarioResult: APIResult
    {
        public List<RegControlDisplay> MissingRegControls { get; set; }
        public int ScenarioId { get; set; }
    }
    
}