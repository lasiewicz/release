﻿using RegistrationAdminTool.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistrationAdminTool.Models
{
    public class BaseViewModel
    {
        public LayoutViewModel LayoutViewModel { get; private set; }

        private List<SelectListItem> _Sites;
        public List<SelectListItem> Sites
        {
            get
            {
                if (_Sites == null)
                {
                    _Sites = SiteHelper.GetSitesAsSelectList(0);
                }
                return _Sites;
            }
            set
            {
                _Sites = value;
            }
        }

        private List<SelectListItem> _DeviceTypes;
        public List<SelectListItem> DeviceTypes
        {
            get
            {
                if (_DeviceTypes == null)
                {
                    _DeviceTypes = DeviceTypeHelper.GetDeviceTypesAsSelectList(0);
                }
                return _DeviceTypes;
            }
            set
            {
                _DeviceTypes = value;
            }
        }

        public BaseViewModel()
        {
            LayoutViewModel = new LayoutViewModel();
        }
    }
}