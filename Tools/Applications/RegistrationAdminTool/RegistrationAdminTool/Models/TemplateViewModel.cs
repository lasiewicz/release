﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.Registration;
using RegistrationAdminTool.Helpers;

namespace RegistrationAdminTool.Models
{
    public class TemplateModel : SelectListItem
    {
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
        private string _name = string.Empty;
        private int _id = 0;
        public int ID
        {
            get { return _id; }
            set { 
                _id = value;
                this.Value = value.ToString();
            }
        }

        public string Name
        {
            get { return this._name; }
            set
            {
                this._name = value;
                this.Text = value;
            }
        }
        public TemplateType Type { get; set; }
        public int SiteID { get; set; }

        
        public static TemplateModel Parse(Template template)
        {
            TemplateModel templateModel = new TemplateModel();
            templateModel.ID = template.ID;
            templateModel.Name = template.Name;
            templateModel.Type = template.Type;
            templateModel.SiteID = template.SiteID;
            return templateModel;
        }
    }

    public class TemplateViewModel: BaseViewModel
    {
        public Dictionary<SiteModel, Dictionary<TemplateType, List<TemplateModel>>> Templates { get; set; }
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
        public int SiteId { get; set; }
        public TemplateType TemplateType { get; set; }
        public List<SelectListItem> Sites { get; set; }
        public List<SelectListItem> TemplateTypes { get; set; } 

        public TemplateViewModel()
        {
        }

        public static TemplateViewModel Parse (Dictionary<int, List<Template>> templatesDict)
        {
            TemplateViewModel templateViewModel = new TemplateViewModel();
            foreach (int siteId in templatesDict.Keys)
            {
                List<Template> templates = templatesDict[siteId];
                foreach (Template t in templates)
                {
                    if(null == templateViewModel.Templates) templateViewModel.Templates = new Dictionary<SiteModel, Dictionary<TemplateType, List<TemplateModel>>>();
                    TemplateModel templateModel = TemplateModel.Parse(t);
                    var siteModel = GetSiteModel(siteId);
                    if (!templateViewModel.Templates.ContainsKey(siteModel)) templateViewModel.Templates[siteModel] = new Dictionary<TemplateType, List<TemplateModel>>();
                    if (!templateViewModel.Templates[siteModel].ContainsKey(templateModel.Type)) templateViewModel.Templates[siteModel][templateModel.Type] = new List<TemplateModel>();
                    templateViewModel.Templates[siteModel][templateModel.Type].Add(templateModel);
                }
            }
            return templateViewModel;
        }

        private static SiteModel GetSiteModel(int siteId)
        {
            SiteModel siteModel = new SiteModel(siteId, SiteHelper.GetSiteName(siteId));
            return siteModel;
        }
    }

    public class SiteModel : SelectListItem
    {
        private readonly int _siteId = 0;
        private readonly string _siteName = string.Empty;

        public int SiteId
        {
            get { return _siteId; }
        }

        public string SiteName
        {
            get { return _siteName; }
        }

        public SiteModel(int siteId, string siteName)
        {
            _siteId = siteId;
            this.Value = _siteId.ToString();
            _siteName = siteName;
            this.Text = _siteName;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is SiteModel)) return false;
            SiteModel sm = (SiteModel)obj;
            return (sm.SiteId == this.SiteId);
        }

        public override int GetHashCode()
        {
            return SiteId;
        }
    }
}