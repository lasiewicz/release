﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Registration;

namespace RegistrationAdminTool.Models
{
    public class ScenariosViewModel: BaseViewModel
    {
        public List<Scenario> Scenarios;
        public int SiteId { get; set; }
        public int ScenarioId { get; set; }
        public int DeviceTypeId { get; set; }
        
        public ScenariosViewModel()
        {
        }
    }
}