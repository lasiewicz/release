﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistrationAdminTool.Models
{
    public class ModelBinderWithEnums : DefaultModelBinder
    {
        /// <summary>
        /// Fix for the default model binder's failure to decode enum types when binding to JSON.
        /// </summary>
        protected override object GetPropertyValue(ControllerContext controllerContext, ModelBindingContext bindingContext,
            PropertyDescriptor propertyDescriptor, IModelBinder propertyBinder)
        {
            var propertyType = propertyDescriptor.PropertyType;
            if (propertyType.IsEnum)
            {
                var providerValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                if (null != providerValue)
                {
                    var value = providerValue.RawValue;
                    if (null != value)
                    {
                        var valueType = value.GetType();
                        if (!valueType.IsEnum && valueType.Name.ToLower() == "string")
                        {
                            int parsedInt = 0;
                            if (int.TryParse(value.ToString(), out parsedInt))
                            {
                                return Enum.ToObject(propertyType, parsedInt);
                            }
                            else
                            {
                                return value;
                            }
                        }else if (!valueType.IsEnum && valueType.Name.ToLower() == "string[]"){
                            int parsedInt = 0;
                            if (int.TryParse(((string[])(value))[0], out parsedInt))
                            {
                                return Enum.ToObject(propertyType, parsedInt);
                            }
                            else
                            {
                                return value;
                            }
                        }
                        else if (!valueType.IsEnum)
                        {
                            return Enum.ToObject(propertyType, value);
                        }
                    }
                }
            }
            return base.GetPropertyValue(controllerContext, bindingContext, propertyDescriptor, propertyBinder);
        }
    }
}