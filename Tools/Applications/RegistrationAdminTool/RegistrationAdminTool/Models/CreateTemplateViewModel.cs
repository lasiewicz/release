﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.Registration;

namespace RegistrationAdminTool.Models
{
    public class CreateTemplateViewModel : BaseViewModel
    {
        public string ErrorMessage { get; set; }
        public int SiteId { get; set; }
        public string Name { get; set; }
        public TemplateType TemplateType { get; set; }
        public List<SelectListItem> TemplateTypes { get; set; }

        public CreateTemplateViewModel()
        {
        }
    }
}