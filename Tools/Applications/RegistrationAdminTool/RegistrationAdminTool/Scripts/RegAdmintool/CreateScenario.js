﻿var spark = spark || {};

spark.CreateScenarioManager = spark.CreateScenarioManager || {

    initialized: false,
    selectedControls: null,
    allControls: null,
    allSteps: null,
    currentMaxPage: 0,
    generalAjaxInProgress: false,
    scenarioToSave: spark.RegAdminManager.NewScenario(),

    addRegControlToCollection: function (serverControl, controlCollection) {
        spark.RegAdminManager.regDebug('createScenario.addRegControlToCollection()');

        var newControl = spark.RegAdminManager.NewRegControlDisplay();
        newControl.RegControlID = serverControl.RegControlID;
        newControl.RegControlSiteID = serverControl.RegControlSiteID;
        newControl.AdditionalText = serverControl.AdditionalText;
        newControl.EnableAutoAdvance = serverControl.AutoAdvance;
        newControl.DisplayType = serverControl.DisplayType;
        newControl.ErrorMessage = serverControl.ErrorMessage;
        newControl.Required = serverControl.Required;
        newControl.Label = serverControl.Label;
        newControl.Name = serverControl.Name;
        newControl.DisplayTypeName = serverControl.DisplayTypeName;
        newControl.RegControlScenarioRequired = serverControl.RegControlScenarioRequired;
        controlCollection.AddControl(newControl, false);
    },

    initializeNewScenario: function () {
        spark.RegAdminManager.regDebug('createScenario.initializeNewScenario()');
        this.currentMaxPage = 0;
        this.allSteps = spark.RegAdminManager.NewStepCollection();
    },

    initializeExistingScenario: function (scenarioId) {
        spark.RegAdminManager.regDebug('createScenario.initializeExistingScenario()');
        this.currentMaxPage = 0;
        this.allSteps = spark.RegAdminManager.NewStepCollection();
        this.scenarioToSave.ID = scenarioId;
        this.makePagesDroppable();
    },

    addControlToPage: function (regcontrolsiteid, pageOrder) {
        spark.RegAdminManager.regDebug('createScenario.addControlToPage()');

        var step = this.allSteps.FindStepByOrder(pageOrder);
        var selectedControl = this.allControls.FindControl(regcontrolsiteid);
        if (selectedControl != null) {
            selectedControl.Order = step.ControlsCollection.Controls.length + 1;
            this.selectedControls.AddControl(selectedControl, false);
            step.ControlsCollection.AddControl(selectedControl, true);

            var page = $("#page" + pageOrder);
            var isNewList = ($("ul", page).length == 0);
            var list = isNewList ? $("<ul class='gallery ui-helper-reset' id='' data-pageOrder='" + pageOrder + "'/>").appendTo(page) : $("ul", page);
            var requiredIndicator = selectedControl.Required ? '' : '**';

            if (selectedControl.RegControlScenarioRequired) {
                $('<li class="drag required" data-regControlSiteId=' + selectedControl.RegControlSiteID + ' data-memberOfPage=' + pageOrder + ' id=selectedControl-' + regcontrolsiteid + '><span>' + selectedControl.Name + '<br>(' + selectedControl.DisplayTypeName + ')' + requiredIndicator + '</span></li>').appendTo(list);
            }
            else {
                $('<li class="drag" data-regControlSiteId=' + selectedControl.RegControlSiteID + ' data-memberOfPage=' + pageOrder + ' id=selectedControl-' + regcontrolsiteid + '><span>' + selectedControl.Name + '<br>(' + selectedControl.DisplayTypeName + ')' + requiredIndicator + '</span></li>').appendTo(list);
            }
            $(".drag").draggable({ revert: "invalid" });

            var newItem = $("#selectedControl-" + regcontrolsiteid);
            newItem.removeAttr("style");
            newItem.addClass("selectedControl");
            //newItem.bind("dblclick", { id: regcontrolsiteid },spark.CreateScenarioManager.selectedControlDblClick);
            if ($("img", newItem).length == 0) {
                newItem.append("<img class='recycle' src='../../Images/icon-close.png' />");
            }
        }
    
    },

    removeControlFromPage: function (listItem) {
        spark.RegAdminManager.regDebug('createScenario.removeControlFromPage');
        var availableList = $('#siteControls ul');
        var selectedregcontrolsiteid = parseInt(listItem.attr("data-regcontrolsiteid"));
        var selectedControl = spark.CreateScenarioManager.selectedControls.FindControl(selectedregcontrolsiteid);
        spark.CreateScenarioManager.selectedControls.RemoveControl(selectedControl);

        var pageOrder = parseInt(listItem.parent().attr("data-pageOrder"));
        spark.CreateScenarioManager.scenarioToSave.StepsCollection.Steps[pageOrder - 1].ControlsCollection.RemoveControl(selectedControl);
        spark.RegAdminManager.regDebug('createScenario.removeControlFromPage - removed from page ' + pageOrder);

        listItem.find('img').remove();
        listItem.removeClass('selectedControl');
        listItem.attr("style", "position: relative");
        listItem.removeAttr("data-memberOfPage");
        listItem.attr('id', 'availableControl-' + selectedregcontrolsiteid);
        listItem.appendTo(availableList);
    },

    removeControlFromAvailableList: function (regcontrolsiteid) {
        spark.RegAdminManager.regDebug('createScenario.removeControlFromAvailableList()');
        $('#availableControl-' + regcontrolsiteid).remove();
    },

    addNewPage: function (container, stepId) {
        spark.RegAdminManager.regDebug('createScenario.addNewPage()');
        if (arguments.length == 1) {stepId = 0;}
    
        this.currentMaxPage++;
        var newStep = spark.RegAdminManager.NewStep();
        newStep.Order = this.currentMaxPage;
        newStep.StepId = stepId;
        this.allSteps.AddStep(newStep);
        this.scenarioToSave.StepsCollection.AddStep(newStep);

        $("#" + container).append(this.getNewPageHTML(this.currentMaxPage));
        //$("#pagelink" + this.currentMaxPage).bind("dblclick", { order: this.currentMaxPage, id: stepId }, spark.CreateScenarioManager.selectedPageDblClick);

        this.makePagesDroppable();
    },

    addExistingPage: function (container, stepId, title, tip, header) {
        spark.RegAdminManager.regDebug('createScenario.addExistingPage()');
        this.currentMaxPage++;
        var newStep = spark.RegAdminManager.NewStep();
        newStep.Order = this.currentMaxPage;
        newStep.StepId = stepId;
    
        if (title) {
            newStep.TitleText = title;
        }
        if (tip) {
            newStep.TipText = tip;
        }
        if (header) {
            newStep.HeaderText = header;
        }
    
        this.allSteps.AddStep(newStep);
        this.scenarioToSave.StepsCollection.AddStep(newStep);

        $("#" + container).append(this.getNewPageHTML(this.currentMaxPage));
        //$("#pagelink" + this.currentMaxPage).bind("dblclick", { order: this.currentMaxPage, id: stepId }, spark.CreateScenarioManager.selectedPageDblClick);

        this.makePagesDroppable();
    },

    getNewPageHTML: function (pageOrder) {
        spark.RegAdminManager.regDebug('createScenario.getNewPageHTML()');
        return "<div id='page" + pageOrder + "' class='scenariopage' data-order='" + pageOrder + "'><span id='pagelink" + pageOrder + "' class='node-link'> Page " + pageOrder + "</span> <img class='page-recycle' src='../../Images/icon-close.png' /></div>";    
    },

    selectedControlDblClick: function (event) {
        spark.RegAdminManager.regDebug('createScenario.selectedControlDblClick()');
        initializeEditField(spark.CreateScenarioManager.selectedControls, event.data.id, spark.CreateScenarioManager.scenarioToSave.ID);
        $('#promptEditField').lightboxme({
            centered: true
        });
    },

    selectedPageDblClick: function (event) {
        spark.RegAdminManager.regDebug('createScenario.selectedPageDblClick()');
        spark.RegAdminManager.regLog(event.data.id);
        initializeEditPage(spark.CreateScenarioManager.allSteps, event.data.order, spark.CreateScenarioManager.scenarioToSave.ID);
        $('#promptEditPage').lightboxme({
            centered: true
        });
        return false;
    },

    makePagesDroppable: function () {
        spark.RegAdminManager.regDebug('createScenario.makePagesDroppable()');
        $(".scenariopage").droppable({
            drop: function (event, ui) {
                spark.RegAdminManager.regDebug('createScenario.makePagesDroppable.drop');

                var item = ui.draggable;
                var page = $(this);
                var pageOrder = parseInt(page.attr("data-order"));
                var controlListId = page.attr('id') + 'controlList';
                var regcontrolsiteid = parseInt(item.attr("data-regcontrolsiteid"));
            
                item.addClass("selectedControl");
                //item.bind("dblclick", { id: regcontrolsiteid }, spark.CreateScenarioManager.selectedControlDblClick);

                var step = spark.CreateScenarioManager.allSteps.FindStepByOrder(pageOrder);
                var selectedControl = spark.CreateScenarioManager.allControls.FindControl(regcontrolsiteid);
                if (selectedControl) {
                    spark.CreateScenarioManager.selectedControls.AddControl(selectedControl, false);
                    step.ControlsCollection.AddControl(selectedControl, true);

                    if (item.attr('data-memberOfPage')) {
                        var previousPageOrder = parseInt(item.attr("data-memberOfPage"));
                        if (previousPageOrder > 0 && spark.CreateScenarioManager.scenarioToSave.StepsCollection.Steps.length >= previousPageOrder) {
                            if (previousPageOrder != pageOrder) {
                                //this means field was moved from another page, so need to remove it from there
                                spark.RegAdminManager.regDebug('createScenario.makePagesDroppable.drop - removed from page ' + previousPageOrder + ', added to page ' + pageOrder);
                                spark.CreateScenarioManager.scenarioToSave.StepsCollection.Steps[previousPageOrder - 1].ControlsCollection.RemoveControl(selectedControl);
                            }
                            else {
                                //this means field was re-arranged in same page
                                spark.RegAdminManager.regDebug('createScenario.makePagesDroppable.drop - re-arranged on page ' + pageOrder);
                            }
                        }
                        else {
                            spark.RegAdminManager.regDebug('createScenario.makePagesDroppable.drop - v1 adding on page ' + pageOrder);
                        }
                    }
                    else {
                        spark.RegAdminManager.regDebug('createScenario.makePagesDroppable.drop - v2 adding on page ' + pageOrder);
                    }

                    item.attr("data-memberOfPage", pageOrder);

                    var isNewList = ($("ul", page).length == 0);
                    var list = isNewList ? $("<ul class='gallery ui-helper-reset' id='" + controlListId + "' data-pageOrder='" + pageOrder + "'/>").appendTo(page) : $("ul", page);
                    if (isNewList == true) {
                        $("#" + controlListId).sortable();
                        $("#" + controlListId).disableSelection();
                    }
                    item.removeAttr("style");
                    if ($("img", item).length == 0) {
                        item.append("<img class='recycle' src='../../Images/icon-close.png' />");
                    }
                    item.attr('id', 'selectedControl-' + regcontrolsiteid);
                    item.appendTo(list);
                }
            }, tolerance: "pointer"
        });
    },

    getControlsForSiteAndDevice: function (siteid, deviceTypeId, scenarioId) {
        spark.RegAdminManager.regDebug('createScenario.getControlsForSiteAndDevice()');
        if (arguments.length == 2) {scenarioId = 0; }
    
        var $siteControls = $('#siteControls');
        //grab regcontrols for the site
        $.ajax({
            type: "GET",
            async: false,
            url: "/controls/" + siteid + "/" + deviceTypeId + "/" + scenarioId,
            success: function (result) {
                if (result != null) {
                    spark.CreateScenarioManager.selectedControls = spark.RegAdminManager.NewRegControlDisplayCollection();
                    spark.CreateScenarioManager.allControls = spark.RegAdminManager.NewRegControlDisplayCollection();

                    $siteControls.empty();
                    var $list = $("<ul/>").appendTo($siteControls);
                    $.each(result, function (i, item) {
                        spark.CreateScenarioManager.addRegControlToCollection(item, spark.CreateScenarioManager.allControls);

                        //render available fields
                        var requiredIndicator = item.Required ? '' : '**';
                        if (item.RegControlScenarioRequired) {
                            $('<li class="drag required" data-regControlSiteId=' + item.RegControlSiteID + ' id=availableControl-' + item.RegControlSiteID + '><span>' + item.Name + ' <br>(' + item.DisplayTypeName + ')' + requiredIndicator + '</span></li>').appendTo($list);
                        }
                        else {
                            $('<li class="drag" data-regControlSiteId=' + item.RegControlSiteID + ' id=availableControl-' + item.RegControlSiteID + '><span>' + item.Name + ' <br>(' + item.DisplayTypeName + ')' + requiredIndicator + '</span></li>').appendTo($list);
                        }
                    });

                    $siteControls.show();

                    $(".drag").draggable({ revert: "invalid" });

                    $siteControls.droppable();

                    spark.CreateScenarioManager.makePagesDroppable();

                    //Clear items from pages
                    var pages = $('#pages');
                    pages.find('.scenariopage').each(function () {
                        $(this).find('ul').remove();
                    });
                }
            }
        });
    },

    initializePage: function (splashTemplateId, registrationTemplateId, confirmationTemplateId) {
        spark.RegAdminManager.regDebug('createScenario.initializePage()');
        $('#divCreateError').hide();

        var siteid = $("#hdnSiteId").val();
        var deviceTypeId = $("#hdnDeviceTypeId").val(); 
        if (siteid > 0) {
            this.getControlsForSiteAndDevice(siteid, deviceTypeId);
            if (arguments.length == 3) {
                this.bindTemplates(siteid, splashTemplateId, registrationTemplateId, confirmationTemplateId);
            } else {
                this.bindTemplates(siteid);
            }
        }
    },

    bindTemplates: function (siteId, splashTemplateId, registrationTemplateId, confirmationTemplateId) {
        spark.RegAdminManager.regDebug('createScenario.bindTemplates()');
        spark.RegAdminManager.regLog(splashTemplateId + " " + registrationTemplateId + " " + confirmationTemplateId);
        $.ajax({
            type: "GET",
            url: "/templates/" + siteId + "/1",
            success: function (result) {
                $('#ddlSplashTemplates').empty();
                for (var i = 0; i < result.length; i++) {
                    if (arguments.length == 4 && splashTemplateId == result[i].ID) {
                        $('<option selected/>').val(result[i].ID).html(result[i].Name).appendTo('#ddlSplashTemplates');
                    } else {
                        $('<option/>').val(result[i].ID).html(result[i].Name).appendTo('#ddlSplashTemplates');
                    }
                
                }
            }
        });

        $.ajax({
            type: "GET",
            url: "/templates/" + siteId + "/2",
            success: function (result) {
                $('#ddlRegistrationTemplates').empty();
                for (var i = 0; i < result.length; i++) {
                    if (arguments.length == 4 && registrationTemplateId == result[i].ID) {
                        $('<option selected/>').val(result[i].ID).html(result[i].Name).appendTo('#ddlRegistrationTemplates');
                    } else {
                        $('<option/>').val(result[i].ID).html(result[i].Name).appendTo('#ddlRegistrationTemplates');
                    }
                }
            }
        });

        $.ajax({
            type: "GET",
            url: "/templates/" + siteId + "/3",
            success: function (result) {
                $('#ddlConfirmationTemplates').empty();
                for (var i = 0; i < result.length; i++) {
                    if (arguments.length == 4 && confirmationTemplateId == result[i].ID) {
                        $('<option selected />').val(result[i].ID).html(result[i].Name).appendTo('#ddlConfirmationTemplates');
                    } else {
                        $('<option/>').val(result[i].ID).html(result[i].Name).appendTo('#ddlConfirmationTemplates');
                    }
                }
            }
        });
    },

    validateExistingScenario: function () {
        spark.RegAdminManager.regDebug('createScenario.validateExistingScenario()');
        $('#spnScenarioNameError').hide();
        $('#spnDescriptionError').hide();
        $('#divPageControlErrors').hide();
    
        //verify name is supplied and not a duplicate
        var name = $('#txtScenarioName').val();
        var siteId = $('#hdnSiteId').val();
        if (!name || name === "") {
            $('#spnScenarioNameError').show();
            $('#spnScenarioNameError').html("Please supply a name");
            return false;
        } else {
            var isTaken = this.isScenarioNameTakenForExistingScenario(siteId, name, this.scenarioToSave.ID);
            if (isTaken == true) {
                $('#spnScenarioNameError').show();
                $('#spnScenarioNameError').html("'" + name + "' is already taken.");
                return false;
            }
        }

        //verify description is supplied
        var description = $('#txtDescription').val();
        if (!description || description === "") {
            $('#spnDescriptionError').show();
            $('#spnDescriptionError').html("Please supply a description");
            return false;
        }

        //make sure scenario has at least one page
        if (this.scenarioToSave.StepsCollection.Steps.length == 0) {
            $('#divPageControlErrors').show();
            $('#divPageControlErrors').html("Scenarios must have at least one page with controls.");
            return false;
        }

        //make sure there are no empty pages
        var pagesErrorString = '';
        for (var idx in this.scenarioToSave.StepsCollection.Steps) {
            if (this.scenarioToSave.StepsCollection.Steps[idx].ControlsCollection.Controls.length == 0) {
                pagesErrorString = pagesErrorString + "Page " + (parseInt(idx) + 1) + " has no controls<br>";
            }
        }
        if (pagesErrorString != '') {
            $('#divPageControlErrors').show();
            $('#divPageControlErrors').html(pagesErrorString);
            return false;
        }
        return true;
    },

    validateNewScenario: function () {
        spark.RegAdminManager.regDebug('validateNewScenario()');
        $('#spnSiteListError').hide();
        $('#spnScenarioNameError').hide();
        $('#spnDescriptionError').hide();
        $('#divPageControlErrors').hide();
    

        //verify siteid
        var siteid = $('#hdnSiteId').val();
        if (siteid <= 0) {
            $('#spnSiteListError').show();
            $('#spnSiteListError').html("Please select a site");
            return false;
        }

        //verify name is supplied and not a duplicate
        var name = $('#txtScenarioName').val();
        if (!name || name === "") {
            $('#spnScenarioNameError').show();
            $('#spnScenarioNameError').html("Please supply a name");
            return false;
        } else {
            var isTaken = this.isScenarioNameTaken(siteid, name);
            if(isTaken==true) {
                $('#spnScenarioNameError').show();
                $('#spnScenarioNameError').html("'" + name + "' is already taken.");
                return false;
            }
        }

        //verify description is supplied
        var description = $('#txtDescription').val();
        if (!description || description === "") {
            $('#spnDescriptionError').show();
            $('#spnDescriptionError').html("Please supply a description");
            return false;
        }

        //make sure scenario has at least one page
        if (this.scenarioToSave.StepsCollection.Steps.length == 0) {
            $('#divPageControlErrors').show();
            $('#divPageControlErrors').html("Scenarios must have at least one page with controls.");
            return false;
        }

        //make sure there are no empty pages
        var pagesErrorString = '';
        for (var idx in this.scenarioToSave.StepsCollection.Steps) {
            if (this.scenarioToSave.StepsCollection.Steps[idx].ControlsCollection.Controls.length == 0) {
                pagesErrorString = pagesErrorString + "Page " + (parseInt(idx)+1) + " has no controls<br>";
            }
        }
        if (pagesErrorString != '') {
            $('#divPageControlErrors').show();
            $('#divPageControlErrors').html(pagesErrorString);
            return false;
        }
        return true;
    },

    isScenarioNameTaken: function (siteId, nameToCheck) {
        spark.RegAdminManager.regDebug('isScenarioNameTaken()');
        var taken  = true;

        $.ajax({
            type: "POST",
            async:false,
            url: "/IsScenarioNameTaken/",
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            data: JSON.stringify({ siteId:siteId, name: nameToCheck }),
            success: function (result) {
                if (result != null && result.Status == 2 && result.IsNameTaken == false) {
                    taken = false;
                }
            }
        });

        return taken;
    },

    isScenarioNameTakenForExistingScenario: function (siteId, nameToCheck, scenarioId) {
        spark.RegAdminManager.regDebug('isScenarioNameTakenForExistingScenario()');
        var taken = true;

        $.ajax({
            type: "POST",
            async: false,
            url: "/IsScenarioNameTaken/",
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            data: JSON.stringify({ siteId:siteId, name: nameToCheck, existingScenarioId: scenarioId }),
            success: function (result) {
                if (result != null && result.Status == 2 && result.IsNameTaken == false) {
                    taken = false;
                }
            }
        });

        return taken;
    },

    showCreateError: function (result) {
        spark.RegAdminManager.regDebug('showCreateError()');
        $('#divCreateError').show();
        var errorMsg = '';
        if (result.MissingRegControls && result.MissingRegControls.length > 0) {
            errorMsg += "<div>Please add the following required controls before saving: ";
            for (var i = 0; i < result.MissingRegControls.length; i++) {
                var regControl = result.MissingRegControls[i];
                errorMsg += regControl.Name;
                if(i < result.MissingRegControls.length-1) errorMsg+= ", ";
            }
            errorMsg += "</div>";
        } else {
            errorMsg += "There was an error creating the scenario. Please contact technical support.";
        }
        $('#divCreateError').html(errorMsg);
    },

    initialize: function (){
        $('.recycle').live("click", function () {
            spark.RegAdminManager.regDebug('createScenario.initialize.recycle.click');
            var image = $(this);
            var listItem = image.parent();
            spark.CreateScenarioManager.removeControlFromPage(listItem);
            $(".drag").draggable({ revert: "invalid" });
        });

        $('#add-page').live("click", function () {
            spark.RegAdminManager.regDebug('createScenario.initialize.add-page.click');
            spark.CreateScenarioManager.addNewPage("pages");
            spark.CreateScenarioManager.makePagesDroppable();
        });

        $('.page-recycle').live("click", function () {
            spark.RegAdminManager.regDebug('createScenario.initialize.page-recycle.click');
            var image = $(this);
            var page = image.parent();
            var order = parseInt(page.attr("data-order"));
            page.find('li').each(function () {
                var listItem = $(this);
                spark.CreateScenarioManager.removeControlFromPage(listItem);
            });
            $(".drag").draggable({ revert: "invalid" });

            //If this is the last page, remove it
            var pageId = page.attr('id').replace('page', '');
            var totalPages = $('.scenariopage', page.parent()).length;
            if (pageId == totalPages) {
                page.remove();
                var step = spark.CreateScenarioManager.allSteps.FindStepByOrder(order);
                spark.RegAdminManager.regLog(step.Order);
                spark.CreateScenarioManager.allSteps.RemoveStep(step);
                spark.CreateScenarioManager.scenarioToSave.StepsCollection.RemoveStep(step);
                spark.CreateScenarioManager.currentMaxPage--;
            }
        });


        $('#btnSaveEditedScenario').live("click", function () {
            spark.RegAdminManager.regDebug('createScenario.initialize.btnSaveEditedScenario.click');
            var isValid = spark.CreateScenarioManager.validateExistingScenario();
            if (isValid && !spark.CreateScenarioManager.generalAjaxInProgress) {
                spark.CreateScenarioManager.scenarioToSave.Name = $('#txtScenarioName').val();
                spark.CreateScenarioManager.scenarioToSave.Description = $('#txtDescription').val();
                spark.CreateScenarioManager.scenarioToSave.SiteID = $('#hdnSiteId').val();
                spark.CreateScenarioManager.scenarioToSave.DeviceType = $('#hdnDeviceTypeId').val();

                $('#ajaxspin').show();

                var splashTemplateId = $('#ddlSplashTemplates').find(":selected").val();
                var regTemplateId = $('#ddlRegistrationTemplates').find(":selected").val();
                var confirmationTemplateId = $('#ddlConfirmationTemplates').find(":selected").val();
                var scenarioForPost = spark.RegAdminManager.NewScenarioForPost(spark.CreateScenarioManager.scenarioToSave);
                var obj = { scenario: scenarioForPost, splashTemplateId: splashTemplateId, regTemplateId: regTemplateId, confirmationTemplateId: confirmationTemplateId };

                $.ajax({
                    type: "POST",
                    async: false,
                    url: "/savescenario/",
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    data: JSON.stringify(obj),
                    complete: function (result) {
                        spark.CreateScenarioManager.generalAjaxInProgress = false;
                    },
                    success: function (result) {
                        if (result != null && result.Status == 2) {
                            window.location.href = "/" + spark.CreateScenarioManager.scenarioToSave.SiteID;
                        } else {
                            spark.CreateScenarioManager.showCreateError(result);
                        }
                    }
                });

                $('#ajaxspin').hide();
            }
        });


        $('#btnSaveCopiedScenario').live("click", function () {
            spark.RegAdminManager.regDebug('createScenario.initialize.btnSaveCopiedScenario.click');
            var isValid = spark.CreateScenarioManager.validateNewScenario();
            if (isValid && !spark.CreateScenarioManager.generalAjaxInProgress) {
                spark.CreateScenarioManager.scenarioToSave.ID = -1;
                spark.CreateScenarioManager.scenarioToSave.Name = $('#txtScenarioName').val();
                spark.CreateScenarioManager.scenarioToSave.Description = $('#txtDescription').val();
                spark.CreateScenarioManager.scenarioToSave.SiteID = $('#hdnSiteId').val();
                spark.CreateScenarioManager.scenarioToSave.DeviceType = $('#hdnDeviceTypeId').val();
                spark.CreateScenarioManager.scenarioToSave.Weight = $('#hdnWeight').val();
                spark.CreateScenarioManager.scenarioToSave.Active = $('#hdnActive').val();

                $('#ajaxspin').show();

                var splashTemplateId = $('#ddlSplashTemplates').find(":selected").val();
                var regTemplateId = $('#ddlRegistrationTemplates').find(":selected").val();
                var confirmationTemplateId = $('#ddlConfirmationTemplates').find(":selected").val();
                var scenarioForPost = spark.RegAdminManager.NewScenarioForPost(spark.CreateScenarioManager.scenarioToSave);
                var obj = { scenario: scenarioForPost, splashTemplateId: splashTemplateId, regTemplateId: regTemplateId, confirmationTemplateId: confirmationTemplateId };

                $.ajax({
                    type: "POST",
                    async: false,
                    url: "/savescenario/",
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    data: JSON.stringify(obj),
                    complete: function (result) {
                        spark.CreateScenarioManager.generalAjaxInProgress = false;
                    },
                    success: function (result) {
                        if (result != null && result.Status == 2) {
                            window.location.href = "/" + spark.CreateScenarioManager.scenarioToSave.SiteID;
                        } else {
                            spark.CreateScenarioManager.showCreateError(result);
                        }
                    }
                });

                $('#ajaxspin').hide();
            }
        });


        $('#btnSaveScenario').live("click", function () {
            spark.RegAdminManager.regDebug('createScenario.initialize.btnSaveScenario.click');
            var isValid = spark.CreateScenarioManager.validateNewScenario();

            if (isValid && !spark.CreateScenarioManager.generalAjaxInProgress) {
                spark.CreateScenarioManager.scenarioToSave.Name = $('#txtScenarioName').val();
                spark.CreateScenarioManager.scenarioToSave.Description = $('#txtDescription').val();
                spark.CreateScenarioManager.scenarioToSave.SiteID = $('#hdnSiteId').val();
                spark.CreateScenarioManager.scenarioToSave.DeviceType = $('#hdnDeviceTypeId').val();
                spark.CreateScenarioManager.scenarioToSave.Weight = $('#hdnWeight').val();
                spark.CreateScenarioManager.scenarioToSave.Active = $('#hdnActive').val();

                $('#ajaxspin').show();

                var splashTemplateId = $('#ddlSplashTemplates').find(":selected").val();
                var regTemplateId = $('#ddlRegistrationTemplates').find(":selected").val();
                var confirmationTemplateId = $('#ddlConfirmationTemplates').find(":selected").val();
                var scenarioForPost = spark.RegAdminManager.NewScenarioForPost(spark.CreateScenarioManager.scenarioToSave);
                var obj = { scenario: scenarioForPost, splashTemplateId: splashTemplateId, regTemplateId: regTemplateId, confirmationTemplateId: confirmationTemplateId };

                $.ajax({
                    type: "POST",
                    async: false,
                    url: "/savescenario/",
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    data: JSON.stringify(obj),
                    complete: function (result) {
                        spark.CreateScenarioManager.generalAjaxInProgress = false;
                    },
                    success: function (result) {
                        if (result != null && result.Status == 2) {
                            window.location.href = "/" + spark.CreateScenarioManager.scenarioToSave.SiteID;
                        } else {
                            spark.CreateScenarioManager.showCreateError(result);
                        }
                    }
                });

                $('#ajaxspin').hide();
            }
        });
    },

};

$(document).ready(function () {
    if (!spark.CreateScenarioManager.initialized) {
        spark.CreateScenarioManager.initialize();
    }
});

