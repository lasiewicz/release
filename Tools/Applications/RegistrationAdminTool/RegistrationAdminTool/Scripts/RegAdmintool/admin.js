﻿
var spark = spark || {};

spark.RegAdminManager = spark.RegAdminManager || {

    initialized: false,
    regLoadScenariosForSiteInProgress: false,
    currentSort: "name",
    currentSortDirection: "asc",
    previewLinkPrifix:"",

    //this function is for debugging purposes, set to false when deploying to prod later.
    regDebugEnabled: true,
    regDebug: function (s) {
        if (this.regDebugEnabled) {
            if (typeof (console) !== "undefined") {
                console.log(s);
            }
        }
    },

    regLog: function (s) {
        if (typeof (console) !== "undefined") {
            console.log(s);
        }
    },

    SortScenarios: function(sort) {

        if (spark.RegAdminManager.currentSort == sort && spark.RegAdminManager.currentSortDirection == "asc") {
            spark.RegAdminManager.currentSortDirection = "desc";
        }
        else {
            spark.RegAdminManager.currentSortDirection = "asc";
        }

        spark.RegAdminManager.loadScenariosForSite(sort, spark.RegAdminManager.currentSortDirection);
        spark.RegAdminManager.currentSort = sort;

        return false;
    },

    getStepNumberDisplay: function (stepOrder) {
        this.regDebug('getStepNumberDisplay()');

        switch (stepOrder) {
            case 1:
                return "One";
            case 2:
                return "Two";
            case 3:
                return "Three";
            case 4:
                return "Four";
            case 5:
                return "Five";
            case 6:
                return "Six";
            case 7:
                return "Seven";
            case 8:
                return "Eight";
            case 9:
                return "Nine";
            case 10:
                return "Ten";
            case 11:
                return "Eleven";
            case 12:
                return "Twelve";
            case 13:
                return "Thirteen";
            case 14:
                return "Fourteen";
            case 15:
                return "Fifteen";
            case 16:
                return "Sixteen";
            case 17:
                return "Seventeen";
            case 18:
                return "Eighteen";
            case 19:
                return "Nineteen";
            case 20:
                return "Twenty";
            default:
                return "Error";
        }
    },

    //Object definitions
    NewRegControlDisplay: function () {
        return {
            RegControlID: 0,
            RegControlSiteID: 0,
            Order: 0,
            DisplayType: 0,
            Name: '',
            Required: false,
            EnableAutoAdvance: false,
            ErrorMessage: '',
            Label: '',
            AdditionalText: '',
            DisplayTypeName: '',
            RegControlScenarioRequired: false
        }
    },

    NewStep: function () {
        return {
            StepId: 0,
            ScenarioId: 0,
            Order: 0,
            StepOrderDisplay: '',
            TipText: '',
            HeaderText: '',
            TitleText: '',
            Controls: null,
            ControlsCollection: this.NewRegControlDisplayCollection()
            
        }
    },

    NewScenario: function () {
        return {
            ID: 0,
            Name: '',
            Description: '',
            SiteID: 0,
            DeviceType: 0,
            SplashTemplateID: 0,
            RegTemplateID: 0,
            ConfirmationTemplateID: 0,
            Steps: null,
            StepsCollection: this.NewStepCollection(),
            Weight: 0.0,
            Active: false,
            
        }
    },

    NewScenarioForPost: function(scenario){
        var scenarioPost = {};
        scenarioPost.ID = scenario.ID;
        scenarioPost.Name = scenario.Name;
        scenarioPost.Description = scenario.Description;
        scenarioPost.SiteID = scenario.SiteID;
        scenarioPost.DeviceType = scenario.DeviceType;
        scenarioPost.SplashTemplateID = scenario.SplashTemplateID;
        scenarioPost.RegTemplateID = scenario.RegTemplateID;
        scenarioPost.ConfirmationTemplateID = scenario.ConfirmationTemplateID;
        scenarioPost.Weight = scenario.Weight;
        scenarioPost.Active = scenario.Active;
        scenarioPost.Steps = [];

        for (i = 0; i < scenario.StepsCollection.Steps.length; i++) {
            var currentStep = scenario.StepsCollection.Steps[i];
            scenarioPost.Steps[i] = {};
            scenarioPost.Steps[i].StepId = currentStep.StepId;
            scenarioPost.Steps[i].ScenarioId = currentStep.ScenarioId;
            scenarioPost.Steps[i].Order = currentStep.Order;
            scenarioPost.Steps[i].StepOrderDisplay = currentStep.StepOrderDisplay;
            scenarioPost.Steps[i].TipText = currentStep.TipText;
            scenarioPost.Steps[i].HeaderText = currentStep.HeaderText;
            scenarioPost.Steps[i].TitleText = currentStep.TitleText;
            scenarioPost.Steps[i].Controls = [];

            for (j = 0; j < scenario.StepsCollection.Steps[i].ControlsCollection.Controls.length; j++) {
                var currentControl = scenario.StepsCollection.Steps[i].ControlsCollection.Controls[j];
                scenarioPost.Steps[i].Controls[j] = {};
                scenarioPost.Steps[i].Controls[j].RegControlID = currentControl.RegControlID;
                scenarioPost.Steps[i].Controls[j].RegControlSiteID = currentControl.RegControlSiteID;
                scenarioPost.Steps[i].Controls[j].Order = currentControl.Order;
                scenarioPost.Steps[i].Controls[j].DisplayType = currentControl.DisplayType;
                scenarioPost.Steps[i].Controls[j].Name = currentControl.Name;
                scenarioPost.Steps[i].Controls[j].Required = currentControl.Required;
                scenarioPost.Steps[i].Controls[j].EnableAutoAdvance = currentControl.EnableAutoAdvance;
                scenarioPost.Steps[i].Controls[j].ErrorMessage = currentControl.ErrorMessage;
                scenarioPost.Steps[i].Controls[j].Label = currentControl.Label;
                scenarioPost.Steps[i].Controls[j].AdditionalText = currentControl.AdditionalText;
                scenarioPost.Steps[i].Controls[j].DisplayTypeName = currentControl.DisplayTypeName;
            }
        }

        return scenarioPost;
    },

    NewStepCollection: function () {
        return {
            Steps: [],
            AddStep: function (step) {
                this.Steps[this.Steps.length] = step;
            },
            RemoveStep: function (step) {
                tmpArr = [];
                for (var idx in this.Steps) {
                    if (this.Steps[idx].Order == step.Order) { continue; }
                    tmpArr[tmpArr.length] = this.Steps[idx];
                }
                this.Steps = tmpArr;
            },
            FindStep: function (stepID) {
                for (var idx in this.Steps) {
                    if (this.Steps[idx].StepID == stepID) {
                        return this.Steps[idx];
                    }
                }
            },
            FindStepByOrder: function (stepOrder) {
                for (var idx in this.Steps) {
                    if (this.Steps[idx].Order == stepOrder) {
                        return this.Steps[idx];
                    }
                }
            }
        }
    },

    NewRegControlDisplayCollection: function () {
        return {
            Controls: [],
            AddControl: function (control, updateOrder) {
                var existingControl = this.FindControl(control.RegControlSiteID);
                if (existingControl != null) {
                    this.RemoveControl(control, updateOrder);
                }
                control.Order = this.Controls.length + 1;
                this.Controls[this.Controls.length] = control;
            },
            RemoveControl: function (control, updateOrder) {
                var tmpArr = [];
                for (var idx in this.Controls) {
                    if (this.Controls[idx].RegControlSiteID == control.RegControlSiteID) { continue; }
                    if (updateOrder) {
                        this.Controls[idx].Order = tmpArr.length + 1;
                    }
                    tmpArr[tmpArr.length] = this.Controls[idx];
                }

                this.Controls = tmpArr;
            },
            FindControl: function (regControlSiteID) {
                for (var idx in this.Controls) {
                    if (this.Controls[idx].RegControlSiteID == regControlSiteID) {
                        return this.Controls[idx];
                    }
                }
                return null;
            }
        }
    },

    alertMessage: function (message, status, target) {
        this.regDebug('alertMessage()');

        var target = target || null;
        if ($('#alertMessage').length > 0) {
            $('#alertMessage').remove();
        }

        $('<div id="alertMessage"><p></p></div>').appendTo("body");

        if (status == 2) {
            $('#alertMessage').removeClass('error'); // remove 'error' class on success
            $('.overlay:visible').hide('slide', { direction: 'down' }, 300);
        } else {
            $('#alertMessage').addClass('error');
        }
    
     $('#alertMessage').hide();
     $('#alertMessage p').html(message);

        $('#alertMessage').css({ top: $(window).scrollTop() + 15 })
            .fadeIn(300)
            .delay(3000)
            .fadeOut(300);
    },

    loadScenariosForSite: function (sort, direction) {

        this.regDebug('loadScenarioForSite()');

        var siteid = $("#siteList option:selected").val();
        $('#scenariosList').hide();
        $('#scenarioDetails').hide();

        if (siteid > 0 && !this.regLoadScenariosForSiteInProgress) {

            //lets setup the preview link 

            
            this.setUpPreviewLink(siteid);

            $('#scenarios').show();
            $('#ajaxspin').show();

            $("#scenariosList tbody").empty(); //remove all rows except header

            var url = "/scenarios/" + siteid;
            if (sort != ''){
                url += "/" + sort + "/" + direction;
            }

            //get scenarios for site
            this.regLoadScenariosForSiteInProgress = true;
            $.ajax({
                type: "GET",
                url: url,
                complete: function(result){
                    spark.RegAdminManager.regLoadScenariosForSiteInProgress = false;
                },
                success: function (result) {
                    if (result != null) {
                    
                        $.each(result, function (i, item) {
                        
                            var validItem = true;

                            var showActive = $("#chkShowActive").is(':checked');
                            var showInActive = $("#chkShowInactive").is(':checked');
                            var showFWS = $("#chkShowFWS").is(':checked');
                            var showMOS = $("#chkShowMOS").is(':checked');
                            var showTOS = $("#chkShowTOS").is(':checked');

                            if (!showActive && item.Active) {
                                validItem = false;
                            }
                            else if (!showInActive && !item.Active) {
                                validItem = false;
                            }

                            if (validItem) {
                                var deviceType = "";
                                switch (item.DeviceType) {
                                    case 1:
                                        deviceType = "FWS";
                                        if (!showFWS) {
                                            validItem = false;
                                        }
                                        break;
                                    case 2:
                                        deviceType = "MOS";
                                        if (!showMOS) {
                                            validItem = false;
                                        }
                                        break;
                                    case 3:
                                        deviceType = "TOS";
                                        if (!showTOS) {
                                            validItem = false;
                                        }
                                        break;
                                }

                                if (validItem) {
                                    var createInfoDisplay = "";
                                    if (item.CreateDate != undefined) {
                                        var createdBy = '';
                                        if (item.CreatedBy != undefined && item.CreatedBy != '') {
                                            createdBy = " - " + item.CreatedBy.toLowerCase().replace('sparkstage','').replace('matchnet','').replace('\\', '');
                                        }
                                        createInfoDisplay = spark.RegAdminManager.getDateDisplay(new Date(parseInt(item.CreateDate.substr(6)))) + createdBy;
                                    }

                                    var updateInfoDisplay = "";
                                    if (item.UpdateDate != undefined) {
                                        var updatedBy = '';
                                        if (item.UpdatedBy != undefined && item.UpdatedBy != '') {
                                            updatedBy = " - " + item.UpdatedBy.toLowerCase().replace('sparkstage','').replace('matchnet','').replace('\\', '');
                                        }
                                        updateInfoDisplay = spark.RegAdminManager.getDateDisplay(new Date(parseInt(item.UpdateDate.substr(6)))) + updatedBy;
                                    }

                                    var scenarioItem = {
                                        "id": item.ID,
                                        "name": item.Name,
                                        "nameEscaped": item.Name.replace(/'/g, "\\'"),
                                        "isOnePageReg": item.IsOnePageReg,
                                        "steps": item.Steps.length,
                                        "splashTemplate": item.SplashTemplate.Name,
                                        "registrationTemplate": item.RegistrationTemplate.Name,
                                        "confirmationTemplate": item.ConfirmationTemplate.Name,
                                        "weight": item.Weight,
                                        "deviceType": deviceType,
                                        "deviceTypeID": item.DeviceType,
                                        "active": item.Active ? "Yes" : "No",
                                        "isActive": item.Active,
                                        "created": createInfoDisplay,
                                        "updated": updateInfoDisplay,
                                        "preview": spark.RegAdminManager.previewLinkPrifix + item.Name
                                    };

                                    var html = Mustache.to_html($("#scenarioListItemTemplate").html(), scenarioItem);
                                    $('#scenariosList tbody').append(html);
                                }
                            }
                        });
                    }
                    $('#ajaxspin').hide();
                    $('#scenariosList').show();
                }
            });
        }
    },
    setUpPreviewLink: function (siteid)
    {
        $.ajax({
            type: "GET",
            url: "/previewurl/" + siteid,
            success: function (result) { if (result != null) { spark.RegAdminManager.previewLinkPrifix = result } }
            });
    },
    loadEditActiveConfirmation: function (selectedScenarioID, selectedScenarioName, deviceTypeID, isCurrentlyActive, weight) {
        this.regDebug('loadEditActiveConfirmation()');
        $("#editActiveSelectedScenarioID").val(selectedScenarioID);
        $("#editActiveSelectedIsCurrentlyActive").val(isCurrentlyActive);
        $("#editActiveSelectedDeviceTypeID").val(deviceTypeID);
        $("#editActiveSelectedWeight").val(weight);
        $("#confirmMessage").text(isCurrentlyActive ? "Deactivate " + selectedScenarioName + " scenario?" : "Activate " + selectedScenarioName + " scenario?");
        
        $('#promptEditActive').lightbox_me({
            centered: true
        });
        
        return false;
    },

    clearEditActiveConfirmationValues: function(){
        $("#editActiveSelectedScenarioID").val('');
        $("#editActiveSelectedIsCurrentlyActive").val('');
        $("#editActiveSelectedDeviceTypeID").val('');
        $("#editActiveSelectedWeight").val('');
        $("#editWeightSelectedScenarioID").val('');
        $("#editWeightSelectedIsCurrentlyActive").val('');
        $("#editWeightSelectedDeviceTypeID").val('');

        $('#divEditWeightError').hide();
        $('#divEditActiveError').hide();
    },

    saveEditActive: function (selectedScenarioID, deviceTypeID, isCurrentlyActive, weight) {
        this.regDebug('saveEditActive()');

        var isActivating = (isCurrentlyActive == "true") ? false : true;
        this.regDebug(isActivating ? "Activating scenario " + selectedScenarioID : "Deactivating scenario " + selectedScenarioID);
        //activating: always goes to weight updates
        //deactivating: only goes to weight updates if weight is not 0
        if (isActivating || (!isActivating && parseFloat(weight) > 0)) {

            //load weight update overlay
            this.loadEditWeightsForSite(selectedScenarioID, deviceTypeID, isCurrentlyActive);
            
        }
        else {
            
            //save active status change
            $('#ajaxspin').show();

            var siteid = $("#siteList option:selected").val();
            var obj = { siteID: siteid, deviceTypeID: deviceTypeID, selectedScenarioID: selectedScenarioID, activateSelectedScenario: isActivating, scenarioWeights: {} };
            obj.scenarioWeights[selectedScenarioID] = 0;

            $.ajax({
                type: "POST",
                async: false,
                url: "/saveactiveweights",
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                data: JSON.stringify(obj),
                complete: function (result) {
                    spark.CreateScenarioManager.generalAjaxInProgress = false;
                },
                success: function (result) {
                    if (result != null && result.Status == 2) {
                        $('#closePromptEditActive').lightbox_me();
                        $('#closePromptEditActive').trigger('close');

                        spark.RegAdminManager.clearEditActiveConfirmationValues();

                        spark.RegAdminManager.loadScenariosForSite(spark.RegAdminManager.currentSort, spark.RegAdminManager.currentSortDirection);
                    } else {
                        spark.RegAdminManager.showCreateError('divEditActiveError', result.StatusMessage);
                    }
                }
            });

            $('#ajaxspin').hide();
        }
    },

    loadEditWeightsForSite: function (selectedScenarioID, deviceTypeID, isCurrentlyActive) {

        this.regDebug('loadEditWeightsForSite()');

        $('#promptEditWeights').lightbox_me({
            centered: true,
            vCentered: true
        });

        var siteid = $("#siteList option:selected").val();

        if (siteid > 0 && !this.regLoadScenariosForSiteInProgress) {

            $('#ajaxspin').show();

            $("#editWeightSelectedScenarioID").val(selectedScenarioID);
            $("#editWeightSelectedIsCurrentlyActive").val(isCurrentlyActive);
            $("#editWeightSelectedDeviceTypeID").val(deviceTypeID);

            $("#scenariosWeightList tbody").empty(); //remove all rows except header

            var url = "/scenarios/" + siteid;

            //get scenarios for site
            this.regLoadScenariosForSiteInProgress = true;
            $.ajax({
                type: "GET",
                url: url,
                async: false,
                complete: function (result) {
                    spark.RegAdminManager.regLoadScenariosForSiteInProgress = false;
                },
                success: function (result) {
                    if (result != null) {
                        var totalWeight = 0;

                        var editSelectedScenarioID = $("#editActiveSelectedScenarioID").val();
                        var isEditingActiveStatus = false;
                        if (editSelectedScenarioID == selectedScenarioID) {
                            isEditingActiveStatus = true;
                        }

                        $.each(result, function (i, item) {

                            var validItem = true;

                            var showActive = $("#chkShowActive").is(':checked');
                            var showInActive = $("#chkShowInactive").is(':checked');
                            var showFWS = $("#chkShowFWS").is(':checked');
                            var showMOS = $("#chkShowMOS").is(':checked');
                            var showTOS = $("#chkShowTOS").is(':checked');

                            if (isEditingActiveStatus && (item.ID == selectedScenarioID)) {
                                validItem = !item.Active;
                                item.Active = true;
                            }
                            else {
                                validItem = item.Active;
                            }

                            if (validItem) {
                                var deviceType = "";
                                switch (item.DeviceType) {
                                    case 1:
                                        deviceType = "FWS";
                                        if (deviceTypeID != 1) {
                                            validItem = false;
                                        }
                                        break;
                                    case 2:
                                        deviceType = "MOS";
                                        if (deviceTypeID != 2) {
                                            validItem = false;
                                        }
                                        break;
                                    case 3:
                                        deviceType = "TOS";
                                        if (deviceTypeID != 3) {
                                            validItem = false;
                                        }
                                        break;
                                }

                                if (validItem) {

                                    var scenarioItem = {
                                        "id": item.ID,
                                        "name": item.Name,
                                        "weight": item.Weight,
                                        "deviceType": deviceType,
                                        "active": item.Active ? "Yes" : "No",
                                    };

                                    totalWeight += item.Weight;

                                    var html = Mustache.to_html($("#scenarioWeightListItemTemplate").html(), scenarioItem);
                                    $('#scenariosWeightList tbody').append(html);
                                }
                            }
                        });

                        var totalObj = {
                            "total": totalWeight
                        }
                        var htmlTotal = Mustache.to_html($("#scenarioWeightTotalTemplate").html(), totalObj);
                        $('#scenariosWeightList tbody').append(htmlTotal);
                    }
                    $('#ajaxspin').hide();
                }
            });

            return false;
        }
    },

    saveWeights: function(){
        this.regDebug('saveWeights()');

        $('#ajaxspin').show();

        //get selected scenario, if any
        var editSelectedScenarioID = $("#editActiveSelectedScenarioID").val() != '' ? $("#editActiveSelectedScenarioID").val() : 0;
        var editSelectedDeviceTypeID = $("#editWeightSelectedDeviceTypeID").val();
        var isActivating = $("#editWeightSelectedIsCurrentlyActive").val() == "true" ? false : true;

        //get weights
        var activeWeightsList = [];
        var total = 0;
        var hasNegativeWeight = false;
        $('.weight-scenario-ids').each(function () {
            var scenarioId = $(this).val();
            var currentWeight = $('#hidWeight' + scenarioId).val();
            var newWeight = $('#txtWeight' + scenarioId).val();
            activeWeightsList[activeWeightsList.length] = { "ScenarioID": scenarioId, "Active": true, "Weight": newWeight };
            var newWeightFloat = parseFloat(newWeight);
            total += newWeightFloat;
            if (newWeightFloat < 0) {
                hasNegativeWeight = true;
            }
        });

        //total validation
        if (total != 1) {
            spark.RegAdminManager.showCreateError('divEditWeightError', 'Total weight is not equal to 1');
            $('#ajaxspin').hide();
            return;
        }
        else if (hasNegativeWeight) {
            spark.RegAdminManager.showCreateError('divEditWeightError', 'Weight may not be a negative number for any scenarios');
            $('#ajaxspin').hide();
            return;
        }

        //save weights
        var siteid = $("#siteList option:selected").val();
        var obj = { siteID: siteid, deviceTypeID: editSelectedDeviceTypeID, selectedScenarioID: editSelectedScenarioID, activateSelectedScenario: isActivating, activeWeights: activeWeightsList };

        $.ajax({
            type: "POST",
            async: false,
            url: "/saveactiveweights",
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            data: JSON.stringify(obj),
            complete: function (result) {
                spark.CreateScenarioManager.generalAjaxInProgress = false;
            },
            success: function (result) {
                if (result != null && result.Status == 2) {
                    $('#closePromptEditWeights').lightbox_me();
                    $('#closePromptEditWeights').trigger('close');

                    spark.RegAdminManager.clearEditActiveConfirmationValues();

                    spark.RegAdminManager.loadScenariosForSite(spark.RegAdminManager.currentSort, spark.RegAdminManager.currentSortDirection);
                } else {
                    spark.RegAdminManager.showCreateError('divEditWeightError', result.StatusMessage);
                }
            }
        });

        $('#ajaxspin').hide();

    },

    previewScenario: function (event) {
        this.regDebug('previewScenario()');

        window.open(event.data.url);
    },

    loadSelectedScenarioDetails: function (scenarioid) {
        this.regDebug('loadSelectedScenarioDetails()');

        $('#scenarioDetails').hide();
        $('#ajaxspin').show();

        if (arguments.length == 0) {
            scenarioid = $("#scenariosList option:selected").val();
        }

        //get scenario details
        $.ajax({
            type: "GET",
            url: "/scenario/" + scenarioid,
            success: function (result) {
                if (result != null) {
                    //$('#scenariosList') remove all options

                    var autoAdvance = "No";
                    if (result.IsAutoAdvance) {
                        autoAdvance = "Yes";
                    }
                    $('#desc').text(result.Description);
                    $('#autoadvance').text(autoAdvance);
                    $('#deviceType').text(result.DeviceType);
                    $('#createdBy').text(result.CreatedBy);
                    if (result.LastEditedDate != null) {
                        var sec = result.LastEditedDate.replace(/\/Date\((-?\d+)\)\//, '$1');
                        var d = new Date(parseInt(sec));
                        //var newDate = dateFormat(d, "mm/dd/yyyy");
                        $('#editDate').text((d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear());
                    }
                    $('#editedBy').text(result.LastEditedBy);
                    $('#split').text(result.SplitPercentage);

                    $('#pageCount').text(result.StepCount);
                    $('#fieldCount').text(result.ControlCount);
                    $('#scenarioName').text(result.Name);
                    $('#url').data("URL", result.Url);

                    var isActive = "No";
                    $('#btnDeactivateScenario').removeClass('disable');
                    $('#btnActivateScenario').removeClass('disable');
                    $('#btnActivateScenario').removeAttr('disabled');
                    $('#btnDeactivateScenario').removeAttr('disabled');
                    if (result.IsActive) {
                        isActive = "Yes";
                        $('#url').html('<a href=' + result.Url + ' target=blank>' + result.Url + '</a>');
                        $('#btnActivateScenario').attr('disabled', 'disabled');
                        $('#btnActivateScenario').addClass('disable');
                        $('#btnEditScenario').attr('disabled', 'disabled');
                        $('#btnEditScenario').addClass('disable');
                        $('#btnPreviewScenario').removeAttr('disabled');
                        $('#btnPreviewScenario').removeClass('disable');
                        $("#btnPreviewScenario").unbind();
                        $("#btnPreviewScenario").bind("click", { url: result.Url }, previewScenario);

                    }
                    else {
                        $('#url').html(result.Url);
                        $('#btnDeactivateScenario').attr('disabled', 'disabled');
                        $('#btnDeactivateScenario').addClass('disable');
                        $('#btnEditScenario').removeAttr("disabled");
                        $('#btnEditScenario').removeClass('disable');
                        $('#btnPreviewScenario').attr('disabled', 'disabled');
                        $('#btnPreviewScenario').addClass('disable');

                    }
                    $('#status').text(isActive);
                    $('#regScenarioId').val(result.RegScenarioId);
                }

                $('#ajaxspin').hide();
                $('#scenarioDetails').show();
            }
        });
    },

    ToggleScenario: function (scenarioId) {
        this.regDebug('ToggleScenario()');

        //toggle scenario
        $.ajax({
            type: "GET",
            url: "/scenario/toggle/" + scenarioId,
            success: function (result) {
                //refresh list box and details section
                $('#siteList').trigger('change');
                $('#scenariosList').trigger('change');
            }
        });
    },

    getDateDisplay: function (date) {

        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;

    },

    showCreateError: function (errorContainerID, message) {
        spark.RegAdminManager.regDebug('showCreateError()');
        $('#' + errorContainerID).html(message).show();
    },

    initialize: function () {


        //set up the preview pri



        this.regDebug('initialize()');
            
    },
};

$(document).ready(function () {
    if (!spark.RegAdminManager.initialized) {
        spark.RegAdminManager.initialize();
    }
});