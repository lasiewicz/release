﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Registration;
using RegistrationAdminTool.Helpers;
using RegistrationAdminTool.Models;
using RegistrationAdminTool.Models.JSONResult;
using Spark.CommonLibrary.Logging;
using Cache = Matchnet.Caching.Cache;
using log4net;
using System.Threading;
using RegistrationAdminTool.Models.Entities;

namespace RegistrationAdminTool.Managers
{
    public class ScenarioManager : BaseManager
    {
        public ILog _logger;
        private int _saveScenarioSleepTime = 3500;
        private int _ensureScenarioSavedsleepTime = 500;
        
        public ScenarioManager()
        {
            _logger = LogManager.GetLogger(GetType());
        }


        private const string ScenariosCacheKey = "Secnarios";
        private List<Scenario> scenarios = null;
        public List<Scenario> Scenarios 
        { 
            get { return scenarios ?? (scenarios = GetScenarios()); }
        }
        public ScenariosViewModel GetScenariosModel(int? siteId, int? scenarioId)
        {
            var selectedSiteId = siteId.HasValue ? siteId.Value : 0;
            
            var model = new ScenariosViewModel
                            {
                                Scenarios = this.Scenarios,
                                Sites = SiteHelper.GetSitesAsSelectList(selectedSiteId),
                                DeviceTypes = DeviceTypeHelper.GetDeviceTypesAsSelectList(1)
                            };

            if (siteId.HasValue && scenarioId.HasValue)
            {
                model.SiteId = siteId.Value;
                model.ScenarioId = scenarioId.Value;
            }

            return model;
        }

        public IEnumerable<Scenario> GetScenariosBySite(int siteId, string sort, string direction)
        {
            if (sort != null)
                sort = sort.ToLower();

            if (direction == "desc")
            {
                switch (sort)
                {
                    case "active":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.Active);
                    case "steps":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.Steps.Count);
                    case "splash":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.SplashTemplate.ID);
                    case "registration":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.RegistrationTemplate.ID);
                    case "confirm":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.ConfirmationTemplate.ID);
                    case "device":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.DeviceType);
                    case "weight":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.Weight);
                    case "created":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.CreateDate);
                    case "updated":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.UpdateDate);
                    case "name":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderByDescending(s => s.Name, StringComparer.OrdinalIgnoreCase);
                    default:
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList();
                }
            }
            else
            {
                switch (sort)
                {
                    case "active":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.Active);
                    case "steps":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.Steps.Count);
                    case "splash":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.SplashTemplate.ID);
                    case "registration":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.RegistrationTemplate.ID);
                    case "confirm":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.ConfirmationTemplate.ID);
                    case "device":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.DeviceType);
                    case "weight":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.Weight);
                    case "created":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.CreateDate);
                    case "updated":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.UpdateDate);
                    case "name":
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList().OrderBy(s => s.Name, StringComparer.OrdinalIgnoreCase);
                    default:
                        return (from s in this.Scenarios where s.SiteID == siteId select s).ToList();
                }
            }
        }

        public CreateScenarioViewModel GetNewScenarioModel(int siteId, int deviceTypeId)
        {
            var model = new CreateScenarioViewModel
            {
                SiteId = siteId,
                SiteName = SiteHelper.GetSiteName(siteId),
                DeviceTypeId = deviceTypeId,
                DeviceTypeName = DeviceTypeHelper.GetDeviceName(deviceTypeId),
                //SiteControls = RegistrationMetadataSA.Instance.GetAllRegControls(deviceTypeId)
                
            };
            return model;
        }

        public SaveScenarioResult SaveScenario(Scenario scenario, int splashTemplateId, int regTemplateId, int confirmationTemplateId)
        {
            var result = new SaveScenarioResult();

            int scenarioId =0;
            List<RegControlDisplay> missingRegControls=null;
            
            scenario.SplashTemplate = new Template(scenario.SiteID, TemplateType.Splash, string.Empty, splashTemplateId);
            scenario.RegistrationTemplate = new Template(scenario.SiteID, TemplateType.Registration, string.Empty, regTemplateId);
            scenario.ConfirmationTemplate = new Template(scenario.SiteID, TemplateType.Confirmation, string.Empty, confirmationTemplateId);
            
            try
            {
                missingRegControls = ValidateScenario(scenario);
                if (null == missingRegControls)
                {
                    scenario.IsOnePageReg = (scenario.Steps.Count == 1);

                    if (scenario.ID > 0)
                    {
                        scenarioId = scenario.ID;
                        scenario.UpdatedBy = HttpContext.Current.User.Identity.Name;

                        RegistrationMetadataSA.Instance.SaveScenario(scenario);
                        ActivityLoggingManager.Instance.Log(RegAdminActionType.EditScenario, scenarioId);
                    }
                    else
                    {
                        scenario.CreatedBy = HttpContext.Current.User.Identity.Name;

                        scenarioId = RegistrationMetadataSA.Instance.CreateRegScenario(scenario);
                        ActivityLoggingManager.Instance.Log(RegAdminActionType.CreateScenario, scenarioId);
                    }

                    //since we do not yet utilize caching, we will attempt to wait a bit to ensure the scenario has been saved to the database
                    if (scenarioId > 0)
                    {
                        Thread.Sleep(_saveScenarioSleepTime);

                        bool verifiedDB = false;
                        int verifyAttempts = 0;

                        while (verifyAttempts <= 3 && !verifiedDB)
                        {
                            verifyAttempts++;
                            Scenario savedScenario = GetScenario(scenarioId);
                            if (savedScenario != null && scenario.Steps.Count == savedScenario.Steps.Count)
                            {
                                //we'll assume steps have been saved and db is ready
                                verifiedDB = true;
                            }
                            else
                            {
                                
                                _logger.InfoFormat("Pausing to wait for scenario to be available in database, scenarioID: {0}, attempt: {1}, sleep milliseconds: {2}", scenarioId, verifyAttempts, _ensureScenarioSavedsleepTime);
                                Thread.Sleep(_ensureScenarioSavedsleepTime);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating scenario." , ex);
            }
            result.MissingRegControls = missingRegControls;
            result.ScenarioId = scenarioId;
            return result;
        }

        private List<RegControlDisplay> ValidateScenario(Scenario scenario)
        {
            List<RegControl> regControlsRequiredForRegScenarioSite = RegistrationMetadataSA.Instance.GetRegControlsRequiredForRegScenarioSite(scenario.SiteID);
            List<RegControl> regControlsForScenario = new List<RegControl>();
            List<RegControlDisplay> missingRegControlsForScenario = null;
            foreach (Step s in scenario.Steps)
            {
                foreach (RegControl rc in s.Controls)
                {
                    regControlsForScenario.Add(rc);
                }
            }

            foreach (RegControl reqRegControl in regControlsRequiredForRegScenarioSite)
            {
                bool hasReqControl = false;
                foreach (RegControl regControl in regControlsForScenario)
                {
                    if (regControl.RegControlID == reqRegControl.RegControlID)
                    {
                        hasReqControl = true;
                        break;
                    }
                }
                if (!hasReqControl)
                {
                    if(null == missingRegControlsForScenario) missingRegControlsForScenario = new List<RegControlDisplay>();
                    missingRegControlsForScenario.Add(new RegControlDisplay(reqRegControl));
                }
            }
            return missingRegControlsForScenario;
        }


        public int CreateScenario(Scenario scenario, 
            int splashTemplateId, int regTemplateId, int confirmationTemplateId)
        {
            int newScenarioId = 0;
            
            scenario.SplashTemplate = new Template(TemplateType.Splash, string.Empty, splashTemplateId);
            scenario.RegistrationTemplate = new Template(TemplateType.Registration, string.Empty, regTemplateId);
            scenario.ConfirmationTemplate = new Template(TemplateType.Confirmation, string.Empty, confirmationTemplateId);
            scenario.CreatedBy = HttpContext.Current.User.Identity.Name;

            try
            {
                newScenarioId = RegistrationMetadataSA.Instance.CreateRegScenario(scenario);
                ActivityLoggingManager.Instance.Log(RegAdminActionType.CreateScenario, newScenarioId);
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating scenario." , ex);
            }

            return newScenarioId;
        }

        public List<RegControlDisplay> GetRegControlsBySiteAndDevice(int siteId, int deviceTypeId)
        {
            var controls = RegistrationMetadataSA.Instance.GetAllRegControls(deviceTypeId).Where(c => c.SiteID == siteId).OrderBy(c => c.Name);
            var regControlDisplays = controls.Select(control => new RegControlDisplay(control)).ToList();

            List<RegControl> regControlsRequiredForRegScenarioSite = RegistrationMetadataSA.Instance.GetRegControlsRequiredForRegScenarioSite(siteId);
            if (regControlsRequiredForRegScenarioSite != null && regControlsRequiredForRegScenarioSite.Count > 0)
            {
                foreach (RegControl rc in regControlsRequiredForRegScenarioSite)
                {
                    foreach (RegControlDisplay rcd in regControlDisplays)
                    {
                        if (rcd.RegControlID == rc.RegControlID)
                        {
                            rcd.RegControlScenarioRequired = true;
                            break;
                        }
                    }
                }
            }

            return regControlDisplays;
        }

        public List<RegControlDisplay> GetRegControlsBySiteAndScenario(int siteId,  int deviceTypeId, int scenarioId)
        {
            var controls = GetRegControlsBySiteAndDevice(siteId, deviceTypeId);
            var scenario = GetScenario(scenarioId);

            if (scenario != null && scenario.Steps != null)
            {
                foreach (var step in scenario.Steps)
                {
                    if (step.Controls != null)
                    {
                        foreach (var control in step.Controls)
                        {
                            if (control.ScenarioOverrides != null)
                            {
                                var scenarioOverride =(from so in control.ScenarioOverrides where so.RegScenarioID == scenarioId select so).FirstOrDefault();
                                var defaultControl = (from c in controls where c.RegControlSiteID == control.RegControlSiteID select c).FirstOrDefault();

                                if (scenarioOverride != null && defaultControl != null)
                                {
                                    defaultControl.AdditionalText = scenarioOverride.AdditionalText;
                                    defaultControl.Label = scenarioOverride.Label;
                                    defaultControl.ErrorMessage = scenarioOverride.RequiredErrorMessage;
                                    defaultControl.Required = scenarioOverride.Required;
                                    defaultControl.AutoAdvance = scenarioOverride.EnableAutoAdvance;
                                    defaultControl.DisplayType = (int)scenarioOverride.DisplayType;
                                }
                            }
                        }
                    }
                }
            }

            return controls;
        }

        public bool IsScenarioNameTaken(int siteId, string name)
        {
            var scenario = (from s in GetScenarios() where s.Name.ToLower().Trim() == name.ToLower().Trim() && s.SiteID == siteId select s).FirstOrDefault();
            return scenario != null;
        }

        public bool IsScenarioNameTaken(int siteId, string name, int existingScenarioId)
        {
            var scenario = (from s in GetScenarios() where s.Name.ToLower().Trim() == name.ToLower().Trim() && s.SiteID == siteId && s.ID != existingScenarioId select s).FirstOrDefault();
            return scenario != null;
        }

        public List<Scenario> GetScenarios()
        {
            var allscenarios = RegistrationMetadataSA.Instance.GetAllRegScenarios();
            allscenarios.Sort((x, y) => x.Name.CompareTo(y.Name));

            /*var allscenarios = Cache.Instance.Get(ScenariosCacheKey) as List<Scenario>;
            if (allscenarios == null)
            {
                allscenarios = RegistrationMetadataSA.Instance.GetAllRegScenarios();
                allscenarios.Sort((x, y) => x.Name.CompareTo(y.Name));
                Cache.Instance.Add(ScenariosCacheKey, allscenarios, null, DateTime.Now.AddHours(1),
                                   System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
            }*/
            return allscenarios;
        }

        private Scenario GetScenario(int scenarioId)
        {
            return (from s in GetScenarios() where s.ID == scenarioId select s).FirstOrDefault();
        }

        public EditScenarioViewModel GetEditScenarioViewModel(int scenarioId, bool forCopy)
        {
            var scenario = (from s in GetScenarios() where s.ID == scenarioId select s).FirstOrDefault();
            if(scenario != null)
            {
                var viewModel = new EditScenarioViewModel(scenario);

                if (forCopy) //means we are copying this scenario in order to create a new one. Lets update the senario ID to 0 and all its step IDs as well. 
                {
                    viewModel.RegScenarioId = 0;
                    
                    foreach(Step s in scenario.Steps)
                    {
                        s.StepId = 0;
                    }

                }
                    

                viewModel.SplashTemplates = GetTemplatesAsSelectList(scenario.SiteID, TemplateType.Splash, scenario.SplashTemplate.ID);
                viewModel.RegistrationTemplates = GetTemplatesAsSelectList(scenario.SiteID, TemplateType.Registration, scenario.RegistrationTemplate.ID);
                viewModel.ConfirmationTemplates = GetTemplatesAsSelectList(scenario.SiteID, TemplateType.Confirmation, scenario.ConfirmationTemplate.ID);
                return viewModel;
            }
            
            return null;
            
        }


        public bool SaveRegControlScenarioOverride(RegControlDisplay regControlDisplay, int scenarioID)
        {
            bool success = true;

            try
            {
                var regControlScenarioOverride = new RegControlScenarioOverride();
                regControlScenarioOverride.RegScenarioID = scenarioID;
                regControlScenarioOverride.AdditionalText = regControlDisplay.AdditionalText;
                regControlScenarioOverride.EnableAutoAdvance = regControlDisplay.AutoAdvance;
                regControlScenarioOverride.Label = regControlDisplay.Label;
                regControlScenarioOverride.Required = regControlDisplay.Required;
                regControlScenarioOverride.RequiredErrorMessage = regControlDisplay.ErrorMessage;

                if(regControlDisplay.AutoAdvance && (ControlDisplayType) regControlDisplay.DisplayType == ControlDisplayType.DropdownList)
                {
                    regControlScenarioOverride.DisplayType = (ControlDisplayType) ControlDisplayType.ListBox;
                }
                else
                {
                    regControlScenarioOverride.DisplayType = (ControlDisplayType) regControlDisplay.DisplayType;
                }

                RegistrationMetadataSA.Instance.SaveRegControlScenarioOverride(regControlScenarioOverride, regControlDisplay.RegControlSiteID);
            }
            catch (Exception ex)
            {
                success = false;

            }

            return success;
        }

        public List<SelectListItem> GetTemplatesAsSelectList(int siteId, TemplateType type, int? selectedTemplateId = null)
        {
            var listItems = new List<SelectListItem>();
            var listTemplates = new TemplateManager().GetTemplates(siteId, type);

            foreach(var template in listTemplates)
            {
                listItems.Add(new SelectListItem { Text = template.Name, Value = template.ID.ToString(), Selected = (selectedTemplateId.HasValue && template.ID == selectedTemplateId.Value) });
            }

            return listItems;
        }

        public bool SaveStep(Step step, int scenarioId)
        {
            bool success = true;

            try
            {
                RegistrationMetadataSA.Instance.SaveRegStep(step, scenarioId);
            }
            catch (Exception ex)
            {
                success = false;

            }

            return success;
        }

        public APIResult SaveActiveAndWeights(int siteID, int deviceTypeID, int selectedScenarioID, bool activateSelectedScenario, List<ActiveWeight> activeWeights)
        {
            APIResult result = new APIResult();
            try
            {
                Dictionary<int, ActiveWeight> activeWeightsDictionary = new Dictionary<int, ActiveWeight>();
                if (activeWeights != null)
                {
                    foreach (ActiveWeight aw in activeWeights)
                    {
                        activeWeightsDictionary[aw.ScenarioID] = aw;
                    }
                }

                List<Scenario> existingScenariosList = (from s in this.Scenarios where s.SiteID == siteID && ((int)s.DeviceType) == deviceTypeID select s).ToList();
                
                //validate total weight is 1 for active scenarios
                double total = 0;
                foreach (Scenario s in existingScenariosList)
                {
                    if (s.ID == selectedScenarioID)
                    {
                        if (activateSelectedScenario)
                        {
                            total += activeWeightsDictionary.ContainsKey(s.ID) ? activeWeightsDictionary[s.ID].Weight : 0;
                        }
                    }
                    else if (activeWeightsDictionary.ContainsKey(s.ID))
                    {
                        total += activeWeightsDictionary[s.ID].Weight;
                    }
                    else if (s.Active)
                    {
                        total += s.Weight;
                    }
                }

                if (total != 1)
                {
                    result.StatusMessage = "Total weight is not equal to 1";
                }
                else
                {
                    foreach (Scenario s in existingScenariosList)
                    {
                        if ((activeWeightsDictionary.ContainsKey(s.ID) || s.ID == selectedScenarioID))
                        {
                            if (s.ID == selectedScenarioID)
                            {
                                if (activateSelectedScenario)
                                {
                                    double newActivateWeight = activeWeightsDictionary.ContainsKey(s.ID) ? activeWeightsDictionary[s.ID].Weight : 0;
                                    //activate scenario
                                    RegistrationMetadataSA.Instance.SaveScenarioStatusWeight(s.ID, true, newActivateWeight, HttpContext.Current.User.Identity.Name, siteID, deviceTypeID);
                                    ActivityLoggingManager.Instance.Log(RegAdminActionType.ToggleScenarioActive, s.ID);
                                    AdminSA.Instance.RegistrationAdminScenarioHistorySave(s.ID, deviceTypeID, siteID, newActivateWeight, HttpContext.Current.User.Identity.Name);
                                }
                                else
                                {
                                    //deactivate scenario
                                    RegistrationMetadataSA.Instance.SaveScenarioStatusWeight(s.ID, false, 0, HttpContext.Current.User.Identity.Name, siteID, deviceTypeID);
                                    ActivityLoggingManager.Instance.Log(RegAdminActionType.ToggleScenarioInactive, s.ID);
                                    AdminSA.Instance.RegistrationAdminScenarioHistorySave(s.ID, deviceTypeID, siteID, 0, HttpContext.Current.User.Identity.Name);
                                }
                            }
                            else
                            {
                                //just updating weight
                                if (s.Weight != activeWeightsDictionary[s.ID].Weight)
                                {
                                    RegistrationMetadataSA.Instance.SaveScenarioStatusWeight(s.ID, true, activeWeightsDictionary[s.ID].Weight, HttpContext.Current.User.Identity.Name, siteID, deviceTypeID);
                                    ActivityLoggingManager.Instance.Log(RegAdminActionType.EditWeight, s.ID);
                                    AdminSA.Instance.RegistrationAdminScenarioHistorySave(s.ID, deviceTypeID, siteID, activeWeightsDictionary[s.ID].Weight, HttpContext.Current.User.Identity.Name);
                                }
                            }

                        }
                    }

                    //since we do not yet utilize caching, we will attempt to wait a bit to ensure the updates has been saved to the database, otherwise users might see incorrect data
                    Thread.Sleep(_saveScenarioSleepTime);

                    result.Status = (int)ResultStatus.Success;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error saving active status and weights.", ex);
                result.StatusMessage = "Unexpected error occurred while saving scenario active status and weights.";
            }

            return result;
        }

    } 
}
