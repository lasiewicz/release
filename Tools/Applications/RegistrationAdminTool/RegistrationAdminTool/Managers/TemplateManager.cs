﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Registration;
using RegistrationAdminTool.Models;
using Spark.CommonLibrary.Logging;
using log4net;
using RegistrationAdminTool.Helpers;

namespace RegistrationAdminTool.Managers
{
    public class TemplateManager : BaseManager
    {
        private ILog _log;
 
        public TemplateManager()
        {
            _log =LogManager.GetLogger(this.GetType());
        }

        public List<Template> GetTemplates(int siteId, TemplateType type)
        {
            List<Template> templates = null;

            Dictionary<int, List<Template>> allTemplates = RegistrationMetadataSA.Instance.GetTemplatesBySite();

            if (allTemplates[siteId] != null)
            {
                templates = (from Template t in allTemplates[siteId] where t.Type == type select t).ToList();
            }

            return templates;
        }

        private bool DoesTemplateExist(SaveTemplateModel templateModel)
        {
            var template = (from t in GetTemplates(templateModel.SiteId, templateModel.TemplateType) where t.Name.ToLower().Trim() == templateModel.Name.ToLower().Trim() && t.SiteID == templateModel.SiteId select t).FirstOrDefault();
            return template != null;
        }

        public bool IsTemplateInUse(int templateId, int siteId)
        {
            List<Scenario> scenarios = new ScenarioManager().GetScenarios();    
            var scenario = (from s in scenarios where (s.RegistrationTemplate.ID == templateId || s.SplashTemplate.ID==templateId || s.ConfirmationTemplate.ID==templateId) && s.SiteID == siteId select s).FirstOrDefault();
            return scenario != null;
        }

        public TemplateViewModel GetTemplateViewModel()
        {
            Dictionary<int, List<Template>> allTemplates = RegistrationMetadataSA.Instance.GetTemplatesBySite();
            TemplateViewModel templateViewModel = TemplateViewModel.Parse(allTemplates);
            templateViewModel.Sites = SiteHelper.GetSitesAsSelectList(templateViewModel.SiteId);
            templateViewModel.TemplateTypes = TemplateHelper.GetTemplateTypesAsSelectList((int)templateViewModel.TemplateType);
            return templateViewModel;
        }

        public CreateTemplateViewModel GetNewTemplateViewModel()
        {
            var model = new CreateTemplateViewModel
            {
                Sites = SiteHelper.GetSitesAsSelectList(0),
                TemplateTypes = TemplateHelper.GetTemplateTypesAsSelectList(0)
            };
            return model;
        }

        public TemplateModel SaveTemplateModel(SaveTemplateModel templateModel)
        {
            Template regTemplate = new Template();
            regTemplate.Name = templateModel.Name;
            regTemplate.SiteID = templateModel.SiteId;
            regTemplate.Type = templateModel.TemplateType;
            TemplateModel model = TemplateModel.Parse(regTemplate);
            TemplateSaveResult templateSaveResult=null;
            try
            {
                if (!DoesTemplateExist(templateModel))
                {
                    templateSaveResult = RegistrationMetadataSA.Instance.CreateTemplate(regTemplate);
                }
                else
                {
                    templateSaveResult = new TemplateSaveResult();
                    templateSaveResult.ResultType = TemplateResultType.TemplateAlreadyExistsForTypeAndSite;
                    templateSaveResult.RegTemplate = regTemplate;
                }
            }
            catch (Exception exception)
            {
                _log.Error("Could not save template",exception);
            }

            if(null == templateSaveResult)
            {
                model.ErrorMessage = string.Format("Error saving template:{0}, type:{1}, and site:{2} ", model.Name, model.Type, model.SiteID);
            } 
            else if (templateSaveResult.ResultType == TemplateResultType.Success)
            {
                model.ID = templateSaveResult.RegTemplate.ID;
                model.SuccessMessage = string.Format("Saved template:{0} for type:{1} and site:{2} ", model.Name, model.Type, model.SiteID);
            }
            else if(templateSaveResult.ResultType==TemplateResultType.TemplateAlreadyExistsForTypeAndSite)
            {
                model.ErrorMessage = string.Format("Error during save. Template already exists for name:{0}, type:{1}, and site:{2}.", model.Name, model.Type, model.SiteID);
            }
            return model;
        }

        public TemplateViewModel RemoveTemplateModel(int templateId, int siteId)
        {
            TemplateSaveResult templateSaveResult = null;
            try
            {
                if (!IsTemplateInUse(templateId, siteId))
                {
                    templateSaveResult = RegistrationMetadataSA.Instance.RemoveTemplate(templateId, siteId);
                }
                else
                {
                    templateSaveResult = new TemplateSaveResult();
                    templateSaveResult.ResultType=TemplateResultType.TemplateInUseByRegScenario;
                }
            }
            catch (Exception exception)
            {
                _log.Error("Could not remove template", exception);
            }
            TemplateViewModel templateViewModel = GetTemplateViewModel();
            if (null == templateSaveResult)
            {
                templateViewModel.ErrorMessage = string.Format("Error removing template:{0} for site:{1} ", templateId, siteId);
            }
            else if (templateSaveResult.ResultType == TemplateResultType.Success)
            {
                templateViewModel.SuccessMessage = string.Format("Removed template:{0} for site:{1} ", templateId, siteId);
            }
            else if (templateSaveResult.ResultType == TemplateResultType.TemplateInUseByRegScenario)
            {
                templateViewModel.ErrorMessage = string.Format("Error during remove. Template:{0} for site:{1} is in use by a reg scenario.", templateId, siteId);
            }
            return templateViewModel;
        }
    }
}