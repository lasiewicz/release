﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Registration;

namespace RegistrationAdminTool.Managers
{
    public class ActivityLoggingManager
    {
        public static ActivityLoggingManager Instance = new ActivityLoggingManager();

        private ActivityLoggingManager() { }

        public void Log(RegAdminActionType actionType, int scenarioId)
        {
            if(HttpContext.Current.User != null)
            {
                AdminSA.Instance.RegistrationAdminActionLogInsert(HttpContext.Current.User.Identity.Name, actionType, scenarioId);
            }
        }
    }
}