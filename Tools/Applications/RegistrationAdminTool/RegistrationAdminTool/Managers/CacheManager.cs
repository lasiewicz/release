﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Spark.CacheBusterClient;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using log4net;

namespace RegistrationAdminTool.Managers
{
    public class CacheManager
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(CacheManager));
        
        public bool ClearRegistrationMetadataCache(CacheType cacheType, string configFilePath)
        {
            var cleared = true;

            try
            {
                var environmentTypeSettingValue = RuntimeSettings.GetSetting(SettingConstants.CACHEBUSTER_ENVIRONMENT);
                var environmentType = ((EnivronmentType)Enum.Parse(typeof(EnivronmentType), environmentTypeSettingValue));

                var client = new Client(configFilePath);
                var apiResults = client.ClearWebBasedCache(CacheType.APIScenarioMetadata, environmentType);
                
                if (apiResults.Any(result => !result.Success))
                    throw new Exception("API cache not busted. url: " + apiResults[0].ErrorMessage + " the config file path was: " + configFilePath + "environmentType " + environmentType.ToString());
                
                var regSiteResults = client.ClearWebBasedCache(cacheType, environmentType);
                
                if (regSiteResults.Any(result => !result.Success))
                    throw new Exception("Reg site cache not busted.: " + regSiteResults[0].ErrorMessage + " the config file path was: " + configFilePath + "environmentType " + environmentType.ToString());
                
            }
            catch(Exception ex)
            {
                Log.Error("Error clearing cache.", ex);
                cleared = false;
            }

            return cleared;
        }
    }
}