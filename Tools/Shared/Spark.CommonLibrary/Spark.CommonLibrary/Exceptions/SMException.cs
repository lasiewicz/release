﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.CommonLibrary.Exceptions
{
    [Serializable]
    public class SMException : ExceptionBase
    {
        #region Constructors

        public SMException(string message)
            : base(message)
        {

        }

        public SMException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public SMException(string message, Exception innerException, ExceptionLevel exceptionLevelOverride)
            : base(message, innerException, exceptionLevelOverride)
        {

        }


        #endregion
    }
}
