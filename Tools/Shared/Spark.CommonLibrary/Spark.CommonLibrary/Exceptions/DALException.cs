﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.CommonLibrary.Exceptions
{
    [Serializable]
    public class DALException : ExceptionBase
    {
        #region Constructors

        public DALException(string message)
            : base(message)
        {

        }

        public DALException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public DALException(string message, Exception innerException, string detailedDatabaseException)
            : base(message, innerException, detailedDatabaseException)
        {

        }

        public DALException(string message, Exception innerException, string detailedDatabaseException, ExceptionLevel exceptionLevel)
            : base(message, innerException, detailedDatabaseException, exceptionLevel)
        {

        }


        #endregion

    }
}
