﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.CommonLibrary.Exceptions
{
    [Serializable]
    public class BLException : ExceptionBase
    {
        #region Constructors

        public BLException(string message)
            : base(message)
        {

        }

        public BLException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public BLException(string message, Exception innerException, ExceptionLevel exceptionLevelOverride)
            : base(message, innerException, exceptionLevelOverride)
        {

        }


        #endregion
    }
}
