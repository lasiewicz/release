﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace Spark.CommonLibrary.Exceptions
{
    public class ExceptionUtility
    {
        public static void LogException(ExceptionBase exceptionBase, Type type, string title)
        {
            ILog logger = log4net.LogManager.GetLogger(type);

            if (exceptionBase.ExceptionLevel == ExceptionLevel.Error)
            {
                logger.Error(title + "\r\n" + "\r\n" + exceptionBase.ExceptionMessageTrace + "\r\n", exceptionBase);
            }
            else if (exceptionBase.ExceptionLevel == ExceptionLevel.Fatal)
            {
                logger.Fatal(title + "\r\n" + "\r\n" + exceptionBase.ExceptionMessageTrace + "\r\n" + "\r\n", exceptionBase);
            }
        }
    }
}
