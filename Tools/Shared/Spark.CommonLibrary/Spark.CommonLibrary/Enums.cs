﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.CommonLibrary
{
    public enum ExceptionLevel : int
    {
        None = 0,
        Debug = 1,
        Information = 2,
        Warning = 3,
        Error = 4,
        Fatal = 5
    }

    #region Site ID enum
    public enum SiteIDs : int
    {
        None = 0,
        JDateCoIL = 4,
        MatchnetUK = 6,
        DateCA = 13,
        Cupid = 15,
        Spark = 101,
        Glimpse = 102,
        JDate = 103,
        JDateFR = 105,
        JDateUK = 107,
        Matchnet = 108,
        CollegeLuv = 112,
        JewishMingle = 9171,
        BBW = 9041,
        BlackMingle = 9051,
        ChristianMingle = 9081,
        InterRacialMingle = 9151,
        ItalianMingle = 9161,
        NRGDating = 19,
        AdventistSingles = 9011,
        CatholicMingle = 9071,
        DeafSingles = 9111,
        LDSMingle = 9191,
        MilitarySingles = 9221,
        SilverSingles = 9231,
        LDSSingles = 9281
        
    }

    public enum CommunityIDs: int
    {
        None=0,
        Spark=1,
        JDate=3,
        Cupid=10,
        ItalianSinglesConnection=21,
        InterRacialSingles=22,
        BBWPersonalsPlus=23,
        BlackSingles=24,
        ChristianMingle=25
    }
    #endregion
}
