﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SODBQueue.ValueObjects
{
    public class SodbReviewQueueItem
    {
        public int SSPBlueId { get; set; }
        public int SiteId { get; set; }
        public int ReviewStatusId { get; set; }
        public Guid? SelectId { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    
}
