﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SODBQueue.ValueObjects
{
    public class SodbQueueCounts
    {
        public QueueType SodbQueueType { get; set; }
        public int Count { get; set; }
        public int SiteId { get; set; }
        public int MaxHours { get; set; }
        public int AverageHours { get; set; }
        public int Level { get; set; }
        public int QueueCountType { get; set; }
    }

    public class SodbAgentReviewCounts
    {
        public int AdminId { get; set; }
        public int PositiveMatchCount { get; set; }
        public int NegativeMatchCount { get; set; }
        public int RequestSSPCount { get; set; }
        public int RequestPhotoCount { get; set; }
        public int EscalateCount { get; set; }
    }

    public enum QueueType
    {
        ReviewQueue = 1,
        SecondReviewQueue = 2
    }

    public enum QueueCountType
    {
        Site = 1,
        Level = 2
    }
}
