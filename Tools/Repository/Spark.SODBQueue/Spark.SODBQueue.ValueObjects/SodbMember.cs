﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Spark.SODBQueue.ValueObjects
{
    public class SodbMember
    {
        public int SSPBlueId { get; set; }
        public int CustomerId { get; set; } 
        public int CallingSystemId { get; set; }
        public int AgeDaysDifference { get; set; }
        public int ZipDistance { get; set; }
        public string MatchType { get; set; }

        public string SODB_FirstName { get; set; }
        public string SODB_LastName { get; set; }
        public string SODB_State { get; set; }
        public string SODB_SourceState { get; set; }
        public int SODB_Zip { get; set; }
        public string SODB_Gender { get; set; }
        public DateTime SODB_DOB { get; set; }
        public int SODBImagesCount { get; set; }
        public string SODB_ImageLink { get; set; }

        public string PU_FirstName { get; set; }
        public string PU_LastName { get; set; }
        public string PU_State { get; set; }
        public string PU_Zip { get; set; } 
        public string PU_Gender { get; set; }
        public DateTime PU_DOB { get; set; }
        public string Utah_PhotoURLS { get; set; }
        public int UtahImagesCount { get; set; }

    }
}
