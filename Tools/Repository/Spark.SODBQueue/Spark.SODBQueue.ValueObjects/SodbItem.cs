﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SODBQueue.ValueObjects
{
    public class SodbItem
    {
        public int SSPBlueId { get; set; }
        public int MemberId { get; set; }
        public int SiteId { get; set; }
        public int AgeDaysDifference { get; set; }
        public int ZipDistance { get; set; }
        public string MatchType { get; set; }
        public int AdminId { get; set; }
        public int AdminActionId { get; set; }
        public string AdminNotes { get; set; }
        public int Level { get; set; }
        public bool IsBHMember { get; set; }
        public string Email { get; set; }

        public string SodbFirstName { get; set; }
        public string SodbLastName { get; set; }
        public string SodbState { get; set; }
        public string SodbSourceState { get; set; }
        public int SodbZip { get; set; }
        public string SodbGender { get; set; }
        public DateTime SodbDateOfBirth { get; set; }
        public int SodbImagesCount { get; set; }
        public string SodbImageLink { get; set; }

        public string SparkFirstName { get; set; }
        public string SparkLastName { get; set; }
        public string SparkState { get; set; }
        public string SparkZip { get; set; } 
        public string SparkGender { get; set; }
        public DateTime SparkDateOfBirth { get; set; }
        public string UtahPhotoUrls { get; set; }
        public int UtahImagesCount { get; set; }

        public List<SodbAdminAction> AdminActions { get; set; }

        public SodbItem()
        {
            AdminActions = new List<SodbAdminAction>();
        }
    }

    public class SodbAdminAction
    {
        public int AdminId { get; set; }
        public int AdminActionId { get; set; }
        public DateTime ActionDate { get; set; }
    }

    public enum ReviewStatus
    {
        Inserted = 1,
        Pending = 2,
        Complete = 3,
        Escalate = 4,
        WaitingforSSP = 5,
        WaitingforMemberPhoto = 6,
        PhotoUploaded = 7
    }

    public enum SodbDecision
    {
        None = 0,
        PositiveMatch = 1,
        NegativeMatch = 2,
        RequestMoreInfo = 3,
        RequestPhoto = 4,
        Escalate = 5
    }
}
