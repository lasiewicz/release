﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Spark.SODBQueue.ValueObjects;

namespace Spark.SODBQueue.DataAccess
{
    internal class SodbQueueDAL
    {
        private const string DbServerName = "ladatamart01.matchnet.com";
        private const string DatabaseNameTest = "sodb_queue_test";
        private const string DatabaseNameprod = "sodb_queue";
        public readonly static SodbQueueDAL Instance = new SodbQueueDAL();

        public string Environment { get; set; }
        public string DatabaseName
        {
            get { return (Environment.ToLower() == "prod") ? DatabaseNameprod : DatabaseNameTest; }
        }

        private SodbQueueDAL() { }

        public DataSet GetSodbItem()
        {
            string connectionString = GetConnectionString();
            var conn = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand();
            var ds = new DataSet();
            try
            {
                conn.Open();
                sqlCommand.CommandText = "dbo.usp_GetSodbItem";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Connection = conn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error getting SODB Queue Item from Database. ConnectionString: {0} SP : {1}", connectionString, "dbo.usp_GetSodbItem"), ex);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return ds;
        }

        public void CompleteSodbReview(SodbItem sodbItem)
        {
            string connectionString = GetConnectionString();
            var conn = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand();

            try
            {
                conn.Open();
                sqlCommand.CommandText = "dbo.usp_CompleteSodbReview";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param;

                param = new SqlParameter
                            {ParameterName = "@SSPBlueID", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = sodbItem.SSPBlueId};
                sqlCommand.Parameters.Add(param);

                param = new SqlParameter
                            {ParameterName = "@CustomerID", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = sodbItem.MemberId};
                sqlCommand.Parameters.Add(param);

                param = new SqlParameter { ParameterName = "@CallingSystemID", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = sodbItem.SiteId };
                sqlCommand.Parameters.Add(param);

                param = new SqlParameter { ParameterName = "@AdminID", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = sodbItem.AdminId };
                sqlCommand.Parameters.Add(param);

                param = new SqlParameter { ParameterName = "@AdminActionID", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = sodbItem.AdminActionId };
                sqlCommand.Parameters.Add(param);

                if (!string.IsNullOrEmpty(sodbItem.AdminNotes))
                {
                    param = new SqlParameter
                                {
                                    ParameterName = "@AdminNotes",
                                    DbType = DbType.String,
                                    Direction = ParameterDirection.Input,
                                    Value = sodbItem.AdminNotes
                                };
                    sqlCommand.Parameters.Add(param);
                }
                sqlCommand.Connection = conn;
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error completing SODB item review. ConnectionString: {0} SP : {1}", connectionString, "dbo.usp_CompleteSodbReview"), ex);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        public DataSet GetSodbReviewQueueStats()
        {
            string connectionString = GetConnectionString();
            var conn = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand();
            var ds = new DataSet();
            try
            {
                conn.Open();
                sqlCommand.CommandText = "dbo.usp_GetSodbReviewQueueStats";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Connection = conn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error getting SODB Queue stats from Database. ConnectionString: {0} SP : {1}", connectionString, "dbo.usp_GetSodbReviewQueueStats"), ex);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return ds;
        }

        public DataSet GetSodbSecondReviewItem(int adminId)
        {
            string connectionString = GetConnectionString();
            var conn = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand();
            var ds = new DataSet();
            try
            {
                conn.Open();
                SqlParameter param;
                sqlCommand.CommandText = "dbo.usp_GetSodbSecondReviewItem";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                param = new SqlParameter { ParameterName = "@AdminID", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = adminId };
                sqlCommand.Parameters.Add(param);

                sqlCommand.Connection = conn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error getting SODB second review Queue item from Database. ConnectionString: {0} SP : {1}", connectionString, "dbo.usp_GetSodbSecondReviewItem"), ex);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return ds;
        }

        public DataSet GetSodbAgentStats(DateTime startDate, DateTime endDate, int? siteId)
        {
            string connectionString = GetConnectionString();
            var conn = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand();
            var ds = new DataSet();
            try
            {
                conn.Open();
                SqlParameter param;
                sqlCommand.CommandText = "dbo.usp_GetSodbAgentReviewStats";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                param = new SqlParameter { ParameterName = "@StartDate", DbType = DbType.DateTime, Direction = ParameterDirection.Input, Value = startDate };
                sqlCommand.Parameters.Add(param);
                param = new SqlParameter { ParameterName = "@EndDate", DbType = DbType.DateTime, Direction = ParameterDirection.Input, Value = endDate };
                sqlCommand.Parameters.Add(param);
                if (siteId.HasValue)
                {
                    param = new SqlParameter { ParameterName = "@SiteId", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = siteId.Value};
                    sqlCommand.Parameters.Add(param);
                }

                sqlCommand.Connection = conn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error getting SODB agent review stats. ConnectionString: {0} SP : {1}", connectionString, "dbo.usp_GetSodbAgentReviewStats"), ex);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return ds;
        }

        public DataSet SearchSodbMember(int memberId, int? siteId, int? sspBlueId)
        {
            string connectionString = GetConnectionString();
            var conn = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand();
            var ds = new DataSet();
            try
            {
                conn.Open();
                SqlParameter param;
                sqlCommand.CommandText = "dbo.usp_SearchSodbMember";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                param = new SqlParameter { ParameterName = "@MemberId", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = memberId };
                sqlCommand.Parameters.Add(param);
                if (siteId.HasValue)
                {
                    param = new SqlParameter { ParameterName = "@SiteId", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = siteId.Value };
                    sqlCommand.Parameters.Add(param);
                }
                if (sspBlueId.HasValue)
                {
                    param = new SqlParameter { ParameterName = "@SSPBlueId", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = sspBlueId.Value };
                    sqlCommand.Parameters.Add(param);
                }

                sqlCommand.Connection = conn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error searching SODB Member. ConnectionString: {0} SP : {1}", connectionString, "dbo.usp_SearchSodbMember"), ex);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return ds;
        }

        public DataSet GetSodbMemberById(int memberId, int siteId, int sspBlueId)
        {
            string connectionString = GetConnectionString();
            var conn = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand();
            var ds = new DataSet();
            try
            {
                conn.Open();
                SqlParameter param;
                sqlCommand.CommandText = "dbo.usp_GetSodbItemById";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                param = new SqlParameter { ParameterName = "@CustomerId", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = memberId };
                sqlCommand.Parameters.Add(param);
                param = new SqlParameter { ParameterName = "@CallingSystemId", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = siteId };
                sqlCommand.Parameters.Add(param);
                param = new SqlParameter { ParameterName = "@SSPBlueId", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = sspBlueId };
                sqlCommand.Parameters.Add(param);

                sqlCommand.Connection = conn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error getting SODB Member. ConnectionString: {0} SP : {1}", connectionString, "dbo.usp_GetSodbItemById"), ex);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return ds;
        }

        public void UpdatePhotoUploadStatus(int memberId, int siteId, int sspBlueId)
        {
            string connectionString = GetConnectionString();
            var conn = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand();

            try
            {
                conn.Open();
                sqlCommand.CommandText = "dbo.usp_UpdateMemberPhotoUploadStatus";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param;

                param = new SqlParameter { ParameterName = "@CustomerID", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = memberId };
                sqlCommand.Parameters.Add(param);

                param = new SqlParameter { ParameterName = "@CallingSystemID", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = siteId };
                sqlCommand.Parameters.Add(param);

                param = new SqlParameter { ParameterName = "@SSPBlueID", DbType = DbType.Int64, Direction = ParameterDirection.Input, Value = sspBlueId };
                sqlCommand.Parameters.Add(param);

                sqlCommand.Connection = conn;
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error updating photo upload status. ConnectionString: {0} SP : {1}", connectionString, "dbo.usp_UpdateMemberPhotoUploadStatus"), ex);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        public DataSet GetSodbMembersWithNoPhoto()
        {
            string connectionString = GetConnectionString();
            var conn = new SqlConnection(connectionString);
            var sqlCommand = new SqlCommand();
            var ds = new DataSet();
            try
            {
                conn.Open();
                SqlParameter param;
                sqlCommand.CommandText = "dbo.usp_GetMembersWithNoPhoto";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Connection = conn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error getting SODB members with no photos. ConnectionString: {0} SP : {1}", connectionString, "dbo.usp_GetMembersWithNoPhoto"), ex);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return ds;
        }

        private string GetConnectionString()
        {
            return string.Format(@"Server={0};Database={1};user id={2};password={3}", DbServerName,
                                 (!string.IsNullOrEmpty(Environment) && Environment.ToLower() == "prod")
                                     ? DatabaseName
                                     : DatabaseNameTest, "sodb_app", "s0dB876412Sp4rk");
        }
    }
}
