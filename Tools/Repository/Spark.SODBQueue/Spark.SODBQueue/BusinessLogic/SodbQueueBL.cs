﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Data;
using Spark.SODBQueue.DataAccess;
using Spark.SODBQueue.ValueObjects;
using System.Data;

namespace Spark.SODBQueue.BusinessLogic
{
    internal class SodbQueueBL
    {
        public readonly static SodbQueueBL Instance = new SodbQueueBL();
        public string Environment { get; set; }

        private SodbQueueBL()
        {
        }

        public SodbItem GetSodbItemToReview()
        {
            SodbQueueDAL.Instance.Environment = Environment;
            SodbItem sodbItem = null;
            try
            {
                DataSet ds = SodbQueueDAL.Instance.GetSodbItem();
                if (ds != null && ds.Tables.Count > 0)
                    sodbItem = ConvertToSodbItem(ds);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to get SODB queue item.", ex);
            }
            return sodbItem;
        }

        public void CompleteSodbReview(SodbItem sodbItem)
        {
            SodbQueueDAL.Instance.Environment = Environment;
            try
            {
                SodbQueueDAL.Instance.CompleteSodbReview(sodbItem);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to complete SODB queue item review.", ex);
            }
        }

        public List<SodbQueueCounts> GetSodbReviewQueueStats()
        {
            SodbQueueDAL.Instance.Environment = Environment;
            var queueCounts = new List<SodbQueueCounts>();
            try
            {
                DataSet ds = SodbQueueDAL.Instance.GetSodbReviewQueueStats();
                if (ds != null && ds.Tables.Count > 0)
                   queueCounts = ConvertToSodbQueueCounts(ds);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to get SODB queue stats.", ex);
            }
            return queueCounts;
        }

        public SodbItem GetSodbSecondReviewItem(int adminId)
        {
            SodbQueueDAL.Instance.Environment = Environment;
            SodbItem sodbItem = null;
            try
            {
                DataSet ds = SodbQueueDAL.Instance.GetSodbSecondReviewItem(adminId);
                if (ds != null && ds.Tables.Count > 0)
                    sodbItem = ConvertToSodbItem(ds);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to get SODB queue item.", ex);
            }
            return sodbItem;
        }

        public List<SodbAgentReviewCounts> GetSodbAgentReviewStats(DateTime startDate, DateTime endDate, int? siteId)
        {
            SodbQueueDAL.Instance.Environment = Environment;
            var sodbAgentStats = new List<SodbAgentReviewCounts>();
            try
            {
                DataSet ds = SodbQueueDAL.Instance.GetSodbAgentStats(startDate, endDate,siteId);
                if (ds != null)
                    sodbAgentStats = ConvertToSodbAgentReviewCounts(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to get SODB agent review stats.", ex);
            }
            return sodbAgentStats;
        }

        public List<SodbItem> SearchSodbMember(int memberId, int? siteId, int? sspBlueId)
        {
            SodbQueueDAL.Instance.Environment = Environment;
            var sodbMembers = new List<SodbItem>();
            try
            {
                DataSet ds = SodbQueueDAL.Instance.SearchSodbMember(memberId, siteId, sspBlueId);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        sodbMembers.Add(new SodbItem { SiteId = Convert.ToInt32(row["callingsystemid"]),
                                                       SSPBlueId = Convert.ToInt32(row["SSPBlueId"]),
                                                       MemberId = Convert.ToInt32(row["customerid"]),
                                                       Level = Convert.ToInt16(row["MatchType"].ToString().Substring(0, 2))});
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to search SODB member.", ex);
            }
            return sodbMembers;
        }

        public SodbItem GetSodbMemberById(int memberId, int siteId, int sspBlueId)
        {
            SodbQueueDAL.Instance.Environment = Environment;
            SodbItem sodbItem = null;
            try
            {
                DataSet ds = SodbQueueDAL.Instance.GetSodbMemberById(memberId, siteId, sspBlueId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    sodbItem = ConvertToSodbItem(ds);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to get SODB member.", ex);
            }
            return sodbItem;
        }

        public void UpdatePhotoUploadStatus(int memberId, int siteId, int sspBlueId)
        {
            SodbQueueDAL.Instance.Environment = Environment;
            try
            {
                SodbQueueDAL.Instance.UpdatePhotoUploadStatus(memberId, siteId, sspBlueId);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to update member photo upload status.", ex);
            }
        }

        public List<SodbItem> GetSodbMembersWithNoPhoto()
        {
            SodbQueueDAL.Instance.Environment = Environment;
            var sodbMembers = new List<SodbItem>();
            try
            {
                DataSet ds = SodbQueueDAL.Instance.GetSodbMembersWithNoPhoto();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        sodbMembers.Add(new SodbItem
                        {
                            SiteId = Convert.ToInt32(row["callingsystemid"]),
                            SSPBlueId = Convert.ToInt32(row["SSPBlueId"]),
                            MemberId = Convert.ToInt32(row["customerid"]),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to get SODB members with no photo.", ex);
            }
            return sodbMembers;
        }

        private SodbItem ConvertToSodbItem(DataSet ds)
        {
            var sodbItem = new SodbItem();
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                var row = ds.Tables[0].Rows[0];

                
                sodbItem.SiteId = Convert.ToInt32(row["callingsystemid"]);
                sodbItem.MemberId = Convert.ToInt32(row["customerid"]);
                sodbItem.SSPBlueId = Convert.ToInt32(row["SSPBlueId"]);
                sodbItem.AgeDaysDifference = (row["AgeDaysDifference"] != null)
                                                 ? Convert.ToInt32(row["AgeDaysDifference"])
                                                 : 0;
                sodbItem.ZipDistance = Convert.ToInt32(row["ZipDistance"]);
                sodbItem.MatchType = Convert.ToString(row["MatchType"]);
                sodbItem.SodbFirstName = (row["SODB_FirstName"] != null) ? Convert.ToString(row["SODB_FirstName"]) : "";
                sodbItem.SodbLastName = (row["SODB_LastName"] != null) ? Convert.ToString(row["SODB_LastName"]) : "";
                sodbItem.SodbState = (row["SODB_State"] != null) ? Convert.ToString(row["SODB_State"]) : "";
                sodbItem.SodbSourceState = (row["SODB_SourceState"] != null)
                                               ? Convert.ToString(row["SODB_SourceState"])
                                               : "";
                sodbItem.SodbZip = (row["SODB_ZIP"] != null) ? Convert.ToInt32(row["SODB_ZIP"]) : 0;
                sodbItem.SodbGender = (row["SODB_Gender"] != null) ? Convert.ToString(row["SODB_Gender"]) : "";
                sodbItem.SodbDateOfBirth = (row["SODB_DOB"] != null)
                                               ? Convert.ToDateTime(row["SODB_DOB"])
                                               : DateTime.MinValue;
                sodbItem.SodbImagesCount = Convert.ToInt32(row["sodbImagesCount"]);
                sodbItem.SodbImageLink = (row["SODB_ImageLink"] != null) ? Convert.ToString(row["SODB_ImageLink"]) : "";
                sodbItem.SparkFirstName = (row["PU_FirstName"] != null) ? Convert.ToString(row["PU_FirstName"]) : "";
                sodbItem.SparkLastName = (row["PU_LastName"] != null) ? Convert.ToString(row["PU_LastName"]) : "";
                sodbItem.SparkState = (row["PU_State"] != null) ? Convert.ToString(row["PU_State"]) : "";
                sodbItem.SparkZip = (row["PU_Zip"] != null) ? Convert.ToString(row["PU_Zip"]) : "";
                sodbItem.SparkGender = (row["PU_Gender"] != null) ? Convert.ToString(row["PU_Gender"]) : "";
                sodbItem.SparkDateOfBirth = (row["PU_DOB"] != null)
                                                ? Convert.ToDateTime(row["PU_DOB"])
                                                : DateTime.MinValue;
                sodbItem.UtahPhotoUrls = (row["Utah_PhotoURLS"] != null) ? Convert.ToString(row["Utah_PhotoURLS"]) : "";
                sodbItem.UtahImagesCount = Convert.ToInt32(row["UtahImagesCount"]);
                sodbItem.AdminId = (row.Table.Columns.Contains("AdminId") && row["AdminId"] != null)
                                       ? Convert.ToInt32(row["AdminId"])
                                       : 0;
                sodbItem.AdminNotes = (row.Table.Columns.Contains("AdminNotes") && row["AdminNotes"] != null)
                                          ? Convert.ToString(row["AdminNotes"])
                                          : "";
                sodbItem.Level = (row.Table.Columns.Contains("MatchType") && row["MatchType"] != null)
                                     ? Convert.ToInt16(row["MatchType"].ToString().Substring(0, 2))
                                     : 0;
                sodbItem.IsBHMember = (row.Table.Columns.Contains("Domain") && row["Domain"] != null) && (row["Domain"].ToString() == "BH");
                sodbItem.Email = (row.Table.Columns.Contains("emailaddress") && row["emailaddress"] != null) ? row["emailaddress"].ToString() : "";
            }

            //Admin action history
            if (ds != null && ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
            {
                sodbItem.AdminActions = new List<SodbAdminAction>();
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    sodbItem.AdminActions.Add(new SodbAdminAction
                    {
                        AdminId = Convert.ToInt32(dr["AdminId"]),
                        AdminActionId = Convert.ToInt32(dr["AdminActionId"]),
                        ActionDate = Convert.ToDateTime(dr["InsertDate"])
                    });
                }
                sodbItem.AdminActions.Sort((x,y) => (x.ActionDate.CompareTo(y.ActionDate)));
            }
            return sodbItem;
        }

        private List<SodbQueueCounts> ConvertToSodbQueueCounts(DataSet ds)
        {
            var queueCounts = new List<SodbQueueCounts>();
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable table = ds.Tables[0];
                    if (table != null && table.Rows.Count > 0)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var queueCount = new SodbQueueCounts();
                            if (row["QueueType"] != null)
                                queueCount.SodbQueueType =
                                    (QueueType) (Enum.Parse(typeof (QueueType), row["QueueType"].ToString()));
                            if (row["QueueCount"] != null)
                                queueCount.Count = Convert.ToInt32(row["QueueCount"]);
                            if (table.Columns.Contains("SiteID") && row["SiteID"] != null)
                                queueCount.SiteId = Convert.ToInt32(row["SiteID"]);
                            if (table.Columns.Contains("MaxHours") && row["MaxHours"] != null)
                                queueCount.MaxHours = Convert.ToInt32(row["MaxHours"]);
                            if (table.Columns.Contains("AverageHours") && row["AverageHours"] != null)
                                queueCount.AverageHours = Convert.ToInt32(row["AverageHours"]);
                            queueCount.QueueCountType = (int) QueueCountType.Site;
                            queueCounts.Add(queueCount);
                        }
                    }
                }
                if(ds.Tables.Count > 1)
                {
                    DataTable table = ds.Tables[1];
                    if (table != null && table.Rows.Count > 0)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            var queueCount = new SodbQueueCounts();
                            if (row["QueueType"] != null)
                                queueCount.SodbQueueType =
                                    (QueueType)(Enum.Parse(typeof(QueueType), row["QueueType"].ToString()));
                            if (row["QueueCount"] != null)
                                queueCount.Count = Convert.ToInt32(row["QueueCount"]);
                            if (table.Columns.Contains("Level") && row["Level"] != null)
                                queueCount.Level = Convert.ToInt32(row["Level"]);
                            if (table.Columns.Contains("MaxHours") && row["MaxHours"] != null)
                                queueCount.MaxHours = Convert.ToInt32(row["MaxHours"]);
                            if (table.Columns.Contains("AverageHours") && row["AverageHours"] != null)
                                queueCount.AverageHours = Convert.ToInt32(row["AverageHours"]);
                            queueCount.QueueCountType = (int)QueueCountType.Level;
                            queueCounts.Add(queueCount);
                        }
                    }
                }
            }
            return queueCounts;
        }


        private List<SodbAgentReviewCounts> ConvertToSodbAgentReviewCounts(DataTable table)
        {
            SodbAgentReviewCounts queueCount = null;
            var queueCounts = new List<SodbAgentReviewCounts>();
            bool addToList = false;
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    addToList = false;
                    int adminId = (row["AdminID"] != null) ? Convert.ToInt32(row["AdminID"]) : 0;
                    queueCount = queueCounts.Where(a => a.AdminId == adminId).FirstOrDefault();
                    if (queueCount == null)
                    {
                        queueCount = new SodbAgentReviewCounts();
                        queueCount.AdminId = adminId;
                        addToList = true;
                    }
                    int adminActionId = (row["AdminActionID"] != null) ? Convert.ToInt32(row["AdminActionID"]) : 0;
                    int adminActionCount = (row["ActionCount"] != null) ? Convert.ToInt32(row["ActionCount"]) : 0;
                    switch(adminActionId)
                    {
                        case (int)SodbDecision.PositiveMatch:
                            queueCount.PositiveMatchCount = adminActionCount;
                        break;
                        case (int)SodbDecision.NegativeMatch:
                            queueCount.NegativeMatchCount = adminActionCount;
                        break;
                        case (int)SodbDecision.RequestMoreInfo:
                            queueCount.RequestSSPCount = adminActionCount;
                        break;
                        case (int)SodbDecision.RequestPhoto:
                            queueCount.RequestPhotoCount = adminActionCount;
                        break;
                        case (int)SodbDecision.Escalate:
                            queueCount.EscalateCount = adminActionCount;
                        break;
                    }
                    if (addToList)
                        queueCounts.Add(queueCount);
                }
            }
            
            return queueCounts;
        }
    }
}
