﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.SODBQueue.BusinessLogic;
using Spark.SODBQueue.ValueObjects;

namespace Spark.SODBQueue.SeviceAdapters
{
    public class SodbQueueSA
    {
        public readonly static SodbQueueSA Instance = new SodbQueueSA();
        public string Environment { get; set; }

        private SodbQueueSA()
        {
        }

        public SodbItem GetSodbItemToReview()
        {
            SodbQueueBL.Instance.Environment = Environment;
            SodbItem sodbItem = null;
            sodbItem = SodbQueueBL.Instance.GetSodbItemToReview();
            return sodbItem;
        }

        public List<SodbQueueCounts> GetSodbReviewQueueStats()
        {
            SodbQueueBL.Instance.Environment = Environment;
            return SodbQueueBL.Instance.GetSodbReviewQueueStats();
        }

        public void CompleteSodbItemReview(SodbItem sodbItem)
        {
            SodbQueueBL.Instance.Environment = Environment;
            SodbQueueBL.Instance.CompleteSodbReview(sodbItem);
        }

        public SodbItem GetSodbSecondReviewItem(int adminId)
        {
            SodbQueueBL.Instance.Environment = Environment;
            SodbItem sodbItem = null;
            sodbItem = SodbQueueBL.Instance.GetSodbSecondReviewItem(adminId);
            return sodbItem;
        }

        public List<SodbAgentReviewCounts> GetSodbAgentReviewStats(DateTime startDate, DateTime endDate, int? siteId)
        {
            SodbQueueBL.Instance.Environment = Environment;
            return SodbQueueBL.Instance.GetSodbAgentReviewStats(startDate, endDate,siteId);
        }


        public List<SodbItem> SearchSodbMember(int memberId, int? siteId, int? sspBlueId)
        {
            SodbQueueBL.Instance.Environment = Environment;
            return SodbQueueBL.Instance.SearchSodbMember(memberId, siteId, sspBlueId);
        }

        public SodbItem GetSodbMemberById(int memberId, int siteId, int sspBlueId)
        {
            SodbQueueBL.Instance.Environment = Environment;
            return SodbQueueBL.Instance.GetSodbMemberById(memberId, siteId, sspBlueId);
        }

        public void UpdatePhotoUploadStatus(int memberId, int siteId, int sspBlueId)
        {
            SodbQueueBL.Instance.Environment = Environment;
            SodbQueueBL.Instance.UpdatePhotoUploadStatus(memberId, siteId, sspBlueId);
        }

        public List<SodbItem> GetSodbMembersWithNoPhoto()
        {
            SodbQueueBL.Instance.Environment = Environment;
            return SodbQueueBL.Instance.GetSodbMembersWithNoPhoto();
        }
    }
}
