﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.SODBQueue.BusinessLogic;
using Spark.SODBQueue.SeviceAdapters;

namespace Spark.SODBQueue.Test
{
    
    public class SodbQueueBLTest
    {
        [Test]
        public void GetSodbMemberToReviewTest()
        {
            SodbQueueSA.Instance.Environment = "dev";
            var sodbItem = SodbQueueSA.Instance.GetSodbItemToReview();
            Assert.IsNotNull(sodbItem);

        }
    }
}
