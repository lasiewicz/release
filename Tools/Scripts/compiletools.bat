set buildtxtpath= ..\..\scripts\LOGS\ToolsBuild.txt

rem echo off 
set branch=%1

set branch=%branch:~5,25%


if "%2"=="debug" goto Setdebug
if "%2"==" " goto Setdebug
if "%2"=="release" goto setrelease
echo invalid build type specified
goto end

:setdebug
set buildtype=debug
goto build

:setrelease
set buildtype=release
goto build

:build



mkdir ..\scripts\LOGS\
rmdir /S /Q ..\bin 


p4 sync -f //toolsscc/%1/ipblocker/...#head
p4 sync -f //toolsscc/%1/shared/...#head
p4 sync -f //toolsscc/%1/service/...#head
p4 sync -f //toolsscc/%1/Applications/...#head

if "%3"=="teamcity" goto Teamcity
if "%3"=="Teamcity" goto Teamcity
if "%3"=="TEAMCITY" goto Teamcity



cd ..\shared\spark.commonLibrary\

msbuild spark.commonLibrary.sln /p:configuration=release > %buildtxtpath%

cd ..\..\IpBlocker\Spark.IPBlocker\

msbuild Spark.IPBlocker.sln /p:configuration=release >> %buildtxtpath%

cd ..\..\ipBlocker\Spark.IPBlocker.Admin

msbuild Spark.IPBlocker.Admin.sln /p:configuration=release >> %buildtxtpath%


cd ..\..\Applications\AdminTool\

msbuild admintool.sln /p:configuration=release >> %buildtxtpath%

cd ..\..\service\Spark.Monitoring\

msbuild Spark.Monitoring.sln /p:configuration=release >> %buildtxtpath%


cd ..\..\Applications\LogViewer\

msbuild LogViewer.sln /p:configuration=release >> %buildtxtpath%


cd ..\..\scripts
goto end
Exit


:Teamcity
if %1 =="toolsprod" goto JustBuild
if %1 =="Toolsprod" goto JustBuild
if %1 =="TOOLSPROD" goto JustBuild
move ..\..\main\bin ..\..\main\BinOfMain
set branchPath=..\..\%branch%\bin
move %branchpath% ..\..\main\Bin

:JustBuild


cd ..\shared\spark.commonLibrary\

msbuild spark.commonLibrary.sln /p:configuration=%buildtype%  /logger:JetBrains.BuildServer.MSBuildLoggers.MSBuildLogger,..\..\scripts\TCLogging\JetBrains.BuildServer.MSBuildLoggers.dll > ..\..\scripts\logs\build.xml



cd ..\..\IpBlocker\Spark.IPBlocker\

msbuild Spark.IPBlocker.sln /p:configuration=%buildtype%  /logger:JetBrains.BuildServer.MSBuildLoggers.MSBuildLogger,..\..\scripts\TCLogging\JetBrains.BuildServer.MSBuildLoggers.dll >> ..\..\scripts\logs\build.xml


cd ..\..\ipBlocker\Spark.IPBlocker.Admin

msbuild Spark.IPBlocker.Admin.sln /p:configuration=%buildtype%  /logger:JetBrains.BuildServer.MSBuildLoggers.MSBuildLogger,..\..\scripts\TCLogging\JetBrains.BuildServer.MSBuildLoggers.dll >> ..\..\scripts\logs\build.xml

cd ..\..\Applications\AdminTool\

msbuild AdminTool.sln /p:configuration=%buildtype%  /logger:JetBrains.BuildServer.MSBuildLoggers.MSBuildLogger,..\..\scripts\TCLogging\JetBrains.BuildServer.MSBuildLoggers.dll >> ..\..\scripts\logs\build.xml

cd ..\..\service\Spark.Monitoring\

msbuild Spark.Monitoring.sln /p:configuration=%buildtype%  /logger:JetBrains.BuildServer.MSBuildLoggers.MSBuildLogger,..\..\scripts\TCLogging\JetBrains.BuildServer.MSBuildLoggers.dll >> ..\..\scripts\logs\build.xml


cd ..\..\scripts 
if %1 =="toolsprod" goto JustBuild2
if %1 =="Toolsprod" goto JustBuild2
if %1 =="TOOLSPROD" goto JustBuild2


move ..\..\main\Bin %branchpath% 
move ..\..\main\BinOfMain ..\..\main\bin 

:JustBuild2
:end