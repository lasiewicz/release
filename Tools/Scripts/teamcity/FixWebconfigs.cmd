rem echo off 

set env=%1
set app=%2

if "%1"=="laadmintools01.corp.matchnet.com" goto Preprod
if "%1"=="Laadmintools01.corp.matchnet.com" goto Preprod
if "%1"=="LAADMINTOOLS01.CORP.MATCHNET.COM" goto Preprod 

set env=%env:~5,7%

ren echo %env% 

del /f /q ..\..\bin\web\%app%\web.config
ren ..\..\bin\web\admintool\web.config.%env% web.config
exit

:Preprod
del /f /q ..\..\bin\web\%app%\web.config
ren ..\..\bin\web\admintool\web.config.prod web.config