dim shell
set shell=createobject("wscript.shell")
Set oShell = CreateObject("WScript.Shell")
Set fso = CreateObject("Scripting.FileSystemObject")
dim WebServArry(20)
dim ServiceArry(20)
oShell.CurrentDirectory = fso.GetParentFolderName(Wscript.ScriptFullName)
curdir=oShell.CurrentDirectory
deployfolder=curdir+"\Deploy"
servfolder=curdir+"\Deploy\Services"
Webservfolder=curdir+"\Deploy\WebServices"
'msgbox(deployfolder)
Set objFolder = fso.CreateFolder(deployfolder)
Set objFolder = fso.CreateFolder(servfolder)
Set objFolder2 = fso.CreateFolder(Webservfolder)
 WebServArry(0)="Spark.AccessScheduler.Service\Spark.AccessScheduler.IISHost"
 WebServArry(1)="Spark.Renewal\Spark.Renewal.Service.IISHost"
 WebServArry(2)="Spark.Payment.Wrapper\Spark.Payment.Wrapper.Service"
 WebServArry(3)="Spark.Purchase\Spark.Purchase.IISHost"
 WebServArry(4)="Spark.Region\Spark.Region.Service"
 WebServArry(5)="Spark.ExternalMail\Spark.ExternalMail.IISHost"
 WebServArry(6)="Spark.ClientConnector\Spark.ClientConnector.IISHost"
 WebServArry(7)="Spark.MatchnetUPSCallback\Spark.MatchnetUPSCallback.IISHost"
 WebServArry(8)="Spark.Discount\Spark.Discount.IISHost"
 WebServArry(9)="Spark.Authorization\Spark.Authorization.Service.IISHost"
 WebServArry(10)="Spark.Access\Spark.Access.IISHost"
 WebServArry(11)="Spark.MingleUPSCallback\Spark.MingleUPSCallback.IISHost"
 WebServArry(12)="Spark.TraceLogConnector\Spark.TraceLogConnector.IISHost"
 WebServArry(13)="Spark.Fraud\Spark.Fraud.IISHost"
 WebServArry(14)="CreditTool\CreditTool"
 WebServArry(15)="VirtualTerminal\VirtualTerminal\VirtualTerminal"
 WebServArry(16)="VirtualTerminal\Spark.Approval\Spark.Approval.Web"
 
 ServiceArry(0)="Spark.Renewal\Spark.Renewal.BatchProcessor.ServiceHost"
 ServiceArry(1)="Spark.Payment\Spark.Payment.WindowsServiceHost"
 ServiceArry(2)="Spark.AccessScheduler.Service\Spark.AccessScheduler.Processor.ServiceHost"
 ServiceArry(3)="Spark.Authorization\Spark.Authorization.BatchProcessor.WindowsServiceHost"
 ServiceArry(4)="Spark.Fraud\Spark.Fraud.Service"


 for x=0 to 3
	fstr=ServiceArry(x)
	dfstr=servfolder
	sourstr="\Service\"
	moveservice
next
for x=0 to 15
	fstr=WebServArry(x)
	dfstr=Webservfolder
	sourstr="\Service\"
	if x>13 then 
		sourstr="\Applications\"
	end if
	movestuff
next

function movestuff
	slash=InStr(WebServArry(x),"\")
	l=len(WebServArry(x))-slash
	folder=mid(WebServArry(x),slash+1,l)
	if x=15 then
		folder="VirtualTerminal"
	end if
	dest=dfstr + "\" + folder
	source=curdir+sourstr + WebServArry(x)
	fso.CreateFolder dest
	fso.CreateFolder dest + "\bin"
	s1=source + "\*.*"
	fso.copyfile s1,dest
	cmd= "attrib -r " + chr(34) + chr(34) + dest + "\*.*" + chr(34) + chr(34) + " /s" 
	oShell.Run cmd , 0, true
	if x=14 then
		f1=source + "\Content"
		'f2=source + "\libraries"
		f3=source + "\Scripts"
		f4=source + "\Service References"
		f5=source + "\Views"
		f6=source + "\Shared"
		t1=dest + "\Content"
		't2=dest + "\libraries"
		t3=dest + "\Scripts"
		t4=dest + "\Service References"
		t5=dest + "\Views"
		t6=dest + "\Shared"
		fso.copyfolder f1,t1
		'fso.copyfolder f2,t2
		fso.copyfolder f3,t3
		fso.copyfolder f4,t4
		fso.copyfolder f5,t5
		fso.copyfolder f6,t6
	end if
	on error resume next
	f1=dest + "\*.vspscc"

	f2=dest + "\*.vsproj"
	f3=dest + "\*.csproj"
	f4=dest + "\*.csproj.user"
	f5=dest + "\*.cs"
	'f6=dest + "\Web.config.stagev*"
	'f7=dest + "\Web.config.prod"
	'f8=dest + "\Web.config.preprod"
	fso.deletefile f1
	fso.deletefile f1
	fso.deletefile f2
	fso.deletefile f3
	fso.deletefile f4
	fso.deletefile f5
	'fso.deletefile f6
	'fso.deletefile f7
	'fso.deletefile f8
	fso.copyfolder source + "\bin",dest + "\bin"
	On Error Goto 0

end function
function moveservice
	slash=InStr(ServiceArry(x),"\")
	l=len(ServiceArry(x))-slash
	folder=mid(ServiceArry(x),slash+1,l)
	dest=dfstr + "\" + folder
	source=curdir+"\bin\Service\" + folder
	fso.CreateFolder dest
if x=4 then
	source=curdir + "\Service\Spark.Fraud\Spark.Fraud.Service\bin\Release"
end if
	s1=source + "\*.*"
	s1=source + "\*.*"
	fso.copyfile s1,dest
	cmd= "attrib -r " + chr(34) + chr(34) + dest + "\*.*" + chr(34) + chr(34) + " /s" 
	oShell.Run cmd , 0, true
	on error resume next
	f1=dest + "\*.pdb"
	f2=dest + "\*.config"
	fso.deletefile f1
	fso.deletefile f2
	fso.copyfolder source + "\bin",dest + "\bin"
	On Error Goto 0

end function