Add-Type -Path .\x64\p4api.net.dll

#ENTER YOUR WORKSPACE SETTINGS HERE
$p4ServerAndPort = 'laperforce01:1666'
$p4UserName='wlasiewicz'
$p4WorkSpace = 'wlasiewicz_BHD-WLASIEWICZ_7030'
$p4Cwd = 'C:\'
$p4SrcBranch = 'Stage2'
$p4DestBranch = 'Stage2'
$env:emailMessage=[string]::Empty
$emailAddresses = "wlasiewicz@spark.net;tlam@spark.net;mmishkoff@spark.net"
#;
#FUNCTIONS
function print($str, $msg){
    Write-Output $str
    $env:emailMessage += $str+"<br/>"
}

function printError($str){
    Write-Error $str
    $env:emailMessage += $str+"<br/>"
}

function sendMail(){
    $from = "William J Lasiewicz <wlasiewicz@spark.net>"
    $replyto = "William J Lasiewicz <wlasiewicz@spark.net>"
    $server = "smtp.matchnet.com"

     Write-Host "Sending Email"
     
     #SMTP server name
     $smtpServer = $server

     #Creating a Mail object
     $msg = new-object Net.Mail.MailMessage
     $msg.isbodyhtml = $true
     
     #Creating SMTP server object
     $smtp = new-object Net.Mail.SmtpClient($smtpServer)
     #$smtp.EnableSsl = $true
     $smtp.UseDefaultCredentials = $true

     #Email structure
     $msg.From = $from
     $msg.ReplyTo = $replyto
     
     $msg.subject = "Perforce AutoMerge Email"
     $msg.body = $env:emailMessage

     $addresses = $emailAddresses.Split(";")
     foreach($a in $addresses){
        if(![string]::IsNullOrEmpty($a)){
            $msg.To.Add($a)
        }
     }   
         
     #Sending email
     $smtp.Send($msg)
     $env:emailMessage=[string]::Empty
}

## MAIN PROGRAM
try
{
    #CONNECT TO PERFORCE
    # define the server, repository and connection
    $serverAddress = New-Object Perforce.P4.ServerAddress($p4ServerAndPort)
    $server = New-Object Perforce.P4.Server($serverAddress)
    $p4Server = New-Object Perforce.P4.P4Server($p4ServerAndPort,$userName,$null,$p4WorkSpace)
    $mRepository = new-Object Perforce.P4.Repository($server)
    $mPerforceConnection = $mRepository.Connection

    # use the connection varaibles for this connection
    $mPerforceConnection.UserName = $p4UserName
    $p4Client=New-Object Perforce.P4.Client
    $mPerforceConnection.Client = $p4Client
    $mPerforceConnection.Client.Name = $p4WorkSpace
    $mPerforceConnection.CurrentWorkingDirectory="$p4Cwd\$p4WorkSpace"
    $p4Server.Client=$p4WorkSpace
    $p4Server.CurrentWorkingDirectory="$p4Cwd\$p4WorkSpace"  

    # connect to the server
    $isConnected = $mPerforceConnection.Connect($null);
    print("$p4UserName is connected to Perforce? $isConnected")

    #OPEN CHANGELISTS FILE
    $changelists = (Get-Content .\changelists.txt) | Sort-Object
    
    foreach($cl in $changelists) {

        try{
           # [System.Windows.Forms.MessageBox]::Show($cl) 
            #get changelist from the current repository
            $changeList = $mRepository.GetChangelist($cl);

            #create new empty changelist to submit
            $newChangeList = New-Object Perforce.P4.Changelist
            $newChangeList = $mRepository.CreateChangelist($newChangeList)
            $newChangeList.Description = "CL: " + $changeList.Id + " - " + $changeList.OwnerName + ": " + $changeList.Description
            $changeCmd0pts = New-Object Perforce.P4.Options([Perforce.P4.ChangeCmdFlags]::Update)
            $newChangeList = $mRepository.UpdateChangelist($newChangeList,$changeCmd0pts)

            #merge changelist
            print("Attempting to merge changelist: " + $changeList.Id)            
            $p4MergeCommand = New-Object Perforce.P4.P4Command($p4Server)
            $p4MergeCommand.Cmd = 'merge'
            $p4MergeCommandArgs = New-Object Perforce.P4.StringList
            $p4MergeCommandArgs.Add('-F')
            $p4MergeCommandArgs.Add("-c" + $newChangeList.Id)
            $p4MergeCommandArgs.Add("-S//Spark/" + $p4SrcBranch)
            $p4MergeCommandArgs.Add("-s//Spark/" + $p4DestBranch+"/...@" + $changeList.Id + ",@" + $changeList.Id)        
            $p4MergeCommandResult=$p4MergeCommand.Run($p4MergeCommandArgs)

            if($p4MergeCommandResult.Success) {
                #resolve changelist       
                $p4ResolveCommand = New-Object Perforce.P4.P4Command($p4Server)
                $p4ResolveCommand.Cmd = 'resolve'
                $p4ResolveCommandArgs = New-Object Perforce.P4.StringList
                $p4ResolveCommandArgs.Add('-am')
                $p4ResolveCommandArgs.Add("-c" + $newChangeList.Id)
                $p4ResolveCommandResult = $p4ResolveCommand.Run($p4ResolveCommandArgs)            

                if($p4ResolveCommandResult.Success){
                    #submit changelist
                    $p4SubmitCommand = New-Object Perforce.P4.P4Command($p4Server)
                    $p4SubmitCommand.Cmd = 'submit'
                    $p4SubmitCommandArgs = New-Object Perforce.P4.StringList
                    $p4SubmitCommandArgs.Add("-c" + $newChangeList.Id)
                    $p4SubmitCommandResult = $p4SubmitCommand.Run($p4SubmitCommandArgs)
                    print "Done!"
                }                
            }
        }
        catch
        {
            printError("Error occurred with changelist: "+ $newChangeList.Id +": " + $_)
            $p4DeleteCommand = New-Object Perforce.P4.P4Command($p4Server)
            $p4DeleteCommand.Cmd = 'change'
            $p4DeleteCommandArgs = New-Object Perforce.P4.StringList
            $p4DeleteCommandArgs.Add('-d')
            $p4DeleteCommandArgs.Add($newChangeList.Id)
            $p4DeleteCommandResult = $p4DeleteCommand.Run($p4DeleteCommandArgs)

            if($p4DeleteCommandResult.Success){                
                print("Delete changelist successful for " + $newChangeList.Id)
            } else {
                printError("Delete changelist failed for " + $newChangeList.Id)
            }
        } 
        finally
        {
            $changeList = $null
            $newChangeList = $null
            $p4MergeCommandArgs = $null
            $p4MergeCommandResult = $null
            $p4ResolveCommandArgs = $null
            $p4ResolveCommandResult = $null
            $p4SubmitCommandArgs = $null
            $p4SubmitCommandResult = $null
            $p4DeleteCommandResult = $null
        }
    }    
} 
catch 
{
    printError("Error occurred trying to connect to project collection: " + $_)
} 
finally 
{
    $isDisconnected = $mPerforceConnection.Disconnect();
    print("$p4UserName is disconnected to Perforce? $isDisconnected")
}

# send result email
sendMail





