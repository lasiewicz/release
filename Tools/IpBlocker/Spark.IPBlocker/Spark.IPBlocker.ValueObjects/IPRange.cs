﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Constants;
using System.Runtime.Serialization;

namespace Spark.IPBlocker.ValueObjects
{
    [DataContract(Namespace = IPBlockerConstants.CONTRACT_NAMESPACE)]
    [KnownType(typeof(BlacklistedIPRange))]
    [KnownType(typeof(WhitelistedIPRange))] 
    [Serializable]
    public class IPRange
    {
        protected List<IPRangeNote> _notes = new List<IPRangeNote>();
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public IPAddress BeginningAddress { get; set; }
        [DataMember]
        public IPAddress EndingAddress { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public Country Country { get; set; }
        [DataMember]
        public List<IPRangeNote> Notes
        {
            get {return _notes;}
            set { _notes = value;} 
        }

        public IPRange() { }

        public IPRange(IPAddress beginningAddress, IPAddress endingAddress)
        {
            BeginningAddress = beginningAddress;
            EndingAddress = endingAddress;
        }
        
        public IPRange(int id, IPAddress beginningAddress, IPAddress endingAddress,
            string createdBy, DateTime createdDate, Country country)
        {
            ID = id;
            BeginningAddress = beginningAddress;
            EndingAddress = endingAddress;
            CreatedBy = createdBy;
            CreatedDate = createdDate;
            Country = country;
        }

        public int Count
        {
            get
            {
                int count = 0;
                if (BeginningAddress.IsValid() && EndingAddress.IsValid())
                {
                    count = (EndingAddress.ToNumber() - BeginningAddress.ToNumber()) + 1;
                }
                return count;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("ID: {0},", ID.ToString()));
            sb.Append(string.Format("Beginning Address: {0},", BeginningAddress == null ? "null" : BeginningAddress.ToString()));
            sb.Append(string.Format("Ending Address: {0},", BeginningAddress == null ? "null" : EndingAddress.ToString()));
            sb.Append(string.Format("Country: {0},", Country == null ? "null" : Country.ToString()));
            sb.Append(string.Format("Created By: {0},", CreatedBy));
            sb.Append(string.Format("Created Date: {0},", CreatedDate.ToString()));
            sb.Append(string.Format("1st Note: {0}", (_notes == null || _notes.Count <1) ? "null" : _notes[0].Note));
            return sb.ToString();
        }
    }
}
