﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Spark.IPBlocker.ValueObjects
{
    [DataContract]
    public class APIResponse
    {
        [DataMember]
        public bool HasAccess { get; set; }
        [DataMember]
        public string Message { get; set; }

        public APIResponse(bool hasAccess, string message)
        {
            HasAccess = hasAccess;
            Message = message;
        }

        public string ToXML()
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.AppendChild(xdoc.CreateXmlDeclaration("1.0", "UTF-8", null));
            XmlNode responseNode = xdoc.CreateElement("Response");
            xdoc.AppendChild(responseNode);

            XmlNode operationNode = xdoc.CreateElement("OperationSuccessful");
            operationNode.InnerText = HasAccess.ToString();
            responseNode.AppendChild(operationNode);

            XmlNode messageNode = xdoc.CreateElement("Message");
            messageNode.InnerText = Message;
            responseNode.AppendChild(messageNode);

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xdoc.WriteTo(xw);

            return sw.ToString();
        }

        public string ToJSON()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(this.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, this);
            return Encoding.Default.GetString(ms.ToArray());
        }
    }
}
