﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Constants;
using System.Runtime.Serialization;

namespace Spark.IPBlocker.ValueObjects
{
    [DataContract(Namespace = IPBlockerConstants.CONTRACT_NAMESPACE)]
    [Serializable]
    public class BlacklistedIPRange : IPRange
    {
        [DataMember]
        public DateTime? ExpireDate { get; set; }

        public BlacklistedIPRange() { }
        
        public BlacklistedIPRange(int id, IPAddress beginningAddress, IPAddress endingAddress,
            string createdBy, DateTime createdDate, Country country, DateTime? expireDate)
            : base(id, beginningAddress, endingAddress, createdBy, createdDate, country)
        {
            ExpireDate = expireDate;
        }
    }
}
