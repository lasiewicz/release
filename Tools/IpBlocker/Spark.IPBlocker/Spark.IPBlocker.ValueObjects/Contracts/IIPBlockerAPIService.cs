﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using Spark.IPBlocker.ValueObjects.Enumerations;
using System.IO;

namespace Spark.IPBlocker.ValueObjects.Contracts
{
    [ServiceContract(Namespace = "http://spark")]
    public interface IIPBlockerAPIService
    {
        [OperationContract, WebGet]
        Stream CheckAccess(string ipaddress, AccessType accessType, int siteID);

        [OperationContract, WebGet]
        Stream CheckAccessJSON(string ipaddress, AccessType accessType, int siteID);

        [OperationContract, WebGet]
        Stream CheckAccessNoLog(string ipaddress, int siteID);

        [OperationContract, WebGet]
        Stream CheckAccessNoLogJSON(string ipaddress, int siteID);

        [OperationContract, WebGet]
        Stream HealthCheck();
    }
}
