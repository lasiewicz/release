﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.ValueObjects.Contracts
{
    public interface IIPBlockerRepository
    {
        int IsAddressBlocked(IPAddress address);
        int IsAddressBlockedNoLog(IPAddress address, int siteID);
        int IsAddressBlocked(IPAddress address, AccessType accessType, int siteID);
        int IsAddressBlockedForGroupId(IPAddress address, int IPRangeGroupID);
        IPRange GetRange(int rangeID);
        List<IPRange> GetOverlappingRanges(IPAddress beginningAddress, IPAddress endingAddress);
        List<AccessSummary> GetSummariesForRange(int rangeID);
        List<WhitelistedIPRange> GetAllWhitelistedIPRanges();
        List<WhitelistedIPRange> GetAllWhitelistedIPRangesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount);
        List<IPRangeSummary> GetExpiringRangeSummariesPaged(DateTime beginDate, DateTime endDate, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount);
        List<IPRangeSummary> GetRangesByCountryPaged(CountryID countryID, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount);
        List<Country> GetAllCountries();
        List<IPRangeCountrySummary> GetCountrySummariesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount);
        void AddRangeNote(int rangeID, string createdBy, string note);
        void LogAccessAttempt(AccessType accessType, int rangeID);
        int SaveRange(IPRange range);
        void DeleteRange(int rangeID);
        void UpdateExpirationDate(int rangeID, DateTime? expireDate);

        List<IPRange> GetIpRangesByGroupID(int ipRangeGroupId);
    }
}
