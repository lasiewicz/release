﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.ValueObjects.Contracts
{
    [ServiceContract(Namespace = "http://spark")]
    public interface IIPBlockerService
    {
        [OperationContract(Name = "CheckAccess")]
        int CheckAccess(IPAddress address, AccessType accessType);

        [OperationContract(Name = "CheckAccessWithSite")]
        int CheckAccess(IPAddress address, AccessType accessType, int siteID);

        [OperationContract(Name = "CheckAccessByGroupID")]
        int CheckAccessByGroupID(string address, int IPRangeGroupID);

        [OperationContract]
        int CheckAccessNoLog(IPAddress address, int siteID);

        [OperationContract]
        IPRange GetRange(int rangeID);

        [OperationContract]
        List<IPRange> GetOverlappingRanges(IPAddress beginningAddress, IPAddress endingAddress);

        [OperationContract]
        List<AccessSummary> GetSummariesForRange(int rangeID);

        [OperationContract]
        List<WhitelistedIPRange> GetAllWhitelistedIPRanges();

        [OperationContract]
        List<WhitelistedIPRange> GetAllWhitelistedIPRangesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount);

        [OperationContract]
        List<IPRangeCountrySummary> GetCountrySummariesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount);

        [OperationContract]
        List<IPRangeSummary> GetRangesByCountryPaged(CountryID countryID, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount);

        [OperationContract]
        List<IPRangeSummary> GetExpiringRangeSummariesPaged(DateTime beginDate, DateTime endDate, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount);

        [OperationContract]
        void AddRangeNote(int rangeID, string createdBy, string note);

        [OperationContract(Name = "SaveRange")]
        int SaveRange(IPRange range);

        [OperationContract(Name="SaveRangeWithOverrides")]
        int SaveRange(IPRange range, List<int> rangesToDelete, List<int> rangesToKeep);

        [OperationContract]
        void DeleteRange(int rangeID);

        [OperationContract]
        List<Country> GetAllCountries();

        [OperationContract]
        void UpdateExpirationDate(int rangeID, DateTime? expireDate);

    }
}
