﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Constants;
using System.Runtime.Serialization;


namespace Spark.IPBlocker.ValueObjects
{
    [DataContract(Namespace = IPBlockerConstants.CONTRACT_NAMESPACE)]
    [Serializable]
    public class IPRangeSummary
    {
        [DataMember]
        public IPRange Range {get;set;}
        [DataMember]
        public List<AccessSummary> Summaries { get; set; }

        public IPRangeSummary(IPRange range)
        {
            Range = range;
            Summaries = new List<AccessSummary>();
        }

        public IPRangeSummary(IPRange range, List<AccessSummary> summaries)
        {
            Range = range;
            Summaries = summaries;
        }
    }
}
