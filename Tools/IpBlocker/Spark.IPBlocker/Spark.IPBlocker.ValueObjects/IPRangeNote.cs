﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Constants;
using System.Runtime.Serialization;

namespace Spark.IPBlocker.ValueObjects
{
    [DataContract(Namespace = IPBlockerConstants.CONTRACT_NAMESPACE)]
    [Serializable]
    public class IPRangeNote
    {
        [DataMember]
        public int ID { get; private set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; private set; }
        [DataMember]
        public string Note { get; set; }

        public IPRangeNote(int id, string createdBy, DateTime createdDate, string note)
        {
            ID = id;
            CreatedBy = createdBy;
            CreatedDate = createdDate;
            Note = note;
        }

    }
}
