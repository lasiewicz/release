﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.IPBlocker.ValueObjects
{
    public class IPRangeCountrySummary
    {
        public Country RangeCountry { get; set; }
        public int BlockCount { get; set; }
        public List<AccessSummary> Summaries { get; set; }

        public IPRangeCountrySummary() { }

        public IPRangeCountrySummary(Country rangeCountry, int blockCount, List<AccessSummary> summaries)
        {
            RangeCountry = rangeCountry;
            BlockCount = blockCount;
            Summaries = summaries;
        }
    }
}
