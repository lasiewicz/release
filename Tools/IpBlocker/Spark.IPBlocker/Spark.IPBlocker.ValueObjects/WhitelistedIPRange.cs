﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Constants;
using System.Runtime.Serialization;


namespace Spark.IPBlocker.ValueObjects
{
    [DataContract(Namespace = IPBlockerConstants.CONTRACT_NAMESPACE)]
    [Serializable]
    public class WhitelistedIPRange : IPRange
    {
        [DataMember]
        public string Name { get; set; }

        public WhitelistedIPRange() { }
        
        public WhitelistedIPRange(int id, IPAddress beginningAddress, IPAddress endingAddress, 
            string createdBy, DateTime createdDate, Country country, string name): base(id, beginningAddress, endingAddress, createdBy, createdDate, country)
        {
            Name = name;
        }
    }
}
