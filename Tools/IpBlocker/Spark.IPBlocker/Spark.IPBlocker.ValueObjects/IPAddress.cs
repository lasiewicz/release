﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Constants;
using System.Runtime.Serialization;


namespace Spark.IPBlocker.ValueObjects
{
    [DataContract(Namespace = IPBlockerConstants.CONTRACT_NAMESPACE)]
    [Serializable]
    public class IPAddress
    {
        [DataMember]
        public byte FirstOctet { get; set; }
        [DataMember]
        public byte SecondOctet { get; set; }
        [DataMember]
        public byte ThirdOctet { get; set; }
        [DataMember]
        public byte FourthOctet { get; set; }

        public IPAddress() { }
        public IPAddress(byte firstOctet, byte secondOctet, byte thirdOctet, byte fourthOctet)
        {
            FirstOctet = firstOctet;
            SecondOctet = secondOctet;
            ThirdOctet = thirdOctet;
            FourthOctet = fourthOctet;
        }

        public override string ToString()
        {
            return FirstOctet.ToString() + "." + SecondOctet.ToString() + "." + ThirdOctet.ToString() + "." + FourthOctet.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is IPAddress)) return false;
            return (obj as IPAddress).ToNumber() == this.ToNumber();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public int ToNumber()
        {
            return FirstOctet * 16777216 + SecondOctet * 65536 + ThirdOctet * 256 + FourthOctet;
        }

        public bool IsValid()
        {
            //return FourthOctet > 0;
            return true;
            //changed because of the need to have addresses that end in 0
        }

        public static bool operator >=(IPAddress address1, IPAddress address2)
        {
            return address1.ToNumber() >= address2.ToNumber();
        }

        public static bool operator > (IPAddress address1, IPAddress address2)
        {
            return address1.ToNumber() > address2.ToNumber();
        }

        public static bool operator <=(IPAddress address1, IPAddress address2)
        {
            return address1.ToNumber() <= address2.ToNumber();
        }

        public static bool operator < (IPAddress address1, IPAddress address2)
        {
            return address1.ToNumber() < address2.ToNumber();
        }

        public static IPAddress StringToIP(string addr)
        {
            IPAddress address = null;
            if (IsValidIP(addr))
            {
                addr = ConvertToIPV6ToIPV4(addr);

                string[] octets = addr.Split('.');
                address = new IPAddress(Convert.ToByte(octets[0]), Convert.ToByte(octets[1]), Convert.ToByte(octets[2]), Convert.ToByte(octets[3]));
            }

            return address;
        }

        /// <summary>
        ///     This is to convert any valid IPV6 address to 127.0.0.1.
        ///     This allows any IPV6 addresses to pass the blocker.
        /// </summary>
        /// <param name="addr"></param>
        /// <returns></returns>
        public static string ConvertToIPV6ToIPV4(string addr)
        {
            System.Net.IPAddress address;

            // Having passed the IsValidIP method, no need to check for error
            System.Net.IPAddress.TryParse(addr, out address);

            switch (address.AddressFamily)
            {
                case System.Net.Sockets.AddressFamily.InterNetworkV6:
                    addr = "127.0.0.1";
                    break;
            }
            return addr;
        }

        /// <summary>
        ///     Used old school RegEx before, modified to use the built in parser in System.Net.
        ///     This validates both IPV4 and 6 addresses.
        /// </summary>
        /// <param name="addr"></param>
        /// <returns></returns>
        public static bool IsValidIP(string addr)
        {
            System.Net.IPAddress address;
            bool isValid;
            if (!System.Net.IPAddress.TryParse(addr, out address)) return false;

            switch (address.AddressFamily)
            {
                case System.Net.Sockets.AddressFamily.InterNetwork:
                    isValid  = true;
                    break;
                case System.Net.Sockets.AddressFamily.InterNetworkV6:
                    isValid = true;
                    break;
                default:
                    isValid = false;
                    break;
            }

            return isValid;
        }

    }
}
