﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Constants;
using System.Runtime.Serialization;

namespace Spark.IPBlocker.ValueObjects
{
    [DataContract(Namespace = IPBlockerConstants.CONTRACT_NAMESPACE)]
    [Serializable]
    public class Country
    {
        [DataMember]
        public CountryID ID { get; private set; }
        [DataMember]
        public string Name { get; private set; }
        [DataMember]
        public string Abbreviation { get; private set; }

        public Country(CountryID id, string name, string abbreviation)
        {
            ID = id;
            Name = name;
            Abbreviation = abbreviation;
        }

        public Country(CountryID id, string name)
        {
            ID = id;
            Name = name;
        }

        public Country(CountryID id)
        {
            ID = id;
        }
    }
}
