﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.IPBlocker.ValueObjects.Exceptions
{
    public class OverlappingRangesNotHandledException : Exception
    {
        public List<IPRange> UnhandledRanges { get; private set; }

        public OverlappingRangesNotHandledException(List<IPRange> unhandledRanges)
        {
            UnhandledRanges = unhandledRanges;
        }
    }
}
