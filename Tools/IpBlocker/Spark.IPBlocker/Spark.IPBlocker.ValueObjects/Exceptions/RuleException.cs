﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.IPBlocker.ValueObjects.Exceptions
{
    public class RuleException: Exception 
    {
        public NameValueCollection Errors { get; private set; }

        public RuleException(string key, string value)
        {
            Errors = new NameValueCollection { { key, value } };
        }

        public RuleException(NameValueCollection errors)
        {
            Errors = errors;
        }
    }
}
