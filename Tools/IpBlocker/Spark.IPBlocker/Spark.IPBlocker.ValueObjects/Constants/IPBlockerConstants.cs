﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.IPBlocker.ValueObjects.Constants
{
    public sealed class IPBlockerConstants
    {
        private IPBlockerConstants()
        {

        }

        public const string CONTRACT_NAMESPACE = "http://spark.net/";
    }
}
