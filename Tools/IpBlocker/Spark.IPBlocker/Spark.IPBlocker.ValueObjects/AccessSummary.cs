﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Constants;
using System.Runtime.Serialization;

namespace Spark.IPBlocker.ValueObjects
{
    [DataContract(Namespace = IPBlockerConstants.CONTRACT_NAMESPACE)]
    [Serializable]
    public class AccessSummary
    {
        [DataMember]
        public AccessType AccessType { get; private set;}
        [DataMember]
        public int TotalAttempts { get; private set; }
        [DataMember]
        public int ThirtyDayAttempts { get; private set; }
        [DataMember]
        public int NinetyDayAttempts { get; private set; }
        [DataMember]
        public DateTime LastAccessDate { get; private set; }

        public AccessSummary(AccessType accessType, int totalAttempts, int thirtyDayAttempts, int ninetyDayAttempts, DateTime lastAccessDate)
        {
            AccessType = accessType;
            TotalAttempts = totalAttempts;
            ThirtyDayAttempts = thirtyDayAttempts;
            NinetyDayAttempts = ninetyDayAttempts;
            LastAccessDate = lastAccessDate;
        }
    }
}
