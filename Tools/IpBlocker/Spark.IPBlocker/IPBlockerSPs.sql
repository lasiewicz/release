CREATE FUNCTION [dbo].[IP_to_BigInt]
(
	@Octet1 tinyint,
	@Octet2 tinyint,
	@Octet3 tinyint,
	@Octet4 tinyint
)
RETURNS bigint
AS
BEGIN
DECLARE @IntIP bigint

SELECT @IntIP =  (@Octet1 * (256 * 256 * 256)) +
(@Octet2 * (256 * 256)) +
(@Octet3 * (256)) +
@Octet4

RETURN @IntIP

END

GO

CREATE PROCEDURE [dbo].[up_IPRangeList_Save]
	@IPRangeListID int,
	@BeginOctet1 tinyint,
	@BeginOctet2 tinyint,
	@BeginOctet3 tinyint,
	@BeginOctet4 tinyint,
	@EndOctet1 tinyint,
	@EndOctet2 tinyint,
	@EndOctet3 tinyint,
	@EndOctet4 tinyint,
	@CreatedBy varchar(25) = null,
	@CountryID int,
	@IPRangeTypeID int,
	@ExpireDate DateTime = null,	
	@Name nvarchar(25) = null
AS
BEGIN
	SET NOCOUNT ON;

	if exists (	SELECT * 
			FROM 	dbo.IPRangeList nolock
			WHERE	IPRangeListID = @IPRangeListID)
	begin
		UPDATE dbo.IPRangeList SET
			 BeginOctet1 = @BeginOctet1,
			 BeginOctet2 = @BeginOctet2,
			 BeginOctet3 = @BeginOctet3,
			 BeginOctet4 = @BeginOctet4,
			 EndOctet1 = @EndOctet1,
			 EndOctet2 = @EndOctet2,
			 EndOctet3 = @EndOctet3,
			 EndOctet4 = @EndOctet4,
			 BeginNumber = dbo.IP_to_BigInt(@BeginOctet1, @BeginOctet2, @BeginOctet3, @BeginOctet4),
			 EndNumber = dbo.IP_to_BigInt(@EndOctet1, @EndOctet2, @EndOctet3, @EndOctet4),
			 CountryID = @CountryID,
			 IPRangeTypeID = @IPRangeTypeID,
			 [ExpireDate] = @ExpireDate,
			 Name = @Name
		WHERE IPRangeListID = @IPRangeListID
	end
	else
		INSERT dbo.IPRangeList 
			(IPRangeListID, 
			BeginOctet1, 
			BeginOctet2, 
			BeginOctet3, 
			BeginOctet4, 
			EndOctet1, 
			EndOctet2,
			EndOctet3,
			EndOctet4,
			BeginNumber,
			EndNumber,
			CreatedBy,
			CreatedDate,
			CountryID,
			IPRangeTypeID,
			[ExpireDate],
			Name) 
			VALUES
			(@IPRangeListID, 
			@BeginOctet1, 
			@BeginOctet2, 
			@BeginOctet3, 
			@BeginOctet4, 
			@EndOctet1,
			@EndOctet2,
			@EndOctet3,
			@EndOctet4,
			dbo.IP_to_BigInt(@BeginOctet1, @BeginOctet2, @BeginOctet3, @BeginOctet4),
			dbo.IP_to_BigInt(@EndOctet1, @EndOctet2, @EndOctet3, @EndOctet4),
			@CreatedBy, 
			GETDATE(),
			@CountryID, 
			@IPRangeTypeID,
			@ExpireDate, 
			@Name)
	SET NOCOUNT OFF	
END	

GO	


