USE [mnKey1]
GO

/****** Object:  Table [dbo].[MemberID]    Script Date: 04/06/2010 11:29:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IPRangeListID](
	[PKID] [int] IDENTITY(1000,3) NOT NULL,
	[InputValue] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].IPRangeListID ADD  CONSTRAINT [DF_IPRangeListID_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO

CREATE TABLE [dbo].[IPRangeAccessLogID](
	[PKID] [int] IDENTITY(1000,3) NOT NULL,
	[InputValue] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[IPRangeAccessLogID] ADD  CONSTRAINT [DF_IPRangeAccessLogID_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO

USE [mnKey2]
GO

/****** Object:  Table [dbo].[MemberID]    Script Date: 04/06/2010 11:29:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IPRangeListID](
	[PKID] [int] IDENTITY(1001,3) NOT NULL,
	[InputValue] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[IPRangeListID] ADD  CONSTRAINT [DF_IPRangeListID_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO

CREATE TABLE [dbo].[IPRangeAccessLogID](
	[PKID] [int] IDENTITY(1001,3) NOT NULL,
	[InputValue] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[IPRangeAccessLogID] ADD  CONSTRAINT [DF_IPRangeAccessLogID_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO

USE [mnKey3]
GO

/****** Object:  Table [dbo].[MemberID]    Script Date: 04/06/2010 11:29:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IPRangeListID](
	[PKID] [int] IDENTITY(1002,3) NOT NULL,
	[InputValue] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[IPRangeListID] ADD  CONSTRAINT [DF_IPRangeListID_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO

CREATE TABLE [dbo].[IPRangeAccessLogID](
	[PKID] [int] IDENTITY(1002,3) NOT NULL,
	[InputValue] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[IPRangeAccessLogID] ADD  CONSTRAINT [DF_IPRangeAccessLogID_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO
