﻿using System;
using System.Collections.Generic;
using System.Text;
using Matchnet.Data.Hydra;

namespace Spark.IPBlocker.IISHost
{
    public class HydraWriterWrapper
    {
        public static readonly HydraWriterWrapper Instance = new HydraWriterWrapper();
        private static HydraWriter hydraWriter;
        private static bool isRunning = false;

        private HydraWriterWrapper()
        {
            hydraWriter = new HydraWriter(new string[] { "mnIPBlocker", "mnKey"});
            isRunning = false;
        }

        public void HydraWriterStart()
        {
            if (!isRunning)
            {
                hydraWriter.Start();
                isRunning = true;
            }
        }

        public void HydraWriterStop()
        {
            if (isRunning)
            {
                hydraWriter.Stop();
                isRunning = false;
            }
        }
    }
}
