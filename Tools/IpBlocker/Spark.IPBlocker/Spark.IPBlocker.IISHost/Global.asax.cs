﻿#region

using System;
using log4net;

#endregion

namespace Spark.IPBlocker.IISHost
{
    public class Global : System.Web.HttpApplication
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (Global));

        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            Log.Debug("Spark.IPBlocker.IISHost AppBegin: Configured logger");
            Log.Debug("Spark.IPBlocker.IISHost AppBegin: Starting Hydra");
            HydraWriterWrapper.Instance.HydraWriterStart();
            Log.Debug("Spark.IPBlocker.IISHost AppBegin: Hydra started");
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError().GetBaseException();

            Log.Error(string.Format("Spark.IPBlocker.IISHost AppError: {0}", ex.Message), ex);
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
            Log.Debug("Spark.IPBlocker.IISHost AppEnd: Stopping Hydra logger");
            HydraWriterWrapper.Instance.HydraWriterStop();
            Log.Debug("Spark.IPBlocker.IISHost AppEnd: Stopped Hydra logger");
        }
    }
}