﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Spark.IPBlocker.Engine.ServiceManagers;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Exceptions;
using Spark.IPBlocker.ValueObjects.Contracts;

namespace Spark.IPBlocker.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class IPBlockerService: IIPBlockerService
    {
        public int CheckAccess(IPAddress address, AccessType accessType)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.CheckAccess(address, accessType);
        }

        public int CheckAccess(IPAddress address, AccessType accessType, int siteID)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.CheckAccess(address, accessType, siteID);
        }

        public int CheckAccessNoLog(IPAddress address, int siteID)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.CheckAccessNoLog(address, siteID);
        }

        //JS-1289
        public int CheckAccessByGroupID(string address, int IPRangeGroupID)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.CheckAccessByGroupID(address, IPRangeGroupID);
        }

        public List<IPRange> GetIpRangesByGroupID(int IPRangeGroupID)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.GetIpRangesByGroupID(IPRangeGroupID);
        }
   
        public IPRange GetRange(int rangeID)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            IPRange range = ipblockerSM.GetRange(rangeID);
            return range;
        }

        public List<IPRange> GetOverlappingRanges(IPAddress beginningAddress, IPAddress endingAddress)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.GetOverlappingRanges(beginningAddress, endingAddress);
        }

        public List<AccessSummary> GetSummariesForRange(int rangeID)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.GetSummariesForRange(rangeID);
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRanges()
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.GetAllWhitelistedIPRanges();
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRangesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.GetAllWhitelistedIPRangesPaged(pageSize, pageNumber, sortField, descending, out totalRowCount);
        }

        public List<IPRangeCountrySummary> GetCountrySummariesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.GetCountrySummariesPaged(pageSize, pageNumber, sortField, descending, out totalRowCount);
        }

        public List<IPRangeSummary> GetRangesByCountryPaged(CountryID countryID, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.GetRangesByCountryPaged(countryID, pageSize, pageNumber, sortField, descending, out totalRowCount);
        }

        public List<IPRangeSummary> GetExpiringRangeSummariesPaged(DateTime beginDate, DateTime endDate, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.GetExpiringRangeSummariesPaged(beginDate, endDate, pageSize, pageNumber, sortField, descending, out totalRowCount);
        }

        public void AddRangeNote(int rangeID, string createdBy, string note)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            ipblockerSM.AddRangeNote(rangeID, createdBy, note);
        }

        public int SaveRange(IPRange range)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.SaveRange(range);
        }

        public int SaveRange(IPRange range, List<int> rangesToDelete, List<int> rangesToKeep)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.SaveRange(range, rangesToDelete, rangesToKeep);
        }
        
        public void DeleteRange(int rangeID)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            ipblockerSM.DeleteRange(rangeID);
        }

        public void UpdateExpirationDate(int rangeID, DateTime? expireDate)
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            ipblockerSM.UpdateExpirationDate(rangeID, expireDate);
        }

        public List<Country> GetAllCountries()
        {
            IPBlockerSM ipblockerSM = new IPBlockerSM();
            return ipblockerSM.GetAllCountries();
        }
    }
}
