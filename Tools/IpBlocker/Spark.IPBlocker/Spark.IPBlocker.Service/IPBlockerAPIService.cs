﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;
using Spark.IPBlocker.Engine.ServiceManagers;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Exceptions;
using Spark.IPBlocker.ValueObjects.Contracts;


namespace Spark.IPBlocker.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class IPBlockerAPIService : IIPBlockerAPIService
    {
        public Stream CheckAccess(string ipaddress, AccessType accessType, int siteID)
        {
            APISM apiSM = new APISM();
            string rawValue = apiSM.CheckAccess(ipaddress, accessType, siteID);
            byte[] resultBytes = Encoding.UTF8.GetBytes(rawValue);
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
            return new MemoryStream(resultBytes);
        }

        public Stream CheckAccessJSON(string ipaddress, AccessType accessType, int siteID)
        {
            APISM apiSM = new APISM();
            string rawValue = apiSM.CheckAccessJSON(ipaddress, accessType, siteID);
            byte[] resultBytes = Encoding.UTF8.GetBytes(rawValue);
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
            return new MemoryStream(resultBytes);
        }

        public Stream CheckAccessNoLog(string ipaddress, int siteID)
        {
            APISM apiSM = new APISM();
            string rawValue = apiSM.CheckAccessNoLog(ipaddress, siteID);
            byte[] resultBytes = Encoding.UTF8.GetBytes(rawValue);
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
            return new MemoryStream(resultBytes);
        }

        public Stream CheckAccessNoLogJSON(string ipaddress, int siteID)
        {
            APISM apiSM = new APISM();
            string rawValue = apiSM.CheckAccessNoLogJSON(ipaddress, siteID);
            byte[] resultBytes = Encoding.UTF8.GetBytes(rawValue);
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
            return new MemoryStream(resultBytes);
        }

        public Stream HealthCheck()
        {
            string MATCHNET_SERVER_DOWN = "Matchnet Server Down";
            string MATCHNET_SERVER_ENABLED = "Matchnet Server Enabled";
            byte[] resultBytes;

            HealthCheckService healthCheck = new HealthCheckService();
            bool isHealthy = healthCheck.Check();

            if (isHealthy)
            {
                resultBytes = Encoding.UTF8.GetBytes(MATCHNET_SERVER_ENABLED);
                WebOperationContext.Current.OutgoingResponse.Headers.Add("Health", MATCHNET_SERVER_ENABLED);
                WebOperationContext.Current.OutgoingResponse.StatusDescription = MATCHNET_SERVER_ENABLED;
            }
            else
            {
                resultBytes = Encoding.UTF8.GetBytes(MATCHNET_SERVER_DOWN);
                WebOperationContext.Current.OutgoingResponse.Headers.Add("Health", MATCHNET_SERVER_DOWN);
                WebOperationContext.Current.OutgoingResponse.StatusDescription = MATCHNET_SERVER_DOWN;
            }
            
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
            
            return new MemoryStream(resultBytes);
        }
    }

}
