﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Spark.IPBlocker.Service
{
    public class HealthCheckService
    {
        string MATCHNET_SERVER_DOWN = "Matchnet Server Down";
        string MATCHNET_SERVER_ENABLED = "Matchnet Server Enabled";


        public bool Check()
        {
            bool healthy = true;
            
            try
            {
                if (!isEnabled()) healthy = false;
                else
                {
                    WebRequest webRequest = WebRequest.Create("http://" + WebOperationContext.Current.IncomingRequest.Headers["Host"]);
                    webRequest.Timeout = 2000;
                    WebResponse webResponse = webRequest.GetResponse();
                }
            }
            catch (Exception ex)
            {
                healthy = false;
            }

            return healthy;
        }

        private bool isEnabled()
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "deploy.txt";

            bool fileExists = System.IO.File.Exists(filePath);
            
            if (!fileExists)
            {
                return true;
            }

            return false;
        }
    }
}
