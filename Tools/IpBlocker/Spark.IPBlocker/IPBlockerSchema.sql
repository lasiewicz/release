USE [mnActivityRecording]
GO
/****** Object:  Table [dbo].[IPRangeType]    Script Date: 04/06/2010 20:02:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IPRangeType](
	[IPRangeTypeID] [int] NOT NULL,
	[Name] [varchar](25) NOT NULL,
 CONSTRAINT [PK_IPRangeType] PRIMARY KEY CLUSTERED 
(
	[IPRangeTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IPRangeNote]    Script Date: 04/06/2010 20:02:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IPRangeNote](
	[IPRangeNote] [int] NOT NULL,
	[IPRangeListID] [int] NOT NULL,
	[CreatedBy] [varchar](25) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_IPRangeNote] PRIMARY KEY CLUSTERED 
(
	[IPRangeNote] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IPRangeList]    Script Date: 04/06/2010 20:02:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IPRangeList](
	[IPRangeListID] [int] NOT NULL,
	[BeginOctet1] [tinyint] NOT NULL,
	[BeginOctet2] [tinyint] NOT NULL,
	[BeginOctet3] [tinyint] NOT NULL,
	[BeginOctet4] [tinyint] NOT NULL,
	[EndOctet1] [tinyint] NOT NULL,
	[EndOctet2] [tinyint] NOT NULL,
	[EndOctet3] [tinyint] NOT NULL,
	[EndOctet4] [tinyint] NOT NULL,
	[BeginNumber] [bigint] NOT NULL,
	[EndNumber] [bigint] NOT NULL,
	[CreatedBy] [varchar](25) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CountryID] [int] NOT NULL,
	[ExpireDate] [datetime] NULL,
	[Name] [varchar](25) NULL,
	[IPRangeTypeID] [int] NOT NULL,
 CONSTRAINT [PK_IPRangeList] PRIMARY KEY CLUSTERED 
(
	[IPRangeListID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IPAccessType]    Script Date: 04/06/2010 20:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IPAccessType](
	[IPAccessTypeID] [int] NOT NULL,
	[Name] [varchar](25) NOT NULL,
 CONSTRAINT [PK_IPAccessType] PRIMARY KEY CLUSTERED 
(
	[IPAccessTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IPAccessLog]    Script Date: 04/06/2010 20:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IPAccessLog](
	[IPAccessLogID] [int] NOT NULL,
	[IPAccessTypeID] [int] NOT NULL,
	[IPRangeListID] [int] NOT NULL,
	[LogDate] [datetime] NOT NULL,
 CONSTRAINT [PK_IPAccessLog] PRIMARY KEY CLUSTERED 
(
	[IPAccessLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[IP_to_BigInt]    Script Date: 04/06/2010 20:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[IP_to_BigInt]
(
	@Octet1 tinyint,
	@Octet2 tinyint,
	@Octet3 tinyint,
	@Octet4 tinyint
)
RETURNS bigint
AS
BEGIN
DECLARE @IntIP bigint

SELECT @IntIP =  (@Octet1 * (256 * 256 * 256)) +
(@Octet2 * (256 * 256)) +
(@Octet3 * (256)) +
@Octet4

RETURN @IntIP

END
GO
/****** Object:  Table [dbo].[Country]    Script Date: 04/06/2010 20:02:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[CountryID] [int] NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[up_IPRangeList_Save]    Script Date: 04/06/2010 20:02:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[up_IPRangeList_Save]
	@IPRangeListID int,
	@BeginOctet1 tinyint,
	@BeginOctet2 tinyint,
	@BeginOctet3 tinyint,
	@BeginOctet4 tinyint,
	@EndOctet1 tinyint,
	@EndOctet2 tinyint,
	@EndOctet3 tinyint,
	@EndOctet4 tinyint,
	@CreatedBy varchar(25) = null,
	@CountryID int,
	@IPRangeTypeID int,
	@ExpireDate DateTime = null,	
	@Name nvarchar(25) = null
AS
BEGIN
	SET NOCOUNT ON;

	if exists (	SELECT * 
			FROM 	dbo.IPRangeList nolock
			WHERE	IPRangeListID = @IPRangeListID)
	begin
		UPDATE dbo.IPRangeList SET
			 BeginOctet1 = @BeginOctet1,
			 BeginOctet2 = @BeginOctet2,
			 BeginOctet3 = @BeginOctet3,
			 BeginOctet4 = @BeginOctet4,
			 EndOctet1 = @EndOctet1,
			 EndOctet2 = @EndOctet2,
			 EndOctet3 = @EndOctet3,
			 EndOctet4 = @EndOctet4,
			 BeginNumber = dbo.IP_to_BigInt(@BeginOctet1, @BeginOctet2, @BeginOctet3, @BeginOctet4),
			 EndNumber = dbo.IP_to_BigInt(@EndOctet1, @EndOctet2, @EndOctet3, @EndOctet4),
			 CountryID = @CountryID,
			 IPRangeTypeID = @IPRangeTypeID,
			 [ExpireDate] = @ExpireDate,
			 Name = @Name
		WHERE IPRangeListID = @IPRangeListID
	end
	else
		INSERT dbo.IPRangeList 
			(IPRangeListID, 
			BeginOctet1, 
			BeginOctet2, 
			BeginOctet3, 
			BeginOctet4, 
			EndOctet1, 
			EndOctet2,
			EndOctet3,
			EndOctet4,
			BeginNumber,
			EndNumber,
			CreatedBy,
			CreatedDate,
			CountryID,
			IPRangeTypeID,
			[ExpireDate],
			Name) 
			VALUES
			(@IPRangeListID, 
			@BeginOctet1, 
			@BeginOctet2, 
			@BeginOctet3, 
			@BeginOctet4, 
			@EndOctet1,
			@EndOctet2,
			@EndOctet3,
			@EndOctet4,
			dbo.IP_to_BigInt(@BeginOctet1, @BeginOctet2, @BeginOctet3, @BeginOctet4),
			dbo.IP_to_BigInt(@EndOctet1, @EndOctet2, @EndOctet3, @EndOctet4),
			@CreatedBy, 
			GETDATE(),
			@CountryID, 
			@IPRangeTypeID,
			@ExpireDate, 
			@Name)
	SET NOCOUNT OFF	
END
GO
