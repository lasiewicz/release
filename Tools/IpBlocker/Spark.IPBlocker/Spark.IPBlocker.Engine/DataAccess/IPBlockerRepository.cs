﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using log4net;
using Matchnet.Exceptions;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Contracts;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Matchnet.Data;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.IPBlocker.Engine.DataAccess
{
    public class IPBlockerRepository : IIPBlockerRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(IPBlockerRepository));
        int _DBCallTimeoutThreshold = 0;

        public IPBlockerRepository()
        {

            _DBCallTimeoutThreshold = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IP_BLOCKER_LOG_TIMEOUT_THRESH"));
        }

        public int IsAddressBlocked(IPAddress address)
        {
            SqlDataReader dataReader = null;
            int returnValue = 0;
            
            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.IsAddressBlocked({0})", address.ToString()));
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                Command command = new Command("mnIPBlocker", "dbo.[up_IPRange_Is_Address_Blocked]", 0);
                command.AddParameter("@Octet1", SqlDbType.TinyInt, ParameterDirection.Input, address.FirstOctet);
                command.AddParameter("@Octet2", SqlDbType.TinyInt, ParameterDirection.Input, address.SecondOctet);
                command.AddParameter("@Octet3", SqlDbType.TinyInt, ParameterDirection.Input, address.ThirdOctet);
                command.AddParameter("@Octet4", SqlDbType.TinyInt, ParameterDirection.Input, address.FourthOctet);
                command.AddParameter("@Octet4", SqlDbType.TinyInt, ParameterDirection.Input, address.FourthOctet);
                command.AddParameter("@Log", SqlDbType.Int, ParameterDirection.Input, 1);
                
                
                dataReader = Client.Instance.ExecuteReader(command);
                if(dataReader.HasRows && dataReader.Read())
                {
                    returnValue = dataReader.GetInt32(0);
                }

                stopwatch.Stop();
                if (stopwatch.ElapsedMilliseconds > _DBCallTimeoutThreshold)
                {
                    LogDBTimeout(address, stopwatch.ElapsedMilliseconds);
                }

                return returnValue;
                
            }
            catch (Exception ex)
            {
                BLException exception = new BLException("Error in IPBlockerRepository.IsAddressBlocked. Address: " + address.ToString(), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed) dataReader.Close();
                Log.Debug(string.Format("Leaving IPBlockerRepository.IsAddressBlocked({0})", address.ToString()));
            }
        }

        public int IsAddressBlocked(IPAddress address, AccessType accessType, int siteID)
        {
            SqlDataReader dataReader = null;
            int returnValue = 0;

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.IsAddressBlocked({0})", address.ToString()));
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                Command command = new Command("mnIPBlocker", "dbo.[up_IPRange_Is_Address_Blocked]", 0);
                command.AddParameter("@Octet1", SqlDbType.TinyInt, ParameterDirection.Input, address.FirstOctet);
                command.AddParameter("@Octet2", SqlDbType.TinyInt, ParameterDirection.Input, address.SecondOctet);
                command.AddParameter("@Octet3", SqlDbType.TinyInt, ParameterDirection.Input, address.ThirdOctet);
                command.AddParameter("@Octet4", SqlDbType.TinyInt, ParameterDirection.Input, address.FourthOctet);
                command.AddParameter("@IPAccessTypeID", SqlDbType.Int, ParameterDirection.Input, (int)accessType);
                command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                command.AddParameter("@Log", SqlDbType.Int, ParameterDirection.Input, 1);

                dataReader = Client.Instance.ExecuteReader(command);
                if (dataReader.HasRows && dataReader.Read())
                {
                    returnValue = dataReader.GetInt32(0);
                }

                stopwatch.Stop();
                if (stopwatch.ElapsedMilliseconds > _DBCallTimeoutThreshold)
                {
                    LogDBTimeout(address, stopwatch.ElapsedMilliseconds);
                }

                return returnValue;

            }
            catch (Exception ex)
            {
                BLException exception = new BLException("Error in IPBlockerRepository.IsAddressBlocked. Address: " + address.ToString() + " AccessType: " + accessType.ToString() + " SiteID: " + siteID.ToString() , ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed) dataReader.Close();
                Log.Debug(string.Format("Leaving IPBlockerRepository.IsAddressBlocked({0})", address.ToString()));
            }
        }

        public int IsAddressBlockedNoLog(IPAddress address, int siteID)
        {
            SqlDataReader dataReader = null;
            int returnValue = 0;

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.IsAddressBlockedNoLog({0})", address.ToString()));
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                Command command = new Command("mnIPBlocker", "dbo.[up_IPRange_Is_Address_Blocked]", 0);
                command.AddParameter("@Octet1", SqlDbType.TinyInt, ParameterDirection.Input, address.FirstOctet);
                command.AddParameter("@Octet2", SqlDbType.TinyInt, ParameterDirection.Input, address.SecondOctet);
                command.AddParameter("@Octet3", SqlDbType.TinyInt, ParameterDirection.Input, address.ThirdOctet);
                command.AddParameter("@Octet4", SqlDbType.TinyInt, ParameterDirection.Input, address.FourthOctet);
                command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                command.AddParameter("@Log", SqlDbType.Int, ParameterDirection.Input, 0);

                dataReader = Client.Instance.ExecuteReader(command);
                if (dataReader.HasRows && dataReader.Read())
                {
                    returnValue = dataReader.GetInt32(0);
                }

                stopwatch.Stop();
                if (stopwatch.ElapsedMilliseconds > _DBCallTimeoutThreshold)
                {
                    LogDBTimeout(address, stopwatch.ElapsedMilliseconds);
                }

                return returnValue;

            }
            catch (Exception ex)
            {
                BLException exception = new BLException("Error in IPBlockerRepository.IsAddressBlockedNoLog. Address: " + address.ToString(), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed) dataReader.Close();
                Log.Debug(string.Format("Leaving IPBlockerRepository.IsAddressBlockedNoLog({0})", address.ToString()));
            }
        }
        
        public int IsAddressBlockedForGroupId(IPAddress address, int IPRangeGroupID)
        {
            SqlDataReader dataReader = null;
            int returnValue = 0;

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.IsAddressBlocked({0})", address.ToString()));
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                Command command = new Command("mnIPBlocker", "dbo.[up_IPRangeGroupedList_Is_Address_Blocked]", 0);
                command.AddParameter("@Octet1", SqlDbType.TinyInt, ParameterDirection.Input, address.FirstOctet);
                command.AddParameter("@Octet2", SqlDbType.TinyInt, ParameterDirection.Input, address.SecondOctet);
                command.AddParameter("@Octet3", SqlDbType.TinyInt, ParameterDirection.Input, address.ThirdOctet);
                command.AddParameter("@Octet4", SqlDbType.TinyInt, ParameterDirection.Input, address.FourthOctet);
                command.AddParameter("@IPRangeGroupID", SqlDbType.Int, ParameterDirection.Input, (int)IPRangeGroupID);
                //command.AddParameter("@Log", SqlDbType.Int, ParameterDirection.Input, 1);

                dataReader = Client.Instance.ExecuteReader(command);
                if (dataReader.HasRows && dataReader.Read())
                {
                    returnValue = dataReader.GetInt32(0);
                }

                stopwatch.Stop();
                if (stopwatch.ElapsedMilliseconds > _DBCallTimeoutThreshold)
                {
                    LogDBTimeout(address, stopwatch.ElapsedMilliseconds);
                }

                return returnValue;

            }
            catch (Exception ex)
            {
                BLException exception = new BLException("Error in IPBlockerRepository.IsAddressBlocked. Address: " + address.ToString() + " IPRangeGroupID: " + IPRangeGroupID, ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed) dataReader.Close();
                Log.Debug(string.Format("Leaving IPBlockerRepository.IsAddressBlocked({0})", address.ToString()));
            }
        }

        public IPRange GetRange(int rangeID)
        {
            IPRange range = null;

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.GetRange({0})", rangeID.ToString()));

                Command command = new Command("mnIPBlocker", "up_IPRangeList_Get_Range", rangeID);
                command.AddParameter("@IPRangeListID", SqlDbType.Int, ParameterDirection.Input, rangeID);

                DataSet ds = Client.Instance.ExecuteDataSet(command);

                DataTable IPRangeTable = ds.Tables[0];
                DataTable IPRangeNoteTable = ds.Tables[1];
                IPRangeTable.ChildRelations.Add(
                    "fk_IPRangeList_IPRangeNote",
                    new DataColumn[] { IPRangeTable.Columns["IPRangeListID"] },
                    new DataColumn[] { IPRangeNoteTable.Columns["IPRangeListID"] }
                    );

                if (IPRangeTable != null && IPRangeTable.Rows.Count == 1)
                {
                    range = dataRowToIPRange(IPRangeTable.Rows[0]);

                    DataRow[] notesRows = IPRangeTable.Rows[0].GetChildRows("fk_IPRangeList_IPRangeNote");
                    if (notesRows != null && notesRows.Count() > 0)
                    {
                        foreach (DataRow notesRow in notesRows)
                        {
                            range.Notes.Add(dataRowToIPRangeNote(notesRow));
                        }
                    }
                }

                return range;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException("Error in IPBlockerRepository.GetRange. RangeID: " + rangeID.ToString(), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.GetRange");
            }
           
        }

        public List<IPRange> GetOverlappingRanges(IPAddress beginningAddress, IPAddress endingAddress)
        {
            List<IPRange> overlappingRanges = null;

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.GetOverlappingRanges({0}, {1})", beginningAddress.ToString(), endingAddress.ToString()));
                
                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Get_Overlapping_Ranges", 0);
                command.AddParameter("@BeginOctet1", SqlDbType.TinyInt, ParameterDirection.Input, beginningAddress.FirstOctet);
                command.AddParameter("@BeginOctet2", SqlDbType.TinyInt, ParameterDirection.Input, beginningAddress.SecondOctet);
                command.AddParameter("@BeginOctet3", SqlDbType.TinyInt, ParameterDirection.Input, beginningAddress.ThirdOctet);
                command.AddParameter("@BeginOctet4", SqlDbType.TinyInt, ParameterDirection.Input, beginningAddress.FourthOctet);
                command.AddParameter("@EndOctet1", SqlDbType.TinyInt, ParameterDirection.Input, endingAddress.FirstOctet);
                command.AddParameter("@EndOctet2", SqlDbType.TinyInt, ParameterDirection.Input, endingAddress.SecondOctet);
                command.AddParameter("@EndOctet3", SqlDbType.TinyInt, ParameterDirection.Input, endingAddress.ThirdOctet);
                command.AddParameter("@EndOctet4", SqlDbType.TinyInt, ParameterDirection.Input, endingAddress.FourthOctet);

                DataSet ds = Client.Instance.ExecuteDataSet(command);

                if(ds.Tables[0] != null)
                {
                    overlappingRanges = new List<IPRange>();
                    foreach(DataRow row in ds.Tables[0].Rows)
                    {
                        overlappingRanges.Add(dataRowToIPRange(row));
                    }
                }

                return overlappingRanges;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException("Error in IPBlockerRepository.GetOverlappingRanges. BeginningAddress: " + beginningAddress.ToString() + " EndingAddress: " + endingAddress.ToString(), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.GetOverlappingRanges");
            }
            
        }

        public List<IPRangeSummary> GetRangesByCountryPaged(CountryID countryID, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            SqlDataReader dataReader = null;
            List<IPRangeSummary> summaries = null;
            totalRowCount = 0;
            string actualSortField;

            switch (sortField)
            {
                case "range":
                    actualSortField = "beginNumber";
                    break;
                default:
                    actualSortField = sortField;
                    break;
            }

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.GetRangesByCountryPaged({0},{1},{2},{3},{4})", countryID.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()));

                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Get_Ranges_By_Country_Paged", 0);
                command.AddParameter("@CountryID", SqlDbType.Int, ParameterDirection.Input, (int)countryID);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
                command.AddParameter("@SortField", SqlDbType.VarChar, ParameterDirection.Input, actualSortField);
                command.AddParameter("@Descending", SqlDbType.Bit, ParameterDirection.Input, descending);

                dataReader = Client.Instance.ExecuteReader(command);

                if (!dataReader.IsClosed)
                {
                    summaries = new List<IPRangeSummary>();
                    while (dataReader.Read())
                    {
                        totalRowCount = Convert.ToInt32(dataReader["TotalRows"].ToString());
                        summaries.Add(datareaderToRangeSummaryByCountry(dataReader));
                    }
                }

                return summaries;

            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerRepository.GetRangesByCountryPaged({0},{1},{2},{3},{4})",countryID.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed) dataReader.Close();
                Log.Debug("Leaving IPBlockerRepository.GetRangesByCountryPaged");
            }
        }

        public List<AccessSummary> GetSummariesForRange(int rangeID)
        {
            SqlDataReader dataReader = null;
            List<AccessSummary> summaries = null;
            
            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.GetSummariesForRange({0})", rangeID.ToString()));
                
                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Get_Summaries", 0);
                command.AddParameter("@RangeListID", SqlDbType.Int, ParameterDirection.Input, rangeID);

                dataReader = Client.Instance.ExecuteReader(command);
                if (dataReader.HasRows)
                {
                    summaries = new List<AccessSummary>();
                    while (dataReader.Read())
                    {
                        summaries.Add(dataReaderToAccessSummary(dataReader));
                    }
                }
                
                return summaries;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerRepository.GetSummariesForRange({0})", rangeID.ToString()), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed) dataReader.Close();
                Log.Debug("Leaving IPBlockerRepository.GetSummariesForRange");
            }
        }


        public List<WhitelistedIPRange> GetAllWhitelistedIPRanges()
        {
            List<WhitelistedIPRange> whitelistedRanges = null;

            try
            {
                Log.Debug("Entering in IPBlockerRepository.GetAllWhitelistedIPRanges");
                
                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Get_Whitelisted_Ranges", 0);
                DataSet ds = Client.Instance.ExecuteDataSet(command);

                if (ds.Tables[0] != null)
                {
                    whitelistedRanges = new List<WhitelistedIPRange>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        whitelistedRanges.Add(dataRowToIPRange(row) as WhitelistedIPRange);
                    }
                }
                
                return whitelistedRanges;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException("Error in IPBlockerRepository.GetAllWhitelistedIPRanges", ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.GetAllWhitelistedIPRanges");
            }
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRangesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            string actualSortField;
            List<WhitelistedIPRange> whitelistedRanges = null;
            totalRowCount = 0;

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.GetAllWhitelistedIPRangesPaged({0},{1},{2},{3})", pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()));
                
                switch (sortField.ToLower())
                {
                    case "name":
                        actualSortField = "IPRangeList.Name";
                        break;
                    case "iprange":
                        actualSortField = "BeginNumber";
                        break;
                    case "country":
                        actualSortField = "Country.Name";
                        break;
                    case "createdby":
                        actualSortField = "CreatedBy";
                        break;
                    case "createddate":
                        actualSortField = "CreatedDate";
                        break;
                    default:
                        actualSortField = string.Empty;
                        break;
                }

                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Get_Whitelisted_Ranges_Paged", 0);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
                command.AddParameter("@SortField", SqlDbType.VarChar, ParameterDirection.Input, actualSortField);
                command.AddParameter("@Descending", SqlDbType.Bit, ParameterDirection.Input, descending);
  

                DataSet ds = Client.Instance.ExecuteDataSet(command);

                if (ds.Tables[0] != null)
                {
                    whitelistedRanges = new List<WhitelistedIPRange>();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        totalRowCount = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalRows"].ToString());
                    }
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        whitelistedRanges.Add(dataRowToIPRange(row) as WhitelistedIPRange);
                    }
                }

                return whitelistedRanges;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerRepository.GetAllWhitelistedIPRangesPaged({0},{1},{2},{3})", pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.GetAllWhitelistedIPRangesPaged");
            }
        }

        public List<IPRangeSummary> GetExpiringRangeSummariesPaged(DateTime beginDate, DateTime endDate, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            List<IPRangeSummary> summaries = null;
            totalRowCount = 0;
            string actualSortField;

            switch (sortField)
            {
                case "range":
                    actualSortField = "beginNumber";
                    break;
                default:
                    actualSortField = sortField;
                    break;
            }

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.GetExpiringRangeSummariesPaged({0},{1},{2},{3},{4},{5})", beginDate.ToString(), endDate.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()));
                
                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Get_Expiring_Ranges_Paged", 0);
                command.AddParameter("@BeginDate", SqlDbType.DateTime, ParameterDirection.Input, beginDate);
                command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
                command.AddParameter("@SortField", SqlDbType.VarChar, ParameterDirection.Input, actualSortField);
                command.AddParameter("@Descending", SqlDbType.Bit, ParameterDirection.Input, descending);
                DataSet ds = Client.Instance.ExecuteDataSet(command);

                if (ds.Tables[0] != null)
                {
                    summaries = new List<IPRangeSummary>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        totalRowCount = Convert.ToInt32(row["TotalRows"]);
                        summaries.Add(dataRowToIPRangeSummary(row) as IPRangeSummary);
                    }
                }

                return summaries;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerRepository.GetExpiringRangeSummariesPaged({0},{1},{2},{3},{4},{5})", beginDate.ToString(), endDate.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.GetExpiringRangeSummariesPaged");
            }
        }



        public void AddRangeNote(int rangeID, string createdBy, string note)
        {
            Int32 IPRangeNoteID = 0;
            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.AddRangeNote({0},{1},{2})", rangeID.ToString(), createdBy, note));

                IPRangeNoteID = KeySA.Instance.GetKey("IPRangeNoteID");

                Command command = new Command("mnIPBlocker", "dbo.up_IPRangeList_Add_Note", IPRangeNoteID);
                command.AddParameter("@IPRangeNoteID", SqlDbType.Int, ParameterDirection.Input, IPRangeNoteID);
                command.AddParameter("@IPRangeListID", SqlDbType.Int, ParameterDirection.Input, rangeID);
                command.AddParameter("@CreatedBy", SqlDbType.VarChar, ParameterDirection.Input, createdBy);
                command.AddParameter("@Note", SqlDbType.VarChar, ParameterDirection.Input, note);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                var exception = new BLException(string.Format("Error in IPBlockerRepository.AddRangeNote({0},{1},{2})", rangeID.ToString(), createdBy, note), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.AddRangeNote");
            }
        }

        public void LogAccessAttempt(AccessType accessType, int rangeID)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.LogAccessAttempt({0},{1})", accessType.ToString(), rangeID.ToString()));

                int IPAccessLogID = KeySA.Instance.GetKey("IPRangeAccessLogID");
                
                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Log_Access_Attempt", 0);
                command.AddParameter("@IPAccessLogID", SqlDbType.Int, ParameterDirection.Input, IPAccessLogID);
                command.AddParameter("@IPAccessTypeID", SqlDbType.Int, ParameterDirection.Input, (int)accessType);
                command.AddParameter("@IPRangeListID", SqlDbType.Int, ParameterDirection.Input, rangeID);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerRepository.LogAccessAttempt({0},{1})", accessType.ToString(), rangeID.ToString()), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.LogAccessAttempt");
            }
        }

        public int SaveRange(IPRange range)
        {
            Int32 IPRangeListID = range.ID;
            
            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.SaveRange({0},{1})", range.BeginningAddress.ToString(), range.EndingAddress.ToString()));
                
                RangeType rangeType = range is BlacklistedIPRange ? RangeType.BlackList : RangeType.WhiteList;

                if (range.ID <= 0)
                {
                    IPRangeListID = KeySA.Instance.GetKey("IPRangeListID");
                }
                else
                {
                    IPRangeListID = range.ID;
                }

                Command command = new Command("mnIPBlocker", "dbo.up_IPRangeList_Save", IPRangeListID);
                command.AddParameter("@IPRangeListID", SqlDbType.Int, ParameterDirection.Input, IPRangeListID);
                command.AddParameter("@BeginOctet1", SqlDbType.TinyInt, ParameterDirection.Input, range.BeginningAddress.FirstOctet);
                command.AddParameter("@BeginOctet2", SqlDbType.TinyInt, ParameterDirection.Input, range.BeginningAddress.SecondOctet);
                command.AddParameter("@BeginOctet3", SqlDbType.TinyInt, ParameterDirection.Input, range.BeginningAddress.ThirdOctet);
                command.AddParameter("@BeginOctet4", SqlDbType.TinyInt, ParameterDirection.Input, range.BeginningAddress.FourthOctet);
                command.AddParameter("@EndOctet1", SqlDbType.TinyInt, ParameterDirection.Input, range.EndingAddress.FirstOctet);
                command.AddParameter("@EndOctet2", SqlDbType.TinyInt, ParameterDirection.Input, range.EndingAddress.SecondOctet);
                command.AddParameter("@EndOctet3", SqlDbType.TinyInt, ParameterDirection.Input, range.EndingAddress.ThirdOctet);
                command.AddParameter("@EndOctet4", SqlDbType.TinyInt, ParameterDirection.Input, range.EndingAddress.FourthOctet);
                command.AddParameter("@CreatedBy", SqlDbType.VarChar, ParameterDirection.Input, range.CreatedBy);
                command.AddParameter("@CountryID", SqlDbType.Int, ParameterDirection.Input, (int)range.Country.ID);
                command.AddParameter("@IPRangeTypeID", SqlDbType.Int, ParameterDirection.Input, (int)rangeType);

                if (rangeType == RangeType.BlackList)
                {
                    command.AddParameter("@ExpireDate", SqlDbType.DateTime, ParameterDirection.Input, (range as BlacklistedIPRange).ExpireDate);
                }
                else
                {
                    command.AddParameter("@Name", SqlDbType.VarChar, ParameterDirection.Input, (range as WhitelistedIPRange).Name);
                }

                Client.Instance.ExecuteAsyncWrite(command);

                return IPRangeListID;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerRepository.SaveRange({0},{1})", range.BeginningAddress.ToString(), range.EndingAddress.ToString()), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.SaveRange");
            }
        }

        public void DeleteRange(int rangeID)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.DeleteRange({0})", rangeID.ToString()));
                
                Command command = new Command("mnIPBlocker", "dbo.up_IPRangeList_Delete", rangeID);
                command.AddParameter("@IPRangeListID", SqlDbType.Int, ParameterDirection.Input, rangeID);
                
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerRepository.DeleteRange({0})", rangeID.ToString()), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.DeleteRange");
            }
        }

        public void UpdateExpirationDate(int rangeID, DateTime? expireDate)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.UpdateExpirationDate({0},{1})", rangeID.ToString(), expireDate.HasValue ? expireDate.Value.ToString() : string.Empty));
                
                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Update_Expiration_Date", rangeID);
                command.AddParameter("@IPRangeListID", SqlDbType.Int, ParameterDirection.Input, rangeID);
                command.AddParameter("@ExpireDate", SqlDbType.DateTime, ParameterDirection.Input, expireDate);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerRepository.UpdateExpirationDate({0}, {1})", rangeID.ToString(), expireDate.HasValue ? expireDate.Value.ToString() : string.Empty), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.UpdateExpirationDate");
            }
        }

        public List<Country> GetAllCountries()
        {
            SqlDataReader dataReader = null;
            List<Country> countries = null;

            try
            {
                Log.Debug("Entering IPBlockerRepository.GetAllCountries()");
                
                Command command = new Command("mnIPBlocker", "dbo.up_Get_All_Countries", 0);
                
                dataReader = Client.Instance.ExecuteReader(command);
                if (dataReader.HasRows)
                {
                    countries = new List<Country>();
                    while (dataReader.Read())
                    {
                        countries.Add(dataReaderToCountry(dataReader));
                    }
                }

                return countries;

            }
            catch (Exception ex)
            {
                BLException exception = new BLException("Error in IPBlockerRepository.GetAllCountries()", ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed) dataReader.Close();
                Log.Debug("Leaving IPBlockerRepository.GetAllCountries");
            }
        }

        public List<IPRangeCountrySummary> GetCountrySummariesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            SqlDataReader dataReader = null;
            List<IPRangeCountrySummary> summaries = null;
            totalRowCount = 0;

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.GetCountrySummariesPaged({0},{1},{2},{3})", pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()));
                
                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Get_Country_Summary_Paged", 0);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
                command.AddParameter("@SortField", SqlDbType.VarChar, ParameterDirection.Input, sortField);
                command.AddParameter("@Descending", SqlDbType.Bit, ParameterDirection.Input, descending);

                dataReader = Client.Instance.ExecuteReader(command);

                if (!dataReader.IsClosed)
                {
                    summaries = new List<IPRangeCountrySummary>();
                    while (dataReader.Read())
                    {
                        totalRowCount = Convert.ToInt32(dataReader["TotalRows"].ToString());
                        summaries.Add(dataReaderToCountrySummary(dataReader));
                    }
                }

                return summaries;

            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerRepository.GetCountrySummariesPaged({0},{1},{2},{3})", pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed) dataReader.Close();
                Log.Debug("Leaving IPBlockerRepository.GetCountrySummariesPaged");
            }
        }

        private AccessSummary dataReaderToAccessSummary(SqlDataReader dataReader)
        {
            int totalCount = Convert.ToInt32(dataReader["TotalAttempts"]);
            int thirtyDayCount = Convert.ToInt32(dataReader["ThirtyDayAttempts"]);
            int ninetyDayCount = Convert.ToInt32(dataReader["NinetyDayAttempts"]);
            AccessType IPAccessTypeID = (AccessType)Convert.ToInt32(dataReader["IPAccessTypeID"]);
            DateTime lastAccessDate = dataReader["LastAttemptDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dataReader["LastAttemptDate"]);

            return new AccessSummary(IPAccessTypeID, totalCount, thirtyDayCount, ninetyDayCount, lastAccessDate);
        }

        private Country dataReaderToCountry(SqlDataReader dataReader)
        {
            CountryID id = (CountryID) Convert.ToInt32(dataReader["CountryID"]);
            string name = Convert.ToString(dataReader["Name"]);
            string abbreviation = Convert.ToString(dataReader["Abbreviation"]);

            return new Country(id, name, abbreviation);
        }

        private IPRange dataRowToIPRange(DataRow row)
        {
            int IPRangeListID = Convert.ToInt32(row["IPRangeListID"]);
            byte beginOctet1 = Convert.ToByte(row["BeginOctet1"]);
            byte beginOctet2 = Convert.ToByte(row["BeginOctet2"]);
            byte beginOctet3 = Convert.ToByte(row["BeginOctet3"]);
            byte beginOctet4 = Convert.ToByte(row["BeginOctet4"]);
            IPAddress beginningAddress = new IPAddress(beginOctet1,beginOctet2,beginOctet3,beginOctet4);
            byte endOctet1 = Convert.ToByte(row["EndOctet1"]);
            byte endOctet2 = Convert.ToByte(row["EndOctet2"]);
            byte endOctet3 = Convert.ToByte(row["EndOctet3"]);
            byte endOctet4 = Convert.ToByte(row["EndOctet4"]);
            IPAddress endingAddress = new IPAddress(endOctet1,endOctet2,endOctet3,endOctet4);
            string createdBy = Convert.ToString(row["CreatedBy"]);
            DateTime createdDate = Convert.ToDateTime(row["CreatedDate"]);
            CountryID countryID = (CountryID)Convert.ToInt32(row["CountryID"]);
            string countryName = Convert.ToString(row["CountryName"]);
            Country country = new Country(countryID, countryName);
            RangeType IPRangeTypeID = (RangeType)Convert.ToInt32(row["IPRangeTypeID"]);
            DateTime? expireDate = null;
            string rangeName = string.Empty;

            if(!Convert.IsDBNull(row["ExpireDate"])) expireDate = Convert.ToDateTime(row["ExpireDate"]);
            if (!Convert.IsDBNull(row["IPRangeName"])) rangeName = Convert.ToString(row["IPRangeName"]);

            if (IPRangeTypeID == RangeType.BlackList)
            {
                return new BlacklistedIPRange(IPRangeListID, beginningAddress, endingAddress, createdBy, createdDate, country, expireDate);
            }
            else
            {
                return new WhitelistedIPRange(IPRangeListID, beginningAddress, endingAddress, createdBy, createdDate, country, rangeName);
            }

        }

        private IPRangeSummary dataRowToIPRangeSummary(DataRow row)
        {
            int IPRangeListID = Convert.ToInt32(row["IPRangeListID"]);
            byte beginOctet1 = Convert.ToByte(row["BeginOctet1"]);
            byte beginOctet2 = Convert.ToByte(row["BeginOctet2"]);
            byte beginOctet3 = Convert.ToByte(row["BeginOctet3"]);
            byte beginOctet4 = Convert.ToByte(row["BeginOctet4"]);
            IPAddress beginningAddress = new IPAddress(beginOctet1, beginOctet2, beginOctet3, beginOctet4);
            byte endOctet1 = Convert.ToByte(row["EndOctet1"]);
            byte endOctet2 = Convert.ToByte(row["EndOctet2"]);
            byte endOctet3 = Convert.ToByte(row["EndOctet3"]);
            byte endOctet4 = Convert.ToByte(row["EndOctet4"]);
            IPAddress endingAddress = new IPAddress(endOctet1, endOctet2, endOctet3, endOctet4);
            string createdBy = Convert.ToString(row["CreatedBy"]);
            DateTime createdDate = Convert.ToDateTime(row["CreatedDate"]);
            CountryID countryID = (CountryID)Convert.ToInt32(row["CountryID"]);
            string countryName = Convert.ToString(row["CountryName"]);
            Country country = new Country(countryID, countryName);
            RangeType IPRangeTypeID = (RangeType)Convert.ToInt32(row["IPRangeTypeID"]);
            DateTime? expireDate = null;
            if (!Convert.IsDBNull(row["ExpireDate"])) expireDate = Convert.ToDateTime(row["ExpireDate"]);
            int registrationAttemptsTotal = Convert.ToInt32(row["RegistrationAttemptsTotal"]);
            int registrationAttemptsNinetyDays = Convert.ToInt32(row["RegistrationAttemptsNinetyDays"]);
            int subscriptionAttemptsTotal = Convert.ToInt32(row["SubscriptionAttemptsTotal"]);
            int subscriptionAttemptsNinetyDays = Convert.ToInt32(row["SubscriptionAttemptsNinetyDays"]);
            DateTime lastAttemptDate = row["MaxLastAttemptDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(row["MaxLastAttemptDate"]);

            BlacklistedIPRange bip = new BlacklistedIPRange(IPRangeListID, beginningAddress, endingAddress, createdBy, createdDate, country, expireDate);

            AccessSummary registrationSummary = new AccessSummary(AccessType.Registration, registrationAttemptsTotal, 0, registrationAttemptsNinetyDays, lastAttemptDate);
            AccessSummary subscriptionSummary = new AccessSummary(AccessType.Subscription, subscriptionAttemptsTotal, 0, subscriptionAttemptsNinetyDays, lastAttemptDate);
            List<AccessSummary> summaries = new List<AccessSummary> { registrationSummary, subscriptionSummary };

            return new IPRangeSummary(bip, summaries);
        }

        private IPRangeSummary datareaderToRangeSummaryByCountry(IDataReader dataReader)
        {
            int IPRangeListID = Convert.ToInt32(dataReader["IPRangeListID"]);
            byte beginOctet1 = Convert.ToByte(dataReader["BeginOctet1"]);
            byte beginOctet2 = Convert.ToByte(dataReader["BeginOctet2"]);
            byte beginOctet3 = Convert.ToByte(dataReader["BeginOctet3"]);
            byte beginOctet4 = Convert.ToByte(dataReader["BeginOctet4"]);
            IPAddress beginningAddress = new IPAddress(beginOctet1, beginOctet2, beginOctet3, beginOctet4);
            byte endOctet1 = Convert.ToByte(dataReader["EndOctet1"]);
            byte endOctet2 = Convert.ToByte(dataReader["EndOctet2"]);
            byte endOctet3 = Convert.ToByte(dataReader["EndOctet3"]);
            byte endOctet4 = Convert.ToByte(dataReader["EndOctet4"]);
            IPAddress endingAddress = new IPAddress(endOctet1, endOctet2, endOctet3, endOctet4);
            string createdBy = Convert.ToString(dataReader["CreatedBy"]);
            DateTime createdDate = Convert.ToDateTime(dataReader["CreatedDate"]);
            CountryID countryID = (CountryID)Convert.ToInt32(dataReader["CountryID"]);
            string countryName = Convert.ToString(dataReader["CountryName"]);
            Country country = new Country(countryID, countryName);
            RangeType IPRangeTypeID = (RangeType)Convert.ToInt32(dataReader["IPRangeTypeID"]);
            DateTime? expireDate = null;
            if (!Convert.IsDBNull(dataReader["ExpireDate"])) expireDate = Convert.ToDateTime(dataReader["ExpireDate"]);
            int registrationAttemptsTotal = Convert.ToInt32(dataReader["RegistrationAttemptsTotal"]);
            int registrationAttemptsNinetyDays = Convert.ToInt32(dataReader["RegistrationAttemptsNinetyDays"]);
            int subscriptionAttemptsTotal = Convert.ToInt32(dataReader["SubscriptionAttemptsTotal"]);
            int subscriptionAttemptsNinetyDays = Convert.ToInt32(dataReader["SubscriptionAttemptsNinetyDays"]);
            DateTime lastAttemptDate = dataReader["MaxLastAttemptDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dataReader["MaxLastAttemptDate"]);

            BlacklistedIPRange bip = new BlacklistedIPRange(IPRangeListID, beginningAddress, endingAddress, createdBy, createdDate, country, expireDate);

            AccessSummary registrationSummary = new AccessSummary(AccessType.Registration, registrationAttemptsTotal, 0, registrationAttemptsNinetyDays, lastAttemptDate);
            AccessSummary subscriptionSummary = new AccessSummary(AccessType.Subscription, subscriptionAttemptsTotal, 0, subscriptionAttemptsNinetyDays, lastAttemptDate);
            List<AccessSummary> summaries = new List<AccessSummary> { registrationSummary, subscriptionSummary };

            return new IPRangeSummary(bip, summaries);
        }

        private IPRangeCountrySummary dataReaderToCountrySummary(IDataReader dataReader)
        {
            CountryID countryID = (CountryID)Convert.ToInt32(dataReader["CountryID"]);
            string countryName = dataReader["Name"].ToString();
            int registrationAttemptsTotal = Convert.ToInt32(dataReader["RegistrationAttemptsTotal"]);
            int subscriptionAttemptsTotal = Convert.ToInt32(dataReader["SubscriptionAttemptsTotal"]);
            int registrationAttemptsNinetyDays = Convert.ToInt32(dataReader["RegistrationAttemptsNinetyDays"]);
            int subscriptionAttemptsNinetyDays = Convert.ToInt32(dataReader["SubscriptionAttemptsNinetyDays"]);
            int blockCount = Convert.ToInt32(dataReader["BlockCount"]);

            Country country = new Country(countryID, countryName);
            AccessSummary subscriptionSummary = new AccessSummary(AccessType.Subscription, subscriptionAttemptsTotal, 0, subscriptionAttemptsNinetyDays, DateTime.MinValue);
            AccessSummary registrationSummary = new AccessSummary(AccessType.Registration, registrationAttemptsTotal, 0, registrationAttemptsNinetyDays, DateTime.MinValue);
            List<AccessSummary> summaries = new List<AccessSummary>();
            summaries.Add(subscriptionSummary);
            summaries.Add(registrationSummary);

            IPRangeCountrySummary countrySummary = new IPRangeCountrySummary(country, blockCount, summaries);
            return countrySummary;
        }

        private IPRangeNote dataRowToIPRangeNote(DataRow row)
        {
            int IPRangeNoteID = Convert.ToInt32(row["IPRangeNoteID"]);
            string createdBy = Convert.ToString(row["CreatedBy"]);
            DateTime createdDate = Convert.ToDateTime(row["CreatedDate"]);
            string note = Convert.ToString(row["Note"]);

            return new IPRangeNote(IPRangeNoteID, createdBy, createdDate, note);
                
        }

        private void LogDBTimeout(IPAddress address, long time)
        {
            Log.Error(string.Format("DB Access Check took {0} milliseconds for IPAddress: {1}", time.ToString(), address.ToString()));
        }


        public List<IPRange> GetIpRangesByGroupID(int ipRangeGroupId)
        {
            List<IPRange> ipRanges = null;

            try
            {
                Log.Debug(string.Format("Entering IPBlockerRepository.GetIpRangesByGroupID({0})", ipRangeGroupId));

                Command command = new Command("mnIPBlocker", "dbo.up_IPRange_Get_By_GroupID", 0);
                command.AddParameter("@IpRangeGroupId", SqlDbType.TinyInt, ParameterDirection.Input, ipRangeGroupId);
               

                DataSet ds = Client.Instance.ExecuteDataSet(command);

                if (ds.Tables[0] != null)
                {
                    ipRanges = new List<IPRange>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ipRanges.Add(dataRowToIPRange(row));
                    }
                }

                return ipRanges;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException("Error in IPBlockerRepository.GetIpRangesByGroupID. IPRangeGroupID: " + ipRangeGroupId, ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerRepository.GetIpRangesByGroupID");
            }
        }
    }
}
