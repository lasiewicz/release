﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.SqlTypes;
using System.Data.Common;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Contracts;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Engine.DataAccess
{
    public class SQLIPBlockerRepository : IIPBlockerRepository
    {
        public List<Country> GetAllCountries()
        {
            IDataReader reader;
            List<Country> countries = null;

            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_Get_All_Countries");
                reader = sqlDb.ExecuteReader(cmd);

                if (!reader.IsClosed)
                {
                    countries = new List<Country>();
                    while (reader.Read())
                    {
                        countries.Add(dataReaderToCountry(reader));
                    }
                }

                return countries;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IPRangeCountrySummary> GetCountrySummariesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            IDataReader reader;
            List<IPRangeCountrySummary> summaries = null;
            totalRowCount = 0;

            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Get_Country_Summary_Paged");
                sqlDb.AddInParameter(cmd, "@PageSize", DbType.Int32, pageSize);
                sqlDb.AddInParameter(cmd, "@PageNumber", DbType.Int32, pageNumber);
                sqlDb.AddInParameter(cmd, "@SortField", DbType.String, sortField);
                sqlDb.AddInParameter(cmd, "@Descending", DbType.Boolean, descending);

                reader = sqlDb.ExecuteReader(cmd);

                if (!reader.IsClosed)
                {
                    summaries = new List<IPRangeCountrySummary>();
                    while (reader.Read())
                    {
                        totalRowCount = Convert.ToInt32(reader["TotalRows"].ToString());
                        summaries.Add(dataReaderToCountrySummary(reader));
                    }
                }

                return summaries;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IPRangeSummary> GetRangesByCountryPaged(CountryID countryID, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            IDataReader reader;
            List<IPRangeSummary> summaries = null;
            totalRowCount = 0;

            string actualSortField;

            switch (sortField)
            {
                case "range":
                    actualSortField = "beginNumber";
                    break;
                default:
                    actualSortField = sortField;
                    break;
            }
            
            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Get_Ranges_By_Country_Paged");
                sqlDb.AddInParameter(cmd, "@CountryID", DbType.Int32, (int)countryID);
                sqlDb.AddInParameter(cmd, "@PageSize", DbType.Int32, pageSize);
                sqlDb.AddInParameter(cmd, "@PageNumber", DbType.Int32, pageNumber);
                sqlDb.AddInParameter(cmd, "@SortField", DbType.String, actualSortField);
                sqlDb.AddInParameter(cmd, "@Descending", DbType.Boolean, descending);

                reader = sqlDb.ExecuteReader(cmd);

                if (!reader.IsClosed)
                {
                    summaries = new List<IPRangeSummary>();
                    while (reader.Read())
                    {
                        totalRowCount = Convert.ToInt32(reader["TotalRows"].ToString());
                        summaries.Add(datareaderToRangeSummaryByCountry(reader));
                    }
                }

                return summaries;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int IsAddressBlocked(IPAddress address)
        {
            IDataReader dataReader = null;
            int returnValue = 0;

            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());

                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Is_Address_Blocked");
                sqlDb.AddInParameter(cmd, "@Octet1", DbType.Byte, address.FirstOctet);
                sqlDb.AddInParameter(cmd, "@Octet2", DbType.Byte, address.SecondOctet);
                sqlDb.AddInParameter(cmd, "@Octet3", DbType.Byte, address.ThirdOctet);
                sqlDb.AddInParameter(cmd, "@Octet4", DbType.Byte, address.FourthOctet);

                dataReader = sqlDb.ExecuteReader(cmd);
                if (!dataReader.IsClosed && dataReader.Read())
                {
                    returnValue = dataReader.GetInt32(0);
                }

                return returnValue;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int IsAddressBlocked(IPAddress address, AccessType accessType, int siteID)
        {
            ///not implemented
            return 0;
        }

        public int IsAddressBlockedNoLog(IPAddress address, int siteID)
        {
            //not implemented
            return 0;
        }

        public int IsAddressBlockedForGroupId(IPAddress address, int IPRangeGroupID)
        {
            //not implemented
            return 0;
        }

        public IPRange GetRange(int rangeID)
        {
            IPRange range = null;

            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());

                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRangeList_Get_Range");
                sqlDb.AddInParameter(cmd, "@IPRangeListID", DbType.Int32, rangeID);

                DataSet ds = sqlDb.ExecuteDataSet(cmd);

                DataTable IPRangeTable = ds.Tables[0];
                DataTable IPRangeNoteTable = ds.Tables[1];
                IPRangeTable.ChildRelations.Add(
                    "fk_IPRangeList_IPRangeNote",
                    new DataColumn[] { IPRangeTable.Columns["IPRangeListID"] },
                    new DataColumn[] { IPRangeNoteTable.Columns["IPRangeListID"] }
                    );

                if (IPRangeTable != null && IPRangeTable.Rows.Count == 1)
                {
                    range = dataRowToIPRange(IPRangeTable.Rows[0]);

                    DataRow[] notesRows = IPRangeTable.Rows[0].GetChildRows("fk_IPRangeList_IPRangeNote");
                    if (notesRows != null && notesRows.Count() > 0)
                    {
                        foreach (DataRow notesRow in notesRows)
                        {
                            range.Notes.Add(dataRowToIPRangeNote(notesRow));
                        }
                    }
                }

                return range;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<IPRange> GetOverlappingRanges(IPAddress beginningAddress, IPAddress endingAddress)
        {
            List<IPRange> overlappingRanges = null;

            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());

                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Get_Overlapping_Ranges");
                sqlDb.AddInParameter(cmd, "@BeginOctet1", DbType.Byte, beginningAddress.FirstOctet);
                sqlDb.AddInParameter(cmd, "@BeginOctet2", DbType.Byte, beginningAddress.SecondOctet);
                sqlDb.AddInParameter(cmd, "@BeginOctet3", DbType.Byte, beginningAddress.ThirdOctet);
                sqlDb.AddInParameter(cmd, "@BeginOctet4", DbType.Byte, beginningAddress.FourthOctet);
                sqlDb.AddInParameter(cmd, "@EndOctet1", DbType.Byte, endingAddress.FirstOctet);
                sqlDb.AddInParameter(cmd, "@EndOctet2", DbType.Byte, endingAddress.SecondOctet);
                sqlDb.AddInParameter(cmd, "@EndOctet3", DbType.Byte, endingAddress.ThirdOctet);
                sqlDb.AddInParameter(cmd, "@EndOctet4", DbType.Byte, endingAddress.FourthOctet);

                DataSet ds = sqlDb.ExecuteDataSet(cmd);

                if (ds.Tables[0] != null)
                {
                    overlappingRanges = new List<IPRange>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        overlappingRanges.Add(dataRowToIPRange(row));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return overlappingRanges;
        }

        public List<AccessSummary> GetSummariesForRange(int rangeID)
        {
            IDataReader dataReader = null;
            List<AccessSummary> summaries = null;

            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());

                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Get_Summaries");
                sqlDb.AddInParameter(cmd, "@RangeListID", DbType.Int32, rangeID);

                dataReader = sqlDb.ExecuteReader(cmd);
                if (!dataReader.IsClosed)
                {
                    summaries = new List<AccessSummary>();
                    while (dataReader.Read())
                    {
                        summaries.Add(dataReaderToAccessSummary(dataReader));
                    }
                }

                return summaries;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IPRangeSummary> GetRangeSummaryForCountry(CountryID ID)
        {
            return new List<IPRangeSummary>();
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRanges()
        {
            List<WhitelistedIPRange> whitelistedRanges = null;

            try
            {

                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Get_Whitelisted_Ranges");

                DataSet ds = sqlDb.ExecuteDataSet(cmd);

                if (ds.Tables[0] != null)
                {
                    whitelistedRanges = new List<WhitelistedIPRange>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        whitelistedRanges.Add(dataRowToIPRange(row) as WhitelistedIPRange);
                    }
                }

                return whitelistedRanges;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRangesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            List<WhitelistedIPRange> whitelistedRanges = null;
            totalRowCount = 0;
            string actualSortField;

            try
            {
                switch (sortField.ToLower())
                {
                    case "name":
                        actualSortField = "IPRangeList.Name";
                        break;
                    case "iprange":
                        actualSortField = "BeginNumber";
                        break;
                    case "country":
                        actualSortField = "Country.Name";
                        break;
                    case "createdby":
                        actualSortField = "CreatedBy";
                        break;
                    case "createddate":
                        actualSortField = "CreatedDate";
                        break;
                    default:
                        actualSortField = string.Empty;
                        break;
                }
                
                SqlDatabase sqlDb = new SqlDatabase(getConnection());

                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Get_Whitelisted_Ranges_Paged");
                sqlDb.AddInParameter(cmd, "@PageSize", DbType.Int32, pageSize);
                sqlDb.AddInParameter(cmd, "@PageNumber", DbType.Int32, pageNumber);
                sqlDb.AddInParameter(cmd, "@SortField", DbType.String, actualSortField);
                sqlDb.AddInParameter(cmd, "@Descending", DbType.Boolean, descending);

                DataSet ds = sqlDb.ExecuteDataSet(cmd);

                if (ds.Tables[0] != null)
                {
                    whitelistedRanges = new List<WhitelistedIPRange>();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        totalRowCount = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalRows"].ToString());
                    }
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        whitelistedRanges.Add(dataRowToIPRange(row) as WhitelistedIPRange);
                    }
                }

                return whitelistedRanges;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IPRangeSummary> GetExpiringRangeSummariesPaged(DateTime beginDate, DateTime endDate, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            List<IPRangeSummary> summaries = null;
            totalRowCount = 0;

            string actualSortField;

            switch (sortField)
            {
                case "range":
                    actualSortField = "beginNumber";
                    break;
                default:
                    actualSortField = sortField;
                    break;
            }

            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Get_Expiring_Ranges_Paged");
                sqlDb.AddInParameter(cmd, "@BeginDate", DbType.DateTime, beginDate);
                sqlDb.AddInParameter(cmd, "@EndDate", DbType.DateTime, endDate);
                sqlDb.AddInParameter(cmd, "@PageSize", DbType.Int32, pageSize);
                sqlDb.AddInParameter(cmd, "@PageNumber", DbType.Int32, pageNumber);
                sqlDb.AddInParameter(cmd, "@SortField", DbType.String, actualSortField);
                sqlDb.AddInParameter(cmd, "@Descending", DbType.Boolean, descending);

                DataSet ds = sqlDb.ExecuteDataSet(cmd);

                if (ds.Tables[0] != null)
                {
                    summaries = new List<IPRangeSummary>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        totalRowCount = Convert.ToInt32(row["TotalRows"]);
                        summaries.Add(dataRowToIPRangeSummary(row) as IPRangeSummary);
                    }
                }

                return summaries;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddRangeNote(int rangeID, string createdBy, string note)
        {
            Int32 IPRangeNoteID = 0;
            try
            {
                IPRangeNoteID = getNextID("IPRangeNoteID");

                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRangeList_Add_Note");
                sqlDb.AddInParameter(cmd, "@IPRangeNoteID", DbType.Int32, IPRangeNoteID);
                sqlDb.AddInParameter(cmd, "@IPRangeListID", DbType.Int32, rangeID);
                sqlDb.AddInParameter(cmd, "@CreatedBy", DbType.String, createdBy);
                sqlDb.AddInParameter(cmd, "@Note", DbType.String, note);

                sqlDb.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LogAccessAttempt(AccessType accessType, int rangeID)
        {
            try
            {
                int IPAccessLogID = getNextID("IPAccessLogID"); ;

                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Log_Access_Attempt");
                sqlDb.AddInParameter(cmd, "@IPAccessLogID", DbType.Int32, IPAccessLogID);
                sqlDb.AddInParameter(cmd, "@IPAccessTypeID", DbType.Int32, (int)accessType);
                sqlDb.AddInParameter(cmd, "@IPRangeListID", DbType.String, rangeID);

                sqlDb.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateExpirationDate(int rangeID, DateTime? expireDate)
        {
            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRange_Update_Expiration_Date");
                sqlDb.AddInParameter(cmd, "@IPRangeListID", DbType.Int32, rangeID);
                if (expireDate.HasValue)
                {
                    sqlDb.AddInParameter(cmd, "@ExpireDate", DbType.Int32, expireDate);
                }
 
                sqlDb.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveRange(IPRange range)
        {
            Int32 IPRangeListID = range.ID;

            try
            {
                RangeType rangeType = range is BlacklistedIPRange ? RangeType.BlackList : RangeType.WhiteList;

                if (range.ID <= 0)
                {
                    IPRangeListID = getNextID("IPRangeListID");
                }
                else
                {
                    IPRangeListID = range.ID;
                }

                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRangeList_Save");
                sqlDb.AddInParameter(cmd, "@IPRangeListID", DbType.Int32, IPRangeListID);
                sqlDb.AddInParameter(cmd, "@BeginOctet1", DbType.Byte, range.BeginningAddress.FirstOctet);
                sqlDb.AddInParameter(cmd, "@BeginOctet2", DbType.Byte, range.BeginningAddress.SecondOctet);
                sqlDb.AddInParameter(cmd, "@BeginOctet3", DbType.Byte, range.BeginningAddress.ThirdOctet);
                sqlDb.AddInParameter(cmd, "@BeginOctet4", DbType.Byte, range.BeginningAddress.FourthOctet);
                sqlDb.AddInParameter(cmd, "@EndOctet1", DbType.Byte, range.EndingAddress.FirstOctet);
                sqlDb.AddInParameter(cmd, "@EndOctet2", DbType.Byte, range.EndingAddress.SecondOctet);
                sqlDb.AddInParameter(cmd, "@EndOctet3", DbType.Byte, range.EndingAddress.ThirdOctet);
                sqlDb.AddInParameter(cmd, "@EndOctet4", DbType.Byte, range.EndingAddress.FourthOctet);
                sqlDb.AddInParameter(cmd, "@CreatedBy", DbType.String, range.CreatedBy);
                sqlDb.AddInParameter(cmd, "@CountryID", DbType.Int32, (int)range.Country.ID);
                sqlDb.AddInParameter(cmd, "@IPRangeTypeID", DbType.Int32, (int)rangeType);

                sqlDb.ExecuteNonQuery(cmd);


                if (rangeType == RangeType.BlackList)
                {
                    sqlDb.AddInParameter(cmd, "@ExpireDate", DbType.DateTime, (range as BlacklistedIPRange).ExpireDate);
                }
                else
                {
                    sqlDb.AddInParameter(cmd, "@Name", DbType.String, (range as WhitelistedIPRange).Name);
                }

                sqlDb.ExecuteNonQuery(cmd);

                return IPRangeListID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteRange(int rangeID)
        {
            try
            {
                SqlDatabase sqlDb = new SqlDatabase(getConnection());
                DbCommand cmd = sqlDb.GetStoredProcCommand("dbo.up_IPRangeList_Delete");
                sqlDb.AddInParameter(cmd, "@IPRangeListID", DbType.Int32, rangeID);

                sqlDb.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Country dataReaderToCountry(IDataReader dataReader)
        {
            CountryID id = (CountryID)Convert.ToInt32(dataReader["CountryID"]);
            string name = Convert.ToString(dataReader["Name"]);
            string abbreviation = Convert.ToString(dataReader["Abbreviation"]);

            return new Country(id, name, abbreviation);
        }

        private AccessSummary dataReaderToAccessSummary(IDataReader dataReader)
        {
            int totalCount = Convert.ToInt32(dataReader["TotalAttempts"]);
            int thirtyDayCount = Convert.ToInt32(dataReader["ThirtyDayAttempts"]);
            int ninetyDayCount = Convert.ToInt32(dataReader["NinetyDayAttempts"]);
            AccessType IPAccessTypeID = (AccessType)Convert.ToInt32(dataReader["IPAccessTypeID"]);
            DateTime lastAccessDate = dataReader["LastAttemptDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dataReader["LastAttemptDate"]);

            return new AccessSummary(IPAccessTypeID, totalCount, thirtyDayCount, ninetyDayCount, lastAccessDate);
        }

        private IPRange dataRowToIPRange(DataRow row)
        {
            int IPRangeListID = Convert.ToInt32(row["IPRangeListID"]);
            byte beginOctet1 = Convert.ToByte(row["BeginOctet1"]);
            byte beginOctet2 = Convert.ToByte(row["BeginOctet2"]);
            byte beginOctet3 = Convert.ToByte(row["BeginOctet3"]);
            byte beginOctet4 = Convert.ToByte(row["BeginOctet4"]);
            IPAddress beginningAddress = new IPAddress(beginOctet1, beginOctet2, beginOctet3, beginOctet4);
            byte endOctet1 = Convert.ToByte(row["EndOctet1"]);
            byte endOctet2 = Convert.ToByte(row["EndOctet2"]);
            byte endOctet3 = Convert.ToByte(row["EndOctet3"]);
            byte endOctet4 = Convert.ToByte(row["EndOctet4"]);
            IPAddress endingAddress = new IPAddress(endOctet1, endOctet2, endOctet3, endOctet4);
            string createdBy = Convert.ToString(row["CreatedBy"]);
            DateTime createdDate = Convert.ToDateTime(row["CreatedDate"]);
            CountryID countryID = (CountryID)Convert.ToInt32(row["CountryID"]);
            string countryName = Convert.ToString(row["CountryName"]);
            Country country = new Country(countryID, countryName);
            RangeType IPRangeTypeID = (RangeType)Convert.ToInt32(row["IPRangeTypeID"]);
            DateTime? expireDate = null;
            string rangeName = string.Empty;

            if (!Convert.IsDBNull(row["ExpireDate"])) expireDate = Convert.ToDateTime(row["ExpireDate"]);
            if (!Convert.IsDBNull(row["IPRangeName"])) rangeName = Convert.ToString(row["IPRangeName"]);

            if (IPRangeTypeID == RangeType.BlackList)
            {
                return new BlacklistedIPRange(IPRangeListID, beginningAddress, endingAddress, createdBy, createdDate, country, expireDate);
            }
            else
            {
                return new WhitelistedIPRange(IPRangeListID, beginningAddress, endingAddress, createdBy, createdDate, country, rangeName);
            }

        }

        private IPRangeSummary dataRowToIPRangeSummary(DataRow row)
        {
            int IPRangeListID = Convert.ToInt32(row["IPRangeListID"]);
            byte beginOctet1 = Convert.ToByte(row["BeginOctet1"]);
            byte beginOctet2 = Convert.ToByte(row["BeginOctet2"]);
            byte beginOctet3 = Convert.ToByte(row["BeginOctet3"]);
            byte beginOctet4 = Convert.ToByte(row["BeginOctet4"]);
            IPAddress beginningAddress = new IPAddress(beginOctet1, beginOctet2, beginOctet3, beginOctet4);
            byte endOctet1 = Convert.ToByte(row["EndOctet1"]);
            byte endOctet2 = Convert.ToByte(row["EndOctet2"]);
            byte endOctet3 = Convert.ToByte(row["EndOctet3"]);
            byte endOctet4 = Convert.ToByte(row["EndOctet4"]);
            IPAddress endingAddress = new IPAddress(endOctet1, endOctet2, endOctet3, endOctet4);
            string createdBy = Convert.ToString(row["CreatedBy"]);
            DateTime createdDate = Convert.ToDateTime(row["CreatedDate"]);
            CountryID countryID = (CountryID)Convert.ToInt32(row["CountryID"]);
            string countryName = Convert.ToString(row["CountryName"]);
            Country country = new Country(countryID, countryName);
            RangeType IPRangeTypeID = (RangeType)Convert.ToInt32(row["IPRangeTypeID"]);
            DateTime? expireDate = null;
            if (!Convert.IsDBNull(row["ExpireDate"])) expireDate = Convert.ToDateTime(row["ExpireDate"]);
            int registrationAttemptsTotal = Convert.ToInt32(row["RegistrationAttemptsTotal"]);
            int registrationAttemptsNinetyDays = Convert.ToInt32(row["RegistrationAttemptsNinetyDays"]);
            int subscriptionAttemptsTotal = Convert.ToInt32(row["SubscriptionAttemptsTotal"]);
            int subscriptionAttemptsNinetyDays = Convert.ToInt32(row["SubscriptionAttemptsNinetyDays"]);
            DateTime lastAttemptDate = row["MaxLastAttemptDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(row["MaxLastAttemptDate"]);

            BlacklistedIPRange bip = new BlacklistedIPRange(IPRangeListID, beginningAddress, endingAddress, createdBy, createdDate, country, expireDate);

            AccessSummary registrationSummary = new AccessSummary(AccessType.Registration, registrationAttemptsTotal, 0, registrationAttemptsNinetyDays, lastAttemptDate);
            AccessSummary subscriptionSummary = new AccessSummary(AccessType.Subscription, subscriptionAttemptsTotal, 0, subscriptionAttemptsNinetyDays, lastAttemptDate);
            List<AccessSummary> summaries = new List<AccessSummary> { registrationSummary, subscriptionSummary };

            return new IPRangeSummary(bip, summaries);
        }

        private IPRangeSummary datareaderToRangeSummaryByCountry(IDataReader dataReader)
        {
            int IPRangeListID = Convert.ToInt32(dataReader["IPRangeListID"]);
            byte beginOctet1 = Convert.ToByte(dataReader["BeginOctet1"]);
            byte beginOctet2 = Convert.ToByte(dataReader["BeginOctet2"]);
            byte beginOctet3 = Convert.ToByte(dataReader["BeginOctet3"]);
            byte beginOctet4 = Convert.ToByte(dataReader["BeginOctet4"]);
            IPAddress beginningAddress = new IPAddress(beginOctet1, beginOctet2, beginOctet3, beginOctet4);
            byte endOctet1 = Convert.ToByte(dataReader["EndOctet1"]);
            byte endOctet2 = Convert.ToByte(dataReader["EndOctet2"]);
            byte endOctet3 = Convert.ToByte(dataReader["EndOctet3"]);
            byte endOctet4 = Convert.ToByte(dataReader["EndOctet4"]);
            IPAddress endingAddress = new IPAddress(endOctet1, endOctet2, endOctet3, endOctet4);
            string createdBy = Convert.ToString(dataReader["CreatedBy"]);
            DateTime createdDate = Convert.ToDateTime(dataReader["CreatedDate"]);
            CountryID countryID = (CountryID)Convert.ToInt32(dataReader["CountryID"]);
            string countryName = Convert.ToString(dataReader["CountryName"]);
            Country country = new Country(countryID, countryName);
            RangeType IPRangeTypeID = (RangeType)Convert.ToInt32(dataReader["IPRangeTypeID"]);
            DateTime? expireDate = null;
            if (!Convert.IsDBNull(dataReader["ExpireDate"])) expireDate = Convert.ToDateTime(dataReader["ExpireDate"]);
            int registrationAttemptsTotal = Convert.ToInt32(dataReader["RegistrationAttemptsTotal"]);
            int registrationAttemptsNinetyDays = Convert.ToInt32(dataReader["RegistrationAttemptsNinetyDays"]); 
            int subscriptionAttemptsTotal = Convert.ToInt32(dataReader["SubscriptionAttemptsTotal"]);
            int subscriptionAttemptsNinetyDays = Convert.ToInt32(dataReader["SubscriptionAttemptsNinetyDays"]);
            DateTime lastAttemptDate = dataReader["MaxLastAttemptDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dataReader["MaxLastAttemptDate"]);
            
            BlacklistedIPRange bip = new BlacklistedIPRange(IPRangeListID, beginningAddress, endingAddress, createdBy, createdDate, country, expireDate);

            AccessSummary registrationSummary = new AccessSummary(AccessType.Registration, registrationAttemptsTotal, 0, registrationAttemptsNinetyDays, lastAttemptDate);
            AccessSummary subscriptionSummary = new AccessSummary(AccessType.Subscription, subscriptionAttemptsTotal, 0, subscriptionAttemptsNinetyDays, lastAttemptDate);
            List<AccessSummary> summaries = new List<AccessSummary>{registrationSummary, subscriptionSummary};

            return new IPRangeSummary(bip, summaries);
        }

        private IPRangeCountrySummary dataReaderToCountrySummary(IDataReader dataReader)
        {
            CountryID countryID = (CountryID)Convert.ToInt32(dataReader["CountryID"]);
            string countryName = dataReader["Name"].ToString();
            int registrationAttemptsTotal = Convert.ToInt32(dataReader["RegistrationAttemptsTotal"]);
            int subscriptionAttemptsTotal = Convert.ToInt32(dataReader["SubscriptionAttemptsTotal"]);
            int registrationAttemptsNinetyDays = Convert.ToInt32(dataReader["RegistrationAttemptsNinetyDays"]);
            int subscriptionAttemptsNinetyDays = Convert.ToInt32(dataReader["SubscriptionAttemptsNinetyDays"]);
            int blockCount = Convert.ToInt32(dataReader["BlockCount"]);

            Country country = new Country(countryID, countryName);
            AccessSummary subscriptionSummary = new AccessSummary(AccessType.Subscription, subscriptionAttemptsTotal, 0, subscriptionAttemptsNinetyDays, DateTime.MinValue);
            AccessSummary registrationSummary = new AccessSummary(AccessType.Registration, registrationAttemptsTotal, 0, registrationAttemptsNinetyDays, DateTime.MinValue);
            List<AccessSummary> summaries = new List<AccessSummary>();
            summaries.Add(subscriptionSummary);
            summaries.Add(registrationSummary);

            IPRangeCountrySummary countrySummary = new IPRangeCountrySummary(country, blockCount, summaries);
            return countrySummary;
        }

        private IPRangeNote dataRowToIPRangeNote(DataRow row)
        {
            int IPRangeNoteID = Convert.ToInt32(row["IPRangeNoteID"]);
            string createdBy = Convert.ToString(row["CreatedBy"]);
            DateTime createdDate = Convert.ToDateTime(row["CreatedDate"]);
            string note = Convert.ToString(row["Note"]);

            return new IPRangeNote(IPRangeNoteID, createdBy, createdDate, note);

        }

        private string getConnection()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["SQLIPBlockerRepository"].ConnectionString;
        }

        private int getNextID(string IDField)
        {
            int id = 0;

            SqlDatabase sqlDb = new SqlDatabase(getConnection());
            DbCommand cmd = null;

            switch (IDField)
            {
                case "IPRangeListID":
                    cmd = sqlDb.GetSqlStringCommand("SELECT Max(IPRangeListID) + 1 FROM IPRangeList");
                    break;
                case "IPAccessLogID":
                    cmd = sqlDb.GetSqlStringCommand("SELECT Max(IPAccessLogID) + 1 FROM IPAccessLog");
                    break;
                case "IPRangeNoteID":
                    cmd = sqlDb.GetSqlStringCommand("SELECT Max(IPRangeNoteID) + 1 FROM IPRangeNote");
                    break;
            }

            id = int.Parse(sqlDb.ExecuteScalar(cmd).ToString());
            
            return id;
        }




        public List<IPRange> GetIpRangesByGroupID(int ipRangeGroupId)
        {
            throw new NotImplementedException();
        }
    }


}
