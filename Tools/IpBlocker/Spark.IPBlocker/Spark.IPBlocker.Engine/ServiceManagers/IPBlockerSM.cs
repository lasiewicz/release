﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Matchnet.Exceptions;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Contracts;
using Spark.IPBlocker.Engine.BusinessLogic;
using Spark.IPBlocker.Engine.DataAccess;
using Spark.IPBlocker.ValueObjects.Exceptions;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Engine.ServiceManagers
{
    public class IPBlockerSM
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(IPBlockerSM));

        public IPBlockerSM() 
        {
        }
        
        public int CheckAccess(IPAddress address, AccessType accessType)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.CheckAccess()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.CheckAccess() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.CheckAccess(repository, address, accessType);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.CheckAccess()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.CheckAccess");
            }
        }

        public int CheckAccess(IPAddress address, AccessType accessType, int siteID)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.CheckAccess()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.CheckAccess() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.CheckAccess(repository, address, accessType, siteID);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.CheckAccess()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.CheckAccess");
            }
        }

        public int CheckAccessNoLog(IPAddress address, int siteID)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.CheckAccess()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.CheckAccess() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.CheckAccessNoLog(repository, address, siteID);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.CheckAccess()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.CheckAccess");
            }
        }

        public int CheckAccessByGroupID(string address, int IPRangeGroupID)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.CheckAccessforKount()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.CheckAccess() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.CheckAccessByGroupId(repository, address, IPRangeGroupID);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.CheckAccess()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.CheckAccess");
            }
        }

        public IPRange GetRange(int rangeID)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetRange()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetRange() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetRange(repository, rangeID);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetRange()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetRange");
            }
        }

        public List<IPRange> GetOverlappingRanges(IPAddress beginningAddress, IPAddress endingAddress)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetOverlappingRanges()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetOverlappingRanges() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetOverlappingRanges(repository, beginningAddress, endingAddress);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetOverlappingRanges()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetOverlappingRanges");
            }
        }

        public List<AccessSummary> GetSummariesForRange(int rangeID)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetSummariesForRange()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetSummariesForRange() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetSummariesForRange(repository, rangeID);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetSummariesForRange()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetSummariesForRange");
            }
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRanges()
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetAllWhitelistedIPRanges()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetAllWhitelistedIPRanges() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetAllWhitelistedIPRanges(repository);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetAllWhitelistedIPRanges()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetAllWhitelistedIPRanges");
            }
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRangesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetAllWhitelistedIPRangesPaged()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetAllWhitelistedIPRangesPaged() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetAllWhitelistedIPRangesPaged(repository, pageSize, pageNumber, sortField, descending, out totalRowCount);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetAllWhitelistedIPRangesPaged()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetAllWhitelistedIPRangesPaged");
            }
        }

        public List<IPRangeCountrySummary> GetCountrySummariesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetCountrySummariesPaged()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetCountrySummariesPaged() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetCountrySummariesPaged(repository, pageSize, pageNumber, sortField, descending, out totalRowCount);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetCountrySummariesPaged()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetCountrySummariesPaged");
            }
        }

        public List<IPRangeSummary> GetRangesByCountryPaged(CountryID countryID, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetRangesByCountryPaged()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetRangesByCountryPaged() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetRangesByCountryPaged(repository, countryID, pageSize, pageNumber, sortField, descending, out totalRowCount);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetRangesByCountryPaged()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetRangesByCountryPaged");
            }
        }

        public List<IPRangeSummary> GetExpiringRangeSummariesPaged(DateTime beginDate, DateTime endDate, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetExpiringRangeSummariesPaged()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetExpiringRangeSummariesPaged() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetExpiringRangeSummariesPaged(repository, beginDate, endDate, pageSize, pageNumber, sortField, descending, out totalRowCount);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetExpiringRangeSummariesPaged()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetExpiringRangeSummariesPaged");
            }
        }

        public void AddRangeNote(int rangeID, string createdBy, string note)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.AddRangeNote()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.AddRangeNote() Repository: {0}", repository.ToString()));
                IPBlockerBL.Instance.AddRangeNote(repository, rangeID, createdBy, note);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.AddRangeNote()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.AddRangeNote");
            }
        }

        public int SaveRange(IPRange range)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.SaveRange()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.SaveRange() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.SaveRange(repository, range);
            }
            catch (OverlappingRangesNotHandledException oex)
            {
                BLException exception = new BLException(string.Format("OverlappingRangesNotHandledException Error in IPBlockerSM.SaveRange()"), oex);
                Log.Error(exception);
                throw oex;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.SaveRange()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.SaveRange");
            }
        }

        public int SaveRange(IPRange range, List<int> rangesToDelete, List<int> rangesToKeep)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.SaveRange()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.SaveRange() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.SaveRange(repository, range, rangesToDelete, rangesToKeep);
            }
            catch (OverlappingRangesNotHandledException oex)
            {
                BLException exception = new BLException(string.Format("OverlappingRangesNotHandledException Error in IPBlockerSM.SaveRange()"), oex);
                Log.Error(exception);
                throw oex;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.SaveRange()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.SaveRange");
            }
        }

        public void DeleteRange(int rangeID)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.DeleteRange()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.DeleteRange() Repository: {0}", repository.ToString()));
                IPBlockerBL.Instance.DeleteRange(repository, rangeID);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.DeleteRange()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.DeleteRange");
            }
        }

        public void UpdateExpirationDate(int rangeID, DateTime? expireDate)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.UpdateExpirationDate()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.UpdateExpirationDate() Repository: {0}", repository.ToString()));
                IPBlockerBL.Instance.UpdateExpirationDate(repository, rangeID, expireDate);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.UpdateExpirationDate()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.UpdateExpirationDate");
            }
        }

        public List<Country> GetAllCountries()
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetAllCountries()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetAllCountries() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetAllCountries(repository);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetAllCountries()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetAllCountries");
            }
        }


        public List<IPRange> GetIpRangesByGroupID(int ipRangeGroupId)
        {
            try
            {
                Log.Debug(string.Format("Entering IPBlockerSM.GetIpRangesByGroupID()"));
                IIPBlockerRepository repository = new IPBlockerRepository();
                Log.Debug(string.Format("IPBlockerSM.GetIpRangesByGroupID() Repository: {0}", repository.ToString()));
                return IPBlockerBL.Instance.GetIpRangesByGroupID(repository, ipRangeGroupId);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerSM.GetIpRangesByGroupID()"), ex);
                Log.Error(exception);
                throw exception;
            }
            finally
            {
                Log.Debug("Leaving IPBlockerSM.GetIpRangesByGroupID");
            }
        }
    }
}
