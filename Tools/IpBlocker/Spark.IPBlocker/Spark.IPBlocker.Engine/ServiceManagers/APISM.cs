﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Contracts;
using Spark.IPBlocker.Engine.BusinessLogic;
using Spark.IPBlocker.Engine.DataAccess;
using Spark.IPBlocker.ValueObjects.Exceptions;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Engine.ServiceManagers
{
    public class APISM
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(APISM));

        public APISM() 
        {
        }
        
        public string CheckAccess(string ipAddress, AccessType accessType, int siteID)
        {
            Log.Debug(string.Format("Entering APISM.CheckAccess()"));
            string xml = string.Empty;
            IIPBlockerRepository repository = new IPBlockerRepository();
            xml = APIBL.Instance.CheckAccess(repository, ipAddress, accessType, siteID);
            Log.Debug(string.Format("Leaving APISM.CheckAccess()"));
            return xml;
        }

        public string CheckAccessJSON(string ipAddress, AccessType accessType, int siteID)
        {
            Log.Debug(string.Format("Entering APISM.CheckAccessJSON()"));
            string json = string.Empty;
            IIPBlockerRepository repository = new IPBlockerRepository();
            json = APIBL.Instance.CheckAccessJSON(repository, ipAddress, accessType, siteID);
            Log.Debug(string.Format("Leaving APISM.CheckAccessJSON()"));
            return json;
        }

        public string CheckAccessNoLog(string ipAddress, int siteID)
        {
            Log.Debug(string.Format("Entering APISM.CheckAccessNoLog()"));
            string xml = string.Empty;
            IIPBlockerRepository repository = new IPBlockerRepository();
            xml = APIBL.Instance.CheckAccessNoLog(repository, ipAddress, siteID);
            Log.Debug(string.Format("Leaving APISM.CheckAccessNoLog()"));
            return xml;
        }

        public string CheckAccessNoLogJSON(string ipAddress, int siteID)
        {
            Log.Debug(string.Format("Entering APISM.CheckAccessNoLogJSON()"));
            string json = string.Empty;
            IIPBlockerRepository repository = new IPBlockerRepository();
            json = APIBL.Instance.CheckAccessNoLogJSON(repository, ipAddress, siteID);
            Log.Debug(string.Format("Leaving APISM.CheckAccessNoLogJSON()"));
            return json;
        }

        public string AddIPRange(string beginningIpaddress, string endingIpaddress, string createdBy, CountryID countryID, DateTime expireDate, string note)
        {
            Log.Debug(string.Format("Entering APISM.AddIPRange()"));
            IIPBlockerRepository repository = new IPBlockerRepository();
            Log.Debug(string.Format("Leaving APISM.AddIPRange()"));
            return APIBL.Instance.AddIPRange(repository, beginningIpaddress, endingIpaddress, createdBy, countryID, expireDate, note);
        }
    }
}
