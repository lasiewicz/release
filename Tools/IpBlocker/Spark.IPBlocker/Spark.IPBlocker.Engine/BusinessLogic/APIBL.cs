﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using Matchnet.Exceptions;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Contracts;
using Spark.IPBlocker.ValueObjects.Exceptions;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Engine.BusinessLogic
{
    public class APIBL
    {
        public static readonly APIBL Instance = new APIBL();
        private static readonly ILog Log = LogManager.GetLogger(typeof(APIBL));

        private APIBL() 
        {
        }
        
        public string CheckAccess(IIPBlockerRepository repository, string ipaddress, AccessType accessType, int siteID)
        {
            return CheckAccessInternal(repository, ipaddress, accessType, siteID).ToXML();
        }

        public string CheckAccessJSON(IIPBlockerRepository repository, string ipaddress, AccessType accessType, int siteID)   
        {
            return CheckAccessInternal(repository, ipaddress, accessType, siteID).ToJSON();
        }

        public string CheckAccessNoLog(IIPBlockerRepository repository, string ipaddress, int siteID)
        {
            return CheckAccessNoLogInternal(repository, ipaddress, siteID).ToXML();
        }

        public string CheckAccessNoLogJSON(IIPBlockerRepository repository, string ipaddress, int siteID)
        {
            return CheckAccessNoLogInternal(repository, ipaddress, siteID).ToJSON();
        }

        private APIResponse CheckAccessInternal(IIPBlockerRepository repository, string ipaddress, AccessType accessType, int siteID)
        {
            APIResponse response = new APIResponse(true, "success");

            try
            {
                Log.Debug(string.Format("Entering APIBL.CheckAccess({0}, {1}, {2}, {3})", repository.ToString(), ipaddress, accessType.ToString(), siteID.ToString()));
                if (IPAddress.IsValidIP(ipaddress))
                {
                    IPAddress address = IPAddress.StringToIP(ipaddress);

                    int rangeID = repository.IsAddressBlocked(address, accessType, siteID);
                    if (rangeID > 0)
                    {
                        Log.Debug("APIBL.CheckAccess: Address blocked, logging attempt");
                        response.HasAccess = false;
                    }
                    else
                    {
                        Log.Debug("APIBL.CheckAccess: Address not blocked");
                    }
                }
                else
                {
                    Log.Debug("APIBL.CheckAccess: Invalid Address");
                    response.HasAccess = false;
                    response.Message = "Invalid IP address";
                }
            }
            catch (Exception ex)
            {
                response.HasAccess = false;
                response.Message = ex.Message;

                var exception = new BLException(string.Format("Error in APIBL.CheckAccess({0}, {1}, {2}, {3})", repository.ToString(), ipaddress, accessType.ToString(), siteID.ToString()), ex);
                Log.Error(exception);
            }
            finally
            {
                Log.Debug("Leaving APIBL.CheckAccess");
            }

            return response;
        }

        private APIResponse CheckAccessNoLogInternal(IIPBlockerRepository repository, string ipaddress, int siteID)
        {
            APIResponse response = new APIResponse(true, "success");

            try
            {
                Log.Debug(string.Format("Entering APIBL.CheckAccess({0}, {1}, {2})", repository.ToString(), ipaddress, siteID.ToString()));
                if (IPAddress.IsValidIP(ipaddress))
                {
                    IPAddress address = IPAddress.StringToIP(ipaddress);

                    int rangeID = repository.IsAddressBlockedNoLog(address, siteID);
                    if (rangeID > 0)
                    {
                        Log.Debug("APIBL.CheckAccess: Address blocked, logging attempt");
                        response.HasAccess = false;
                    }
                    else
                    {
                        Log.Debug("APIBL.CheckAccess: Address not blocked");
                    }
                }
                else
                {
                    Log.Debug("APIBL.CheckAccess: Invalid Address");
                    response.HasAccess = false;
                    response.Message = "Invalid IP address";
                }
            }
            catch (Exception ex)
            {
                response.HasAccess = false;
                response.Message = ex.Message;

                BLException exception = new BLException(string.Format("Error in APIBL.CheckAccess({0}, {1}, {2})", repository.ToString(), ipaddress, siteID.ToString()), ex);
                Log.Error(exception);
            }
            finally
            {
                Log.Debug("Leaving APIBL.CheckAccess");
            }

            return response;
        }


        public string AddIPRange(IIPBlockerRepository repository, string beginningIpaddress, string endingIpaddress, string createdBy, CountryID countryID, DateTime expireDate, string note)
        {
            APIResponse response = new APIResponse(true, "success");

            try
            {
                Log.Debug(string.Format("Entering APIBL.AddIPRange({0},{1},{2},{3},{4},{5},{6})", repository.ToString(), beginningIpaddress, endingIpaddress, createdBy, countryID.ToString(), expireDate.ToString(), note));
                if (IPAddress.IsValidIP(beginningIpaddress) && IPAddress.IsValidIP(endingIpaddress))
                {
                    IPAddress beginAddress = IPAddress.StringToIP(beginningIpaddress);
                    IPAddress endAddress = IPAddress.StringToIP(endingIpaddress);
                    Country country = new Country(countryID);
                    DateTime? nullableExpireDate = null;
                    if (expireDate != DateTime.MinValue) nullableExpireDate = expireDate;

                    BlacklistedIPRange range = new BlacklistedIPRange(0, beginAddress, endAddress, createdBy, DateTime.Now, country, nullableExpireDate);
                    range.Notes.Add(new IPRangeNote(0, createdBy, DateTime.Now, note));

                    int newRangeID = repository.SaveRange(range);
                    Log.Debug(string.Format("APIBL.AddIPRange: Range added. ID: {0}", newRangeID.ToString()));
                }
                else
                {
                    Log.Debug("APIBL.AddIPRange: Invalid addresses in range");
                    response.HasAccess = false;
                    response.Message = "Invalid addresses in range";
                }

                return response.ToXML();
            }
            catch (Exception ex)
            {
                response.HasAccess = false;
                response.Message = ex.Message;
                
                BLException exception = new BLException(string.Format("Error in APIBL.AddIPRange({0},{1},{2},{3},{4},{5},{6})", repository.ToString(), beginningIpaddress, endingIpaddress, createdBy, countryID.ToString(), expireDate.ToString(), note), ex);
                Log.Error(exception);

                response.Message = ex.Message;
                return response.ToXML();
            }
            finally
            {
                Log.Debug("Leaving APIBL.AddIPRange");
            }
        }
    }
}
