﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Contracts;
using Spark.IPBlocker.ValueObjects.Exceptions;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Engine.BusinessLogic
{
    public class IPBlockerBL: IDisposable
    {
        public static readonly IPBlockerBL Instance = new IPBlockerBL();
        Logger logger;

        private IPBlockerBL() 
        {
            logger = new Logger(typeof(IPBlockerBL));
        }
        
        #region IDisposable Members

        public void  Dispose()
        {
        }

        #endregion

        public int CheckAccessNoLog(IIPBlockerRepository repository, IPAddress address, int siteID)
        {
            logger.Debug(string.Format("Entering IPBlockerBL.CheckAccess({0}, {1}, {2})", repository.ToString(), address.ToString(), siteID.ToString()));

            if (!address.IsValid())
            {
                logger.Debug("IPBlockerBL.CheckAccess: Invalid Address");
                NameValueCollection errors = new NameValueCollection();
                errors.Add("IPAddress", "Zeroes are not allowed in the IP address.");
                throw new RuleException(errors);
            }

            int rangeID = -1;

            try
            {
                rangeID = repository.IsAddressBlockedNoLog(address, siteID);
                if (rangeID > 0)
                {
                    logger.Debug("IPBlockerBL.CheckAccess: Address blocked");
                }
                else
                {
                    logger.Debug("IPBlockerBL.CheckAccess: Address not blocked");
                }
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.CheckAccess({0}, {1}, {2})", repository.ToString(), address.ToString(), siteID.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.CheckAccess");
            }

            return rangeID;
        }

        public int CheckAccess(IIPBlockerRepository repository, IPAddress address, AccessType accessType)
        {
            return CheckAccess(repository, address, accessType, 0);
        }
            
        public int CheckAccess(IIPBlockerRepository repository, IPAddress address, AccessType accessType, int siteID)
        {
            logger.Debug(string.Format("Entering IPBlockerBL.CheckAccess({0}, {1}, {2}, {3})", repository.ToString(), address.ToString(), accessType.ToString(), siteID.ToString()));

            if (!address.IsValid())
            {
                logger.Debug("IPBlockerBL.CheckAccess: Invalid Address");
                NameValueCollection errors = new NameValueCollection();
                errors.Add("IPAddress", "Zeroes are not allowed in the IP address.");
                throw new RuleException(errors);
            }

            int rangeID = -1;

            try
            {
                rangeID = repository.IsAddressBlocked(address, accessType, siteID);
                if (rangeID > 0)
                {
                    logger.Debug("IPBlockerBL.CheckAccess: Address blocked, logging attempt");
                    //repository.LogAccessAttempt(accessType, rangeID);
                }
                else
                {
                    logger.Debug("IPBlockerBL.CheckAccess: Address not blocked");
                }
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.CheckAccess({0}, {1}, {2}, {3})", repository.ToString(), address.ToString(), accessType.ToString(), siteID.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.CheckAccess");
            }

            return rangeID;
        }

        public IPRange GetRange(IIPBlockerRepository repository, int rangeID)
        {
            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.GetRange({0}, {1})", repository.ToString(), rangeID.ToString()));
                IPRange range = repository.GetRange(rangeID);
                logger.Debug(string.Format("IPBlockerBL.GetRange found range: {0}", (range == null).ToString()));
                return range;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.GetRange({0}, {1})", repository.ToString(), rangeID.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.GetRange");
            }
        }

        public List<IPRange> GetOverlappingRanges(IIPBlockerRepository repository, IPAddress beginningAddress, IPAddress endingAddress)
        {
            List<IPRange> overlapping;
            NameValueCollection errors = new NameValueCollection();

            logger.Debug(string.Format("Entering IPBlockerBL.GetOverlappingRanges({0}, {1}, {2})", repository.ToString(), beginningAddress.ToString(), endingAddress.ToString()));

            if (!beginningAddress.IsValid())
            {
                errors.Add("BeginningAddress", "IP address can not end in zero.");
                logger.Debug("IPBlockerBL.GetOverlappingRanges: Invalid beginning address");
            }
            if (!endingAddress.IsValid())
            {
                errors.Add("EndingAddress", "IP address can not end in zero.");
                logger.Debug("IPBlockerBL.GetOverlappingRanges: Invalid ending address");
            }
            if (endingAddress < beginningAddress)
            {
                errors.Add("IPRange", "The ending address must be greater than the beginning address");
                logger.Debug("IPBlockerBL.GetOverlappingRanges: Ending address greater than beginning address");
            }

            if (errors.Count > 0) throw new RuleException(errors);

            try
            {
                overlapping = repository.GetOverlappingRanges(beginningAddress, endingAddress);
                logger.Debug(string.Format("IPBlockerBL.GetOverlappingRanges found ranges: {0}", (overlapping == null && overlapping.Count > 0).ToString()));
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.GetOverlappingRanges({0}, {1}, {2})", repository.ToString(), beginningAddress.ToString(), endingAddress.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.GetOverlappingRanges");
            }
            
            return overlapping;
        }

        public List<AccessSummary> GetSummariesForRange(IIPBlockerRepository repository, int rangeID)
        {
            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.GetSummariesForRange({0}, {1})", repository.ToString(), rangeID.ToString()));
                
                List<AccessSummary> summaries = repository.GetSummariesForRange(rangeID);
                if (summaries == null)
                {
                    logger.Debug("IPBlockerBL.GetSummariesForRange: No existing summaries, creating blank ones");
                    AccessSummary regSummary = new AccessSummary(AccessType.Registration, 0, 0, 0, DateTime.MinValue);
                    AccessSummary subSummary = new AccessSummary(AccessType.Subscription, 0, 0, 0, DateTime.MinValue);
                    summaries = new List<AccessSummary> { regSummary, subSummary };
                }

                return summaries;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.GetSummariesForRange({0}, {1})", repository.ToString(), rangeID.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.GetSummariesForRange");
            }
        }

        public List<Country> GetAllCountries(IIPBlockerRepository repository)
        {
            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.GetAllCountries({0})", repository.ToString()));
                List<Country> countries = repository.GetAllCountries();
                logger.Debug(string.Format("IPBlockerBL.GetAllCountries returned {0} countries", countries.Count.ToString()));
                return countries;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.GetAllCountries({0})", repository.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.GetAllCountries");
            }
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRanges(IIPBlockerRepository repository)
        {
            List<WhitelistedIPRange> whitelistedRanges;

            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.GetAllWhitelistedIPRanges({0})", repository.ToString()));
                whitelistedRanges = repository.GetAllWhitelistedIPRanges();
                return whitelistedRanges;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.GetAllWhitelistedIPRanges({0})", repository.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.GetAllWhitelistedIPRanges");
            }
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRangesPaged(IIPBlockerRepository repository, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            List<WhitelistedIPRange> whitelistedRanges;

            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.GetAllWhitelistedIPRangesPaged({0},{1},{2},{3},{4})", repository.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()));
                whitelistedRanges = repository.GetAllWhitelistedIPRangesPaged(pageSize, pageNumber, sortField, descending, out totalRowCount);
                logger.Debug(string.Format("IPBlockerBL.GetAllWhitelistedIPRangesPaged returned {0} ranges", whitelistedRanges == null ? "0" : whitelistedRanges.Count.ToString()));
                return whitelistedRanges;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.GetAllWhitelistedIPRangesPaged({0},{1},{2},{3}, {4})", repository.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.GetAllWhitelistedIPRangesPaged");
            }
        }

        public List<IPRangeCountrySummary> GetCountrySummariesPaged(IIPBlockerRepository repository, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            List<IPRangeCountrySummary> countrySummaries;

            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.GetCountrySummariesPaged({0},{1},{2},{3},{4})", repository.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()));
                countrySummaries = repository.GetCountrySummariesPaged(pageSize, pageNumber, sortField, descending, out totalRowCount);
                logger.Debug(string.Format("IPBlockerBL.GetCountrySummariesPaged returned {0} summaries", countrySummaries == null ? "0" : countrySummaries.Count.ToString()));
                return countrySummaries;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.GetCountrySummariesPaged({0},{1},{2},{3},{4})", repository.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.GetCountrySummariesPaged");
            }
        }

        public List<IPRangeSummary> GetRangesByCountryPaged(IIPBlockerRepository repository, CountryID countryID, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            List<IPRangeSummary> summaries;

            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.GetRangesByCountryPaged({0},{1},{2},{3},{4},{5})", repository.ToString(), countryID.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()));
                summaries = repository.GetRangesByCountryPaged(countryID, pageSize, pageNumber, sortField, descending, out totalRowCount);
                logger.Debug(string.Format("IPBlockerBL.GetRangesByCountryPaged returned {0} summaries", summaries == null ? "0" : summaries.Count.ToString()));
                return summaries;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.GetRangesByCountryPaged({0},{1},{2},{3},{4},{5})", repository.ToString(), countryID.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.GetRangesByCountryPaged");
            }
        }

        public List<IPRangeSummary> GetExpiringRangeSummariesPaged(IIPBlockerRepository repository, DateTime beginDate, DateTime endDate, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            List<IPRangeSummary> summaries;

            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.GetExpiringRangeSummariesPaged({0},{1},{2},{3},{4},{5},{6})", repository.ToString(), beginDate.ToString(), endDate.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()));

                if (beginDate > endDate)
                {
                    NameValueCollection errors = new NameValueCollection();
                    errors.Add("BeginDate", "End Date must be greater than/equal to Begin Date");
                    logger.Debug("IPBlockerBL.GetExpiringRangeSummariesPaged: Begin Date > End Date");
                    throw new RuleException(errors);
                }

                summaries = repository.GetExpiringRangeSummariesPaged(beginDate, endDate, pageSize, pageNumber, sortField, descending, out totalRowCount);
                logger.Debug(string.Format("IPBlockerBL.GetExpiringRangeSummariesPaged returned {0} summaries", summaries == null ? "0" : summaries.Count.ToString()));
                return summaries;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.GetExpiringRangeSummariesPaged({0},{1},{2},{3},{4},{5},{6})", repository.ToString(), beginDate.ToString(), endDate.ToString(), pageSize.ToString(), pageNumber.ToString(), sortField, descending.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.GetExpiringRangeSummariesPaged");
            }
        }

        public void AddRangeNote(IIPBlockerRepository repository, int rangeID, string createdBy, string note)
        {
            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.AddRangeNote({0},{1},{2},{3})", repository.ToString(), rangeID.ToString(), createdBy, note));
                repository.AddRangeNote(rangeID, createdBy, note);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.AddRangeNote({0},{1},{2},{3})", repository.ToString(), rangeID.ToString(), createdBy, note), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.AddRangeNote");
            }
        }

        public void UpdateExpirationDate(IIPBlockerRepository repository, int rangeID, DateTime? expireDate)
        {
            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.UpdateExpirationDate({0},{1},{2})", repository.ToString(), rangeID.ToString(), expireDate.HasValue ? expireDate.Value.ToString() : "null"));
                repository.UpdateExpirationDate(rangeID, expireDate);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.UpdateExpirationDate({0},{1},{2})", repository.ToString(), rangeID.ToString(), expireDate.HasValue ? expireDate.Value.ToString() : "null"), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.UpdateExpirationDate");
            }
        }


        public int SaveRange(IIPBlockerRepository repository, IPRange range)
        {
            return SaveRange(repository, range, null, null);
        }

        public int SaveRange(IIPBlockerRepository repository, IPRange range, List<int> rangesToDelete, List<int> rangesToKeep)
        {
            int returnID = 0;
            
            NameValueCollection errors = new NameValueCollection();

            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.SaveRange({0},{1},{2},{3})", repository.ToString(), range.ToString(), rangesToDelete == null ? "null" : rangesToDelete.ToString(), rangesToKeep == null ? "null" : rangesToKeep.ToString()));
                
                if (!range.BeginningAddress.IsValid())
                {
                    errors.Add("BeginningAddress", "Zeroes are not allowed at the end of an IP address.");
                    logger.Debug("IPBlockerBL.SaveRange: Zero in beginning address");
                }
                if (!range.EndingAddress.IsValid())
                {
                    errors.Add("EndingAddress", "Zeroes are not allowed at the end of an IP address.");
                    logger.Debug("IPBlockerBL.SaveRange: Zero in ending address");
                }
                if (range.EndingAddress < range.BeginningAddress)
                {
                    errors.Add("IPRange", "The ending address must be greater than the beginning address");
                    logger.Debug("IPBlockerBL.SaveRange: beginning address > ending address");
                }

                if (errors.Count > 0) throw new RuleException(errors);

                if (range is BlacklistedIPRange)
                {
                    logger.Debug("IPBlockerBL.SaveRange: range is blacklisted range");
                    //Make sure that the user is handling all overlapping ranges, else throw an exception
                    List<IPRange> unhandledRanges = ReconcileOverlappingRanges(repository, range, rangesToDelete, rangesToKeep);
                    logger.Debug("IPBlockerBL.SaveRange: finished reconciling overlapping ranges");
                    if (unhandledRanges != null)
                    {
                        throw new OverlappingRangesNotHandledException(unhandledRanges);
                    }
                }
                else
                {
                    logger.Debug("IPBlockerBL.SaveRange: range is whitelisted range");
                }

                if (rangesToDelete != null)
                {
                    logger.Debug("IPBlockerBL.SaveRange: deleting overlapping ranges");
                    //delete the overlapping ranges requested
                    foreach (int rangeToDelete in rangesToDelete)
                    {
                        repository.DeleteRange(rangeToDelete);
                    }
                }

                if (range.ID <= 0)
                {
                    logger.Debug("IPBlockerBL.SaveRange: new range, performing initial save");
                    //this is a new range, so let's save it and get the ID back
                    returnID = repository.SaveRange(range);
                    List<IPRangeNote> tempNotes = range.Notes;

                    if (range is BlacklistedIPRange)
                    {
                        range = new BlacklistedIPRange(returnID, range.BeginningAddress, range.EndingAddress, range.CreatedBy, DateTime.Now, range.Country, (range as BlacklistedIPRange).ExpireDate);
                    }
                    else
                    {
                        range = new WhitelistedIPRange(returnID, range.BeginningAddress, range.EndingAddress, range.CreatedBy, DateTime.Now, range.Country, (range as WhitelistedIPRange).Name);
                    }

                    range.Notes = tempNotes;
                    logger.Debug("IPBlockerBL.SaveRange: saved new range and constructed new object");
                }
                else
                {
                    logger.Debug("IPBlockerBL.SaveRange: existing range, performing save");
                    repository.SaveRange(range);
                }

                if (range.Notes != null)
                {
                    foreach (IPRangeNote note in range.Notes)
                    {
                        if (note.ID <= 0)
                        {
                            logger.Debug("IPBlockerBL.SaveRange: saving new note");
                            repository.AddRangeNote(range.ID, note.CreatedBy, note.Note);
                        }
                    }
                }

                return range.ID;
            }
            catch (OverlappingRangesNotHandledException oex)
            {
                BLException exception = new BLException(string.Format("Overlapping Ranges not handled exception in IPBlockerBL.SaveRange({0},{1},{2},{3})", repository.ToString(), range.ToString(), rangesToDelete == null ? "null" : rangesToDelete.ToString(), rangesToKeep == null ? "null" : rangesToKeep.ToString()), oex);
                throw oex;
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.SaveRange({0},{1},{2},{3})", repository.ToString(), range.ToString(), rangesToDelete == null ? "null" : rangesToDelete.ToString(), rangesToKeep == null ? "null" : rangesToKeep.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.SaveRange");
            }
        }

        public void DeleteRange(IIPBlockerRepository repository, int rangeID)
        {
            try
            {
                logger.Debug(string.Format("Entering IPBlockerBL.DeleteRange({0}, {1}))", repository.ToString(), rangeID.ToString()));
                repository.DeleteRange(rangeID);
            }
            catch (Exception ex)
            {
                BLException exception = new BLException(string.Format("Error in IPBlockerBL.DeleteRange({0}, {1}))", repository.ToString(), rangeID.ToString()), ex);
                logger.Error(exception);
                throw exception;
            }
            finally
            {
                logger.Debug("Leaving IPBlockerBL.DeleteRange");
            }
        }

        private List<IPRange> ReconcileOverlappingRanges(IIPBlockerRepository repository, IPRange range, List<int> rangesToDelete, List<int> rangesToKeep)
        {
            List<IPRange> unhandledRanges = new List<IPRange>();
            if (rangesToDelete == null) rangesToDelete = new List<int>();
            if (rangesToKeep == null) rangesToKeep = new List<int>();
            
            List<IPRange> overlappingRanges = repository.GetOverlappingRanges(range.BeginningAddress, range.EndingAddress);

            foreach (IPRange overlapRange in overlappingRanges)
            {
                if (overlapRange is BlacklistedIPRange && !rangesToDelete.Contains(overlapRange.ID) && !rangesToKeep.Contains(overlapRange.ID))
                {
                    unhandledRanges.Add(overlapRange);
                }
            }

            return unhandledRanges.Count == 0 ? null : unhandledRanges;
        }
    

}
}
