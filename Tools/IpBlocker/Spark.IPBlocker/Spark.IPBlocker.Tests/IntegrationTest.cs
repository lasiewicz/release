﻿#region

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using Spark.Common.Adapter;
using Spark.IPBlocker.Tests.IPBlockerServiceReference1;

#endregion

namespace Spark.IPBlocker.Tests
{
    [TestClass]
    public class IntegrationTest
    {
        //private  const string Host = "http://localhost:9998";
        private  const string Host = "http://lastgweb301:9998";

        [TestMethod]
        public void CheckAccessViaWebService()
        {
            var ipAddress = new IPBlockerServiceReference1.IPAddress
            {
                FirstOctet = 64,
                SecondOctet = 16,
                ThirdOctet = 85,
                FourthOctet = 39
            };
            var client = new IPBlockerServiceClient();
            var result = client.CheckAccessWithSite(ipAddress, AccessType.Login, 103);
            // 0 means not blocked
            Assert.IsTrue(result == 0);
            client.Close();
        }

        [TestMethod]
        public void CheckAccessJsonViaWebService()
        {
            var client = new RestClient(Host);
 
            var request = new RestRequest("IPBlockerAPIService.svc/CheckAccessJSON", Method.GET);
            //request.AddParameter("name", "value"); // adds to POST or URL querystring based on Method
            request.AddParameter("ipaddress", "192.168.1.1");
            request.AddParameter("accessType", "Login");
            request.AddParameter("siteID", "103"); 


            // easily add HTTP Headers
            //request.AddHeader("header", "value");

            // execute the request
            var response = client.Execute(request);
            Assert.IsNotNull(response);
            var content = response.Content; 
            Assert.IsNotNull(content);
            Assert.IsTrue(content.IndexOf("success", StringComparison.Ordinal) > 0);
        }


        [TestMethod]
        public void CheckAccessJsonViaWebServiceWithIPV6()
        {
            var client = new RestClient(Host);

            var request = new RestRequest("IPBlockerAPIService.svc/CheckAccessJSON", Method.GET);
            //request.AddParameter("name", "value"); // adds to POST or URL querystring based on Method
            request.AddParameter("ipaddress", "FE80:0000:0000:0000:0202:B3FF:FE1E:8329");
            request.AddParameter("accessType", "Login");
            request.AddParameter("siteID", "103");


            // easily add HTTP Headers
            //request.AddHeader("header", "value");

            // execute the request
            var response = client.Execute(request);
            Assert.IsNotNull(response);
            var content = response.Content;
            Assert.IsNotNull(content);
            Assert.IsTrue(content.IndexOf("success", StringComparison.Ordinal) > 0);
        }

        [TestMethod]
        public void CheckAccessNoLogJsonViaWebServiceWithIPV6()
        {
            var client = new RestClient(Host);

            var request = new RestRequest("IPBlockerAPIService.svc/CheckAccessNoLogJSON", Method.GET);
            //request.AddParameter("name", "value"); // adds to POST or URL querystring based on Method
            request.AddParameter("ipaddress", "FE80:0000:0000:0000:0202:B3FF:FE1E:8329");
            //request.AddParameter("accessType", "Login");
            request.AddParameter("siteID", "103");


            // easily add HTTP Headers
            //request.AddHeader("header", "value");

            // execute the request
            var response = client.Execute(request);
            Assert.IsNotNull(response);
            var content = response.Content;
            Assert.IsNotNull(content);
            Assert.IsTrue(content.IndexOf("success", StringComparison.Ordinal) > 0);
        }

        /// <summary>
        ///     This tests what Member Service does internally on registration.
        /// </summary>
        [TestMethod]
        public void CheckAccessRegistrationViaServiceAdapter()
        {
            var intAddress = BitConverter.ToInt32(System.Net.IPAddress.Parse("192.168.3.132").GetAddressBytes(), 0);
            var ipAddressString = new System.Net.IPAddress(BitConverter.GetBytes(intAddress)).ToString();
            var allow = IPBlockerServiceAdapter.Instance.CheckIPAccess(ipAddressString, 103,
                Common.IPBlockerAccessType.Registration);

            Assert.IsTrue(allow);
        }
    }
}