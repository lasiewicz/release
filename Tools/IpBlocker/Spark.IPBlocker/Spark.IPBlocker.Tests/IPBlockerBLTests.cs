﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Contracts;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.IPBlocker.ValueObjects.Exceptions;
using Spark.IPBlocker.Engine.BusinessLogic;

namespace Spark.IPBlocker.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class IPBlockerBLTests
    {
        public IPBlockerBLTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CheckAccess_Validates_Arguments()
        {
            IIPBlockerRepository repository = MockRepository();
            IPAddress address = new IPAddress(255, 221, 192, 0);
            int errorCount = 0;
            string errorName = string.Empty;
            int result;

            try
            {
                result = IPBlockerBL.Instance.CheckAccess(repository, address, AccessType.Registration);
            }
            catch(RuleException ex)
            {
                errorCount = ex.Errors.Count;
                errorName = ex.Errors.GetKey(0);
            }

            Assert.AreEqual(errorCount, 1);
            Assert.AreEqual(errorName, "IPAddress");
           
        }

        [TestMethod]
        public void CheckAccess_Returns_Negative_When_IP_Not_In_Repository()
        {
            IIPBlockerRepository repository = MockRepository();
            IPAddress address = new IPAddress(1, 1, 1, 1);
            int result;

            result = IPBlockerBL.Instance.CheckAccess(repository, address, AccessType.Registration);
            Assert.IsTrue(result <= 0);
        }

        [TestMethod]
        public void CheckAccess_Returns_RangeID_When_IP_In_Repository()
        {
            IIPBlockerRepository repository = MockRepository();
            IPAddress address = new IPAddress(224, 1, 24, 100);
            int result;

            result = IPBlockerBL.Instance.CheckAccess(repository, address, AccessType.Registration);
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void CheckAccess_Logs_RangeID_When_IP_In_Repository()
        {
            IIPBlockerRepository repository = MockRepository();

            IPAddress address = new IPAddress(192, 168, 122, 5);

            List<AccessSummary> accessSummaries = IPBlockerBL.Instance.GetSummariesForRange(repository, 1);
            AccessSummary regSummary = (from AS in accessSummaries where AS.AccessType == AccessType.Registration select AS).First();
            int originalSummaryTotal = regSummary.TotalAttempts;
            int originalSummaryNinetyDays = regSummary.NinetyDayAttempts;

            int result = IPBlockerBL.Instance.CheckAccess(repository, address, AccessType.Registration);

            List<AccessSummary> accessSummariesAfter = IPBlockerBL.Instance.GetSummariesForRange(repository, 1);
            AccessSummary regSummaryAfter = (from AS in accessSummaries where AS.AccessType == AccessType.Registration select AS).First();
            int afterSummaryTotal = regSummaryAfter.TotalAttempts;
            int afterSummaryNinetyDays = regSummaryAfter.NinetyDayAttempts;

            Assert.AreEqual(originalSummaryNinetyDays + 1, afterSummaryNinetyDays);
            Assert.AreEqual(originalSummaryTotal + 1, afterSummaryTotal);
        }

        [TestMethod]
        public void GetRange_Returns_Range_When_Given_Valid_ID()
        {
            IIPBlockerRepository repository = MockRepository();
            IPRange range = IPBlockerBL.Instance.GetRange(repository, 1);

            Assert.IsNotNull(range);
        }

        [TestMethod]
        public void GetRange_Does_Not_Return_Range_When_Given_Invalid_ID()
        {
            IIPBlockerRepository repository = MockRepository();
            IPRange range = IPBlockerBL.Instance.GetRange(repository, 455);

            Assert.IsNull(range);
        }

        [TestMethod]
        public void GetSummariesForRange_Returns_Summaries_When_Given_Valid_ID()
        {
            IIPBlockerRepository repository = MockRepository();
            List<AccessSummary> summaries = IPBlockerBL.Instance.GetSummariesForRange(repository, 1);

            Assert.IsNotNull(summaries);
            Assert.AreEqual(summaries.Count, 2);
            Assert.AreEqual((from AS in summaries where AS.AccessType == AccessType.Registration select AS).Count() , 1);
            Assert.AreEqual((from AS in summaries where AS.AccessType == AccessType.Subscription select AS).Count(), 1);
            
        }

        [TestMethod]
        public void GetSummariesForRange_Does_Not_Return_Sumamries_When_Given_Invalid_ID()
        {
            IIPBlockerRepository repository = MockRepository();
            List<AccessSummary> summaries = IPBlockerBL.Instance.GetSummariesForRange(repository, 455);

            Assert.IsNull(summaries);
        }

        [TestMethod]
        public void GetOverlappingRanges_Validates_IP_Range()
        {
            IIPBlockerRepository repository = MockRepository();
            IPAddress beginningAddress = new IPAddress(255, 221, 192, 0);
            IPAddress endingAddress = new IPAddress(255, 220, 192, 0);
            List<IPRange> ranges;

            int errorCount = 0;
            string errorName = string.Empty;

            try
            {
                ranges = IPBlockerBL.Instance.GetOverlappingRanges(repository, beginningAddress, endingAddress);
            }
            catch (RuleException ex)
            {
                errorCount = ex.Errors.Count;
            }
            
            //should raise three errors, one each for the IP addresses containing zeros and the other
            //one for the ending address being greater than the beginning address
            Assert.AreEqual(errorCount, 3);
        }

        [TestMethod]
        public void GetOverlappingRanges_Returns_Overlapping_Ranges()
        {
            IIPBlockerRepository repository = MockRepository();
            IPAddress beginningBlackAddress = new IPAddress(192, 168, 122, 14);
            IPAddress endingBlackAddress = new IPAddress(192, 168, 122, 25);
            IPAddress beginningWhiteAddress = new IPAddress(244, 167, 23, 58);
            IPAddress endingWhiteAddress = new IPAddress(244, 167, 23, 64);
            List<IPRange> blacklistedRanges;
            List<IPRange> whitelistedRanges;

            blacklistedRanges = IPBlockerBL.Instance.GetOverlappingRanges(repository, beginningBlackAddress, endingBlackAddress);
            whitelistedRanges = IPBlockerBL.Instance.GetOverlappingRanges(repository, beginningWhiteAddress, endingWhiteAddress);

            Assert.AreEqual(blacklistedRanges.Count, 1);
            Assert.AreEqual(whitelistedRanges.Count, 1);
        }

        [TestMethod]
        public void GetOverlappingRanges_Does_Not_Return_Overlapping_Ranges_When_It_Shouldnt()
        {
            IIPBlockerRepository repository = MockRepository();
            IPAddress beginningBlackAddress = new IPAddress(192, 168, 121, 1);
            IPAddress endingBlackAddress = new IPAddress(192, 168, 121, 56);
            IPAddress beginningWhiteAddress = new IPAddress(22, 23, 12, 167);
            IPAddress endingWhiteAddress = new IPAddress(22, 23, 12, 170);
            List<IPRange> blacklistedRanges;
            List<IPRange> whitelistedRanges;

            blacklistedRanges = IPBlockerBL.Instance.GetOverlappingRanges(repository, beginningBlackAddress, endingBlackAddress);
            whitelistedRanges = IPBlockerBL.Instance.GetOverlappingRanges(repository, beginningWhiteAddress, endingWhiteAddress);

            Assert.AreEqual(blacklistedRanges.Count, 0);
            Assert.AreEqual(whitelistedRanges.Count, 0);
        }

        [TestMethod]
        public void GetAllWhitelistedIPRanges_Returns_All_Whitelisted_Ranges()
        {
            IIPBlockerRepository repository = MockRepository();

            List<WhitelistedIPRange> whitelistedRanges = IPBlockerBL.Instance.GetAllWhitelistedIPRanges(repository);

            Assert.AreEqual(whitelistedRanges.Count, 2);
        }

        [TestMethod]
        public void GetExpiringRanges_Validates_Dates()
        {
            IIPBlockerRepository repository = MockRepository();
            DateTime beginDate = new DateTime(2010, 4, 21);
            DateTime endDate = new DateTime(2010, 4, 19);
            int errorCount = 0;
            string errorName = string.Empty;

            try
            {
                //List<BlacklistedIPRange> expiringRanges = IPBlockerBL.Instance.GetExpiringRanges(repository, beginDate, endDate);
            }
            catch (RuleException ex)
            {
                errorCount = ex.Errors.Count;
                errorName = ex.Errors.GetKey(0);
            }

            Assert.AreEqual(errorCount, 1);
            Assert.AreEqual(errorName, "BeginDate");
        }

        [TestMethod]
        public void GetExpiringRanges_Returns_Expiring_Ranges()
        {
            IIPBlockerRepository repository = MockRepository();
            DateTime beginDate = new DateTime(2010, 4, 19);
            DateTime endDate = new DateTime(2010, 4, 25);

            //List<BlacklistedIPRange> expiringRanges = IPBlockerBL.Instance.GetExpiringRanges(repository, beginDate, endDate);

            //Assert.AreEqual(expiringRanges.Count, 1);
        }

        [TestMethod]
        public void AddRangeNote_Adds_Note()
        {
            IIPBlockerRepository repository = MockRepository();
            int rangeID = 1;
            string note = "this is a test note";
            BlacklistedIPRange range;

            IPBlockerBL.Instance.AddRangeNote(repository, rangeID, "matt m", note);
            range = IPBlockerBL.Instance.GetRange(repository, rangeID) as BlacklistedIPRange;

            Assert.AreEqual(note, range.Notes[0].Note);
        }

        [TestMethod]
        public void DeleteRange_Deletes_Note()
        {
            IIPBlockerRepository repository = MockRepository();
            int rangeID = 1;
            BlacklistedIPRange range;

            IPBlockerBL.Instance.DeleteRange(repository, rangeID);
            range = IPBlockerBL.Instance.GetRange(repository, rangeID) as BlacklistedIPRange;

            Assert.IsNull(range);
        }

        [TestMethod]
        public void SaveRange_Saves_New_Ranges()
        {
            IIPBlockerRepository repository = MockRepository();

            //new blacklisted range no notes
            BlacklistedIPRange newBlackRange = new BlacklistedIPRange(0, new IPAddress(15, 15, 15, 15), new IPAddress(15, 15, 15, 225), "matt m", new DateTime(2010, 4, 5), new Country(CountryID.USA), null);
            //new whitelisted range with note
            WhitelistedIPRange newWhiteRange = new WhitelistedIPRange(0, new IPAddress(16, 16, 16, 16), new IPAddress(16, 16, 16, 225), "matt m", new DateTime(2010, 4, 5), new Country(CountryID.USA), "Matt1");
            newWhiteRange.Notes.Add(new IPRangeNote(0, "matt m", DateTime.Now, "This is a test note"));

            int newBlackRangeID = IPBlockerBL.Instance.SaveRange(repository, newBlackRange);
            int newWhiteRangeID = IPBlockerBL.Instance.SaveRange(repository, newWhiteRange);

            BlacklistedIPRange newBlackRange2 = IPBlockerBL.Instance.GetRange(repository, newBlackRangeID) as BlacklistedIPRange;
            WhitelistedIPRange newWhiteRange2 = IPBlockerBL.Instance.GetRange(repository, newWhiteRangeID) as WhitelistedIPRange;

            Assert.IsNotNull(newBlackRange2);
            Assert.AreEqual(newBlackRange2.BeginningAddress, new IPAddress(15, 15, 15, 15));
            Assert.AreEqual(newBlackRange2.EndingAddress, new IPAddress(15, 15, 15, 225));
            Assert.AreEqual(newBlackRange2.CreatedBy, "matt m");
            Assert.AreEqual(newBlackRange2.Country.ID, CountryID.USA);
            Assert.IsNull(newBlackRange2.ExpireDate);

            Assert.IsNotNull(newWhiteRange2);
            Assert.AreEqual(newWhiteRange2.BeginningAddress, new IPAddress(16, 16, 16, 16));
            Assert.AreEqual(newWhiteRange2.EndingAddress, new IPAddress(16, 16, 16, 225));
            Assert.AreEqual(newWhiteRange2.CreatedBy, "matt m");
            Assert.AreEqual(newWhiteRange2.Country.ID, CountryID.USA);
            Assert.AreEqual(newWhiteRange2.Name, "Matt1");
            Assert.AreEqual(newWhiteRange2.Notes.Count, 1);
            Assert.AreEqual(newWhiteRange2.Notes[0].Note, "This is a test note");
        }

        [TestMethod]
        public void SaveRange_Catches_Overlapping_Ranges()
        {
            IIPBlockerRepository repository = MockRepository();
            bool throwsOverlappingRangesNotHandledException = false;
            List<IPRange> unhandledRanges = null;

            //new blacklisted range no notes
            BlacklistedIPRange newBlackRange = new BlacklistedIPRange(0, new IPAddress(192, 168, 122, 4), new IPAddress(192, 168, 122, 7), "matt m", new DateTime(2010, 4, 5), new Country(CountryID.USA), null);

            try
            {
                IPBlockerBL.Instance.SaveRange(repository, newBlackRange);
            }
            catch (OverlappingRangesNotHandledException ex)
            {
                throwsOverlappingRangesNotHandledException = true;
                unhandledRanges = ex.UnhandledRanges;
            }

            Assert.IsTrue(throwsOverlappingRangesNotHandledException);
            Assert.AreEqual(unhandledRanges.Count, 1);
        }

        [TestMethod]
        public void SaveRange_Handles_Overlapping_Range_Deletes()
        {
            IIPBlockerRepository repository = MockRepository();
            List<int> rangesToDelete = new List<int>();
            rangesToDelete.Add(1);

            BlacklistedIPRange newBlackRange = new BlacklistedIPRange(0, new IPAddress(192, 168, 122, 4), new IPAddress(192, 168, 122, 7), "matt m", new DateTime(2010, 4, 5), new Country(CountryID.USA), null);
            
            IPBlockerBL.Instance.SaveRange(repository, newBlackRange, rangesToDelete, null);
            BlacklistedIPRange deletedRange = IPBlockerBL.Instance.GetRange(repository, 1) as BlacklistedIPRange;

            Assert.IsNull(deletedRange);
        }

        [TestMethod]
        public void SaveRange_Validates_Arguments()
        {
            IIPBlockerRepository repository = MockRepository();
            int NumErrors = 0;

            //new blacklisted range no notes
            BlacklistedIPRange newBlackRange = new BlacklistedIPRange(0, new IPAddress(192, 168, 122, 0), new IPAddress(192, 167, 122, 0), "matt m", new DateTime(2010, 4, 5), new Country(CountryID.USA), null);

            try
            {
                IPBlockerBL.Instance.SaveRange(repository, newBlackRange);
            }
            catch (RuleException ex)
            {
                NumErrors = ex.Errors.Count;
            }

            Assert.AreEqual(NumErrors, 3);
        }

        [TestMethod]
        public void SaveRange_Saves_New_Ranges_Real()
        {
            IIPBlockerRepository repository = new Spark.IPBlocker.Engine.DataAccess.IPBlockerRepository();

            //new blacklisted range no notes
            BlacklistedIPRange newBlackRange = new BlacklistedIPRange(0, new IPAddress(15, 15, 15, 15), new IPAddress(15, 15, 15, 225), "matt m", new DateTime(2010, 4, 5), new Country(CountryID.USA), null);
            //new whitelisted range with note

            IPBlockerBL.Instance.SaveRange(repository, newBlackRange);
           
        }

        [TestMethod]
        public void testy()
        {
            Spark.IPBlocker.Engine.ServiceManagers.IPBlockerSM sm = new Spark.IPBlocker.Engine.ServiceManagers.IPBlockerSM();
            Spark.IPBlocker.Service.IPBlockerService s = new Spark.IPBlocker.Service.IPBlockerService();
            List<Country> countries = s.GetAllCountries();
            int x = 1;
        }

        private static IIPBlockerRepository MockRepository()
        {
            List<BlacklistedIPRange> blacklistedIPs = new List<BlacklistedIPRange>();
            List<WhitelistedIPRange> whitelistedIPs = new List<WhitelistedIPRange>();
            Dictionary<int, List<AccessSummary>> accessDictionary = new Dictionary<int, List<AccessSummary>>();
            int maxIPRangeID;
            
            Country usa = new Country(CountryID.USA, "USA");
            Country canada = new Country(CountryID.Canada, "Canada");

            BlacklistedIPRange black1 = new BlacklistedIPRange(1, new IPAddress(192, 168, 122, 1), new IPAddress(192, 168, 122, 24),
                "matt m", new DateTime(2010, 4, 2), canada, null);
            BlacklistedIPRange black2 = new BlacklistedIPRange(2, new IPAddress(224, 1, 24, 22), new IPAddress(224, 1, 24, 224),
                "matt m", new DateTime(2010, 4, 2), canada, null);
            BlacklistedIPRange black3 = new BlacklistedIPRange(3, new IPAddress(13, 56, 12, 1), new IPAddress(13, 57, 12, 22),
                "matt m", new DateTime(2010, 4, 2), usa, new DateTime(2010, 4, 22));

            black2.Notes.Add(new IPRangeNote(1, "matt m", DateTime.Now, "This is my first note"));
            black2.Notes.Add(new IPRangeNote(2, "matt m", DateTime.Now, "This is my second note"));

            blacklistedIPs.Add(black1);
            blacklistedIPs.Add(black2);
            blacklistedIPs.Add(black3);


            WhitelistedIPRange white1 = new WhitelistedIPRange(4, new IPAddress(22, 23, 12, 1), new IPAddress(22, 23, 12, 166),
                "matt m", new DateTime(2010, 4, 2), usa, "matt 1");
            WhitelistedIPRange white2 = new WhitelistedIPRange(5, new IPAddress(244, 167, 23, 56), new IPAddress(244, 167, 23, 192),
                "matt m", new DateTime(2010, 4, 2), usa, "matt 2");
            white2.Notes.Add(new IPRangeNote(3, "matt m", DateTime.Now, "This is my first whitelist note"));

            whitelistedIPs.Add(white1);
            whitelistedIPs.Add(white2);

            AccessSummary summary1 = new AccessSummary(AccessType.Registration, 100, 20, 1,new DateTime(2010, 4, 1));
            AccessSummary summary2 = new AccessSummary(AccessType.Subscription, 244, 36, 1,new DateTime(2010, 4, 1));
            AccessSummary summary3 = new AccessSummary(AccessType.Registration, 500, 220, 1,new DateTime(2010, 4, 1));
            AccessSummary summary4 = new AccessSummary(AccessType.Subscription, 400, 200, 1,new DateTime(2010, 4, 1));

            List<AccessSummary> accessList1 = new List<AccessSummary>();
            accessList1.Add(summary1);
            accessList1.Add(summary2);

            List<AccessSummary> accessList2 = new List<AccessSummary>();
            accessList2.Add(summary3);
            
            List<AccessSummary> accessList3 = new List<AccessSummary>();
            accessList3.Add(summary4);

            accessDictionary.Add(1, accessList1);
            accessDictionary.Add(2, accessList2);
            accessDictionary.Add(3, accessList3);
            maxIPRangeID = 5;

            return new TestIPBlockerRepository(blacklistedIPs, whitelistedIPs, accessDictionary, maxIPRangeID);

        }

    }
}
