﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Contracts;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Tests
{
    public class TestIPBlockerRepository: IIPBlockerRepository
    {
        private List<BlacklistedIPRange> _blacklistedIPs = new List<BlacklistedIPRange>();
        private List<WhitelistedIPRange> _whitelistedIPs = new List<WhitelistedIPRange>();
        private Dictionary<int, List<AccessSummary>> _accessDictionary = new Dictionary<int, List<AccessSummary>>();
        private int _maxIPRangeID;

        public TestIPBlockerRepository(List<BlacklistedIPRange> blacklistedIPs, List<WhitelistedIPRange> whitelistedIPs, Dictionary<int, List<AccessSummary>> accessDictionary, int maxIPRangeID)
        {
            _blacklistedIPs = blacklistedIPs;
            _whitelistedIPs = whitelistedIPs;
            _accessDictionary = accessDictionary;
            _maxIPRangeID = maxIPRangeID;
        }

        public int IsAddressBlocked(IPAddress address)
        {
            int returnValue = -1;

            foreach(BlacklistedIPRange range in _blacklistedIPs)
            {
                if (address >= range.BeginningAddress && address <= range.EndingAddress)
                {
                    returnValue = range.ID;
                    break;
                }
            }

            return returnValue;
        }

        public int IsAddressBlockedNoLog(IPAddress address, int siteID)
        {
            //not implemented
            return 0;
        }

        public int IsAddressBlocked(IPAddress address, AccessType accessType, int siteID)
        {
            //not implemented
            return 0;
        }

        public IPRange GetRange(int rangeID)
        {
            if ((rangeID > _blacklistedIPs.Count + _whitelistedIPs.Count) || rangeID <= 0) return null;

            BlacklistedIPRange blackRange = (from bip in _blacklistedIPs where bip.ID == rangeID select bip).FirstOrDefault();
            if (blackRange != null) return blackRange;

            WhitelistedIPRange whiteRange = (from wip in _whitelistedIPs where wip.ID == rangeID select wip).FirstOrDefault();
            if (whiteRange != null) return whiteRange;

            return null;
        }

        public List<IPRange> GetOverlappingRanges(IPAddress beginningAddress, IPAddress endingAddress)
        {
            List<IPRange> ranges = new List<IPRange>();
            
            foreach (BlacklistedIPRange blackRange in _blacklistedIPs)
            {
                if ((beginningAddress >= blackRange.BeginningAddress && beginningAddress <= blackRange.EndingAddress) || (endingAddress >= blackRange.BeginningAddress && endingAddress <= blackRange.EndingAddress))
                {
                    ranges.Add(blackRange);
                }
            }

            foreach (WhitelistedIPRange whiteRange in _whitelistedIPs)
            {
                if ((beginningAddress >= whiteRange.BeginningAddress && beginningAddress <= whiteRange.EndingAddress) || (endingAddress >= whiteRange.BeginningAddress && endingAddress <= whiteRange.EndingAddress))
                {
                    ranges.Add(whiteRange);
                }
            }

            return ranges;
        }

        public List<AccessSummary> GetSummariesForRange(int rangeID)
        {
            if(_accessDictionary.ContainsKey(rangeID))
            {
                return _accessDictionary[rangeID];
            }
            else
            {
                return null;
            }
        }

        public List<IPRangeSummary> GetRangeSummaryForCountry(CountryID ID)
        {
            List<BlacklistedIPRange> blacklisted = (from bip in _blacklistedIPs where bip.Country.ID == ID select bip).ToList();
            List<IPRangeSummary> summaries = new List<IPRangeSummary>();

            foreach (BlacklistedIPRange range in blacklisted)
            {
                if (_accessDictionary.ContainsKey(range.ID))
                {
                    IPRangeSummary summary = new IPRangeSummary(range);
                    summary.Summaries = _accessDictionary[range.ID];
                    summaries.Add(summary);
                }
            }

            return summaries;
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRanges()
        {
            return _whitelistedIPs;
        }

        public List<WhitelistedIPRange> GetAllWhitelistedIPRangesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            totalRowCount = _whitelistedIPs.Count;
            return _whitelistedIPs;
        }

        public List<BlacklistedIPRange> GetExpiringRanges(DateTime beginDate, DateTime endDate)
        {
            return (from ranges in _blacklistedIPs where ranges.ExpireDate.HasValue select ranges).ToList();
        }

        public void AddRangeNote(int rangeID, string createdBy, string note)
        {
            BlacklistedIPRange blacklisted = (from bip in _blacklistedIPs where bip.ID == rangeID select bip).FirstOrDefault();
            if (blacklisted != null)
            {
                blacklisted.Notes.Add(new IPRangeNote(0, createdBy, DateTime.Now, note));
                return;
            }

            WhitelistedIPRange whitelisted = (from wip in _whitelistedIPs where wip.ID == rangeID select wip).FirstOrDefault();
            if (whitelisted != null)
            {
                whitelisted.Notes.Add(new IPRangeNote(0, createdBy, DateTime.Now, note));
                return;
            }
        }

        public void LogAccessAttempt(AccessType accessType, int rangeID)
        {
             AccessSummary summary;

            if (_accessDictionary.ContainsKey(rangeID))
            {
                summary = (from sum in _accessDictionary[rangeID] where sum.AccessType == accessType select sum).FirstOrDefault();
                if(summary == null)
                {
                    summary = new AccessSummary(accessType, 1, 1, 1,DateTime.Now);
                }
                else
                {
                    AccessSummary newSummary = new AccessSummary(accessType, summary.TotalAttempts + 1, summary.NinetyDayAttempts + 1, 1, DateTime.Now);
                    _accessDictionary[rangeID].Remove(summary);
                    _accessDictionary[rangeID].Add(newSummary);
                }
            }
            else
            {
                summary = new AccessSummary(accessType, 1, 1, 1, DateTime.Now);
                List<AccessSummary> summaries = new List<AccessSummary>();
                summaries.Add(summary);
                _accessDictionary.Add(rangeID, summaries);
            }
        }

        public int SaveRange(IPRange range)
        {
            int newID = 0;

            if (range.ID <= 0)
            {
                _maxIPRangeID++;
                newID = _maxIPRangeID;

                if (range is BlacklistedIPRange)
                {
                    BlacklistedIPRange newBlackRange = new BlacklistedIPRange(newID, range.BeginningAddress, range.EndingAddress, range.CreatedBy, range.CreatedDate, range.Country, (range as BlacklistedIPRange).ExpireDate);
                    _blacklistedIPs.Add(newBlackRange);
                }
                else
                {
                    WhitelistedIPRange newWhiteRange = new WhitelistedIPRange(newID, range.BeginningAddress, range.EndingAddress, range.CreatedBy, range.CreatedDate, range.Country, (range as WhitelistedIPRange).Name);
                    _whitelistedIPs.Add(newWhiteRange);
                }

                return newID;
            }
            else
            {
                BlacklistedIPRange blacklisted = (from bip in _blacklistedIPs where bip.ID == range.ID select bip).FirstOrDefault();
                if (blacklisted != null)
                {
                    _blacklistedIPs.Remove(blacklisted);
                    _blacklistedIPs.Add(range as BlacklistedIPRange);
                }

                WhitelistedIPRange whitelisted = (from wip in _whitelistedIPs where wip.ID == range.ID select wip).FirstOrDefault();
                if (whitelisted != null)
                {
                    _whitelistedIPs.Remove(whitelisted);
                    _whitelistedIPs.Add(range as WhitelistedIPRange);
                }

                return range.ID;
            }

        }

        public void DeleteRange(int rangeID)
        {
            if (rangeID <= 0)
            {
                return;
            }

            BlacklistedIPRange blacklisted = (from bip in _blacklistedIPs where bip.ID == rangeID select bip).FirstOrDefault();
            if (blacklisted != null)
            {
                _blacklistedIPs.Remove(blacklisted);
            }

            WhitelistedIPRange whitelisted = (from wip in _whitelistedIPs where wip.ID == rangeID select wip).FirstOrDefault();
            if (whitelisted != null)
            {
                _whitelistedIPs.Remove(whitelisted);
            }
        }

        public List<IPRangeCountrySummary> GetCountrySummariesPaged(int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            totalRowCount = 0;
            return new List<IPRangeCountrySummary>();
        }

        public List<IPRangeSummary> GetRangesByCountryPaged(CountryID countryID, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            totalRowCount = 0;
            return new List<IPRangeSummary>();
        }

        public List<IPRangeSummary> GetExpiringRangeSummariesPaged(DateTime beginDate, DateTime endDate, int pageSize, int pageNumber, string sortField, bool descending, out int totalRowCount)
        {
            totalRowCount = 0;
            return new List<IPRangeSummary>();
        }

        public void UpdateExpirationDate(int rangeID, DateTime? expireDate)
        {
            if (rangeID <= 0)
            {
                return;
            }

            BlacklistedIPRange blacklisted = (from bip in _blacklistedIPs where bip.ID == rangeID select bip).FirstOrDefault();
            if (blacklisted != null)
            {
                blacklisted.ExpireDate = expireDate;
            }
        }

        public List<Country> GetAllCountries()
        {
            return new List<Country>();
        }


        public int IsAddressBlockedForGroupId(IPAddress address, int IPRangeGroupID)
        {
            //Not implemented
            return 0;
        }


        public List<IPRange> GetIpRangesByGroupID(int ipRangeGroupId)
        {
            throw new NotImplementedException();
        }
    }
}
