﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.IPBlocker.Admin.IPBlockerServiceReference;
using Spark.IPBlocker.Admin.Models;
using Spark.IPBlocker.Admin.Models.ViewModels;
using Spark.IPBlocker.Admin.Models.ModelBinders;
using Spark.IPBlocker.Admin.Controllers.ActionFilters;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.CommonLibrary.Exceptions;
using Spark.CommonLibrary.Logging;

namespace Spark.IPBlocker.Admin.Controllers
{
    public class IPRangeController : Controller
    {
        Logger _logger;
        
        public IPRangeController()
        {
            _logger = new Logger(typeof(IPRangeController));
        }
        
        [UserNameFilter, LogActionFilter]
        public ActionResult Details(int id, string username)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.View))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();

                IPRange range = client.GetRange(id);
                List<AccessSummary> summaries = client.GetSummariesForRange(id);
                IPRangeSummary summary = new IPRangeSummary(range, summaries);

                IPRangeDetailViewModel viewModel = new IPRangeDetailViewModel(summary, new PermissionDictionary(), username);
                if (securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Edit))
                {
                    viewModel.Permissions.AddPermission(ObjectPermission.AddNoteButton);
                    viewModel.Permissions.AddPermission(ObjectPermission.UpdateExpirationDateButton);
                }
                if (securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Delete))
                    viewModel.Permissions.AddPermission(ObjectPermission.DeleteButton);

                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.Details", ex);
                return RedirectToAction("ErrorException");
            }
        }

        [UserNameFilter, LogActionFilter]
        public ActionResult WhitelistDetails(int id, string username)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.View))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                IPRange range = client.GetRange(id);

                IPRangeWhitelistDetailViewModel viewModel = new IPRangeWhitelistDetailViewModel((range as WhitelistedIPRange), new PermissionDictionary(), username);

                if (securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Delete))
                    viewModel.Permissions.AddPermission(ObjectPermission.DeleteButton);

                if (securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Edit))
                    viewModel.Permissions.AddPermission(ObjectPermission.AddNoteButton);

                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.WhitelistDetails", ex);
                return RedirectToAction("ErrorException");
            }
        }

        //
        // GET: /IPRange/Create

        [LogActionFilter]
        public ActionResult Create(int? id)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Create))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                RangeType createType = RangeType.BlackList; //default to blacklist if no type provided
                if (id.HasValue)
                {
                    createType = (RangeType)id;
                }
                //todo: check to make sure incoming id is valid enum type
                IPRangeCreateViewModel viewModel = null;

                List<Country> countries = CountryRepository.GetAllCountries();

                if (createType == RangeType.BlackList)
                {
                    viewModel = new IPRangeCreateViewModel(new BlacklistedIPRange(), countries);
                }
                else
                {
                    viewModel = new IPRangeCreateViewModel(new WhitelistedIPRange(), countries);
                }

                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.Create", ex);
                return RedirectToAction("ErrorException");
            }
        } 

        //
        // POST: /IPRange/Create

        [HttpPost, UserNameFilter, LogActionFilter]
        public ActionResult Create([ModelBinder(typeof(IPRangeModelBinder))] [Bind(Exclude="Country")] IPRange range, string userName)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Create))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                if (!range.BeginningAddress.IsValid())
                {
                    ModelState.AddModelError("BeginningAddress", "IP Address can not end in zero");
                }

                if (!range.EndingAddress.IsValid())
                {
                    ModelState.AddModelError("EndingAddress", "IP Address can not end in zero");
                }

                if (range.BeginningAddress > range.EndingAddress)
                {
                    ModelState.AddModelError("Address", "Ending address must be greater than beginning address");
                }

                if(ModelState.IsValid)
                {
                    range.CreatedBy = userName;
                    foreach(IPRangeNote note in range.Notes)
                    {
                        note.CreatedBy = userName;
                    }

                    IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();

                    if (range is BlacklistedIPRange)
                    {
                        List<IPRange> overlappingRanges = client.GetOverlappingRanges(range.BeginningAddress, range.EndingAddress);

                        IPRangeCreateConfirmViewModel vm = new IPRangeCreateConfirmViewModel(range, overlappingRanges);
                        return View("CreateConfirm", vm);
                    }
                    else
                    {
                        client.SaveRange(range);
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    List<Country> countries = CountryRepository.GetAllCountries();
                    IPRangeCreateViewModel viewModel = new IPRangeCreateViewModel(range, countries);
                    return View(viewModel);
                }
            }
            catch(Exception ex)
            {
                _logger.Error("Error in IPRangeController.Create", ex);
                return RedirectToAction("ErrorException");
            }
        }

        [LogActionFilter]        
        public ActionResult CreateConfirm()
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Create))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                if (Session["IPRangeCreateConfirmViewModel"] == null || !(Session["IPRangeCreateConfirmViewModel"] is IPRangeCreateConfirmViewModel))
                {
                }

                IPRangeCreateConfirmViewModel vm = Session["IPRangeCreateConfirmViewModel"] as IPRangeCreateConfirmViewModel;

                return View(vm);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.CreateConfirm", ex);
                return RedirectToAction("ErrorException");
            }
        }

        [HttpPost, LogActionFilter]
        public ActionResult CreateConfirm([ModelBinder(typeof(CreateConfirmRangeModelBinder))] BlacklistedIPRange range,
            [ModelBinder(typeof(CreateConfirmOverlappingDeleteModelBinder))] List<int> rangesToDelete,
            [ModelBinder(typeof(CreateConfirmOverlappingKeepModelBinder))] List<int> rangesToKeep)
        {

            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Create))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                if (rangesToDelete == null && rangesToKeep == null)
                {
                    client.SaveRange(range);
                }
                else
                {
                    client.SaveRangeWithOverrides(range, rangesToDelete, rangesToKeep);
                }

                Session.Remove("IPRangeCreateConfirmViewModel");

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.CreateConfirm", ex);
                return RedirectToAction("ErrorException");
            }
        } 

        //
        // GET: /IPRange/Edit/5
        [LogActionFilter]
        public ActionResult Delete(int id)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Delete))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                IPRange range = client.GetRange(id);

                if (range is BlacklistedIPRange)
                {
                    List<AccessSummary> summaries = client.GetSummariesForRange(id);
                    return View(new IPRangeSummary(range, summaries));
                }
                else
                {
                    return View(new IPRangeSummary(range));
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.Delete", ex);
                return RedirectToAction("ErrorException");
            }
        }

        //
        // POST: /IPRange/Delete/5

        [HttpPost, LogActionFilter]
        public ActionResult Delete(int id, FormCollection formCollection)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.Delete))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();

                IPRange rangeToDelete = client.GetRange(id);

                if (rangeToDelete != null)
                {
                    client.DeleteRange(id);

                    if (rangeToDelete is BlacklistedIPRange)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("ViewWhiteList");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

                
                
                
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.Delete", ex);
                return RedirectToAction("ErrorException");
            }

        }

        [LogActionFilter]
        public ActionResult ViewExpiring(int? id)
        {

            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.View))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                List<IPRangeSummary> summaries = null;
                int totalRows = 0;

                if (isPagingRequest())
                {
                    ExpiringRangesDateOptions dateOption;
                    DateTime beginDate = DateTime.Now;
                    DateTime endDate = DateTime.Now;

                    if (!id.HasValue)
                    { dateOption = ExpiringRangesDateOptions.All; }
                    else
                    {
                        dateOption = (ExpiringRangesDateOptions)id.Value;
                    }

                    switch (dateOption)
                    {
                        case ExpiringRangesDateOptions.AlreadyInactive:
                            beginDate = new DateTime(1754, 1, 1);
                            endDate = DateTime.Now;
                            break;
                        case ExpiringRangesDateOptions.SevenDays:
                            beginDate = DateTime.Now;
                            endDate = DateTime.Today.AddDays(7);
                            break;
                        case ExpiringRangesDateOptions.ThirtyDays:
                            beginDate = DateTime.Now;
                            endDate = DateTime.Today.AddDays(30);
                            break;
                        case ExpiringRangesDateOptions.SixtyDays:
                            beginDate = DateTime.Now;
                            endDate = DateTime.Today.AddDays(60);
                            break;
                        case ExpiringRangesDateOptions.All:
                            beginDate = new DateTime(1754, 1, 1);
                            endDate = DateTime.MaxValue;
                            break;
                    }

                    PagingRequest pr = new PagingRequest(Request);

                    summaries = client.GetExpiringRangeSummariesPaged(out totalRows, beginDate, endDate, pr.RecordsPerPage, pr.Page, pr.SortName, pr.IsDescending);
                    FlexigridObject fgo = FlexigridHelper.RangesByCountryToFG(summaries, pr.Page, totalRows);
                    return Json(fgo);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.ViewExpiring", ex);
                return RedirectToAction("ErrorException");
            }

        }

        [LogActionFilter]
        public ActionResult ViewExpiringTomorrow()
        {
            try
            {
                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                List<IPRangeSummary> summaries = null;
                int totalRows = 0;
                
                DateTime beginDate = DateTime.Today.AddDays(1);
                DateTime endDate = DateTime.Today.AddDays(1);

                summaries = client.GetExpiringRangeSummariesPaged(out totalRows, beginDate, endDate, 1000, 1, "", true);
                return View(summaries);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.ViewExpiringTomorrow", ex);
                return RedirectToAction("ErrorException");
            }
        }

        [LogActionFilter]
        public ActionResult ViewWhiteList()
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.View))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                List<WhitelistedIPRange> wip = null;

                if (isPagingRequest())
                {
                    PagingRequest pr = new PagingRequest(Request);
                    int totalRows = 0;

                    wip = client.GetAllWhitelistedIPRangesPaged(out totalRows, pr.RecordsPerPage, pr.Page, pr.SortName, pr.IsDescending);
                    FlexigridObject fgo = FlexigridHelper.WhitelistedIPRangeToFG(wip, pr.Page, totalRows);
                    return Json(fgo);
                }
                else
                {
                    wip = client.GetAllWhitelistedIPRanges();
                    return View(wip);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.ViewWhiteList", ex);
                return RedirectToAction("ErrorException");
            }
        }

        [LogActionFilter]
        public ActionResult RangesByCountry(int id)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.View))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                List<IPRangeSummary> summaries = null;
                int totalRows = 0;

                if (isPagingRequest())
                {
                    PagingRequest pr = new PagingRequest(Request);

                    summaries = client.GetRangesByCountryPaged(out totalRows, (CountryID)id, pr.RecordsPerPage, pr.Page, pr.SortName, pr.IsDescending);
                    FlexigridObject fgo = FlexigridHelper.RangesByCountryToFG(summaries, pr.Page, totalRows);
                    return Json(fgo);
                }
                else
                {
                    Country country = CountryRepository.GetCountry((CountryID)id);
                    return View(country);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.RangesByCountry", ex);
                return RedirectToAction("ErrorException");
            }
        }

        private bool isPagingRequest()
        {
            return !string.IsNullOrEmpty(Request.Form["page"]);
        }

        [UserNameFilter, LogActionFilter]
        public string AddNote(string note, int rangeID, string userName)
        {
            try
            {
                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();

                client.AddRangeNote(rangeID, userName, note);

                return note;
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.AddNote", ex);
                throw ex;
            }
        }

        [LogActionFilter]
        public bool UpdateExpirationDate(int rangeID, DateTime? expireDate)
        {
            bool success = true;

            try
            {
                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                client.UpdateExpirationDate(rangeID, expireDate);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.UpdateExpirationDate", ex);
                success = false;
            }

            return success;
        }

        [LogActionFilter]
        public ActionResult GetOverlappingAddressesForIP(string beginningIPaddress, string endingIPaddress)
        {
            List<IPRange> overlappingRanges = null;

            try
            {
                IPAddress beginAddress = IPAddress.StringToIP(beginningIPaddress);
                IPAddress endAddress = IPAddress.StringToIP(endingIPaddress);

                IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                overlappingRanges = client.GetOverlappingRanges(beginAddress, endAddress);
            }
            catch (Exception ex)
            {
                _logger.Error("Error in IPRangeController.GetOverlappingAddressesForIP", ex);
                throw ex;
            }

            return Json(overlappingRanges.ToArray<IPRange>(), JsonRequestBehavior.AllowGet);
        }

        [LogActionFilter]
        public ActionResult ErrorAccess()
        {
            ModelState.AddModelError("ErrorMessage", "Your account does not have the required authorization to access the IP Blocker Admin tool.  Please check with Systems.");
            return View("Error");
        }

        [LogActionFilter]
        public ActionResult ErrorException()
        {
            ModelState.AddModelError("ErrorMessage", "The system encountered an unexpected error. Please try again later.");
            return View("Error");
        }
    }
}
