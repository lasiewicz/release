﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.IPBlocker.Admin.IPBlockerServiceReference;
using Spark.IPBlocker.Admin.Models;
using Spark.IPBlocker.Admin.Models.ViewModels;
using Spark.IPBlocker.Admin.Models.ModelBinders;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.CommonLibrary.Logging;

namespace Spark.IPBlocker.Admin.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        Logger _logger;

        public HomeController()
        {
            _logger = new Logger(typeof(HomeController));
        }
        
        public ActionResult Index()
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, Constants.AZManApplicationName, (int)AuthorizationOperation.View))
                {
                    return RedirectToAction("ErrorAccess", "IPRange");
                }

                if (isPagingRequest())
                {
                    List<IPRangeCountrySummary> summaries = null;

                    PagingRequest pr = new PagingRequest(Request);
                    int totalRows = 0;

                    summaries = IPBlockerServiceAdapter.GetProxyInstance().GetCountrySummariesPaged(out totalRows, pr.RecordsPerPage, pr.Page, pr.SortName, pr.IsDescending);
                    FlexigridObject fgo = FlexigridHelper.CountrySummariesToFG(summaries, pr.Page, totalRows);
                    return Json(fgo);
                }
                else
                    return View();
            }
            catch (Exception ex)
            {
                _logger.Error("Error in HomeController.Index", ex);
                return RedirectToAction("ErrorException", "IPRange");
            }

        }

        public ActionResult Error()
        {
            return View();
        }

        private bool isPagingRequest()
        {
            return !string.IsNullOrEmpty(Request.Form["page"]);
        }
    }
}
