﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spark.IPBlocker.Admin.Controllers.ActionFilters
{
    public class UserNameFilter: ActionFilterAttribute
    {
      public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            const string Key = "userName";

            if (filterContext.ActionParameters.ContainsKey(Key))
            {
                if (filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    string username = filterContext.HttpContext.User.Identity.Name;
                    if(username.Contains(@"\"))
                    {
                        username = username.Substring(username.IndexOf(@"\") + 1);
                    }
                    filterContext.ActionParameters[Key] = username.FilterDomainName();
                }
            }
     
            base.OnActionExecuting(filterContext);
        }
    }
}
