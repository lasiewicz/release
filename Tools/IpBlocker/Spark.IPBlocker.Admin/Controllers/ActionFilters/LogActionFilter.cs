﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Spark.CommonLibrary.Exceptions;
using Spark.CommonLibrary.Logging;

namespace Spark.IPBlocker.Admin.Controllers.ActionFilters
{
    public class LogActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.RouteData.Values["controller"].ToString();
            string actionName = filterContext.RouteData.Values["action"].ToString();

            StringBuilder sb = new StringBuilder();
            
            sb.Append("Entering " +  controllerName + "." + actionName + "(");
            int count = 0;

            foreach(string key in filterContext.ActionParameters.Keys)
            {
                string seperator = (count ==0 ? "" : ",");
                string value = "null";
                if(filterContext.ActionParameters[key] != null)
                {
                    value = filterContext.ActionParameters[key].ToString();
                }

                sb.Append(seperator + value);
                count++;
            }

            sb.Append(")");

            Logger logger = new Logger(filterContext.ActionDescriptor.ControllerDescriptor.ControllerType);
            logger.Debug(sb.ToString());

            base.OnActionExecuting(filterContext);
        }
    }
}
