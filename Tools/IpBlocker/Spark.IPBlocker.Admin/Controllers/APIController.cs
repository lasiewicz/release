﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.IPBlocker.Admin.IPBlockerServiceReference;
using Spark.IPBlocker.Admin.Models;
using Spark.IPBlocker.Admin.Models.ViewModels;
using Spark.IPBlocker.Admin.Models.ModelBinders;
using Spark.IPBlocker.Admin.Controllers.ActionFilters;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;
using Spark.CommonLibrary.Logging;

namespace Spark.IPBlocker.Admin.Controllers
{
    public class APIController : Controller
    {
        //
        // GET: /API/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TriggerExpiringRangesMail()
        {
            bool success = true;
            
            try
            {
                RangesExpiringTomorrowTrigger trigger = new RangesExpiringTomorrowTrigger();
                success = trigger.TriggerMail();
            }
            catch(Exception ex)
            {
                success = false;
                Logger logger = new Logger(typeof(APIController));
                logger.Error("Error in APIController.TriggerExpiringRangesMail", ex);
            }

            return View(success);
        }

        [HttpPost, UserNameFilter]
        public ActionResult AddIPRange(string beginningIpaddress, string endingIpaddress, string createdBy, CountryID countryID, DateTime expireDate, string note, string requestingApplication, string username)
        {
            IPAPIResponse response = new IPAPIResponse(true, "success");

            try
            {
                if (IPAddress.IsValidIP(beginningIpaddress) && IPAddress.IsValidIP(endingIpaddress))
                {
                    IPAddress beginAddress = IPAddress.StringToIP(beginningIpaddress);
                    IPAddress endAddress = IPAddress.StringToIP(endingIpaddress);
                    Country country = new Country(countryID);
                    DateTime? nullableExpireDate = null;
                    if (expireDate != DateTime.MinValue) nullableExpireDate = expireDate;

                    BlacklistedIPRange range = new BlacklistedIPRange(0, beginAddress, endAddress, createdBy, DateTime.Now, country, nullableExpireDate);
                    range.Notes.Add(new IPRangeNote(0, createdBy, DateTime.Now, note));

                    IIPBlockerService client = IPBlockerServiceAdapter.GetProxyInstance();
                    int newRangeID = client.SaveRange(range);

                    APIAddNotifier notifier = new APIAddNotifier();
                    notifier.Notify(range, requestingApplication, username, CountryRepository.GetCountry(countryID), note);
                }
                else
                {
                    response.OperationSuccessful = false;
                    response.Message = "Invalid addresses in range";
                }
            }
            catch (Exception ex)
            {
                response.OperationSuccessful = false;
                response.Message = ex.Message;

                Logger logger = new Logger(typeof(APIController));
                Exception exception = new Exception(string.Format("Error in APIBL.AddIPRange({0},{1},{2},{3},{4},{5})", beginningIpaddress, endingIpaddress, createdBy, countryID.ToString(), expireDate.ToString(), note), ex);
                logger.Error(exception);
            }

            return View(response);

        }



        [HttpPost]
        public ActionResult CheckAccess(string ipaddress, AccessType accessType)
        {
            string message = "Success";
            bool canAccess = true;
            object[] returnValues = new object[2] { canAccess, message };

            try
            {
                if (IPAddress.IsValidIP(ipaddress))
                {
                    IIPBlockerService client = new IPBlockerServiceClient();
                    IPAddress address = IPAddress.StringToIP(ipaddress);
                    int rangeID = client.CheckAccess(address, accessType);
                    if (rangeID > 0) returnValues[0] = false;
                }
                else
                {
                    returnValues[0] = false;
                    returnValues[1] = "Invalid IP address";
                }

                return Json(returnValues);
            }
            catch (Exception ex)
            {
                returnValues[1] = ex.Message;
                return Json(returnValues);
            }
        }

    }
}
