﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewExpiring
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>View Expiring</h2><br />

    <select id="dateOption">
        <option value="<%=ExpiringRangesDateOptions.All.ToString("d") %>">All</option>
        <option value="<%=ExpiringRangesDateOptions.SevenDays.ToString("d") %>">7 Days</option>
        <option value="<%=ExpiringRangesDateOptions.ThirtyDays.ToString("d") %>">30 Days</option>
        <option value="<%=ExpiringRangesDateOptions.SixtyDays.ToString("d") %>">60 Days</option>
        <option value="<%=ExpiringRangesDateOptions.AlreadyInactive.ToString("d") %>">Already Expired</option>
    </select><br /><br />

    <table id="blocks">

    </table>

<script type="text/javascript">
    $(document).ready(function() {
        $("#dateOption").change(function() {
            var option = $("#dateOption").val();
            $('#blocks').flexOptions({ url: '<%=Url.Action("ViewExpiring") %>/' + option }).flexReload();
        });
    }); 
    
    function button_click(com, grid) {
        if ($('.trSelected', grid).length > 0) {
            var id = $('.trSelected').attr('id').slice(3);
            window.location = "<%=Url.Action("Delete") %>/" + id;
        }
    }

    $("#blocks").flexigrid({
        url: '<%=Url.Action("ViewExpiring") %>/',
        dataType: 'json',
        colModel : [
				{ display: 'RangeID', name: 'ID', width: 1, sortable: false, hide:true, align: 'center' },
				{ display: 'IP Range', name: 'range', width: 140, sortable: true, align: 'center' },
				{ display: 'Sub Attempts<br>Total', name: 'subscriptionattemptstotal', width: 80, sortable: true, align: 'center' },
				{ display: 'Sub Attempts<br>90', name: 'subscriptionattemptsninetydays', width: 80, sortable: true, align: 'center' },
				{ display: 'Reg Attempts<br>Total', name: 'registrationattemptstotal', width: 80, sortable: true, align: 'center' },
				{ display: 'Reg Attempts<br>90', name: 'registrationattemptsninetydays', width: 80, sortable: true, align: 'center' },
				{ display: 'Last Access', name: 'maxlastattemptdate', width: 80, sortable: true, align: 'center' },
                { display: 'Status', name: 'status', width: 100, sortable: false, align: 'center' },
                { display: 'User', name: 'createdby', width: 100, sortable: true, align: 'center' },
                { display: 'DateCreated', name: 'createddate', width: 80, sortable: true, align: 'center' }
				],
                buttons: [
                { name: 'Delete', bclass: 'deletebutton', onpress: button_click },
                { separator: true }
                ],
            sortname: "range",
            sortorder: "desc",
            usepager: true,
			resizable: false,
			singleSelect: true,
			useRp: true,
			rp: 15,
			showTableToggleBtn: true,
			width: 1012,
			height: 600,
			showToggleBtn: false,
			onSuccess: function() { onDblClick("<%=Url.Action("Details") %>/") }
});
</script>
</asp:Content>
