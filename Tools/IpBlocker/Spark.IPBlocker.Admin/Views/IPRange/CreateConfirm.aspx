﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IPRangeCreateConfirmViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	CreateConfirm
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%
    List<BlacklistedIPRange> blacklistedRanges = null;
    List<WhitelistedIPRange> whitelistedRanges = null;
    if (Model.OverlappingRanges != null)
    {
        blacklistedRanges = (from ol in Model.OverlappingRanges where ol is BlacklistedIPRange select ol as BlacklistedIPRange).ToList();
        whitelistedRanges = (from ol in Model.OverlappingRanges where ol is WhitelistedIPRange select ol as WhitelistedIPRange).ToList();
    }
%>


    <fieldset>
        <legend>Confirm New IP Range</legend>
        <h3>Are you sure you want to add this IP Block?</h3>
        <dl id="confirm-ip-range-info">
            <dt class="newline">Location: </dt>
                <dd><%=Model.Range.Country.Name %></dd>
            <dt class="newline">IPAddress Range:</dt>
                <dd><%=Model.Range.BeginningAddress %> - <%=Model.Range.EndingAddress %></dd>
            <dt class="newline">Blocking:</dt>
                <dd><%=Model.Range.Count %> IP addresses</dd>
            <dt class="newline">Blocked by:</dt>
                <dd><%=Model.Range.CreatedBy %></dd>
            <dt class="newline">Blocked on:</dt>
                <dd><%=Model.Range.CreatedDate.ToShortDateString() %></dd>
             <dt class="newline">Justification:</dt>
                <dd><%=Model.Range.Notes[0].Note %></dd>
    </dl>

    <br /><br />
    <% using (Html.BeginForm("CreateConfirm", "IPRange")) { %>
        <% if (Model.OverlappingRanges != null) {%>        
            <% if (blacklistedRanges != null && blacklistedRanges.Count > 0)
               { %>
                <p>This IP block supersedes other IP blocks already in place. Please select what you would like to happen to the existing blocks.</p>   
                <table class="displayTable">
                <thead><tr><td>Location</td><td>Range</td><td>Created By</td><td>Date</td><td>Keep</td><td>Delete</td></tr></thead>
                <% foreach (BlacklistedIPRange bip in blacklistedRanges) {%>
                    <tr>
                        <td><%=bip.Country.Name%></td>
                        <td><%=bip.BeginningAddress.ToString()%> - <%=bip.EndingAddress.ToString()%></td>
                        <td><%=bip.CreatedBy%></td>
                        <td><%=bip.CreatedDate.ToShortDateString()%></td>
                        <td><%=Html.RadioButton(Constants.ConfirmActionPrefix + bip.ID, ConfirmAction.Keep.ToString("d"), true)%></td>
                        <td><%=Html.RadioButton(Constants.ConfirmActionPrefix + bip.ID, ConfirmAction.Delete.ToString("d"))%></td>
                    </tr>
                <%} %>
                </table>
            <%} %>
            <% if (whitelistedRanges != null && whitelistedRanges.Count > 0)  { %>
                <p>The IP range you are adding contains IP addresses that have been whitelisted. The whitelisted IP addresses are shown below. You can still add the new IP range, however the white listed IP address below will never be blocked.</p>
                <p><table class="displayTable">
                    <tr><td>Name</td><td>Location</td><td>Range</td><td>By</td><td>On</td></tr>
                    <% foreach (WhitelistedIPRange wip in whitelistedRanges) {%>
                        <tr>
                            <td><%=wip.Name%></td>
                            <td><%=wip.Country.Name %></td>
                            <td><%=wip.BeginningAddress.ToString()%> - <%=wip.EndingAddress.ToString()%></td>
                            <td><%=wip.CreatedBy%></td>
                            <td><%=wip.CreatedDate.ToShortDateString()%></td>
                        </tr>
                    <% } %>
                </table></p>
            <% } %>
        <%} %>
        <%=Html.Hidden("range", SerializationUtils.Serialize(Model.Range))%>
        <%=Html.Hidden("overlappingRanges", SerializationUtils.Serialize(Model.OverlappingRanges))%>
        <br /><input type="submit" value="Yes, Add this Block " />    
<%} %>
<br /><%=Html.ActionLink("No, Return to add block", "Create") %>
</fieldset>
</asp:Content>
