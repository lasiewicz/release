﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<List<WhitelistedIPRange>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewWhiteList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>White List</h2><br />
    <table id="whitelist-ip-addresses">

    </table>

<script type="text/javascript">
    function delete_click(com, grid) {
        if ($('.trSelected', grid).length > 0) {
            var id = $('.trSelected').attr('id').slice(3);
            window.location = "<%=Url.Action("Delete") %>/" + id;
        }
    }

    function create_click(com, grid) {
        window.location = "<%=Url.Action("Create") %>/1";
    }


    $("#whitelist-ip-addresses").flexigrid({
            url: '<%=Url.Action("ViewWhiteList") %>/',
        dataType: 'json',
        colModel : [
				{ display: 'IPRangeListID', name: 'ID', width: 100, sortable: true, hide:true, align: 'center' },
				{ display: 'Name', name: 'name', width: 100, sortable: true, align: 'center' },
				{ display: 'IP Range', name: 'ipRange', width: 150, sortable: true, align: 'center' },
				{ display: 'Country', name: 'country', width: 100, sortable: true, align: 'center' },
				{ display: 'User', name: 'createdBy', width: 100, sortable: true, align: 'center' },
				{ display: 'Date Created', name: 'createdDate', width: 100, sortable: true, align: 'center' }
				],
			buttons: [
                { name: 'Delete', bclass: 'deletebutton', onpress: delete_click },
                { separator: true },
                { name: 'Create New', bclass: 'deletebutton', onpress: create_click },
                { separator: true }
                ],
            sortname: "dateCreated",
            sortorder: "desc",
            usepager: true,
            resizable: false,
			singleSelect: true,
			useRp: true,
			rp: 15,
			showTableToggleBtn: true,
			width: 660,
			height: 600,
			showToggleBtn: false,
			onSuccess: function() { onDblClick("<%=Url.Action("WhitelistDetails") %>/") }
});
</script>
</asp:Content>
