﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IPRangeCreateViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Add New IP Range
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(function(){
            $('#Note').keyup(function() {
                limitChars('Note', 1500);
            })
        });
    </script>
    <%
        string rangeType; if (Model.Range is WhitelistedIPRange) rangeType = Spark.IPBlocker.ValueObjects.Enumerations.RangeType.WhiteList.ToString("d"); else rangeType = Spark.IPBlocker.ValueObjects.Enumerations.RangeType.BlackList.ToString("d");
    %>
        
    <fieldset>
        <legend>Add New IP Range</legend>
        <table>
        <% using (Html.BeginForm("Create", "IPRange")) { %>
            <input type="hidden" value="0" id="ID" name="ID" />
            <% if (Model.Range is WhitelistedIPRange) { %>
                <tr>
                    <td>Name:</td>
                    <td><%=Html.TextBox("Name", null, new { maxlength = 25})%>  <%=Html.ValidationMessage("Name") %>  </td>
                </tr>
            <%} %>
            <tr>
                <td>Location:</td>
                <td>
                    <select id="Country" name="Country">
                        <option value="-1">Select...</option>
                        <% foreach (Country country in Model.Countries) { %>
                            <option value="<%= country.ID.ToString("d") %>"><%= country.Name%></option>
                        <%} %>
                    </select>&nbsp;&nbsp;<%= Html.ValidationMessage("Country")%>
                </td>
            </tr>
            <tr>
                <td>IP Range:</td>
                <td>
                    <%=Html.TextBox("BeginningAddress.FirstOctet", null, new { maxlength = 3, @class = "ipOctetTextbox" })%>.<%=Html.TextBox("BeginningAddress.SecondOctet", null, new { maxlength = 3, @class = "ipOctetTextbox" })%>.<%=Html.TextBox("BeginningAddress.ThirdOctet", null, new { maxlength = 3, @class = "ipOctetTextbox" })%>.<%=Html.TextBox("BeginningAddress.FourthOctet", null, new { maxlength = 3, @class = "ipOctetTextbox" })%> - <br />
                    <%=Html.TextBox("EndingAddress.FirstOctet", null, new { maxlength = 3, @class = "ipOctetTextbox" })%>.<%=Html.TextBox("EndingAddress.SecondOctet", null, new { maxlength = 3, @class = "ipOctetTextbox" })%>.<%=Html.TextBox("EndingAddress.ThirdOctet", null, new { maxlength = 3, @class = "ipOctetTextbox" })%>.<%=Html.TextBox("EndingAddress.FourthOctet", null, new { maxlength = 3, @class = "ipOctetTextbox" })%>
                    <%= Html.ValidationMessage("BeginningAddress")%>
                       <input type="button" value="Check IP" id="checkIP" /><br /><%= Html.ValidationMessage("Address")%>
                </td>
            </tr>
            <tr>
                <td>Justification:</td>
                <td>
                    <%=Html.TextArea("Note")%><br />
                    <%=Html.ValidationMessage("Note") %>
                </td>
            </tr>
            
            <% if (Model.Range is BlacklistedIPRange) { %> 
                <tr>
                    <td>Active Until:</td>
                    <td><%=Html.TextBox("ExpireDate", null, new { @class = "datepicker" })%><%=Html.ValidationMessage("expireDate") %></td>
                </tr>
            <%} %>
            </table><br />
            <%=Html.Hidden("rangeType", rangeType) %>
            <input type="submit" value="Add IP Block" />
            
        <% } %>
        
        </fieldset>

</asp:Content>
