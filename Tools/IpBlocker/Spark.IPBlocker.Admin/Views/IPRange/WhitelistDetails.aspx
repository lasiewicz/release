﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IPRangeWhitelistDetailViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	WhitelistDetails
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
        function noteAdded(ajaxContent) {
            var noteAdded = ajaxContent.get_data();
            var currentDate = getDate();
            var user = "<%=Model.UserName %>";

            $('#notes > tbody:last').append('<tr><td class="note-author">' + user + ':</td><td  class="note-content">' + noteAdded + '</td><td  class="note-date">' + currentDate + '</td></tr>');
        }
        function noteAddFailure(ajaxContent) {
            alert("There was an error adding your note.");
        }

        $(function(){
            $('#Note').keyup(function() {
                limitChars('Note', 1500);
            })
        });
    </script>
    <h2>View Whitelisted IP Range</h2>
        <%if(Model.Permissions.HasPermission(ObjectPermission.DeleteButton)){ %>
            <div id='confirm-dialog'><% using (Html.BeginForm("Delete", "IPRange", FormMethod.Post, new { id = "deleteForm" })) { %>
                <%=Html.Hidden("ID", Model.Range.ID)%>
                <input type="submit" name="confirm" class="confirm" value="Delete" />
            <% }%>
            </div>
        
            <div class='confirm-text' style="display:none">
                <p>Are you sure you want to delete the IP Range <strong><%=Model.Range.BeginningAddress.ToString() %> - <%=Model.Range.EndingAddress.ToString()%></strong> ?</p>
            </div>
        <% }%>
        
    <dl id="ip-range-info">
        <dt>IP Address Range: <%=Model.Range.BeginningAddress.ToString()%> - <%=Model.Range.EndingAddress.ToString()%> </dt>
        <dd></dd>
        <dt>Name: <%= Model.Range.Name%></dt><dd></dd>
        <dt class="blocked-by">Blocked by: <%= Model.Range.CreatedBy%></dt><dd></dd>
        <dt>Blocked on: <%= Model.Range.CreatedDate.ToShortDateString()%></dt><dd></dd>
    </dl>
    
    <% if (Model.Range.Notes.Count > 0)
       {%>
    <h3>Notes</h3>
    <table id="notes">
        <tbody>
        <%foreach (IPRangeNote note in Model.Range.Notes)
          { %>
         <tr><td class="note-author"><%=note.CreatedBy %>:</td> <td class="note-content"><%=note.Note %></td><td class="note-date"><%= note.CreatedDate.ToShortDateString() %></td></tr>
        <% } %>
        </tbody>
    </table>        
    <% } %>
    
    <%if(Model.Permissions.HasPermission(ObjectPermission.AddNoteButton)){ %>
        <ul id="add-note">
            <li>
            <label for="Note">Add Note:</label>
            </li>
            <li id="add-note-textarea">
                <% using (Ajax.BeginForm("AddNote", new AjaxOptions{OnSuccess="noteAdded", OnFailure="noteAddFailure"})){ %>
                    <%=Html.TextArea("Note")%>
                    <%=Html.Hidden("RangeID",  Model.Range.ID.ToString()) %>
                    <input type="submit" name="submit-addnote" id="submit-addnote" value="Add Note" />
                <% }%>
             
            </li>
        </ul>
    <% } %>
    <script type="text/javascript">
        $(document).ready(function() {
            dialogModal('.confirm', '.confirm-text', {
                "Cancel": function() { $(this).dialog("close"); },
                "Ok": function() { $(this).dialog("close"); $('#deleteForm').submit(); }
            });
        });
    </script>

</asp:Content>
