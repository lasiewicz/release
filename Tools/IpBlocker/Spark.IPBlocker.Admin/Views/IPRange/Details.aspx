﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IPRangeDetailViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%
        AccessSummary subSummary = (from s in Model.RangeSummary.Summaries where s.AccessType == AccessType.Subscription select s).FirstOrDefault();
        AccessSummary regSummary = (from s in Model.RangeSummary.Summaries where s.AccessType == AccessType.Registration select s).FirstOrDefault();
    %>

    <h2>View IP Range</h2>
        
        <%if(Model.Permissions.HasPermission(ObjectPermission.DeleteButton)){ %>
            <div id='confirm-dialog'><% using (Html.BeginForm("Delete", "IPRange", FormMethod.Post, new { id = "deleteForm" })) { %>
            <%=Html.Hidden("ID", Model.RangeSummary.Range.ID)%>
            <input type="submit" name="confirm" class="confirm" value="Delete" />
            <% }%>
            </div>
        
    
            <div class='confirm-text' style="display:none">
                <p>Are you sure you want to delete the IP Range <strong><%=Model.RangeSummary.Range.BeginningAddress.ToString()%> - <%=Model.RangeSummary.Range.EndingAddress.ToString()%></strong> ?</p>
            </div>
        <% }%>

    <dl id="ip-range-info">
        <dt>IP Address Range: <%=Model.RangeSummary.Range.BeginningAddress.ToString()%> - <%=Model.RangeSummary.Range.EndingAddress.ToString()%> </dt>
        <dd></dd>
        <dt class="subscription-attempts">Subscription Attempts:</dt>
        <dd class="subscription-attempts">
            <dl>
                <dt>Total: <%= subSummary == null ? "0" : subSummary.TotalAttempts.ToString()  %></dt>
                <dd></dd>
                <dt>Last 90 Days: <%= subSummary == null ? "0" : subSummary.NinetyDayAttempts.ToString()  %></dt>
                <dd></dd>
                <dt>Last 30 Days: <%= subSummary == null ? "0" : subSummary.ThirtyDayAttempts.ToString()  %></dt>
                <dd></dd>
            </dl>
        </dd>
        <dt class="registration-attempts">Registration Attempts:</dt>
        <dd class="registration-attempts">
            <dl>
                <dt>Total: <%= regSummary == null ? "0" : regSummary.TotalAttempts.ToString()%></dt>
                <dd></dd>
                <dt>Last 90 Days: <%= regSummary == null ? "0" : regSummary.NinetyDayAttempts.ToString()%></dt>
                <dd></dd>
                <dt>Last 30 Days: <%= regSummary == null ? "0" : regSummary.ThirtyDayAttempts.ToString()%></dt>
                <dd></dd>
            </dl>
        </dd>
        <dt class="blocked-by">Blocked by: <%= Model.RangeSummary.Range.CreatedBy%></dt><dd></dd>
        <dt>Blocked on: <%= Model.RangeSummary.Range.CreatedDate.ToShortDateString()%></dt><dd></dd>
    </dl>
    
    <% if (Model.RangeSummary.Range.Notes.Count > 0)
       {%>
    <h3>Notes</h3>
    <table id="notes">
        <tbody>
        <%foreach (IPRangeNote note in Model.RangeSummary.Range.Notes)
          { %>
         <tr><td class="note-author"><%=note.CreatedBy %>: <td class="note-content"><%=note.Note %></td><td class="note-date"><%= note.CreatedDate.ToShortDateString() %></td></tr>
        <% } %>
        </tbody>
    </table>        
    <% } %>
    
    <%if(Model.Permissions.HasPermission(ObjectPermission.AddNoteButton)){ %> 
        <ul id="add-note">
            <li>
            <label for="Note">Add Note:</label>
            </li>
            <li id="add-note-textarea">
                <% using (Ajax.BeginForm("AddNote", new AjaxOptions{OnSuccess="noteAdded", OnFailure="noteAddFailure"})){ %>
                    <%=Html.TextArea("Note")%>
                    <%=Html.Hidden("RangeID", Model.RangeSummary.Range.ID.ToString())%>
                    <input type="submit" name="submit-addnote" id="submit-addnote" value="Add Note" />
                <% }%>
             
            </li>
        </ul>
    <% } %>
    
   <ul id="active-until">
        <li>
        <label for="Note">Active Until:</label>
        </li>
        <%if(Model.Permissions.HasPermission(ObjectPermission.UpdateExpirationDateButton)){ %> 
            <li id="Li1">
                <% using (Ajax.BeginForm("UpdateExpirationDate", new AjaxOptions { OnFailure = "expireDateFailure", OnSuccess = "expireDateChanged" })) { %>
                    <%=Html.Hidden("RangeID",  Model.RangeSummary.Range.ID.ToString()) %>
                    <%=Html.TextBox("ExpireDate", (Model.RangeSummary.Range as BlacklistedIPRange).ExpireDate.HasValue ? (Model.RangeSummary.Range as BlacklistedIPRange).ExpireDate.Value.ToShortDateString() : "", new { @class = "datepicker" })%>
                    </li>
                    <li>
                    <input type="submit" name="submit-save-inactive-date" id="submit-save-inactive-date" value="Save Inactive Date" />
                <% }%><br /><br />
                <% using (Ajax.BeginForm("UpdateExpirationDate", new AjaxOptions { OnSuccess = "expireDateNulled", OnFailure = "expireDateFailure" })) { %>
                    <%=Html.Hidden("RangeID", Model.RangeSummary.Range.ID.ToString())%>
                    <input type="submit" name="submit-delete-inactive-date" id="submit-delete-inactive-date" value="Delete Inactive Date" />
                 <% }%>
            </li>
        <% } else {%>
        <li><%= (Model.RangeSummary.Range as BlacklistedIPRange).ExpireDate.HasValue ? (Model.RangeSummary.Range as BlacklistedIPRange).ExpireDate.Value.ToShortDateString() : "" %></li>
        <% }%>
    </ul>

    <script type="text/javascript">
        $(document).ready(function() {
            dialogModal('.confirm', '.confirm-text', {
                "Cancel": function() { $(this).dialog("close"); },
                "Ok": function() { $(this).dialog("close"); $('#deleteForm').submit(); }
            });
        });
        
        function noteAdded(ajaxContent) {
            var noteAdded = ajaxContent.get_data();
            var currentDate = getDate();
            var user = "<%=Model.UserName %>";

           
            $('<tr style="display:none"><td class="note-author">' + user + ':</td><td  class="note-content">' + noteAdded + '</td><td  class="note-date">' + currentDate + '</td></tr>')
                .appendTo('#notes > tbody:last')
                .fadeIn('slow', function(){if($.browser.msie) this.style.removeAttribute('filter')});
        }   
        function noteAddFailure(ajaxContent) {
            popUp({message:"There was an error adding your note."});
        }

        function expireDateNulled(ajaxContent) {
            $('#ExpireDate').val('');
            popUp({message:"The Active Until date has been updated."});
        }
        
        function expireDateChanged(ajaxContent){
            popUp({message:"The Active Until date has been updated."});
        }

        function expireDateFailure(ajaxContent) {
            popUp({message:"There was an error updating the expiration date."});
        }

        $(function(){
            $('#Note').keyup(function() {
                limitChars('Note', 1500);
            })
        });


    </script>

</asp:Content>
