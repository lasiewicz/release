﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<IPRangeSummary>>" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>View Expiring Tomorrow</title>
</head>
<body>
    <div style="width: 90%;margin-left: auto;margin-right: auto;"><p>Here is the list of IP Ranges that are due to expire tomorrow (<%=DateTime.Today.AddDays(1).ToShortDateString() %>).</p><br />
    <table style="border: solid 1px #e8eef4;border-collapse: collapse;">
        <thead style="padding: 6px 5px;text-align: left; font-weight:bold; background-color: #e8eef4;border: solid 1px #e8eef4; vertical-align:bottom">
            <tr>
                <td style="padding: 5px;border: solid 1px #e8eef4;">IP Range</td>
                <td style="padding: 5px;border: solid 1px #e8eef4;">Sub Attempts Total</td>
                <td style="padding: 5px;border: solid 1px #e8eef4;">Sub Attempts 90 Days</td>
                <td style="padding: 5px;border: solid 1px #e8eef4;">Reg Attempts Total</td>
                <td style="padding: 5px;border: solid 1px #e8eef4;">Reg Attempts 90 Days</td>
                <td style="padding: 5px;border: solid 1px #e8eef4;">Last Access</td>
                <td style="padding: 5px;border: solid 1px #e8eef4;">Created By</td>
                <td style="padding: 5px;border: solid 1px #e8eef4;">Created Date</td>
            </tr>
        </thead>
        <tbody>
    <%foreach (IPRangeSummary summary in Model){ 
        AccessSummary subSummary = (from s in summary.Summaries where s.AccessType == AccessType.Subscription select s).FirstOrDefault();
        AccessSummary regSummary = (from s in summary.Summaries where s.AccessType == AccessType.Registration select s).FirstOrDefault();
    %>
            <tr>
                <td style="padding: 5px;border: solid 1px #e8eef4;"><a href="<%=Url.ActionAbsolute("Details", "IPRange")%>/<%=summary.Range.ID.ToString() %>"><%=summary.Range.BeginningAddress.ToString() + " - " + summary.Range.EndingAddress.ToString()%></a></td>
                <td style="padding: 5px;border: solid 1px #e8eef4;"><%=subSummary.TotalAttempts.ToString()%></td>
                <td style="padding: 5px;border: solid 1px #e8eef4;"><%=subSummary.NinetyDayAttempts.ToString()%></td>
                <td style="padding: 5px;border: solid 1px #e8eef4;"><%=regSummary.TotalAttempts.ToString()%></td>
                <td style="padding: 5px;border: solid 1px #e8eef4;"><%=regSummary.NinetyDayAttempts.ToString()%></td>
                <td style="padding: 5px;border: solid 1px #e8eef4;"><%= (regSummary.LastAccessDate == DateTime.MinValue) ? string.Empty : regSummary.LastAccessDate.ToShortDateString()%></td>
                <td style="padding: 5px;border: solid 1px #e8eef4;"><%=summary.Range.CreatedBy%></td>
                <td style="padding: 5px;border: solid 1px #e8eef4;"><%=summary.Range.CreatedDate.ToShortDateString()%></td>
            </tr>
    <%} %>
        </tbody>
    </table>
    </div>
</body>
</html>
