﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Spark.IPBlocker.ValueObjects.IPRangeSummary>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Delete
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%
        AccessSummary subSummary = null;
        AccessSummary regSummary = null;
        if (Model.Range is BlacklistedIPRange)
        {
            subSummary = (from s in Model.Summaries where s.AccessType == AccessType.Subscription select s).FirstOrDefault();
            regSummary = (from s in Model.Summaries where s.AccessType == AccessType.Registration select s).FirstOrDefault();
        }
    %>

    <% if (Model.Range is BlacklistedIPRange){%>
        <h2>Are you sure you want to delete this IP Range?</h2>
    <%} else {%>
        <h2>Are you sure you want to delete this whitelisted IP Range?</h2>
    <%} %>
    <dl id="ip-range-info">
        <dt>Location: <%=Model.Range.Country.Name %></dt><dd></dd>
        <dt>IP Address Range: <%=Model.Range.BeginningAddress.ToString()%> - <%=Model.Range.EndingAddress.ToString()%> </dt><dd></dd>
        <% if (Model.Range is BlacklistedIPRange){%>
            <%if ((Model.Range as BlacklistedIPRange).ExpireDate.HasValue) { %>
                <dt>Active Until: <%=(Model.Range as BlacklistedIPRange).ExpireDate.Value.ToShortDateString()%></dt><dd></dd>
            <%} %>
            <dt class="subscription-attempts">Subscription Attempts:</dt>
            <dd class="subscription-attempts">
                <dl>
                    <dt>Total: <%= subSummary == null ? "0" : subSummary.TotalAttempts.ToString()%></dt>
                    <dd></dd>
                    <dt>Last 90 Days: <%= subSummary == null ? "0" : subSummary.NinetyDayAttempts.ToString()%></dt>
                    <dd></dd>
                    <dt>Last 30 Days: <%= subSummary == null ? "0" : subSummary.ThirtyDayAttempts.ToString()%></dt>
                    <dd></dd>
                </dl>
            </dd>
            <dt class="registration-attempts">Registration Attempts:</dt>
            <dd class="registration-attempts">
                <dl>
                    <dt>Total: <%= regSummary == null ? "0" : regSummary.TotalAttempts.ToString()%></dt>
                    <dd></dd>
                    <dt>Last 90 Days: <%= regSummary == null ? "0" : regSummary.NinetyDayAttempts.ToString()%></dt>
                    <dd></dd>
                    <dt>Last 30 Days: <%= regSummary == null ? "0" : regSummary.ThirtyDayAttempts.ToString()%></dt>
                    <dd></dd>
                </dl>
            </dd>
        <%} %>
        <dt class="blocked-by">Blocked by: <%= Model.Range.CreatedBy %></dt><dd></dd>
        <dt>Blocked on: <%= Model.Range.CreatedDate.ToShortDateString() %></dt><dd></dd>
    </dl>
    
    <% if (Model.Range.Notes.Count > 0) {%>
    <h3>Notes</h3>
    <table id="notes">
        <tbody>
        <%foreach(IPRangeNote note in Model.Range.Notes) { %>
         <tr><td class="note-author"><%=note.CreatedBy %>:</td> <td class="note-content"><%=note.Note %></td><td class="note-date"><%= note.CreatedDate.ToShortDateString() %></td></tr>
        <% } %>
        </tbody>
    </table>        
    <% } %>
    <% using (Html.BeginForm()) { %>
        <%=Html.Hidden("id", Model.Range.ID.ToString()) %>
        <input type="submit" value="Yes, Delete this Block" />
    <% }%>
    <br /><%=Html.ActionLink("No, keep this block", "Index", "Home") %>
</asp:Content>
