﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <table id="country-blocks">

    </table>

<script type="text/javascript">
    function button_click(com, grid) {
        if ($('.trSelected', grid).length > 0) {
            var result = confirm('Delete ' + $('.trSelected').attr('id').slice(3) + '?');
        }
    }

    $("#country-blocks").flexigrid({
            url: '/Home/Index',
        dataType: 'json',
        colModel : [
				{ display: 'CountryID', name: 'ID', width: 100, sortable: false, hide:true, align: 'center' },
				{ display: 'Name', name: 'name', width: 100, sortable: true, align: 'center' },
				{ display: 'Blocks', name: 'blockcount', width: 100, sortable: true, align: 'center' },
				{ display: 'Reg Attempts 90', name: 'registrationattemptsninetydays', width: 100, sortable: true, align: 'center' },
				{ display: 'Reg Attempts Total', name: 'registrationattemptstotal', width: 100, sortable: true, align: 'center' },
				{ display: 'Sub Attempts 90', name: 'subscriptionattemptsninetydays', width: 100, sortable: true, align: 'center' },
				{ display: 'Sub Attempts Total', name: 'subscriptionattemptstotal', width: 100, sortable: true, align: 'center' }
				],
			sortname: "name",
            sortorder: "desc",
            usepager: true,
            resizable: false,
			singleSelect: true,
			useRp: true,
			rp: 15,
			showTableToggleBtn: true,
			width: 730,
			height: 600,
			showToggleBtn: false,
			onSuccess: function() { onDblClick("/IPRange/RangesByCountry/") }
});
</script>
</asp:Content>
