﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.IPBlocker.Admin.Models
{
    public static class Constants
    {
        public static string ConfirmActionPrefix = "action";
        public static string AZManApplicationName = "ipblocker";
    }

    public enum ConfirmAction
    {
        Keep = 0,
        Delete = 1
    }

    public enum AuthorizationOperation
    {
        View = 100,
        Create = 200,
        Edit = 300,
        Delete = 400
    }

    public enum ObjectPermission
    {
        AddNoteButton = 0,
        UpdateExpirationDateButton = 1,
        DeleteButton = 2,
        CreateNewButton = 3
    }

    public enum ExpiringRangesDateOptions
    {
        SevenDays = 0,
        ThirtyDays = 1,
        SixtyDays = 2,
        AlreadyInactive = 3, 
        All = 4
    }
}
