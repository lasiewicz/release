﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.IO;
using Spark.IPBlocker.ValueObjects;

namespace Spark.IPBlocker.Admin.Models
{
    public static class SerializationUtils
    {
        public static string Serialize(object obj)
        {
            StringWriter sw = new StringWriter();
            new LosFormatter().Serialize(sw, obj);
            return sw.ToString();
        }

        public static object Deserialize(string data)
        {
            if (data == null)
            {
                return null;
            }
            return new LosFormatter().Deserialize(data);
        }

    }
}
