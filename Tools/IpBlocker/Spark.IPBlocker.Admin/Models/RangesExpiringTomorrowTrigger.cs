﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.IO;

namespace Spark.IPBlocker.Admin.Models
{
    public class RangesExpiringTomorrowTrigger
    {
        string _url = string.Empty;
        string _from = string.Empty;
        string _to = string.Empty;
        string _host = string.Empty;
        
        public RangesExpiringTomorrowTrigger()
        {
            _url = System.Configuration.ConfigurationManager.AppSettings["ExpiringTomorrowURL"];
            _from = System.Configuration.ConfigurationManager.AppSettings["ExpiringTomorrowMailFrom"];
            _to = System.Configuration.ConfigurationManager.AppSettings["ExpiringTomorrowMailTo"];
            _host = System.Configuration.ConfigurationManager.AppSettings["MailHost"];
        }

        public bool TriggerMail()
        {
            bool success = true;

            string page = GetWebContents(_url);
            MailWebContentsToUser(page);
            
            return success;

        }
        
        private string GetWebContents(string url)
        {
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
            myRequest.Credentials = CredentialCache.DefaultCredentials;
            myRequest.Method = "GET";
            WebResponse myResponse = myRequest.GetResponse();
            StreamReader sr = new StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8);
            string result = sr.ReadToEnd();
            sr.Close();
            myResponse.Close();
            return result;
        }

        private void MailWebContentsToUser(string body)
        {
            MailMessage mail = new MailMessage();
            mail.IsBodyHtml = true;
            mail.Subject = "Tomorrow's Expiring Ranges";
            mail.From = new MailAddress(_from);
            mail.To.Add(new MailAddress(_to));
            mail.Body = body;

            SmtpClient mailClient = new SmtpClient();
            mailClient.Host = _host;
            mailClient.Port = 25;
            mailClient.Send(mail);
            //relay.matchnet.com
        }
    }
}
