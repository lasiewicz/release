﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.IPBlocker.Admin.Models;
using Spark.IPBlocker.Admin.Models.ViewModels;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Admin.Models.ModelBinders
{
    public class CreateConfirmRangeModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var model = base.BindModel(controllerContext, bindingContext);
            BlacklistedIPRange range = null;

            string serializedRange = bindingContext.ValueProvider.GetValue("range").AttemptedValue;
            if (!string.IsNullOrEmpty(serializedRange))
            {
                range = SerializationUtils.Deserialize(serializedRange) as BlacklistedIPRange;
            }

            return range;
        }
    }
}
