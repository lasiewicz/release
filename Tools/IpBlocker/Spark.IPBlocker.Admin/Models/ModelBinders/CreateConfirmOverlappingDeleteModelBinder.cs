﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.IPBlocker.Admin.Models;
using Spark.IPBlocker.Admin.Models.ViewModels;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Admin.Models.ModelBinders
{
    public class CreateConfirmOverlappingDeleteModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var model = base.BindModel(controllerContext, bindingContext);
            List<IPRange> overlappingRanges = null;
            List<int> rangesToDelete = null;

            string serializedOverlappingRanges = bindingContext.ValueProvider.GetValue("overlappingRanges").AttemptedValue;
            if (!string.IsNullOrEmpty(serializedOverlappingRanges))
            {
                overlappingRanges = SerializationUtils.Deserialize(serializedOverlappingRanges) as List<IPRange>;
            }

            foreach (IPRange range in overlappingRanges)
            {
                if (range is BlacklistedIPRange)
                {
                    ConfirmAction action = (ConfirmAction)Convert.ToInt32(bindingContext.ValueProvider.GetValue(Constants.ConfirmActionPrefix + range.ID).AttemptedValue);
                    if (action == ConfirmAction.Delete)
                    {
                        if (rangesToDelete == null) rangesToDelete = new List<int>();
                        rangesToDelete.Add(range.ID);
                    }
                }
            }

            return rangesToDelete;
        }
    }
}




