﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Admin.Models.ModelBinders
{
    public class IPRangeModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            BlacklistedIPRange blacklistedRange = new BlacklistedIPRange();
            WhitelistedIPRange whitelistedRange = new WhitelistedIPRange();
            int countryID = 0;
            bool hasErrors = false;
            string note = string.Empty;
            RangeType type = RangeType.BlackList;
            
            IPRange model = base.BindModel(controllerContext, bindingContext) as IPRange;

            object attemptedRangeType = bindingContext.ValueProvider.GetValue("rangeType").AttemptedValue;

            if (attemptedRangeType != null)
            {
                type = (RangeType)Convert.ToInt32(attemptedRangeType);
            }
            
            object attemptedNote = bindingContext.ValueProvider.GetValue("Note").AttemptedValue;
            object attemptedCountryID = bindingContext.ValueProvider.GetValue("Country").AttemptedValue;

            if (attemptedCountryID == null || !int.TryParse(attemptedCountryID.ToString(), out countryID) || countryID <0)
                { bindingContext.ModelState.AddModelError("Country", "Must supply country"); hasErrors = true; }
            if (attemptedNote == null || attemptedNote.ToString() == string.Empty)
                { bindingContext.ModelState.AddModelError("Note", "Must supply a justification"); hasErrors = true; }

            if (!hasErrors)
            {
                Country country = CountryRepository.GetCountry((CountryID)countryID);

                if (type == RangeType.BlackList)
                {
                    object attemptedExpireDate = bindingContext.ValueProvider.GetValue("expireDate").AttemptedValue;
                    DateTime expireDate;
                    DateTime? nullableExpireDate = null;
                    if (attemptedExpireDate != null && DateTime.TryParse(attemptedExpireDate.ToString(), out expireDate))
                        { nullableExpireDate = expireDate; }
                    else if(!string.IsNullOrEmpty(attemptedExpireDate.ToString()))
                        {bindingContext.ModelState.AddModelError("ExpireDate", "Must be valid date.");}

                    blacklistedRange = new BlacklistedIPRange(0, model.BeginningAddress, model.EndingAddress, string.Empty, DateTime.Now, country, nullableExpireDate);
                    blacklistedRange.Notes.Add(new IPRangeNote(0, string.Empty, DateTime.Now, attemptedNote.ToString()));
                }
                else
                {
                    string name = bindingContext.ValueProvider.GetValue("name").AttemptedValue;
                    if(string.IsNullOrEmpty(name))
                    { bindingContext.ModelState.AddModelError("Name", "Must supply a name"); }

                    whitelistedRange = new WhitelistedIPRange(0, model.BeginningAddress, model.EndingAddress, string.Empty, DateTime.Now, country, name);
                    whitelistedRange.Notes.Add(new IPRangeNote(0, string.Empty, DateTime.Now, attemptedNote.ToString()));
                }
            }

            if (type == RangeType.BlackList)
            {
                return blacklistedRange;
            }
            else
            {
                return whitelistedRange;
            }

        }

    }
}
