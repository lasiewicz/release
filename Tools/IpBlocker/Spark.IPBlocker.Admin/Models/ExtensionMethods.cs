﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spark.IPBlocker.Admin
{
    public static class ExtensionMethods
    {
        public static string FilterDomainName(this String str)
        {
            if (str.Contains(@"\"))
            {
                str = str.Substring(str.IndexOf(@"\") + 1);
            }

            return str;
        }

        public static string ToShortDateStringEmptyIfMin(this DateTime dt)
        {
            if (dt == DateTime.MinValue) return string.Empty;
            else return dt.ToShortDateString();
        }

        public static Uri GetBaseUrl(this UrlHelper url)
        {
            Uri contextUri = new Uri(url.RequestContext.HttpContext.Request.Url, url.RequestContext.HttpContext.Request.RawUrl);
            UriBuilder realmUri = new UriBuilder(contextUri) { Path = url.RequestContext.HttpContext.Request.ApplicationPath, Query = null, Fragment = null };
            return realmUri.Uri;
        }

        public static string ActionAbsolute(this UrlHelper url, string actionName, string controllerName)
        {
            return new Uri(GetBaseUrl(url), url.Action(actionName, controllerName)).AbsoluteUri;
        }

    }  
}
