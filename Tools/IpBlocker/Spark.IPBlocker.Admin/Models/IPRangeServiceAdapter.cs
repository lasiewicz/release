﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.IPBlocker.Admin.IPBlockerServiceReference;
using Spark.CommonLibrary.Logging;

namespace Spark.IPBlocker.Admin.Models
{
    /// <summary>
    /// Adapter for interfacing with IP Range Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: Static; this adapter does not need to be instantiated, but it's CloseProxyInstance() should be called at the 
    /// end of the web request (e.g. in Global.asax: Application_EndRequest)
    /// </summary>
    public class IPBlockerServiceAdapter
    {
        public static IPBlockerServiceClient GetProxyInstance()
        {
            IPBlockerServiceClient _Client = null;

            try
            {
                if (HttpContext.Current.Items["IPBlockerServiceClient"] != null)
                {
                    _Client = (IPBlockerServiceClient)HttpContext.Current.Items["IPBlockerServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new IPBlockerServiceClient();
                        HttpContext.Current.Items["IPBlockerServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new IPBlockerServiceClient();
                        HttpContext.Current.Items["IPBlockerServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new IPBlockerServiceClient();
                    HttpContext.Current.Items["IPBlockerServiceClient"] = _Client;
                }


            }
            catch (Exception ex)
            {
                Logger logger = new Logger(typeof(IPBlockerServiceAdapter));
                logger.Error("Error getting IPBlockerServiceClient!", ex);
            }

            return _Client;

        }

        public static void CloseProxyInstance()
        {
            IPBlockerServiceClient _Client;
            if (HttpContext.Current.Items["IPBlockerServiceClient"] != null)
            {
                _Client = (IPBlockerServiceClient)HttpContext.Current.Items["IPBlockerServiceClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    HttpContext.Current.Items.Remove("IPBlockerServiceClient");
                }
            }
        }
    }
}
