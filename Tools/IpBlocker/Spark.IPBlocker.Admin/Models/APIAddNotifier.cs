﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Text;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Admin.Models
{
    public class APIAddNotifier
    {
        string _from = string.Empty;
        string _to = string.Empty;
        string _host = string.Empty;

        public APIAddNotifier()
        {
            _from = System.Configuration.ConfigurationManager.AppSettings["ExpiringTomorrowMailFrom"];
            _to = System.Configuration.ConfigurationManager.AppSettings["ExpiringTomorrowMailTo"];
            _host = System.Configuration.ConfigurationManager.AppSettings["MailHost"];
        }

        public void Notify(IPRange range, string requestingApplication, string username, Country country, string note)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<html><body><p>Hello,</p>An IP has been added to the Block list through the IP Blocker API.</p>");
            sb.Append("<p>The block came from {0}.</p>");
            sb.Append("<p>IP Range: {1}<br>");
            sb.Append("Country: {2}<br>");
            sb.Append("User: {3}<br>");
            sb.Append("Time: {4}<br>");
            sb.Append("Notes: {5}<br></p>");
            sb.Append("<p><font size=-2>Do not reply to the email. It is an automated email from the IP Blocking application.</font></p>");
            sb.Append("</body></html>");

            string body = string.Format(sb.ToString(), requestingApplication, range.BeginningAddress.ToString() + " - " + range.EndingAddress.ToString(), country.Name, username, DateTime.Now.ToString(), note);
            SendEmail(body);
        }

        private void SendEmail(string body)
        {
            MailMessage mail = new MailMessage();
            mail.IsBodyHtml = true;
            mail.Subject = "Automatic IP Block via API";
            mail.From = new MailAddress(_from);
            mail.To.Add(new MailAddress(_to));
            mail.Body = body;

            SmtpClient mailClient = new SmtpClient();
            mailClient.Host = _host;
            mailClient.Port = 25;
            mailClient.Send(mail);
        }
    }
}
