﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.IPBlocker.ValueObjects;

namespace Spark.IPBlocker.Admin.Models.ViewModels
{
    public class IPRangeCreateConfirmViewModel
    {
        public IPRange Range { get; private set; }
        public List<IPRange> OverlappingRanges { get; private set; }

        public IPRangeCreateConfirmViewModel(IPRange range, List<IPRange> overlappingRanges)
        {
            Range = range;
            OverlappingRanges = overlappingRanges;
        }

        public IPRangeCreateConfirmViewModel(IPRange range) :this(range, null) { }

    }
}
