﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.IPBlocker.ValueObjects;

namespace Spark.IPBlocker.Admin.Models.ViewModels
{
    public class IPRangeDetailViewModel
    {
        public IPRangeSummary RangeSummary { get; private set; }
        public PermissionDictionary Permissions { get; private set; }
        public string UserName { get; private set; }

        public IPRangeDetailViewModel(IPRangeSummary rangeSummary, PermissionDictionary permissions, string userName)
        {
            RangeSummary = rangeSummary;
            Permissions = permissions;
            UserName = userName;
        }
    }
}
