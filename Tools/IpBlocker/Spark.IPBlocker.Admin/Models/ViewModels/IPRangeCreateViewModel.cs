﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.IPBlocker.ValueObjects;

namespace Spark.IPBlocker.Admin.Models.ViewModels
{
    public class IPRangeCreateViewModel
    {
        public IPRange Range { get; set; }
        public List<Country> Countries { get; set; }

        public IPRangeCreateViewModel(IPRange range, List<Country> countries)
        {
            Range = range;
            Countries = countries;
        }
    }


}
