﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.IPBlocker.ValueObjects;

namespace Spark.IPBlocker.Admin.Models.ViewModels
{
    public class IPRangeWhitelistDetailViewModel
    {
        public WhitelistedIPRange Range { get; private set; }
        public PermissionDictionary Permissions { get; private set; }
        public string UserName { get; private set; }

        public IPRangeWhitelistDetailViewModel(WhitelistedIPRange range, PermissionDictionary permissions, string userName)
        {
            Range = range;
            Permissions = permissions;
            UserName = userName;
        }
    }
}
