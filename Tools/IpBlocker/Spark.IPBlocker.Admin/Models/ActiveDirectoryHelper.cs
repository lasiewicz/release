﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using Microsoft.Interop.Security.AzRoles;
using System.Configuration;

namespace Spark.IPBlocker.Admin.Models
{
    public class ActiveDirectoryHelper
    {
        public bool CheckUserAccess(IIdentity User, string applicationName, int applicationOperationID)
        {
            if (!User.IsAuthenticated) return false;
            
            WindowsIdentity user;
            
            if(User is FormsIdentity)
            {
                user = new WindowsIdentity(User.Name);
            }
            else
            {
                user = (WindowsIdentity) User;
            }
                        
            bool result = false;
            if (VerifyUserAccess(user, applicationName, applicationOperationID) == 0)
            {
                result = true;
            }
            return result;
        }

        private int VerifyUserAccess(WindowsIdentity User, string applicationName, int applicationOperationID)
        {
            AzAuthorizationStore store = new AzAuthorizationStoreClass();
            store.Initialize(0, ConfigurationManager.ConnectionStrings["ActiveDirectoryAzMan"].ConnectionString, null);
            IAzApplication app = store.OpenApplication(applicationName, null);

            IntPtr token = User.Token;

            IAzClientContext ctx = app.InitializeClientContextFromToken((ulong)token.ToInt64(), null);

            int result = AccessCheck(ctx, "Check Operation Access", applicationOperationID);
            return result;
        }

        private int AccessCheck(IAzClientContext ctx, string objectName, int operation)
        {
            object[] ops = new object[] { (object)operation };

            object[] scopes = new object[] { (object)"" };

            object[] results = (object[])ctx.AccessCheck(objectName, scopes, ops, null, null, null, null, null);
            return (int)results[0];
        }
    }
}
