﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.IPBlocker.Admin.Models
{
    public class PermissionDictionary
    {
        List<ObjectPermission> _permissions = new List<ObjectPermission>();

        public void AddPermission(ObjectPermission permission)
        {
            if (!_permissions.Contains(permission)) _permissions.Add(permission);
        }

        public bool HasPermission(ObjectPermission permission)
        {
            if (_permissions.Contains(permission)) return true; else return false;
        }

    }
}
