﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.IPBlocker.Admin.Models
{
    public class PagingRequest
    {
        public int Page { get; private set; }
        public int RecordsPerPage { get; private set; }
        public string SortName { get; private set; }
        public bool IsDescending { get; private set; }

        public PagingRequest(HttpRequestBase request)
        {
            Page = int.Parse(request.Form["page"]);
            RecordsPerPage = int.Parse(request.Form["rp"]);
            SortName = request.Form["sortname"].ToString();
            string sortorder = request.Form["sortorder"].ToString();

            IsDescending = (sortorder == "undefined" || sortorder == "desc") ? true : false;
        }
    }
}
