﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Spark.IPBlocker.Admin.IPBlockerServiceReference;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;


namespace Spark.IPBlocker.Admin.Models
{
    
    public static class CountryRepository
    {
        const string countryKey = "countries";

        public static Country GetCountry(CountryID id)
        {
            ensureCache();
            List<Country> countries = HttpRuntime.Cache[countryKey] as List<Country>;
            return (from c in countries where c.ID == id select c).FirstOrDefault();
        }

        public static List<Country> GetAllCountries()
        {
            ensureCache();
            return HttpRuntime.Cache[countryKey] as List<Country>;
        }
        
        private static void ensureCache()
        {
            if (HttpRuntime.Cache[countryKey] == null)
            {
                IIPBlockerService client = new IPBlockerServiceClient();
                List<Country> countries = client.GetAllCountries();
                HttpRuntime.Cache.Add(countryKey, countries, null, Cache.NoAbsoluteExpiration, new TimeSpan(3, 0, 0), CacheItemPriority.Normal, null);
            }
        }
    }
}
