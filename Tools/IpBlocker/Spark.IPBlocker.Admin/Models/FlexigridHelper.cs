﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.IPBlocker.Admin.IPBlockerServiceReference;
using Spark.IPBlocker.ValueObjects;
using Spark.IPBlocker.ValueObjects.Enumerations;

namespace Spark.IPBlocker.Admin.Models
{
    public static class FlexigridHelper
    {
        public static FlexigridObject WhitelistedIPRangeToFG(List<WhitelistedIPRange> wip, int page, int totalRows)
        {
            List<FlexigridRow> rows = new List<FlexigridRow>();
            FlexigridObject fgo = new FlexigridObject();
            fgo.page = page;
            fgo.total = totalRows;

            foreach (WhitelistedIPRange range in wip)
            {
                FlexigridRow row = new FlexigridRow();
                row.id = range.ID.ToString();
                row.cell.Add(range.ID.ToString());
                row.cell.Add(range.Name);
                row.cell.Add(range.BeginningAddress.ToString() + " - " + range.EndingAddress.ToString());
                row.cell.Add(range.Country.Name);
                row.cell.Add(range.CreatedBy);
                row.cell.Add(range.CreatedDate.ToShortDateString());
                rows.Add(row);
            }

            fgo.rows = rows;

            return fgo;
        }

        public static FlexigridObject CountrySummariesToFG(List<IPRangeCountrySummary> summaries, int page, int totalRows)
        {
            List<FlexigridRow> rows = new List<FlexigridRow>();
            FlexigridObject fgo = new FlexigridObject();
            fgo.page = page;
            fgo.total = totalRows;

            foreach (IPRangeCountrySummary summary in summaries)
            {
                AccessSummary subSummary = (from s in summary.Summaries where s.AccessType == AccessType.Subscription select s).FirstOrDefault();
                AccessSummary regSummary = (from s in summary.Summaries where s.AccessType == AccessType.Registration select s).FirstOrDefault();
                
                FlexigridRow row = new FlexigridRow();
                row.id = summary.RangeCountry.ID.ToString("d");
                row.cell.Add(summary.RangeCountry.ID.ToString("d"));
                row.cell.Add(summary.RangeCountry.Name);
                row.cell.Add(summary.BlockCount.ToString());
                row.cell.Add(regSummary.NinetyDayAttempts.ToString());
                row.cell.Add(regSummary.TotalAttempts.ToString());
                row.cell.Add(subSummary.NinetyDayAttempts.ToString());
                row.cell.Add(subSummary.TotalAttempts.ToString());
                rows.Add(row); 
            }

            fgo.rows = rows;
            return fgo;
        }

        public static FlexigridObject RangesByCountryToFG(List<IPRangeSummary> summaries, int page, int totalRows)
        {
            List<FlexigridRow> rows = new List<FlexigridRow>();
            FlexigridObject fgo = new FlexigridObject();
            fgo.page = page;
            fgo.total = totalRows;

            foreach (IPRangeSummary summary in summaries)
            {
                AccessSummary subSummary = (from s in summary.Summaries where s.AccessType == AccessType.Subscription select s).FirstOrDefault();
                AccessSummary regSummary = (from s in summary.Summaries where s.AccessType == AccessType.Registration select s).FirstOrDefault();
                BlacklistedIPRange range = summary.Range as BlacklistedIPRange;
                string statusString = "Active";


                if (range.ExpireDate.HasValue)
                {
                    if (range.ExpireDate > DateTime.Now) 
                        statusString = "Inactive on " + range.ExpireDate.Value.ToShortDateString();
                    else
                        statusString = "Inactive";
                }

                FlexigridRow row = new FlexigridRow();
                row.id = summary.Range.ID.ToString("d");
                row.cell.Add(summary.Range.ID.ToString("d"));
                row.cell.Add(summary.Range.BeginningAddress.ToString() + " - " + summary.Range.EndingAddress.ToString());
                row.cell.Add(subSummary.TotalAttempts.ToString());
                row.cell.Add(subSummary.NinetyDayAttempts.ToString());
                row.cell.Add(regSummary.TotalAttempts.ToString());
                row.cell.Add(regSummary.NinetyDayAttempts.ToString());
                row.cell.Add(regSummary.LastAccessDate.ToShortDateStringEmptyIfMin());
                row.cell.Add(statusString);
                row.cell.Add(range.CreatedBy);
                row.cell.Add(range.CreatedDate.ToShortDateString());

                rows.Add(row);
            }

            fgo.rows = rows;
            return fgo;
        }
    }
}

