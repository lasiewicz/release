﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.IPBlocker.Admin.Models
{
    public class FlexigridObject
    {
        public int page;
        public int total;
        public List<FlexigridRow> rows = new List<FlexigridRow>();
    }
}
