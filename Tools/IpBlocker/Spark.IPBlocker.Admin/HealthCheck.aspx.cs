﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;

namespace Spark.IPBlocker.Admin
{
    public partial class HealthCheck : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string MATCHNET_SERVER_DOWN = "Matchnet Server Down";
            string MATCHNET_SERVER_ENABLED = "Matchnet Server Enabled";
            string ServerStatus = MATCHNET_SERVER_DOWN;

            try
            {
                if (isEnabled())
                {
                    ServerStatus = MATCHNET_SERVER_ENABLED;
                }
            }
            catch
            {

            }
            finally
            {
                Response.AddHeader("Health", ServerStatus);
                Response.Write(ServerStatus);
                Response.StatusDescription = ServerStatus; 
                Response.End();
            }
        }

        private bool isEnabled()
        {
            string filePath = Path.GetDirectoryName(Server.MapPath("/")) + @"\deploy.txt";

            bool fileExists = System.IO.File.Exists(filePath);

            if (!fileExists)
            {
                return true;
            }

            return false;
        }
    }
}
