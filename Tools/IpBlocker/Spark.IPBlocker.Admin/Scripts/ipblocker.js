﻿$(document).ready(function() {
    $(".datepicker").datepicker();
    $("#checkIP").click(function(e) { checkIP(e) });
    select_nav();
    highlightTextInput();

});

function dialogModal(opener, target, buttons) {
    // TODO: make a modular dialog Modal
    // This is for messages that are placed on the page at 
    // render and activated by a click.
    $(target).dialog({
        autoOpen: false,
        buttons: buttons,
        modal: true,
        resizable: false
    });
    $(opener).click(function() {
        $(target).dialog('open');
        return false;
    });
}

function popUp(options) {
    var popupMessage = options.message;
    var popupHeight = options.height || 100;
    var popupWidth = options.width || 300;
    var popupModal = options.modal || true;
    var popupResizable = options.resizable || true;
    var popupButtons = options.buttons || {};
    
    if ($('#popup').length == 0) {
        $('<div id="popup"><p></p></div>').appendTo("body");
    }
    $('#popup p').html(popupMessage);
    
    $('#popup').dialog({
            height: popupHeight,
            width: popupWidth,
            modal: popupModal,
            resizable: popupResizable,
            buttons : popupButtons
     });

}

function checkIP(e) {
    e.preventDefault();
    var beginningIP = $('#BeginningAddress_FirstOctet').val() + "." + $('#BeginningAddress_SecondOctet').val() + "." + $('#BeginningAddress_ThirdOctet').val() + "." + $('#BeginningAddress_FourthOctet').val();
    var endingIP = $('#EndingAddress_FirstOctet').val() + "." + $('#EndingAddress_SecondOctet').val() + "." + $('#EndingAddress_ThirdOctet').val() + "." + $('#EndingAddress_FourthOctet').val();

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/IPRange/GetOverlappingAddressesForIP?beginningIPaddress=" + beginningIP + "&endingIPaddress=" + endingIP,
        data: "{}",
        dataType: "json",
        success: function(data) {
            if (data.length > 0) {
                popUp({ message: getOverlappingHTML(data), height: 300 });
            }
            else {
                popUp({ message: "The address does not overlap with any existing ranges" });
            }
        },
        error: function(data){
            popUp({ message: "There was an error checking the address. Make sure it is in the correct format." });
        }
    });

 }

 function getBlacklistlink(id, beginIpaddress, endIpaddress) {
     var link = "<a href='Details/" + id + "'>" + beginIpaddress.FirstOctet + "." + beginIpaddress.SecondOctet + "." + beginIpaddress.ThirdOctet + "." + beginIpaddress.FourthOctet + " - " +
      endIpaddress.FirstOctet + "." + endIpaddress.SecondOctet + "." + endIpaddress.ThirdOctet + "." + endIpaddress.FourthOctet + "</a>";
     return link;
 }

 function getWhitelistlink(id, beginIpaddress, endIpaddress) {
     var link = "<a href='WhitelistDetails/" + id + "'>" + beginIpaddress.FirstOctet + "." + beginIpaddress.SecondOctet + "." + beginIpaddress.ThirdOctet + "." + beginIpaddress.FourthOctet + " - " +
      endIpaddress.FirstOctet + "." + endIpaddress.SecondOctet + "." + endIpaddress.ThirdOctet + "." + endIpaddress.FourthOctet + "</a>";
     return link;
 }
 
function getOverlappingHTML(data)
{
    var htmlString = "<p>The address overlaps with the following ranges:</p><ul>";
    for (iprange in data) {
         var range = data[iprange];
         if(range.Name == undefined)
         {
             htmlString = htmlString + "<li>" + getBlacklistlink(range.ID, range.BeginningAddress, range.EndingAddress);  
         }  
         else
         {
             htmlString = htmlString + "<li>" + getWhitelistlink(range.ID, range.BeginningAddress, range.EndingAddress);  
         }
    }
    htmlString + "</ul>";
    return htmlString;
}

function limitChars(textid, limit) {
    var text = $('#' + textid).val();
    var textlength = text.length;

    if (textlength > limit) {
        $('#' + textid).val(text.substr(0, limit));
        return false;
    }
    else {
        return true;
    }
}

function getDate() {
    var currentTime = new Date()
    var month = currentTime.getMonth() + 1
    var day = currentTime.getDate()
    var year = currentTime.getFullYear()

    return month + "/" + day + "/" + year;
}


function select_nav() {
    var selected = window.location.href;
    var setMenu = false;

    $.each($("#menu a"), function(i, val) {
        if (val == selected || val == selected + "/") {
            $(val).addClass("selected");
            setMenu = true;
        }
    });

    if (setMenu == false) {
        $('#creditHome').addClass("selected");
    }
}

function highlightTextInput() {

    $('input[type="text"]').focus(function() {
        $(this).addClass("focusField");
    });
    $('input[type="text"]').blur(function() {
        $(this).removeClass("focusField");
    });
}

function onDblClick(rowUrl) {
    $('tbody tr td').each(function() {
        $(this).dblclick(function(e) {
            var row = $(this).parents('tr');
            var id = row.children('td').eq(0).children('div').get(0).innerHTML;
            window.location = rowUrl + id;
        });
    });
}