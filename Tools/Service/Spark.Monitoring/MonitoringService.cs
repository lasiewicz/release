﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using Matchnet.Exceptions;
using Spark.Monitoring.ServiceManagers;
using Spark.Monitoring.ValueObjects;

namespace Spark.Monitoring
{
    partial class MonitoringService : Matchnet.RemotingServices.RemotingServiceBase
    {
        private System.ComponentModel.Container components = null;
        private MonitoringSM monitoringSM;

        public MonitoringService()
        {
            InitializeComponent();
        }

        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new MonitoringService() };
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "An unhandled exception occured.", e.ExceptionObject as Exception);
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                monitoringSM = new MonitoringSM();
                base.RegisterServiceManager(monitoringSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
        }

    }
}
