﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Monitoring.ValueObjects.ServiceDefinitions
{
    public interface IMonitoringService
    {
        List<LogEntry> QueryEventLog(SearchQuery query, ref int totalRows);
        DistinctValue GetDistinctValues();
        CachedDistinctValues GetCachedDistinctValues();
    }
}
