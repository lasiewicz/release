﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Monitoring.ValueObjects
{
    /// <summary>
    /// Parameters for searching Event Log
    /// </summary>
    [Serializable]
    public class SearchQuery : Matchnet.IValueObject
    {
        private string machineName;
        private string category;
        private string source;
        private string entryType;
        private DateTime? startDate;
        private DateTime? endDate;
        private string message;
        private int startRow;
        private int pageSize;

        public SearchQuery(string machineName, string category, string source, string entryType, string message, DateTime? startDate, DateTime? endDate, int startRow, int pageSize)
        {
            this.machineName = machineName;
            this.category = category;
            this.source = source;
            this.entryType = entryType;
            this.message = message;
            this.startDate = startDate;
            this.endDate = endDate;
            this.startRow = startRow;
            this.pageSize = pageSize;
        }

        public string MachineName { get { return machineName; } }
        public string Category { get { return category; } }
        public string Source { get { return source; } }
        public string EntryType { get { return entryType; } }
        public string Message { get { return message; } }
        public DateTime? StartDate { get { return startDate; } }
        public DateTime? EndDate { get { return endDate; } }
        public int StartRow { get { return startRow; } }
        public int PageSize { get { return pageSize; } }
    }
}
