﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Monitoring.ValueObjects
{
    [Serializable]
    public class DistinctValue
    {
        List<string> machineNames;
        List<string> source;
        List<string> category;

        public DistinctValue(List<string> machineNames, List<string> source, List<string> category)
        {
            this.machineNames = machineNames;
            this.source = source;
            this.category = category;
        }

        public List<string> MachineNames { get { return machineNames; } }
        public List<string> Source { get { return source; } }
        public List<string> Category { get { return category; } }

    }
}
