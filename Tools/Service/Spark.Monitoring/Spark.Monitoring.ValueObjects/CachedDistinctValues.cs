﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;

namespace Spark.Monitoring.ValueObjects
{
    [Serializable]
    public class CachedDistinctValues : IValueObject, ICacheable, IReplicable
    {
        private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode cacheMode = Matchnet.CacheItemMode.Absolute;
        private Int32 cacheTTLSeconds = 60 * 60;
        List<string> machineNames;
        List<string> source;
        List<string> category;

        public List<string> MachineNames { get { return machineNames; } }
        public List<string> Source { get { return source; } }
        public List<string> Category { get { return category; } }

        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return cachePriority;
            }
            set
            {
                cachePriority = value;
            }
        }

        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return cacheMode;
            }
            set
            {
                cacheMode = value;
            }
        }

        public Int32 CacheTTLSeconds
        {
            get
            {
                return cacheTTLSeconds;
            }
            set
            {
                cacheTTLSeconds = value;
            }
        }

        public CachedDistinctValues(List<string> machineNames, List<string> source, List<string> category)
        {
            this.machineNames = machineNames;
            this.source = source;
            this.category = category;
        }

        public string GetCacheKey()
        {
            return "MONITORING_UNIQUE_VALUES";
        }

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

    }
}
