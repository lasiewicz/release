﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Monitoring.ValueObjects
{
    /// <summary>
    /// ValueObject for Event log entry
    /// </summary>
    [Serializable]
    public class LogEntry : Matchnet.IValueObject
    {
        private Guid logGUID;
        private string machineName;
        private string category;
        private string source;
        private string entryType;
        private DateTime entryDate;
        private DateTime recordInsertDate;
        private string message;

        public LogEntry(Guid guid, string machineName, string category, string source, string entryType, string message, DateTime entryDate, DateTime recordInsertDate)
        {
            this.logGUID = guid;
            this.machineName = machineName;
            this.source = source;
            this.entryType = entryType;
            this.entryDate = entryDate;
            this.recordInsertDate = recordInsertDate;
            this.message = message;
            this.category = category;
        }

        public Guid LogGUID { get { return logGUID; } }
        public string MachineName { get { return machineName; } }
        public string Category { get { return category; } }
        public string Source { get { return source; } }
        public string EntryType { get { return entryType; } }
        public DateTime EntryDate { get { return entryDate; } }
        public DateTime RecordInsertDate { get { return recordInsertDate; } }
        public string Message { get { return message; } }
    }
}
