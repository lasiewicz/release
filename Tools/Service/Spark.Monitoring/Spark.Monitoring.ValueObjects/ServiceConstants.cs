﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Monitoring.ValueObjects
{
    public class ServiceConstants
    {
        public const string SERVICE_CONSTANT = "MONITORING_SVC";
        public const string SERVICE_NAME = "Spark.Monitoring.Service";
        public const string SERVICE_MANAGER_NAME = "MonitoringSM";
    }
}
