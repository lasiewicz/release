﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet;
using Matchnet.Exceptions;
using Spark.Monitoring.BusinessLogic;
using Spark.Monitoring.ValueObjects;
using Spark.Monitoring.ValueObjects.ServiceDefinitions;

namespace Spark.Monitoring.ServiceManagers
{
    public class MonitoringSM : MarshalByRefObject, IMonitoringService, IDisposable , IServiceManager //, IReplicationRecipient
    {
        public MonitoringSM()
        {
        }

        public List<LogEntry> QueryEventLog(SearchQuery query, ref int totalRows)
        {
            try
            {
                return MonitoringBL.Instance.QueryEventLog(query,ref totalRows);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SM Error while fetching event log records.", ex));
            }
        }

        public DistinctValue GetDistinctValues()
        {
            try
            {
                return MonitoringBL.Instance.GetDistinctValues();
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SM Error while fetching distinct values of Machine Name, Source and Category.", ex));
            }
        }
        public CachedDistinctValues GetCachedDistinctValues()
        {
            try
            {
                return MonitoringBL.Instance.GetCachedDistinctValues();
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SM Error while fetching distinct values of Machine Name, Source and Category.", ex));
            }
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void PrePopulateCache()
        {
        }

        public void Dispose()
        {
        }
    }
}
