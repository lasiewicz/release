﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Spark.Monitoring.ValueObjects;
using Spark.Monitoring.ValueObjects.ServiceDefinitions;

namespace Spark.Monitoring.ServiceAdapters
{
    public class MonitoringSA : SABase
    {
        public static readonly MonitoringSA Instance = new MonitoringSA();

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MONITORING_SVC_SA_CONNECTION_LIMIT"));
        }

        private MonitoringSA()
        {
        }

        public List<LogEntry> QueryEventLog(SearchQuery query, ref int totalRows)
        {
            string uri = getServiceManagerUri();
            List<LogEntry> results = new List<LogEntry>();
            try
            {
                results = getService(uri).QueryEventLog(query, ref totalRows);
            }
            catch (Exception ex)
            {
                throw new SAException("SA Error while fetching event log records.", ex);
            }
            finally
            {
                base.Checkin(uri);
            }
            return results;
        }

        public DistinctValue GetDistinctValues()
        {
            string uri = getServiceManagerUri();
            DistinctValue value = null;
            try
            {
                value = getService(uri).GetDistinctValues();
            }
            catch (Exception ex)
            {
                throw new SAException("SA Error while fetching distinct values of Machine Name, Source and Category.", ex);
            }
            finally
            {
                base.Checkin(uri);
            }
            return value;
        }

        public CachedDistinctValues GetCachedDistinctValues()
        {
            string uri = getServiceManagerUri();
            CachedDistinctValues value = null;
            try
            {
                value = getService(uri).GetCachedDistinctValues();
            }
            catch (Exception ex)
            {
                throw new SAException("SA Error while fetching distinct values of Machine Name, Source and Category.", ex);
            }
            finally
            {
                base.Checkin(uri);
            }
            return value;
        }

        private static string getServiceManagerUri()
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MONITORING_SVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private static IMonitoringService getService(string uri)
        {
            try
            {
                return (IMonitoringService)Activator.GetObject(typeof(IMonitoringService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }
    }
}
