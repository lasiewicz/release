﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

using Matchnet.Data;
using Matchnet.Exceptions;
using Spark.Monitoring.ValueObjects;

namespace Spark.Monitoring.BusinessLogic
{
    public class MonitoringBL
    {
        public readonly static MonitoringBL Instance = new MonitoringBL();
        private Matchnet.Caching.Cache cache;
        private int ttl;

        private MonitoringBL()
        {
            cache = Matchnet.Caching.Cache.Instance;
            ttl = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MONITORING_SVC_CACHE_TTL_SECONDS"));
        }
        /// <summary>
        /// Method to return Log Entry Search results
        /// </summary>
        /// <param name="query">Search query parameters</param>
        /// <param name="totalRows">returns total rows for paging purpose</param>
        /// <returns>list of log entrees</returns>
        public List<LogEntry> QueryEventLog(SearchQuery query, ref int totalRows)
        {
            List<LogEntry> logResults = new List<LogEntry>();

            try
            {
                Command command = new Command("mnMonitoring", "dbo.up_getEventLog", 0);
                if (!string.IsNullOrEmpty(query.MachineName))
                {
                    command.AddParameter("@MachineName", SqlDbType.VarChar, ParameterDirection.Input, query.MachineName);
                }
                if (!string.IsNullOrEmpty(query.Source))
                {
                    command.AddParameter("@Source", SqlDbType.VarChar, ParameterDirection.Input, query.Source);
                }
                if (!string.IsNullOrEmpty(query.Category))
                {
                    command.AddParameter("@Category", SqlDbType.VarChar, ParameterDirection.Input, query.Category);
                }
                if (!string.IsNullOrEmpty(query.EntryType))
                {
                    command.AddParameter("@EntryType", SqlDbType.VarChar, ParameterDirection.Input, query.EntryType);
                }
                if (!string.IsNullOrEmpty(query.Message))
                {
                    command.AddParameter("@Message", SqlDbType.VarChar, ParameterDirection.Input, query.Message);
                }
                if (query.StartDate.HasValue)
                {
                    command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, query.StartDate.Value);
                }
                if (query.EndDate.HasValue)
                {
                    command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, query.EndDate.Value);
                }
                command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, query.StartRow);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, query.PageSize);
                command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, totalRows);

                SqlParameterCollection rowCount;
                DataTable logTable = Client.Instance.ExecuteDataTable(command, out rowCount);

                foreach (DataRow row in logTable.Rows)
                {
                    LogEntry entry = new LogEntry(
                                         new Guid(row["GUID"].ToString()),
                                         row["MachineName"].ToString(),
                                         row["Category"].ToString(),
                                         row["Source"].ToString(),
                                         row["EntryType"].ToString(),
                                         row["Message"].ToString(),
                                         Convert.ToDateTime(row["DateTime"]),
                                         Convert.ToDateTime(row["RecordInsertDate"]));
                    logResults.Add(entry);
                }
                totalRows = Convert.ToInt32(rowCount["@TotalRows"].Value);
            }
            catch (Exception ex)
            {
                throw new BLException("BL Error while fetching event log records.", ex);
            }

            return logResults;
        }

        /// <summary>
        /// Method to return unique list of machine names, source and category
        /// </summary>
        /// <returns></returns>
        public DistinctValue GetDistinctValues()
        {
            DistinctValue values = null;
            List<string> machineNames = new List<string>();
            List<string> source = new List<string>();
            List<string> category = new List<string>();
            try
            {
                Command command = new Command("mnMonitoring", "dbo.up_getUniqueValues", 0);
                DataSet resultSet = Client.Instance.ExecuteDataSet(command);
                if (resultSet != null)
                {
                    //Machine Names
                    DataTable machinesTable = resultSet.Tables[0];
                    if (machinesTable != null && machinesTable.Rows.Count > 0)
                    {
                        foreach (DataRow row in machinesTable.Rows)
                        {
                            machineNames.Add(row["MachineName"].ToString());
                        }
                    }
                    //Category
                    DataTable categoryTable = resultSet.Tables[1];
                    if (categoryTable != null && categoryTable.Rows.Count > 0)
                    {
                        foreach (DataRow row in categoryTable.Rows)
                        {
                            category.Add(row["Category"].ToString());
                        }
                    }
                    //Source
                    DataTable sourceTable = resultSet.Tables[2];
                    if (sourceTable != null && sourceTable.Rows.Count > 0)
                    {
                        foreach (DataRow row in sourceTable.Rows)
                        {
                            source.Add(row["Source"].ToString());
                        }
                    }
                    values = new DistinctValue(machineNames, source, category);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("BL Error while fetching distinct values of Machine names, source and category.", ex);
            }
            return values;
        }

        /// <summary>
        /// Method to return unique list of machine names, source and category from cache and if values are not in cache, populates it
        /// </summary>
        /// <returns></returns>
        public CachedDistinctValues GetCachedDistinctValues()
        {
            CachedDistinctValues values = null;
            List<string> machineNames = new List<string>();
            List<string> source = new List<string>();
            List<string> category = new List<string>();
            try
            {
                values = cache.Get("MONITORING_UNIQUE_VALUES") as CachedDistinctValues;
                if (values == null)
                {
                    Command command = new Command("mnMonitoring", "dbo.up_getUniqueValues", 0);
                    DataSet resultSet = Client.Instance.ExecuteDataSet(command);
                    if (resultSet != null)
                    {
                        //Machine Names
                        DataTable machinesTable = resultSet.Tables[0];
                        if (machinesTable != null && machinesTable.Rows.Count > 0)
                        {
                            foreach (DataRow row in machinesTable.Rows)
                            {
                                machineNames.Add(row["MachineName"].ToString());
                            }
                        }
                        //Category
                        DataTable categoryTable = resultSet.Tables[1];
                        if (categoryTable != null && categoryTable.Rows.Count > 0)
                        {
                            foreach (DataRow row in categoryTable.Rows)
                            {
                                category.Add(row["Category"].ToString());
                            }
                        }
                        //Source
                        DataTable sourceTable = resultSet.Tables[2];
                        if (sourceTable != null && sourceTable.Rows.Count > 0)
                        {
                            foreach (DataRow row in sourceTable.Rows)
                            {
                                source.Add(row["Source"].ToString());
                            }
                        }
                        values = new CachedDistinctValues(machineNames, source, category);
                        cache.Insert(values);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException("BL Error while fetching distinct values of Machine names, source and category.", ex);
            }
            return values;
        }
    }
}
