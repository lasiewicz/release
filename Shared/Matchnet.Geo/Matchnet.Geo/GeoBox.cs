using System;
using Matchnet.Geo;

namespace Matchnet.Geo
{
	/// <summary>
	/// Logic for Geographocal Boxes and distance calculations.
	/// </summary>
	/// 
	[Serializable]
	public class GeoBox
	{


		Coordinates coordinates;
		double angleShift=0; //degree
		double radius=0;
		//central box
		Box searchBox;
		bool metricFlag=false;

		public GeoBox(){}
		public GeoBox(Coordinates coord, double angle, double boxradius,bool metric)
		{
			double rad= GeoUtil.DegreeToRadian(angle);
			coordinates=coord;
			metricFlag=metric;
			radius=boxradius;
			searchBox=new Box();
			if(rad!=0)
			{

				searchBox.Key.X=(int)(coord.Longitude/rad);
				searchBox.Key.Y=(int)(coord.Latitude/rad);
				searchBox.Size=GeoUtil.CalculateBoxSize(coord,rad,metricFlag);
				GeoUtil.CalculateSearchArea(searchBox,boxradius);
			}
		}


		public  Coordinates BoxCoordinates
		{
			get {return coordinates;}
		}
		public double AngleShift
		{ 
			get 
			{ return angleShift;}
			set 
			{angleShift = value;}
		}

		public double Radius
		{ 
			get 
			{ return radius;}
			set 
			{radius = value;}
		}

		public bool MetricFlag
		{
			get 
			{return metricFlag;}
			set
			{metricFlag=value;}
		}

		public Box SearchBox
		{
			get{ return searchBox;}
			set{  searchBox=value;}
		}

		
				
	}
}
