using System;

namespace Matchnet.Geo
{
	/// <summary>
	/// Summary description for GeoUtil.
	/// </summary>
	public class GeoUtil
	{
		
		public const double CONVERSION_KM_MILES=(double)0.621371192 ;
		public const double CONVERSION_MILES_KM=(double)1.609344 ; 

		public const double LATITUDE_MAX=(double)Math.PI/2 ; 
		public const double LATITUDE_MIN=(double) - Math.PI/2 ; 

		public const double LONGITUDE_MAX=(double)Math.PI ; 
		public const double LONGITUDE_MIN= (double)- Math.PI ; 

		public const double EARTH_RADIUS_MILES=(double)3963.191 ;

		public static double DegreeToRadian(double degree)
		{
			return (degree * Math.PI/180 );
		}

		public static double CalculateDistance(Coordinates coord1, Coordinates coord2, bool metric)
		{
			try
			{
				double distance=0;
				double multiplier, radius;
				multiplier=(metric?CONVERSION_KM_MILES:1);
				radius=EARTH_RADIUS_MILES * multiplier;
				
				distance=radius * (2.0 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin((coord2.Latitude-coord1.Latitude)/2.0),2) + (Math.Cos(coord1.Latitude) * Math.Cos(coord2.Latitude) * Math.Pow(Math.Sin((coord2.Longitude- coord1.Longitude)/2.0),2)))));

				return distance;
			}
			catch (Exception ex)
			{throw(ex);}

		}


		public static BoxSize CalculateBoxSize(Coordinates startCoord, double radiant,bool metric)
		{
			BoxSize size=null;
			try
			{
				size=new BoxSize();
				double dist=0;	
				double shiftLongitude=MinMax( startCoord.Longitude + radiant,LONGITUDE_MIN,LONGITUDE_MAX);
				Coordinates shiftCoord=new Coordinates(startCoord.Latitude,shiftLongitude);
				dist=CalculateDistance(startCoord,shiftCoord,metric);
				size.SideX=dist;

				double shiftLatitude=MinMax( startCoord.Latitude + radiant,LATITUDE_MIN,LATITUDE_MAX);
				shiftCoord.Latitude=shiftLatitude;
				shiftCoord.Longitude=startCoord.Longitude;
                dist=CalculateDistance(startCoord,shiftCoord,metric);
				size.SideY=dist;

				return size;
			}
			catch (Exception ex)
			{throw(ex);}

		}


		public static void CalculateSearchArea(Box searchBox, double radius)
		{
			try
			{
				int shiftX=0;
				int shiftY=0;
				
				if(searchBox==null)
					return;
				if(searchBox.Size.SideX !=0) 
				{
					shiftX=(int)(radius/searchBox.Size.SideX) +(int)( radius % searchBox.Size.SideX > 0?1:0);
				}
				
				if(searchBox.Size.SideY !=0) 
				{
					shiftY=(int)(radius/searchBox.Size.SideY) + (int)( radius % searchBox.Size.SideY > 0?1:0);
				}

				searchBox.Area.BoxXMin=searchBox.Key.X - shiftX;
				searchBox.Area.BoxXMax=searchBox.Key.X + shiftX;
				
				searchBox.Area.BoxYMin=searchBox.Key.Y - shiftY;
				searchBox.Area.BoxYMax=searchBox.Key.Y + shiftY;


			}
			catch (Exception ex)
			{throw(ex);}

		}


		public static BoxKey GetBoxKey(double longitude, double latitude,double angle)
		{
			double rad= GeoUtil.DegreeToRadian(angle);
			BoxKey key=new BoxKey();
			if(rad!=0)
			{

				key.X=(int)(longitude/rad);
				key.Y=(int)(latitude/rad);
				
			}
			return key;
		}
		
		public static double MinMax(double val, double min, double max)
		{
			double ret=0;
			ret=( val >= min && val <=max?val:( val > max?max:min));
			return ret;
		}
	}
}
