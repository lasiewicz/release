using System;

namespace Matchnet.Geo
{
	/// <summary>
	/// Summary description for Coordinates.
	/// </summary>
	[Serializable]
	public class Coordinates
	{
		double latitude=0;//radian
		double longitude=0; //radian
		public Coordinates(){}
		public Coordinates(double lat, double longit)
		{
			latitude=GeoUtil.MinMax(lat,GeoUtil.LATITUDE_MIN,GeoUtil.LATITUDE_MAX);
			longitude=GeoUtil.MinMax(longit,GeoUtil.LONGITUDE_MIN,GeoUtil.LONGITUDE_MAX);
		}

		public double Latitude
		{ 
			get 
			{ return latitude;}
			set 
			{latitude = value;}

		}


		public double Longitude
		{ 
			get 
			{return longitude;}
			set 
			{longitude = value;}

		}

	}
[Serializable]
	public class BoxSize
	{
		double sideX=0.0;
		double sideY=0.0;
		public double SideX
		{ 
			get 
			{ return sideX;}
			set 
			{sideX = value;}

		}

		public double SideY
		{ 
			get 
			{ return sideY;}
			set 
			{sideY = value;}

		}


	}
[Serializable]
	public class BoxKey
	{
		int x=0;
		int y=0;
		public int X
		{ 
			get 
			{ return x;}
			set 
			{x = value;}

		}

		public int Y
		{ 
			get 
			{ return y;}
			set 
			{y = value;}

		}


	}


[Serializable]
	public class BoxSearchArea
	{
		int boxXMin=0;
		int boxXMax=0;
		int boxYMin=0;
		int boxYMax=0;


		public  int BoxXMin
		{
			get{return boxXMin;}
			set{ boxXMin=value;}
		}

		public  int BoxXMax
		{
			get{return boxXMax;}
			set{ boxXMax=value;}
		}
		public  int BoxYMin
		{
			get{return boxYMin;}
			set{ boxYMin=value;}
		}

		public  int BoxYMax
		{
			get{return boxYMax;}
			set{ boxYMax=value;}
		}
	}
[Serializable]
	public class Box
	{
		BoxSize size;
		BoxKey key;
		BoxSearchArea area;


		public Box()
		{
			size=new BoxSize();
			key=new BoxKey();
			area=new BoxSearchArea();
		}
		public BoxSize Size
		{
			get {return size;}
			set {size=value;}
		}

		public BoxKey Key
		{
			get {return key;}
			set {key=value;}
		}

		public BoxSearchArea Area
		{
			get {return area;}
			set {area=value;}
		}


	}

}
