using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Matchnet.Geo;

namespace TestClient
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox txtLatitude;
		private System.Windows.Forms.TextBox txtLongitude;
		private System.Windows.Forms.TextBox txtAngle;
		private System.Windows.Forms.TextBox txtRadius;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtBoxX;
		private System.Windows.Forms.TextBox txtBoxY;
		private System.Windows.Forms.TextBox txtBoxXMin;
		private System.Windows.Forms.TextBox txtBoxXMax;
		private System.Windows.Forms.TextBox txtBoxYMin;
		private System.Windows.Forms.TextBox txtBoxYMax;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.CheckBox chkMetric;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox txtBoxYSize;
		private System.Windows.Forms.TextBox txtBoxXSize;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtLatitude = new System.Windows.Forms.TextBox();
			this.txtLongitude = new System.Windows.Forms.TextBox();
			this.txtAngle = new System.Windows.Forms.TextBox();
			this.txtRadius = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.txtBoxX = new System.Windows.Forms.TextBox();
			this.txtBoxY = new System.Windows.Forms.TextBox();
			this.txtBoxXMin = new System.Windows.Forms.TextBox();
			this.txtBoxXMax = new System.Windows.Forms.TextBox();
			this.txtBoxYMin = new System.Windows.Forms.TextBox();
			this.txtBoxYMax = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.chkMetric = new System.Windows.Forms.CheckBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.txtBoxYSize = new System.Windows.Forms.TextBox();
			this.txtBoxXSize = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// txtLatitude
			// 
			this.txtLatitude.Location = new System.Drawing.Point(192, 24);
			this.txtLatitude.Name = "txtLatitude";
			this.txtLatitude.Size = new System.Drawing.Size(192, 20);
			this.txtLatitude.TabIndex = 0;
			this.txtLatitude.Text = "";
			// 
			// txtLongitude
			// 
			this.txtLongitude.Location = new System.Drawing.Point(192, 56);
			this.txtLongitude.Name = "txtLongitude";
			this.txtLongitude.Size = new System.Drawing.Size(192, 20);
			this.txtLongitude.TabIndex = 1;
			this.txtLongitude.Text = "";
			// 
			// txtAngle
			// 
			this.txtAngle.Location = new System.Drawing.Point(544, 24);
			this.txtAngle.Name = "txtAngle";
			this.txtAngle.Size = new System.Drawing.Size(72, 20);
			this.txtAngle.TabIndex = 2;
			this.txtAngle.Text = "";
			// 
			// txtRadius
			// 
			this.txtRadius.Location = new System.Drawing.Point(544, 56);
			this.txtRadius.Name = "txtRadius";
			this.txtRadius.Size = new System.Drawing.Size(72, 20);
			this.txtRadius.TabIndex = 3;
			this.txtRadius.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(168, 24);
			this.label1.TabIndex = 4;
			this.label1.Text = "Latitude";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(168, 24);
			this.label2.TabIndex = 5;
			this.label2.Text = "Longitude";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(432, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(104, 24);
			this.label3.TabIndex = 6;
			this.label3.Text = "Angle";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(432, 56);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(104, 24);
			this.label4.TabIndex = 7;
			this.label4.Text = "Radius";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(640, 24);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(80, 32);
			this.button1.TabIndex = 8;
			this.button1.Text = "GetBox";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtBoxX
			// 
			this.txtBoxX.Location = new System.Drawing.Point(232, 176);
			this.txtBoxX.Name = "txtBoxX";
			this.txtBoxX.Size = new System.Drawing.Size(192, 20);
			this.txtBoxX.TabIndex = 9;
			this.txtBoxX.Text = "";
		
			// 
			// txtBoxY
			// 
			this.txtBoxY.Location = new System.Drawing.Point(232, 208);
			this.txtBoxY.Name = "txtBoxY";
			this.txtBoxY.Size = new System.Drawing.Size(192, 20);
			this.txtBoxY.TabIndex = 10;
			this.txtBoxY.Text = "";
			
			// 
			// txtBoxXMin
			// 
			this.txtBoxXMin.Location = new System.Drawing.Point(232, 264);
			this.txtBoxXMin.Name = "txtBoxXMin";
			this.txtBoxXMin.Size = new System.Drawing.Size(192, 20);
			this.txtBoxXMin.TabIndex = 11;
			this.txtBoxXMin.Text = "";
			// 
			// txtBoxXMax
			// 
			this.txtBoxXMax.Location = new System.Drawing.Point(232, 288);
			this.txtBoxXMax.Name = "txtBoxXMax";
			this.txtBoxXMax.Size = new System.Drawing.Size(192, 20);
			this.txtBoxXMax.TabIndex = 12;
			this.txtBoxXMax.Text = "";
			// 
			// txtBoxYMin
			// 
			this.txtBoxYMin.Location = new System.Drawing.Point(232, 320);
			this.txtBoxYMin.Name = "txtBoxYMin";
			this.txtBoxYMin.Size = new System.Drawing.Size(192, 20);
			this.txtBoxYMin.TabIndex = 13;
			this.txtBoxYMin.Text = "";
			// 
			// txtBoxYMax
			// 
			this.txtBoxYMax.Location = new System.Drawing.Point(232, 352);
			this.txtBoxYMax.Name = "txtBoxYMax";
			this.txtBoxYMax.Size = new System.Drawing.Size(192, 20);
			this.txtBoxYMax.TabIndex = 14;
			this.txtBoxYMax.Text = "";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(144, 176);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 24);
			this.label5.TabIndex = 15;
			this.label5.Text = "Box X";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(144, 216);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(56, 24);
			this.label6.TabIndex = 16;
			this.label6.Text = "BoxY";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(136, 264);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(80, 24);
			this.label7.TabIndex = 17;
			this.label7.Text = "Box X Min";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(136, 288);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(80, 24);
			this.label8.TabIndex = 18;
			this.label8.Text = "Box X Max";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(136, 320);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(80, 24);
			this.label9.TabIndex = 19;
			this.label9.Text = "Box Y Min";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(136, 352);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(80, 24);
			this.label10.TabIndex = 20;
			this.label10.Text = "Box Y Max";
			// 
			// chkMetric
			// 
			this.chkMetric.Location = new System.Drawing.Point(24, 96);
			this.chkMetric.Name = "chkMetric";
			this.chkMetric.TabIndex = 21;
			this.chkMetric.Text = "Metric System";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(464, 216);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(56, 24);
			this.label11.TabIndex = 25;
			this.label11.Text = "BoxY size";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(464, 176);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(56, 24);
			this.label12.TabIndex = 24;
			this.label12.Text = "Box X size";
			// 
			// txtBoxYSize
			// 
			this.txtBoxYSize.Location = new System.Drawing.Point(528, 208);
			this.txtBoxYSize.Name = "txtBoxYSize";
			this.txtBoxYSize.Size = new System.Drawing.Size(192, 20);
			this.txtBoxYSize.TabIndex = 23;
			this.txtBoxYSize.Text = "";
			// 
			// txtBoxXSize
			// 
			this.txtBoxXSize.Location = new System.Drawing.Point(528, 176);
			this.txtBoxXSize.Name = "txtBoxXSize";
			this.txtBoxXSize.Size = new System.Drawing.Size(192, 20);
			this.txtBoxXSize.TabIndex = 22;
			this.txtBoxXSize.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(728, 533);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.txtBoxYSize);
			this.Controls.Add(this.txtBoxXSize);
			this.Controls.Add(this.chkMetric);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.txtBoxYMax);
			this.Controls.Add(this.txtBoxYMin);
			this.Controls.Add(this.txtBoxXMax);
			this.Controls.Add(this.txtBoxXMin);
			this.Controls.Add(this.txtBoxY);
			this.Controls.Add(this.txtBoxX);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtRadius);
			this.Controls.Add(this.txtAngle);
			this.Controls.Add(this.txtLongitude);
			this.Controls.Add(this.txtLatitude);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

	
		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				double lat;
				double longit;
				lat=double.Parse(txtLatitude.Text);
				longit=double.Parse(txtLongitude.Text);
				Coordinates c=new Coordinates(lat,longit);

				GeoBox g=new GeoBox(c,double.Parse(txtAngle.Text),double.Parse(txtRadius.Text),chkMetric.Checked);

				txtBoxX.Text=g.SearchBox.Key.X.ToString();
				txtBoxY.Text=g.SearchBox.Key.Y.ToString();

				txtBoxXMin.Text=g.SearchBox.Area.BoxXMin.ToString();
				txtBoxXMax.Text=g.SearchBox.Area.BoxXMax.ToString();
				txtBoxYMin.Text=g.SearchBox.Area.BoxYMin.ToString();
				txtBoxYMax.Text=g.SearchBox.Area.BoxYMax.ToString();

				txtBoxXSize.Text=g.SearchBox.Size.SideX.ToString();
				txtBoxYSize.Text=g.SearchBox.Size.SideY.ToString();

			}
			catch(Exception ex)
			{ MessageBox.Show(ex.Message);}
		}
	}
}
