﻿namespace Spark.Registration.Dictionaries.Constants
{
    public class ExternalValueConstants
    {
        public const string SeekingGenderID = "SeekingGenderID";
        public const string GenderID = "GenderID";
        public const string ZipCode = "ZipCode";
        public const string CountryRegionID = "CountryRegionID";
        public const string XSparkOriginIP  = "X-Spark-OriginIP";
    }
}