﻿namespace Spark.Registration.Dictionaries.Constants
{
    public class URLConstants
    {
        public const string ScenarioName = "Scenario";
        public const string ScenarioID = "ScenarioID";
        public const string EID = "eid";
        public const string PRM = "prm";
        public const string PromotionID = "PromotionID";
        public const string LandingPageID = "lpid";
        public const string LandingPageTestID = "lptestid";
        public const string Luggage = "Luggage";
        public const string LuggageID = "lgid";
        public const string Refcd = "Refcd";
        public const string BannerID = "bid";
        public const string LGID2 = "lgid2";
        public const string Omnivar = "omnivar";
        public const string AID = "aid";
        public const string PID = "pid";
        public const string SID = "sid";
        public const string TSACR = "tsacr";
        public const string Referrer = "referrer";
        public const string StaticRegForm = "staticregform";
        public const string RecaptureID = "RegistrationGUID";
        public const string DeviceType = "dt";
        public const string DestinationURL = "DestinationURL";
        public const string LandingPageIDDefault = "10608";
    }
}