﻿namespace Spark.Registration.Dictionaries.Constants
{
    public class SessionConstants
    {
        public const string MemberID = "MemberID";
        public const string ConfirmationPixelsFired = "ConfirmationPixelsFired";
        public const string PromotionId = "prm";
        public const string Luggage = "lgid";
        public const string Referrer = "referrer";
        public const string EID = "eid";
        public const string LandingPageID = "lpid";
        public const string LandingPageTestID = "lptestid";
        public const string LGID2 = "lgid2";
        public const string Omnivar = "omnivar";
        public const string Refcd = "refcd";
        public const string TSACR = "tsacr";
        public const string BannerId = "bid";
        public const string MemberGendermask = "Gendermask";
        public const string MemberBirthdate = "Birthdate";
        public const string MemberRegionZipCode = "ZipCode";
        public const string RequestFromLandingPage = "RequestFromLandingPage";
        //public const string SessionID = "REGSESSIONID";
        public const string GenerateRegistrationPersistence = "GenerateRegistrationPersistence";
        public const string ScenarioName = "SCENARIO_NAME";
        public const string DeviceType = "devicetype";
        public const string OmnitureFirstPageLoadTime = "OmnitureFirstPageLoadTime";
    }
}