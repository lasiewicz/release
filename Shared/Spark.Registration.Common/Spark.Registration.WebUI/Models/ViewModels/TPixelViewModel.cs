﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Registration.Models.ViewModels
{
    public class TPixelViewModel
    {
        public string Application { get; set; }
        public string MemberID { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string PromotionID { get; set; }
        public string Luggage { get; set; }
        public string BannerID { get; set; }
        public string EID { get; set; }
        public string Channel { get; set; }
        public string Subchannel { get; set; }
        public string DMA { get; set; }
        public string Depth1RegionID { get; set; }
        public string PromoID { get; set; }
        public string PRTID { get; set; }
        public string ScenarioID { get; set; }
        public string StepID { get; set; }
        public string TestID { get; set; }
    }
}