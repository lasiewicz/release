﻿using Spark.Registration.Managers;
using Spark.Registration.Framework.HTTPContextWrappers;

namespace Spark.Registration.Models.ViewModels
{
    public class StepRequestViewModel
    {
        public PopulatedStep Step { get; set; }
        public OmnitureViewModel OmnitureData { get; private set; }
        public int CurrentStepNumber { get; set; }
        public int TotalStepCount { get; set; }
        
        public StepRequestViewModel(){}

        public StepRequestViewModel(PopulatedStep step, int currentStepNumber, int totalStepCount, string sessionID, OmnitureManager omnitureManager)
        {
            Step = step;
            CurrentStepNumber = currentStepNumber;
            TotalStepCount = totalStepCount;
            
            OmnitureData = omnitureManager.GetStepOmnitureModel(currentStepNumber, sessionID);
        }
    }
}
