﻿using System.Collections.Specialized;

namespace Spark.Registration.Models
{
    public class UpdateRegistrationRequest
    {
        public int StepNumber { get; set; }
        public NameValueCollection Values { get; set; }
    }
}