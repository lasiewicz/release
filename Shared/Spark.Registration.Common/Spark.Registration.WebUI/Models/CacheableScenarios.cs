﻿using System.Collections.Generic;
using Matchnet;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;

namespace Spark.Registration.Models
{
    public class CacheableScenarios: ICacheable
    {
        public static string CACHE_KEY = "^AllScenarioCacheKey";
        public List<Scenario> Scenarios { get; private set; }

        public CacheableScenarios(List<Scenario> scenarios)
        {
            Scenarios = scenarios;
        }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.High;
            }
            set{}
        }

        public int CacheTTLSeconds
        {
            get
            {
                return 21600; //6 hours
            }
            set
            {
                
            }
        }

        public string GetCacheKey()
        {
            return CACHE_KEY;
        }

        #endregion
    }
}
