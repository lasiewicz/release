﻿using System.Collections.Generic;
using System.Text;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;

namespace Spark.Registration.Models
{
    public class PopulatedStep
    {
        public string CSSClass { get { return _step.CSSClass; } }
        public string TipText { get { return _step.TipText; } }
        public string HeaderText { get { return _step.HeaderText; } }
        public string TitleText { get { return _step.TitleText; } }
        public RegionData RegionData { get; set; }
        
        private readonly Step _step;
        public List<PopulatedRegControl> PopulatedControls { get; set; }

        public PopulatedStep() {}

        public PopulatedStep(Step step, List<PopulatedRegControl> populatedControls, RegionData regionData)
        {
            _step = step;
            PopulatedControls = populatedControls;
            RegionData = regionData;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Step: " + _step.Order + " ");

            foreach (var control in PopulatedControls)
            {
                var value = control.Control.Name.ToLower() == "password" ? string.Empty : control.SelectedValue;
                sb.Append(control.Control.Name + " - " + value + " ");
            }

            return sb.ToString();
        }
    }
}