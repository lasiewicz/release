﻿using System.Collections.Generic;
using Matchnet;
using Spark.Common.RestConsumer.V2.Models.Content.Region;

namespace Spark.Registration.Models
{
    public class RegionData: ICacheable
    {
        public const string CACHE_KEY = "^RegionData";
        public List<Spark.Registration.Models.Web.Region> Countries { get; set; }
        public List<Spark.Registration.Models.Web.Region> States { get; set; }
        public List<Spark.Registration.Models.Web.Region> Cities { get; set; }
        public int RegionID { get; set; }
        public int CountryRegionID { get; set; }
        public int StateRegionID { get; set; }
        public int CityRegionID { get; set; }
        public string ZipCode { get; set; }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.High;
            }
            set { }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return 86400; //24 hours
            }
            set
            {

            }
        }

        public string GetCacheKey()
        {
            return CACHE_KEY;
        }

        #endregion
    }
}