﻿using System.Collections.Generic;
using Matchnet;
using Spark.Common.RestConsumer.V2.Models.Content.Region;

namespace Spark.Registration.Models
{
    public class CachedRegionData:ICacheable    
    {
        public static string CACHE_KEY = "^CachedRegionData";
        public List<Spark.Registration.Models.Web.Region> Countries { get; set; }
        public Dictionary<int, List<Spark.Registration.Models.Web.Region>> StatesByRegionID { get; set; }
        public Dictionary<string, List<Spark.Registration.Models.Web.Region>> CitiesByZipCode { get; set; }
        public Dictionary<int, List<Spark.Registration.Models.Web.Region>> CitiesByRegionID { get; set; }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.High;
            }
            set { }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return 86400; //24 hours
            }
            set
            {

            }
        }

        public string GetCacheKey()
        {
            return CACHE_KEY;
        }

        #endregion
    }
}