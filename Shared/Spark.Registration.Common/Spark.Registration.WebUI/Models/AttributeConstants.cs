﻿
namespace Spark.Registration.Models
{
    public class AttributeConstants
    {
        public const string GenderMask = "gendermask";
        public const string Birthdate = "birthdate";
        public const string EmailAddress = "EmailAddress";
        public const string UserName = "UserName";
        public const string PromotionID = "PromotionID";
        public const string TrackingRegApplication = "TrackingRegApplication";
        public const string TrackingRegOS = "TrackingRegOS";
        public const string TrackingRegFormFactor = "TrackingRegFormFactor";
        public const string Password = "Password";
        public const string RegsistrationSessionID = "RegsistrationSessionID";
        public const string RegistrationScenarioID = "RegistrationScenarioID";
        public const string Luggage = "Luggage";
        public const string Refcd = "refcd";
        public const string BannerId = "bid";
        public const string RegionId = "regionid";

        public const string LandingPageID = "LandingPageID";
        public const string LandingPageTestID = "LandingPageTestID";
        public const string MingleBannerID = "bid";
        public const string MinglePRM = "prm";
        public const string MingleLuggageID = "lgid";

    }
}
