﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json.Linq;

namespace Spark.Registration.Models
{
    [DataContract(Name = "MingleMappedAttributes")]
    public class MingleMappedAttributes
    {
        [IgnoreDataMember]
        public object JSONObject { get; set; }

        private Dictionary<string, List<int>> _attributeDataMultiValue;
        private Dictionary<string, object> _attributeData;

        [DataMember(Name = "AttributeDataJson")]
        public string AttributeDataJson { get; set; } // JSON dictionary

        [DataMember(Name = "AttributeDataMultiValueJson")]
        public string AttributeDataMultiValueJson { get; set; } // JSON dictionary

        public Dictionary<string, object> AttributeData
        {
            get
            {
                if (_attributeData == null)
                {
                    if (!String.IsNullOrEmpty(AttributeDataJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _attributeData = ser.Deserialize<Dictionary<string, object>>(AttributeDataJson);
                    }
                    else if (null != JSONObject && JSONObject is JObject)
                    {
                        var list = ((JObject)JSONObject).SelectToken("AttributeDataJson").ToList();
                        foreach (JProperty jp in list)
                        {
                            if (null == _attributeData) _attributeData = new Dictionary<string, object>();
                            if (jp.Value.Type == JTokenType.Integer)
                            {
                                _attributeData.Add(jp.Name, jp.Value.ToObject<int>());
                            }
                            else if (jp.Value.Type == JTokenType.Date)
                            {
                                _attributeData.Add(jp.Name, jp.Value.ToObject<DateTime>());
                            }
                            else
                            {
                                _attributeData.Add(jp.Name, jp.Value.ToString());
                            }
                        }
                    }
                    _attributeData = _attributeData ?? new Dictionary<string, object>();
                }
                return _attributeData;
            }

            set { _attributeData = value; }
        }

        public Dictionary<string, List<int>> AttributeDataMultiValue
        {
            get
            {
                if (_attributeDataMultiValue == null)
                {
                    if (!String.IsNullOrEmpty(AttributeDataMultiValueJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _attributeDataMultiValue = ser.Deserialize<Dictionary<string, List<int>>>(AttributeDataMultiValueJson);
                    }
                    else if (null != JSONObject && JSONObject is JObject)
                    {
                        var list = ((JObject)JSONObject).SelectToken("AttributeDataMultiValueJson").ToList();
                        foreach (JProperty jp in list)
                        {
                            if (null == _attributeDataMultiValue) _attributeDataMultiValue = new Dictionary<string, List<int>>();
                            _attributeDataMultiValue.Add(jp.Name, jp.Value.Select(m => m.Value<int>()).ToList<int>());
                        }
                    }
                    _attributeDataMultiValue = _attributeDataMultiValue ?? new Dictionary<string, List<int>>();
                }
                return _attributeDataMultiValue;
            }
            set { _attributeDataMultiValue = value; }
        }
    }
}