﻿using System;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Framework.Helpers;
using Spark.Registration.Interfaces.Web;
using Spark.Registration.Models;
using Spark.Registration.Models.ViewModels;
//using Spark.Registration.Dictionaries.Constants;
using Spark.Registration.Managers;
using System.Configuration;

namespace Spark.Registration.Managers
{
    public class TPixelManager : ManagerBase
    {
        private readonly string _pageName;
        private readonly Scenario _scenario;

        public TPixelManager(string pageName, ICurrentRequest currentRequest, Scenario scenario)
        {
            if (scenario == null)
            {
                throw new ArgumentException("Null scenario passed into OmnitureManager");
            }

            _scenario = scenario;

            if (scenario.ExternalSessionType == ExternalSessionType.MOS)
            {
                _pageName = RegistrationConfiguration.MOSOmniturePagenamePrefix + pageName;
            }
            else
            {
                _pageName = pageName;
            }

            CurrentRequest = currentRequest;
        }

        public TPixelViewModel GetModelTPixel()
        {

            return GetTPixelViewModel();
        }

        private TPixelViewModel GetTPixelViewModel()
        {
            TPixelViewModel tp = new TPixelViewModel();

            switch (Convert.ToInt32(_scenario.DeviceType))
            {
                case 1:
                    tp.Application = "FWS";
                    break;
                case 2:
                    tp.Application = "MOS";
                    break;
                case 3:
                    tp.Application = "TOS";
                    break;

            }


            tp.ScenarioID = _scenario.ID.ToString();

            if (UserSession[SessionConstants.PromotionId] != null)
                tp.PromotionID = UserSession[SessionConstants.PromotionId].ToString();

            if (UserSession[SessionConstants.EID] != null)
                tp.EID = UserSession[SessionConstants.EID].ToString();

            if (UserSession[SessionConstants.Luggage] != null)
                tp.Luggage = UserSession[SessionConstants.Luggage].ToString();

            if (UserSession.MemberId > 0)
            {
                tp.Gender = MemberHelper.GetGenderDescription(UserSession[SessionConstants.MemberGendermask].ToString());

                tp.Age = MemberHelper.GetAge(UserSession[SessionConstants.MemberBirthdate].ToString()).ToString();

                if (UserSession[SessionConstants.MemberRegionZipCode] != null)
                {
                    tp.Depth1RegionID = UserSession[SessionConstants.MemberRegionZipCode].ToString();
                }

                tp.MemberID = UserSession.MemberId.ToString();
            }


            return tp;
        }

    }
}