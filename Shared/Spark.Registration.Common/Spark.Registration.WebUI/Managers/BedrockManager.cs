﻿
namespace Spark.Registration.Managers
{
    public class BedrockManager: ManagerBase
    {
        public bool ShouldRedirectUserToBedrock()
        {
            var cookieManager = new CookieManager();
            //Check the presense of the cookies required to login.
            var shouldRedirect = cookieManager.BedrockAutoLogonCookiesSet();

            if (!shouldRedirect)
            {
                var bedrockSid = cookieManager.GetSessionFromBedrockCookie();
                if (!string.IsNullOrEmpty(bedrockSid))
                {
                    var memberId = ApiAdapter.GetMemberForSession(bedrockSid);
                    if (memberId >0)
                    {
                        shouldRedirect = true;
                    }

                }
            }

            return shouldRedirect;
        }
    }
}