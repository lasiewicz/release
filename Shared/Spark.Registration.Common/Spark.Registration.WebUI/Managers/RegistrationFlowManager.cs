﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Matchnet;
using Spark.Authentication.OAuth.V2;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Member;
using Spark.Common.RestConsumer.V2.Models.MingleMigration;
using Spark.Common.RestConsumer.V2.Models.OAuth2;
using Spark.Registration.Framework;
using Spark.Registration.Framework.Helpers;
using Spark.Registration.Interfaces.Adapters;
using Spark.Registration.Interfaces.Scenarios;
using Spark.Registration.Interfaces.Tracking;
using Spark.Registration.Interfaces.Web;
using Spark.Registration.Models;
using Spark.Registration.Models.ViewModels;
using log4net;
////using Spark.Registration.Dictionaries.Constants; 
using Spark.Registration.Managers;

namespace Spark.Registration.Managers
{
    public class RegistrationFlowManager : ManagerBase
    {
        //TODO - SWITCH THIS TO CM WHEN I FIGURE OUT HOW TO MAKE IT WORK
        private const string USE_CHRISTIAN_REG_API = "USE_CHRISTIAN_REG_API";
        private const double ONE_INCH_IN_CM = 0.393701;
        private const int EXPONENT_OF_NUMBER = 2;

        private static readonly ILog Log = LogManager.GetLogger(typeof (RegistrationFlowManager));

        private IScenarioLoader _scenarioLoader;
        private IMemberLevelTracking _memberLevelTracking;
        private ILoginManager _loginManager;
        private Scenario _scenario;
        private OmnitureManager _omnitureManager;
        private CookieManager _cookieManager;
        private Object regStartLock = new Object();

        public RegistrationFlowManager(IScenarioLoader scenarioLoader = null, ICurrentRequest currentRequest = null)
        {
            if (scenarioLoader != null)
            {
                _scenarioLoader = scenarioLoader;
            }
            if (currentRequest != null)
            {
                CurrentRequest = currentRequest;
            }

            Initialize();
        }

        public IScenarioLoader ScenarioLoader
        {
            get { return _scenarioLoader ?? (_scenarioLoader = new APIScenarioLoader()); }
            set { _scenarioLoader = value; }
        }

        public IMemberLevelTracking MemberLevelTracking
        {
            get
            {
                return _memberLevelTracking ??
                       (_memberLevelTracking =
                           UnityHelper.Resolver.GetService(typeof (IMemberLevelTracking)) as IMemberLevelTracking);
            }
            set { _memberLevelTracking = value; }
        }

        public ILoginManager LoginManagerInstance
        {
            get { return _loginManager ?? (_loginManager = LoginManager.Instance); }
            set { _loginManager = value; }
        }

        public OmnitureManager OmnitureManagerInstance
        {
            get
            {
                return _omnitureManager ??
                       (_omnitureManager = new OmnitureManager("Registration", CurrentRequest, _scenario));
            }
            set { _omnitureManager = value; }
        }

        public CookieManager CookieManagerInstance
        {
            get { return _cookieManager ?? (_cookieManager = new CookieManager()); }
            set { _cookieManager = value; }
        }

        public StepRequestViewModel GetNextStep()
        {
            PopulatedStep step = null;
            var currentStep = 1;
            var nextStep = 2;

            var scenarioHelper = new ScenarioHelper(_scenario);
            var lastCompletedStep = scenarioHelper.GetLastCompletedStep(Persistence);

            if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.CurrentStep]))
            {
                var peristedCurrentStep = Convert.ToInt32(Persistence[PersistenceConstants.CurrentStep]);

                //By futzing around with the browser, it's possible for the cookie to have an incorrect currentstep value. 
                //By ensuring that we use the minimum of what our cookie thinks is the current step and the last actual completed step, 
                //we won't mess up the flow
                currentStep = Math.Min(peristedCurrentStep, lastCompletedStep);
                nextStep = currentStep + 1;
            }

            Log.DebugFormat("Enter GetNextStep - RegSessionID: {0} CurrentStep: {1} NextStep: {2}",
                Persistence[PersistenceConstants.SessionID], currentStep.ToString(), nextStep.ToString());

            if (nextStep <= _scenario.Steps.Count)
            {
                step = GetPopulatedStep(nextStep - 1);
                currentStep = nextStep;
                Persistence.AddValue(PersistenceConstants.CurrentStep, nextStep.ToString());
                Persistence.Persist();

                Log.DebugFormat("GetNextStep Populated Step: {0}", step);
            }

            Log.DebugFormat("GetNextStep Persistence: {0}", Persistence);

            return new StepRequestViewModel(step, currentStep, _scenario.Steps.Count,
                Persistence[PersistenceConstants.SessionID], OmnitureManagerInstance);
        }

        public StepRequestViewModel GetCurrentStep(HttpRequestBase httpRequestBase = null)
        {
            try
            {
                PopulatedStep step = null;
                var currentStep = 1;

                var scenarioHelper = new ScenarioHelper(_scenario);
                var lastCompletedStep = scenarioHelper.GetLastCompletedStep(Persistence);

                if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.CurrentStep]))
                {
                    var peristedCurrentStep = Convert.ToInt32(Persistence[PersistenceConstants.CurrentStep]);

                    //By futzing around with the browser, it's possible for the cookie to have an incorrect currentstep value. 
                    //By ensuring that we don't use the persisted current step if its more than 1 step past the lastcompleted step, 
                    //we won't mess up the flow
                    currentStep = peristedCurrentStep <= lastCompletedStep + 1
                        ? peristedCurrentStep
                        : lastCompletedStep + 1;
                }

                if (currentStep == 1 &&
                    string.IsNullOrEmpty(Persistence[PersistenceConstants.RegistrationStartRecorded]))
                {
                    RecordRegistrationStart(httpRequestBase);
                }

                Log.DebugFormat("Enter GetCurrentStep - RegSessionID: {0} CurrentStep: {1} ",
                    Persistence[PersistenceConstants.SessionID], currentStep.ToString());

                if (currentStep <= _scenario.Steps.Count)
                {
                    step = GetPopulatedStep(currentStep - 1);

                    Persistence.AddValue(PersistenceConstants.CurrentStep, currentStep.ToString());

                    Log.DebugFormat("GetCurrentStep RegSessionID: {0} Populated Step: {1}",
                        Persistence[PersistenceConstants.SessionID], step);
                }

                Log.DebugFormat("GetCurrentStep Persistence: {0}", Persistence);

                return new StepRequestViewModel(step, currentStep, _scenario.Steps.Count,
                    Persistence[PersistenceConstants.SessionID], OmnitureManagerInstance);
            }
            catch (Exception ex)
            {
                Log.Error("Error in RegistrationFlowManager.GetCurrentStep.", ex);
            }
            return null;
        }

        public StepRequestViewModel GetPreviousStep()
        {
            PopulatedStep step = null;
            var currentStep = 1;
            var previousStep = 1;

            if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.CurrentStep]))
            {
                currentStep = Convert.ToInt32(Persistence[PersistenceConstants.CurrentStep]);
                previousStep = currentStep - 1;
            }

            Log.DebugFormat("Enter GetPreviousStep - RegSessionID: {0} CurrentStep: {1} PreviousStep: {2}",
                Persistence[PersistenceConstants.SessionID], currentStep.ToString(), previousStep.ToString());

            if (previousStep >= 1)
            {
                step = GetPopulatedStep(previousStep - 1);
                currentStep = previousStep;
                Persistence.AddValue(PersistenceConstants.CurrentStep, previousStep.ToString());
                Persistence.Persist();

                Log.DebugFormat("GetPreviousStep RegSessionID: {0} Populated Step: {1}",
                    Persistence[PersistenceConstants.SessionID], step);
            }

            Log.DebugFormat("GetPreviousStep Persistence: {0}", Persistence);

            return new StepRequestViewModel(step, currentStep, _scenario.Steps.Count,
                Persistence[PersistenceConstants.SessionID], OmnitureManagerInstance);
        }

        public UpdateRegistrationResponse SubmitStep(List<NameValuePair> values)
        {
            var sb = new StringBuilder();
            var stepDetailsCollector = new Dictionary<string, object>();

            var currentStep = 1;
            var lastCompletedStep = 0;

            //if this is a step with the regionpicker on it, validate that the regioncityid is supplied, and if not return an error
            if ((from nvp in values where nvp.Name.ToLower().Contains("region") select nvp).FirstOrDefault() != null)
            {
                var regioncityNvp =
                    (from nvp in values where nvp.Name.ToLower() == "regioncityid" select nvp).FirstOrDefault();

                if (regioncityNvp == null || string.IsNullOrEmpty(regioncityNvp.Value))
                {
                    var response = new UpdateRegistrationResponse(UpdateRegistrationStatus.Failure);
                    response.ErrorMessages.Add("regioncityid", "Must supply a valid region");
                    return response;
                }
            }

            if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.CurrentStep]))
            {
                currentStep = Convert.ToInt32(Persistence[PersistenceConstants.CurrentStep]);
            }

            sb.Append(string.Format("SubmitStep submitted values for step {0}  RegSessionID: {1} ", currentStep,
                Persistence[PersistenceConstants.SessionID]));

            foreach (var value in values)
            {
                var valueString = value.Name == "password" ? string.Empty : value.Value;
                sb.Append(string.Format("Name: {0} Value: {1} ", value.Name, valueString));
            }

            Log.Debug(sb.ToString());

            if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.LastCompletedStep]))
            {
                lastCompletedStep = Convert.ToInt32(Persistence[PersistenceConstants.LastCompletedStep]);
            }

            foreach (NameValuePair nvp in values)
            {
                if (nvp.Name.ToLower().Contains("region"))
                {
                    PersistRegionPickerData(nvp);
                    if (nvp.Name.ToLower() == "regioncityid")
                    {
                        stepDetailsCollector.Add("regionid", nvp.Value);
                    }
                }
                else
                {
                    RegControl control = _scenario.GetControl(nvp.Name);
                    if (control != null && control.IsAttribute)
                    {
                        Persistence.AddAttributeValue(nvp.Name, nvp.Value);
                        stepDetailsCollector.Add(nvp.Name, nvp.Value);
                    }
                    else
                    {
                        Persistence.AddValue(nvp.Name, nvp.Value);
                    }
                }
            }


            if (currentStep > lastCompletedStep)
            {
                lastCompletedStep = currentStep;
                Persistence.AddValue(PersistenceConstants.LastCompletedStep, lastCompletedStep.ToString());
            }

            Persistence.Persist();
            RecordRegistrationStep(currentStep, stepDetailsCollector);

            Log.DebugFormat("SubmitStep Persistence: {0}", Persistence);

            if (currentStep == (_scenario.Steps.Count))
            {
                return RegisterNewUser();
            }

            return new UpdateRegistrationResponse(UpdateRegistrationStatus.Success);
        }

        private void PersistRegionPickerData(NameValuePair data)
        {
            switch (data.Name.ToLower())
            {
                case "regioncountryid":
                    Persistence.AddValue(PersistenceConstants.RegionCountryID, data.Value);
                    break;
                case "regionstateid":
                    Persistence.AddValue(PersistenceConstants.RegionStateID, data.Value);
                    Persistence.RemoveValue(PersistenceConstants.RegionZipCode);
                    break;
                case "regionzipcode":
                    Persistence.AddValue(PersistenceConstants.RegionZipCode, data.Value);
                    Persistence.RemoveValue(PersistenceConstants.RegionStateID);
                    break;
                case "regioncityid":
                    Persistence.AddValue(PersistenceConstants.RegionCityID, data.Value);
                    Persistence.AddAttributeValue("regionid", data.Value);
                    break;
            }
        }

        private PopulatedStep GetPopulatedStep(int stepNumber)
        {
            var populatedRegControls = new List<PopulatedRegControl>();
            var unpopulatedStep = _scenario.Steps[stepNumber];
            var attributeValues = Persistence.GetAttributeValues();
            RegionData regionData = null;

            foreach (RegControl control in unpopulatedStep.Controls)
            {
                if (control.Name.ToLower() == "regionid")
                {
                    regionData = new RegionManager().PopulateRegionDataFromPersistence();
                }
                string value = string.Empty;

                if (attributeValues.ContainsKey(control.Name.ToLower()))
                {
                    value = attributeValues[control.Name.ToLower()];
                }

                //if the control has any device overrides set for the scenario's device type, set control properties to override properties
                if (control.DeviceOverrides != null && control.DeviceOverrides.Count > 0)
                {
                    var cdo =
                        (from o in control.DeviceOverrides where o.DeviceType == _scenario.DeviceType select o)
                            .FirstOrDefault();
                    if (cdo != null)
                    {
                        if (!string.IsNullOrEmpty(cdo.AdditionalText)) control.AdditionalText = cdo.AdditionalText;
                        if (!string.IsNullOrEmpty(cdo.Label)) control.Label = cdo.Label;
                        if (!string.IsNullOrEmpty(cdo.RequiredErrorMessage))
                            control.RequiredErrorMessage = cdo.RequiredErrorMessage;
                    }
                }

                //if the control has any scenario overrides, set control properties to override properties
                if (control.ScenarioOverrides != null && control.ScenarioOverrides.Count > 0)
                {
                    var cdo =
                        (from o in control.ScenarioOverrides where o.RegScenarioID == _scenario.ID select o)
                            .FirstOrDefault();
                    if (cdo != null)
                    {
                        control.ControlDisplayType = cdo.DisplayType;
                        control.Required = cdo.Required;
                        control.EnableAutoAdvance = cdo.EnableAutoAdvance;
                        if (!string.IsNullOrEmpty(cdo.AdditionalText)) control.AdditionalText = cdo.AdditionalText;
                        if (!string.IsNullOrEmpty(cdo.Label)) control.Label = cdo.Label;
                        if (!string.IsNullOrEmpty(cdo.RequiredErrorMessage))
                            control.RequiredErrorMessage = cdo.RequiredErrorMessage;
                    }
                }

                if (control.Validations != null && control.Validations.Count > 0)
                {
                    foreach (var validation in control.Validations)
                    {
                        if (validation.DeviceOverrides != null && validation.DeviceOverrides.Count > 0)
                        {
                            var vo =
                                (from v in validation.DeviceOverrides
                                    where v.DeviceType == _scenario.DeviceType
                                    select v).FirstOrDefault();
                            if (vo != null && !string.IsNullOrEmpty(vo.ErrorMessage))
                            {
                                validation.ErrorMessage = vo.ErrorMessage;
                            }
                        }
                    }
                }

                populatedRegControls.Add(new PopulatedRegControl(control, value));
            }

            return new PopulatedStep(unpopulatedStep, populatedRegControls, regionData);
        }

        private void Initialize()
        {
            _scenario = ScenarioLoader.GetCurrentScenario();
        }

        public UpdateRegistrationResponse RegisterNewUser()
        {
            var response = new UpdateRegistrationResponse {Status = UpdateRegistrationStatus.Registered};
            var attributeValues = Persistence.GetAttributeValues();
            var emailAddress = attributeValues.GetValue(AttributeConstants.EmailAddress);
            var password = Persistence[AttributeConstants.Password];
            var username = attributeValues.GetValue(AttributeConstants.UserName);

            var registerRequest = CreateExtendedRegisterRequest(emailAddress, password, username, attributeValues);

            if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.SessionTrackingID]))
            {
                registerRequest.SessionTrackingID = Conversion.CInt(Persistence[PersistenceConstants.SessionTrackingID]);
            }

            Log.DebugFormat("Registration beginning. RegSessionID: {0} RegisterData: {1} ",
                Persistence[PersistenceConstants.SessionID], registerRequest);

            // switch on runtime setting between BH and UT api
            var shouldUseChristianRegApi =
                Convert.ToBoolean(RegistrationConfiguration.GetRuntimeSettings(USE_CHRISTIAN_REG_API));

            // call the API
            var registerResult = GetRegistrationResultFromApi(shouldUseChristianRegApi, registerRequest);

            Log.DebugFormat("Registration requested. RegSessionID: {0} Status: {1}",
                Persistence[PersistenceConstants.SessionID], registerResult.RegisterStatus);

            if (registerResult.RegisterStatus == "Success")
            {
                Log.DebugFormat("Registration Success! RegSessionID: {0} MemberID: {1}",
                    Persistence[PersistenceConstants.SessionID], registerResult.MemberId);

                CreateOAuthToken(registerResult, registerRequest);
            }
            else
            {
                Log.Warn("Failed to register user");
                response.Status = UpdateRegistrationStatus.Failure;
                var errorMessage = RegistrationConfiguration.FailedRegistrationMessage;

                if (!string.IsNullOrEmpty(registerResult.RegisterStatus) &&
                    registerResult.RegisterStatus.ToLower() != "failure")
                {
                    errorMessage = registerResult.RegisterStatus;
                }

                response.ErrorMessages.Add("General", errorMessage);
                return response;
            }

            CookieManagerInstance.ClearCookie("MOS_regiobb");

            // No need for external session if we're passing control to UT
            if (!shouldUseChristianRegApi)
            {
                //set external session 
                var externalSessionHelper = new ExternalSessionHelper(CookieManagerInstance, ApiAdapter);
                externalSessionHelper.CreateExternalSession(_scenario.ExternalSessionType, UserSession,
                    Persistence[PersistenceConstants.SessionID]);

                Log.DebugFormat("Bedrock Session Generated. RegSessionID: {0}",
                    Persistence[PersistenceConstants.SessionID]);
            }

            //Read key values from reg cookie into session before clearing the persistence cookie
            UserSession[SessionConstants.MemberGendermask] = attributeValues.GetValue(AttributeConstants.GenderMask);
            UserSession[SessionConstants.MemberBirthdate] = attributeValues.GetValue(AttributeConstants.Birthdate);

            if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionZipCode]))
            {
                UserSession[SessionConstants.MemberRegionZipCode] = Persistence[PersistenceConstants.RegionZipCode];
            }

            //clear old reg values
            Persistence.Clear();

            //this is so the base controller knows not to regenerate the persistence cookie again
            UserSession[SessionConstants.GenerateRegistrationPersistence] = false;

            //put the scenario name in session so that the confirmation page can have it
            UserSession[SessionConstants.ScenarioName] = _scenario.Name;

            return response;
        }

        private ExtendedRegisterResult GetRegistrationResultFromApi(bool shouldUseChristianRegApi,
            ExtendedRegisterRequest registerRequest)
        {
            if (shouldUseChristianRegApi)
            {
                // transform into mingle registration request for easy transport
                var mingleRequest = TransformToMingleRegistrationRequest(registerRequest);

                // map to mingle values
                registerRequest.AttributeData = ApiAdapter.MapToMingleAttributes(mingleRequest);

                //Fix not properly mapped DATA
                FixDataForLDSSO(registerRequest.AttributeData);

                // cleanup non-supported attributes
                RemoveNonSupportedMingleAttributes(registerRequest.AttributeData);

                //adding the prm, lgid and bid
                AddMingleTrackingInfo(registerRequest.AttributeData);

                // move needed attributes to multi value
                RelocateMultiValueAttributes(registerRequest);

                // get a new client
                var registrationAdapter =
                    UnityHelper.Resolver.GetService(typeof (IMingleRegistrationAdapter)) as IMingleRegistrationAdapter;

                // validate with Mingle API
                if (registrationAdapter != null)// && registrationAdapter.ValidateExtended(registerRequest))
                {
                    // register with Mingle API
                    var registrationResult = registrationAdapter.RegisterExtended(new Spark.Registration.Models.Web.MingleRegistrationRequest
                    {
                        attributeData = registerRequest.AttributeData,
                        attributeDataMultiValue = registerRequest.AttributeDataMultiValue,
                        ipaddress = registerRequest.IpAddress,
                        username = registerRequest.UserName,
                        password = registerRequest.Password,
                        emailaddress = registerRequest.EmailAddress
                    });

                    Log.DebugFormat("GetRegistrationResultFromApi registrationAdapter.RegisterExtended registration stage and code:" + registrationResult.Status + " " + registrationResult.Code);

                    if (registrationResult.Code == 200)
                    {
                        return registrationResult.Data;
                    }
                }
            }
            else
            {
                return ApiAdapter.RegisterExtended(registerRequest);
            }

            return new ExtendedRegisterResult {RegisterStatus = "Failed"};
        }

        private static readonly string[] NON_SUPPORTED_ATTRIBUTES = new string[]
        {
            "TrackingRegApplication",
            "TrackingRegOS",
            "TrackingRegFormFactor",
            "source",
            "optional"
        };

        private void RemoveNonSupportedMingleAttributes(Dictionary<string, object> attributes)
        {
            // remove these from the list as they are not supported
            //TrackingRegApplication: 1
            //TrackingRegOS: "WinNT 36"
            //TrackingRegFormFactor: "falsefalseUnknownUnknown480640"
            //source: 55020
            //optional: ""

            foreach (var attributeName in NON_SUPPORTED_ATTRIBUTES)
            {
                var key =
                    attributes.FirstOrDefault(
                        a => String.Equals(a.Key, attributeName, StringComparison.CurrentCultureIgnoreCase)).Key;

                if (!string.IsNullOrWhiteSpace(key))
                {
                    attributes.Remove(key);
                }
            }
        }

        private void AddMingleTrackingInfo(Dictionary<string, object> attributes)
        {

            //affiliate stuff accouring to mingle https://confluence.matchnet.com/display/SOFTENG/RegisterExtended+API+call
            if (UserSession[SessionConstants.PromotionId] != null)
            {
                attributes.Add(AttributeConstants.MinglePRM, Convert.ToInt32(UserSession[SessionConstants.PromotionId]));
            }

            if (UserSession[SessionConstants.BannerId] != null)
            {
                attributes.Add(AttributeConstants.MingleBannerID, Convert.ToInt32(UserSession[SessionConstants.BannerId]));
            }

            // check to see if the luggage has to be an integer in the mingle api. 
            //if (UserSession[SessionConstants.Luggage] != null)
            //{
            //    attributes.Add(AttributeConstants.MingleLuggageID, UserSession[SessionConstants.Luggage]);
            //}

        }



        private static readonly string [] MULTI_VALUE_ATTRIBUTES = new string[]
        {
            "has_ethnicity",
            "extra_language"
        };

        private void RelocateMultiValueAttributes(ExtendedRegisterRequest registerRequest)
        {
            // there should be nothing here at this point
            registerRequest.AttributeDataMultiValue.Clear();

            // find attributes that are multi value
            foreach (var key in MULTI_VALUE_ATTRIBUTES.Where(key => registerRequest.AttributeData.ContainsKey(key)))
            {
                if (!string.IsNullOrWhiteSpace(key))
                {
                    if (key == "has_ethnicity")
                    {
                        double has_ethnicity_value = Convert.ToDouble(registerRequest.AttributeData[key]);
                        registerRequest.AttributeData[key] = Convert.ToInt32(Math.Pow(EXPONENT_OF_NUMBER, has_ethnicity_value));
                    }

                    // add to new list
                    registerRequest.AttributeDataMultiValue.Add(key,
                        new List<int> {Convert.ToInt32(registerRequest.AttributeData[key])});

                    // remove from old list
                    registerRequest.AttributeData.Remove(key);
                }
            }
        }

        //FixDataForChristian
        private void FixDataForLDSSO(Dictionary<string, object> attributeData)
        {
            //Only if this is a mobile device to we do this. It doesnt work on mobile. 
            if (!HttpContext.Current.Request.Browser.IsMobileDevice)
            {
                //lets take care of the height first has_height should be going over in inches
                if (attributeData["has_height"] != null)
                    attributeData["has_height"] = ConvertCmtoInches(Convert.ToInt32(attributeData["has_height"]));
            }


            //lets fix the gender seeking
            if (attributeData["has_gender"] != null)
            {
                var gender = Convert.ToInt32(attributeData["has_gender"]);
                if (gender == 9)
                    attributeData["has_gender"] = 0;
                if (gender == 6)
                    attributeData["has_gender"] = 1;
            }
            
             
            // Fawad recommended to comment this out. There is an method that saves data to MySQL. MySQL has some differences between the 
            // our SQL Server database schema. There may be differences between certain attributes. 
            // Also, he mentioned that the CM Reg site was created after all the LA based sites, which means that the code had to be modified in order to work with
            // the utah owned databases. You will notice this if you were to open up a reg site for a site that is"Owned" by LA/BH such as jdate.com.
            // He also mentioned that there would be a benefit to created a shared library for these projects, so that all the new sites can "reuse" these code libraries.


            //if both exists switch em
            //if(attributeData["has_religion"] != null)
            //{
            //    attributeData.Add("has_religion_raised_in", attributeData["has_religion"]);
            //    attributeData.Remove("has_religion");
            //}

            //if (attributeData["has_religion2"] != null)
            //{
                
            //    attributeData.Add("has_religion", attributeData["has_religion2"]);
            //    attributeData.Remove("has_religion2");

            //}

            


            if (attributeData["has_location_region"] != null)
            {
                //lets fix the location information
                //location_zip or countryid, regioinid, and cityid
                var remove_has_location_region = false;

                if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionZipCode]))
                {
                    attributeData.Add("location_zip", Persistence[PersistenceConstants.RegionZipCode]);
                    remove_has_location_region = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionCountryID]))
                    {
                        attributeData.Add("countryid", Persistence[PersistenceConstants.RegionCountryID]);
                        remove_has_location_region = true;
                    }

                    if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionStateID]))
                    {
                        attributeData.Add("regionid", Persistence[PersistenceConstants.RegionStateID]);
                        remove_has_location_region = true;
                    }

                    if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionCityID]))
                    {
                        attributeData.Add("cityid", Persistence[PersistenceConstants.RegionCityID]);
                        remove_has_location_region = true;
                    }
                }


                if (remove_has_location_region)
                    attributeData["has_location_region"] = null;

            }


        }

        private int ConvertCmtoInches(int cm)
        {
            return Convert.ToInt32(Math.Round(cm * ONE_INCH_IN_CM));
        }

        private ExtendedRegisterRequest CreateExtendedRegisterRequest(string emailAddress, string password, string username,
            Dictionary<string, string> attributeValues)
        {
            var registerRequest = new ExtendedRegisterRequest
            {
                EmailAddress = emailAddress,
                IpAddress = CurrentRequest.ClientIP,
                Password = password,
                UserName = username
            };

            var attributesToUpdate = new Dictionary<string, object>();
            var multiValueAttributesToUpdate = new Dictionary<string, List<int>>();

            foreach (var attribute in attributeValues)
            {
                RegControl control = _scenario.GetControl(attribute.Key);

                CustomAttributeUpdater.AttributeUpdaterDelegate valueSetter;

                if (CustomAttributeUpdater.AttributeUpdaterMethodMap.TryGetValue(attribute.Key.ToLower(), out valueSetter))
                {
                    valueSetter(attributesToUpdate, multiValueAttributesToUpdate, attribute.Value);
                    continue;
                }

                if (attribute.Key.ToLower() != AttributeConstants.EmailAddress.ToLower() && attribute.Key.ToLower() != AttributeConstants.UserName.ToLower())
                {
                    if (control.IsMultiValue)
                    {
                        var values = new List<int>();

                        foreach (var option in control.Options)
                        {
                            if (option.Value.HasValue && option.Value > 0 && (option.Value.Value & Convert.ToInt32(attribute.Value)) == option.Value)
                            {
                                if (attribute.Key.ToLower() == "bodytype" && option.Value != 2)
                                {
                                    values.Add(option.Value.Value);
                                }
                                
                            }
                        }

                        multiValueAttributesToUpdate.Add(attribute.Key, values);
                    }
                    else
                    {
                        object value;
                        switch (control.DataType.ToLower())
                        {
                            case "number":
                                int numValue;
                                var isNumber = int.TryParse(attribute.Value, out numValue);

                                if (!isNumber)
                                {
                                    //this value has to be numeric, and since it's not we should throw an exception so we can log it
                                    throw new Exception(
                                        string.Format(
                                            "Attribute '{0}' was not in numeric format, value was '{1}', RegSessionID: {2}",
                                            attribute.Key, attribute.Value, Persistence[PersistenceConstants.SessionID]));
                                }

                                value = numValue;
                                break;
                            case "bit":
                                bool boolValue;
                                var isBool = bool.TryParse(attribute.Value, out boolValue);

                                if (!isBool)
                                {
                                    //this value has to be boolean, and since it's not we should throw an exception so we can log it
                                    throw new Exception(
                                        string.Format(
                                            "Attribute '{0}' was not in boolean format, value was '{1}', RegSessionID: {2}",
                                            attribute.Key, attribute.Value, Persistence[PersistenceConstants.SessionID]));
                                }

                                value = boolValue;
                                break;
                            default:
                                value = attribute.Value;
                                break;
                        }

                        attributesToUpdate.Add(attribute.Key, value);
                    }
                }
            }

            //reg sceario metadata
            attributesToUpdate.Add(AttributeConstants.RegsistrationSessionID, Persistence[PersistenceConstants.SessionID]);
            attributesToUpdate.Add(AttributeConstants.RegistrationScenarioID, _scenario.ID);

            attributesToUpdate.Add(AttributeConstants.TrackingRegApplication,
                (int) MemberLevelTracking.GetApplication(CurrentRequest, UserSession));
            attributesToUpdate.Add(AttributeConstants.TrackingRegOS, MemberLevelTracking.GetOs(CurrentRequest));
            attributesToUpdate.Add(AttributeConstants.TrackingRegFormFactor, MemberLevelTracking.GetFormFactor(CurrentRequest));

            //affiliate
            if (UserSession[SessionConstants.PromotionId] != null)
            {
                attributesToUpdate.Add(AttributeConstants.PromotionID,
                    Convert.ToInt32(UserSession[SessionConstants.PromotionId]));
            }
            if (UserSession[SessionConstants.Luggage] != null)
            {
                attributesToUpdate.Add(AttributeConstants.Luggage, UserSession[SessionConstants.Luggage]);
            }
            if (UserSession[SessionConstants.Refcd] != null)
            {
                attributesToUpdate.Add(AttributeConstants.Refcd, UserSession[SessionConstants.Refcd]);
            }
            if (UserSession[SessionConstants.BannerId] != null)
            {
                attributesToUpdate.Add(AttributeConstants.BannerId, Convert.ToInt32(UserSession[SessionConstants.BannerId]));
            }
            if (UserSession[SessionConstants.LandingPageID] != null)
            {
                int gottenLandingPageID = -1;
                bool isInt = int.TryParse(UserSession[SessionConstants.LandingPageID].ToString(), out gottenLandingPageID);

                if (isInt && gottenLandingPageID > -1)
                    attributesToUpdate.Add(AttributeConstants.LandingPageID, gottenLandingPageID);
            }


            if (UserSession[SessionConstants.LandingPageTestID] != null)
            {
                int gottenLandingPageTestID = -1;
                bool isInt = int.TryParse(UserSession[SessionConstants.LandingPageTestID].ToString(),
                    out gottenLandingPageTestID);

                if (isInt && gottenLandingPageTestID > -1)
                    attributesToUpdate.Add(AttributeConstants.LandingPageTestID, gottenLandingPageTestID);
            }

            registerRequest.AttributeData = attributesToUpdate;
            registerRequest.AttributeDataMultiValue = multiValueAttributesToUpdate;
            //JS-1573 turning off iovation
            //registerRequest.IovationBlackBox = CookieManagerInstance.ReadFromCookie("MOS_regiobb");
            registerRequest.RegistrationSessionID = Persistence[PersistenceConstants.SessionID];
            registerRequest.RecaptureID = Persistence[PersistenceConstants.RecaptureID] ?? string.Empty;
            return registerRequest;
        }

        private void CreateOAuthToken(ExtendedRegisterResult registerResult, ExtendedRegisterRequest registerRequest)
        {
            UserSession.MemberId = registerResult.MemberId;
            OAuthTokens tokens = new OAuthTokens();
            tokens.AccessExpiresTime = registerResult.AccessExpiresTime;
            tokens.AccessToken = registerResult.AccessToken;
            tokens.ExpiresIn = registerResult.ExpiresIn;
            tokens.IsPayingMember = registerResult.IsPayingMember;
            tokens.MemberId = registerResult.MemberId;
            HttpResponse currentResponse = HttpContext.Current.Response;
            DeviceType deviceType = MemberLevelTracking.GetDeviceType(CurrentRequest, UserSession);
            if (IsSuaLogonEnabled(deviceType))
            {
                Brand brand = RegistrationConfiguration.Brand;
                //for handhelds, use mos sua setting
                string suaCookieDomainSetting = (DeviceType.Handheld == deviceType)
                                                ? "MOS_SUA_TOP_DOMAIN_COOKIE_ENABLED"
                                                : "SUA_TOP_DOMAIN_COOKIE_ENABLED";
                bool isTopLevelDomainEnabled = SettingsService.SettingExistsFromSingleton(suaCookieDomainSetting, brand.Site.Community.Id, brand.Site.Id) &&
                                               SettingsService.GetSettingFromSingleton(suaCookieDomainSetting, brand.Site.Community.Id, brand.Site.Id) ==
                    Boolean.TrueString.ToLower();

                // For UTAH, their sites dont always end in .com, therefore, We need a way to set the cookie [Domain] property to ".ldsso.dev"


                SuaLogonManager.SaveTokensToCookies(new HttpResponseWrapper(currentResponse), tokens, brand, "long",
                    isTopLevelDomainEnabled);

                //  Lets set the sua_at cookie Domain property for sua_at and sua_mid
                // Add two new cookies
                HttpCookie localSuaATCookie = new HttpCookie("sua_at");
                HttpCookie localSuaMIDCookie = new HttpCookie("sua_mid");

                DateTime now = DateTime.Now;
                localSuaATCookie.Expires = now.AddHours(1);
                localSuaMIDCookie.Expires = now.AddHours(1);

                localSuaATCookie.Value = currentResponse.Cookies["sua_at"].Value;
                localSuaMIDCookie.Value = currentResponse.Cookies["sua_mid"].Value;

                localSuaATCookie.Domain = ".atarsha.ldsso.dev";
                localSuaMIDCookie.Domain = ".atarsha.ldsso.dev";

                currentResponse.Cookies.Add(localSuaATCookie);
                currentResponse.Cookies.Add(localSuaMIDCookie);

            }
            else
            {
                LoginManagerInstance.SaveTokensToCookies(currentResponse, tokens, true, registerRequest.EmailAddress,
                    RegistrationConfiguration.OAuthCookieDomain);
//                    tokens = LoginManagerInstance.FetchTokensAndSaveToCookies(registerRequest.EmailAddress,
//                        registerRequest.Password, false,
//                    true, RegistrationConfiguration.OAuthCookieDomain, RegistrationConfiguration.BrandId);

                // Add two new cookies so that we can validate autologin functionaliy on stage/dev. The domains are different than those set in 'SaveTokensToCookies' method
                HttpCookie localSuaATCookie = new HttpCookie("MOS_ACCESS");
                HttpCookie localSuaMIDCookie = new HttpCookie("MOS_MEMBER");

                DateTime now = DateTime.Now;
                localSuaATCookie.Expires = now.AddHours(1);
                localSuaMIDCookie.Expires = now.AddHours(1);

                localSuaATCookie.Value = currentResponse.Cookies["MOS_ACCESS"].Value;
                localSuaMIDCookie.Value = currentResponse.Cookies["MOS_MEMBER"].Value;

                localSuaATCookie.Domain = ".atarsha.ldsso.dev";
                localSuaMIDCookie.Domain = ".atarsha.ldsso.dev";

                currentResponse.Cookies.Add(localSuaATCookie);
                currentResponse.Cookies.Add(localSuaMIDCookie);


            }

            if (tokens == null)
            {
                throw new Exception("failed to get access tokens for member: " + registerResult.MemberId);
            }
        }

        private void RecordRegistrationStart(HttpRequestBase httpRequestBase = null)
        {
            if (string.IsNullOrEmpty(Persistence[PersistenceConstants.RegistrationStartRecorded]))
            {
                var sessionID = Persistence[PersistenceConstants.SessionID];
                var formFactor = MemberLevelTracking.GetFormFactor(CurrentRequest);
                var ipAddress = !string.IsNullOrEmpty(CurrentRequest.ClientIP)
                                    ? BitConverter.ToInt32(IPAddress.Parse(CurrentRequest.ClientIP).GetAddressBytes(), 0)
                                    : Matchnet.Constants.NULL_INT;
                
                lock (regStartLock)
                {
                    Persistence.AddValue(PersistenceConstants.RegistrationStartRecorded, "true");
                    Persistence.Persist();
                    
                    ApiAdapter.RecordRegistrationStart(sessionID, formFactor,
                                                       (int) Common.MemberLevelTracking.Application.FullSite,
                                                       _scenario.ID, ipAddress, httpRequestBase);
                }
            }
        }

        private void RecordRegistrationStep(int stepID, Dictionary<string, object> stepDetails)
        {
            string emailAddress;

            var sessionID = Persistence[PersistenceConstants.SessionID];
            var recaptureID = Persistence[PersistenceConstants.RecaptureID] ?? string.Empty;
            var attributes = Persistence.GetAttributeValues();
            attributes.TryGetValue(AttributeConstants.EmailAddress, out emailAddress);
            
            var allRegFields = attributes.ToDictionary(item => item.Key, item => item.Value);

            var fields = new []
            {
                PersistenceConstants.RegionCountryID,
                PersistenceConstants.RegionStateID,
                PersistenceConstants.RegionCityID,
                PersistenceConstants.RegionZipCode,
                PersistenceConstants.SessionID,
                PersistenceConstants.RegistrationStartRecorded,
                PersistenceConstants.ScenarioName
            };

            foreach (var field in fields.Where(field => Persistence[field] != null))
            {
                allRegFields.Add(field, Persistence[field]);
            }

            recaptureID = ApiAdapter.RecordRegistrationStep(sessionID, stepID, stepDetails, allRegFields, emailAddress, recaptureID);

            if (Persistence[PersistenceConstants.RecaptureID] == null)
            {
                Persistence.AddValue(PersistenceConstants.RecaptureID, recaptureID);
                Persistence.Persist();
            }
        }

        private MingleAttributeMappingRequest TransformToMingleRegistrationRequest(ExtendedRegisterRequest request)
        {
            return new MingleAttributeMappingRequest
            {
                AttributeData = request.AttributeData,
                IpAddress = request.IpAddress,
                AttributeDataMultiValue = request.AttributeDataMultiValue
            };
        }
    }
}
