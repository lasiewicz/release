﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Security;
using Spark.Registration.Models;

namespace Spark.Registration.Managers
{
    public class CookieManager : ManagerBase
    {
        private const string AUTOLOGON_COOKIE_NAME_EMAIL_ADDRESS = "al-juso";
        private const string AUTOLOGON_COOKIE_NAME_PASSWORD = "al-amho";
        private const string REGISTRATION_TRANSFER_COOKIE = "registrationTransfer";
        private const string SESSIONID = "sid";
        private const string MOS_COOKIE_NAME = "MOS_SES_KEY";
        private const string MOS_STAYONFULLSITE_COOKIE_NAME = "sofs";
        private const string SPARK_WS_CRYPT_KEY = "A877C90D";
        private const string CONNECT_COOKIE_NAME = "SPWSCYPHER";

        public string ReadFromCookie(string name)
        {
            var cookie = CurrentRequest.Cookies[name];
            return cookie != null ? Uri.UnescapeDataString(cookie.Value) : string.Empty;
        }

        public void ClearCookie(string name)
        {
            var cookie = new HttpCookie(name) { Expires = DateTime.Now.AddDays(-1) };
            CurrentRequest.Cookies.Add(cookie);
        }

        public bool GenerateBedrockSessionCookie(string sid)
        {
            bool generated = false;
            
            string cookieName = GetBedrockSessionCookieName();

            HttpCookie sessionCookie = CurrentResponse.Cookies[cookieName] ?? new HttpCookie(cookieName);
            
            if(!string.IsNullOrEmpty(sid))
            {
                sessionCookie[SESSIONID] = sid;
                sessionCookie.Domain = "." + RegistrationConfiguration.SiteURI;
                sessionCookie.Expires = DateTime.Now.AddYears(1);
                CurrentResponse.Cookies.Add(sessionCookie);
                generated = true;
            }

            return generated;
        }

        public bool GenerateMOSSessionCookie(string sid)
        {
            bool generated = false;

            HttpCookie sessionCookie = CurrentResponse.Cookies[MOS_COOKIE_NAME] ?? new HttpCookie(MOS_COOKIE_NAME);

            if (!string.IsNullOrEmpty(sid))
            {
                sessionCookie.Value = sid;
                sessionCookie.Domain = "." + RegistrationConfiguration.SiteURI;
                sessionCookie.Expires = DateTime.Now.AddYears(1);
                CurrentResponse.Cookies.Add(sessionCookie);
                generated = true;
            }

            return generated;
        }

        public void GenerateRegistrationTransferCookie(Dictionary<string,string> cookieValues )
        {
            if (cookieValues != null && cookieValues.Count > 0 && CurrentRequest.Cookies[REGISTRATION_TRANSFER_COOKIE] == null)
            {
                var regTransfercookie = new HttpCookie(REGISTRATION_TRANSFER_COOKIE);

                foreach(var key in cookieValues.Keys)
                {
                    regTransfercookie.Values.Add(key, cookieValues[key]);
                }

                CurrentResponse.AppendCookie(regTransfercookie);
                regTransfercookie.Domain = "." + RegistrationConfiguration.SiteURI;
                regTransfercookie.Expires = DateTime.Now.AddYears(1);
            }
        }

        public bool RegistrationTransferCookieSet()
        {
            bool set = false;
            if (CurrentRequest.Cookies.AllKeys.Contains(REGISTRATION_TRANSFER_COOKIE))
            {
                set = true;
            }
            return set;
        }

        public string GetSessionFromBedrockCookie()
        {
            var sid = string.Empty;
            var sessionCookieName = GetBedrockSessionCookieName();
            if (CurrentRequest.Cookies[sessionCookieName] != null && CurrentRequest.Cookies[sessionCookieName].Values.AllKeys.Contains(SESSIONID))
            {
                sid = CurrentRequest.Cookies[sessionCookieName][SESSIONID];
            }

            return sid;
        }

        public bool BedrockAutoLogonCookiesSet()
        {
            return (CurrentRequest.Cookies.AllKeys.Contains(AUTOLOGON_COOKIE_NAME_EMAIL_ADDRESS)
                && CurrentRequest.Cookies.AllKeys.Contains(AUTOLOGON_COOKIE_NAME_PASSWORD));
        }

        
        public string GetBedrockSessionCookieName()
        {
            var env = RegistrationConfiguration.BedrockHost;
            string cookieName = "mnc5";

            // Dev version cookie - This should use the same setting as stg, but don't want to break any existing functionality.
            if (env.ToLower().IndexOf("dev") > -1 || env.ToLower().IndexOf("local") > -1)
            {
                cookieName = "dev" + cookieName;
            }
            else if (env.ToLower().IndexOf("stg") > -1)
            {
                cookieName = "stg" + cookieName;
            }

            return cookieName;
        }


        public bool CheckForSofsCookie()
        {
            //Check if 'sofs' cookie is present and its value is 1. If so, do not redirect to MOS. Stay on full web site.
            bool stayOnFullSite = false;
            if(CurrentRequest.Cookies.AllKeys.Contains(MOS_STAYONFULLSITE_COOKIE_NAME))
            {
                var cookie = CurrentRequest.Cookies[MOS_STAYONFULLSITE_COOKIE_NAME];
                stayOnFullSite = cookie != null && cookie.Value == "1";
            }
            return stayOnFullSite;
        }

        public void GenerateConnectCookie()
        {
            bool setCookie = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_SPWSCYPHER_COOKIE"));

            if (setCookie)
            {
                string encryptedSessionId = HttpUtility.UrlEncode(Crypto.Instance.EncryptText(SPARK_WS_CRYPT_KEY, UserSession.Key));
                HttpCookie connectCookie = CurrentResponse.Cookies[CONNECT_COOKIE_NAME] ?? new HttpCookie(CONNECT_COOKIE_NAME);
                connectCookie.Value = encryptedSessionId;
                connectCookie.Domain = "." + GetConnectCookeDomain();
                connectCookie.Expires = DateTime.Now.AddMonths(1);
                CurrentResponse.Cookies.Add(connectCookie);
            }
        }

        private string GetConnectCookeDomain()
        {
            string devSubdomain = String.Empty;
            string env = "";
            try
            {
                env = RuntimeSettings.GetSetting("ENVIRONMENT_TYPE");
            }
            catch (Exception)
            {
                env = "";
            }
            
            if (env == String.Empty || env == "prod")
                devSubdomain = RegistrationConfiguration.SiteURI;
            else if(!string.IsNullOrEmpty(env) && env.Contains("stg"))
                devSubdomain = RegistrationConfiguration.BedrockHost;
            else
                devSubdomain = env + "." + RegistrationConfiguration.SiteURI;
            return devSubdomain;
        }
    }
}