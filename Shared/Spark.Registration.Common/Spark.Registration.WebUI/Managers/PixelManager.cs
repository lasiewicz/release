﻿using System;
using System.Collections.Generic;
using Matchnet;
using Spark.Common.RestConsumer.V2;
using Spark.Common.RestConsumer.V2.Models.Content.Pixels;
using log4net;
using Spark.Registration.Models;
//using Spark.Registration.Dictionaries.Constants; 

namespace Spark.Registration.Managers
{
    public class PixelManager : ManagerBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ManagerBase));
        
        public List<string> RetrievePixels(string pageName)
        {
            List<string> renderedPixels  = null;

            if (UserSession[SessionConstants.ConfirmationPixelsFired] == null || Conversion.CBool(UserSession[SessionConstants.ConfirmationPixelsFired]) != true)
            {
                var memberID = UserSession.MemberId.ToString();
                var luggageID = UserSession[SessionConstants.Luggage] ?? string.Empty;
                var promotionID = UserSession[SessionConstants.PromotionId] ?? string.Empty;
                var referrer = UserSession[SessionConstants.Referrer] ?? string.Empty;

                renderedPixels = ApiAdapter.GetPixels(memberID, luggageID.ToString(), promotionID.ToString(), referrer.ToString(), pageName);
                UserSession[SessionConstants.ConfirmationPixelsFired] = true;
            }
            return renderedPixels;
        }
    }
}