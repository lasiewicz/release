﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Spark.Registration.Serialization
{
    public class NewtonsoftJsonResult : ActionResult
    {
        /// <summary>
        /// The result object to render using JSON.
        /// </summary>

        public object Result { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "application/json";
            JsonSerializer serializer = new JsonSerializer();
            serializer.Serialize(context.HttpContext.Response.Output, this.Result);
        }
    }
}