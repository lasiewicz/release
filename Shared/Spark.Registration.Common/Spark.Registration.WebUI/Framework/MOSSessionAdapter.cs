﻿using Matchnet.Configuration.ServiceAdapters;
using Spark.Caching;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Interfaces;
using Spark.Registration.Interfaces.Adapters;

namespace Spark.Registration.Framework
{
    public class MOSSessionAdapter : IMOSSessionAdapter
    {
        #region IMOSSessionAdapter Members

        public void CreateSession(Session session, string sessionId)
        {
            MembaseCaching.GetInstance(RuntimeSettings.GetMembaseConfigByBucket("mobilesession")).Insert(sessionId, session);
        }

        #endregion
    }
}