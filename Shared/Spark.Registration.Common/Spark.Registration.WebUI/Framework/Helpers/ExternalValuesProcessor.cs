﻿using Matchnet;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Interfaces;
using Spark.Registration.Interfaces.Persistence;
using Spark.Registration.Interfaces.Web;
using Spark.Registration.Managers;
using Spark.Registration.Models;
//using Spark.Registration.Dictionaries.Constants; 

namespace Spark.Registration.Framework.Helpers
{
    public class ExternalValuesProcessor
    {
        private readonly ICurrentRequest _request;
        private readonly Scenario _scenario;
        private readonly IRegistrationValuePersistence _persistence;
        
        public ExternalValuesProcessor(ICurrentRequest request, Scenario scenario, IRegistrationValuePersistence persistence)
        {
            _request = request;
            _scenario = scenario;
            _persistence = persistence;
        }
        
        public void ProcessPostedValues()
        {
            var attributeValues = _persistence.GetAttributeValues();

            foreach(string key in _request.Params)
            {
                var control = _scenario.GetControl(key);
                if (control != null && attributeValues[key] == null)
                {
                    _persistence.AddAttributeValue(key, _request[key]);
                }
            }

            //if both gender and seekinggender are supplied, translate into gendermask
            if(_request[ExternalValueConstants.GenderID] != null)
            {
                var genderId = Conversion.CInt(_request[ExternalValueConstants.GenderID]);
                if (genderId != Matchnet.Constants.NULL_INT)
                {
                    if(_request[ExternalValueConstants.SeekingGenderID] != null)
                    {
                        int seekingGenderId = Conversion.CInt(_request[ExternalValueConstants.SeekingGenderID]);
                        {
                            if (seekingGenderId != Matchnet.Constants.NULL_INT)
                            {
                                var genderMask = TranslateExternalGenderAndSeekingIntoGendermask(genderId, seekingGenderId);
                                if(genderMask > 0)
                                {
                                    _persistence.AddAttributeValue(AttributeConstants.GenderMask, genderMask.ToString());
                                }
                            }
                        }
                    }
                }
            }

            //also get region if zip and country are supplied
            if (_request[ExternalValueConstants.ZipCode] != null
                && _request[ExternalValueConstants.CountryRegionID] != null
                && Conversion.CInt(_request[ExternalValueConstants.CountryRegionID]) != Matchnet.Constants.NULL_INT)
            {
                
                var regionManager = new RegionManager();
                var cities = regionManager.GetCitiesForZipCode(_request[ExternalValueConstants.ZipCode]);
                if(cities != null && cities.Count > 0)
                {
                    _persistence.AddValue(PersistenceConstants.RegionZipCode, _request[ExternalValueConstants.ZipCode]);
                    _persistence.AddValue(PersistenceConstants.RegionCountryID, _request[ExternalValueConstants.CountryRegionID]);
                    _persistence.AddValue(PersistenceConstants.RegionCityID, cities[0].Id.ToString());
                    _persistence.AddAttributeValue(AttributeConstants.RegionId, cities[0].Id.ToString());
                }
            }

            _persistence.Persist();
        }

        private int TranslateExternalGenderAndSeekingIntoGendermask(int gender, int seeking)
        {
            var translatedGenderId = 0;

            if(gender ==1 && seeking ==8)
            {
                //man seeking woman
                translatedGenderId = 1;
            }
            if(gender==2 && seeking ==4)
            {
                //woman seeking man
                translatedGenderId = 2;
            }
            if(gender==1 && seeking==4)
            {
                //man seeking man
                translatedGenderId = 3;
            }
            if(gender==2 && seeking==8)
            {
                //woman seeking woman
                translatedGenderId = 4;
            }

            return translatedGenderId;
        }
    }
}