﻿using System.Web.Mvc;

namespace Spark.Registration.Framework.Helpers
{
    public static class UnityHelper
    {
        public static IDependencyResolver Resolver
        {
            get { return DependencyResolver.Current; }
        }
    }
}