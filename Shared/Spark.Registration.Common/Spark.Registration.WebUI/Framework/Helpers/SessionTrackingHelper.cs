﻿using Matchnet;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Interfaces;
using Spark.Registration.Interfaces.Adapters;
using Spark.Registration.Interfaces.Persistence;
using Spark.Registration.Models;
//using Spark.Registration.Dictionaries.Constants; 

namespace Spark.Registration.Framework.Helpers
{
    public class SessionTrackingHelper
    {
        public void CreateAndPersistSessionTracking(IRegistrationValuePersistence persistence, IApiAdapter adapter, Session userSession, string clientIP)
        {
            var sessionId = persistence[PersistenceConstants.SessionID];
            var promotionId = userSession[SessionConstants.PromotionId] == null
                                  ? 0
                                  : Conversion.CInt(userSession[SessionConstants.PromotionId], 0);

            var bannerId = userSession[SessionConstants.BannerId] == null
                                  ? 0
                                  : Conversion.CInt(userSession[SessionConstants.BannerId], 0);


            var landingPageId = "";
            if (userSession[SessionConstants.LandingPageID] != null)
            {
                int isIntLandingPageID = 0;
                if (int.TryParse(userSession[SessionConstants.LandingPageID].ToString(), out isIntLandingPageID))
                {
                    landingPageId = isIntLandingPageID.ToString();
                }
            }

            var landingPageTestId = "";
            if(userSession[SessionConstants.LandingPageTestID] != null)
            {
                int isIntLandingPageTestID = 0;

                if (int.TryParse(userSession[SessionConstants.LandingPageTestID].ToString(), out isIntLandingPageTestID))
                {
                    landingPageTestId = isIntLandingPageTestID.ToString();
                }

            }
            var luggageId = userSession[SessionConstants.Luggage] ?? string.Empty;
            var referrer = userSession[SessionConstants.Referrer] ?? string.Empty;

            var trackingId = adapter.CreateSessionTrackingRequest(sessionId, promotionId, luggageId.ToString(), bannerId, clientIP,
                                                                  referrer.ToString(), landingPageId.ToString(), landingPageTestId.ToString());
            if (trackingId > 0)
            {
                persistence.AddValue(PersistenceConstants.SessionTrackingID, trackingId.ToString());
                persistence.Persist();
            }
        }
    }
}