﻿using System;
//using System.Web.Http;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Caching;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Interfaces;
using Spark.Registration.Interfaces.Adapters;
using Spark.Registration.Managers;

namespace Spark.Registration.Framework.Helpers
{
    public class ExternalSessionHelper
    {
        private IApiAdapter _apiAdapter;
        private CookieManager _cookieManager;
        private IMOSSessionAdapter _mosSessionAdapter;

        public CookieManager CookieManagerInstance
        {
            get { return _cookieManager ?? (_cookieManager = new CookieManager()); }
            set { _cookieManager = value; }
        }

        public IApiAdapter APIAdapter
        {
            get
            {
                _apiAdapter = _apiAdapter ?? UnityHelper.Resolver.GetService(typeof(IApiAdapter)) as IApiAdapter;
                return _apiAdapter;
            }
            set { _apiAdapter = value; }
        }

        public IMOSSessionAdapter MOSSessionAdapter
        {
            get { return _mosSessionAdapter ?? (_mosSessionAdapter = UnityHelper.Resolver.GetService(typeof(IMOSSessionAdapter)) as IMOSSessionAdapter); }
            set { _mosSessionAdapter = value; }
        }

        public ExternalSessionHelper(CookieManager cookieManager, IApiAdapter apiAdapter)
        {
            _cookieManager = cookieManager;
            _apiAdapter = apiAdapter;
        }

        public void CreateExternalSession(ExternalSessionType sessionType, Session session, string sessionId)
        {
            switch(sessionType)
            {
                case ExternalSessionType.Bedrock:
                    APIAdapter.CreateBedrockSession(session.MemberId, sessionId);
                    CookieManagerInstance.GenerateBedrockSessionCookie(sessionId);
                    CookieManagerInstance.GenerateConnectCookie();
                    break;
                case ExternalSessionType.MOS:
                    MOSSessionAdapter.CreateSession(session, sessionId);
                    CookieManagerInstance.GenerateMOSSessionCookie(sessionId);
                    break;
            }
        }
    }
}