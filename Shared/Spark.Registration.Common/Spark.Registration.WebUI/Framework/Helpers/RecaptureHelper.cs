﻿using System.Collections.Generic;
using System.Linq;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Interfaces;
using Spark.Registration.Interfaces.Adapters;
using Spark.Registration.Interfaces.Persistence;
using Spark.Registration.Models;
//using Spark.Registration.Dictionaries.Constants; 

namespace Spark.Registration.Framework.Helpers
{
    public class RecaptureHelper
    {
        public void LoadPersistenceFromRecapture(IRegistrationValuePersistence persistence, IApiAdapter adapter, string recaptureID, Scenario currentScenario, List<Scenario> allScenarios)
        {
            var recaptureInfo = adapter.GetRegistrationRecpatureInfo(recaptureID);

            if (recaptureInfo == null || recaptureInfo.RegistrationFields == null)
            {
                //nothing returned, exit out
                return;
            }
            
            if (recaptureInfo.RegistrationFields.ContainsKey(PersistenceConstants.ScenarioName))
            {
                //if we captured the scenario name and it's still a valid scenario, switch the current scenario to it. 
                var recapturedScenario = (from s in allScenarios
                                            where
                                                s.Name.ToLower() ==
                                                recaptureInfo.RegistrationFields[PersistenceConstants.ScenarioName].ToLower()
                                            select s).FirstOrDefault();
                if (recapturedScenario != null)
                {
                    currentScenario = recapturedScenario;
                    persistence.AddValue(PersistenceConstants.ScenarioName, currentScenario.Name);
                }
            }
                
            foreach(var field in recaptureInfo.RegistrationFields)
            {
                var regControl = currentScenario.GetControl(field.Key);
                if(regControl != null)
                {
                    persistence.AddAttributeValue(field.Key, field.Value);    
                }
            }

            if (recaptureInfo.RegistrationFields.ContainsKey(PersistenceConstants.RegionCountryID))
            {
                persistence.AddValue(PersistenceConstants.RegionCountryID, recaptureInfo.RegistrationFields[PersistenceConstants.RegionCountryID]);
            }
            if (recaptureInfo.RegistrationFields.ContainsKey(PersistenceConstants.RegionStateID))
            {
                persistence.AddValue(PersistenceConstants.RegionStateID, recaptureInfo.RegistrationFields[PersistenceConstants.RegionStateID]);
            }
            if (recaptureInfo.RegistrationFields.ContainsKey(PersistenceConstants.RegionCityID))
            {
                persistence.AddValue(PersistenceConstants.RegionCityID, recaptureInfo.RegistrationFields[PersistenceConstants.RegionCityID]);
            }
            if (recaptureInfo.RegistrationFields.ContainsKey(PersistenceConstants.RegionZipCode))
            {
                persistence.AddValue(PersistenceConstants.RegionZipCode, recaptureInfo.RegistrationFields[PersistenceConstants.RegionZipCode]);
            }
            if (recaptureInfo.RegistrationFields.ContainsKey(PersistenceConstants.SessionID))
            {
                persistence.AddValue(PersistenceConstants.SessionID, recaptureInfo.RegistrationFields[PersistenceConstants.SessionID]);
            }
            if (recaptureInfo.RegistrationFields.ContainsKey(PersistenceConstants.RegistrationStartRecorded))
            {
                persistence.AddValue(PersistenceConstants.RegistrationStartRecorded, recaptureInfo.RegistrationFields[PersistenceConstants.RegistrationStartRecorded]);
            }

            //when coming back as a recapture, always start reg from beginning
            persistence.AddValue(PersistenceConstants.CurrentStep, "1");
            persistence.AddValue(PersistenceConstants.LastCompletedStep, "1");

            persistence.Persist();
            
        }
    }
}