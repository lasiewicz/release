﻿using System;
using System.Linq;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Interfaces;
using Spark.Registration.Interfaces.Persistence;

namespace Spark.Registration.Framework.Helpers
{
    public class ScenarioHelper
    {
        private readonly Scenario _scenario;
        
        public ScenarioHelper(Scenario scenario)
        {
            if(scenario == null)
            {
                throw new ArgumentException("Null scenario in ScenarioHelper constructor");
            }
            
            _scenario = scenario;
        }

        public int GetLastCompletedStep(IRegistrationValuePersistence persistence)
        {
            var lastCompletedStep = 0;
            var  attributeValues = persistence.GetAttributeValues();

            //steps should be in correct order, but this is a safety check
            var orderedScenarioSteps = (from s in _scenario.Steps orderby s.Order select s).ToList(); 
            
            for (var stepNumber = 0; stepNumber < _scenario.Steps.Count; stepNumber++ )
            {
                //var step = (from s in _scenario.Steps where s.Order == stepNumber select s).First();

                var stepCompleted = orderedScenarioSteps[stepNumber].Controls.All(control => !control.IsAttribute || (attributeValues.Keys.Contains(control.Name) && (!control.Required || !string.IsNullOrEmpty(attributeValues[control.Name]))));

                if (!stepCompleted) break;
                lastCompletedStep = stepNumber+1;
            }

            return lastCompletedStep;
        }
    }
}