﻿using System.Web.Mvc;
using Spark.Registration.Managers;
using Spark.Registration.Models;

namespace Spark.Registration.Framework.Attributes
{
    public class RedirectToBedrockForLoggedInMembersAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var bedrockManager = new BedrockManager();
            bool shouldRedirect = bedrockManager.ShouldRedirectUserToBedrock();
            if (shouldRedirect)
            {
                filterContext.Result = new RedirectResult("http://" + RegistrationConfiguration.BedrockHost + RegistrationConfiguration.BedrockRedirectPath);
            }
        }
    }


}
