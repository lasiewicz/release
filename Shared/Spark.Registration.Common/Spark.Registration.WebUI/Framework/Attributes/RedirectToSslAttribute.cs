﻿using log4net;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Registration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spark.Registration.Framework.Attributes
{
    public class RedirectToSslAttribute : ActionFilterAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RedirectToSslAttribute));

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Brand brand = RegistrationConfiguration.Brand;
            Log.Info("my site id is" + brand.Site.ToString());
               
            if (filterContext.HttpContext.Request.Url.Host.ToLower().Contains(RegistrationConfiguration.SiteURI.ToLower()))
            {
                Log.Info("does host contain: " + filterContext.HttpContext.Request.Url.Host.ToLower().Contains(RegistrationConfiguration.SiteURI.ToLower()).ToString());
                

                bool requiresSSL = Convert.ToBoolean(RuntimeSettings.GetSetting("USE_REGISTRATION_SSL", brand.Site.Community.Id, brand.Site.Id));
                Log.Info("requiresSSL " + requiresSSL.ToString() + ", brand.Site.Community.Id: " + brand.Site.Community.Id + ", brand.Site.Id: " + brand.Site.Id);
                   
                if (requiresSSL)
                {
                    //ssl will be at lb so we will need to rely on port
                    //lb will append 44 to begining of port to indicate ssl
                    string port = filterContext.HttpContext.Request.ServerVariables["SERVER_PORT"];
                    bool ssl = ((port.Length > 2 && port.StartsWith("44")) || port == "443");
                    
                    if (!ssl)
                    {
                        var httpsURL = filterContext.HttpContext.Request.Url.ToString().Replace("http:", "https:");
                        
                        //if (!httpsURL.Contains("www")) 
                        //{
                        //    httpsURL = httpsURL.Replace("https://", "https://www.");
                        //    Log.Info("SSL redirect url to. server ssl with www :" + httpsURL.ToString());
                        //}
                        

                        UriBuilder uriBuilder = new UriBuilder(httpsURL);
                        uriBuilder.Port = -1;
                        filterContext.Result = new RedirectResult(uriBuilder.Uri.ToString());
                        Log.Info("SSL redirect url to. server ssl: " + ssl.ToString() + ", port: " + port + ", httpsURL: " + httpsURL + ", uriBuilder: " + uriBuilder.Uri.ToString());
                    }
                }
            }
        }
    }
}