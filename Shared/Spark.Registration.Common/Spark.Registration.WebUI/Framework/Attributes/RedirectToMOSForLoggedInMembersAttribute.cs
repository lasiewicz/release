﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Managers;
using Spark.Registration.Models;

namespace Spark.Registration.Framework.Attributes
{
    public class RedirectToMOSForLoggedInMembersAttribute : ActionFilterAttribute
    {
        private Session UserSession { get; set; }
        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            UserSession = filterContext.HttpContext.Items["UserSession"] as Session;
            if (UserSession != null)
            {
                var mosManager = new MOSManager();
                bool shouldRedirect = mosManager.ShouldRedirectToMOS(UserSession);
                if (shouldRedirect)
                {
                    filterContext.Result = new RedirectResult("http://" + RegistrationConfiguration.MOSHost +
                                           RegistrationConfiguration.MOSRedirectPath);
                }
            }
        }
    }
}
