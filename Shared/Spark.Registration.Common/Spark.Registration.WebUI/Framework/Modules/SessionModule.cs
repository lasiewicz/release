﻿using System;
using System.Web;
using System.Web.SessionState;
using Matchnet;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Managers;
using Spark.Registration.Models;
//using Spark.Registration.Dictionaries.Constants; 

namespace Spark.Registration.Framework.Modules
{
    public class SessionModule : IHttpModule, IRequiresSessionState
    {
        public const string COOKIE_SESSION_KEY = "REG_SES_KEY";
        
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += OpenSession;
            context.EndRequest += CloseSession;
        }

        
        public static void OpenSession(object sender, EventArgs e)
        {
            if ((HttpContext.Current.Request.Path.IndexOf(".") != -1) || (HttpContext.Current.Request.Path.IndexOf("healthcheck") != -1))
                return;

            var sessionCookie = HttpContext.Current.Request.Cookies.Get(COOKIE_SESSION_KEY);

            var userSession = new Session();

            if ((sessionCookie == null) || (sessionCookie.Value == string.Empty))
            {
                sessionCookie = new HttpCookie(COOKIE_SESSION_KEY);
            }
           
            foreach(var key in sessionCookie.Values.AllKeys)
            {
                if (key != null)
                {
                    userSession[key] = sessionCookie[key];
                }
            }

            if (!string.IsNullOrEmpty(sessionCookie.Values[SessionConstants.MemberID]))
            {
                userSession.MemberId = Conversion.CInt(sessionCookie.Values[SessionConstants.MemberID]);
            }

            // At this point, we have a valid session, Load it onto context.
            HttpContext.Current.Items["UserSession"] = userSession;
        }


        public static void CloseSession(object sender, EventArgs e)
        {
            if ((HttpContext.Current.Request.Path.IndexOf(".") != -1) || (HttpContext.Current.Request.Path.IndexOf("healthcheck") != -1))
                return;
            
            // Time to update session from a page life cycle. 

            var session = HttpContext.Current.Items["UserSession"] as Session;

            if (session == null) return;

            var sessionCookie = HttpContext.Current.Request.Cookies.Get(COOKIE_SESSION_KEY) ??
                                new HttpCookie(COOKIE_SESSION_KEY);

            foreach(var key in session.Properties.Keys)
            {
                if (session[key] != null)
                {
                    sessionCookie[key] = session[key].ToString();
                }
            }

            if(session.MemberId > 0)
            {
                sessionCookie[SessionConstants.MemberID] = session.MemberId.ToString();
            }

            sessionCookie.Expires = DateTime.Now.AddMinutes(20);

            try
            {
                HttpContext.Current.Response.Cookies.Add(sessionCookie);
            }
            catch (HttpException)
            {
                //just ignore this for now                
            }
            
        }
    }
}
