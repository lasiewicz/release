﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Matchnet;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Framework.Exceptions;
using Spark.Registration.Framework.Helpers;
using Spark.Registration.Interfaces;
using Spark.Registration.Interfaces.Adapters;
using Spark.Registration.Interfaces.Scenarios;
using Spark.Registration.Interfaces.Tracking;
using Spark.Registration.Managers;
using Spark.Registration.Models;
using log4net;
//using Spark.Registration.Dictionaries.Constants; 
using Cache = Matchnet.Caching.Cache;

namespace Spark.Registration.Framework
{
    public class APIScenarioLoader : ManagerBase, IScenarioLoader
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(APIScenarioLoader));
        private IMemberLevelTracking _memberLevelTracking;
        
        private List<Scenario> _allScenarios;

        public APIScenarioLoader(IApiAdapter apiAdapter = null, bool emptyCache = false, HttpRequestBase httpRequestBase = null)
        {
            if (apiAdapter != null)
            {
                ApiAdapter = apiAdapter;
            }

            if (emptyCache) Cache.Instance.Remove(CacheableScenarios.CACHE_KEY);
            
            Intialize();
        }

        public IMemberLevelTracking MemberLevelTracking
        {
            get { return _memberLevelTracking ?? (_memberLevelTracking = UnityHelper.Resolver.GetService(typeof(IMemberLevelTracking)) as IMemberLevelTracking); }
            set { _memberLevelTracking = value; }
        }

        #region IScenarioLoader Members

        public string GetRandomScenarioName()
        {
            DeviceType deviceType = MemberLevelTracking.GetDeviceType(CurrentRequest, UserSession);
            var sessionId = Persistence[PersistenceConstants.SessionID] ?? string.Empty;

            Log.DebugFormat("Getting random scenario - Device type: {0} ({1})", deviceType, sessionId);

            UserSession[SessionConstants.DeviceType] = deviceType;

            //Commenting this out, since product wants traffic from Landing page to view test scenarios as well
            //if this request originated from a landing page, attempt to get control scenario. 
            //If one not found, then fall back to API for random scenario
            //if(UserSession[SessionConstants.RequestFromLandingPage] != null && Conversion.CBool(UserSession[SessionConstants.RequestFromLandingPage]))
            //{
            //    Scenario controlScenario = _allScenarios.GetControlScenario(deviceType);
            //    if (controlScenario != null)
            //    {
            //        Log.DebugFormat("Getting random scenario - Control scenario chosen: {0} ({1})", controlScenario.Name, sessionId);
            //        return controlScenario.Name;
            //    }
            //}

            //if there's only 1 scenario, no sense in making REST call to get a random one
            var deviceScenarios = ( from s in _allScenarios where s.DeviceType == deviceType select s).Count();
            if (deviceScenarios == 1)
            {
                var scenarioName = (from s in _allScenarios where s.DeviceType == deviceType select s).First().Name;
                Log.DebugFormat("Getting random scenario - Only one scenario for device type: {0} ({1})", scenarioName, sessionId);
                return scenarioName;
            }

            Log.DebugFormat("Getting random scenario - Calling API to get random scenario name: ({0})", sessionId);
            return ApiAdapter.GetRandomScenarioName(deviceType);
        }

        public List<Scenario> GetScenarios()
        {
            return _allScenarios;
        }

        public Scenario GetNamedScenario(string name)
        {
            return (from Scenario s in _allScenarios where s.Name.ToLower() == name.ToLower() select s).FirstOrDefault();
        }

        public Scenario GetCurrentScenario()
        {
            Log.DebugFormat("Getting current scenario ({0})", Persistence[PersistenceConstants.SessionID]);

            string scenarioName = string.Empty;

            bool scenarioAlreadyChosen = false;
            if (Persistence.HasValues && !string.IsNullOrEmpty(Persistence[PersistenceConstants.ScenarioName]))
            {
                scenarioName = Persistence[PersistenceConstants.ScenarioName];
                if (GetNamedScenario(scenarioName) != null)
                {
                    Log.DebugFormat("Scenario retrieved from persistence: {0} ({1})", scenarioName, Persistence[PersistenceConstants.SessionID]);
                    scenarioAlreadyChosen = true;
                }
                else
                {
                    //the scenario in persistence is invalid for some reason (possibly it was removed since the user's last visit)
                    //so set the name back to empty, this way we will look in the request or get a new random scenario name
                    Log.DebugFormat("Scenario retrieved from persistence: {0} was invalid, moving on ({1})", scenarioName, Persistence[PersistenceConstants.SessionID]);
                    scenarioName = string.Empty;
                }
            }

            //check request first
            if (scenarioName == string.Empty)
            {
                scenarioName = GetScenarioFromRequest();
                Log.DebugFormat("Scenario retrieved from request: {0} ({1})", scenarioName, Persistence[PersistenceConstants.SessionID]);
            }

            //if still empty, pick a random scenario from the API
            if (scenarioName == string.Empty)
            {
                scenarioName = GetRandomScenarioName();
                Log.DebugFormat("Scenario retrieved randomly: {0} ({1})", scenarioName, Persistence[PersistenceConstants.SessionID]);
            }

            if (!scenarioAlreadyChosen)
            {
                Persistence.AddValue(PersistenceConstants.ScenarioName, scenarioName);
                Persistence.Persist();
            }

            var scenario = GetNamedScenario(scenarioName);

            if (scenario == null)
            {
                //this is a massive error, we don't have a valid scenario, so throw an exception
                throw new ScenarioException("No valid scenarion chosen in GetCurrentScenario().");
            }

            return scenario;
        }

        private string GetScenarioFromRequest()
        {
            string scenarioName = string.Empty;

            if(!string.IsNullOrEmpty(CurrentRequest[URLConstants.ScenarioName]))
            {
                Scenario scenarioByName =
                    (from Scenario s in _allScenarios
                     where s.Name.ToLower() == CurrentRequest[URLConstants.ScenarioName].ToLower()
                     select s).FirstOrDefault();

                if (scenarioByName != null) scenarioName = scenarioByName.Name;
            }

            return scenarioName;
        }

        private void Intialize()
        {
            var cachedScenarios = Cache.Instance.Get(CacheableScenarios.CACHE_KEY) as CacheableScenarios;
            if (cachedScenarios == null || cachedScenarios.Scenarios == null || cachedScenarios.Scenarios.Count < 1)
            {
                _allScenarios = ApiAdapter.GetScenarios();
                if(_allScenarios == null)//API Error
                    throw new ScenarioException("Unable to retreive scenarios from API");

                Cache.Instance.Add(CacheableScenarios.CACHE_KEY, new CacheableScenarios(_allScenarios), null, DateTime.Now.AddHours(6), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
            }
            else
            {
                _allScenarios = cachedScenarios.Scenarios;
            }
        }

        #endregion
    }
}