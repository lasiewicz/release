﻿using System.Web;
using Spark.Registration.Interfaces;
using Spark.Registration.Interfaces.Web;

namespace Spark.Registration.Framework.HTTPContextWrappers
{
    public class CurrentBrowserCapabilities : ICurrentBrowserCapabilities
    {
        #region ICurrentBrowserCapabilities Members

        public string Platform
        {
            get { return HttpContext.Current.Request.Browser.Platform; }
        }

        public int MajorVersion
        {
            get { return HttpContext.Current.Request.Browser.MajorVersion; }
        }

        public bool IsMobileDevice
        {
            get { return HttpContext.Current.Request.Browser.IsMobileDevice; }
        }

        public string MobileDeviceManufacturer
        {
            get { return HttpContext.Current.Request.Browser.MobileDeviceManufacturer; }
        }

        public string MobileDeviceModel
        {
            get { return HttpContext.Current.Request.Browser.MobileDeviceModel; }
        }

        public int ScreenPixelsHeight
        {
            get { return HttpContext.Current.Request.Browser.ScreenPixelsHeight; }
        }

        public int ScreenPixelsWidth
        {
            get { return HttpContext.Current.Request.Browser.ScreenPixelsWidth; }
        }

        public string this[string index]
        {
            get { return HttpContext.Current.Request.Browser[index]; }
        }

        #endregion
    }
}