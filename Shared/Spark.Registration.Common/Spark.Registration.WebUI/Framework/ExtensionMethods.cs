﻿using System.Collections.Generic;
using System.Linq;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;

namespace Spark.Registration.Framework
{
    public static class ExtensionMethods
    {
        public static string GetValue(this Dictionary<string, string> dict, string key)
        {
            string value = (from item in dict where item.Key.ToLower() == key.ToLower() select item.Value).FirstOrDefault();
            if(!string.IsNullOrEmpty(value))
            {
                return value;
            }

            return string.Empty;
        }

        public static RegControl GetControl(this Scenario scenario, string controlName)
        {
            return
                (from step in scenario.Steps
                 from control in step.Controls
                 where control.Name.ToLower() == controlName.ToLower()
                 select control).FirstOrDefault();
        }

        public static List<RegControl> GetAllControls(this Scenario scenario)
        {
            return
                (from step in scenario.Steps
                 from control in step.Controls
                 where control.IsAttribute 
                 select control).ToList();
        }

        public static Scenario GetControlScenario(this List<Scenario> scenarios, DeviceType deviceType)
        {
            return scenarios.Where(s => s.IsControl && s.DeviceType == deviceType).FirstOrDefault();
        }
    }   
}
