﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.Registration.Models.Web
{
    public class MingleRegionRequest
    {
        public int CountryID { get; set; }
        public int RegionID { get; set; }
    }
}
