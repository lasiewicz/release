﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Spark.Registration.Models.Web
{
    public class MingleRegistrationRequest
    {
        public Dictionary<string, object> attributeData { get; set; }

        public Dictionary<string, List<int>> attributeDataMultiValue { get; set; }

        public string username { get; set; }

        public string emailaddress { get; set; }

        public string password { get; set; }

        public string ipaddress { get; set; }

        public string attributeDataJson
        {
            get { return JsonConvert.SerializeObject((object) this.attributeData); }
        }

        public string attributeDataMultiValueJson
        {
            get { return JsonConvert.SerializeObject((object) this.attributeDataMultiValue); }
        }

        public override string ToString()
        {
            /*
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("ExtendedRegisterRequest" + Environment.NewLine);
            stringBuilder.AppendLine(string.Format("EmailAddress: {0}, IP: {1} Password: {2} UserName: {3} {4} ",
                (object) this.emailaddress, (object) this.ipaddress, (object) string.Empty, (object) this.username,
                (object) Environment.NewLine));
            stringBuilder.AppendLine("Attributes");
            foreach (string index in this.attributeData.Keys)
            {
                object obj = index.ToLower() == "password" ? (object) string.Empty : this.attributeData[index];
                stringBuilder.AppendLine(string.Format("Key: {0} Value: {1} ", (object) index, obj));
            }
            if (this.attributeDataMultiValue != null && this.attributeDataMultiValue.Count > 0)
            {
                stringBuilder.AppendLine("MultiValueAttributes");
                foreach (string index in this.attributeDataMultiValue.Keys)
                {
                    stringBuilder.Append(Environment.NewLine + string.Format("Key: {0} Values: ", (object) index));
                    int num1 = 0;
                    foreach (int num2 in this.attributeDataMultiValue[index])
                    {
                        ++num1;
                        stringBuilder.Append(num2.ToString());
                        if (num1 != this.attributeDataMultiValue[index].Count)
                            stringBuilder.Append(", ");
                    }
                }
            }
            return ((object) stringBuilder).ToString();
            */
            return JsonConvert.SerializeObject(this);
        }
    }

}
