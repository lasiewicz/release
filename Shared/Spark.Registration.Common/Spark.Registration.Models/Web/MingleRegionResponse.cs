﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.Registration.Models.Web
{
    public class MingleRegionResponse : MingleResponse
    {
        public List<Region> data { get; set; }
    }

    public class Region
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Depth { get; set; }
    }
}

