﻿using System;
using System.Collections.Generic;
using System.Web;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Common.RestConsumer.V2.Models.Content.Promotion;
using Spark.Common.RestConsumer.V2.Models.Content.Region;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Content.RegistrationActivityRecording;
using Spark.Common.RestConsumer.V2.Models.Member;
using Spark.Common.RestConsumer.V2.Models.MingleMigration;

namespace Spark.Registration.Interfaces.Adapters
{
    public interface IApiAdapter
    {
        ExtendedRegisterResult RegisterExtended(ExtendedRegisterRequest register);

        void RecordRegistrationStart(string registrationSessionID, string formFactor, int applicationID, int scenarioID,
                                     int ipAddress, HttpRequestBase httpRequestBase = null);

        string RecordRegistrationStep(string registrationSessionID, int stepID,
            Dictionary<string, object> stepDetails, Dictionary<string, string> allRegFields, string emailAddress, string recaptureID);
        RegistrationRecaptureInfo GetRegistrationRecpatureInfo(string recaptureID);
        PromotionMappingResult MapMapURLToPromo(string referrer);
        void CreateBedrockSession(int memberId, string sessionId);
        int GetMemberForSession(string sessionId);
        List<Country> GetCountries();
        List<Region> GetRegionsForZip(string zipCode);
        List<State> GetStatesForCountry(int countryRegionId);
        List<City> GetCitiesForCountryState(int countryRegionId, int stateRegionId);
        List<Scenario> GetScenarios();
        string GetRandomScenarioName(DeviceType deviceType);
        int GetMembersOnlineCount();
        List<string> GetPixels(string memberId, string luggageId, string promotionId, string referralURL, string pageName);

        int CreateSessionTrackingRequest(string sessionId, int promotionId, string luggageId, int bannerId,
                                         string clientIP, string referrerURL, string landingPageId, string landingPageTestId);

        bool ValidateRegistrationAttribute(string attributeName, string attributeValue);

        Brand GetBrand();

        // Map attributes from BH to Mingle
        Dictionary<string, object> MapToMingleAttributes(MingleAttributeMappingRequest request);
    }
}
