﻿using Spark.Common.RestConsumer.V2.Models.Session;

namespace Spark.Registration.Interfaces.Adapters
{
    public interface IMOSSessionAdapter
    {
        void CreateSession(Session session, string sessionId);
    }
}