﻿using System.Collections.Generic;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;

namespace Spark.Registration.Interfaces.Scenarios
{
    public interface IScenarioLoader
    {
        string GetRandomScenarioName();
        List<Scenario> GetScenarios();
        Scenario GetNamedScenario(string name);
        Scenario GetCurrentScenario();
    }
}