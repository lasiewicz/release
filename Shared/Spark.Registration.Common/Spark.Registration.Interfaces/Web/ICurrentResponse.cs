﻿using System.Web;

namespace Spark.Registration.Interfaces.Web
{
    public interface ICurrentResponse
    {
        HttpCookieCollection Cookies { get; }
        void AppendCookie(HttpCookie cookie);
        void SetCookie(HttpCookie cookie);
    }
}