﻿using System.Collections;

namespace Spark.Registration.Interfaces.Web
{
    public interface ICurrentContext
    {
        IDictionary Items { get; }
        bool IsNull { get; }
    }
}