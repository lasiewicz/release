﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Web;
using log4net;
using Spark.Common.RestConsumer;
using Spark.Common.RestConsumer.Models.OAuth2;

#endregion

namespace Spark.Authentication.OAuth
{
    /// <summary>
    ///     This class is used to log members in and out using OAuth by saving tokens to cookies.  
    ///     This is how JDate Mobile and IM handle login.  
    ///     Bedrock does not depend on OAuth login, but needs to save OAuth tokens because IM depends on OAuth.
    /// </summary>
    public class LoginManager : ILoginManager
    {
        private const string CookieUserPreviouslyRegisteredKey = "MOS_IS_REG";
        private const string CookieEmailPasswordKey = "MOS_REM";

        private const string CookieMemberId = "MOS_MEMBER";
        private const string CookieAccessToken = "MOS_ACCESS";
        private const string CookieAccessExpires = "MOS_ACCESS_EXPIRES";
        private const string CookieRefreshToken = "MOS_REFRESH";
        private const string CookieIsSubscriber = "MOS_SUB";
        private const string CookieEmail = "MOS_EA";
        private const string CookieDomain = "MOS_DOMAIN";
        private static readonly ILog Log;

        // TODO: remove these when all clients pass in app IDs
        private static readonly ushort ApplicationId;
        private static readonly string ClientSecret;

        private static readonly int LoginMinutesNoRememberMe;
        private static readonly int LoginMinutesRememberMe;

        public static readonly LoginManager Instance = new LoginManager();

        private LoginManager()
        {
        }

        static LoginManager()
        {
            Log = LogManager.GetLogger(typeof (LoginManager));

            var str = ConfigurationManager.AppSettings["LoginMinutesNoRememberMe"];
            if (!Int32.TryParse(str, out LoginMinutesNoRememberMe))
            {
                throw new Exception("Missing or invalid config entry for LoginMinutesNoRememberMe");
            }

            str = ConfigurationManager.AppSettings["LoginMinutesRememberMe"];
            if (!Int32.TryParse(str, out LoginMinutesRememberMe))
            {
                throw new Exception("Missing or invalid config entry for LoginMinutesRememberMe");
            }

            // TODO: remove these when all clients pass in app IDs
            if (ConfigurationManager.AppSettings["ApplicationId"] != null)
            {
                UInt16.TryParse(ConfigurationManager.AppSettings["ApplicationId"], out ApplicationId);
            }
            if (ConfigurationManager.AppSettings["ClientSecret"] != null)
            {
                ClientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            }
        }

        /// <summary>
        ///     Makes a REST API call to fetch access tokens
        /// </summary>
        /// <param name = "email"></param>
        /// <param name = "password"></param>
        /// <param name="passwordIsEncrypted"></param>
        /// <param name="brandId"></param>
        /// <param name="applicationId"> </param>
        /// <param name="clientSecret"> </param>
        /// <returns></returns>
        public OAuthTokens GetTokensViaPassword(string email, string password, bool passwordIsEncrypted, int brandId, int applicationId, string clientSecret)
        {
            var urlParams = new Dictionary<string, string>
                                {{"applicationId", applicationId.ToString(CultureInfo.InvariantCulture)}};
            var queryParams = new Dictionary<string, string> {{"client_secret", clientSecret}};
            OAuthTokens tokenResult = null;
            try
            {
                tokenResult = RestConsumer.GetInstance(brandId).Post<OAuthTokens, TokenRequestPassword>(urlParams,
                                                                                            new TokenRequestPassword
                                                                                                {
                                                                                                    Email = email,
                                                                                                    Password = password,
                                                                                                    PasswordIsEncrypted = passwordIsEncrypted
                                                                                                },
                                                                                            queryParams);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Exception getting OAuth tokens from REST using email/password.  Email: {0}", email);
                Log.Error(ex);
            }

            if (tokenResult == null)
            {
                Log.ErrorFormat("got null tokenresult on request for access token using email and password credentials.  Eamil: {0}", email);
                return null;
            }

            if (tokenResult.Success)
            {
                Log.DebugFormat("Successful token request for member ID {0}", tokenResult.MemberId);
                var expirationTime = DateTime.Now.AddSeconds(tokenResult.ExpiresIn);
                Log.DebugFormat("New tokens - Access: {0} Expires in {1} seconds at {2}", tokenResult.AccessToken,
                                tokenResult.ExpiresIn, expirationTime);
                return tokenResult;
            }

            Log.ErrorFormat("Failed to obtain new access token using email and password credentials. Email: {0} Error: {1}", email, tokenResult.Error);
            return tokenResult; // return the error
        }

        public OAuthTokens GetTokensViaRefresh(int memberId, string refreshToken, int brandId, int applicationId, string clientSecret)
        {
            var urlParams = new Dictionary<string, string>
                                {{"applicationId", applicationId.ToString(CultureInfo.InvariantCulture)}};
            var tokenRequest = new TokenRefreshRequest {RefreshToken = refreshToken, MemberId = memberId};
            var queryParams = new Dictionary<string, string> {{"client_secret", clientSecret}};
            Log.DebugFormat("Making token request using refresh token for member id {0}", memberId);
            string errorMessage;
            OAuthTokens tokenResult = null;
            try
            {
                tokenResult = RestConsumer.GetInstance(brandId).Post<OAuthTokens, TokenRefreshRequest>(urlParams, tokenRequest,
                                                                                           queryParams);
                if (tokenResult != null && tokenResult.Success)
                {
                    Log.DebugFormat("Successful token refresh request for member ID {0}", tokenResult.MemberId);
                    var expirationTime = DateTime.Now.AddSeconds(tokenResult.ExpiresIn);
                    Log.DebugFormat("New tokens - Access: {0} Expires in {1} seconds at {2}", tokenResult.AccessToken,
                                    tokenResult.ExpiresIn, expirationTime);
                    return tokenResult;
                }
                errorMessage = tokenResult == null ? "null token result" : tokenResult.Error;
            }
            catch (ThreadAbortException ex)
            {
                errorMessage = "Thread abort exception during refresh token request";
                Log.Error(errorMessage, ex);
                if (ex.InnerException != null)
                {
                    Log.Error("Inner exception: ", ex.InnerException);
                }
            }
            catch (Exception ex)
            {
                errorMessage = "Exception during refresh token request";
                Log.Error(errorMessage, ex);
            }

            var message =
                String.Format(
                    "Failed to obtain new access token using refresh token for app id {0}, user id {1} error {2}",
                    applicationId, tokenRequest.MemberId, errorMessage);
            Log.Error(message);
            return tokenResult;
        }

        public OAuthTokens GetTokensFromCookies()
        {
            var tokens = new OAuthTokens();
            var cookie = GetCookieFromRequestOrResponse(CookieMemberId);
            if (cookie != null)
            {
                int memberId;
                if (Int32.TryParse(cookie.Value, out memberId))
                {
                    tokens.MemberId = memberId;
                }
            }

            cookie = GetCookieFromRequestOrResponse(CookieAccessToken);
            if (cookie != null)
            {
                tokens.AccessToken = cookie.Value;
            }

            cookie = GetCookieFromRequestOrResponse(CookieRefreshToken);
            if (cookie != null)
            {
                tokens.RefreshToken = cookie.Value;
            }

            cookie = GetCookieFromRequestOrResponse(CookieIsSubscriber);
            if (cookie != null)
            {
                tokens.IsPayingMember = cookie.Value == "Y";
            }

            cookie = GetCookieFromRequestOrResponse(CookieAccessExpires);
            if (cookie != null)
            {
                var expireTimeStr = cookie.Value;
                DateTime accessExpires;
                if (DateTime.TryParse(expireTimeStr, out accessExpires))
                {
                    tokens.ExpiresIn = (int) (accessExpires - DateTime.Now).TotalSeconds;
                }
            }

            return tokens;
        }

        public void SetSubscriberCookie(bool isSubscriber, string oauthCookieDomain)
        {
            var cookie = new HttpCookie(CookieIsSubscriber)
                             {
                                 Expires = DateTime.Now.AddMinutes(LoginMinutesRememberMe),
                                 Value = isSubscriber ? "Y" : "N",
                                 Domain = oauthCookieDomain
                             };
            AddOrUpdateCookie(cookie);
        }


        public void SaveTokensToCookies(HttpResponse response, OAuthTokens tokens, bool useRememeberMeMinutes,
                                               string email, string environment, string brandDomain)
        {
            var oauthCookieDomain = !String.IsNullOrEmpty(environment) ? "." + environment : string.Empty;
            oauthCookieDomain += "." + brandDomain;
            var refreshTokenMins = useRememeberMeMinutes ? LoginMinutesRememberMe : LoginMinutesNoRememberMe;

            var cookie = new HttpCookie(CookieMemberId)
                             {
                                 Expires = DateTime.Now.AddMinutes(LoginMinutesRememberMe),
                                 Value = tokens.MemberId.ToString(CultureInfo.InvariantCulture),
                                 Domain = oauthCookieDomain
                             };
            // using LoginMinutesRememberMe time (currently 60 days)
            AddOrUpdateCookie(cookie);

            if (refreshTokenMins != 0)
                // if 0, refresh token cookie doesn't need to be saved, value was just read from cookie
            {
                // refresh token expiration time controls when user will need to login with email/password
                cookie = new HttpCookie(CookieRefreshToken)
                             {
                                 Expires = DateTime.Now.AddMinutes(refreshTokenMins),
                                 Value = tokens.RefreshToken,
                                 Domain = oauthCookieDomain
                             };
                AddOrUpdateCookie(cookie);
            }

            SetSubscriberCookie(tokens.IsPayingMember, oauthCookieDomain);

            var accessExpires = DateTime.Now.AddSeconds(tokens.ExpiresIn);
            cookie = new HttpCookie(CookieAccessToken)
                         {Expires = accessExpires, Value = tokens.AccessToken, Domain = oauthCookieDomain};
            AddOrUpdateCookie(cookie);

            cookie = new HttpCookie(CookieAccessExpires)
                         {
                             Expires = accessExpires,
                             Value = DateTime.Now.AddSeconds(tokens.ExpiresIn).ToString(CultureInfo.InvariantCulture),
                             Domain = oauthCookieDomain
                         };
            AddOrUpdateCookie(cookie);

            // Remember email addy in a cookie, fill out for convenience
            if (email != null)
            {
                cookie = new HttpCookie(CookieEmail)
                             {Expires = DateTime.Now.AddDays(7), Value = email, Domain = oauthCookieDomain};
                AddOrUpdateCookie(cookie);
            }

            cookie = new HttpCookie(CookieUserPreviouslyRegisteredKey)
                         {Expires = DateTime.Now.AddDays(30), Value = "Y", Domain = oauthCookieDomain};
            AddOrUpdateCookie(cookie);

            cookie = new HttpCookie(CookieDomain)
                         {
                             Expires = DateTime.Now.AddMinutes(refreshTokenMins),
                             Value = environment,
                             Domain = oauthCookieDomain
                         };
            // setting a cookie with the domain of all the OAuth cookies.  With this set, the client can detect which environment the cookies were set for, stage/prod, 
            // and can be used to keep users logged into multiple environments without having to clear cookies

            AddOrUpdateCookie(cookie);
        }

        /// <summary>
        ///     This method effectively logs in users via OAuth.  Bedrock calls this method so that IM users will be logged in.
        ///     This method makes a REST call to collect tokens, then saves to cookies.
        /// </summary>
        /// <param name = "email"></param>
        /// <param name = "password"></param>
        /// <param name="passwordIsEncrypted"></param>
        /// <param name = "useRememberMeExpirationTime">if true, use longer cookie lifespan, else use shorter.  Both times are config driven</param>
        /// <param name="environment">www, dev, stgv3, etc. </param>
        /// <param name="brandDomain">jdate.com, blacksingles.com, etc.</param>
        /// <param name="brandId"></param>
        /// <param name="applicationId"> </param>
        /// <param name="clientSecret"> </param>
        /// <returns></returns>
        public OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted,
                                                              bool useRememberMeExpirationTime, string environment, string brandDomain, int brandId, int applicationId, string clientSecret)
        {
            try
            {
                const int retryLimit = 3;
                var retries = 0;
                OAuthTokens tokens;
                var gotTokens = false;
                do
                {
                    retries++;
                    tokens = GetTokensViaPassword(email, password, passwordIsEncrypted, brandId, applicationId, clientSecret);
                    if (tokens == null)
                    {
                        Log.Warn("Failed to get oauth tokens");
                        continue;
                    }
                    if (!tokens.Success)
                    {
                        Log.WarnFormat("Failed to get oauth tokens, error was {0}", tokens.Error);
                    }
                    else
                    {
                        gotTokens = true;
                        break;
                    }
                } while (retries < retryLimit);

                if (!gotTokens)
                {
                    return null;
                }

                SaveTokensToCookies(HttpContext.Current.Response, tokens, useRememberMeExpirationTime, email, environment, brandDomain);
                return tokens;
            }
            catch (Exception ex)
            {
                Log.Error("Failed to collect and cookie tokens", ex);
                return null;
            }
        }

        public void RemoveTokenCookies(string oauthCookieDomain)
        {
            // kill the autologin cookie
            RemoveCookie(CookieEmailPasswordKey, oauthCookieDomain.ToLower());
            RemoveCookie(CookieAccessToken, oauthCookieDomain.ToLower());
            RemoveCookie(CookieAccessExpires, oauthCookieDomain.ToLower());
            RemoveCookie(CookieRefreshToken, oauthCookieDomain.ToLower());
        }

        public bool UserPreviouslyLoggedIn()
        {
            var cookie = HttpContext.Current.Request.Cookies.Get(CookieUserPreviouslyRegisteredKey);
            return cookie != null && cookie.Value == "Y";
        }

        private static void AddOrUpdateCookie(HttpCookie newCookie)
        {
            var keys = new HashSet<string>(HttpContext.Current.Response.Cookies.AllKeys);
            if (!keys.Contains(newCookie.Name))
            {
                HttpContext.Current.Response.AppendCookie(newCookie);
            }
            else
            {
                // reuse the cookie already in httpcookiecollection to avoid duplication
                HttpContext.Current.Response.SetCookie(newCookie);
            }
        }

        private static HttpCookie GetCookieFromRequestOrResponse(string cookieName)
        {
            var outGoing = HttpContext.Current.Response.Cookies[cookieName];
            // this call creates a new cookie if it doesn't exist
            if (outGoing != null && !String.IsNullOrEmpty(outGoing.Value))
            {
                return outGoing;
            }
            if (outGoing != null)
            {
                HttpContext.Current.Response.Cookies.Remove(cookieName);
                // no value?  this cookie was just created by the indexer above, so remove it
            }
            var cookie = HttpContext.Current.Request.Cookies.Get(cookieName);
            return cookie;
        }

        private static void RemoveCookie(string key, string oauthCookieDomain)
        {
            var current = HttpContext.Current.Request.Cookies[key];
            if (current == null) return;
            var myCookie = new HttpCookie(key)
            {
                Expires = DateTime.Now.AddYears(-1),
                Value = current.Value,
                Domain = oauthCookieDomain
            };
            HttpContext.Current.Response.Cookies.Set(myCookie);
        }

        [Obsolete("please use the overload that has the environment parameter instead")]
        public void SaveTokensToCookies(HttpResponse response, OAuthTokens tokens, bool useRememeberMeMinutes, string email, string brandDomain)
        {
            SaveTokensToCookies(response, tokens, useRememeberMeMinutes, email, String.Empty, brandDomain);
        }

        [Obsolete("please use the overload that has the environment parameter instead")]
        public OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted, bool useRememberMeExpirationTime, string brandDomain, int brandId)
        {
            return FetchTokensAndSaveToCookies(email, password, passwordIsEncrypted, useRememberMeExpirationTime, String.Empty, brandDomain, brandId, ApplicationId, ClientSecret);
        }

        [Obsolete("please use the overload that accepts app id and client secret instead")]
        public OAuthTokens GetTokensViaPassword(string email, string password, bool passwordIsEncrypted, int brandId)
        {
            return GetTokensViaPassword(email, password, passwordIsEncrypted, brandId, ApplicationId, ClientSecret);
        }

        [Obsolete("please use the overload that accepts app id and client secret instead")]
        public OAuthTokens GetTokensViaRefresh(int memberId, string refreshToken, int brandId)
        {
            return GetTokensViaRefresh(memberId, refreshToken, brandId, ApplicationId, ClientSecret);
        }

        [Obsolete("please use the overload with app id and client secret instead")]
        public OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted, bool useRememberMeExpirationTime, string environment, string brandDomain, int brandId)
        {
            return FetchTokensAndSaveToCookies(email, password, passwordIsEncrypted, useRememberMeExpirationTime,
                                               environment, brandDomain, brandId, ApplicationId, ClientSecret);
        }
    }
}
