﻿#region

using System.Collections.Generic;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.MingleAPIAdapter.ExternalModels;
using Spark.MingleAPIAdapter.ResponseModels;

#endregion

namespace Spark.MingleAPIAdapter
{
    public class Adapter
    {
        public bool ApprovePhoto(Brand brand, ExternalMigrationPhotoApprovalRequest request)
        {
            const string urlPath = "/photos/approve";

            var apiClient = new APIClient(brand, false);

            List<MigrationPhotoApprovalResponse> responseData;

            var response = apiClient.MakeRequest(urlPath, null, request, true, out responseData);

            return response.ValidResponse && response.StatusCode == System.Net.HttpStatusCode.OK && responseData != null
                   && responseData[0].success;
        }

        public bool MingleSynchPassword(int memberId, Brand brand, string emailAddress, string password, string salt)
        {
            var urlPath = "/oauth2/insertUpdatePassword";

            var apiClient = new APIClient(brand, true);
            var synchRequest = new MinglePasswordSynchRequest
            {
                MemberId = memberId,
                EmailAddress = emailAddress,
                P1 = password,
                P2 = salt
            };

            MinglePasswordSynchResponse returnObj = null;

            var response = apiClient.MakeRequest(urlPath, null, synchRequest, true, out returnObj);

            if (response.ValidResponse && response.StatusCode == System.Net.HttpStatusCode.OK && returnObj != null)
            {
                return true;
            }

            return false;
        }
    }
}