﻿#region

using System;
using Newtonsoft.Json;

#endregion

namespace Spark.MingleAPIAdapter.ExternalModels
{
    [Serializable]
    public class ExternalMigrationPhotoApprovalRequest
    {
        [JsonProperty("memberID")]
        public int MemberId { get; set; }

        [JsonProperty("memberPhotoID")]
        public int MemberPhotoId { get; set; }

        [JsonIgnore]
        public int BrandId { get; set; }

        [JsonProperty("cloudFullPath")]
        public string CloudFullPath { get; set; }

        [JsonProperty("cloudPreviewPath")]
        public string CloudPreviewPath { get; set; }

        [JsonProperty("cloudThumbPath")]
        public string CloudThumbPath { get; set; }

        [JsonIgnore]
        public string LaFullPath { get; set; }

        [JsonIgnore]
        public string LaPreviewPath { get; set; }

        [JsonIgnore]
        public string LaThumbPath { get; set; }

        [JsonProperty("originalPath")]
        public string OriginalPath { get; set; }

        [JsonProperty("nineSixtyPath")]
        public string NineSixtyPath { get; set; }

        [JsonProperty("fullWidth")]
        public int FullWidth { get; set; }

        [JsonProperty("fullHeight")]
        public int FullHeight { get; set; }

        [JsonProperty("previewWidth")]
        public int PreviewWidth { get; set; }

        [JsonProperty("previewHeight")]
        public int PreviewHeight { get; set; }

        [JsonProperty("thumbWidth")]
        public int ThumbWidth { get; set; }

        [JsonProperty("thumbHeight")]
        public int ThumbHeight { get; set; }

        /// <summary>
        ///     Mingle requires the user name instead of the Id
        /// </summary>
        [JsonProperty("adminName")]
        public string AdminName { get; set; }

        /// <summary>
        ///     Using the custom convert below since Mingle expects 1 or 0
        /// </summary>
        [JsonProperty("approvedForMain")]
        [JsonConverter(typeof (BoolConverter))]
        public bool ApprovedForMain { get; set; }

        public override string ToString()
        {
            return
                string.Format(
                    "MemberId: {0} MemberPhotoID: {1} BrandID: {2} CloudFullPath: {3} CloudPreviewPath: {4} CloudThumbPath: {5} LaFullPath: {6} LaPreviewPath: {7} LaThumbPath: {8} OriginalPath: {9} NineSixtyPath: {10}",
                    MemberId, MemberPhotoId, BrandId, CloudFullPath, CloudPreviewPath, CloudThumbPath, LaFullPath,
                    LaPreviewPath, LaThumbPath, OriginalPath, NineSixtyPath);
        }
    }

    public class BoolConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((bool)value) ? 1 : 0);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return reader.Value.ToString() == "1";
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(bool);
        }
    }
}