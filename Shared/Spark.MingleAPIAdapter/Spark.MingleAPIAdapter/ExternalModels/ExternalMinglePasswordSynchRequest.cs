﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.MingleAPIAdapter.ExternalModels
{
    [Serializable]
    public class ExternalMinglePasswordSynchRequest
    {
        public int BrandId { get; set; }
        public int MemberId { get; set; }
        public string EmailAddress { get; set; }
        public string P1 { get; set; }
        public string P2 { get; set; }
    }
}
