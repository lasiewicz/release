﻿namespace Spark.MingleAPIAdapter.ResponseModels
{
    /// <summary>
    ///     Response from this endpoint
    ///     https://api-dev.securemingle.com/v1/9071/photos/approve?client_secret=qKzSMMS5fvH1PzfKHqf3uDaTVycTIHE2H5mRBfvRcv3
    ///     &clientid=55536
    /// </summary>
    internal class MigrationPhotoApprovalResponse
    {
        public int memberID { get; set; }
        public int memberPhotoID { get; set; }
        public bool success { get; set; }
        public string msg { get; set; }
    }
}