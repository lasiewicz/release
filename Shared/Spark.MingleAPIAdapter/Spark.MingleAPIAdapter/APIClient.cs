﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Newtonsoft.Json;
using RestSharp;

namespace Spark.MingleAPIAdapter
{
    internal class APIClient
    {
        private string _APIUrl;
        private string _APIClientSecret;
        private string _APIClientID;
        private bool _APIIgnoreSSLErrors;
        private Brand _brand;
        
        public APIClient(Brand brand, bool usePWSynchSettings)
        {
            if (usePWSynchSettings)
            {
                _APIUrl = RuntimeSettings.GetSetting(SettingConstants.MINGLE_PW_SYNCH_API_URL, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                _APIClientSecret = RuntimeSettings.GetSetting(SettingConstants.MINGLE_PW_SYNCH_CLIENT_SECRET, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                _APIClientID = RuntimeSettings.GetSetting(SettingConstants.MINGLE_PW_SYNCH_CLIENT_ID, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            else
            {
                _APIUrl = RuntimeSettings.GetSetting(SettingConstants.MINGLE_REST_API_URL, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                _APIClientSecret = RuntimeSettings.GetSetting(SettingConstants.MINGLE_REST_API_CLIENT_SECRET, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                _APIClientID = RuntimeSettings.GetSetting(SettingConstants.MINGLE_REST_API_CLIENT_ID, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            _APIIgnoreSSLErrors = Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.MINGLE_REST_API_IGNORE_SSL_ERRORS, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            _brand = brand;
        }

        public RestStatus MakeRequest<T>(string url, Dictionary<string, string> urlSegments, object jsonParameter, bool usePost, out T returnObject) where T : new()
        {
            returnObject = default(T);
            
            if(url.IndexOf("/") != 0)
            {
                url = "/" + url;
            }
            
            var client = new RestClient(_APIUrl);
            var request = new RestRequest("/" + _brand.Site.SiteID.ToString() + url + "?client_secret={clientSecret}&clientid={clientId}", usePost ? Method.POST : Method.GET);

            if (_APIIgnoreSSLErrors)
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            }

            if (usePost && jsonParameter != null)
            {
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddParameter("application/json",
                    JsonConvert.SerializeObject(jsonParameter), ParameterType.RequestBody);
            }

            request.AddUrlSegment("clientSecret", _APIClientSecret);
            request.AddUrlSegment("clientId", _APIClientID);

            if (urlSegments != null && urlSegments.Count > 0)
            {
                foreach (var segment in urlSegments)
                {
                    request.AddUrlSegment(segment.Key, segment.Value);
                }
            }

            string errorMessage = string.Empty;
            try
            {
                var response = client.Execute<ResponseWrapper<T>>(request) as RestResponse<ResponseWrapper<T>>;

                if (response != null && response.Data.Data != null)
                {
                    returnObject = response.Data.Data;
                    return new RestStatus { ValidResponse = true, StatusCode = response.StatusCode };
                }
                if (response != null && !string.IsNullOrEmpty(response.ErrorMessage))
                {
                    errorMessage = response.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return new RestStatus { ValidResponse = false, ErrorMessage = errorMessage};
        }
    }
}
