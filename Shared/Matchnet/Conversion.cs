using System;

namespace Matchnet
{
	/// <summary>
	/// Summary description for Conversion.
	/// </summary>
	public class Conversion
	{
		/// <summary>
		/// 
		/// </summary>
		private Conversion()
		{
			
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="birthDate"></param>
		/// <returns></returns>
		public static Int32 Age(DateTime birthDate)
		{
			DateTime now = System.DateTime.Now;
			Int32 age = 0;
			
			age = (Int32)now.Year - (Int32)birthDate.Year;
			if (now.Month == birthDate.Month)
			{
				if (now.Day < birthDate.Day)
					age--;
			}
			else if (now.Month < birthDate.Month) 
			{
				age--;
			}

			if (age > 0)
				return age;
			else
				return 0;

		}


		#region CDecimal and Overloads

		/// <summary>
		/// Converts a string to a decimal.
		/// </summary>
		/// <param name="strValue"></param>
		/// <param name="dDefaultValue"></param>
		/// <returns></returns>
		public static decimal CDecimal(string strValue, decimal dDefaultValue)
		{
			decimal dValue;
			try
			{
				dValue = Convert.ToDecimal(strValue);
			}
			catch
			{
				dValue = dDefaultValue;
			}

			return dValue;
		}

		/// <summary>
		/// Converts a string to a decimal, uses Constants.NULL_DECIMAL as default value.
		/// </summary>
		/// <param name="strValue"></param>
		/// <returns></returns>
		public static decimal CDecimal(string strValue)
		{
			return CDecimal(strValue, Matchnet.Constants.NULL_DECIMAL);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="objValue"></param>
		/// <param name="dDefaultValue"></param>
		/// <returns></returns>
		public static decimal CDecimal(object objValue, decimal dDefaultValue)
		{
			if (objValue == null)
			{
				return Matchnet.Constants.NULL_DECIMAL;
			}

			return CDecimal(objValue.ToString(), dDefaultValue);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="objValue"></param>
		/// <returns></returns>
		public static decimal CDecimal(object objValue)
		{
			return CDecimal(objValue, Matchnet.Constants.NULL_DECIMAL);
		}

		#endregion

		#region CDouble and Overloads

		/// <summary>
		/// Converts a string to a double.
		/// </summary>
		/// <param name="strValue"></param>
		/// <param name="dDefaultValue"></param>
		/// <returns></returns>
		public static double CDouble(string strValue, double dDefaultValue)
		{
			double dResult;

			if (Double.TryParse(strValue, System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.InvariantInfo, out dResult))
			{
				if (dResult >= Double.MinValue && dResult <= Double.MaxValue)
				{
					return dResult;
				}
			}
			return dDefaultValue;
		}

		/// <summary>
		/// Converts a string to a double, uses Constants.NULL_DOUBLE as default value.
		/// </summary>
		/// <param name="strValue"></param>
		/// <returns></returns>
		public static double CDouble(string strValue)
		{
			return CDouble(strValue, Matchnet.Constants.NULL_DOUBLE);
		}

		#endregion

		#region CInt and Overloads
		/// <summary>
		/// Converts a string to an integer.
		/// </summary>
		/// <param name="strValue">The string to convert to an integer.</param>
		/// <param name="iDefaultValue">The default integer to return if conversion fails.</param>
		/// <returns>The integer representation of the object.</returns>
		/// 
		public static int CInt(string strValue, int iDefaultValue)
		{
			double dResult;

			// shoudl use Int.TryParse, but that is not supported until v2005
			if(Double.TryParse(strValue, System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.InvariantInfo, out dResult))
			{
				if (dResult >= Int32.MinValue && dResult <= Int32.MaxValue)
					return Convert.ToInt32(dResult);
			}
			return iDefaultValue;
		}

		/// <summary>
		/// Overloaded CInt, uses NULL for the default value.
		/// </summary>
		/// <param name="strValue">The string to convert to an integer</param>
		/// <returns>The string represented as an integer.  If not an integer, then NULL_INT.</returns>
		public static int CInt(string strValue)
		{
			return CInt(strValue, Matchnet.Constants.NULL_INT);
		}

		/// <summary>
		/// Overloaded CInt, uses NULL for the default value and an object instead of a string.
		/// </summary>
		/// <param name="objValue"></param>
		/// <returns></returns>
		public static int CInt(object objValue)
		{
			if (objValue == null)
			{
				return Matchnet.Constants.NULL_INT;
			}

			return CInt(objValue, Matchnet.Constants.NULL_INT);
		}

		/// <summary>
		/// Overloaded CInt, uses an object instead of a string.
		/// </summary>
		/// <param name="objValue">The object to convert to an integer.</param>
		/// <param name="iDefaultValue">The default value to return if not an integer.</param>
		/// <returns>The integer representation of the object, or iDefaultValue if not an integer.</returns>
		public static int CInt(object objValue, int iDefaultValue)
		{
			if (objValue == null)
			{
				return iDefaultValue;
			}

			return CInt(objValue.ToString(), iDefaultValue);
		}
		#endregion

		#region CDateTime and Overloads
		/// <summary>
		/// 
		/// </summary>
		/// <param name="objValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static DateTime CDateTime(object objValue, DateTime defaultValue)
		{
			if (objValue != null && objValue != System.DBNull.Value)
			{
				try
				{
					return Convert.ToDateTime(objValue);
				}
				catch
				{
					return defaultValue;
				}
			}
			else
			{
				return defaultValue;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="objValue"></param>
		/// <returns></returns>
		public static DateTime CDateTime(object objValue)
		{
			return CDateTime(objValue, DateTime.MinValue);
		}
		#endregion

		#region CBool and Overloads
		/// <summary>
		/// 
		/// </summary>
		/// <param name="objValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static bool CBool(object objValue, bool defaultValue)
		{
			if (objValue != null && objValue != System.DBNull.Value)
			{
				try
				{
					return Convert.ToBoolean(objValue);
				}
				catch
				{
					return defaultValue;
				}
			}
			else
			{
				return defaultValue;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="objValue"></param>
		/// <returns></returns>
		public static bool CBool(object objValue)
		{
			return CBool(objValue, false);
		}
		#endregion
	}
}
