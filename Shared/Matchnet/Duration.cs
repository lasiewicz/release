using System;

namespace Matchnet
{
	public class Duration
	{
		private Duration()
		{
		}


		public static Int32 GetMinutes(DurationType durationType,
			Int32 duration)
		{
			switch(durationType)
			{
				case DurationType.Minute:
					return duration;
				case DurationType.Hour:
					return duration* 60;
				case DurationType.Day:
					return duration * 24 * 60;
				case DurationType.Week:
					return duration * 7 * 24 * 60;
				case DurationType.Month:
					return (Int32)((TimeSpan)DateTime.Now.AddMonths(duration).Subtract(DateTime.Now)).TotalMinutes;
				case DurationType.Year:
					return (Int32)((TimeSpan)DateTime.Now.AddYears(duration).Subtract(DateTime.Now)).TotalMinutes;
				default:
					return 0;
			}
		}
	}
}
