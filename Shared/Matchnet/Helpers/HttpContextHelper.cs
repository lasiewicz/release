﻿using System.Web;

namespace Matchnet.Helpers
{
    public interface IHttpContextHelper
    {
        HttpContextBase CurrentHttpContextBase { get; }
    }

    public class HttpContextHelper : IHttpContextHelper
    {
        public HttpContextBase CurrentHttpContextBase
        {
            get { return new HttpContextWrapper(HttpContext.Current); }
        }
    }
}