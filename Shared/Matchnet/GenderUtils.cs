using System;

namespace Matchnet
{
	/// <summary>
	/// Summary description for GenderUtils.
	/// </summary>
	public class GenderUtils
	{

		private readonly static int [] _ReversibleMasks = {5,  6,  9,  10,  20,  24,  36,  40,  65,  66,  80,  96,  129,  130,  144,  160 };

		private const GenderMask MASK_SLEF =
									GenderMask.Male | 
									GenderMask.Female | 
									GenderMask.MTF | 
									GenderMask.FTM;

		private const GenderMask MASK_SEEKING =
									GenderMask.SeekingMale | 
									GenderMask.SeekingFemale | 
									GenderMask.SeekingMTF | 
									GenderMask.SeekingFTM;


		/// <summary>
		/// 
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static int GetGenderMaskSelf(int genderMask) { return genderMask & (int)MASK_SLEF; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static int GetGenderMaskSeeking(int genderMask) { return genderMask & (int)MASK_SEEKING; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static int GetGenderSeekingFromGender(int genderMask) { return (genderMask & (int)MASK_SLEF) << 2; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static int GetGenderFromGenderSeeking(int genderMask) { return (genderMask & (int)MASK_SEEKING) >> 2; }
		

		/// <summary>
		/// Determines whether or not a given gender and seeking gender indicate a homosexual orientation.
		/// Currently, if any "seeking" bit matches any "self" bit, then gender is considered homosexual.
		/// </summary>
		/// <param name="gender"></param>
		/// <param name="seekingGender"></param>
		/// <returns></returns>
		public static bool IsHomosexual(int gender, int seekingGender) { return IsMaskHomosexual(gender + seekingGender); }


		/// <summary>
		///	Determines whether or not a given gender mask indicate a homosexual orientation.
		///	Currently, if any "seeking" bit matches any "self" bit, then gender is considered homosexual.
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static bool IsMaskHomosexual(int genderMask) { return ((genderMask & (int)MASK_SLEF) & ((genderMask & (int)MASK_SEEKING) >> 2)) > 0; }


		/// <summary>
		/// Determines whether or not a given gender and seeking gender indicate
		/// a heterosexual orientation
		/// </summary>
		/// <param name="gender"></param>
		/// <param name="seekingGender"></param>
		/// <returns></returns>
		public static bool IsHeterosexual(int gender, int seekingGender) { return(!IsHomosexual(gender, seekingGender)); }


		/// <summary>
		///	Determines whether or not a given gender mask indicate a
		///	heterosexual orientation
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static bool IsMaskHeterosexual(int genderMask) { return(!IsMaskHomosexual(genderMask)); }

		/// <summary>
		/// Apparently the gender mask is stored in such a way that if
		/// we want it to dispaly properly we have to flip it when we
		/// pull it from the preferences and before it gets put back
		/// into the preferences if and only if the user is a
		/// heterosexual.  This method encapsulates that functionality.
		/// 
		/// 
		/// Revision 10/21/04 - TB
		/// Flipping the mask has no effect if the user is homosexual,
		/// so we'll just flip it regardless.
		/// 
		/// Revision 7/7/05 - NH
		/// The name FlipMaskIfHeterosexual is missleading. It will flip it regardless.
		/// After discussion w/Tom, introducing an if(hetero) {} has unknown effects so leave it as is.
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static int FlipMaskIfHeterosexual(int genderMask) { return ((GetGenderMaskSelf(genderMask) << 2)  + (GetGenderMaskSeeking(genderMask) >> 2)); }

		/// <summary>
		/// Determines if the mask supplied contains ALL BITS of the genderID supplied.
		/// </summary>
		/// <param name="genderMask"></param>
		/// <param name="genderID"></param>
		/// <returns>True = Mask contains ALL bits of GenderID supplied</returns>
		public static bool MaskContains(int genderMask, int genderID) { return ((genderMask & genderID) == genderID); }

		/// <summary>
		/// Determine if the bitmask is flippable, that is that the operation of 
		/// getting "who I am looking for" is sane given this.
		/// Currenly only reversible if a single "self" and a single "seeking" bits are found.
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static bool IsFlippable(int genderMask) { return (Array.BinarySearch(_ReversibleMasks,genderMask) >= 0);	}

	}
}
