using System;

namespace Matchnet
{
	/// <summary>
	/// Contract for standard cacheable class
	/// </summary>
	public interface ICacheable
	{
		/// <summary>
		/// Gets and sets the number of seconds that the item will be cached (used in conjunction with CacheMode)
		/// </summary>
		int CacheTTLSeconds
		{
			get;
			set;
		}

		/// <summary>
		/// Gets and Matching.CacheItemMode
		/// </summary>
		CacheItemMode CacheMode
		{
			get;
		}

		/// <summary>
		///  Gets and sets the caching Priority level for this value object
		/// </summary>
		CacheItemPriorityLevel CachePriority
		{
			get;
			set;
		}

		/// <summary>
		/// Returns the cache key for the class
		/// </summary>
		/// <returns>String reprenting the cache key for this class</returns>
		string GetCacheKey();
	}
}
