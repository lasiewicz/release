using System;

namespace Matchnet
{
	public interface IByteSerializable
	{
		byte[] ToByteArray();
		void FromByteArray(byte[] bytes);
	}
}
