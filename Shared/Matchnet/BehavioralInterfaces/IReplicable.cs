using System;

namespace Matchnet
{
	/// <summary>
	/// 
	/// </summary>
	public interface IReplicable : ICacheable, IValueObject
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		ReplicationPlaceholder GetReplicationPlaceholder();
	}
}
