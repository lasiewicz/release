﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;

namespace Matchnet
{
    /// <summary>
    /// Interface for caching providers
    /// </summary>
    public interface ICaching
    {
        /// <summary>
        /// Get an item from cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        object this[string key] { get; set; }

        /// <summary>
        /// Adds cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <returns></returns>
        ICacheable Add(ICacheable cacheableObject);

        /// <summary>
        /// Adds cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="overrideItemPriority"></param>
        /// <returns></returns>
        ICacheable Add(ICacheable cacheableObject, CacheItemPriorityLevel overrideItemPriority);

        /// <summary>
        /// Adds cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="cacheDependency"></param>
        /// <returns></returns>
        ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency);

        /// <summary>
        /// Adds cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="onRemoveCallback"></param>
        /// <returns></returns>
        ICacheable Add(ICacheable cacheableObject, CacheItemRemovedCallback onRemoveCallback);

        /// <summary>
        /// Adds cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="cacheDependency"></param>
        /// <param name="onRemoveCallback"></param>
        /// <returns></returns>
        ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency,
                       CacheItemRemovedCallback onRemoveCallback);

        /// <summary>
        /// Adds cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="cacheDependency"></param>
        /// <param name="overrideItemPriority"></param>
        /// <param name="onRemoveCallback"></param>
        /// <returns></returns>
        ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency,
                       CacheItemPriorityLevel overrideItemPriority, CacheItemRemovedCallback onRemoveCallback);

        /// <summary>
        /// Adds an item into cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="dependencies"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="slidingExpiration"></param>
        /// <param name="priority"></param>
        /// <param name="onRemoveCallback"></param>
        /// <returns></returns>
        object Add(string key,
                   object value,
                   System.Web.Caching.CacheDependency dependencies,
                   DateTime absoluteExpiration,
                   TimeSpan slidingExpiration,
                   System.Web.Caching.CacheItemPriority priority,
                   System.Web.Caching.CacheItemRemovedCallback onRemoveCallback);
        
        /// <summary>
        /// Retrieves an item from cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        object Get(string key);

        /// <summary>
        /// Retrieves multiple items from cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        IDictionary<string, object> Get(List<string> key);

        /// <summary>
        /// Gets an enumeration of items in the cache
        /// </summary>
        /// <returns></returns>
        IEnumerator GetEnumerator();

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        void Insert(ICacheable cacheableObject);

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="overrideItemPriority"></param>
        void Insert(ICacheable cacheableObject, CacheItemPriorityLevel overrideItemPriority);

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="cacheDependency"></param>
        void Insert(ICacheable cacheableObject, CacheDependency cacheDependency);

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="onRemoveCallback"></param>
        void Insert(ICacheable cacheableObject, CacheItemRemovedCallback onRemoveCallback);

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="cacheDependency"></param>
        /// <param name="onRemoveCallback"></param>
        void Insert(ICacheable cacheableObject, CacheDependency cacheDependency,
                    CacheItemRemovedCallback onRemoveCallback);

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="cacheableObject"></param>
        /// <param name="cacheDependency"></param>
        /// <param name="overrideItemPriority"></param>
        /// <param name="onRemoveCallback"></param>
        void Insert(ICacheable cacheableObject, CacheDependency cacheDependency,
                    CacheItemPriorityLevel overrideItemPriority, CacheItemRemovedCallback onRemoveCallback);

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        void Insert(string key, object value);

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="dependencies"></param>
        void Insert(string key,
                    object value,
                    System.Web.Caching.CacheDependency dependencies);

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="dependencies"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="slidingExpiration"></param>
        void Insert(string key,
                    object value,
                    System.Web.Caching.CacheDependency dependencies,
                    DateTime absoluteExpiration,
                    TimeSpan slidingExpiration);

        /// <summary>
        /// Inserts a cacheable object into cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="dependencies"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="slidingExpiration"></param>
        /// <param name="priority"></param>
        /// <param name="onRemoveCallback"></param>
        void Insert(string key,
                    object value,
                    System.Web.Caching.CacheDependency dependencies,
                    DateTime absoluteExpiration,
                    TimeSpan slidingExpiration,
                    System.Web.Caching.CacheItemPriority priority,
                    System.Web.Caching.CacheItemRemovedCallback onRemoveCallback);

        /// <summary>
        /// Removes an item from cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        object Remove(string key);

        /// <summary>
        /// Removes all items from cache
        /// </summary>
        void Clear();

        /// <summary>
        /// Removes an item from cache without returning the item.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool RemoveWithoutGet(string key);		
    }
}
