﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Caching;

namespace Matchnet.BaseClasses
{
    /// <summary>
    /// This base class's purpose is to remove boilerplate code of creating cacheable entities.
    /// The constractor sets default values. The properties are not immutable, so they can be changed after that.
    /// </summary>
    /// <param name="cacheItemMode"></param>
    /// <param name="cachePriority"></param>
    /// <param name="cacheTtlSeconds"></param>
    public abstract class BaseCacheable: ICacheable
    {
        protected BaseCacheable(CacheItemMode cacheItemMode, CacheItemPriorityLevel cachePriority,
            int cacheTtlSeconds)
        {
            CacheMode = cacheItemMode;
            CachePriority = cachePriority;
            CacheTTLSeconds = cacheTtlSeconds;
        }

        #region Implementation of ICacheable

        public abstract string GetCacheKey();

        public int CacheTTLSeconds { get; set; }

        public CacheItemMode CacheMode { get; set; }

        public CacheItemPriorityLevel CachePriority { get; set; }

        #endregion
    }
}
