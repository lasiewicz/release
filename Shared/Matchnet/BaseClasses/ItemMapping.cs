﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.BaseClasses
{
    public class ItemMapping<TKey, TValue>
    {
        public ItemMapping(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
        public TKey Key { get; private set; }
        public TValue Value { get; private set; }
    }
}
