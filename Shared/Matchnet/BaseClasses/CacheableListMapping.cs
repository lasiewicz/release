﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Caching;
using Matchnet.BaseClasses;

namespace Matchnet.BaseClasses
{
    public abstract class CacheableListMapping<TKey, TValueItem> : BaseCacheable
    {
        public CacheableListMapping(
            TKey key, 
            List<TValueItem> value,
            CacheItemPriorityLevel cachePriority = CacheItemPriorityLevel.Default, 
            int cacheTtlSeconds = 300) 
            : base(CacheItemMode.Absolute, cachePriority, cacheTtlSeconds)
        {
            Key = key;
            Value = value;
        }

        public TKey Key { get; private set; }
        public List<TValueItem> Value { get; private set; }
        protected abstract string GetCacheKeyFromId(TKey id);

        public override string GetCacheKey()
        {
            return GetCacheKeyFromId(Key);
        }


    }
}
