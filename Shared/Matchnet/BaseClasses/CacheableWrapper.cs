﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.BaseClasses
{
    public abstract class CacheableWrapper<TData> : BaseCacheable
    {
        protected CacheableWrapper(TData data, CacheItemMode cacheItemMode, CacheItemPriorityLevel cachePriority, int cacheTtlSeconds) : base(cacheItemMode, cachePriority, cacheTtlSeconds)
        {
            Data = data;
        }
        public TData Data { get; private set; }
    }
}
