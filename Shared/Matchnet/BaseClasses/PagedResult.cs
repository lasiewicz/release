﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.BaseClasses
{
    [Serializable]
    public class PagedResult<TData>
    {
        public PagedResult()
        {

        }

        public PagedResult(List<TData> items, int total)
        {
            Items = items;
            Total = total;
        }
        public List<TData> Items { get; set; }
        public int Total { get; set; }
    }
}
