using System;

namespace Matchnet
{
	/// <summary>
	/// The integer representation of a language
	/// </summary>
	public enum Language : int
	{
		/// <summary>
		/// 
		/// </summary>
		English = 2,

		/// <summary>
		/// 
		/// </summary>
		French = 8,

		/// <summary>
		/// 
		/// </summary>
		German = 32,

		/// <summary>
		/// 
		/// </summary>
		Hebrew = 262144
	}

}
