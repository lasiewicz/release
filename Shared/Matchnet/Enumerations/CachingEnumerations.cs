using System;

namespace Matchnet
{

	
	/// <summary>
	///  Specifies the relative priority of items stored in the Matching.Caching.Cache.
	/// </summary>
	[Serializable]
	public enum CacheItemPriorityLevel
	{
		/// <summary>
		/// Cache items with this priority level are the least likely to be deleted from the cache as the server frees system memory.  
		/// </summary>
		High = 1,
		/// <summary>
		/// Cache items with this priority level are likely to be deleted from the cache as the server frees system memory only after those items with Low or BelowNormal priority. This is the default.  
		/// </summary>
		Normal = 2,
		/// <summary>
		/// Cache items with this priority level are the most likely to be deleted from the cache as the server frees system memory.  
		/// </summary>
		Low = 3,
		/// <summary>
		/// The default value for a cached item's priority is Normal.
		/// </summary>
		Default = 4,
		/// <summary>
		/// Cache items with this priority level are less likely to be deleted as the server frees system memory than those assigned a Normal priority.
		/// </summary>
		AboveNormal = 5,
		/// <summary>
		/// Cache items with this priority level are more likely to be deleted from the cache as the server frees system memory than items assigned a Normal priority.
		/// </summary>
		BelowNormal = 6,
	}

	/// <summary>
	/// Specifies the removal mode for a given cache item
	/// </summary>
	[Serializable]
	public enum CacheItemMode
	{
		/// <summary>
		/// The cache item will be removed based on an absolute expiration.
		/// </summary>
		Absolute = 0,
		/// <summary>
		/// The cache item will be removed based on a sliding expiration.
		/// </summary>
		Sliding = 1
	}


}
