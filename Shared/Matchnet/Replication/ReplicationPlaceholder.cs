using System;

namespace Matchnet
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public sealed class ReplicationPlaceholder // NOT A VALUE OBJECT IN A FORMAL SENSE
	{
		private string _cacheKey = Constants.NULL_STRING;

		private ReplicationPlaceholder()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cacheKey"></param>
		public ReplicationPlaceholder(string cacheKey)
		{
			_cacheKey = cacheKey;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _cacheKey;
		}

	}
}
