using System;

namespace Matchnet.Replication
{
	public interface IReplicationAction
	{
		void Play();
	}
}
