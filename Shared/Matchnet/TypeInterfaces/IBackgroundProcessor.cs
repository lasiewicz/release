using System;

namespace Matchnet
{
	public interface IBackgroundProcessor
	{
		void Start();

		void Stop();
	}
}
