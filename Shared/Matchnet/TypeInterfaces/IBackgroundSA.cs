using System;

namespace Matchnet.TypeInterfaces
{
	public interface IBackgroundSA
	{
		void DoBackgroundWork();

		Int32 GetInterval();
	}
}
