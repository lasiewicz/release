using System;

namespace Matchnet
{
	/// <summary>
	/// 
	/// </summary>
	public interface IReplicationRecipient
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="replicableObject"></param>
		void Receive(IReplicable replicableObject);
	}
}
