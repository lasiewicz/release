using System;


namespace Matchnet
{
	public interface IReplicationActionRecipient
	{
		void Receive(IReplicationAction replicationAction);
	}
}
