using System;

namespace Matchnet
{

	/// <summary>
	/// Contract for standard Service Manager class
	/// </summary>
	public interface IServiceManager : IDisposable
	{
		/// <summary>
		/// 
		/// </summary>
		void PrePopulateCache();
	}
}
