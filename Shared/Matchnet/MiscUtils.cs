using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace Matchnet
{
    /// <summary>
    /// Summary description for MiscUtils.
    /// </summary>
    public class MiscUtils
    {
        public static Object BinaryDeserializeObject(Byte[] objBytes)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            Stream stream = new MemoryStream(objBytes);

            Object obj = formatter.Deserialize(stream);

            stream.Close();

            return obj;
        }

        public static Byte[] BinarySerializeObject(Object obj)
        {
            if (obj == null)
            {
                return null;
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();

            Stream stream = new MemoryStream();
            binaryFormatter.Serialize(stream, obj);

            Byte[] returnObj = ReadBytes(stream);

            stream.Close();

            return returnObj;
        }

        public static Byte[] ReadBytes(Stream stream)
        {
            return ReadBytes(stream, stream.Length);
        }

        /// <summary>
        /// Reads data from a stream until the end is reached. The
        /// data is returned as a byte array. An IOException is
        /// thrown if any of the underlying IO calls fail.
        /// </summary>
        /// <param name="stream">The stream to read data from</param>
        /// <param name="initialLength">The initial buffer length</param>
        public static Byte[] ReadBytes(Stream stream, Int64 initialLength)
        {
            stream.Position = 0;

            // If we've been passed an unhelpful initial length, set to largest 64bit value.
            if (initialLength < 1)
            {
                initialLength = Int64.MaxValue - 1;
            }

            Byte[] buffer = new Byte[initialLength];
            Int32 read = 0;

            Int32 chunk;
            while ((chunk = stream.Read(buffer, read, buffer.Length - read)) > 0)
            {
                read += chunk;

                // If we've reached the end of our buffer, check to see if there's
                // any more information
                if (read == buffer.Length)
                {
                    Int32 nextByte = stream.ReadByte();

                    // End of stream? If so, we're done
                    if (nextByte == -1)
                    {
                        return buffer;
                    }

                    // Nope. Resize the buffer, put in the byte we've just
                    // read, and continue
                    Byte[] newBuffer = new Byte[buffer.Length*2];
                    Array.Copy(buffer, newBuffer, buffer.Length);
                    newBuffer[read] = (Byte) nextByte;
                    buffer = newBuffer;
                    read++;
                }
            }
            // Buffer is now too big. Shrink it.
            Byte[] ret = new Byte[read];
            Array.Copy(buffer, ret, read);
            return ret;
        }

        /// <summary>
        /// Method that allows for async (non-blocking) calls to methods.  Based on this article:
        /// http://haacked.com/archive/2009/01/09/asynchronous-fire-and-forget-with-lambdas.aspx/
        /// </summary>
        /// <param name="callback">method to be run asynchronously</param>
        /// <param name="app">name of app running method</param>
        /// <param name="failureLogMessage">message to log in case of exception</param>
        /// <returns></returns>          
        public static bool FireAndForget(WaitCallback callback, string app, string failureLogMessage)
        {
            return ThreadPool.QueueUserWorkItem(o => {
                                                try
                                                {
                                                    callback(o);
                                                }
                                                catch (Exception e)
                                                {
                                                    ExceptionTraceHelper.Instance.Trace(app, e, failureLogMessage);
                                                }
                                            });
        }
    }
}