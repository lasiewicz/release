﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Matchnet
{
    [Serializable]
    public class ExceptionTraceHelper : IValueObject
    {
        private const string FORMAT_STR =
            "App Source:{0}\r\n============\r\n Ex.Source:{1} - Ex.Msg:{2}\r\n============";

        private const string FORMAT_STR_TRACE = "App Source:{0}\r\n============\r\nTrace:{1}\r\n============";

        public static readonly ExceptionTraceHelper Instance = new ExceptionTraceHelper();

        private ExceptionTraceHelper()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public void Trace(string appSource, Exception ex, object obj)
        {
            try
            {
                System.Diagnostics.EventLog.WriteEntry(appSource,getTrace(appSource, ex, obj), EventLogEntryType.Error);
            }
            catch (Exception e)
            {
                e = null;
            }
        }

        public void Trace(string appSource, string trace)
        {
            try
            {
                System.Diagnostics.EventLog.WriteEntry(appSource, String.Format(FORMAT_STR_TRACE, appSource, trace), EventLogEntryType.Error);
            }
            catch (Exception e)
            {
                e = null;
            }
        }

        public void DebugTrace(string appSource, string trace)
        {
            try
            {
                Trace(appSource, trace);
            }
            catch (Exception e)
            {
                e = null;
            }
        }

        public void DebugTrace(string appSource, Exception ex, object obj)
        {
            try
            {

#if DEBUG
                Trace(appSource, ex, obj);

#endif

            }
            catch (Exception e)
            {
                e = null;
            }
        }

        public string getTrace(string appSource, Exception ex, object obj)
        {
            string exStr = "";
            string innerexStr = "";


            try
            {
                exStr = String.Format(FORMAT_STR, appSource, ex.Source, ex.Message);
                if (ex.InnerException != null)
                    innerexStr = String.Format(FORMAT_STR, appSource, ex.InnerException.Source,
                                               ex.InnerException.Message);
                exStr += innerexStr;
                if (obj != null)
                    exStr += "\r\nValue Obj:" + obj.ToString();

                return exStr;


            }
            catch (Exception e)
            {
                e = null;
                return exStr;

            }
        }
    }
}