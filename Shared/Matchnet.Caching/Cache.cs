using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Caching;

using Matchnet;

namespace Matchnet.Caching
{
    /// <summary>
    /// Implements the cache for a MatchNet application.
    /// </summary>
    public sealed class Cache : ICaching
    {
        /// <summary>
        /// Retrieves a reference to the singleton instance of Matching.Caching.Cache
        /// </summary>
        public static readonly Cache Instance = new Cache();

        private Cache()
        {
            // Hide the default constructor to support the singleton model
        }


        /// <summary>
        /// An Matching.ICacheable object if the item was previously stored in the Cache; otherwise null.
        /// </summary>
        public object this[string key]
        {
            get
            {
                return this.Get(key);
            }
            set
            {
                Insert(key,value);
            }
        }


        public void Clear()
        {
            IDictionaryEnumerator de = System.Web.HttpRuntime.Cache.GetEnumerator();
            while (de.MoveNext())
            {
                this.Remove(de.Key.ToString());
            }
        }


        /// <summary>
        /// Retrieves the specified item from the Matching.Caching.Cache object.  
        /// </summary>
        /// <param name="key">The identifier for the cache item to retrieve.</param>
        /// <returns>An Matching.ICacheable object if the item was previously stored in the Cache; otherwise null</returns>
        public object Get(string key)
        {
            return System.Web.HttpRuntime.Cache[key] as ICacheable;
        }

        /// <summary>
        /// Retrieves multiple items from the Matching.Caching.Cache object.  
        /// </summary>
        /// <param name="keys">List of keys</param>
        /// <returns>Dictionary of key, value pairs</returns>
        public IDictionary<string, object> Get(List<string> keys)
        {
            IDictionary<string, object> objects = new Dictionary<string, object>();

            foreach (string key in keys)
            {
                objects[key]=Get(key);
            }
            return objects;
        }


        /// <summary>
        ///  Gets the number of items stored in the cache.
        /// </summary>
        public int Count
        {
            get
            {
                return System.Web.HttpRuntime.Cache.Count;
            }
        }


        /// <summary>
        ///  Retrieves a dictionary enumerator used to iterate through the key settings and their values contained in the cache.
        /// </summary>
        /// <returns>An enumerator to iterate through the System.Web.Caching.Cache object.</returns>
        public IEnumerator GetEnumerator()
        {
            return System.Web.HttpRuntime.Cache.GetEnumerator();
        }


        /// <summary>
        /// Retrieves the specified item from the Matching.Cache.Caching.Cache object.  
        /// </summary>
        /// <param name="key">The identifier for the cache item to retrieve.</param>
        /// <returns>An Matching.IValueObject if the item was previously stored in the Cache; otherwise null</returns>
        public object Remove(string key)
        {
            return System.Web.HttpRuntime.Cache.Remove(key) as ICacheable;
        }


        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        public void Insert(ICacheable cacheableObject)
        {
            if (cacheableObject == null)
            {
                return;
            }

            Insert(cacheableObject, null, cacheableObject.CachePriority, null);
        }


        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="overrideItemPriority">The cost of the object relative to other items stored in the cache, as expressed by the System.Web.Caching.CacheItemPriority enumeration. This value is used by the cache when it evicts objects; objects with a lower cost are removed from the cache before objects with a higher cost.</param>
        public void Insert(ICacheable cacheableObject, CacheItemPriorityLevel overrideItemPriority)
        {
            if (cacheableObject == null)
            {
                return;
            }
            Insert(cacheableObject, null, overrideItemPriority, null);
        }


        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="cacheDependency">The file or cache key dependencies for the item. When any dependency changes, the object becomes invalid and is removed from the cache. If there are no dependencies, this parameter contains null.</param>
        public void Insert(ICacheable cacheableObject, CacheDependency cacheDependency)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return;
            }
            Insert(cacheableObject, cacheDependency, cacheableObject.CachePriority, null);
        }


        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="onRemoveCallback">A delegate that, if provided, will be called when an object is removed from the cache. You can use this to notify applications when their objects are deleted from the cache.</param>
        public void Insert(ICacheable cacheableObject, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return;
            }
            Insert(cacheableObject, null, cacheableObject.CachePriority, onRemoveCallback);
        }


        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="cacheDependency">The file or cache key dependencies for the item. When any dependency changes, the object becomes invalid and is removed from the cache. If there are no dependencies, this parameter contains null.</param>
        /// <param name="onRemoveCallback">A delegate that, if provided, will be called when an object is removed from the cache. You can use this to notify applications when their objects are deleted from the cache.</param>
        public void Insert(ICacheable cacheableObject, CacheDependency cacheDependency, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return;
            }
            Insert(cacheableObject, cacheDependency, cacheableObject.CachePriority, onRemoveCallback);
        }


        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object with dependencies, expiration and priority policies, and a delegate you can use to notify your application when the inserted item is removed from the Cache . 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="cacheDependency">The file or cache key dependencies for the item. When any dependency changes, the object becomes invalid and is removed from the cache. If there are no dependencies, this parameter contains null.</param>
        /// <param name="overrideItemPriority">The cost of the object relative to other items stored in the cache, as expressed by the System.Web.Caching.CacheItemPriority enumeration. This value is used by the cache when it evicts objects; objects with a lower cost are removed from the cache before objects with a higher cost.</param>
        /// <param name="onRemoveCallback">A delegate that, if provided, will be called when an object is removed from the cache. You can use this to notify applications when their objects are deleted from the cache.</param>
        public void Insert(ICacheable cacheableObject, CacheDependency cacheDependency, CacheItemPriorityLevel overrideItemPriority, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return;
            }

            // Set default values for cache removal variables
            DateTime absoluteExpiration = System.Web.Caching.Cache.NoAbsoluteExpiration;
            TimeSpan slidingExpiration = System.Web.Caching.Cache.NoSlidingExpiration;

            // Configure caching based of the specified cache mode
            switch (cacheableObject.CacheMode)
            {
                case (CacheItemMode.Absolute):
                    absoluteExpiration = DateTime.Now.AddSeconds(Math.Abs(cacheableObject.CacheTTLSeconds));
                    break;
                case (CacheItemMode.Sliding):
                    slidingExpiration = TimeSpan.FromSeconds(Math.Abs(cacheableObject.CacheTTLSeconds));
                    break;
                default:
                    throw (new NotImplementedException("The specified Matching.CacheItemMode enumeration value is not supported."));
            }


            // Insert the object into the cache
            System.Web.HttpRuntime.Cache.Insert(cacheableObject.GetCacheKey(), cacheableObject, cacheDependency, absoluteExpiration, slidingExpiration, translatePriority(overrideItemPriority), onRemoveCallback);
        }


        /// <summary>
        /// Adds an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <returns>An Matching.IValueObject if the item was previously stored in the Cache; otherwise null</returns>
        public ICacheable Add(ICacheable cacheableObject)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, null, cacheableObject.CachePriority, null);
        }


        /// <summary>
        /// Adds an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="overrideItemPriority">The cost of the object relative to other items stored in the cache, as expressed by the System.Web.Caching.CacheItemPriority enumeration. This value is used by the cache when it evicts objects; objects with a lower cost are removed from the cache before objects with a higher cost.</param>
        /// <returns>An Matching.IValueObject if the item was previously stored in the Cache; otherwise null</returns>
        public ICacheable Add(ICacheable cacheableObject, CacheItemPriorityLevel overrideItemPriority)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, null, overrideItemPriority, null);
        }


        /// <summary>
        /// Adds an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="cacheDependency">The file or cache key dependencies for the item. When any dependency changes, the object becomes invalid and is removed from the cache. If there are no dependencies, this parameter contains null.</param>
        /// <returns>An Matching.IValueObject if the item was previously stored in the Cache; otherwise null</returns>
        public ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, cacheDependency, cacheableObject.CachePriority, null);
        }


        /// <summary>
        /// Adds an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="onRemoveCallback">A delegate that, if provided, will be called when an object is removed from the cache. You can use this to notify applications when their objects are deleted from the cache.</param>
        /// <returns>An Matching.IValueObject if the item was previously stored in the Cache; otherwise null</returns>
        public ICacheable Add(ICacheable cacheableObject, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, null, cacheableObject.CachePriority, onRemoveCallback);
        }


        /// <summary>
        /// Adds an object into the Matching.Caching.Cache object. 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="cacheDependency">The file or cache key dependencies for the item. When any dependency changes, the object becomes invalid and is removed from the cache. If there are no dependencies, this parameter contains null.</param>
        /// <param name="onRemoveCallback">A delegate that, if provided, will be called when an object is removed from the cache. You can use this to notify applications when their objects are deleted from the cache.</param>
        /// <returns>An Matching.IValueObject if the item was previously stored in the Cache; otherwise null</returns>
        public ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, cacheDependency, cacheableObject.CachePriority, onRemoveCallback);
        }


        /// <summary>
        /// Adds an object into the Matching.Caching.Cache object with dependencies, expiration and priority policies, and a delegate you can use to notify your application when the inserted item is removed from the Cache . 
        /// </summary>
        /// <param name="cacheableObject">The object (Matching.ICacheable) to be inserted in the cache.</param>
        /// <param name="cacheDependency">The file or cache key dependencies for the item. When any dependency changes, the object becomes invalid and is removed from the cache. If there are no dependencies, this parameter contains null.</param>
        /// <param name="overrideItemPriority">The cost of the object relative to other items stored in the cache, as expressed by the System.Web.Caching.CacheItemPriority enumeration. This value is used by the cache when it evicts objects; objects with a lower cost are removed from the cache before objects with a higher cost.</param>
        /// <param name="onRemoveCallback">A delegate that, if provided, will be called when an object is removed from the cache. You can use this to notify applications when their objects are deleted from the cache.</param>
        /// <returns>A Matching.IValueObject if the item was previously stored in the Cache; otherwise null</returns>
        public ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency, CacheItemPriorityLevel overrideItemPriority, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }

            // Set default values for cache removal variables
            DateTime absoluteExpiration = System.Web.Caching.Cache.NoAbsoluteExpiration;
            TimeSpan slidingExpiration = System.Web.Caching.Cache.NoSlidingExpiration;

            // Configure caching based of the specified cache mode
            switch (cacheableObject.CacheMode)
            {
                case (CacheItemMode.Absolute):
                    absoluteExpiration = DateTime.Now.AddSeconds(Math.Abs(cacheableObject.CacheTTLSeconds));
                    break;
                case (CacheItemMode.Sliding):
                    slidingExpiration = TimeSpan.FromSeconds(Math.Abs(cacheableObject.CacheTTLSeconds));
                    break;
                default:
                    throw (new NotImplementedException("The specified Matching.CacheItemMode enumeration value is not supported."));
            }


            // Add the object to the cache
            object cacheItem = System.Web.HttpRuntime.Cache.Add(cacheableObject.GetCacheKey(), cacheableObject, cacheDependency, absoluteExpiration, slidingExpiration, translatePriority(overrideItemPriority), onRemoveCallback) as IValueObject;

            if (cacheItem == null)
            {
                return null;
            }
            else
            {
                return (ICacheable)cacheItem;
            }
        }

        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Insert(string key, object value)
        {
            System.Web.HttpRuntime.Cache.Insert(key, value);
        }

        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object with dependencies
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="dependencies"></param>
        public void Insert(string key,
            object value,
            System.Web.Caching.CacheDependency dependencies)
        {
            System.Web.HttpRuntime.Cache.Insert(key, value, dependencies);
        }

        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object with dependencies, expiration and priority
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="dependencies"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="slidingExpiration"></param>
        public void Insert(string key,
            object value,
            System.Web.Caching.CacheDependency dependencies,
            DateTime absoluteExpiration,
            TimeSpan slidingExpiration)
        {
            System.Web.HttpRuntime.Cache.Insert(key, value, dependencies, absoluteExpiration, slidingExpiration);
        }

        /// <summary>
        /// Inserts an object into the Matching.Caching.Cache object with dependencies, expiration and priority policies, and a delegate you can use to notify your application when the inserted item is removed from the Cache . 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="dependencies"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="slidingExpiration"></param>
        /// <param name="priority"></param>
        /// <param name="onRemoveCallback"></param>
        public void Insert(string key,
            object value,
            System.Web.Caching.CacheDependency dependencies,
            DateTime absoluteExpiration,
            TimeSpan slidingExpiration,
            System.Web.Caching.CacheItemPriority priority,
            System.Web.Caching.CacheItemRemovedCallback onRemoveCallback)
        {
            System.Web.HttpRuntime.Cache.Insert(key, value, dependencies, absoluteExpiration, slidingExpiration, priority, onRemoveCallback);
        }

        /// <summary>
        /// Adds an object into the Matching.Caching.Cache object with dependencies, expiration and priority policies, and a delegate you can use to notify your application when the inserted item is removed from the Cache . 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="dependencies"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="slidingExpiration"></param>
        /// <param name="priority"></param>
        /// <param name="onRemoveCallback"></param>
        /// <returns></returns>
        public object Add(string key,
           object value,
           System.Web.Caching.CacheDependency dependencies,
           DateTime absoluteExpiration,
           TimeSpan slidingExpiration,
           System.Web.Caching.CacheItemPriority priority,
           System.Web.Caching.CacheItemRemovedCallback onRemoveCallback)
        {
            return System.Web.HttpRuntime.Cache.Add(key, value, dependencies, absoluteExpiration, slidingExpiration, priority, onRemoveCallback);
        }


        private CacheItemPriority translatePriority(CacheItemPriorityLevel cacheItemPriorityLevel)
        {
            switch (cacheItemPriorityLevel)
            {
                case (Matchnet.CacheItemPriorityLevel.High):
                    return CacheItemPriority.High;
                case (Matchnet.CacheItemPriorityLevel.AboveNormal):
                    return CacheItemPriority.AboveNormal;
                case (Matchnet.CacheItemPriorityLevel.Normal):
                    return CacheItemPriority.Normal;
                case (Matchnet.CacheItemPriorityLevel.BelowNormal):
                    return CacheItemPriority.BelowNormal;
                case (Matchnet.CacheItemPriorityLevel.Low):
                    return CacheItemPriority.Low;
                case (Matchnet.CacheItemPriorityLevel.Default):
                    return CacheItemPriority.Default;
                default:
                    return CacheItemPriority.Default;
            }
        }

        /// <summary>
        /// Removes specfied item from cache. Returns flag indicating successful removal.
        /// </summary>
        /// <param name="key">The identifier for the cache item to retrieve.</param>
        /// <returns>boolean flag indicating successful removal</returns>
        public bool RemoveWithoutGet(string key)
        {
            return null != System.Web.HttpRuntime.Cache.Remove(key);
        }

    }
}
