﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Caching.CacheStrategies
{
    /// <summary>
    /// Uses Matchnet Cache to provide Caching abilities
    /// </summary>
    public class MatchnetCacheBasicProvider : IBasicCacheProvider
    {
        public object Get(string key)
        {
            return Cache.Instance.Get(key);
        }

        public ICacheable Add(ICacheable cacheableObject)
        {
            return Cache.Instance.Add(cacheableObject);
        }
    }
}
