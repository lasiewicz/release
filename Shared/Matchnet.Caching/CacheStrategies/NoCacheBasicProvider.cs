﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Caching.CacheStrategies
{
    /// <summary>
    /// Mainly to be used in tests where the logic shall be tested each time it is called.
    /// </summary>
    public class NoCacheBasicProvider : IBasicCacheProvider
    {
        public object Get(string key)
        {
            return null;
        }

        public ICacheable Add(ICacheable cacheableObject)
        {
            return cacheableObject;
        }
    }
}
