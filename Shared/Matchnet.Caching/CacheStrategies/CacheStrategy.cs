﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.BaseClasses;
using Matchnet.Caching.CacheStrategies;

namespace Matchnet.Caching
{
    public class CacheStrategy
    {

        public CacheStrategy(IBasicCacheProvider cahceProvider)
        {
            _cacheProvider = cahceProvider;
        }

        private IBasicCacheProvider _cacheProvider;

        public List<TValue> GetValuesByKeys<TKey, TValue>(List<TKey> keys,
                 Func<List<TKey>, List<ItemMapping<TKey, TValue>>> dbFetcher,
                 Func<TKey, List<TValue>, CacheableListMapping<TKey, TValue>> mappingInfoFactory,
                 Func<TKey, string> sourceKeyProvider
)
        {
            var ret = new List<TValue>();

            // Split by Cached and non-cached items
            var notCachedSource = new List<TKey>();
            foreach (var s in keys)
            {
                var cachedSource = _cacheProvider.Get(sourceKeyProvider(s)) as CacheableListMapping<TKey, TValue>;

                if (cachedSource == null)
                    notCachedSource.Add(s);
                else
                    ret.AddRange(cachedSource.Value);
            }

            // Fetch non-cached items from db
            var res = dbFetcher(notCachedSource);

            // Group recently fetched items to prepare for cache
            var groupedItems = new Dictionary<TKey, List<TValue>>();
            foreach (var item in res)
            {
                if (!groupedItems.ContainsKey(item.Key))
                    groupedItems[item.Key] = new List<TValue>();
                groupedItems[item.Key].Add(item.Value);
                ret.Add(item.Value);
            }

            // Add the prepared items to cache
            foreach (var item in groupedItems)
            {
                _cacheProvider.Add(mappingInfoFactory(item.Key, item.Value));
            }
            return ret;
        }

        public TValue GetValueByKey<TKey, TValue>(TKey key, Func<TKey, TValue> dbFetcher, Func<TKey, string> sourceKeyProvider)
            where TValue : class, ICacheable
        {
            var cachedObject = _cacheProvider.Get(sourceKeyProvider(key)) as TValue;
            if (cachedObject != null)
                return cachedObject;
            var objectFromDb = dbFetcher(key);
            _cacheProvider.Add(objectFromDb);
            return objectFromDb;
        }
    }
}
