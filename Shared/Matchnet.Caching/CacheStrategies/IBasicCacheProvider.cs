﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Caching.CacheStrategies
{
    public interface IBasicCacheProvider
    {
        object Get(string key);
        ICacheable Add(ICacheable cacheableObject);
    }
}
