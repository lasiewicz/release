﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Caching.CacheBuster.HTTPContextWrappers;
using Matchnet.Caching.CacheBuster.Interfaces;

namespace Matchnet.Caching.CacheBuster
{
    public class CacheBuster
    {
        private ICurrentServer _currentServer;
        private ICurrentRequest _request;

        public ICurrentRequest CurrentRequest
        {
            get
            {
                _request = _request ?? new CurrentRequest();
                return _request;
            }
            set { _request = value; }
        }

        public ICurrentServer CurrentServer
        {
            get
            {
                _currentServer = _currentServer ?? new CurrentServer();
                return _currentServer;
            }
            set { _currentServer = value; }
        }

        public bool BustCache(string cacheKey)
        {
            bool cacheBusted = false;

            var cacheBusterAccess = new CacheBusterAccess { CurrentServer = CurrentServer };
            if (cacheBusterAccess.HasAccess(CurrentRequest.ClientIP))
            {
                Cache.Instance.Remove(cacheKey);
                cacheBusted = true;
            }

            return cacheBusted;
        }

        public bool BustCacheWithPrefix(string cacheKeyPrefix)
        {
            bool cacheBusted = false;
            var itemsToRemove = new List<string>();
            
            var cacheBusterAccess = new CacheBusterAccess { CurrentServer = CurrentServer };
            if (cacheBusterAccess.HasAccess(CurrentRequest.ClientIP))
            {
                var cacheEnumerator = Cache.Instance.GetEnumerator() as IDictionaryEnumerator;

                if (cacheEnumerator != null)
                {
                    while (cacheEnumerator.MoveNext())
                    {
                        if (cacheEnumerator.Key.ToString().ToLower().StartsWith(cacheKeyPrefix))
                        {
                            itemsToRemove.Add(cacheEnumerator.Key.ToString());
                        }
                    }

                    foreach (var key in itemsToRemove)
                    {
                         Cache.Instance.Remove(key);
                    }
                }

                cacheBusted = true;
            }

            return cacheBusted;
        }

    }
}
