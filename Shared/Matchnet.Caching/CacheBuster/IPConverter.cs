﻿using System.Net;

namespace Matchnet.Caching.CacheBuster
{
    public class IPConverter
    {
        public int ToNumber(string ipAddress)
        {
            int ipNumber;

            try
            {
                //sanity check
                var convertedAddress = IPAddress.Parse(ipAddress);

                //if here, the ip is valid
                var octets = ipAddress.Split('.');

                ipNumber = int.Parse(octets[0]) * 16777216 + int.Parse(octets[1]) * 65536 + int.Parse(octets[2]) * 256 + int.Parse(octets[3]);
            }
            catch
            {
                ipNumber = 0;
            }

            return ipNumber;
        }
    }
}
