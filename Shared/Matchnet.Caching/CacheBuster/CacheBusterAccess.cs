﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Caching.CacheBuster.HTTPContextWrappers;
using Matchnet.Caching.CacheBuster.Interfaces;

namespace Matchnet.Caching.CacheBuster
{
    public class CacheBusterAccess
    {
        private ICurrentServer _currentServer;

        public ICurrentServer CurrentServer
        {
            get
            {
                _currentServer = _currentServer ?? new CurrentServer();
                return _currentServer;
            }
            set { _currentServer = value; }
        }

        public bool HasAccess(string ipAddress)
        {
            var hasAccess = false;
            var ipConverter = new IPConverter();

            int ipAddressAsNumber = ipConverter.ToNumber(ipAddress);

            if (ipAddressAsNumber == 0)
            {
                return hasAccess;
            }

            var reader = new IPRangeWhitelistReader();
            var configPath = CurrentServer.MapPath("~/Cachebuster/IPRangeWhitelist.xml");
            var whitelist = reader.GetWhitelistFromConfigFile(configPath);

            foreach (var ipRange in whitelist.IPRanges)
            {
                var beginningAddressAsNumber = ipConverter.ToNumber(ipRange.BeginningAddress);
                var endingAddressAsNumber = ipConverter.ToNumber(ipRange.EndingAddress);

                if (beginningAddressAsNumber != 0 && endingAddressAsNumber != 0 &&
                    ipAddressAsNumber >= beginningAddressAsNumber &&
                    ipAddressAsNumber <= endingAddressAsNumber)
                {
                    hasAccess = true;
                }
            }

            return hasAccess;
        }
    }
}
