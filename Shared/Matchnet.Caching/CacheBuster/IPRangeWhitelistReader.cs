﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Matchnet.Caching.CacheBuster
{
    public class IPRangeWhitelistReader
    {
        public IPRangeWhitelist GetWhitelistFromConfigFile(string filePath)
        {
            IPRangeWhitelist whitelist = null;

            TextReader textReader = null;
            try
            {
                textReader = new StreamReader(filePath);
                var deserializer = new XmlSerializer(typeof(IPRangeWhitelist));
                whitelist = deserializer.Deserialize(textReader) as IPRangeWhitelist;
            }
            catch
            {
                //it's ok to supress this error for now
            }
            finally
            {
                if (textReader != null)
                {
                    textReader.Close();
                }
            }

            return whitelist;
        }
    }
}
