﻿using System.Web;
using Matchnet.Caching.CacheBuster.Interfaces;

namespace Matchnet.Caching.CacheBuster.HTTPContextWrappers
{
    public class CurrentServer : ICurrentServer
    {
        public string MapPath(string path)
        {
            return HttpContext.Current.Server.MapPath(path);
        }
    }
}
