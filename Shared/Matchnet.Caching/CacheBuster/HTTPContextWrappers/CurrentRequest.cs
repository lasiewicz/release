﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Matchnet.Caching.CacheBuster.Interfaces;

namespace Matchnet.Caching.CacheBuster.HTTPContextWrappers
{
    public class CurrentRequest : ICurrentRequest
    {
        private string _clientIP;
        
        public string ClientIP
        {
            get
            {
                if (_clientIP == null)
                {
                    _clientIP = HttpContext.Current.Request.Headers["client-ip"] ?? HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];

                    Int32 commaPos = _clientIP.IndexOf(",");
                    if (commaPos > -1)
                    {
                        _clientIP = _clientIP.Substring(0, commaPos);
                    }
                }

                return _clientIP;
            }
        }
    }
}
