﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Matchnet.Caching.CacheBuster
{
    [Serializable]
    public class IPRangeWhitelist
    {
        [XmlElement("IPRange")]
        public List<IPRange> IPRanges { get; set; }
    }
}
