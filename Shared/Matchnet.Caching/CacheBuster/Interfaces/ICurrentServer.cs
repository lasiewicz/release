﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Caching.CacheBuster.Interfaces
{
    public interface ICurrentServer
    {
        string MapPath(string path);
    }
}
