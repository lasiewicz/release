﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Caching.CacheBuster.Interfaces
{
    public interface ICurrentRequest
    {
        string ClientIP { get; }
    }
}
