﻿using System;
using System.Xml.Serialization;

namespace Matchnet.Caching.CacheBuster
{
    [Serializable]
    public class IPRange
    {
        [XmlAttribute("BeginningAddress")]
        public string BeginningAddress { get; set; }

        [XmlAttribute("EndingAddress")]
        public string EndingAddress { get; set; }
    }
}
