﻿using Matchnet.Caching.CacheBuster.Interfaces;

namespace Matchnet.CachingTest.Mocks
{
    public class CurrentServerMock : ICurrentServer
    {
        private string _mappedPath = string.Empty;

        #region ICurrentServer Members

        public string MapPath(string path)
        {
            return _mappedPath;
        }

        #endregion

        public void SetMappedPath(string path)
        {
            _mappedPath = path;
        }
    }
}
