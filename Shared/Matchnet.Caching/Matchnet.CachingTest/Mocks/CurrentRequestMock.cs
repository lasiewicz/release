﻿using Matchnet.Caching.CacheBuster.Interfaces;

namespace Matchnet.CachingTest.Mocks
{
    public class CurrentRequestMock : ICurrentRequest
    {
        private string _clientIP = "192.168.3.1";
        
        public string ClientIP
        {
            get { return _clientIP; }
        }

        public void SetClientIP(string setClientIP)
        {
            _clientIP = setClientIP;
        }
    }
}
