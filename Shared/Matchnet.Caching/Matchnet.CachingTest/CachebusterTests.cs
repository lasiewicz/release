﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Caching;
using Matchnet.Caching.CacheBuster;
using Matchnet.CachingTest.Mocks;
using NUnit.Framework;
using Cache = Matchnet.Caching.Cache;

namespace Matchnet.CachingTest
{
    public class CachebusterTests
    {
        [Test]
        public void TestReaderWorksWithValidFile()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var path = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "IPRangeWhitelist.xml";
            var ipReader = new IPRangeWhitelistReader();
            var whitelist = ipReader.GetWhitelistFromConfigFile(path);
            Assert.IsNotNull(whitelist);
            Assert.IsNotNull(whitelist.IPRanges);
            Assert.IsTrue(whitelist.IPRanges.Count == 2);
            Assert.IsTrue(whitelist.IPRanges[0].BeginningAddress == "192.168.0.0");
            Assert.IsTrue(whitelist.IPRanges[0].EndingAddress == "192.168.0.255");
            Assert.IsTrue(whitelist.IPRanges[1].BeginningAddress == "192.168.1.0");
            Assert.IsTrue(whitelist.IPRanges[1].EndingAddress == "192.168.1.255");
        }

        [Test]
        public void TestReaderReturnsNullWithInvalidFile()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var path = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "NotExists.xml";
            var ipReader = new IPRangeWhitelistReader();
            var whitelist = ipReader.GetWhitelistFromConfigFile(path);
            Assert.IsNull(whitelist);
        }

        [Test]
        public void TestAccessWithValidIP()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var configPath = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "IPRangeWhitelist.xml";

            var serverMock = new CurrentServerMock();
            serverMock.SetMappedPath(configPath);

            var ip = "192.168.1.10";
            var cacheBusterAccess = new CacheBusterAccess();
            cacheBusterAccess.CurrentServer = serverMock;

            var hasAccess = cacheBusterAccess.HasAccess(ip);
            Assert.IsTrue(hasAccess);
        }

        [Test]
        public void TestAccessWithInvalidIP()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var configPath = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "IPRangeWhitelist.xml";

            var serverMock = new CurrentServerMock();
            serverMock.SetMappedPath(configPath);

            var ip = "192.168.1000.10";
            var cacheBusterAccess = new CacheBusterAccess();
            cacheBusterAccess.CurrentServer = serverMock;

            var hasAccess = cacheBusterAccess.HasAccess(ip);
            Assert.IsFalse(hasAccess);
        }

        [Test]
        public void TestAccessWithValidIPOutsideRange()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var configPath = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "IPRangeWhitelist.xml";

            var serverMock = new CurrentServerMock();
            serverMock.SetMappedPath(configPath);

            var ip = "192.168.3.1";
            var cacheBusterAccess = new CacheBusterAccess();
            cacheBusterAccess.CurrentServer = serverMock;

            var hasAccess = cacheBusterAccess.HasAccess(ip);
            Assert.IsFalse(hasAccess);
        }

        [Test]
        public void TestBustCache()
        {
            Cache.Instance.Clear();
            
            var currentDirectory = Directory.GetCurrentDirectory();
            var configPath = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "IPRangeWhitelist.xml";

            var serverMock = new CurrentServerMock();
            serverMock.SetMappedPath(configPath);

            var requestMock = new CurrentRequestMock();
            requestMock.SetClientIP("192.168.1.10");

            var cacheBuster = new CacheBuster();
            cacheBuster.CurrentRequest = requestMock;
            cacheBuster.CurrentServer = serverMock;

            var cacheableObject1 = new CacheableObject(1);
            var cacheableObject2 = new CacheableObject(2);
            
            Cache.Instance.Add(cacheableObject1.GetCacheKey(), cacheableObject1, null, DateTime.Now.AddHours(6), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
            Cache.Instance.Add(cacheableObject2.GetCacheKey(), cacheableObject2, null, DateTime.Now.AddHours(6), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);

            var cachedObject1 = Cache.Instance.Get(cacheableObject1.GetCacheKey()) as CacheableObject;
            var cachedObject2 = Cache.Instance.Get(cacheableObject2.GetCacheKey()) as CacheableObject;

            Assert.IsNotNull(cachedObject1);
            Assert.IsNotNull(cachedObject2);

            var cachebusted = cacheBuster.BustCache(cacheableObject1.GetCacheKey());
            Assert.IsTrue(cachebusted);

            cachebusted = cacheBuster.BustCache(cacheableObject2.GetCacheKey());
            Assert.IsTrue(cachebusted);

            cachedObject1 = Cache.Instance.Get(cacheableObject1.GetCacheKey()) as CacheableObject;
            cachedObject2 = Cache.Instance.Get(cacheableObject2.GetCacheKey()) as CacheableObject;

            Assert.IsNull(cachedObject1);
            Assert.IsNull(cachedObject2);

        }

        [Test]
        public void TestBustCacheWithPrefix()
        {
            Cache.Instance.Clear();
            
            var currentDirectory = Directory.GetCurrentDirectory();
            var configPath = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "IPRangeWhitelist.xml";

            var serverMock = new CurrentServerMock();
            serverMock.SetMappedPath(configPath);

            var requestMock = new CurrentRequestMock();
            requestMock.SetClientIP("192.168.1.10");

            var cacheBuster = new CacheBuster();
            cacheBuster.CurrentRequest = requestMock;
            cacheBuster.CurrentServer = serverMock;

            var cacheableObject1 = new CacheableObject(1);
            var cacheableObject2 = new CacheableObject(2);

            Cache.Instance.Add(cacheableObject1.GetCacheKey(), cacheableObject1, null, DateTime.Now.AddHours(6), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
            Cache.Instance.Add(cacheableObject2.GetCacheKey(), cacheableObject2, null, DateTime.Now.AddHours(6), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);

            var cachebusted = cacheBuster.BustCacheWithPrefix(CacheableObject.CACHE_KEY);
            Assert.IsTrue(cachebusted);

            var cachedObject1 = Cache.Instance.Get(cacheableObject1.GetCacheKey()) as CacheableObject;
            var cachedObject2 = Cache.Instance.Get(cacheableObject2.GetCacheKey()) as CacheableObject;

            Assert.IsNull(cachedObject1);
            Assert.IsNull(cachedObject2);
        }

        [Test]
        public void TestBustCacheWithPrefixWorksWhenNoObjectsWithPrefixPresent()
        {
            Cache.Instance.Clear();
            
            var currentDirectory = Directory.GetCurrentDirectory();
            var configPath = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "IPRangeWhitelist.xml";

            var serverMock = new CurrentServerMock();
            serverMock.SetMappedPath(configPath);

            var requestMock = new CurrentRequestMock();
            requestMock.SetClientIP("192.168.1.10");

            var cacheBuster = new CacheBuster();
            cacheBuster.CurrentRequest = requestMock;
            cacheBuster.CurrentServer = serverMock;

            var cachebusted = cacheBuster.BustCacheWithPrefix(CacheableObject.CACHE_KEY);
            Assert.IsTrue(cachebusted);
        }

        [Test]
        public void TestBustCacheWithPrefixLeavesOtherObjectsInCache()
        {
            Cache.Instance.Clear();
            
            var currentDirectory = Directory.GetCurrentDirectory();
            var configPath = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "IPRangeWhitelist.xml";

            var serverMock = new CurrentServerMock();
            serverMock.SetMappedPath(configPath);

            var requestMock = new CurrentRequestMock();
            requestMock.SetClientIP("192.168.1.10");

            var cacheBuster = new CacheBuster();
            cacheBuster.CurrentRequest = requestMock;
            cacheBuster.CurrentServer = serverMock;

            var cacheableObject1 = new CacheableObject(1);
            var cacheableObject2 = new CacheableObject(2);
            var cacheableObject3 = new CacheableObject2(3);

            Cache.Instance.Add(cacheableObject1.GetCacheKey(), cacheableObject1, null, DateTime.Now.AddHours(6), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
            Cache.Instance.Add(cacheableObject2.GetCacheKey(), cacheableObject2, null, DateTime.Now.AddHours(6), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
            Cache.Instance.Add(cacheableObject3.GetCacheKey(), cacheableObject3, null, DateTime.Now.AddHours(6), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);

            cacheBuster.BustCacheWithPrefix(CacheableObject.CACHE_KEY);

            var cachedObject1 = Cache.Instance.Get(cacheableObject3.GetCacheKey()) as CacheableObject2;

            Assert.IsNotNull(cachedObject1);
        }
    }

    public class CacheableObject: ICacheable
    {
        public static string CACHE_KEY = "cacheobjectcachekey";
        public int ID { get; set; }

        public CacheableObject(int id)
        {
            ID = id;
        }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.High;
            }
            set
            {
                
            }
        }

        public int CacheTTLSeconds
        {
            get { return 21600; }
            set
            {
                
            }
        }

        public string GetCacheKey()
        {
            return CACHE_KEY + ID.ToString();
        }

        #endregion
    }

    public class CacheableObject2 : ICacheable
    {
        public static string CACHE_KEY = "cacheobject2cachekey";
        public int ID { get; set; }

        public CacheableObject2(int id)
        {
            ID = id;
        }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.High;
            }
            set
            {

            }
        }

        public int CacheTTLSeconds
        {
            get { return 21600; }
            set
            {

            }
        }

        public string GetCacheKey()
        {
            return CACHE_KEY + ID.ToString();
        }

        #endregion
    }
}
