﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Matchnet.CachingTest
{
    [TestFixture]
    public class MembaseCacheTest
    {
        private Caching.Cache _cacheInstance;
        [TestFixtureSetUp]
        public void StartUp()
        {
            _cacheInstance = Caching.Cache.Instance;
            //_cacheInstance.UseNunit = true;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            //_cacheInstance.UseNunit = false;
        }

        
        public void TestClear()
        {
            _cacheInstance.Clear();
            Assert.AreEqual(0,_cacheInstance.Count);
        }

        [Test]
        public void TestAdd()
        {
            ICacheable cacheObj = new TestCacheable(1);
            _cacheInstance.Remove(cacheObj.GetCacheKey());
            ICacheable val = _cacheInstance.Add(cacheObj);
            Assert.Null(val);
            val = _cacheInstance.Add(cacheObj);
            Assert.NotNull(val);
            Assert.AreEqual(cacheObj, val);
        }

        [Test]
        public void TestInsertAndGet()
        {
            ICacheable cacheObj = new TestCacheable(4);
            _cacheInstance.Remove(cacheObj.GetCacheKey());
            _cacheInstance.Insert(cacheObj);
            var val = _cacheInstance.Get(cacheObj.GetCacheKey());
            Assert.AreEqual(((TestCacheable)cacheObj).Value,((TestCacheable)val).Value);
        }

        [Test]
        public void TestRemove()
        {
            ICacheable cacheObj = new TestCacheable(3);
            _cacheInstance.Insert(cacheObj);
            ICacheable val = _cacheInstance.Remove(cacheObj.GetCacheKey());
            Assert.AreEqual(((TestCacheable)cacheObj).Value, ((TestCacheable)val).Value);
            val = _cacheInstance.Get(cacheObj.GetCacheKey());
            Assert.Null(val);
        }
    }

    [Serializable]
    public class TestCacheable : ICacheable
    {
        private string val = "MembaseUnitTest";
        private int index = 0;
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;

        public string Value { get { return val; } }

        public TestCacheable(int index)
        {
            this.index = index;
        }

        public string GetCacheKey()
        {
            return "Key_Test_" + index;
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }
    }
}
