﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.BaseClasses;
using Matchnet.Caching.CacheStrategies;

namespace Matchnet.Caching
{
    /// <summary>
    /// Singleton around CacheStrategy
    /// </summary>
    public static class CacheHelper
    {
        static CacheStrategy  _cacheStrategy = new CacheStrategy(new MatchnetCacheBasicProvider()); 

        public static List<TValue> GetValuesByKeys<TKey, TValue>(List<TKey> keys,
                        Func<List<TKey>, List<ItemMapping<TKey, TValue>>> dbFetcher,
                        Func<TKey, List<TValue>, CacheableListMapping<TKey, TValue>> mappingInfoFactory,
                        Func<TKey, string> sourceKeyProvider
)
        {
            return _cacheStrategy.GetValuesByKeys(keys, dbFetcher, mappingInfoFactory, sourceKeyProvider);
        }

        public static TValue GetValueByKey<TKey, TValue>(TKey key, Func<TKey, TValue> dbFetcher, Func<TKey, string> sourceKeyProvider)
            where TValue : class, ICacheable
        {
            return _cacheStrategy.GetValueByKey(key, dbFetcher, sourceKeyProvider);
        }
    }
}
