using System;


namespace Matchnet.Queuing
{
	public interface IProcessorWorker
	{
		void ProcessMessage(object messageBody);
	}
}
