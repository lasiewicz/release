using System;

namespace Matchnet.Queuing
{
	[Serializable]
	public class InitMessage : IInitMessage
	{
		public void Process()
		{
			System.Diagnostics.Trace.WriteLine("__InitMessage");
		}
	}
}
