using System;


namespace Matchnet.Queuing
{
	internal interface IProcessorInternal : IDisposable
	{
		void Receive(string queuePath,
			TimeSpan timeout,
			IProcessorWorker processorWorker,
			string serviceName);
	}
}
