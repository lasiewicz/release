using System;
using System.EnterpriseServices;
using System.Messaging;
using System.Threading;

using Matchnet.Exceptions;


namespace Matchnet.Queuing
{
	[Transaction(TransactionOption.Required)]
	public class TransactionalProcessorInternal : ServicedComponent, IProcessorInternal
	{
		public void Receive(string queuePath,
			TimeSpan timeout,
			IProcessorWorker processorWorker,
			string serviceName)
		{
			System.Messaging.Message message = null;
            MessageQueueTransaction trans = null;

            try
            {
                try
                {
                    trans = new MessageQueueTransaction();

                    trans.Begin();

                    message = Util.GetQueue(queuePath, true, true).Receive(timeout, trans);
                }
                catch (MessageQueueException mqEx)
                {
                    if (trans != null)
                    {
                        if (trans.Status == MessageQueueTransactionStatus.Pending)
                        {
                            trans.Abort();
                        }
                        trans.Dispose();
                    }
                    if (mqEx.Message != "Timeout for the requested operation has expired.")
                    {
                        throw mqEx;
                    }
                    else
                    {
                        //Queue is empty. Make the thread sleep.
                        Thread.Sleep(1000);
                    }
                }

                if (message != null)
                {
                    processorWorker.ProcessMessage(message.Body);
                }

                if (trans != null && trans.Status == MessageQueueTransactionStatus.Pending)
                {
                    trans.Commit();
                }

                //if (ContextUtil.IsInTransaction)
                //{
                //    ContextUtil.SetComplete();
                //}
            }
            catch (Exception ex)
            {
                //ContextUtil.SetAbort();
                if (trans != null)
                {
                    if (trans.Status == MessageQueueTransactionStatus.Pending)
                    {
                        trans.Abort();
                    }
                    trans.Dispose();
                }
                new ServiceBoundaryException(serviceName, "Error processing queue item (queuePath: " + queuePath + ", worker type: " + processorWorker.GetType().ToString() + ")", ex);
                Thread.Sleep(5000);
            }
            finally
            {
                if (trans != null)
                {
                    trans.Dispose();
                }
            }
		}
	}
}
