using System;

namespace Matchnet.Queuing
{
	public interface IInitMessage
	{
		void Process();
	}
}
