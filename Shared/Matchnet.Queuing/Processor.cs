using System;
using System.Messaging;
using System.Threading;


namespace Matchnet.Queuing
{
	public class Processor
	{
		private string _queuePath;
		private byte _threadCount;
		private bool _transactional;
		private IProcessorWorker _processorWorker;
		private string _serviceName;
		private bool _runnable = true;
		private Thread[] _threads;

		
		public Processor(string queuePath,
			byte threadCount,
			bool transactional,
			IProcessorWorker processorWorker,
			string serviceName)
		{
			_queuePath = queuePath;
			_threadCount = threadCount;
			_transactional = transactional;
			_processorWorker = processorWorker;
			_serviceName = serviceName;
		}


		public void Start()
		{
			_runnable = true;
			if (!MessageQueue.Exists(_queuePath))
			{
				MessageQueue.Create(_queuePath, _transactional);
			}

			_threads = new Thread[_threadCount];

			for (byte threadNum = 0; threadNum < _threadCount; threadNum++)
			{
				_threads[threadNum] = new Thread(new ThreadStart(workCycle));
				_threads[threadNum].Start();
			}
		}


		public void Stop()
		{
			_runnable = false;

		}


		public void Join()
		{
			for (byte threadNum = 0; threadNum < _threadCount; threadNum++)
			{
				_threads[threadNum].Join();
			}
		}


		private void workCycle()
		{
			IProcessorInternal processorInternal = null;
			TimeSpan timeout = new TimeSpan(0, 0, 0, 0, 500);

			while (_runnable)
			{
				try
				{
					if (_transactional)
					{
						processorInternal = new TransactionalProcessorInternal();
					}
					else
					{
						//processorInternal = new StandardProcessorInternal();
					}

					processorInternal.Receive(_queuePath,
						timeout,
						_processorWorker,
						_serviceName);

					processorInternal.Dispose();
				}
				catch (Exception ex)
				{
					System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", "").Replace("\n", ""));
					Thread.Sleep(10000);
				}
			}
		}
	}
}
