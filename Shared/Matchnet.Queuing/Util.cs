using System;
using System.Messaging;


namespace Matchnet.Queuing
{
	public class Util
	{
		private Util()
		{
		}


		public static MessageQueue GetQueue(string queuePath,
			bool create,
			bool transactional)
		{
			MessageQueue queue = null;
			
			if (MessageQueue.Exists(queuePath))
			{
				queue = new MessageQueue(queuePath);
			}
			else if (create)
			{
				queue = MessageQueue.Create(queuePath, transactional);
			}
			else
			{
				throw new Exception("Queue does not exist (queuePath: " + queuePath + ").");
			}

			queue.Formatter = new BinaryMessageFormatter();

			return queue;
		}
	}
}
