﻿using System;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Spark.Logging;
//using Spark.Managers.HTTPContextWrappers;
using Spark.Managers.Interfaces;

namespace Spark.Managers.Managers
{
    public class LoggingManager: ILoggingManager
    {
		//private ICurrentRequest _currentRequest = null;

		//public ICurrentRequest CurrentRequest
		//{
		//    get { return _currentRequest ?? new CurrentRequest(); }
		//    set { _currentRequest = value; }
		//}

        public void LogException(Exception exception, IMember member, Brand brand, string className)
        {
            if(exception == null)
            {
                return;
            }
            
            if (exception.GetType().ToString().ToLower() == "system.threading.threadabortexception")
            {
                return;
            }

            if (FilterViewStateException(exception))
            {
                return;
            }

            StringBuilder sb = new StringBuilder();
            
            sb.Append(exception.Source + "-" + exception.Message);

            if(member != null)
            {
                sb.Append("\r\nMemberID:" + member.MemberID.ToString());
            }
            if(brand != null)
            {
                sb.Append("\r\nBrandID:" + brand.BrandID.ToString());
            }
            
			//sb.Append("\r\nURL:" + CurrentRequest.Url);
			//sb.Append("\r\nRefURL=" + CurrentRequest.UrlReferrer);
			//sb.Append("\r\nHost:" + CurrentRequest.ClientIP);
            sb.Append("\r\n" + exception + "\r\n");
            sb.Append("\r\nStack=" + exception.StackTrace);
            
            if (exception.InnerException != null)
            {
                sb.Append("\r\nInnerException=" +exception.InnerException);
                sb.Append("\r\nStackInner=" + exception.InnerException.StackTrace);
            }

            RollingFileLogger.Instance.LogException("WEB", className, exception, sb.ToString());
        }

        public void LogInfoMessage(string className, string message)
        {
             RollingFileLogger.Instance.LogInfoMessage("WEB", className, message, null);
        }

        private bool FilterViewStateException(Exception ex)
        {
            bool ret = false;
            try
            {
				//string url = CurrentRequest.Url.ToString().ToLower();
				//string rawurl = CurrentRequest.RawUrl.ToLower();
				//if (url.IndexOf("anthem_callback=true") >= 0 && rawurl.IndexOf("registration") < 0)
				//    ret = true;
                return ret;
            }
            catch (Exception)
            { return ret; }
        }
    }
}