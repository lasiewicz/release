﻿using System;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Managers.Interfaces;
using Matchnet;

namespace Spark.Managers.Managers
{
    public class SettingsManager
    {
        private ISettingsSA _settingsService;
        private ILoggingManager _loggingManager = null;

        public ISettingsSA SettingsService
        {
            get{ return _settingsService ?? RuntimeSettings.Instance; }
            set { _settingsService = value; }
        }

        public ILoggingManager LoggingManager
        {
            get
            {
                if (_loggingManager == null)
                {
                    _loggingManager = new LoggingManager();
                }
                return _loggingManager;
            }

            set { _loggingManager = value; }
        }

        public int GetSettingInt(string settingConstant, Brand brand)
        {
            int settingValue = Constants.NULL_INT;
            
            try
            {
                settingValue = Conversion.CInt((brand == null)
                    ? SettingsService.GetSettingFromSingleton(settingConstant) 
                    : SettingsService.GetSettingFromSingleton(settingConstant, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            }
            catch (Exception ex)
            {
                LoggingManager.LogException(ex, null, brand, "SettingsManager");
            }

            return settingValue;

        }

        public bool GetSettingBool(string settingConstant, Brand brand)
        {
            bool settingValue = false;
           
            try
            {
                settingValue = Conversion.CBool((brand == null)
                    ? SettingsService.GetSettingFromSingleton(settingConstant)
                    : SettingsService.GetSettingFromSingleton(settingConstant, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            }
            catch (Exception ex)
            {
                LoggingManager.LogException(ex, null, brand, "SettingsManager");
            }

            return settingValue;
        }

        public bool GetSettingBool(string settingConstant, bool defaultValue)
        {
            bool settingValue = defaultValue;

            try
            {
                settingValue = Conversion.CBool(SettingsService.GetSettingFromSingleton(settingConstant));
            }
            catch (Exception ex)
            {
                LoggingManager.LogException(ex, null, null, "SettingsManager");
                settingValue = defaultValue;
            }

            return settingValue;
        }

        public string GetSettingString(string settingConstant, Brand brand)
        {
            string settingValue = string.Empty;

            try
            {
                settingValue = (brand == null)
                    ? SettingsService.GetSettingFromSingleton(settingConstant)
                    : SettingsService.GetSettingFromSingleton(settingConstant, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                LoggingManager.LogException(ex, null, brand, "SettingsManager");
            }

            return settingValue;
        }

    }
}