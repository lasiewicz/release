﻿using System;
using System.IO;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Managers.Interfaces;

namespace Spark.Managers.Managers
{
    public class PhotoRulesManager : AbstractManagerBaseClass, IPhotoRules
    {
        public const int MAX_FILE_SIZE = 5120000;
        
        public bool IsValidImage(Stream stream, int contentLength)
        {
            bool isValid = true;

            try
            {
//                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

                // Convert to byte array
                Stream fileStream = stream;
                fileStream.Position = 0;
                Byte[] fileBytes = new Byte[contentLength];
                fileStream.Read(fileBytes, 0, contentLength);
                stream.Flush();

                if (!IsAcceptedImageType(fileBytes)) isValid = false;
            }
            catch (Exception)
            {
                isValid = false;
            }

            return isValid;
        }

        public bool IsFileSizeAllowed(int fileSize)
        {
            return (fileSize < MAX_FILE_SIZE);
        }

        public int GetMaxPhotoCount(Brand brand)
        {
            return SettingsManager.GetSettingInt(SettingConstants.MAX_PHOTO_COUNT, brand);
        }

        public bool IsAcceptedImageType(byte[] input)
        {
            try
            {
                if (input == null) return false;
                if (input.Length < 32) return false;
                // JPG
                string signature;
                signature = BitConverter.ToString(input, 0, 4);
                /*
                47 49 46 38 37 61   GIF87a 
                47 49 46 38 39 61   GIF89a 
                */
                if (signature.StartsWith("47-49-46-38")) return true;
                if (signature.StartsWith("47-49-46-38")) return true;
                /*
                 42 4D   BM 
                            BMP, DIB   Windows bitmap image
 
                */
                if (signature.StartsWith("42-4D")) return true;

                /*
			 
                FF D8 FF E0 xx xx 4A 46 49 46 00   ÿ??.JFIF. 
                                                    JFIF, JPE, JPEG, JPG   JPEG/JFIF graphics file
                                                    Trailer: FF D9 (..)
                FF D8 FF E1 xx xx 45 78 69 66 00   ÿ??.Exif. 

                 */
                string subtype;
                subtype = BitConverter.ToString(input, 6, 4);
                // JFIF
                if (signature.StartsWith("FF-D8-FF-E0") && subtype == "4A-46-49-46") return true;
                // EXIF
                if (signature.StartsWith("FF-D8-FF-E1") && subtype == "45-78-69-66") return true;
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool FileNameExtensionAllowed(string filename)
        {
            string phototypes = @"(.*\.[j/J][p/P][e/E][g/G]$)|(.*\.[j/J][p/P][g/G]$)|(.*\.[g/G][i/I][f/F]$)|(.*\.[b/B][m/M][p/P]$)";
            System.Text.RegularExpressions.Regex regValidation = new System.Text.RegularExpressions.Regex(phototypes);
            return regValidation.IsMatch(filename);
        }
    }
}