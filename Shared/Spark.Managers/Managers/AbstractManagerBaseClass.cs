﻿using Spark.Managers.Interfaces;

namespace Spark.Managers.Managers
{
    public abstract class AbstractManagerBaseClass
    {
//        private ICurrentRequest _currentRequest = null;
        private SettingsManager _settingsManager = null;
        private ILoggingManager _loggingManager = null;

		//public ICurrentRequest CurrentRequest
		//{
		//    get { return _currentRequest ?? new CurrentRequest(); }
		//    set { _currentRequest = value; }
		//}

        public ILoggingManager LoggingManager
        {
            get
            {
                if (_loggingManager == null)
                {
                    _loggingManager = new LoggingManager();
                }
                return _loggingManager;
            }
        }

        public SettingsManager SettingsManager
        {
            get
            {
                if(_settingsManager == null)
                {
                    _settingsManager = new SettingsManager();
                }
                return _settingsManager;
            }
        }
    }
}