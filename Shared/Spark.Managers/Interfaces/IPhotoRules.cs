﻿using System.IO;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.Managers.Interfaces
{
    public interface IPhotoRules
    {
        bool IsValidImage(Stream stream, int contentLength);
        bool IsFileSizeAllowed(int fileSize);
        int GetMaxPhotoCount(Brand brand);
        bool IsAcceptedImageType(byte[] input);
        bool FileNameExtensionAllowed(string filename);
    }
}
