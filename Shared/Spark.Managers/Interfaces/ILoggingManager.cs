﻿using System;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Spark.Managers.Interfaces
{
    public interface ILoggingManager
    {
        void LogException(Exception exception, IMember member, Brand brand, string className);
        void LogInfoMessage(string className, string message);
    }
}