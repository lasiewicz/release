﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet;
using Matchnet.Configuration.ValueObjects;
using NUnit.Framework;

namespace Spark.Caching.Test
{
    public class MembaseCachingTest
    {
        private ICaching cacheInstance;
        [TestFixtureSetUp]
        public void StartUp()
        {
            var config = new MembaseConfig("webcache", "", int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, 3600);
            //http://ladevappfab01:8091 - membase server dev
            config.Servers.Add(new MembaseConfigServer("172.16.200.141", 8091));
            cacheInstance = MembaseCaching.GetSingleton(config);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
        }

        [Test]
        public void TestAddCacheableObject()
        {
            var cacheableObj = new CacheableMock(10);
            cacheInstance.Remove(cacheableObj.GetCacheKey());
            var val = cacheInstance.Add(cacheableObj);
            Assert.Null(val);
            //Add item again. If item already exists in cache, it returns the value
            val = cacheInstance.Add(cacheableObj);
            Assert.NotNull(val);
            Assert.AreEqual(val, cacheableObj);
            var actualval = (CacheableMock)cacheInstance.Get(cacheableObj.GetCacheKey());
            Assert.AreEqual(cacheableObj.Value, actualval.Value);
        }

        [Test]
        public void TestAddNullCacheableObject()
        {
            var val = cacheInstance.Add(null);
            Assert.Null(val);
        }

        [Test]
        public void TestSerializableInput()
        {
            var key = "Key_Non_Serialized";
            cacheInstance.Remove(key);
            var obj = new NonSerializedMock();
            cacheInstance.Insert(key,obj);
            var actual = cacheInstance.Get(key);
            Assert.Null(actual);
        }

        [Test]
        public void TestAddKeyValue()
        {
            string key = "Key_3";
            string value = "MembaseUnitTest";
            cacheInstance.Remove(key);
            object cacheItem = cacheInstance.Add(key, value, null, DateTime.Now.AddSeconds(20), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
            Assert.Null(cacheItem);
            //Try adding the item again
            cacheItem = cacheInstance.Add(key, value, null, DateTime.Now.AddSeconds(20), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null); 
            Assert.NotNull(cacheItem);
            cacheItem = cacheInstance.Get(key);
            Assert.AreEqual(value, cacheItem);
        }

        [Test]
        public void TestAddNullValue()
        {
            string key = "Key_4";
            cacheInstance.Remove(key);
            var value = cacheInstance.Add(key,null, null, DateTime.Now.AddSeconds(20), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
            Assert.Null(value);
        }

        [Test]
        public void TestGet()
        {
            string key = "Key_6";
            string value = "MembaseUnitTest";
            cacheInstance.Insert(key, value);
            var actual = cacheInstance.Get(key);
            Assert.AreEqual(value, actual);
        }

        [Test]
        public void TestGetEmptyNullKey()
        {
            Assert.Null(cacheInstance.Get(""));
            Assert.Null(cacheInstance.Get((string)null));
        }

        [Test]
        public void TestInsertCacheableObject()
        {
            var cacheableObj = new CacheableMock(20);
            cacheInstance.Remove(cacheableObj.GetCacheKey());
            cacheInstance.Insert(cacheableObj);
            var actualval = (CacheableMock)cacheInstance.Get(cacheableObj.GetCacheKey());
            Assert.AreEqual(cacheableObj.Value, actualval.Value);
        }

        [Test]
        public void TestInsertNullCacheableObject()
        {
            cacheInstance.Insert(null);
        }

        [Test]
        public void TestInsertAndGet()
        {
            string cacheKey = "Key_1";
            string cacheValue = "MembaseUnitTest";
            cacheInstance.Insert(cacheKey, cacheValue);
            object cacheItem = cacheInstance.Get(cacheKey);
            Assert.NotNull(cacheItem);
            Assert.AreEqual(cacheValue, cacheItem);

            cacheInstance.Insert(cacheKey, cacheValue, null);
            cacheItem = cacheInstance.Get(cacheKey);
            Assert.NotNull(cacheItem);
            Assert.AreEqual(cacheValue, cacheItem);

            cacheInstance.Insert(cacheKey, cacheValue, null, DateTime.Now.AddSeconds(10), System.Web.Caching.Cache.NoSlidingExpiration);
            cacheItem = cacheInstance.Get(cacheKey);
            Assert.NotNull(cacheItem);
            Assert.AreEqual(cacheValue, cacheItem);

            cacheInstance.Insert(cacheKey, cacheValue, null, DateTime.Now.AddSeconds(10), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
            cacheItem = cacheInstance.Get(cacheKey);
            Assert.NotNull(cacheItem);
            Assert.AreEqual(cacheValue, cacheItem);
        }

        [Test]
        public void TestInsertAndGetWithTimeDelayedTtl()
        {
            var cacheKey = "Key_1";
            var cacheValue = "MembaseUnitTest";
            cacheInstance.Remove(cacheKey);
            cacheInstance.Insert(cacheKey, cacheValue, null, DateTime.Now.AddSeconds(30),
                System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.High, null);
            // check right away
            var cacheItem1 = cacheInstance.Get(cacheKey);
            Assert.IsNotNull(cacheItem1, "1st null");
            // check mid way
            Thread.Sleep(new TimeSpan(0, 0, 15));
            var cacheItem2 = cacheInstance.Get(cacheKey);
            Assert.IsNotNull(cacheItem2, "2nd null");
            // check after expiration
            Thread.Sleep(new TimeSpan(0, 0, 16));
            var cacheItem3 = cacheInstance.Get(cacheKey);
            Assert.IsNull(cacheItem3, "3nd is not null");
        }

        [Test]
        public void TestInsertWithNullValue()
        {
            string cacheKey = "Key_2";
            cacheInstance.Insert(cacheKey, null);
            Assert.Null(cacheInstance.Get(cacheKey));
        }

        [Test]
        public void TestRemove()
        {
            string cacheKey = "Key_4";
            string cacheValue = "MembaseUnitTest";
            cacheInstance.Insert(cacheKey, cacheValue);
            cacheInstance.Remove(cacheKey);
            object cacheItem = cacheInstance.Get(cacheKey);
            Assert.Null(cacheItem);
        }
    }

    [Serializable]
    public class CacheableMock : ICacheable
    {
        private string val = "MembaseUnitTest";
        private int index = 0;
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;

        public string Value { get { return val; } }

        public CacheableMock(int index)
        {
            this.index = index;
        }

        public string GetCacheKey()
        {
            return "Key_Test_" + index;
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }
    }

    [Serializable]
    public class NonSerializedMock
    {
        private string val = "MembaseUnitTest";
        public string Value { get { return val; } set { val = value; } }

        private NestedNonSerializedMock obj = new NestedNonSerializedMock();
        public NestedNonSerializedMock MockObj { get { return obj; } set { obj = value; } }

        private Dictionary<string,List<string>>  items = new Dictionary<string, List<string>>();
        public Dictionary<string,List<string>> Items {get {return items;}}

        private List<Hashtable> hashtableItems = new List<Hashtable>();
        public List<Hashtable> HashtableItems
        {
            get { return hashtableItems; }
        }
    }
    

    
    public class NestedNonSerializedMock
    {
        private string val = "MembaseUnitTest";
        public string Value { get { return val; } set { val = value; } }
    }
}
