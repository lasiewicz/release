﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;
using Couchbase;
using Couchbase.Configuration;
using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;
using Matchnet;
using Matchnet.Configuration.ValueObjects;

namespace Spark.Caching
{   
    public class MembaseCachingImpl : ICaching
    {
        private int _timeToLiveInSeconds = 60 * 60;
        private IMemcachedClient _cacheClient = null;

        public MembaseCachingImpl(MembaseConfig config)
        {
            InitMembaseClient(config, false);
        }

        public MembaseCachingImpl(MembaseConfig config, bool isMemcachedBucket)
        {
            InitMembaseClient(config, isMemcachedBucket);
        }

        private void InitMembaseClient(MembaseConfig membaseConfig, bool isMemcachedBucket)
        {
            if (isMemcachedBucket)
            {
                var config = new MemcachedClientConfiguration
                {
                    Protocol = MemcachedProtocol.Binary
                };
                if (membaseConfig.SocketPoolMaxPoolSize > int.MinValue)
                    config.SocketPool.MaxPoolSize = membaseConfig.SocketPoolMaxPoolSize;
                if (membaseConfig.SocketPoolMinPoolSize > int.MinValue)
                    config.SocketPool.MinPoolSize = membaseConfig.SocketPoolMinPoolSize;
                if (membaseConfig.SocketPoolConnectionTimeout > int.MinValue)
                    config.SocketPool.ConnectionTimeout = new TimeSpan(0, 0, membaseConfig.SocketPoolConnectionTimeout);
                if (membaseConfig.SocketPoolDeadTimeout > int.MinValue)
                    config.SocketPool.DeadTimeout = new TimeSpan(0, 0, membaseConfig.SocketPoolDeadTimeout);
                if (membaseConfig.SocketPoolQueueTimeout > int.MinValue)
                    config.SocketPool.QueueTimeout = new TimeSpan(0, 0, membaseConfig.SocketPoolQueueTimeout);
                if (membaseConfig.SocketPoolReceiveTimeout > int.MinValue)
                    config.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, membaseConfig.SocketPoolReceiveTimeout);
                foreach (var server in membaseConfig.Servers)
                {
                    config.AddServer(server.ServerIP, server.ServerPort);
                }
                _timeToLiveInSeconds = membaseConfig.ObjectTimeToLiveInSeconds;
                _cacheClient = new MemcachedClient(config);

            }
            else
            {
                var config = new CouchbaseClientConfiguration
                {
                    Bucket = membaseConfig.BucketName,
                    BucketPassword =
                        (string.IsNullOrEmpty(membaseConfig.BucketPassword)
                             ? ""
                             : membaseConfig.BucketPassword),
                };
                if (membaseConfig.RetryCount > int.MinValue)
                    config.RetryCount = membaseConfig.RetryCount;
                if (membaseConfig.RetryTimeout > int.MinValue)
                    config.RetryTimeout = new TimeSpan(0, 0, membaseConfig.RetryTimeout);
                if (membaseConfig.SocketPoolMaxPoolSize > int.MinValue)
                    config.SocketPool.MaxPoolSize = membaseConfig.SocketPoolMaxPoolSize;
                if (membaseConfig.SocketPoolMinPoolSize > int.MinValue)
                    config.SocketPool.MinPoolSize = membaseConfig.SocketPoolMinPoolSize;
                if (membaseConfig.SocketPoolConnectionTimeout > int.MinValue)
                    config.SocketPool.ConnectionTimeout = new TimeSpan(0, 0, membaseConfig.SocketPoolConnectionTimeout);
                if (membaseConfig.SocketPoolDeadTimeout > int.MinValue)
                    config.SocketPool.DeadTimeout = new TimeSpan(0, 0, membaseConfig.SocketPoolDeadTimeout);
                if (membaseConfig.SocketPoolQueueTimeout > int.MinValue)
                    config.SocketPool.QueueTimeout = new TimeSpan(0, 0, membaseConfig.SocketPoolQueueTimeout);
                if (membaseConfig.SocketPoolReceiveTimeout > int.MinValue)
                    config.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, membaseConfig.SocketPoolReceiveTimeout);
                foreach (var server in membaseConfig.Servers)
                {
                    config.Urls.Add(
                        new Uri(string.Format("http://{0}:{1}/pools/default", server.ServerIP, server.ServerPort)));
                }
                _timeToLiveInSeconds = membaseConfig.ObjectTimeToLiveInSeconds;
                _cacheClient = new CouchbaseClient(config);
            }
        }
        #region ICaching Members

        public object this[string key]
        {
            get { return Get(key); }
            set
            {
                Insert(key, value);
            }
        }

        public ICacheable Add(Matchnet.ICacheable cacheableObject)
        {
            if (cacheableObject == null)
                return null;

            if (_cacheClient.Store(StoreMode.Add,
                cacheableObject.GetCacheKey(), cacheableObject, DateTime.Now.AddSeconds(Math.Abs(cacheableObject.CacheTTLSeconds))))
                return null;
            return cacheableObject;
        }


        public ICacheable Add(ICacheable cacheableObject, CacheItemPriorityLevel overrideItemPriority)
        {
            return Add(cacheableObject);
        }

        public ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency)
        {
            return Add(cacheableObject);
        }

        public ICacheable Add(ICacheable cacheableObject, CacheItemRemovedCallback onRemoveCallback)
        {
            return Add(cacheableObject);
        }

        public ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency,
                       CacheItemRemovedCallback onRemoveCallback)
        {
            return Add(cacheableObject);
        }

        public ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency,
                       CacheItemPriorityLevel overrideItemPriority, CacheItemRemovedCallback onRemoveCallback)
        {
            return Add(cacheableObject);
        }

        public object Add(string key,
                   object value,
                   System.Web.Caching.CacheDependency dependencies,
                   DateTime absoluteExpiration,
                   TimeSpan slidingExpiration,
                   System.Web.Caching.CacheItemPriority priority,
                   System.Web.Caching.CacheItemRemovedCallback onRemoveCallback)
        {
            if (value == null)
                return null;
            if (_cacheClient.Store(StoreMode.Add,
                                 key, value, absoluteExpiration))
                return null;
            return value;
        }

        public void Clear()
        {
            _cacheClient.FlushAll();
        }

        public void Insert(ICacheable cacheableObject)
        {
            if (cacheableObject == null) return;

            Insert(cacheableObject.GetCacheKey(), cacheableObject, null,
                DateTime.Now.AddSeconds(Math.Abs(cacheableObject.CacheTTLSeconds)), TimeSpan.MinValue,
                CacheItemPriority.Default, null);
        }

        public void Insert(string key, object value)
        {
            _cacheClient.Store(StoreMode.Set, key, value, DateTime.Now.AddSeconds(_timeToLiveInSeconds));
        }

        public object Get(string key)
        {
            if (string.IsNullOrEmpty(key))
                return null;

            return _cacheClient.Get(key);
        }

        public IDictionary<string, object> Get(List<string> keys)
        {
            IDictionary<string, object> objects = new Dictionary<string, object>();
            if (keys != null && keys.Count > 0)
            {
                objects = _cacheClient.Get(keys);
            }
            return objects;
        }

        public IEnumerator GetEnumerator()
        {
            return null;
        }

        public void Insert(ICacheable cacheableObject, CacheItemPriorityLevel overrideItemPriority)
        {
            Insert(cacheableObject);
        }

        public void Insert(ICacheable cacheableObject, CacheDependency cacheDependency)
        {
            Insert(cacheableObject);
        }

        public void Insert(ICacheable cacheableObject, CacheItemRemovedCallback onRemoveCallback)
        {
            Insert(cacheableObject);
        }

        public void Insert(ICacheable cacheableObject, CacheDependency cacheDependency,
                    CacheItemRemovedCallback onRemoveCallback)
        {
            Insert(cacheableObject);
        }

        public void Insert(ICacheable cacheableObject, CacheDependency cacheDependency,
                    CacheItemPriorityLevel overrideItemPriority, CacheItemRemovedCallback onRemoveCallback)
        {
            Insert(cacheableObject);
        }

        public void Insert(string key,
                    object value,
                    CacheDependency dependencies)
        {
            Insert(key, value);
        }

        public void Insert(string key,
            object value,
            CacheDependency dependencies,
            DateTime absoluteExpiration,
            TimeSpan slidingExpiration)
        {
            Insert(key, value, null, absoluteExpiration, TimeSpan.MinValue, CacheItemPriority.Default, null);
        }

        public void Insert(string key,
            object value,
            CacheDependency dependencies,
            DateTime absoluteExpiration,
            TimeSpan slidingExpiration,
            CacheItemPriority priority,
            CacheItemRemovedCallback onRemoveCallback)
        {
            // passing in DateTime does NOT work, could be an issue with the client, do no remove the commented old code
            //_cacheClient.Store(StoreMode.Set, key, value, absoluteExpiration);

            var timeSpan = absoluteExpiration - DateTime.Now;
            _cacheClient.Store(StoreMode.Set, key, value, timeSpan);
        }

        public object Remove(string key)
        {
            object val = _cacheClient.Get(key);
            _cacheClient.Remove(key);
            return val;
        }

        public bool RemoveWithoutGet(string key)
        {
            return _cacheClient.Remove(key);
        }
        #endregion

    }

}
