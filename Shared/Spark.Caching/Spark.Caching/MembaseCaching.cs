﻿using Matchnet;
using Matchnet.Configuration.ValueObjects;
using System.Collections.Generic;

namespace Spark.Caching
{
    /// <summary>
    /// This class uses Membase caching 
    /// </summary>
    public class MembaseCaching
    {
        private static string _Lock = "";
        private static Dictionary<string, MembaseCachingImpl> _Singletons = new Dictionary<string, MembaseCachingImpl>();

        private MembaseCaching(){}

        #region static methods

        /// <summary>
        /// Returns instance of ICaching (NOT SINGLETON)
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public static ICaching GetInstance(MembaseConfig config)
        {
            return new MembaseCachingImpl(config);
        }

        /// <summary>
        /// Returns instance of ICaching (NOT SINGLETON)
        /// </summary>
        /// <param name="config"></param>
        /// <param name="isMemcachedBucket"></param>
        /// <returns></returns>
        public static ICaching GetInstance(MembaseConfig config, bool isMemcachedBucket)
        {
            return new MembaseCachingImpl(config, isMemcachedBucket);
        }

        /// <summary>
        /// Returns singleton instance of ICaching based on bucketname in the config param
        /// </summary>
        /// <returns></returns>
        public static ICaching GetSingleton(MembaseConfig config)
        {
            return GetSingleton(config, false);
        }

        /// <summary>
        /// Returns singleton instance of ICaching based on bucketname in the config param
        /// </summary>
        /// <param name="config"></param>
        /// <param name="isMemcachedBucket"></param>
        /// <returns></returns>
        public static ICaching GetSingleton(MembaseConfig config, bool isMemcachedBucket)
        {
            string bucketName = config.BucketName.ToLower() + "_" + isMemcachedBucket.ToString();
            if (!_Singletons.ContainsKey(bucketName))
            {
                lock (_Lock)
                {
                    if (!_Singletons.ContainsKey(bucketName))
                    {
                        _Singletons[bucketName] = new MembaseCachingImpl(config, isMemcachedBucket);
                    }
                }
            }

            return _Singletons[bucketName];
        }

        #endregion
    }
}
