﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Spark.CacheBusterClient
{
    [Serializable]
    public class Host
    {
        [XmlAttribute("URL")]
        public string URL { get; set; }

        [XmlAttribute("Name")]
        public string Name { get; set; }
    }
}
