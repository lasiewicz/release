﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ServiceDefinitions;

namespace Spark.CacheBusterClient
{
    public class ServiceHelper
    {
        internal static IMemberService getMemberService(string uri)
        {
           return (IMemberService)Activator.GetObject(typeof(IMemberService), uri);
        }

        public bool ClearMemberCache(string uri)
        {
            bool cleared = true;

            try
            {
                var service = getMemberService(uri);
                service.ExpireEntireMemberCache();
            }
            catch (Exception)
            {
                cleared = false;
            }

            return cleared;
        }
    }
}
