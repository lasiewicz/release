﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Spark.CacheBusterClient
{
    public class ConfigReader
    {
        private string _configFilePath = string.Empty;

        public ConfigReader(string configFilePath)
        {
            _configFilePath = fixConfigFilePath(configFilePath);
        }
        
        private string fixConfigFilePath(string configFilePath)
        {
            if(configFilePath.LastIndexOf('\\') != configFilePath.Length -1)
            {
                configFilePath = configFilePath + "\\";
            }

            return configFilePath;
        }

        public WebBasedCacheList GetWebBasedCacheList()
        {
            WebBasedCacheList cachelist = null;

            var filePath = _configFilePath + "WebBasedCaches.xml";

            TextReader textReader = null;
            try
            {
                textReader = new StreamReader(filePath);
                var deserializer = new XmlSerializer(typeof(WebBasedCacheList));
                cachelist = deserializer.Deserialize(textReader) as WebBasedCacheList;
            }
            catch(Exception ex)
            {
                //it's ok to supress this error for now
            }
            finally
            {
                if (textReader != null)
                {
                    textReader.Close();
                }
            }

            return cachelist;
        }

        public ServiceCacheList GetServiceCacheList()
        {
            ServiceCacheList cachelist = null;

            var filePath = _configFilePath + "WebBasedCaches.xml";

            TextReader textReader = null;
            try
            {
                textReader = new StreamReader(filePath);
                var deserializer = new XmlSerializer(typeof(ServiceCacheList));
                cachelist = deserializer.Deserialize(textReader) as ServiceCacheList;
            }
            catch
            {
                //it's ok to supress this error for now
            }
            finally
            {
                if (textReader != null)
                {
                    textReader.Close();
                }
            }

            return cachelist;
        }
    }
}
