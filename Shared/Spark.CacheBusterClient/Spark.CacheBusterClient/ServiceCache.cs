﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Spark.CacheBusterClient
{
    public enum ServiceCacheType
    {
        Member = 0
    }

    [Serializable]
    public class ServiceCache
    {
        [XmlElement("Environment")]
        public List<Environment> Environments { get; set; }

        [XmlAttribute("Type")]
        public ServiceCacheType Type { get; set; }
    }
}
