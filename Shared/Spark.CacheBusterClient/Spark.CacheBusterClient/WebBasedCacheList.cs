﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Spark.CacheBusterClient
{
    [Serializable]
    public class WebBasedCacheList
    {
        [XmlElement("WebBasedCache")]
        public List<WebBasedCache> WebBasedCaches { get; set; }
    }
}
