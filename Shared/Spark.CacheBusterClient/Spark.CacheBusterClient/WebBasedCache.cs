﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Spark.CacheBusterClient
{
    public enum CacheType 
    {
        [XmlEnum("RegSiteScenarioMetadataJDate")]
        RegSiteScenarioMetadataJDate = 0,
        [XmlEnum("RegSiteScenarioMetadataSpark")]
        RegSiteScenarioMetadataSpark = 1,
        [XmlEnum("RegSiteScenarioMetadataBBW")]
        RegSiteScenarioMetadataBBW = 2,
        [XmlEnum("RegSiteScenarioMetadataBlack")]
        RegSiteScenarioMetadataBlack = 3,
        [XmlEnum("RegSiteScenarioMetadataJDateUK")]
        RegSiteScenarioMetadataJDateUK = 4,
        [XmlEnum("RegSiteScenarioMetadataJDateIL")]
        RegSiteScenarioMetadataJDateIL = 5,
        [XmlEnum("RegSiteScenarioMetadataJDateFR")]
        RegSiteScenarioMetadataJDateFR = 6,
        [XmlEnum("RegSiteScenarioMetadataCupid")]
        RegSiteScenarioMetadataCupid = 7,
        [XmlEnum("APIScenarioMetadata")]
        APIScenarioMetadata=8,
        [XmlEnum("RegSiteScenarioMetadataAdventistSingles")]
        RegSiteScenarioMetadataAdventistSingles = 9,
        [XmlEnum("RegSiteScenarioMetadataCatholicMingle")]
        RegSiteScenarioMetadataCatholicMingle = 10,
        [XmlEnum("RegSiteScenarioMetadataChristianMingle")]
        RegSiteScenarioMetadataChristianMingle = 11,
        [XmlEnum("RegSiteScenarioMetadataDeafSingles")]
        RegSiteScenarioMetadataDeafSingles = 12,
        [XmlEnum("RegSiteScenarioMetadataLDSMingle")]
        RegSiteScenarioMetadataLDSMingle = 13,
        [XmlEnum("RegSiteScenarioMetadataMilitarySingles")]
        RegSiteScenarioMetadataMilitarySingles = 14,
        [XmlEnum("RegSiteScenarioMetadataSilverSingles")]
        RegSiteScenarioMetadataSilverSingles = 16,
        [XmlEnum("RegSiteScenarioMetadataLDSSingles")]
        RegSiteScenarioMetadataLDSSingles = 17,

    }
    
    [Serializable]
    public class WebBasedCache
    {
        [XmlElement("Environment")]
        public List<Environment> Environments { get; set; }

        //[XmlIgnore]
        [XmlAttribute("Type")]
        public CacheType Type { get; set; }
    }
}
