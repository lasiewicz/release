using System;
using System.Diagnostics;

using Matchnet.Exceptions;

namespace Matchnet.EmailLibrary
{
	public class AllowedDomains
	{
		private string[] _allowedDomains = null;
		private string _serviceName;

		public AllowedDomains(string serviceName)
		{
			_allowedDomains = getAllowedDomains();
			_serviceName = serviceName;

			EventLog.WriteEntry(_serviceName, "AllowedDomains:" + String.Join(",", _allowedDomains));
		}

		private string[] getAllowedDomains()
		{
			string[] allowedDomainsSplit = null;
			string allowedDomains = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_ALLOWED_DOMAINS").Trim().ToLower();

			if (allowedDomains.Length > 0)
			{
				allowedDomainsSplit = allowedDomains.Split(',');
			}
			else
			{
				allowedDomainsSplit = new string[0];
			}

			return allowedDomainsSplit;
		}

		public bool IsAllowedDomain(string emailAddress)
		{

			try
			{
				bool domainAllowed = true;
				if (emailAddress == null || emailAddress == String.Empty)
				{
					throw new BLException("Email address cannot be null or empty");
				}


				if (_allowedDomains.Length == 0)
				{
					domainAllowed = true;
				}
				else
				{
					foreach (string domain in _allowedDomains)
					{
						if (emailAddress.ToLower().EndsWith(domain))
						{
							domainAllowed = true;

							break;
						}
						else
						{
							domainAllowed = false;
						}
					}
				}

				if (!domainAllowed)
				{
					EventLog.WriteEntry(_serviceName, "Sending to this email address's domain is not allowed. [Email Address:" + emailAddress + "]", EventLogEntryType.Warning);
				}

				return domainAllowed;
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry(_serviceName, ex.ToString(), EventLogEntryType.Warning);
				return false;
			}
		}
	}
}
