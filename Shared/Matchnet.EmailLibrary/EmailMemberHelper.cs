using System;

using System.Diagnostics;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Spark.CloudStorage;
using Spark.Managers.Managers;

namespace Matchnet.EmailLibrary
{
	/// <summary>
	/// Summary description for MemberHelper.
	/// </summary>
	public class EmailMemberHelper
	{
		private const Int32 MAX_REGION_STRING_LENGTH = 40;
        private const string SPARK_WS_CRYPT_KEY = "A877C90D";

		public static int DetermineMemberAge(DateTime birthDate)
		{
			int age = Conversion.Age(birthDate);

			//	Age can't be more than 999 for the templates and sometimes bad data gives
			//	us a year as a return value, so for our purposes here we will just knock
			//	the value down to 999

			if (age >= 1000)
			{
				age = 999;
			}

			return (age);
		}

		public static int DetermineMemberAge(IMemberDTO member, Brand brand)
		{
			DateTime birthDate = member.GetAttributeDate(brand, "Birthdate");

			return DetermineMemberAge(birthDate);
		}

		public static string DetermineMemberRegionDisplay(Int32 regionID, Brand brand)
		{
			string regionString = "";
			RegionLanguage regionLanguage;

			if (regionID > 0)
			{
				regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, brand.Site.LanguageID);

				// Incrementally build up the regionString with commas where necessary
				// based on the contents being empty or not before and after each comma
				// This made for CI/CP and tested on CL/GL 
				// start WEL
				regionString = regionLanguage.CityName;

				if (regionString != String.Empty && regionLanguage.StateDescription != String.Empty)
				{
					regionString += ", ";
				}

				regionString += regionLanguage.StateDescription;

				if (regionString != String.Empty && regionLanguage.CountryAbbreviation != String.Empty)
				{
					regionString += ", ";
				}

				regionString += regionLanguage.CountryAbbreviation;

				// end WEL

				if (regionString.Length > MAX_REGION_STRING_LENGTH)
				{
					regionString = regionString.Substring(0, MAX_REGION_STRING_LENGTH - 3) + "...";
				}
			}
			else
			{
				regionString = "N/A";
			}
			return regionString;
		}

		public static string DetermineMemberRegionDisplay(IMemberDTO pMember, Brand brand)
		{
			int regionAttribute;
			regionAttribute = pMember.GetAttributeInt(brand, "regionid");
			return DetermineMemberRegionDisplay(regionAttribute, brand);
		}

		public static string DetermineMemberThumbnail(IMemberDTO member, Brand brand)
		{
			string retVal = EmailMemberHelper.DetermineBaseNoPhoto(brand);
              bool showPhotosFromCloud =
                  
                new SettingsManager().GetSettingBool(SettingConstants.ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD, brand);

		    var noDefaultImage = false;
			if (member.GetPhotos(brand.Site.Community.CommunityID).Count > 0)
			{
				Photo photo = member.GetPhotos(brand.Site.Community.CommunityID)[0];

				if (photo.IsApproved)
				{
					if (photo.IsPrivate)
					{
						return EmailMemberHelper.DetermineBasePrivatePhoto(brand);
					}
					else
					{
					    noDefaultImage = true;
						retVal = photo.FileCloudPath;

						if (retVal.ToLower().StartsWith("/photo"))
						{
							// got relative URI, "/Photo300/foo/bar/goo.jpg" make it a fully qualified based on brand.
                            retVal = showPhotosFromCloud ? photo.FileCloudPath : photo.FileWebPath;
						}

					}
				}
			}
            
            if(!noDefaultImage) 
                return retVal;

            Client cloudClient = new Client(brand.Site.Community.CommunityID, brand.Site.SiteID, new SettingsManager().SettingsService);
            retVal  = cloudClient.GetFullCloudImagePath(retVal, FileType.MemberPhoto, false);

			return (retVal);
		}

		public static string DetermineBaseNoPhoto(Brand brand)
		{
			// if the site is JDIL (site id of 4) use Site folder for the image (Because JDIL is within JD community)
			if (brand.Site.SiteID == 4)
			{
				return "http://" + brand.Site.DefaultHost + "." + brand.Uri + @"/img/Site/" + brand.Uri.Replace(".", "-") + "/nophoto.jpg";
			}
			else
			{
				return "http://" + brand.Site.DefaultHost + "." + brand.Uri + @"/img/Community/" + brand.Site.Community.Name + "/nophoto.jpg";
			}
		}

		public static string DetermineBasePrivatePhoto(Brand brand)
		{
			return "http://" + brand.Site.DefaultHost + "." + brand.Uri + @"/img/Community/" + brand.Site.Community.Name + "/privatephoto80x104_ynm4.jpg";
		}

		public static string GetDateDisplay(DateTime pDate, Brand pBrand)
		{
			// GMT offset for pacific server time is -8 during standard time.
			// We calculate the local time based on the server time, which means we can be off by 1 hour during summer time.
			pDate = pDate.AddHours(8 + pBrand.Site.GMTOffset);

			if (pBrand.Site.CultureInfo.Name.ToLower() == "he-il" ||
				pBrand.Site.CultureInfo.Name.ToLower() == "fr-FR" ||
				pBrand.Site.SiteID == 107)
			{
				return pDate.ToString("dd/MM/yyyy hh:mm tt");
			}

			return pDate.ToString("MM/dd/yyyy hh:mm tt");
		}
		public static string DetermineMemberAboutMe(Brand brand, IMemberDTO member, IMemberDTO viewingMember)
		{
			string reason = String.Empty;

			/*
			string reason = (brand.Site.LanguageID == (int)Language.Hebrew)? 
				"הטקסט החופשי ממתין לאישור על-ידי צוות שירות הלקוחות." : 
				"My new essay is being approved by Customer Care. Check back soon to find out more about me.";
			*/

			if (brand.Site.LanguageID == (int)Language.Hebrew)
			{
				reason = "הטקסט החופשי ממתין לאישור על-ידי צוות שירות הלקוחות.";
			}
			else if (brand.Site.LanguageID == (int)Language.French)
			{
				reason = "Mon nouvel essai est en cours de validation par l’équipe de Jdate.fr. Mais, n’hésitez pas a revenir voir mon profil pour en savoir plus !";
			}
			else
			{
				reason = "My new essay is being approved by Customer Care. Check back soon to find out more about me.";
			}

			string retVal = member.GetAttributeTextApproved(brand, "AboutMe", viewingMember.MemberID, reason);

			if (retVal != null && retVal.Length > 30)
			{
				retVal = retVal.Substring(0, 27) + "...";
			}

			return (retVal);
		}
		public static string CreateEcardUrl(IMemberDTO member, Brand brand, string cardUrl)
		{
			// Don't mess with an http.
			if (!cardUrl.ToLower().StartsWith("http"))
			{
				string devSubdomain = String.Empty;
				if (brand.Site.DefaultHost == "dev")
					devSubdomain = "dev.";

				cardUrl = String.Format("http://connect.{0}{1}/{2}", devSubdomain, brand.Uri, cardUrl);
			}
			return cardUrl;
			/*	20070731	Modified by: RB
						// HACK FOR TT 19893.  This should be temporary for less than a month (08/30/2006).
						if (cardUrl.IndexOf("jdate.co.il") == -1)
						{
							return cardUrl;
						}
						else
						{
							EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "MemberID " + member.MemberID.ToString()
								+ " will not be sent an Ecard ExternalMail to jdate.co.il (cardUrl: " + cardUrl
								+ ").", EventLogEntryType.Warning);
							return Constants.NULL_STRING;
						}
			*/
		}

		public static string DetermineMemberSeeking(Brand brand, IMemberDTO member)
		{
			int genderMask = member.GetAttributeInt(brand, "GenderMask");

			string gender = GetGenderString(brand.BrandID, genderMask);
			string seeking = GetSeekingGenderString(brand.BrandID, genderMask);

			return gender + " " + SeekingText(brand.BrandID) + " " + seeking;
		}

        public static string DetermineMemberGender(Brand brand, IMemberDTO member)
        {
            int genderMask = member.GetAttributeInt(brand, "GenderMask");
            return GetGenderString(brand.BrandID, genderMask);
        }

		public static string GetGenderString(int brandID, int genderMask)
		{
			if ((genderMask & (Int32)GenderMask.Male) == (Int32)GenderMask.Male)
			{
				return MaleText(brandID);
			}

			if ((genderMask & (Int32)GenderMask.Female) == (Int32)GenderMask.Female)
			{
				return FemaleText(brandID);
			}

			if ((genderMask & (Int32)GenderMask.MTF) == (Int32)GenderMask.MTF)
			{
				return MaleToFemaleText(brandID);
			}

			if ((genderMask & (Int32)GenderMask.FTM) == (Int32)GenderMask.FTM)
			{
				return FemaleToMaleText(brandID);
			}

			// Else
			return "";
		}
		public static string GetSeekingGenderString(int brandID, int genderMask)
		{
			string seeking = "";

			if ((genderMask & (Int32)GenderMask.SeekingFemale) == (Int32)GenderMask.SeekingFemale)
			{
				seeking = FemaleText(brandID);
			}

			if ((genderMask & (Int32)GenderMask.SeekingMale) == (Int32)GenderMask.SeekingMale)
			{
				if (seeking.Length > 0)
				{
					seeking = seeking + ", ";
				}

				seeking = seeking + MaleText(brandID);
			}

			if ((genderMask & (Int32)GenderMask.SeekingMTF) == (Int32)GenderMask.SeekingMTF)
			{
				if (seeking.Length > 0)
					seeking = seeking + ", ";

				seeking = seeking + MaleToFemaleText(brandID);
			}

			if ((genderMask & (Int32)GenderMask.SeekingFTM) == (Int32)GenderMask.SeekingFTM)
			{
				if (seeking.Length > 0)
					seeking = seeking + ", ";

				seeking = seeking + FemaleToMaleText(brandID);
			}

			return seeking;
		}
		public static Language DetermineLanguageFromBrandID(int brandID)
		{
			Language retVal = Language.English;

			Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
			if (brand == null)
				return retVal;
			retVal = (Language)Enum.Parse(typeof(Language), brand.Site.LanguageID.ToString());


			return (retVal);
		}

        public static string DetermineUIDForPixel(int pMessageListId, int pMemberId, int pCommunityId)
        {
            string joined = string.Join("_", new string[] {
                pMessageListId.ToString(),
                pMemberId.ToString(),
                pCommunityId.ToString()
            });

            return Matchnet.Security.Crypto.Encrypt(SPARK_WS_CRYPT_KEY, joined);
        }

		#region PRIVATE HELPER METHOD

		private static string FemaleToMaleText(int brandID)
		{
			string retVal = "[female turning male]";

			switch (DetermineLanguageFromBrandID(brandID))
			{
				case (Language.English):
					retVal = "Female Turning Male";
					break;
				case (Language.French):
					retVal = "[french female turning male]";
					break;
				case (Language.German):
					retVal = "[german female turning male]";
					break;
				case (Language.Hebrew):
					retVal = "[hebrew female turning male]";
					break;
			}

			return (retVal);
		}
		private static string SeekingText(int brandID)
		{
			string retVal = " seeking ";

			switch (DetermineLanguageFromBrandID(brandID))
			{
				case (Language.English):
					retVal = " seeking ";
					break;
				case (Language.French):
					retVal = " Recherche un/une ";
					break;
				case (Language.German):
					retVal = " [german seeking] ";
					break;
				case (Language.Hebrew):
					retVal = " המחפש/ת ";
					break;
			}

			return (retVal);
		}
		private static string MaleToFemaleText(int brandID)
		{
			string retVal = "[male turning female]";

			switch (DetermineLanguageFromBrandID(brandID))
			{
				case (Language.English):
					retVal = "Male Turning Female";
					break;
				case (Language.French):
					retVal = "[french male turning female]";
					break;
				case (Language.German):
					retVal = "[german male turning female]";
					break;
				case (Language.Hebrew):
					retVal = "[hebrew male turning female]";
					break;
			}

			return (retVal);
		}
		private static string MaleText(int brandID)
		{
			string retVal = "Man";

			switch (DetermineLanguageFromBrandID(brandID))
			{
				case (Language.English):
					retVal = "Man";
					break;
				case (Language.French):
					retVal = "Homme";
					break;
				case (Language.German):
					retVal = "[german male]";
					break;
				case (Language.Hebrew):
					retVal = "גבר";
					break;
			}

			return (retVal);
		}
		private static string FemaleText(int brandID)
		{
			string retVal = "Woman";

			switch (DetermineLanguageFromBrandID(brandID))
			{
				case (Language.English):
					retVal = "Woman";
					break;
				case (Language.French):
					retVal = "Femme";
					break;
				case (Language.German):
					retVal = "[german female]";
					break;
				case (Language.Hebrew):
					retVal = "אשה";
					break;
			}

			return (retVal);
		}

		#endregion
	}
}
