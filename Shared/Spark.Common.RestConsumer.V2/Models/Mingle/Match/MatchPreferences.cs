﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Match
{
    public class MatchPreferences
    {
        public int memberId { get; set; }
        public string gender { get; set; }
        public string seekingGender { get; set; }
        public int minAge { get; set; }
        public int maxAge { get; set; }
        public int maxDistance { get; set; }
        public int minHeight { get; set; }
        public int maxHeight { get; set; }
        public string[] bodyType { get; set; }
        public string[] education { get; set; }
        public string[] maritalStatus { get; set; }
        public string[] religion { get; set; }
        public string[] churchActivity { get; set; }
        public string[] smoke { get; set; }
        public string[] drink { get; set; }
        public string[] ethnicity { get; set; }
        public int bodyTypeImportance { get; set; }
        public int ethnicityImportance { get; set; }
        public int educationImportance { get; set; }
        public int maritalStatusImportance { get; set; }
        public int religionImportance { get; set; }
        public int churchActivityImportance { get; set; }
        public int smokeImportance { get; set; }
        public int drinkImportance { get; set; }
    }
}
