﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Search
{
	[DataContract(Name = "SearchPreferences", Namespace = "")]
	public class SearchPreferences
	{
        //[DataMember(Name = "brandUri")]
        //public string BrandUri { get; set; }

		[DataMember(Name = "memberId")]
		public int MemberId { get; set; }

		[DataMember(Name = "gender")]
		public string Gender { get; set; }

		[DataMember(Name = "seekingGender")]
		public string SeekingGender { get; set; }

		[DataMember(Name = "minAge")]
		[Range(18, 99)]
		public int MinAge { get; set; }

		[DataMember(Name = "maxAge")]
		[Range(18, 99)]
		public int MaxAge { get; set; }

		[DataMember(Name = "location")]
		public string Location { get; set; }

		[DataMember(Name = "maxDistance")]
		public int MaxDistance { get; set; }

		private bool _showOnlyJewishMembers = true;
		public virtual bool ShowOnlyJewishMembers
		{
			get { return _showOnlyJewishMembers; }
			set { _showOnlyJewishMembers = value; }
		}

		[DataMember(Name = "showOnlyMembersWithPhotos")]
		public bool ShowOnlyMembersWithPhotos { get; set; }

		public bool IsMale
		{
			get
			{
				return this.Gender.ToLower() == "male";
			}
		}
		public bool IsFemale
		{
			get
			{
				return this.Gender.ToLower() == "female";
			}
		}
		public bool SeekingMale
		{
			get
			{
				return this.SeekingGender.ToLower() == "male";
			}
		}
		public bool SeekingFemale
		{
			get
			{
				return this.SeekingGender.ToLower() == "female";
			}
		}
	}
}
