﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Content.AttributeData
{
    [DataContract(Name = "AttributeOption", Namespace = "")]
    public class AttributeOption
    {
        [DataMember(Name = "AttributeOptionId")]
        public int AttributeOptionId { get; set; }

        [DataMember(Name = "Description")]
        public string Description { get; set; }

        [DataMember(Name = "ListOrder")]
        public int ListOrder { get; set; }

        [DataMember(Name = "Value")]
        public int? Value { get; set; }
    }
}
