﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Content
{
    [DataContract(Name = "SettingValue", Namespace = "")]
    public class SettingValue
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}
