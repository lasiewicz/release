﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.GAM
{
    //class AdTargeting
    //{
    //}


    [DataContract(Name = "AdTargeting", Namespace = "")]
    public class AdTargeting
    {

        [DataMember(Name = "sub_status")]
        public string sub_status { get; set; }
        [DataMember(Name = "since_reg")]
        public string[] since_reg { get; set; }
        [DataMember(Name = "til_renew")]
        public string[] til_renew { get; set; }
        [DataMember(Name = "religion")]
        public string religion { get; set; }
        [DataMember(Name = "ethnicity")]
        public string[] ethnicity { get; set; }
        [DataMember(Name = "reg_device")]
        public string reg_device { get; set; }
        [DataMember(Name = "features")]
        public string[] features { get; set; }
        [DataMember(Name = "education")]
        public string education { get; set; }

    }
}



