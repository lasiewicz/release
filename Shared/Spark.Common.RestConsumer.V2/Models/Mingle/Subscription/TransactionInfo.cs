﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Subscription
{
	[DataContract(Name = "TransactionInfo", Namespace = "")]
	public class TransactionInfo
	{
		[DataMember(Name = "username")]
		public string Username { get; set; }

		[DataMember(Name = "confirmationNumber")]
		public string ConfirmationNumber { get; set; }

		[DataMember(Name = "creditCardType")]
		public string CreditCardType { get; set; }

		[DataMember(Name = "maskedCreditCardNumber")]
		public string MaskedCreditCardNumber { get; set; }

		[DataMember(Name = "transactionDate")]
		public DateTime TransactionDate { get; set; }

        [DataMember(Name = "subscriptionStatus")]
        public string SubscriptionStatus { get; set; }

        [DataMember(Name = "subscriptionStatusDetailed")]
        public string SubscriptionStatusDetailed { get; set; }

        [DataMember(Name = "planDescription")]
        public string PlanDescription { get; set; }

        [DataMember(Name = "promoId")]
        public int PromoId { get; set; }
	}
}