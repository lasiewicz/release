﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Globalization;


namespace Spark.Common.RestConsumer.V2.Models.Mingle.Subscription
{
    [DataContract(Name = "SubscriptionPlanStatus", Namespace = "")]
    public class SubscriptionPlanStatus
    {
        [DataMember(Name = "IsSelfSuspended")]
        public bool IsSelfSuspended { get; set; }

        [DataMember(Name = "PackageTypeId")]
        public int PackageTypeId { get; set; }

        [DataMember(Name = "IsFreeTrial")]
        public bool IsFreeTrial { get; set; }

        [DataMember(Name = "BaseSubscriptionPlan")]
        public SubscriptionBase BaseSubscriptionPlan { get; set; }

        [DataMember(Name = "PremiumOptions")]
        public List<SubscriptionPremiumOption> PremiumOptions { get; set; }

        [DataMember(Name = "FreeTrialSubscriptionPlan")]
        public FreeTrialSubscriptionPlan FreeTrialSubscriptionPlan { get; set; }

        [DataMember(Name = "SubscriptionEndDate")]
        public string SubscriptionEndDate { get; set; }
    }
}
