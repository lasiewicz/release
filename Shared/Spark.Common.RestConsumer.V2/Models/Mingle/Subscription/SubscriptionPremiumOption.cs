﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Subscription
{
    [DataContract(Name = "SubscriptionPremiumOption", Namespace = "")]
    public class SubscriptionPremiumOption
    {
        [DataMember(Name = "PremiumSubscriptionType")]
        public int PremiumSubscriptionType { get; set; }

        [DataMember(Name = "IsEnabled")]
        public bool IsEnabled { get; set; }

        [DataMember(Name = "RenewalRate")]
        public double RenewalRate { get; set; }

        [DataMember(Name = "CurrencyType")]
        public int CurrencyType { get; set; }

        [DataMember(Name = "RenewalDuration")]
        public int RenewalDuration { get; set; }

        [DataMember(Name = "RenewalDurationType")]
        public int RenewalDurationType { get; set; }

        public string RenewalDurationTimeSpan
        {
            get
            {
                 string[,] durationType = { { "None", "0" }, { "Minute", "1" }, { "Hour ", "2" }, { "Day", "3" }, { "Week", "4" }, { "Month", "5" }, { "Year", "6" } };

                    for (int i = 0; i <= durationType.GetUpperBound(0); i++)
                    {
                        if (Int32.Parse(durationType[i, 1]) == RenewalDurationType)
                        {
                            return durationType[i, 0];
                        }
                    }
                    return "No duration type found";
            }
        }

        public string CurrencyTypeSymbol
        {
            get
            {
                string[,] currencyType = { { "None", "0" }, { "USDollar", "&#36;" }, { "Euro", "&#128;" }, { "CanadianDollar ", "&#36;" }, { "Pound", "&#163;" }, { "AustralianDollar", "&#36;" }, { "Shekels", "&#8362;" } };

                if (CurrencyType < currencyType.GetUpperBound(0))
                {
                    return currencyType[CurrencyType, 1];
                }
                return "No currency type found";
            }
        }

        public string PremiumType
        {
            get
            {
                string[,] premiumSubscriptionType = { { "None", "0" }, { "Profile Highlight", "1" }, { "Member Spotlight", "2" }, { "JMeter", "4" }, { "All Access", "32" }, { "Read Receipt", "128" } };

                for (int i = 0; i <= premiumSubscriptionType.GetUpperBound(0); i++)
                {
                    if (Int32.Parse(premiumSubscriptionType[i, 1]) == PremiumSubscriptionType)
                    {
                        return premiumSubscriptionType[i, 0];
                    }
                }
                return "No premium subscription type found"; 
            }
        }

        [DataMember(Name = "Expires")]
        public string Expires { get; set; }

        [DataMember(Name = "PackageTypeID")]
        public int PackageTypeID { get; set; }

        public string PackageType
        {
            get
            {
                string[,] premiumSubscriptionType = { { "None", "0" }, { "Basic", "1" }, { "Bundled", "2" }, { "AlaCarte", "3" }, { "Discount", "4" }, { "RemainingCredit", "5" } , { "OneOff", "6" }, {"ServiceFee", "7"} , {"FreeTrial", "8"}, {"BOGOEligible", "9"}};

                for (int i = 0; i <= premiumSubscriptionType.GetUpperBound(0); i++)
                {
                    if (Int32.Parse(premiumSubscriptionType[i, 1]) == PremiumSubscriptionType)
                    {
                        return premiumSubscriptionType[i, 0];
                    }
                }
                return "No package type found"; 
            }
        }
    }
}
