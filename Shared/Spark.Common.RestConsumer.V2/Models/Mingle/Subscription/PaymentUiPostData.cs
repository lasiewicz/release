﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Subscription
{
    [DataContract(Name = "PaymentUiPostData", Namespace = "")]
    public class PaymentUiPostData
    {
        /// <summary>
        /// Json string used to POST to Pament UI from mobile the subscription page.
        /// </summary>
        [DataMember(Name = "Json")]
        public String Json { get; set; }

        /// <summary>
        /// This is the Spark Payment UI url to POST the Json string to.
        /// </summary>
        [DataMember(Name = "PaymentUiUrl")]
        public String PaymentUiUrl { get; set; }

        [DataMember(Name = "ExternalClientTrackingId")]
        public int ExternalClientTrackingId { get; set; }
    }
}
