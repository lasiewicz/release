﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Subscription
{
    [DataContract(Name = "IsMobileAppSubscriber", Namespace = "")]
    public class IsMobileAppSubscriber
    {
        [DataMember(Name = "mobileStore")]
        public string MobileStore { get; set; }

        [DataMember(Name = "isSubscriber")]
        public bool IsSubscriber { get; set; }
    }
}
