﻿#region

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

#endregion

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Subscription
{
    [Serializable]
    [DataContract(Name = "PaymentUiPostDataRequest", Namespace = "")]
    public class PaymentUiPostDataRequest
    {
        public PaymentUiPostDataRequest()
        {
            CancelUrl = "";
            ReturnUrl = "";
            ConfirmationUrl = "";
            DestinationUrl = "";
            PrtId = "";
            //SrId = "";
        }

        //[DataMember(Name = "BrandUri")]
        //public string BrandUri { get; set; }
        [DataMember(Name = "PaymentUiPostDataType")]
        public string PaymentUiPostDataType { get; set; }

        [DataMember(Name = "CancelUrl")]
        public string CancelUrl { get; set; }

        [DataMember(Name = "ReturnUrl")]
        public string ReturnUrl { get; set; }

        [DataMember(Name = "ConfirmationUrl")]
        public string ConfirmationUrl { get; set; }

        [DataMember(Name = "DestinationUrl")]
        public string DestinationUrl { get; set; }

        /// <summary>
        ///     Purchase Reason Type Id. Semicolon separated for multiple values.
        /// </summary>
        [DataMember(Name = "PrtId")]
        public string PrtId { get; set; }

        /// <summary>
        ///     Subscription Reason (?) Id. Semicolon separated for multiple values.
        /// </summary>
        //[DataMember(Name = "SrId")]
        //public string SrId { get; set; }

        [DataMember(Name = "ClientIp")]
        public string ClientIp { get; set; }

        [DataMember(Name = "PrtTitle")]
        public string PrtTitle { get; set; }

        /// <summary>
        ///     temporary, util MVC3 is able to deserialize a dictionary
        /// </summary>
        [DataMember(Name = "omnitureVariablesJson")]
        public string OmnitureVariablesJson
        {
            get { return JsonConvert.SerializeObject(OmnitureVariables); }
        }

        [DataMember(Name = "properties")]
        public Dictionary<String, String> OmnitureVariables { get; set; }

        [DataMember(Name = "MemberLevelTrackingLastApplication")]
        public string MemberLevelTrackingLastApplication { get; set; }

        [DataMember(Name = "MemberLevelTrackingIsMobileDevice")]
        public string MemberLevelTrackingIsMobileDevice { get; set; }

        [DataMember(Name = "MemberLevelTrackingIsTablet")]
        public string MemberLevelTrackingIsTablet { get; set; }

        [DataMember(Name = "OrderID")]
        public string OrderID { get; set; }

        [DataMember(Name = "PromoType")]
        public string PromoType { get; set; }

        [DataMember(Name = "TemplateId")]
        public string TemplateId { get; set; }

        [DataMember(Name = "PromoID")]
        public int PromoID { get; set; }

        [DataMember(Name = "GiftDestinationUrl")]
        public string GiftDestinationUrl { get; set; }

        [DataMember(Name = "AdminDomainName")]
        public string AdminDomainName { get; set; }

        [DataMember(Name = "MemberID")]
        public int MemberID { get; set; }

        [DataMember(Name = "AdminMemberID")]
        public int AdminMemberID { get; set; }

        [DataMember(Name = "PaymentType")]
        public int PaymentType { get; set; }
    
    }
}