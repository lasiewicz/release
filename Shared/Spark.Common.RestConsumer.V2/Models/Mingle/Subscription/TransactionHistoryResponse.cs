﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Subscription
{
    [DataContract(Name = "TransactionHistoryResponse", Namespace = "")]
    public class TransactionHistoryResponse 
    {
        public int code { get; set; }
        public string status { get; set; }
        public List<TransactionHistory> data { get; set; }
    }
}