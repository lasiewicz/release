﻿#region

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

#endregion

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Subscription
{
    [Serializable]
    [DataContract(Name = "SubscriptionPremiumChangeDataRequest", Namespace = "")]
    public class SubscriptionPremiumChangeDataRequest
    {
        public SubscriptionPremiumChangeDataRequest()
        {
            
        }

        //[DataMember(Name = "BrandUri")]
        //public string BrandUri { get; set; }
        [DataMember(Name = "BaseSubscriptionPlan")]
        public SubscriptionBase BaseSubscriptionPlan { get; set; }

        [DataMember(Name = "PremiumOptions")]
        public List<SubscriptionPremiumOption> PremiumOptions { get; set; }

        [DataMember (Name = "IsSelfSuspended")]
        public bool IsSelfSuspended { get; set; }
    }
}