﻿using System;
using System.Runtime.Serialization;



namespace Spark.Common.RestConsumer.V2.Models.Mingle.Subscription
{
    [DataContract(Name = "TransactionHistory", Namespace = "")]
    public class TransactionHistory 
    {
        [DataMember(Name = "Description")]
        public string Description { get; set; }

        [DataMember(Name = "Date")]
        public string Date { get; set; }

        [DataMember(Name = "TransactionType")]
        public string TransactionType { get; set; }

        [DataMember(Name = "InitialCost")]
        public object InitialCost { get; set; }

        [DataMember(Name = "InitialDuration")]
        public int InitialDuration { get; set; }

        [DataMember(Name = "InitialDurationType")]
        public int InitialDurationType { get; set; }

        [DataMember(Name = "RenewalRate")]
        public double RenewalRate { get; set; }

        [DataMember(Name = "RenewalDuration")]
        public int RenewalDuration { get; set; }

        [DataMember(Name = "RenewalDurationType")]
        public int RenewalDurationType { get; set; }

        [DataMember(Name = "CurrencyType")]
        public int CurrencyType { get; set; }

        [DataMember(Name = "PlanID")]
        public int PlanID { get; set; }

        [DataMember(Name = "PromoID")]
        public int? PromoID { get; set; }

        [DataMember(Name = "PromoDescription")]
        public string PromoDescription { get; set; }

        [DataMember(Name = "PromoOffer")]
        public string PromoOffer { get; set; }

        [DataMember(Name = "Status")]
        public string Status { get; set; }

        [DataMember(Name = "PaymentType")]
        public string PaymentType { get; set; }

        [DataMember(Name = "Account")]
        public int? Account { get; set; }

        [DataMember(Name = "Confirmation")]
        public int Confirmation { get; set; }

        [DataMember(Name = "AuthID")]
        public int? AuthID { get; set; }

        [DataMember(Name = "ChargeID")]
        public int? ChargeID { get; set; }
    }
}
