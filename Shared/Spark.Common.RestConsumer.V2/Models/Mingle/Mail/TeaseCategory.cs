﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Mail
{
    [DataContract(Name = "TeaseCategory", Namespace = "")]
    public class TeaseCategory : IEntity
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "teases")]
        public List<Tease> Teases { get; set; }
    }
}
