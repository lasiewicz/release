using System;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Mail
{
	[DataContract(Name = "MailMessage", Namespace = "")]
	public class MailMessage : IEntity
	{
        [DataMember(Name = "messageId")]
        public int messageId { get; set; }

        [DataMember(Name = "folderId")]
        public int folderId { get; set; }

		[DataMember(Name = "id")]
		public int Id { get; set; }

        [DataMember(Name = "markAsRead")]
        public bool markAsRead { get; set; }

		//[DataMember(Name = "senderMemberId")]
		//public int SenderMemberId { get; set; }

		//[DataMember(Name = "senderMembername")]
		//public string SenderMemberName { get; set; }

		//[DataMember(Name = "senderMemberThumbFileWebPath")]
		//public string SenderMemberThumbFileWebPath { get; set; }

		[DataMember(Name = "sender")]
		public FullProfile Sender { get; set; }

		[DataMember(Name = "recipient")]
		public FullProfile Recipient { get; set; }

		[DataMember(Name = "body")]
        [Required]
        [DataType(DataType.MultilineText)]
		public string Body { get; set; }

		[DataMember(Name = "subject")]
		public string Subject { get; set; }

        [DataMember(Name = "mailType")]
        public string MailType { get; set; }

        //[DataMember(Name = "insertDate")]
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yy h:mm tt}")]
        //public DateTime InsertDate { get; set; }

        //[DataMember(Name = "openDate")]
        //public DateTime OpenDate { get; set; }

        //[DataMember(Name = "messageStatus")]
        //public int MessageStatus { get; set; }

		[DataMember(Name = "memberFolderId")]
		public int MemberFolderId { get; set; }

        [DataMember(Name = "prevMessageId")]
        public int prevMessageId { get; set; }

        [DataMember(Name = "nextMessageId")]
        public int nextMessageId { get; set; }

        [DataMember(Name = "subject")]
        public string subject { get; set; }

        [DataMember(Name = "mailType")]
        public string mailType { get; set; }

        [DataMember(Name = "body")]
        public string body { get; set; }

        [DataMember(Name = "cardFlash")]
        public string cardFlash { get; set; }

        [DataMember(Name = "cardImage")]
        public string cardImage { get; set; }

        [DataMember(Name = "imMessages")]
        public string[] imMessages { get; set; }

        [DataMember(Name = "insertDate")]
        public string insertDate { get; set; }

        [DataMember(Name = "openDate")]
        public string openDate { get; set; }

        [DataMember(Name = "messageStatus")]
        public int messageStatus { get; set; }

	}
}
