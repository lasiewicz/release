﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Mail
{
	[DataContract(Name = "PostMessageResult", Namespace = "")]
	public class PostMessageResult
	{
		[DataMember(Name = "messageId")]
		public int MessageId { get; set; }

		[DataMember(Name = "failReason")]
		public string FailureReason { get; set; }

		[DataMember(Name = "teasesLeft")]
		public string TeasesLeft { get; set; }
	}
}
