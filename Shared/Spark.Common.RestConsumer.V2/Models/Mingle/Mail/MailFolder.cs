using System.Collections.Generic;
using System.Runtime.Serialization;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Mail
{
	[DataContract(Name = "MailFolder", Namespace = "")]
	public class MailFolder : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

		/// <summary>
		/// Represents the CommunityID for this message.
		/// </summary>
		[DataMember(Name = "groupId")]
		public int GroupId { get; set; }

		[DataMember(Name = "mailboxOwnerMemberId")]
		public int mailboxOwnerMemberId { get; set; }

		[DataMember(Name = "description")]
		public string Description { get; set; }

        [DataMember(Name = "messageList")]
		public List<MailMessage> messageList { get; set; }

        [DataMember(Name = "id")]
        public int MessageId { get; set; }

        [DataMember(Name = "folderId")]
        public int folderId { get; set; }

        [DataMember(Name = "pageSize")]
        public int pageSize { get; set; }

        [DataMember(Name = " pageNumber")]
        public int pageNumber { get; set; }

        [DataMember(Name = "filter")]
        public string filter { get; set; }
     
        [DataMember(Name = "sender")]
        public FullProfile Sender { get; set; }

        [DataMember(Name = "recipient")]
        public FullProfile Recipient { get; set; }


        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "mailType")]
        public string MailType { get; set; }

        [DataMember(Name = "body")]
        public string body { get; set; }

        [DataMember(Name = "cardFlash")]
        public string cardFlash { get; set; }

        [DataMember(Name = "cardImage")]
        public string cardImage { get; set; }

        [DataMember(Name = "insertDate")]
        public string insertDate { get; set; }

        [DataMember(Name = "openDate")]
        public string openDate { get; set; }

        [DataMember(Name = "messageStatus")]
        public int messageStatus { get; set; }

        [DataMember(Name = "memberFolderId")]
        public int memberFolderId { get; set; }
        
	}
}

