using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using Spark.Common.RestConsumer.V2.Models.Mingle.Photos;

using Spark.Common.RestConsumer.V2.Models.Mingle.GAM;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Profile
{
	[DataContract(Name = "FullProfile", Namespace = "")]
	public class FullProfile : MiniProfile
	{

      //Basic Info
        [DataMember(Name = "primaryPhoto")]
        public Photo PrimaryPhoto { get; set; }

        public List<Photo> Photos { get; set; }

        [DataMember(Name = "photoCount")]
        public int PhotoCount { get; set; }

        //remove
        [DataMember(Name = "children")]

        public string ChildrenCountText { get; set; }

        [DataMember(Name = "language")]
        public List<string> LanguagesSpoken { get; set; }

        [DataMember(Name = "education")]
        public string Education { get; set; }

        [DataMember(Name = "occupation")]
        public string Occupation { get; set; }

        // In My Own Words
        [DataMember(Name = "aboutMe")]
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public string AboutMe { get; set; }

        // My Ideal Match
        [DataMember(Name = "matchMinAge")]
		public int? AgeLowest { get; set; }

        [DataMember(Name = "matchMaxAge")]
		public int? AgeHighest { get; set; }

        /// <summary>
        /// Do not use. Should be deprecated.
        /// todo: This is a helper method that should be pushed down to the client leve.
        /// </summary>
		public string AgeRange
		{
			get
			{
				if (AgeLowest == null || AgeHighest == null) return null;
                //hard coded localized string here hence the need to deprecate.
				return String.Format("{0} to {1}", AgeLowest, AgeHighest);
			}
		}

        [DataMember(Name = "matchMaritalStatus")]
        public List<string> MatchMaritalStatus { get; set; }

        [DataMember(Name = "matchJDateReligion")]
        public List<string> MatchReligiousBackground { get; set; }

        [DataMember(Name = "matchEducationLevel")]
        public List<string> MatchEducationLevel { get; set; }

        [DataMember(Name = "matchSmokingHabits")]
        public List<string> MatchSmokingHabits { get; set; }

        [DataMember(Name = "matchDrinkingHabits")]
        public List<string> MatchDrinkingHabits { get; set; }

		[DataMember(Name = "isViewersFavorite")]
		public bool IsViewersFavorite { get; set; }


        [DataMember(Name = "removeFromSearch")]
        public bool RemoveFromSearch { get; set; }

        //CM attributes
        [DataMember(Name = "ministry_displayname")]
        public string MinistryDisplayname { get; set; }

        [DataMember(Name = "body_type")]
        public string BodyType { get; set; }


        [DataMember(Name = "hair")]
        [DisplayFormat(NullDisplayText = "(not answered yet)")]
        public string Hair { get; set; }

        [DataMember(Name = "eyes")]
        [DisplayFormat(NullDisplayText = "(not answered yet)")]
        public string EyeColor { get; set; }

        [DataMember(Name = "about_children")]
        public string AboutChildren { get; set; }

        [DataMember(Name = "children_at_home")]
        public string ChildrenAtHome { get; set; }

        [DataMember(Name = "smoke")]
        public string SmokingHabits { get; set; }

        [DataMember(Name = "drink")]
        public string DrinkingHabits { get; set; }

        [DataMember(Name = "religion")]
        public string Religion { get; set; }

        [DataMember(Name = "religion2")]
        public string Religion2 { get; set; }

        [DataMember(Name = "church_activity")]
        public string ChurchActivity { get; set; }

        [DataMember(Name = "self_description")]
        public string SelfDescription { get; set; }


        [DataMember(Name = "music")]
        public string[] FavoriteMusic { get; set; }

        [DataMember(Name = "favorite_musicians")]
        public string FavoriteMusicians { get; set; }

        [DataMember(Name = "ethnicity")]
        public string[] Ethnicity { get; set; }

        [DataMember(Name = "movie")]
        public string[] MovieGenres { get; set; }

        [DataMember(Name = "favorite_movies")]
        public string FavoriteMovies { get; set; }

        [DataMember(Name = "favorite_tv")]
        public string FavoriteTV { get; set; }

        [DataMember(Name = "outdoors")]
        public string[] OutdoorActivities { get; set; }

        [DataMember(Name = "indoors")]
        public string[] IndoorActivities { get; set; }

        [DataMember(Name = "season")]
        public string Season { get; set; }

        [DataMember(Name = "great_trip")]
        public string GreatTrip { get; set; }

        [DataMember(Name = "food")]
        public string[] FavoriteFoods { get; set; }

        [DataMember(Name = "favorite_restaurants")]
        public string FavoriteRestaurants { get; set; }

        [DataMember(Name = "looks")]
        public string Looks { get; set; }

        [DataMember(Name = "mentality")]
        public string Mentality { get; set; }

        [DataMember(Name = "political")]
        public string PoliticalOrientation { get; set; }

        [DataMember(Name = "schools_attended")]
        public string SchoolsAttended { get; set; }

        [DataMember(Name = "living_quarters")]
        public string LivingQuarters { get; set; }

        [DataMember(Name = "prefer_to_live")]
        public string PreferToLive { get; set; }

        [DataMember(Name = "punctual")]
        public string Punctual { get; set; }

        [DataMember(Name = "height")]
        public string Height { get; set; }

        [DataMember(Name = "past_relationships")]
        public string PastRelationships { get; set; }

        [DataMember(Name = "planning")]
        public string Planning { get; set; }

        [DataMember(Name = "trendy")]
        public string Trendy { get; set; }

        [DataMember(Name = "wants_age")]
        public string WantsAge { get; set; }

        [DataMember(Name = "wants_distance")]
        public string WantsDistance { get; set; }

        [DataMember(Name = "maxDistance")]
        public int MaxDistance { get; set; }


        [DataMember(Name = "wants_height")]
        public string WantsHeight { get; set; }

        [DataMember(Name = "minHeight")]
        public int MinHeight { get; set; }

        [DataMember(Name = "maxHeight")]
        public int MaxHeight { get; set; }

        [DataMember(Name = "wants_body_type")]
        public string[] WantsBodyType { get; set; }

        [DataMember(Name = "bodyType")]
        public string[] BodyTypeMatch { get; set; }

        [DataMember(Name = "wants_education")]
        public string[] WantsEducationLevel { get; set; }

       
        [DataMember(Name = "wants_marital_status")]
        public string[] WantsMaritalStatus { get; set; }

        [DataMember(Name = "wants_religion")]
        public string[] WantsReligion { get; set; }

        [DataMember(Name = "wants_church_activity")]
        public string[] WantsChurchActivity { get; set; }

        [DataMember(Name = "wants_ethnicity")]
        public string[] WantsEthnicity { get; set; }

        [DataMember(Name = "wants_smoke")]
        public string[] WantsSmokingHabits { get; set; }

        
        [DataMember(Name = "wants_drink")]
        public string[] WantsDrinkingHabits { get; set; }
       

        [DataMember(Name = "greeting")]
        public string Greeting { get; set; }

        [DataMember(Name = "first_date")]
        public string FirstDate { get; set; }

        [DataMember(Name = "christian_meaning")]
        public string ChristianMeaning { get; set; }

        [DataMember(Name = "christian_how_long")]
        public string ChristianHowLong { get; set; }

        [DataMember(Name = "five_years")]
        public string FiveYears { get; set; }

        [DataMember(Name = "favorite_scripture")]
        public string FavoriteScripture { get; set; }

        [DataMember(Name = "anythingelse")]
        public string Anythingelse { get; set; }

        [DataMember(Name = "lookingFor")]
        public string[] LookingFor { get; set; }

        [DataMember(Name = "birthdate")]
        public string Birthdate { get; set; }

        //[DataMember(Name = "isIAPPayingMember")]
        //public bool IsIAPPayingMember { get; set; }

        //[DataMember(Name = "isIABPayingMember")]
        //public bool IsIABPayingMember { get; set; }

      
        [DataMember(Name = "adTargeting")]
        public AdTargeting AdTargeting { get; set; }

        [DataMember(Name = "canBuyUpgradeSubscription")]
        public bool CanBuyUpgradeSubscription { get; set; }

        [DataMember(Name = "canBuyPremiumService")]
        public bool CanBuyPremiumService { get; set; }
	}
}