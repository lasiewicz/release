﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.OAuth2
{
	[DataContract(Name = "OAuthTokens")]
	public class OAuthTokens
	{
        [DataMember(Name = "success")]
        public bool Success { get; set; }

        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }

		[DataMember(Name = "accessToken")]
		public string AccessToken { get; set; }

		[DataMember(Name = "AccessExpiresTime")]
		public DateTime AccessExpiresTime { get; set; }

		[DataMember(Name = "expiresIn")]
		public int ExpiresIn { get; set; }

		[DataMember(Name = "refreshToken")]
		public string RefreshToken { get; set; }

		[DataMember(Name = "RefreshTokenExpiresTime")]
		public DateTime RefreshTokenExpiresTime { get; set; }
		
		[DataMember(Name = "isPayingMember")]
        public bool IsPayingMember { get; set; }

    }
}
