﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.OAuth2
{
    [DataContract(Name = "TokenRefreshRequest")]
    public class TokenRefreshRequest
    {
        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "refreshToken")]
        public string RefreshToken { get; set; }
    }
}
