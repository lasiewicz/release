﻿#region

using System.Runtime.Serialization;

#endregion

namespace Spark.Common.RestConsumer.V2.Models.Mingle.OAuth2
{
    [DataContract(Name = "AuthenticateAccessTokenResponse", Namespace = "")]
    public class AuthenticateAccessTokenResponse
    {
        [DataMember(Name = "Authenticated")]
        public bool Authenticated { get; set; }

        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "FailCode")]
        public int FailCode { get; set; }

        [DataMember(Name = "FailReason")]
        public string FailReason { get; set; }
    }
}