﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.OAuth2
{
    [DataContract(Name = "validatedaccesstoken", Namespace = "")]
    public class ValidatedAccessToken
    {
        [DataMember(Name = "memberid")]
        public Int32 MemberId { get; set; }
    }
}
