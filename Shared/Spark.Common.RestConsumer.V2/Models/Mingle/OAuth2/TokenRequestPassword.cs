﻿using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.OAuth2
{
    [DataContract(Name = "TokenRequestPassword")]
    public class TokenRequestPassword
    {
        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "passwordIsEncrypted")]
        public bool PasswordIsEncrypted { get; set; }
    }
}
