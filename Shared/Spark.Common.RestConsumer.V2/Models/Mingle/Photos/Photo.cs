﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mingle.Photos
{
    [DataContract(Name = "Photo", Namespace = "")]
    public class Photo
    {
        [DataMember(Name = "memberPhotoId")]
        public Int32 MemberPhotoId { get; set; }
        [DataMember(Name = "fullId")]
        public Int32 FullId { get; set; }
        [DataMember(Name = "thumbId")]
        public Int32 ThumbId { get; set; }
        [DataMember(Name = "fullPath")]
        public string FullPath { get; set; }
        [DataMember(Name = "thumbPath")]
        public string ThumbPath { get; set; }
        [DataMember(Name = "caption")]
        public string Caption { get; set; }
        [DataMember(Name = "listorder")]
        public Int32 ListOrder { get; set; }
        [DataMember(Name = "isCaptionApproved")]
        public bool IsCaptionApproved { get; set; }
        [DataMember(Name = "isPhotoApproved")]
        public bool IsPhotoApproved { get; set; }
        [DataMember(Name = "IsMain")]
        public bool IsMain { get; set; }

        [DataMember(Name = "isApprovedForMain")]
        public bool IsApprovedForMain { get; set; }

        [DataMember(Name = "photo_960x640")]
        public string Photo960x640 { get; set; }
	}
}
