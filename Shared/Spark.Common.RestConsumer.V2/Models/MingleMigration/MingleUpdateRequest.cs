﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.V2.Models.MingleMigration
{
    public class MingleUpdateRequest
    {
        public Dictionary<string, object> AttributeData { get; set; }
        public Dictionary<string, List<int>> AttributeDataMultiValue { get; set; }
        public Dictionary<string, int> SearchPreferences { get; set; }
        public Dictionary<string, List<int>> SearchPreferencesMultiValue { get; set; }
        public Dictionary<string, string> ApprovedTextAttributeData;

        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }
        
        [DataMember(Name = "P1")]
        public string P1 { get; set; }
        
        [DataMember(Name = "ApprovedTextAttributeDataJson")]
        public string ApprovedTextAttributeDataJson
        {
            get { return JsonConvert.SerializeObject(ApprovedTextAttributeData); }
        }
        
        [DataMember(Name = "AttributeDataJson")]
        public string AttributeDataJson
        {
            get { return JsonConvert.SerializeObject(AttributeData); }
        }

        [DataMember(Name = "AttributeDataMultiValueJson")]
        public string AttributeDataMultiValueJson
        {
            get { return JsonConvert.SerializeObject(AttributeDataMultiValue); }
        }

        [DataMember(Name = "SearchPreferencesJson")]
        public string SearchPreferencesJson
        {
            get { return JsonConvert.SerializeObject(SearchPreferences); }
        }

        [DataMember(Name = "SearchPreferencesMultiValueJson")]
        public string SearchPreferencesMultiValueJson
        {
            get { return JsonConvert.SerializeObject(SearchPreferencesMultiValue); }
        }
    }
}
