﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.V2.Models.MingleMigration
{
    public class MingleFullProfileTransferRequest
    {
        public Dictionary<string, List<int>> AttributeDataMultiValue { get; set; }
        public Dictionary<string, object> AttributeData { get; set; }
        public Dictionary<string, int> SearchPreferences { get; set; }
        public Dictionary<string, List<int>> SearchPreferencesMultiValue { get; set; }
        public Dictionary<string, string> ApprovedTextAttributeData { get; set; }

        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }
       
        [DataMember(Name = "AttributeDataJson")]
        public string AttributeDataJson { get { return JsonConvert.SerializeObject(AttributeData); } } // JSON dictionary

        [DataMember(Name = "AttributeDataMultiValueJson")]
        public string AttributeDataMultiValueJson { get { return JsonConvert.SerializeObject(AttributeDataMultiValue); } } // JSON dictionary

        [DataMember(Name = "SearchPreferencesJson")]
        public string SearchPreferencesJson { get { return JsonConvert.SerializeObject(SearchPreferences); } } // JSON dictionary

        [DataMember(Name = "SearchPreferencesMultiValueJson")]
        public string SearchPreferencesMultiValueJson { get { return JsonConvert.SerializeObject(SearchPreferencesMultiValue); } } // JSON dictionary

        [DataMember(Name = "ApprovedTextAttributeDataJson")]
        public string ApprovedTextAttributeDataJson { get { return JsonConvert.SerializeObject(ApprovedTextAttributeData); } } // JSON dictionary

        

        
       
    
    }
}
