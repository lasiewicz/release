﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.V2.Models.MingleMigration
{
    public class MingleRegistrationRequest
    {
        public Dictionary<string, object> AttributeData { get; set; }
        public Dictionary<string, List<int>> AttributeDataMultiValue { get; set; }
        public Dictionary<string, int> SearchPreferences { get; set; }
        public Dictionary<string, List<int>> SearchPreferencesMultiValue { get; set; }

        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
        
        [DataMember(Name = "ipAddress")]
        public string IpAddress { get; set; }

        [DataMember(Name = "AttributeDataJson")]
        public string AttributeDataJson
        {
            get { return JsonConvert.SerializeObject(AttributeData); }
        }

        [DataMember(Name = "AttributeDataMultiValueJson")]
        public string AttributeDataMultiValueJson
        {
            get { return JsonConvert.SerializeObject(AttributeDataMultiValue); }
        }

        [DataMember(Name = "SearchPreferencesJson")]
        public string SearchPreferencesJson
        {
            get { return JsonConvert.SerializeObject(SearchPreferences); }
        }

        [DataMember(Name = "SearchPreferencesMultiValueJson")]
        public string SearchPreferencesMultiValueJson
        {
            get { return JsonConvert.SerializeObject(SearchPreferencesMultiValue); }
        }
    }
}
