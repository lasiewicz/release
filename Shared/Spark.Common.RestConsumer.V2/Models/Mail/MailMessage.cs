using System;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Spark.Common.RestConsumer.V2.Models.Profile;

namespace Spark.Common.RestConsumer.V2.Models.Mail
{
	[DataContract(Name = "MailMessage", Namespace = "")]
	public class MailMessage : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

		//[DataMember(Name = "senderMemberId")]
		//public int SenderMemberId { get; set; }

		//[DataMember(Name = "senderMembername")]
		//public string SenderMemberName { get; set; }

		//[DataMember(Name = "senderMemberThumbFileWebPath")]
		//public string SenderMemberThumbFileWebPath { get; set; }

		[DataMember(Name = "sender")]
		public MiniProfile Sender { get; set; }

		[DataMember(Name = "recipient")]
		public MiniProfile Recipient { get; set; }

		[DataMember(Name = "body")]
        [Required]
        [DataType(DataType.MultilineText)]
		public string Body { get; set; }

		[DataMember(Name = "subject")]
		public string Subject { get; set; }

        [DataMember(Name = "mailType")]
        public string MailType { get; set; }

		[DataMember(Name = "insertDate")]
		[DisplayFormat(DataFormatString = "{0:MM/dd/yy h:mm tt}")]
		public DateTime InsertDate { get; set; }

		[DataMember(Name = "openDate")]
		public DateTime OpenDate { get; set; }

		[DataMember(Name = "messageStatus")]
		public int MessageStatus { get; set; }

		[DataMember(Name = "memberFolderId")]
		public int MemberFolderId { get; set; }
	}
}
