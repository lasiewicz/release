﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Mail
{
	[Serializable]
    [DataContract(Name = "create", Namespace = "")]
	public class DeleteMessage
	{
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "CommunityId")]
        public int CommunityId { get; set; }
        [DataMember(Name = "MessageId")]
        public int MessageId { get; set; }
        [DataMember(Name = "CurrentFolderId")]
        public int CurrentFolderId { get; set; }
	}
}