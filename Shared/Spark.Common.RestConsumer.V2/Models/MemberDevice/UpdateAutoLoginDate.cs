﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.MemberDevice
{
    public class UpdateAutoLoginDate 
    {
        [DataMember(Name = "applicationId")]
        public int ApplicationId { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "brandId")]
        public int BrandId { get; set; }

        [DataMember(Name = "siteId")]
        public int SiteId { get; set; }
    }
}
