﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.V2.Models.Activity
{
    [DataContract(Name = "ActivityWithParametersRequest")]
    public class ActivityWithParametersRequest  
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "TargetMemberId")]
        public int TargetMemberId { get; set; }

        [DataMember(Name = "ActionType")]
        public string ActionType { get; set; }

        [DataMember(Name = "ParametersJsonString")]
        public string ParametersJsonString { get { return JsonConvert.SerializeObject(Parameters); } }

        public Dictionary<string, string> Parameters { get; set; }

        public ActivityWithParametersRequest()
        {
            Parameters = new Dictionary<string, string>();
        }
    }
}
