﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Applications
{

    [DataContract(Name = "Application", Namespace = "")]
	public class Application
	{
        [DataMember(Name = "brandId")]
        public int BrandId { get; set; }

		[DataMember(Name = "applicationId")]
		public int ApplicationId { get; set; }

		[DataMember(Name = "ownerMemberId")]
		public int OwnerMemberId { get; set; }

		[DataMember(Name = "clientSecret")]
		public string ClientSecret { get; set; }

		[DataMember(Name = "title")]
		public string Title { get; set; }

		[DataMember(Name = "description")]
		public string Description { get; set; }

		[DataMember(Name = "redirectUrl")]
		public string RedirectUrl { get; set; }
	}
}
