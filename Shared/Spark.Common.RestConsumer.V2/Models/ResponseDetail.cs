﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models
{
    public class ResponseDetail
    {
        [DataMember(Name = "code")]
        public int Code { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "errorCode")]
        public int ErrorCode { get; set; }

        [DataMember(Name = "errorMessage")]
        public string ErrorMessage { get; set; }
    }
}
