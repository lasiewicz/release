﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Subscription
{
    [DataContract(Name = "SubscriptionBase", Namespace = "")]
    public class SubscriptionBase
    {
        [DataMember(Name = "IsEnabled")]
        public bool IsEnabled { get; set; }

        [DataMember(Name = "RenewalRate")]
        public float RenewalRate { get; set; }

        [DataMember(Name = "CurrencyType")]
        public int CurrencyType { get; set; }

        [DataMember(Name = "RenewalDurationType")]
        public int RenewalDurationType { get; set; }

        public string RenewalDurationTimeSpan
        {
            get
            {
                string[,] durationType = { { "None", "0" }, { "Minute", "1" }, { "Hour ", "2" }, { "Day", "3" }, { "Week", "4" }, { "Month", "5" }, { "Year", "6" } };

                for (int i = 0; i <= durationType.GetUpperBound(0); i++)
                {
                    if (Int32.Parse(durationType[i, 1]) == RenewalDurationType)
                    {
                        return durationType[i, 0];
                    }
                }
                return "No duration type found";
            }
        }

        public string CurrencyTypeSymbol
        {
            get
            {
                string[,] currencyType = { { "None", "0" }, { "USDollar", "&#36;" }, { "Euro", "&#128;" }, { "CanadianDollar ", "&#36;" }, { "Pound", "&#163;" }, { "AustralianDollar", "&#36;" }, { "Shekels", "&#8362;" } };

                if (CurrencyType < currencyType.GetUpperBound(0))
                {
                    return currencyType[CurrencyType, 1];
                }
                return "No currency type found";
            }
        }

        [DataMember(Name = "RenewalDuration")]
        public int RenewalDuration { get; set; }

        [DataMember(Name = "InitialDuration")]
        public int InitialDuration { get; set; }
    }
}
