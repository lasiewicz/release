﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
namespace Spark.Common.RestConsumer.V2.Models.Subscription
{
    [DataContract(Name = "InternalResponse", Namespace = "")]
    public class InternalResponse 
    {
        [DataMember(Name = "responsecode")]
        public string Responsecode { get; set; }

        [DataMember(Name = "responsemessage")]
        public string Responsemessage { get; set; }

        [DataMember(Name = "userInterfaceActionTypeID")]
        public int UserInterfaceActionTypeId { get; set; }

        [DataMember(Name = "userInterfaceResponseCode")]
        public int? UserInterfaceResponseCode { get; set; }

    }
}
