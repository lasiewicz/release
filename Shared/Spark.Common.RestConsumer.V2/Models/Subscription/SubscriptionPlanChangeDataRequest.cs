﻿#region

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

#endregion

namespace Spark.Common.RestConsumer.V2.Models.Subscription
{
    [Serializable]
    [DataContract(Name = "SubscriptionPlanChangeDataRequest", Namespace = "")]
    public class SubscriptionPlanChangeDataRequest
    {
        public SubscriptionPlanChangeDataRequest()
        {
            
        }

        //[DataMember(Name = "BrandUri")]
        //public string BrandUri { get; set; }
        [DataMember(Name = "BaseSubscriptionPlan")]
        public SubscriptionBase BaseSubscriptionPlan { get; set; }

        [DataMember(Name = "IsSelfSuspended")]
        public bool IsSelfSuspended { get; set; }

        [DataMember(Name = "TerminationReasonId")]
        public int TerminationReasonId { get; set; }
    }
}