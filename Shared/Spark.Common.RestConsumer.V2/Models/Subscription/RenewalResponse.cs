﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
namespace Spark.Common.RestConsumer.V2.Models.Subscription
{
    [DataContract(Name = "RenewalResponse", Namespace = "")]
    public class RenewalResponse 
    {
        [DataMember(Name = "version")]
        public string Version { get; set; }

        [DataMember(Name = "response")]
        public string Response { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "InternalResponse")]
        public InternalResponse InternalResponse { get; set; }

        [DataMember(Name = "TransactionID")]
        public int TransactionId { get; set; }

        [DataMember(Name = "PremiumType")]
        public int PremiumType { get; set; }

    }
}
