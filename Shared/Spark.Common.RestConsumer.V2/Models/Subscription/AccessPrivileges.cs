﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
namespace Spark.Common.RestConsumer.V2.Models.Subscription
{
    [DataContract(Name = "AccessPrivileges", Namespace = "")]
    public class AccessPrivileges 
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "endDateUtc")]
        public DateTime EndDateUtc { get; set; }

        [DataMember(Name = "remainingCount")]
        public Nullable<Int32> RemainingCount { get; set; }
    }
}
