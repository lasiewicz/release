﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
namespace Spark.Common.RestConsumer.V2.Models.Subscription
{
    [DataContract(Name = "SubscriptionPlanChangeConfirmation", Namespace = "")]
    public class SubscriptionPlanChangeConfirmation 
    {
        [DataMember(Name = "RenewalSubscriptionId")]
        public int RenewalSubscriptionId { get; set; }

        [DataMember(Name = "RenewalResponse")]
        public RenewalResponse RenewalResponse { get; set; }

        [DataMember(Name = "PremiumType")]
        public int PremiumType { get; set; }
    }
}
