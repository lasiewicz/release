﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.OAuth2
{
    [DataContract(Name = "validatedaccesstoken", Namespace = "")]
    public class ValidatedAccessToken
    {
        [DataMember(Name = "MemberId")]
        public Int32 MemberId { get; set; }
    }

    public class ValidatedAccessTokenV2 : ValidatedAccessToken
    {
        [DataMember(Name = "IsBetaRedesignSite")]
        public bool IsBetaRedesignSite { get; set; }

        [DataMember(Name = "IsBetaRedesignSiteOffered")]
        public bool IsBetaRedesignSiteOffered { get; set; }
    }

}
