﻿#region

using System.Runtime.Serialization;

#endregion

namespace Spark.Common.RestConsumer.V2.Models.OAuth2
{
    [DataContract(Name = "PasswordResetRequest")]
    public class PasswordResetRequest
    {
        /// <summary>
        ///   member's email address
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }
    }
}