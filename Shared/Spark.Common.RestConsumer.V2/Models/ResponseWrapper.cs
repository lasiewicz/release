﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models
{
    [DataContract(Name = "ResponseWrapper", Namespace = "")]
    public class ResponseWrapper<T>
    {
        [DataMember(Name = "code")]
        public int Code { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "data")]
        public T Data { get; set; }

        [DataMember(Name = "error")]
        public Error Error { get; set; }
    }

    [DataContract(Name = "Error", Namespace = "")]
    public class Error
    {
        [DataMember(Name = "code")]
        public int Code { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}
