﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Profile
{
	[DataContract(Name = "MiniProfile", Namespace = "")]
	public class MiniProfile : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

		[DataMember(Name = "memberId")]
		public int MemberId { get; set; }

		[DataMember(Name = "username")]
		public string Username { get; set; }

		private string _thumbnailUrl;
		[DataMember(Name = "thumbnailUrl")]
		public string ThumbnailUrl
		{
			get
			{
				if (String.IsNullOrEmpty(_thumbnailUrl))
				{
					if (Gender != null)
					{
						if (Gender.ToLower() == "male")
						{
							return "/images/no-photo-m.png";
						}
						if (Gender.ToLower() == "female")
						{
							return "/images/no-photo-f.png";
						}
						return "/images/no-photo-l.png";
					}
				}
				return _thumbnailUrl;
			}
			set { _thumbnailUrl = value; }
		}

		private string _photoUrl;
		[DataMember(Name = "photoUrl")]
		public string PhotoUrl
		{
			get
			{
				if (String.IsNullOrEmpty(_photoUrl))
				{
					if (Gender != null)
					{
						if (Gender.ToLower() == "male")
						{
							return "/images/no-photo-l-m.png";
						}
						if (Gender.ToLower() == "female")
						{
							return "/images/no-photo-l-f.png";
						}
						return "/images/no-photo-l.png";
					}
				}
				return _photoUrl;
			}
			set { _photoUrl = value; }
		}

		[DataMember(Name = "maritalStatus")]
		public string MaritalStatus { get; set; }

		[DataMember(Name = "gender")]
		public string Gender { get; set; }

		[DataMember(Name = "seekingGender")]
		public string SeekingGender { get; set; }

		private static string GetGenderText(string gender)
		{
			if (String.Compare(gender, "Male", true) == 0)
                return "Man";
			if (String.Compare(gender, "Female", true) == 0)
                return "Woman";
			return gender;
		}

		public string GenderDisplay
		{
			get { return GetGenderText(Gender); }
		}

		public string SeekingGenderDisplay
		{
			get { return GetGenderText(SeekingGender); }
		}

		[DataMember(Name = "lookingFor")] // Relationship Mask (a friend, a date, marriage, etc)
		public List<string> LookingFor { get; set; }

		[DataMember(Name = "age")]
		public int? Age { get; set; }

        [DataMember(Name = "jDateEthnicity")]
        public string JDateEthnicity { get; set; }

		[DataMember(Name = "location")] // City, State
		public string Location { get; set; }

		[DataMember(Name = "lastLoggedIn")]
		public DateTime? LastLoggedIn { get; set; }

		[DataMember(Name = "lastUpdated")]
		public DateTime? LastUpdatedProfile { get; set; }

		[DataMember(Name = "isOnline")]
		public bool IsOnline { get; set; }

        [DataMember(Name = "SubscriptionStatus")]
        public string SubscriptionStatus { get; set; }

        [DataMember(Name = "ZipCode")]
        public string ZipCode { get; set; }

        [DataMember(Name = "RegistrationDate")]
        public DateTime? RegistrationDate { get; set; }

        [DataMember(Name = "regionId")]
        public int? RegionId { get; set; }

        [DataMember(Name = "SubscriptionStatusGam")]
        public string SubscriptionStatusGam { get; set; }

        [DataMember(Name = "isIAPPayingMember")]
        public bool IsIAPPayingMember { get; set; }

        [DataMember(Name = "isIABPayingMember")]
        public bool IsIABPayingMember { get; set; }

        [DataMember(Name = "isPayingMember")]
        public bool IsPayingMember { get; set; }
	}
}