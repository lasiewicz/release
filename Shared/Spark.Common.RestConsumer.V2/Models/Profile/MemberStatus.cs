﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Profile
{

    [DataContract(Name = "MemberStatus", Namespace = "")]
    public class MemberStatus
    {
        [DataMember(Name = "subscriptionStatus")]
        public string SubscriptionStatus { get; set; }
    }
}
