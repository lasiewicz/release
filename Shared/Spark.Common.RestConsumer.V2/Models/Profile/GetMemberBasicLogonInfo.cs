﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Profile
{
    [DataContract(Name = "GetMemberBasicLogonInfo", Namespace = "")]
    public class GetMemberBasicLogonInfo
    {
        [DataMember(Name = "memberId")]
		public int MemberId { get; set; }

        [DataMember(Name = "emailAddress")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        [DataMember(Name = "brandId")]
        public int BrandId { get; set; }

        [DataMember(Name = "siteId")]
        public int SiteId { get; set; }
    }
}
