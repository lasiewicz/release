﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.V2.Models.Profile
{
    public class DisplaySettings
    {
        [DataMember(Name = "Online")]
        public bool Online { get; set; }
    }
}
