namespace Spark.Common.RestConsumer.V2.Models
{
	public interface IEntity
	{
		int Id { get; set; }
	}
}