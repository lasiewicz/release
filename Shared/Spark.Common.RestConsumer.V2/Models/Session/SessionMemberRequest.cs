﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Session
{
    [DataContract(Name = "SessionMemberRequest")]
    public class SessionMemberRequest
    {
        [DataMember(Name = "SessionId")]
        public string SessionId { get; set; }
    }
}
