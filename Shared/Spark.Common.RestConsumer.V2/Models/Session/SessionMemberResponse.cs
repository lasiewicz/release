﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Session
{
    [DataContract(Name = "SessionMemberResponse")]
    public class SessionMemberResponse
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
    }
}
