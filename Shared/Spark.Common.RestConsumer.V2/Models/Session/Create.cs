﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Session
{
    [Serializable]
    [DataContract(Name = "Create", Namespace = "")]
    public class Create
    {
        [DataMember(Name = "BrandId")]
        public Int32 BrandId { get; set; }
    }
}
