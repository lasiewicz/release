﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Session
{
    [DataContract(Name = "SessionCreateRequest", Namespace = "")]
    public class SessionCreateRequest
    {
        [DataMember(Name = "MemberID")]
        public Int32 MemberID { get; set; } 
    }
}
