﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Session
{
    [DataContract(Name = "SessionCreateRequestWithSessionId", Namespace = "")]
    public class SessionCreateRequestWithSessionId: SessionCreateRequest
    {
        [DataMember(Name = "SessionId")]
        public string SessionId { get; set; }
    }
}
