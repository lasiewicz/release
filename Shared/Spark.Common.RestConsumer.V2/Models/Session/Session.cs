﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.V2.Models.Session
{
    [Serializable]
    [DataContract(Name = "session", Namespace = "")]
    public class Session
    {
        public Session()
        {
            Properties = new Dictionary<string, object>();
        }
        
        [DataMember(Name = "key")]
        public String Key { get; set; }

        [DataMember(Name = "memberId")]
        public Int32 MemberId
        {
         
            get
            {
                if (Properties.ContainsKey("MemberId"))
                {
                    return Convert.ToInt32(Properties["MemberId"]);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (Properties.ContainsKey("MemberId"))
                {
                    Properties["MemberId"] = Convert.ToString(value);
                }
                else
                {
                    Properties.Add("MemberId", Convert.ToString(value));
                }
            }
        }

        [DataMember(Name = "brandId")]
        public Int32 BrandId
        {
            get
            {
                if (Properties.ContainsKey("BrandId"))
                {
                    return Convert.ToInt32(Properties["BrandId"]);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (Properties.ContainsKey("BrandId"))
                {
                    Properties["BrandId"] = Convert.ToString(value);
                }
                else
                {
                    Properties.Add("BrandId", Convert.ToString(value));
                }
            }
        }

        [DataMember(Name = "ispayingmember")]
        public Boolean IsPayingMember
        {
            get
            {
                if (Properties.ContainsKey("IsPayingMember"))
                {
                    return Convert.ToBoolean(Properties["IsPayingMember"]);
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if (Properties.ContainsKey("IsPayingMember"))
                {
                    Properties["IsPayingMember"] = Convert.ToBoolean(value);
                }
                else
                {
                    Properties.Add("IsPayingMember", Convert.ToBoolean(value));
                }
            }
        }

        ///// <summary>
        ///// temporary, util MVC3 is able to deserialize a dictionary
        ///// </summary>
        //[DataMember(Name = "propertiesJson")]
        //public string PropertiesJson
        //{
        //    get
        //    {
        //        return JsonConvert.SerializeObject(Properties);
        //    }
        //}

        [DataMember(Name = "properties")]
        public Dictionary<String, Object> Properties { get; set; }

        [DataMember(Name = "detroy")]
        public Boolean Destroy { get; set; }

        [DataMember(Name = "this")]
        public Object this[string index]
        {
            get { return Properties.Keys.Contains(index.ToLower()) ? Properties[index.ToLower()] : null; }
            set
            {
                if (Properties.Keys.Contains(index.ToLower()))
                {
                    Properties[index.ToLower()] = value;
                }
                else
                {
                    Properties.Add(index.ToLower(), value);
                }
            }
        }

        public void RemoveValue(string key)
        {
            Properties.Remove(key.ToLower());
        }
    }
}
