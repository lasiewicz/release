﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Member
{
    [DataContract(Name = "CompleteRegistrationResult", Namespace = "")]
    public class CompleteRegistrationResult
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "Status")]
        public string Status { get; set; }
    }
}
