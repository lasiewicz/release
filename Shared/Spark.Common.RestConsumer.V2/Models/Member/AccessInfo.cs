﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Member
{
    [DataContract(Name = "AccessInfo", Namespace = "")]
    public class AccessInfo
    {
        [DataMember(Name = "isPayingMember")]
        public bool IsPayingMember { get; set; }
    }
}
