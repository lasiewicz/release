using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Member
{
	[DataContract(Name = "authenticate", Namespace = "")]
	public class Authenticate
	{
		[DataMember(Name = "id")]
		public Int32 Id { get; set; }

		[DataMember(Name = "statusCode")]
		public Int32 StatusCode { get; set; }

        [DataMember(Name = "statusMessage")]
        public string StatusMessage { get; set; }
	}
}