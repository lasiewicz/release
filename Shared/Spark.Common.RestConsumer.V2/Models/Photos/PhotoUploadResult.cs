﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Photos
{
	[DataContract(Name = "PhotoUploadResult", Namespace = "")]
	public class PhotoUploadResult
	{
		[DataMember(Name = "success")]
		public bool Success { get; set; }

		[DataMember(Name = "photoId")]
		public int MessageId { get; set; }

		[DataMember(Name = "failureReason")]
		public string FailureReason { get; set; }

	}
}
