﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Photos
{
	public class PhotoCaptionUpdate
	{
		[DataMember(Name = "memberId")]
		public int MemberId { get; set; }

		[DataMember(Name = "memberPhotoId")]
		public int MemeberPhotoId { get; set; }

		[DataMember(Name = "caption")]
		public string Caption { get; set; }
	}
}
