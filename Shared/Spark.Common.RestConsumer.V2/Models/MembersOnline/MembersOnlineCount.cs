﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.MembersOnline
{
    [Serializable]
    [DataContract(Name = "MembersOnlineCount", Namespace = "")]
    public class MembersOnlineCount
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }
    }
}
