﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.MembersOnline
{
    [Serializable]
    [DataContract(Name = "MembersOnlineRequest", Namespace = "")]
    public class MembersOnlineRequest
    {
    }
}
