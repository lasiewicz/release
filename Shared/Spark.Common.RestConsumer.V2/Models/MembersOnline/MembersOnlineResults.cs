﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Spark.Common.RestConsumer.V2.Models.Profile;

namespace Spark.Common.RestConsumer.V2.Models.MembersOnline
{
    [Serializable]
    [DataContract(Name = "MembersOnlineResults", Namespace = "")]
    public class MembersOnlineResults
    {
        [DataMember(Name = "Members")]
        public List<MiniProfile> Members { get; set; }
    }
}
