﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.MembersOnline
{
    [DataContract(Name = "MembersOnlinePreferences", Namespace = "")]
    public class MembersOnlinePreferences
    {
        [DataMember(Name = "seekingGender")]
        public string SeekingGender { get; set; }

        [DataMember(Name = "minAge")]
        [Range(18, 99)]
        public int MinAge { get; set; }

        [DataMember(Name = "maxAge")]
        [Range(18, 99)]
        public int MaxAge { get; set; }

        [DataMember(Name = "regionId")]
        public int RegionId { get; set; }

        [DataMember(Name = "languageId")]
        public int LanguageId { get; set; }
    }
}
