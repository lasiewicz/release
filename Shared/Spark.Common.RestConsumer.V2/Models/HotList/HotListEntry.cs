using System;
using System.Runtime.Serialization;
using Spark.Common.RestConsumer.V2.Models.Profile;
using System.ComponentModel.DataAnnotations;

namespace Spark.Common.RestConsumer.V2.Models.HotList
{
	[DataContract(Name = "hotListEntry", Namespace = "")]
	public class HotListEntry
	{
		[DataMember(Name = "category")]
		public string Category { get; set; }
		
		[DataMember(Name = "actionDate")]
		[DisplayFormat(DataFormatString = "{0:MM/dd/yy h:mm tt}")]
		public DateTime ActionDate { get; set; }

		[DataMember(Name = "miniProfile")]
		public MiniProfile MiniProfile { get; set; }
	}
}