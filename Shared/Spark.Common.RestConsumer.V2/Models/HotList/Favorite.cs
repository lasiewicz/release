﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.HotList
{
	public class Favorite
	{
        //[DataMember(Name = "brandUri")]
        //public string BrandUri { get; set; }

		[DataMember(Name = "favoriteMemberId")]
		public int FavoriteMemberId { get; set; }
	}
}
