﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Common.RestConsumer.V2.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationComplete", Namespace = "")]
    public class RegistrationComplete
    {
        [DataMember(Name = "RegistrationSessionID")]
        public string RegistrationSessionID { get; set; }

        [DataMember(Name = "MemberID")]
        public int MemberID { get; set; }

        [DataMember(Name = "TimeStamp")]
        public DateTime TimeStamp { get; set; }
    }
}