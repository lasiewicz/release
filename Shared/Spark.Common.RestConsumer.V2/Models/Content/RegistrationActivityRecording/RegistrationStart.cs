﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Common.RestConsumer.V2.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationStart", Namespace = "")]
    public class RegistrationStart
    {
        [DataMember(Name = "RegistrationSessionID")]
        public string RegistrationSessionID { get; set; }

        [DataMember(Name = "FormFactor")]
        public string FormFactor { get; set; }

        [DataMember(Name = "ApplicationID")]
        public int ApplicationID { get; set; }

        [DataMember(Name = "ScenarioID")]
        public int ScenarioID { get; set; }

        [DataMember(Name = "IPAddress")]
        public int IPAddress { get; set; }

        [DataMember(Name = "TimeStamp")]
        public DateTime TimeStamp { get; set; }
        
    }
}