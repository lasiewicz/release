﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationRecaptureRequest", Namespace = "")]
    public class RegistrationRecaptureRequest
    {
        [DataMember(Name = "RecaptureID")]
        public string RecaptureID { get; set; }
    }
}
