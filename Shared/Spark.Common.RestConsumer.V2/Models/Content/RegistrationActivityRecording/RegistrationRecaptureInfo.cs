﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationRecaptureInfo", Namespace = "")]
    public class RegistrationRecaptureInfo
    {
        [DataMember(Name = "RegistrationFields")]
        public Dictionary<string, string> RegistrationFields { get; set; }

        [DataMember(Name = "RecaptureID")]
        public string RecaptureID { get; set; }
    }
}
