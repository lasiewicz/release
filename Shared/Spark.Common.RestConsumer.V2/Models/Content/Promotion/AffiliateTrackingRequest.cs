﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Content.Promotion
{
    [Serializable]
    [DataContract(Name = "AffiliateTrackingRequest", Namespace = "")]
    public class AffiliateTrackingRequest
    {
        /// <summary>
        /// Absolute(full including querystring) URL of the referring site
        /// </summary>
        [DataMember(Name = "urlReferrer")]
        public String UrlReferrer { get; set; }
    }
}
