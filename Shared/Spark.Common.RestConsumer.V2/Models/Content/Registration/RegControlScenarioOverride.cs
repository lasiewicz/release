﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Content.Registration
{
    public class RegControlScenarioOverride
    {
        [DataMember(Name = "RegScenarioID")]
        public int RegScenarioID { get; set; }

        [DataMember(Name = "DisplayType")]
        public int DisplayType { get; set; }

        [DataMember(Name = "Required")]
        public bool Required { get; set; }

        [DataMember(Name = "EnableAutoAdvance")]
        public bool EnableAutoAdvance { get; set; }

        [DataMember(Name = "AdditionalText")]
        public string AdditionalText { get; set; }

        [DataMember(Name = "Label")]
        public string Label { get; set; }

        [DataMember(Name = "RequiredErrorMessage")]
        public string RequiredErrorMessage { get; set; }
    }
}
