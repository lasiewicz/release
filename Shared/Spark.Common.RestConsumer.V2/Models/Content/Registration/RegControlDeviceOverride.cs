﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Content.Registration
{
    [DataContract(Name = "RegControlDeviceOverride", Namespace = "")]
    public class RegControlDeviceOverride
    {
        [DataMember(Name = "DeviceType")]
        public DeviceType DeviceType { get; set; }

        [DataMember(Name = "AdditionalText")]
        public string AdditionalText { get; set; }

        [DataMember(Name = "Label")]
        public string Label { get; set; }

        [DataMember(Name = "RequiredErrorMessage")]
        public string RequiredErrorMessage { get; set; }
    }
}
