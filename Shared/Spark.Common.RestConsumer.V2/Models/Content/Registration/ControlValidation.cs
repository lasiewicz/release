﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Content.Registration
{
    [DataContract(Name = "ControlValidation", Namespace = "")]
    public class ControlValidation
    {
        [DataMember(Name = "Type")]
        public int Type { get; set; }

        [DataMember(Name = "Value")]
        public string Value { get; set; }

        [DataMember(Name = "ErrorMessage")]
        public string ErrorMessage { get; set; }

        [DataMember(Name = "DeviceOverrides")]
        public List<ControlValidationDeviceOverride> DeviceOverrides { get; set; }
    }
}
