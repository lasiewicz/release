﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Content.Registration
{
    [DataContract(Name = "ControlValidationDeviceOverride", Namespace = "")]
    public class ControlValidationDeviceOverride
    {
        [DataMember(Name = "DeviceType")]
        public DeviceType DeviceType { get; set; }

        [DataMember(Name = "ErrorMessage")]
        public string ErrorMessage { get; set; }
    }
}
