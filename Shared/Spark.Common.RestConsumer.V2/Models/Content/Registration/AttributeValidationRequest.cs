﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Content.Registration
{
    [DataContract(Name = "AttributeValidationRequest", Namespace = "")]
    public class AttributeValidationRequest
    {
        [DataMember(Name = "AttributeName")]
        public string AttributeName { get; set; }

        [DataMember(Name = "AttributeValue")]
        public string AttributeValue { get; set; }

        [DataMember(Name = "BrandId")]
        public int BrandId { get; set; }
    }
}
