﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Content.Registration
{
    [DataContract(Name = "ControlCustomOption", Namespace = "")]
    public class ControlCustomOption
    {
        [DataMember(Name = "Value")]
        public int Value { get; set; }

        [DataMember(Name = "ListOrder")]
        public int ListOrder { get; set; }

        [DataMember(Name = "Description")]
        public string Description { get; set; }
    }
}
