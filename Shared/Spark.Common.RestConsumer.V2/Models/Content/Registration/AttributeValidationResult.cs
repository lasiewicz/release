﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Content.Registration
{
    [DataContract(Name = "AttributeValidationResult", Namespace = "")]
    public class AttributeValidationResult
    {
        [DataMember(Name = "AttributeValueExists")]
        public bool AttributeValueExists { get; set; }
    }
}
