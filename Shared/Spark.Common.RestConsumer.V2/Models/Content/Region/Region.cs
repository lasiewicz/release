﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Content.Region
{
    [DataContract(Name = "Region", Namespace = "")]
    public class Region
    {
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "Description")]
        public string Description { get; set; }

        [DataMember(Name = "Depth")]
        public int Depth { get; set; }
    }
}

