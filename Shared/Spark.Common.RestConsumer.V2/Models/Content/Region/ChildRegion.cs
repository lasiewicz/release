﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.V2.Models.Content.Region
{
    [DataContract(Name = "ChildRegion", Namespace = "")]
    public class ChildRegion : Region
    {
    }
}

