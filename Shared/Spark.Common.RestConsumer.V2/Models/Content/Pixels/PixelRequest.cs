﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Content.Pixels
{

    [DataContract(Name = "PixelRequest")]
    public class PixelRequest
    {
        [DataMember(Name = "PageName")]
        public string PageName { get; set; }

        [DataMember(Name = "PromotionID")]
        public int PromotionID { get; set; }

        [DataMember(Name = "LanguageID")]
        public int LanguageID { get; set; }

        [DataMember(Name = "ReferralURL")]
        public string ReferralURL { get; set; }

        [DataMember(Name = "LuggageID")]
        public string LuggageID { get; set; }

        [DataMember(Name = "MemberID")]
        public int MemberID { get; set; }

        [DataMember(Name = "RegionID")]
        public int RegionID { get; set; }

        [DataMember(Name = "GenderMask")]
        public int GenderMask { get; set; }

        [DataMember(Name = "Birthdate")]
        public DateTime Birthdate { get; set; }

        [DataMember(Name = "SubscriptionAmount")]
        public string SubscriptionAmount { get; set; }

        [DataMember(Name = "SubscriptionDuration")]
        public string SubscriptionDuration { get; set; }

        public PixelRequest(string pageName, int languageID, int memberID, int regionID, int genderMask, DateTime birthdate)
        {
            PageName = pageName;
            MemberID = memberID;
            RegionID = regionID;
            GenderMask = genderMask;
            PromotionID = 0;
            LanguageID = languageID;
            Birthdate = birthdate;
        }
    }

}
