﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.V2.Models.Content.BrandConfig
{
    [DataContract(Name = "Brand", Namespace = "")]
    public class Brand : IEntity
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "site")]
        public Site Site { get; set; }

        [DataMember(Name = "uri")]
        public string Uri { get; set; }
    }
}
