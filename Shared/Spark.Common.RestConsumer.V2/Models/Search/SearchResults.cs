﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Spark.Common.RestConsumer.V2.Models.Profile;

namespace Spark.Common.RestConsumer.V2.Models.Search
{
    [Serializable]
    [DataContract(Name = "SearchResults", Namespace = "")]
    public class SearchResults
    {
        [DataMember(Name = "Members")]
        public List<MiniProfile> Members { get; set; }
    }
}
