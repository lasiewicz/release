﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using RestSharp;
using log4net;

namespace Spark.Common.RestConsumer.V2.Configuration
{
	internal class ResourceConfigReader
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(ResourceConfigReader));
		internal static ResourceConfigReader Instance = new ResourceConfigReader();

		private ResourceConfigReader()
		{
		}

		//private Dictionary<Type, Dictionary<WebMethod, string>> _resourceDictionary;
		//internal Dictionary<Type, Dictionary<WebMethod, string>> ResourceDictionary
		private Dictionary<string, Dictionary<Method, string>> _resourceDictionary;
		internal Dictionary<string, Dictionary<Method, string>> ResourceDictionary
		{
			get
			{
				if (_resourceDictionary == null)
				{
					var xmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
					                           @"RestConsumer.xml");
                    if (!File.Exists(xmlPath))
				    {
                        xmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                                                   @"bin\RestConsumer.xml");
				    }
					ResourceList resourceList;
					using (TextReader textReader = new StreamReader(xmlPath))
					{
						var deserializer = new XmlSerializer(typeof (ResourceList));
						resourceList = deserializer.Deserialize(textReader) as ResourceList;
						textReader.Close();
					}
					if (resourceList != null)
					{
						//_resourceDictionary = new Dictionary<Type, Dictionary<WebMethod, string>>();
						_resourceDictionary = new Dictionary<string, Dictionary<Method, string>>();
						foreach (var resource in resourceList.Resources)
						{
							//resource.TypeName = resource.TypeName.ToLower();
							//var obj = Activator.CreateInstance("Spark.Common.RestConsumer", resource.TypeName);
							//var resourceType = obj.Unwrap().GetType();
							var resourceDict = new Dictionary<Method, string>();
							//_resourceDictionary.Add(resource.TypeName, );
							foreach (var route in resource.Routes)
							{
								var methods = route.Methods.Split(',');
								foreach (var method in methods)
								{
									Method webMethod;
									// .net 4.0 version:
									//if (!Enum.TryParse(method, true, out webMethod))
									//{
									//    throw new Exception(
									//        String.Format("failed to parse webmethod in rest consumer XML at type {0}",
									//                      resource.TypeName));
									//}

									// .net 3.5 version:
									try
									{
										webMethod = (Method)Enum.Parse(typeof(Method), method, true);
									}
									catch (Exception ex)
									{
										Log.Error(ex);
										throw new Exception(
											String.Format("failed to parse webmethod in rest consumer XML at type {0}",
														  resource.TypeName));
									}

									if (resourceDict.ContainsKey(webMethod))
									{
										throw new Exception(String.Format("error in rest config xml - duplicate for method {0} for resource {1}", webMethod, resource.TypeName));
									}
									resourceDict.Add(webMethod, route.Path);
								}
							}
							//if (_resourceDictionary.ContainsKey(resourceType)) //resource.TypeName))
							if (_resourceDictionary.ContainsKey(resource.TypeName))
							{
								throw new Exception(String.Format("error in rest config xml - duplicate section for resource {0}", resource.TypeName));
							}
							//_resourceDictionary.Add(resourceType, resourceDict);
							_resourceDictionary.Add(resource.TypeName, resourceDict);
						}
					}

				}
				return _resourceDictionary;
			}
		}

	}
}