﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Spark.Common.RestConsumer.V2.Configuration
{
    [Serializable]
    public class ResourceList
    {
        [XmlElement("Resource")]
        public List<Resource> Resources;

    }
}
