﻿using System;
using System.Collections.Generic;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Content;

namespace Spark.Common.RestConsumer.V2
{
 
   struct CachedSetting
    {
        public string StringValue;
        public bool BoolValue;
        public DateTime CachedTime;
        public int BrandID;
    }

    public class RuntimeSettings
    {
        const int CacheSeconds = 30;

        private static readonly Dictionary<string, CachedSetting> SettingDict = new Dictionary<string, CachedSetting>();
        public static readonly RuntimeSettings Instance = new RuntimeSettings();
        private RuntimeSettings() { }

        public bool IsSettingEnabled(string settingName, int brandId)
        {
            CachedSetting cachedSetting;
            if (SettingDict.TryGetValue(string.Format("{0}-{1}",settingName, brandId), out cachedSetting))
            {
                var elapsedSeconds = (DateTime.Now - cachedSetting.CachedTime).TotalSeconds;
                if (elapsedSeconds <= CacheSeconds)
                {
                    return cachedSetting.BoolValue;
                }
            }
            // setting is missing or stale
            var urlParams = new Dictionary<string, string> {{"setting", settingName},{"brandId", brandId.ToString()}};
            ResponseDetail responseDetail;
            var settingValue = RestConsumer.Instance.Get<Setting>(urlParams, out responseDetail);
            if (settingValue != null)
            {
                SettingDict[string.Format("{0}-{1}", settingName, brandId)] = new CachedSetting { CachedTime = DateTime.Now, BoolValue = settingValue.IsEnabled, BrandID = brandId };
                return settingValue.IsEnabled;
            }
            return false;
        }

        public string GetSettingValue(string settingName, int brandId)
        {
            CachedSetting cachedSetting;
            if (SettingDict.TryGetValue(string.Format("{0}-{1}", settingName, brandId), out cachedSetting))
            {
                var elapsedSeconds = (DateTime.Now - cachedSetting.CachedTime).TotalSeconds;
                if (elapsedSeconds <= CacheSeconds)
                {
                    return cachedSetting.StringValue;
                }
            }
            // setting is missing or stale
            var urlParams = new Dictionary<string, string> { { "setting", settingName }, { "brandId", brandId.ToString() } };
            ResponseDetail responseDetail;
            var settingValue = RestConsumer.Instance.Get<SettingValue>(urlParams, out responseDetail);
            if (settingValue != null)
            {
                SettingDict[string.Format("{0}-{1}", settingName, brandId)] = new CachedSetting { CachedTime = DateTime.Now, StringValue = settingValue.Value, BrandID = brandId};
                return settingValue.Value;
            }
            return null;
        }
    }
}
 
