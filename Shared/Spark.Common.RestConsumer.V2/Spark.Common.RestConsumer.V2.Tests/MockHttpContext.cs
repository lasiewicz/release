﻿using System.Collections;
using System.Web;

namespace Spark.Common.RestConsumer.V2.Tests
{
    public class MockHttpContext : HttpContextBase
    {
        private HttpRequestBase _httpRequestBase;
        private HttpResponseBase _httpResponseBase;
        private IDictionary _items = new Hashtable();
        public MockHttpContext(HttpRequestBase httpRequestBase, HttpResponseBase httpResponseBase)
        {
            _httpRequestBase = httpRequestBase;
            _httpResponseBase = httpResponseBase;
        }

        public override HttpRequestBase Request
        {
            get { return _httpRequestBase; }
        }

        public override HttpResponseBase Response
        {
            get { return _httpResponseBase; }
        }

        public override IDictionary Items
        {
            get { return _items; }
        }
    }
}