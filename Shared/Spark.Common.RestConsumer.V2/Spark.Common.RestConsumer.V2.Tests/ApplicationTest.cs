﻿using System;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Applications;

namespace Spark.Common.RestConsumer.V2.Tests
{

	/// <summary>
	///This is a test class for ProfileTest and is intended
	///to contain all ProfileTest Unit Tests
	///</summary>
	[TestFixture]
	public class ApplicationTest
	{
		private const string brandHost = "jdate.com";
		private static Uri appUri = null;
	    private static int appId;
	    private static string appSecret;
		const int testUserId1 = 100170050;


	    private const int devAppId = 1054;
        const string devAppSecret = "nZGVVfj8dfaBPKsx_dmcRXQml8o5N-iivf5lBkrAmLQ1";

		// mobiletest101 100170050
		// mobiletest102 111644211
		// mobiletest103 100170023
		// mobiletest104 111644214
		// mobiletest105 108009966


        // 1. Make sure to change settings in App.config accordingly to point to the right environment.
        //      1.1 refer to this project's app.config
        // 2. on the rest server you're hitting, modify web.config -> oauthrequired -> false and change back. todo:it should require oauth..        
		[Test]
		public void CreateAppTest()
		{
		    ResponseDetail responseDetail;
			var appResource = RestConsumer.Instance.Post<ResourceResult, Application>(null, CreateAppObject(), out responseDetail); 

			Assert.IsNotNull(appResource, "app resource");
			Assert.IsNotNull(appResource.Resource, "app resource.resource");
			Assert.IsTrue(Uri.IsWellFormedUriString(appResource.Resource, UriKind.Absolute), "invalid uri returned for app resource");
		    Assert.IsTrue(Uri.TryCreate(appResource.Resource, UriKind.Absolute, out appUri));

		    var path = appUri.AbsolutePath;
		    var appIdStr = path.Substring(path.LastIndexOf('/') + 1);
            Assert.IsTrue(Int32.TryParse(appIdStr, out appId));
            Assert.IsTrue(appId > 0, "app id was 0");
            var qs = appUri.Query;
            appSecret = qs.Substring(qs.LastIndexOf('=') + 1); // assumes secret is last qs param
            // http://rest.local.jdate.com/apps/application/1025?client_id=1025&client_secret=kcVQr69yB7NqEtMHPdVPqYntGR-Xenh1JYyvZ1G8elk1
		}

	    private Application CreateAppObject()
	    {
	        return new Application
	        {
	            BrandId = 1003,// Change here
	            OwnerMemberId = 27029711,// Change here
	            ClientSecret = Security.Encryption.EncryptBytes(Security.Encryption.GetCryptoBytes(15)),
                Title = "JDate Android App", // Change here
                Description = "Android optimized app for JDate circa 2015",// Change here
                RedirectUrl = "http://www.jdate.com" //Change here
	        };
	    }

        [Test]
	    public void TestCreateAppObject()
	    {
	        Application appObject = CreateAppObject();
            Console.WriteLine(string.Format("BrandId:{0}, OwnerId:{1}, ClientSecret='{2}', Title:'{3}', Description='{4}'",appObject.BrandId, appObject.OwnerMemberId,appObject.ClientSecret, appObject.Title, appObject.Description));
	    }

	    [Test] // must run a create app test first to generate app id
		public void GetNewlyCreatedAppTest()
	    {
	        ResponseDetail responseDetail;
			var urlParams = new Dictionary<string, string> { { "applicationId", appId.ToString() } }; 
			var queryStringParams = new Dictionary<string, string> { { "client_secret", appSecret } };
            var app = RestConsumer.Instance.Get<Application>(urlParams, out responseDetail, queryStringParams);
			Assert.IsNotNull(app, "app");
		}

        [Test] // must run a create app test first to generate app id
        public void GetAppTest()
        {
            ResponseDetail responseDetail;
            var urlParams = new Dictionary<string, string> { { "applicationId", "1039" } };
            var queryStringParams = new Dictionary<string, string> { { "client_secret", "mynewsecret" } };
            var app = RestConsumer.Instance.Get<Application>(urlParams, out responseDetail, queryStringParams);
            Assert.IsNotNull(app, "app");
        }

        [Test] // must run a create app test first to generate app id
        public void UpdateAppTest()
        {
            ResponseDetail responseDetail;
            var urlParams = new Dictionary<string, string> { { "applicationId", "1039" } };
            var queryStringParams = new Dictionary<string, string> { { "client_secret", "mynewsecret" } };
            var app = RestConsumer.Instance.Get<Application>(urlParams, out responseDetail, queryStringParams);
            Assert.IsNotNull(app, "null app");

            app.Title = "unit test app - updated";
            app.Description = "app updated by a unit test";

            ResponseDetail responseDetail2;
            RestConsumer.Instance.Put<Application>(urlParams, app, out responseDetail2, queryStringParams);
            Assert.IsNotNull(app, "app");
        }

        [Test] // must run a create app test first to generate app id
        public void DeleteAppTest()
        {
            ResponseDetail responseDetail;
            var urlParams = new Dictionary<string, string> { { "applicationId", appId.ToString() } };
            var queryStringParams = new Dictionary<string, string> { { "client_secret", appSecret} };
            var app = RestConsumer.Instance.Get<Application>(urlParams, out responseDetail, queryStringParams);
            Assert.IsNotNull(app, "app was null before deletion");
            Assert.AreNotEqual(0, app.ApplicationId, "app was mssing before deletion");

            ResponseDetail responseDetail2;
            RestConsumer.Instance.Delete<Application>(urlParams, out responseDetail2, queryStringParams);

            try
            {
                ResponseDetail responseDetail3;
                app = RestConsumer.Instance.Get<Application>(urlParams, out responseDetail3, queryStringParams);
                Assert.IsNotNull(app, "null is returned for deleted app");
                Assert.AreEqual(0, app.ApplicationId, "app exists after deletion");
            }
            catch
            {
                // an exception here means we failed to get the app after deleting, which is probably fine
            }
        }
    
    }
}
