﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.MembersOnline;

namespace Spark.Common.RestConsumer.V2.Tests
{
    [TestFixture]
    public class OAuthConsumerTest
    {
        private OAuthConsumer oAuthConsumer;
        private int memberId=100004579;
        private string accessToken = "2/vyley0qO2kIYmYdtUfWrSNHucvrYwPqwPeYtYYOaQjM=";
        private int expiresIn = 12000;

        [TestFixtureSetUp]
        public void Setup()
        {
            oAuthConsumer = new OAuthConsumer(memberId, accessToken, DateTime.Now.AddSeconds(expiresIn), 1003, GetMockHttpContext());
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            oAuthConsumer = null;
        }

        [Test]
        public void TestMakeMemberOnlineAdd()
        {
            ResponseDetail responseDetail;
            oAuthConsumer.Post<MembersOnlineRequest>(null, out responseDetail);
        }

        [Test]
        public void TestMakeMemberOnlineRemove()
        {
            ResponseDetail responseDetail;
            oAuthConsumer.Delete<MembersOnlineRequest>(null, out responseDetail);            
        }

        private HttpContextBase GetMockHttpContext()
        {
            string urlHost = "http://api.local.spark.net";
            string urlPath = "/v2/brandId/1003/profile/miniProfile/0";
            string queryString = "access_token=1%2FF6BgNEpHyz%2BAjgjTR9An8X7E8%2F16DA3FeqgVbYF%2BIe4%";
            var stringWriter = new StringWriter();
            HttpResponse httpResponse = new HttpResponse(stringWriter);
            httpResponse.StatusCode = (int)HttpStatusCode.OK;
            string originIp = "192.168.1.141";
            var mockHttpContext = MockHttpContextFactory.GetMockHttpContext(urlHost + urlPath, queryString, originIp,
                new HttpResponseWrapper(httpResponse));
            return mockHttpContext;
        }

    }
}
