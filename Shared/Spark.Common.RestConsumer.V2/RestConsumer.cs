﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;
using Spark.Common.RestConsumer.V2.Configuration;
using Spark.Common.RestConsumer.V2.Models;
using log4net;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.V2
{
    public class RestConsumer : IRestConsumer
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RestConsumer));
        // Not a static variable to support multiton
        private string _brandId;
        const string BrandIdKey = "brandId";

        // maps Types (i.e. ViewProfile) and WebMethods (GET, POST, etc.) to URLs on the REST server user to access the given resources
    	protected Dictionary<Type, Dictionary<Method, string>> RequestMap = new Dictionary<Type, Dictionary<Method, string>>();
		protected static Dictionary<string, Dictionary<Method, string>> ResourceMap;
        // Need to deprecate once multiton is fully implemented.
        public static readonly RestConsumer Instance = new RestConsumer();
        // Multiton pattern
        private static readonly Dictionary<int, RestConsumer> Instances = new Dictionary<int, RestConsumer>();

        protected HttpContextBase HttpContext
        {
            get
            {
                if (System.Web.HttpContext.Current == null) return null;
                var context = new HttpContextWrapper(System.Web.HttpContext.Current);
                return context;
            }
        }



        private RestConsumer()
        {
            try
            {
                ResourceMap = ResourceConfigReader.Instance.ResourceDictionary;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("failed to read rest consumer configuration\n {0}", ex);
                throw;
            }

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["BrandId"]))
            {
                _brandId = ConfigurationManager.AppSettings["BrandId"];
                Log.DebugFormat("Created a new Rest Consumer instance for brand :{0}", _brandId);
            }
            else
            {
                Log.ErrorFormat("Missing BrandId in config.");
            }
        }

        /// <summary>
        /// Multiton pattern to support unique instances of RestConsumer
        /// </summary>
        /// <param name="brandId"></param>
        /// <returns></returns>
        public static RestConsumer GetInstance(int brandId)
        {
            lock (Instances)
            {
                RestConsumer instance;
                if (!Instances.TryGetValue(brandId, out instance))
                {
                    instance = new RestConsumer {_brandId = brandId.ToString()};
                    Instances.Add(brandId, instance);
                    Log.DebugFormat("Created a new Rest Consumer instance for brand :{0}", brandId);
                }
                return instance;
            }
        }


        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
                                                               {
                                                                   MissingMemberHandling = MissingMemberHandling.Ignore,
                                                                   NullValueHandling = NullValueHandling.Include,
                                                                   DefaultValueHandling = DefaultValueHandling.Include,
                                                               };

		/// <summary>
		/// Given a type (ViewProfile) and WebMEthod (GET, POST, etc.), and url Params, returns a corresponding REST URL used to access the recources,
		/// with the URL params mapped into the URL.  
		/// </summary>
		/// <param name="dataRequestType">Type of object that needs to be fetched, updated, created</param>
		/// <param name="webMethod">operation to be done on the object</param>
		/// <param name="urlParamDictionary">params such as IDs, needed for the REST resource URL</param>
		/// <returns>a URL to the REST resource</returns>
        protected virtual string GetResourcePath(Type dataRequestType, Method webMethod, Dictionary<string, string> urlParamDictionary, HttpRequestBase httpRequest = null)
        {
            // get the resources for this Type (i.e. MiniProfile)
            Dictionary<Method, string> methodResourceDict;

            var className = dataRequestType.ToString();
//            if (!ResourceMap.TryGetValue(dataRequestType, out methodResourceDict) || methodResourceDict == null || methodResourceDict.Count == 0)
			if (!ResourceMap.TryGetValue(className, out methodResourceDict) || methodResourceDict == null || methodResourceDict.Count == 0)
            {
                throw new Exception(String.Format("No resources found for type {0}", className));
            }

            // get the list resources for the webmethod (GET, POST, etc.) for this Type
            string resource;


            if (!methodResourceDict.TryGetValue(webMethod, out resource))
            {
                throw new Exception(String.Format("No resource found for web method {0} for type {1}", webMethod, className));
            }


			var path = resource;
			if (urlParamDictionary == null)
			{
				urlParamDictionary = new Dictionary<string, string>();
			}

            if (!urlParamDictionary.ContainsKey(BrandIdKey))
            {
                urlParamDictionary.Add(BrandIdKey, _brandId);
            }

			foreach (var pair in urlParamDictionary)
			{
				var key = '{' + pair.Key + '}';
				path = path.Replace(key, pair.Value);
			}
			// todo: throw exception if there were expected params that were not given values in the urlParamDictionary
			// also, throw exception if there were extra params that weren't used in the URL
			return path;
        }

        //private static void AddVersionHeader(RestBase request)
        //{
        //    const string versionHeader = "version=V2.1";
        //    var headers = request.GetAllHeaders();
        //    var accept = headers["Accept"];
        //    if (accept == null)
        //    {
        //        try
        //        {
        //            request.AddHeader("Accept", versionHeader);
        //        }
        //        catch (Exception ex)
        //        {
        //            Log.Error(ex);
        //        }
        //    }
        //    else
        //    {
        //        accept.Value = accept.Value + ';' + versionHeader;
        //    }
        //}


		private static RestClient CreateJsonRestClient()
		{
            var client = new RestClient(ConfigurationManager.AppSettings["RESTAuthority"]);
            client.AddHandler("application/json", new SparkRestSharpSerializer(Settings));
            client.Timeout = new TimeSpan(0, 0, Convert.ToInt16(ConfigurationManager.AppSettings["RestClientTimeOutSecs"])).Milliseconds;
            //client.RetryPolicy = new RetryPolicy { RetryCount = 0 }; //RestSharp does not support retries
            return client;
		}

        /// <summary>
        /// For GET/DELETE
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="urlParamDict"></param>
        /// <param name="webMethod"></param>
        /// <param name="responseDetail"></param>
        /// <param name="queryStringParams"></param>
        /// <returns></returns>
        private TResponse MakeGetOrDeleteRequest<TResponse>(Dictionary<string, string> urlParamDict, Method webMethod, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams, HttpRequestBase httpRequest) where TResponse : class 
        {
            var result = MakeRequest<TResponse, RestResponse>(urlParamDict, webMethod, null, out responseDetail, queryStringParams, httpRequest);
            return result;
        }

        private RestResponse MakePutOrPostRequestNoReturn<TRequest>(Dictionary<string, string> urlParamDict, Method webMethod, TRequest entity, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams, HttpRequestBase httpRequest)
        {
		    var restResponse = MakeRequest<RestResponse, TRequest>(urlParamDict, webMethod, entity, out responseDetail, queryStringParams, httpRequest);
			return restResponse;
        }

        /// <summary>
        /// POST or PUT entity object, return object of type T
        /// </summary>
        /// <typeparam name="TResponse">Type of return object to expect</typeparam>
        /// <typeparam name="TRequest">Type of entity to be serialized into post/put content body</typeparam>
        /// <param name="urlParamDict">URL varaibles will be matched with keys and replaced with corresponding values.</param>
        /// <param name="webMethod">POST, PUT, GET, or DELETE</param>
        /// <param name="entity">entity to be serialized into post/put content body</param>
        /// <param name="responseDetail"></param>
        /// <param name="queryStringParams"></param>
        /// <returns></returns>
        private TResponse MakeRequest<TResponse, TRequest>(Dictionary<string, string> urlParamDict, Method webMethod, TRequest entity, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null) where TResponse : class
        {
            responseDetail = new ResponseDetail();
    		var client = CreateJsonRestClient();
           
            if (client == null)
            {
                throw new Exception("Could not create a Json Rest Client");
            }
            if (httpRequest != null && httpRequest.UserAgent != null)
            {
                    client.UserAgent = httpRequest.UserAgent;
            }
            var request = new RestRequest
            {
                Method = webMethod,
                Resource = String.Empty,
                JsonSerializer = new SparkRestSharpSerializer(Settings),
                RequestFormat = DataFormat.Json
            };
            request.AddHeader("Accept", "application/json; version=V2.1");
            request.AddHeader("Content-Type", "application/json; charset=utf-8");

            // add all the custom headers after this 
            

            if (httpRequest != null && httpRequest.Headers.Count > 0) 
            {
                var originIP = httpRequest.Headers.Get("X-Spark-OriginIP");
                if (originIP != null)
                    request.AddHeader("X-Spark-OriginIP", originIP);
            }
            else 
            {
                string remoteHost = string.Empty;
                if (HttpContext != null && HttpContext.Request != null)
                {
                    remoteHost = HttpContext.Request.Headers["client-ip"] ?? HttpContext.Request.ServerVariables["REMOTE_HOST"];
                }
                
                if (remoteHost != null)
                    request.AddHeader("X-Spark-OriginIP", remoteHost);

            }
            

            if (Log.IsDebugEnabled)
            {
                if (!EqualityComparer<TRequest>.Default.Equals(entity, default(TRequest)))
                {
                    var bodyJSON = request.JsonSerializer.Serialize(entity);
                    if (bodyJSON != null && bodyJSON.IndexOf("string", StringComparison.OrdinalIgnoreCase) == -1)
                    {
                        Log.DebugFormat("request body: {0}", bodyJSON);
                    }
                }
            }

            switch (webMethod)
            {
                case Method.POST:
                case Method.PUT:
                    request.AddBody(entity);
                    request.Resource = GetResourcePath(typeof(TRequest), webMethod, urlParamDict);
                        // if POST (or PUT), the resource whose path we want is the object being POSTed, not the return object
                    // i.e. if POSTing a Mail message, lookup up the URL for a Mail message
                    break;
                case Method.DELETE:
                    request.AddBody(entity);
                    request.Resource = GetResourcePath(typeof(TRequest), webMethod, urlParamDict);
                        // if POST (or PUT), the resource whose path we want is the object being POSTed, not the return object
                    // i.e. if POSTing a Mail message, lookup up the URL for a Mail message
                    break;
                case Method.GET:
                    request.Resource = GetResourcePath(typeof(TResponse), webMethod, urlParamDict);
                        // if GET, look up the resource for the type of object being *returned*
                    break;
            }
            if (queryStringParams == null)
            {
                queryStringParams = new Dictionary<string, string>();
            }
            if (queryStringParams.Count > 0)
            {
                var builder = new StringBuilder(request.Resource.Contains("?") ? "&" : "?");
                foreach (var qs in queryStringParams)
                {
                    builder.AppendFormat("{0}={1}&", qs.Key, qs.Value);
                }
                builder.Remove(builder.Length -1, 1);
                request.Resource += builder.ToString();
            }
            var fullRequestURL = client.BaseUrl + request.Resource;
            RestResponse<ResponseWrapper<TResponse>> restResponseT = null;
            RestResponse restResponse = null;
			if (typeof(TResponse) == (typeof(RestResponse)))
			{
				try
				{
                    Log.InfoFormat("REST request using RestSharp.  Method: {0} Path: {1} Entity: {2}", webMethod, fullRequestURL, entity);
                    //AddVersionHeader(request);
                    restResponse = client.Execute(request) as RestResponse;
                    if (restResponse != null)
                    {
                        var newHeaders = restResponse.Headers;
                        Log.Debug(newHeaders);
                    }
				}
				catch (Exception ex)
				{
					string errorDetail;
					if (restResponse == null)
					{
                        errorDetail = String.Format("Error making a REST call.\n Method: {0} \nPath: {1} \nEntity: {2}", webMethod, fullRequestURL, entity);
					}
					else
					{
                        errorDetail = String.Format("Error making a REST call.\n Method: {0} \nPath: {1} \nEntity: {2}\n Response code: {3}\n desc: {4}\n Content: {5}", webMethod, fullRequestURL, entity, restResponse.StatusCode, restResponse.StatusDescription, restResponse.Content);
					}
                    Log.ErrorFormat("Failed to make REST call.  Method: {0} Path: {1} Entity: {2} Exception:\n{3}", webMethod, fullRequestURL, entity, ex);

					throw new Exception(errorDetail, ex);
				}
				return restResponse as TResponse;
			}

            try
            {
                Log.InfoFormat("Making rest request.  Method: {0} Path: {1} Entity: {2}", webMethod, fullRequestURL, entity);
                //AddVersionHeader(request);
                restResponseT = client.Execute<ResponseWrapper<TResponse>>(request) as RestResponse<ResponseWrapper<TResponse>>;
            }
            catch (Exception ex)
            {
                string errorDetail;
                if (restResponseT == null)
                {
                    errorDetail = String.Format("Error making a REST call.\n Method: {0} \nPath: {1} \nEntity: {2}", fullRequestURL, webMethod, entity);
                }
                else
                {
                    errorDetail = String.Format("Error making a REST call.\n Method: {0} \nPath: {1} \nEntity: {2}\n Response code: {3}\n desc: {4}\n Content: {5}", fullRequestURL, webMethod, entity, restResponseT.StatusCode, restResponseT.StatusDescription, restResponseT.Content);
                }
                Log.ErrorFormat("Failed to make REST call.  Method: {0} Path: {1} Entity: {2} Exception:\n{3}", webMethod, fullRequestURL, entity, ex);
                throw new Exception(errorDetail, ex);
            }

            if (restResponseT.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Log.WarnFormat("Failure in server request, code: {0} description: {1}", restResponseT.StatusCode, restResponseT.StatusDescription);

                if (restResponseT.Content != null && restResponseT.Content.IndexOf("UniqueId", StringComparison.Ordinal) != -1)
                {
                    var token = JObject.Parse(restResponseT.Content);
                    Log.ErrorFormat("UniqueId: {0} Message: {1}", token.SelectToken("UniqueId"),
                                    token.SelectToken("Message"));
                }

                if (restResponseT.StatusCode == 0)
                {
                    Log.WarnFormat("restResponseT errormessage: {0} innerException: {1} number of attempts: {2}", 
                        restResponseT.ErrorMessage,restResponseT.ErrorException, request.Attempts);
                }

                // Was null checking for restResponseT.Data before RestSharp
                if (restResponseT.Data == null)
                {
                    var restAuthority = ConfigurationManager.AppSettings["RESTAuthority"];
                    throw new Exception(String.Format("Could not communicate with API at {0}", restAuthority));
                }

                if (restResponseT.Data.Data != null)
                {
                    if (Log.IsDebugEnabled)
                    {
                        var bodyJSON = request.JsonSerializer.Serialize(restResponseT.Data.Data);
                        Log.DebugFormat("response body: {0}", bodyJSON);
                    }
                    return restResponseT.Data.Data;
                }
                
                if (restResponseT.Content != null)
                {
                    var jObj = JObject.Parse(restResponseT.Content);
                    if (jObj == null)
                    {
                        return null;
                    }
                    var result = jObj;
                    if (jObj["Result"] != null)
                    {
                        result = (JObject)jObj["Result"];
                    }
                    var error = result["error"];
                    responseDetail = new ResponseDetail
                        {
                            Code = (int)result["code"],
                            Status = (string)result["status"],
                            ErrorCode = (int)error["code"],
                            ErrorMessage = (string)error["message"]
                        };
                    return null;

                }
            }
            if (restResponseT.StatusCode == System.Net.HttpStatusCode.OK)
            {
				Log.DebugFormat("returning object {0}", restResponseT.Data);
                if (restResponseT.Data == null)
                {
                    throw new Exception("Missing content entity");
                }
                return restResponseT.Data.Data;
            }

            return default(TResponse);
        }

        /// <summary>
        /// Get an object by its resource ID
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <returns>an object of type T</returns>
        public virtual TResponse Get<TResponse>(Dictionary<string, string> urlParamDict, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null) where TResponse : class
        {
            return MakeGetOrDeleteRequest<TResponse>(urlParamDict, Method.GET, out responseDetail, queryStringParams, httpRequest);
        }

		/// <summary>
		/// Delete an object by its resource ID
		/// </summary>
		/// <typeparam name="TRequest">Type of object to delete</typeparam>
		/// <returns></returns>
		public virtual RestResponse Delete<TRequest>(Dictionary<string, string> urlParamDict, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null) where TRequest : class
		{
		    return MakeRequest<RestResponse, TRequest>(urlParamDict, Method.DELETE, null, out responseDetail, queryStringParams, httpRequest);
		}

        /// <summary>
        /// POST (create) an entity object of type E
        /// </summary>
        /// <typeparam name="TRequest">type of the object</typeparam>
        /// <param name="entity"></param>
        /// <param name="urlParamDict"></param>
        /// <param name="responseDetail"></param>
        /// <param name="queryStringParams"></param>
        public virtual RestResponse Post<TRequest>(TRequest entity, Dictionary<string, string> urlParamDict, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null)
        {
            return MakePutOrPostRequestNoReturn(urlParamDict, Method.POST, entity, out responseDetail, queryStringParams, httpRequest);
        }

        /// <summary>
        /// update object at given resource ID
        /// </summary>
        /// <typeparam name="E">type of the object to udpate</typeparam>
        /// <param name="urlParamDict">URL varaibles will be matched with keys and replaced with corresponding values.</param>
        /// <param name="entity">the new object, which will replace the old object</param>
        /// <param name="responseDetail"></param>
        /// <param name="queryStringParams"></param>
        public virtual RestResponse Put<E>(Dictionary<string, string> urlParamDict, E entity, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null)
        {
            return MakePutOrPostRequestNoReturn(urlParamDict, Method.PUT, entity, out responseDetail, queryStringParams, httpRequest);
        }

        /// <summary>
        /// Create object of type E.  Also returns an object of type T
        /// </summary>
        /// <typeparam name="TResponse">Type of object to create</typeparam>
        /// <typeparam name="TRequest">Type of object to be returned</typeparam>
        /// <param name="urlParamDict"></param>
        /// <param name="entity">object to be created</param>
        /// <param name="responseDetail"></param>
        /// <param name="queryStringParams"></param>
        /// <returns>object of type T</returns>
        public virtual TResponse Post<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null) where TResponse : class
        {
            return MakeRequest<TResponse, TRequest>(urlParamDict, Method.POST, entity, out responseDetail, queryStringParams, httpRequest);
        }

        public virtual TResponse Delete<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null) where TResponse : class
        {
            ResponseDetail responseDetail;
            return MakeRequest<TResponse, TRequest>(urlParamDict, Method.DELETE, entity, out responseDetail, queryStringParams, httpRequest);
        }

        /// <summary>
        /// Update object of type E.  Also returns an object of type T
        /// </summary>
        /// <typeparam name="TResponse">Type of object to update</typeparam>
        /// <typeparam name="TRequest">Type of object to be returned</typeparam>
        /// <param name="urlParamDict">URL varaibles will be matched with keys and replaced with corresponding values.</param>
        /// <param name="entity">object to be created</param>
        /// <param name="responseDetail"></param>
        /// <param name="queryStringParams"></param>
        /// <returns>object of type T</returns>
        public virtual TResponse Put<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null) where TResponse : class
        {
            return MakeRequest<TResponse, TRequest>(urlParamDict, Method.PUT, entity, out responseDetail, queryStringParams, httpRequest);
        }

        public virtual RestResponse Put<TRequest>(TRequest entity, Dictionary<string, string> urlParamDict, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null)
        {
            return MakePutOrPostRequestNoReturn(urlParamDict, Method.PUT, entity, out responseDetail, queryStringParams, httpRequest);
        }

        //private TReply MakeRestCall<TReply, TRequest>
        //    (TRequest request, String basePath, List<String> segments, Method webMethod) where TReply : class 
        //{
        //    var client = CreateJsonRestClient();
        //    if (client == null)
        //    {
        //        throw new Exception("Could not create a Json Rest Client");
        //    }
        //    var restRequest = new RestRequest { Method = webMethod };
        //    var path = String.Format(basePath, segments.ToArray());
        //    restRequest.Resource = path;

        //    RestResponse<TReply> restResponseT;
            
        //    if (typeof(TReply) == (typeof(RestResponse)))
        //    {
        //        var restResponse = client.Execute(restRequest);
        //        return restResponse as TReply;
        //    }
        //    else
        //    {
        //        restResponseT = client.Execute<TReply>(restRequest) as TReply;
        //    }
        //    if (restResponseT.StatusCode == System.Net.HttpStatusCode.OK)
        //    {
        //        return restResponseT.Data;
        //    }

        //    return default(TReply);
        //}



		//public RestResponse Post<TRequest>(TRequest entity, Dictionary<string, string> queryStringParams = null)
		//{
		//    throw new NotImplementedException();
		//}
	}  
}
