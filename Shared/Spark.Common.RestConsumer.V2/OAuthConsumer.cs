﻿#region

using System;
using System.Collections.Generic;
using System.Web;
using RestSharp;
using Spark.Common.RestConsumer.V2.Models;

#endregion

namespace Spark.Common.RestConsumer.V2
{
    /// <summary>
    ///     Adapter Pattern.  The wrapper handles oauth validation, including refreshing the token before passing request to OAuthConsumer
    ///     Deprecate this if then else shorthands once the multiton pattern has been implemented in all rest consumer clients.
    /// </summary>
    public class OAuthConsumer : IRestConsumer
    {
        private int _brandId;

        // Deprecate this constructor once the multiton pattern has been implemented in all rest consumer clients.
        public OAuthConsumer(int memberId, string accessToken, DateTime accessExpires)
        {
            Initialize(memberId, accessToken, accessExpires, int.MinValue);
        }

        public OAuthConsumer(int memberId, string accessToken, DateTime accessExpires, int brandId)
        {
            Initialize(memberId, accessToken, accessExpires, brandId);
        }

        public OAuthConsumer(int memberId, string accessToken, DateTime accessExpires, int brandId, System.Web.HttpContextBase httpContext)
        {
            Initialize(memberId, accessToken, accessExpires, brandId, httpContext);
        }

        public int LoggedInMemberId { get; set; }
        private string AccessToken { get; set; }
        private DateTime AccessExpires { get; set; }
        public HttpContextBase HttpContext { get; set; }

        #region IRestConsumer Members

        public TResponse Delete<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity,
                                                     Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null)
            where TResponse : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? V2.RestConsumer.Instance.Delete<TResponse, TRequest>(urlParamDict, entity, queryStringParams, httpRequest)
                       : V2.RestConsumer.GetInstance(_brandId).Delete<TResponse, TRequest>(urlParamDict, entity,
                                                                                        queryStringParams, httpRequest);
        }


        public TResponse Get<TResponse>(Dictionary<string, string> urlParamDict, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null) where TResponse : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? V2.RestConsumer.Instance.Get<TResponse>(urlParamDict, out responseDetail, queryStringParams, httpRequest)
                       : V2.RestConsumer.GetInstance(_brandId).Get<TResponse>(urlParamDict, out responseDetail, queryStringParams, httpRequest);
        }


        public TResponse Post<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null)
            where TResponse : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? V2.RestConsumer.Instance.Post<TResponse, TRequest>(urlParamDict, entity, out responseDetail, queryStringParams, httpRequest)
                       : V2.RestConsumer.GetInstance(_brandId).Post<TResponse, TRequest>(urlParamDict, entity, out responseDetail,
                                                                                      queryStringParams, httpRequest);
        }

        public RestResponse Put<TRequest>(TRequest entity, Dictionary<string, string> urlParamDict, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null)
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? V2.RestConsumer.Instance.Put(entity, urlParamDict, out responseDetail, queryStringParams, httpRequest)
                       : V2.RestConsumer.GetInstance(_brandId).Put(entity, urlParamDict, out responseDetail, queryStringParams, httpRequest);
        }

        public TResponse Put<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null)
            where TResponse : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? V2.RestConsumer.Instance.Put<TResponse, TRequest>(urlParamDict, entity, out responseDetail, queryStringParams, httpRequest)
                       : V2.RestConsumer.GetInstance(_brandId).Put<TResponse, TRequest>(urlParamDict, entity, out responseDetail,
                                                                                     queryStringParams, httpRequest);
        }

        public RestResponse Put<TRequest>(Dictionary<string, string> urlParamDict, TRequest entity, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null)
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? V2.RestConsumer.Instance.Put(urlParamDict, entity, out responseDetail, queryStringParams, httpRequest)
                       : V2.RestConsumer.GetInstance(_brandId).Put(urlParamDict, entity, out responseDetail, queryStringParams, httpRequest);
        }

        public RestResponse Delete<TRequest>(Dictionary<string, string> urlParamDict, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null) where TRequest : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? V2.RestConsumer.Instance.Delete<TRequest>(urlParamDict, out responseDetail, queryStringParams, httpRequest)
                       : V2.RestConsumer.GetInstance(_brandId).Delete<TRequest>(urlParamDict, out responseDetail, queryStringParams, httpRequest);
        }

        #endregion

        private void Initialize(int memberId, string accessToken, DateTime accessExpires, int brandId, HttpContextBase httpContext = null)
        {
            LoggedInMemberId = memberId;
            AccessToken = accessToken;
            AccessExpires = accessExpires;
            _brandId = brandId;
            HttpContext = httpContext;
        }


        private void DecorateOauthParams(IDictionary<string, string> urlParamDict,
                                         IDictionary<string, string> queryStringParams)
        {
            //var tokens = LoginManager.GetTokensFromCookies();
            if (LoggedInMemberId < 1 || String.IsNullOrEmpty(AccessToken))
            {
                throw new Exception("Missing OAuth tokens");
            }
            //AccessToken = tokens.AccessToken;
            //AccessExpires = DateTime.Now.AddSeconds(tokens.ExpiresIn);
            if (AccessExpires == DateTime.MinValue)
            {
                throw new Exception("Invalid time for Access Expires cookie");
            }
            if (AccessExpires < DateTime.Now.AddSeconds(30))
                // if access token will expires within a minute, get a new one
            {
                throw new Exception(String.Format("access token expiring, should have been refreshed: {0}",
                                                  AccessExpires));
            }
            queryStringParams.Add("access_token", AccessToken);

            if (LoggedInMemberId > 0)
            {
                urlParamDict["memberId"] = LoggedInMemberId.ToString();
            }
        }

        /// <summary>
        ///     POST (create) an entity object of type E
        /// </summary>
        /// <typeparam name = "TRequest">type of the object</typeparam>
        /// <param name = "entity"></param>
        /// <param name="responseDetail"></param>
        /// <param name = "queryStringParams"></param>
        public virtual RestResponse Post<TRequest>(TRequest entity, out ResponseDetail responseDetail, Dictionary<string, string> queryStringParams = null, HttpRequestBase httpRequest = null)
        {
            var urlParamDict = new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? V2.RestConsumer.Instance.Post(entity, urlParamDict, out responseDetail, queryStringParams, httpRequest)
                       : V2.RestConsumer.GetInstance(_brandId).Post(entity, urlParamDict, out responseDetail, queryStringParams, httpRequest);
        }
    }
}