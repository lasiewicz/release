﻿using System;
using System.IO;
using System.Text;
//using Hammock;
//using Hammock.Serialization;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer
{
    //internal class SparkSerializer : ISerializer, IDeserializer
    //{
    //    private readonly JsonSerializer _serializer;

    //    public SparkSerializer(JsonSerializerSettings settings)
    //    {
    //        _serializer = new JsonSerializer
    //                        {
    //                            ConstructorHandling = settings.ConstructorHandling,
    //                            ContractResolver = settings.ContractResolver,
    //                            ObjectCreationHandling = settings.ObjectCreationHandling,
    //                            MissingMemberHandling = settings.MissingMemberHandling,
    //                            DefaultValueHandling = settings.DefaultValueHandling,
    //                            NullValueHandling = settings.NullValueHandling
    //                        };

    //        foreach (var converter in settings.Converters)
    //        {
    //            _serializer.Converters.Add(converter);
    //        }
    //    }

    //    public virtual object Deserialize(string content, Type type)
    //    {
    //        using (var stringReader = new StringReader(content))
    //        {
    //            using (var jsonTextReader = new JsonTextReader(stringReader))
    //            {
    //                return _serializer.Deserialize(jsonTextReader, type);
    //            }
    //        }
    //    }

    //    public virtual T Deserialize<T>(string content)
    //    {
    //        using (var stringReader = new StringReader(content))
    //        {
    //            using (var jsonTextReader = new JsonTextReader(stringReader))
    //            {
    //                return _serializer.Deserialize<T>(jsonTextReader);
    //            }
    //        }
    //    }

    //    public virtual string Serialize(object instance, Type type)
    //    {
    //        using (var stringWriter = new StringWriter())
    //        {
    //            using (var jsonTextWriter = new JsonTextWriter(stringWriter))
    //            {
    //                jsonTextWriter.Formatting = Formatting.Indented;
    //                jsonTextWriter.QuoteChar = '"';

    //                _serializer.Serialize(jsonTextWriter, instance);

    //                var result = stringWriter.ToString();
    //                return result;
    //            }
    //        }
    //    }

    //    public virtual string ContentType
    //    {
    //        get { return "application/json"; }
    //    }

    //    public Encoding ContentEncoding
    //    {
    //        get { return Encoding.UTF8; }
    //    }

    //    public object Deserialize(RestResponseBase response, Type type)
    //    {
    //        using (var stringReader = new StringReader(response.Content))
    //        {
    //            using (var jsonTextReader = new JsonTextReader(stringReader))
    //            {
    //                return _serializer.Deserialize(jsonTextReader, type);
    //            }
    //        }
    //    }

    //    public T Deserialize<T>(RestResponseBase response)
    //    {
    //        using (var stringReader = new StringReader(response.Content))
    //        {
    //            using (var jsonTextReader = new JsonTextReader(stringReader))
    //            {
    //                return _serializer.Deserialize<T>(jsonTextReader);
    //            }
    //        }
    //    }

    //}
}