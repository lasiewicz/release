using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Spark.SAL.TestHarness
{
	/// <summary>
	/// Summary description for PreferenceHarness.
	/// </summary>
	public class PreferenceHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListBox lbPreferences;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PreferenceHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbPreferences = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// lbPreferences
			// 
			this.lbPreferences.Location = new System.Drawing.Point(80, 64);
			this.lbPreferences.Name = "lbPreferences";
			this.lbPreferences.Size = new System.Drawing.Size(120, 95);
			this.lbPreferences.TabIndex = 0;
			// 
			// PreferenceHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(384, 325);
			this.Controls.Add(this.lbPreferences);
			this.Name = "PreferenceHarness";
			this.Text = "PreferenceHarness";
			this.Load += new System.EventHandler(this.PreferenceHarness_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void PreferenceHarness_Load(object sender, System.EventArgs e)
		{
			foreach (string preferenceName in Spark.SAL.Preference.PreferenceNames)
			{
				lbPreferences.Items.Add(Spark.SAL.Preference.GetInstance(preferenceName).Attribute.Name);
			}
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new PreferenceHarness());
		}
	}
}
