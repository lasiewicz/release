using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;

namespace Spark.SAL.TestHarness
{
	/// <summary>
	/// Summary description for TestHarness.
	/// </summary>
	public class MemberSearchHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListBox lbSavedSearches;
		private System.Windows.Forms.TextBox tbMemberID;
		private System.Windows.Forms.ComboBox cbCommunity;
		private System.Windows.Forms.Label lblMemberID;
		private System.Windows.Forms.Label lblCommunity;
		private System.Windows.Forms.Label lblSavedSearches;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.ListBox lbPreferences;
		private System.Windows.Forms.Label lblValue;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.ListBox lbMembers;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MemberSearchHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbSavedSearches = new System.Windows.Forms.ListBox();
			this.tbMemberID = new System.Windows.Forms.TextBox();
			this.cbCommunity = new System.Windows.Forms.ComboBox();
			this.lblMemberID = new System.Windows.Forms.Label();
			this.lblCommunity = new System.Windows.Forms.Label();
			this.lblSavedSearches = new System.Windows.Forms.Label();
			this.btnLoad = new System.Windows.Forms.Button();
			this.lbPreferences = new System.Windows.Forms.ListBox();
			this.lblValue = new System.Windows.Forms.Label();
			this.btnSearch = new System.Windows.Forms.Button();
			this.lbMembers = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// lbSavedSearches
			// 
			this.lbSavedSearches.Location = new System.Drawing.Point(32, 176);
			this.lbSavedSearches.Name = "lbSavedSearches";
			this.lbSavedSearches.Size = new System.Drawing.Size(96, 108);
			this.lbSavedSearches.TabIndex = 0;
			this.lbSavedSearches.SelectedIndexChanged += new System.EventHandler(this.lbSavedSearches_SelectedIndexChanged);
			// 
			// tbMemberID
			// 
			this.tbMemberID.Location = new System.Drawing.Point(96, 24);
			this.tbMemberID.Name = "tbMemberID";
			this.tbMemberID.Size = new System.Drawing.Size(104, 20);
			this.tbMemberID.TabIndex = 1;
			this.tbMemberID.Text = "988272102";
			// 
			// cbCommunity
			// 
			this.cbCommunity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbCommunity.Location = new System.Drawing.Point(96, 56);
			this.cbCommunity.Name = "cbCommunity";
			this.cbCommunity.Size = new System.Drawing.Size(112, 21);
			this.cbCommunity.TabIndex = 2;
			// 
			// lblMemberID
			// 
			this.lblMemberID.Location = new System.Drawing.Point(24, 24);
			this.lblMemberID.Name = "lblMemberID";
			this.lblMemberID.Size = new System.Drawing.Size(72, 16);
			this.lblMemberID.TabIndex = 3;
			this.lblMemberID.Text = "MemberID";
			// 
			// lblCommunity
			// 
			this.lblCommunity.Location = new System.Drawing.Point(24, 56);
			this.lblCommunity.Name = "lblCommunity";
			this.lblCommunity.Size = new System.Drawing.Size(64, 16);
			this.lblCommunity.TabIndex = 4;
			this.lblCommunity.Text = "Community";
			// 
			// lblSavedSearches
			// 
			this.lblSavedSearches.Location = new System.Drawing.Point(32, 152);
			this.lblSavedSearches.Name = "lblSavedSearches";
			this.lblSavedSearches.Size = new System.Drawing.Size(96, 16);
			this.lblSavedSearches.TabIndex = 5;
			this.lblSavedSearches.Text = "Saved Searches:";
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(96, 96);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(48, 24);
			this.btnLoad.TabIndex = 6;
			this.btnLoad.Text = "Load";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// lbPreferences
			// 
			this.lbPreferences.Location = new System.Drawing.Point(240, 112);
			this.lbPreferences.Name = "lbPreferences";
			this.lbPreferences.Size = new System.Drawing.Size(176, 199);
			this.lbPreferences.TabIndex = 7;
			this.lbPreferences.SelectedIndexChanged += new System.EventHandler(this.lbPreferences_SelectedIndexChanged);
			// 
			// lblValue
			// 
			this.lblValue.Location = new System.Drawing.Point(472, 120);
			this.lblValue.Name = "lblValue";
			this.lblValue.Size = new System.Drawing.Size(80, 32);
			this.lblValue.TabIndex = 8;
			this.lblValue.Text = "label1";
			// 
			// btnSearch
			// 
			this.btnSearch.Location = new System.Drawing.Point(488, 264);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(64, 32);
			this.btnSearch.TabIndex = 9;
			this.btnSearch.Text = "Search";
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// lbMembers
			// 
			this.lbMembers.Location = new System.Drawing.Point(608, 128);
			this.lbMembers.Name = "lbMembers";
			this.lbMembers.Size = new System.Drawing.Size(144, 173);
			this.lbMembers.TabIndex = 10;
			// 
			// MemberSearchHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(776, 461);
			this.Controls.Add(this.lbMembers);
			this.Controls.Add(this.btnSearch);
			this.Controls.Add(this.lblValue);
			this.Controls.Add(this.lbPreferences);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.lblSavedSearches);
			this.Controls.Add(this.lblCommunity);
			this.Controls.Add(this.lblMemberID);
			this.Controls.Add(this.cbCommunity);
			this.Controls.Add(this.tbMemberID);
			this.Controls.Add(this.lbSavedSearches);
			this.Name = "MemberSearchHarness";
			this.Text = "TestHarness";
			this.Load += new System.EventHandler(this.TestHarness_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MemberSearchHarness());
		}

		private void TestHarness_Load(object sender, System.EventArgs e)
		{
			//Member member = MemberSA.Instance.GetMember(49137122, MemberLoadFlags.None);
			//member.SetAttributeInt(1, 0, 0, "IsAdmin", 1);
			//MemberSA.Instance.SaveMember(member);
			Matchnet.Member.ValueObjects.Privilege.PrivilegeCollection privs = MemberPrivilegeSA.Instance.GetPrivileges();
			
			MemberPrivilegeSA.Instance.SaveMemberPrivilege(49137122, 2);

			foreach (Community community in Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetCommunities().Values)
			{
				cbCommunity.Items.Add(community);
			}
			cbCommunity.DisplayMember = "Name";
			cbCommunity.ValueMember = "CommunityID";
			cbCommunity.SelectedIndex = 6;
		}

		private void btnLoad_Click(object sender, System.EventArgs e)
		{
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            MemberSearchCollection searches = SAL.MemberSearchCollection.Load(16000206, brand);
			
			foreach (MemberSearch search in searches)
			{
				lbSavedSearches.Items.Add(search);
			}

			lbSavedSearches.DisplayMember = "SearchName";
			lbSavedSearches.ValueMember = "SearchName";
		}

		private void lbSavedSearches_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			MemberSearch search = (MemberSearch) lbSavedSearches.SelectedItem;

			lbPreferences.Items.Clear();

			foreach (Preference preference in Preference.SearchPreferences)
			{
				if (search[preference] != null)
				{
					lbPreferences.Items.Add(preference.Attribute.Name);
				}
			}
		}

		private void lbPreferences_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			MemberSearch search = (MemberSearch) lbSavedSearches.SelectedItem;
			Preference preference = Preference.GetInstance(lbPreferences.SelectedItem.ToString());

			switch (preference.PreferenceType)
			{
				case PreferenceType.Mask:
				case PreferenceType.Int:
					lblValue.Text = (search[preference] as MemberSearchPreferenceInt).Value.ToString();
					break;
				case PreferenceType.Range:
					lblValue.Text = "(" + (search[preference] as MemberSearchPreferenceRange).MinValue + "," + (search[preference] as MemberSearchPreferenceRange).MaxValue + ")";
					break;
				case PreferenceType.Text:
					lblValue.Text = (search[preference] as MemberSearchPreferenceText).Value;
					break;
			}
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			MemberSearch search = (MemberSearch) lbSavedSearches.SelectedItem;
			Int32 totalResults;
			MemberCollection members = search.GetSearchResults(1, 10, out totalResults);
			
			foreach (Member member in members)
			{
				lbMembers.Items.Add(member);
			}

			lbMembers.DisplayMember = "MemberID";
		}
	}
}
