﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SAL
{
    public static class SearchPreferenceConstants
    {
        public const string RegionID = "RegionID";
        public const string Distance = "Distance";
        public const string MinAge = "MinAge";
        public const string MaxAge = "MaxAge";
        public const string GenderMask = "GenderMask";
        public const string SearchType = "SearchTypeID";
        public const string OrderBy = "SearchOrderBy";

        public static List<string> GetAllSearchPreferenceConstants()
        {
            var constants = new List<string> {RegionID, Distance, MinAge, MaxAge, GenderMask, SearchType, OrderBy};

            return constants;
        }
    }
}
