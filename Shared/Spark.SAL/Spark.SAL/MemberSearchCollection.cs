using System;
using System.Collections;

using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Search.ServiceAdapters;
using SearchVO = Matchnet.Search.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;

namespace Spark.SAL
{
	/// <summary>
	/// Represent's a members collection of saved searches in a particular community.
	/// </summary>
	public class MemberSearchCollection : System.Collections.IEnumerable
	{
		public const Int32 MAX_SAVED_SEARCHES = 5; //maximum number of saved searches that a user may have
		private Int32 _memberID;
		private Int32 _communityID;
		private MemberSearch _primarySearch;
		private bool _isVisitor;

		private Hashtable _searches = new Hashtable(MAX_SAVED_SEARCHES);
		private SearchVO.MemberSearchCollection _memberSearchCollection;

		private MemberSearchCollection()
		{
		}

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
			/*set
			{
				_memberID = value;
			}*/
		}

		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
			/*set
			{
				_communityID = value;
			}*/
		}

		public void Add(MemberSearch memberSearch)
		{
			if (_searches.Count >= MAX_SAVED_SEARCHES)
			{
				throw new System.Exception("Cannot add search.  User already has the maximum allowable number of searches.");
			}

			if (_primarySearch == null)
			{
				_primarySearch = memberSearch;
			}
            
            _searches.Add(memberSearch.SearchName, memberSearch);

			//Update underlying Search VO
			//_memberSearchCollection.Add(memberSearch);
		}

		public MemberSearch PrimarySearch
		{
			get
			{
				return _primarySearch;
			}
		}

		private SearchVO.MemberSearchCollection memberSearchCollection
		{
			get
			{
				return _memberSearchCollection;
			}
			set
			{
				_memberSearchCollection = value;

				_searches.Clear();
				foreach (SearchVO.MemberSearch memberSearch in _memberSearchCollection)
				{
					_searches.Add(memberSearch.SearchName, new MemberSearch(memberSearch));
				}
			}
		}

        public static MemberSearchCollection Load(Int32 memberID, Brand brand)
        {
            return Load(memberID, brand, false);
        }

		public static MemberSearchCollection Load(Int32 memberID, Brand brand, bool ignoreSACache)
		{
			MemberSearchCollection memberSearchCollection = new MemberSearchCollection();

			memberSearchCollection._memberID = memberID;
			memberSearchCollection._communityID = brand.Site.Community.CommunityID;

			memberSearchCollection._isVisitor = memberID <= 0;
            UserSession session = SessionSA.Instance.GetSession(Settings.IsDevMode);
			if (memberSearchCollection._isVisitor || (session.GetString("VLKeepSearchPrefs", String.Empty) == "true"))
			{				
				if (MemberSearch.USE_OLD_SCHEMA)
				{
					SearchVO.SearchPreferenceCollection preferences = session.Get(SearchVO.SearchPreferencesKey.GetCacheKey(session.Key.ToString())) as SearchVO.SearchPreferenceCollection;
					
					if (preferences == null || !preferences.ContainsKey("GenderMask") || !preferences.ContainsKey("MinAge") || !preferences.ContainsKey("MaxAge") || !preferences.ContainsKey("Distance"))
					{
						memberSearchCollection.Add(MemberSearch.GetDefaultSearch(brand));	
					}
					else
					{
						MemberSearch memberSearch = MemberSearch.GetMemberSearch(preferences, memberID, memberSearchCollection.CommunityID, "Default");
						memberSearchCollection.Add(memberSearch);
					}
				}
				else
				{
					SearchVO.MemberSearchCollection searchVOCollection = session.Get("MemberSearchCollection") as SearchVO.MemberSearchCollection;
					if (searchVOCollection != null)
					{
						memberSearchCollection.memberSearchCollection = searchVOCollection;
					}
					else
					{
						memberSearchCollection.Add(MemberSearch.GetDefaultSearch(brand));
					}
				}
			}
			else
			{
				if (MemberSearch.USE_OLD_SCHEMA)
				{
                    SearchVO.SearchPreferenceCollection preferences = SearchPreferencesSA.Instance.GetSearchPreferences(memberID, memberSearchCollection.CommunityID, ignoreSACache);

					MemberSearch memberSearch;
					
					if (preferences == null)
					{
						memberSearch = MemberSearch.GetDefaultSearch(brand);
					}
					else
					{
						memberSearch = MemberSearch.GetMemberSearch(preferences, memberID, memberSearchCollection.CommunityID, "Default");
					}

					memberSearchCollection.Add(memberSearch);
				}
				else
				{
					memberSearchCollection.memberSearchCollection = SearchPreferencesSA.Instance.GetMemberSearches(memberID, memberSearchCollection.CommunityID);
				}
			}

			return memberSearchCollection;
		}

		public void Save()
		{
			if (_isVisitor)
			{
				UserSession session = SessionSA.Instance.GetSession(Settings.IsDevMode);
				session.Add("MemberSearchCollection", _memberSearchCollection, SessionPropertyLifetime.TemporaryDurable);
				//Session is saved at the end of each request in ContextGlobal, so no need to save here.
			}
			else
			{
				if (MemberSearch.USE_OLD_SCHEMA)
				{
					PrimarySearch.Save();
				}
				else
				{
					SearchPreferencesSA.Instance.SaveMemberSearches(_memberSearchCollection);
				}
			}
		}

		#region IEnumerable Members

		public System.Collections.IEnumerator GetEnumerator()
		{
			return _searches.Values.GetEnumerator();
		}

		#endregion
	}
}
