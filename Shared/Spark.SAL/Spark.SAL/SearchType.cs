using System;

namespace Spark.SAL
{
	/// <summary>
	/// Summary description for SearchType.
	/// </summary>
	public enum SearchType
	{
		PostalCode = 1,
		Region = 4,
		AreaCode = 2,
		College = 8
	}
}
