using System;

using SearchVO = Matchnet.Search.ValueObjects;

namespace Spark.SAL
{
	public abstract class MemberSearchPreference
	{
		private Preference _preference;
		protected SearchVO.MemberSearchPreference _memberSearchPreference;

		protected MemberSearchPreference()
		{
		}

		public static MemberSearchPreference CreateInstance(SearchVO.MemberSearchPreference memberSearchPreference)
		{
			if (memberSearchPreference == null)
			{
				throw new ArgumentNullException("memberSearchPreference");
			}

			MemberSearchPreference returnMSP;

			Preference preference = Preference.GetInstance(memberSearchPreference.PreferenceID);

			switch (preference.PreferenceType)
			{
				case PreferenceType.Int:
					returnMSP = new MemberSearchPreferenceInt();
					break;
				case PreferenceType.Mask:
					returnMSP = new MemberSearchPreferenceMask();
					break;
				case PreferenceType.Range:
					returnMSP = new MemberSearchPreferenceRange();
					break;
				case PreferenceType.Text:
					returnMSP = new MemberSearchPreferenceText();
					break;
				default:
					throw new Exception("Unrecognized PreferenceType.");
			}

			returnMSP._preference = preference;
			returnMSP._memberSearchPreference = memberSearchPreference;

			return returnMSP;
		}

		public static MemberSearchPreference CreateInstance(Int32 memberSearchID, Preference preference)
		{
			if (preference == null)
			{
				throw new ArgumentNullException("preference");
			}

			return CreateInstance(new SearchVO.MemberSearchPreference(memberSearchID, preference.PreferenceID));
		}

		public Preference Preference
		{
			get
			{

				return _preference;
			}
			set
			{
				_preference = value;
			}
		}

		public Int32 Weight
		{
			get
			{
				return _memberSearchPreference.Weight;
			}
		}
	}
}
