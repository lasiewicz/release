using System;

namespace Spark.SAL
{
	/// <summary>
	/// Summary description for Settings.
	/// </summary>
	public class Settings
	{
		public static bool IsDevMode
		{
			get
			{
				return Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE"));
			}
		}
	}
}
