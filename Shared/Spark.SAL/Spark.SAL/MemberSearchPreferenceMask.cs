using System;

namespace Spark.SAL
{
	public class MemberSearchPreferenceMask : MemberSearchPreferenceInt
	{
		public bool ContainsValue(Int32 targetValue)
		{
			if (Value == Matchnet.Constants.NULL_INT)
			{
				return false;
			}

			return ((Value & targetValue) == targetValue);
		}

		public void ClearMask()
		{
			Value = 0;
		}

		public void AddValue(Int32 targetValue)
		{
			Value |= targetValue;
		}
	}
}
