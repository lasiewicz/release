using System;

namespace Spark.SAL
{
	public class MemberSearchPreferenceText : MemberSearchPreference
	{
		public string Value
		{
			get
			{
				return _memberSearchPreference.Value;
			}
			set
			{
				_memberSearchPreference.Value = value;
			}
		}
	}
}
