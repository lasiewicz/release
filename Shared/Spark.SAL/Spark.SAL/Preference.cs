using System;
using System.Collections;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Search.ServiceAdapters;
using SearchVO = Matchnet.Search.ValueObjects;

namespace Spark.SAL
{
	public class Preference
	{
		private Int32 _preferenceID;
		private Int32 _attributeID;
		private PreferenceType _preferenceType;

		private Preference(SearchVO.Preference preference)
		{
			_preferenceID = preference.PreferenceID;
			_attributeID = preference.AttributeID;
			_preferenceType = (PreferenceType) preference.PreferenceTypeID;
		}

		public Int32 PreferenceID
		{
			get
			{
				return _preferenceID;
			}
		}

		public Matchnet.Content.ValueObjects.AttributeMetadata.Attribute Attribute
		{
			get
			{
				return _attributes.GetAttribute(_attributeID);
			}
		}

		public string PreferenceName
		{
			get
			{
				return Attribute.Name;
			}
		}

		public PreferenceType PreferenceType
		{
			get
			{
				return _preferenceType;
			}
		}

		private static Attributes _attributes = AttributeMetadataSA.Instance.GetAttributes();
		private static SearchVO.PreferenceCollection _preferences;
		private static Hashtable _preferencesByName;
		private static Hashtable _preferencesByID;
		private static Object _lock = new Object();

		private static void initializeCollections()
		{
			lock (_lock)
			{
				if (_preferences == null)
				{
					_preferences = Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance.GetPreferences();
					_preferencesByName = new Hashtable();
					_preferencesByID = new Hashtable();

					foreach (SearchVO.Preference preference in Preferences)
					{
						string preferenceName = _attributes.GetAttribute(preference.AttributeID).Name;
						Preference SALPreference = new Preference(preference);
						_preferencesByName.Add(preferenceName.ToLower(), SALPreference);
						_preferencesByID.Add(preference.PreferenceID, SALPreference);
					}
				}
			}
		}

		private static SearchVO.PreferenceCollection Preferences
		{
			get
			{
				if (_preferences == null)
				{
					initializeCollections();
				}

				return _preferences;
			}
		}

		private static Hashtable PreferencesByName
		{
			get
			{
				if (_preferencesByName == null)
				{
					initializeCollections();
				}

				return _preferencesByName;
			}
		}

		private static Hashtable PreferencesByID
		{
			get
			{
				if (_preferencesByID == null)
				{
					initializeCollections();
				}

				return _preferencesByID;
			}
		}

		public static ICollection SearchPreferences
		{
			get
			{
				return PreferencesByName.Values;
			}
		}

		public static ICollection PreferenceNames
		{
			get
			{
				return PreferencesByName.Keys;
			}
		}

		public static Preference GetInstance(string preferenceName)
		{
			preferenceName = preferenceName.ToLower();
			if (PreferencesByName.ContainsKey(preferenceName))
			{
				return PreferencesByName[preferenceName] as Preference;
			}
			else
			{
				return null;
			}
		}

		public static Preference GetInstance(Int32 preferenceID)
		{
			if (PreferencesByID.ContainsKey(preferenceID))
			{
				return PreferencesByID[preferenceID] as Preference;
			}
			else
			{
				return null;
			}
		}
	}
}
