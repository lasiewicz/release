using System;

namespace Spark.SAL
{
	/// <summary>
	/// Summary description for Gender.
	/// </summary>
	public enum Gender
	{
		Male = 1,
		Female = 2
	}
}
