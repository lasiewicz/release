using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters;
using SearchVO = Matchnet.Search.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;

namespace Spark.SAL
{
	/// <summary>
	/// Represents an individual member search and all associated preferences.
	/// </summary>
	public class MemberSearch
	{
		public const bool USE_OLD_SCHEMA = true;

		private bool _isVisitor;
		private Int32 _memberSearchID;
		private Int32 _memberID;
		private Int32 _groupID;

		private SearchVO.MemberSearch _memberSearch;

		private Hashtable _preferences = new Hashtable();

		#region Constructors

		//TODO: change Int32 gendermask to Gender gender, Gender seekingGender
		/// <summary>
		/// Constructor for member search.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="searchType"></param>
		/// <param name="genderMask"></param>
		/// <param name="minAge"></param>
		/// <param name="maxAge"></param>
		/// <param name="regionID"></param>
		/// <param name="distance"></param>
		public MemberSearch(Int32 memberID, Int32 communityID, SearchType searchType, Int32 genderMask, Int32 minAge, Int32 maxAge, Int32 regionID, Int32 distance)
		{
			_isVisitor = false;
			init(memberID, communityID, searchType, genderMask, minAge, maxAge, regionID, distance);
		}

		//Constructor for visitor search
        public MemberSearch(SearchType searchType, Int32 genderMask, Int32 minAge, Int32 maxAge, Int32 regionID, Int32 distance)
		{
			_isVisitor = true;
			init(0, 0, searchType, genderMask, minAge, maxAge, regionID, distance);
		}

		internal MemberSearch(SearchVO.MemberSearch memberSearch)
		{
			_memberSearch = memberSearch;
			initializePreferencesHashtable();
		}

		private void init(Int32 memberID, Int32 communityID, SearchType searchType, Int32 genderMask, Int32 minAge, Int32 maxAge, Int32 regionID, Int32 distance)
		{
			_memberID = memberID;
			_groupID = communityID;

			Int32 memberSearchID = 0;//TODO:KeySA.Instance.GetKey("MemberSearchID");

			_memberSearch = new SearchVO.MemberSearch(memberSearchID, memberID, communityID, string.Empty, false);

			SearchVO.MemberSearchPreference searchTypePreference = new SearchVO.MemberSearchPreference(memberSearchID, Preference.GetInstance("SearchTypeID").PreferenceID);
			searchTypePreference.Value = ((Int32) searchType).ToString();
			_memberSearch.AddPreference(searchTypePreference);

			SearchVO.MemberSearchPreference genderMaskPreference = new SearchVO.MemberSearchPreference(memberSearchID, Preference.GetInstance("GenderMask").PreferenceID);
			genderMaskPreference.Value = genderMask.ToString();
			_memberSearch.AddPreference(genderMaskPreference);

			SearchVO.MemberSearchPreference birthDatePreference = new SearchVO.MemberSearchPreference(memberSearchID, Preference.GetInstance("Birthdate").PreferenceID);
			birthDatePreference.MinValue = minAge;
			birthDatePreference.MaxValue = maxAge;
			_memberSearch.AddPreference(birthDatePreference);

			SearchVO.MemberSearchPreference regionIDPreference = new SearchVO.MemberSearchPreference(memberSearchID, Preference.GetInstance("RegionID").PreferenceID);
			regionIDPreference.Value = regionID.ToString();
			_memberSearch.AddPreference(regionIDPreference);

			SearchVO.MemberSearchPreference distancePreference = new SearchVO.MemberSearchPreference(memberSearchID, Preference.GetInstance("Distance").PreferenceID);
			distancePreference.Value = distance.ToString();
			_memberSearch.AddPreference(distancePreference);

			initializePreferencesHashtable();
		}

		private void initializePreferencesHashtable()
		{
			foreach (SearchVO.MemberSearchPreference preference in _memberSearch)
			{
				_preferences.Add(Preference.GetInstance(preference.PreferenceID), MemberSearchPreference.CreateInstance(preference));
			}
		}

		#endregion

		#region Properties
		public bool IsVisitor
		{
			get
			{
				return _isVisitor;
			}
		}

		public Int32 MemberSearchID
		{
			get
			{
				return _memberSearchID;
			}
		}

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID=value;
				if(_memberID <= 0)
					_isVisitor=true;
				else
					_isVisitor=false;
			}
		}

		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}

		public string SearchName
		{
			get
			{
				return _memberSearch.SearchName;
			}
			set
			{
				_memberSearch.SearchName = value;
			}
		}

		public SearchType SearchType
		{
			get
			{
				Int32 searchTypeID = (this[Preference.GetInstance("SearchTypeID")] as MemberSearchPreferenceInt).Value;
				return (SearchType) searchTypeID;
			}
			set
			{
				MemberSearchPreferenceInt searchType = this[Preference.GetInstance("SearchTypeID")] as MemberSearchPreferenceInt;

				searchType.Value = (Int32) value;
			}
		}

		public Gender Gender
		{
			get
			{
				Int32 genderMask = (this[Preference.GetInstance("GenderMask")] as MemberSearchPreferenceMask).Value;
				return GenderUtils.GetGender(genderMask);
			}
			set
			{
				MemberSearchPreferenceMask genderMask = this[Preference.GetInstance("GenderMask")] as MemberSearchPreferenceMask;

				genderMask.Value = GenderUtils.GetGenderMask((Gender) value, SeekingGender);
			}
		}

		public Gender SeekingGender
		{
			get
			{
				Int32 genderMask = (this[Preference.GetInstance("GenderMask")] as MemberSearchPreferenceMask).Value;
				return GenderUtils.GetSeekingGender(genderMask);
			}
			set
			{
				MemberSearchPreferenceMask genderMask = this[Preference.GetInstance("GenderMask")] as MemberSearchPreferenceMask;

				genderMask.Value = GenderUtils.GetGenderMask(Gender, (Gender) value);
			}
		}

		public Int32 RegionID
		{
			get
			{
				return (this[Preference.GetInstance("RegionID")] as MemberSearchPreferenceInt).Value;
			}
			set
			{
				(this[Preference.GetInstance("RegionID")] as MemberSearchPreferenceInt).Value = value;
			}
		}

		public Int32 Distance
		{
			get
			{
				return (this[Preference.GetInstance("Distance")] as MemberSearchPreferenceInt).Value;
			}
			set
			{
				(this[Preference.GetInstance("Distance")] as MemberSearchPreferenceInt).Value = value;
			}
		}

		public bool HasPhoto
		{
			get
			{
				return (this[Preference.GetInstance("HasPhotoFlag")] as MemberSearchPreferenceInt).Value == 1;
			}
			set
			{
				(this[Preference.GetInstance("HasPhotoFlag")] as MemberSearchPreferenceInt).Value = value ? 1 : 0;
			}
		}

		public MemberSearchPreferenceRange Age
		{
			get
			{
				return this[Preference.GetInstance("Birthdate")] as MemberSearchPreferenceRange;
			}
		}

		public Int32 SchoolID
		{
			get
			{
				return (this[Preference.GetInstance("SchoolID")] as MemberSearchPreferenceInt).Value;
			}
			set
			{
				(this[Preference.GetInstance("SchoolID")] as MemberSearchPreferenceInt).Value = value;
			}
		}

        public string KeywordSearch
        {
            get
            {
                MemberSearchPreferenceText msp = this[Preference.GetInstance("KeywordSearch")] as MemberSearchPreferenceText;
                if (msp != null)
                    return msp.Value;
                else
                    return "";
            }
            set
            {
                MemberSearchPreferenceText msp = this[Preference.GetInstance("KeywordSearch")] as MemberSearchPreferenceText;
                if (msp != null)
                {
                    msp.Value = value;
                }
            }
        }

		public bool IsPrimary
		{
			get
			{
				return _memberSearch.IsPrimary;
			}
		}
		#endregion

		public MemberSearchPreference this[Preference preference]
		{
			get
			{
                MemberSearchPreference msp = null;

                if (preference != null)
                {
                    msp = _preferences[preference] as MemberSearchPreference;

                    if (msp == null)
                    {
                        msp = MemberSearchPreference.CreateInstance(_memberSearchID, preference);
                        _preferences[preference] = msp;
                    }
                }

				return msp;
			}
			set
			{
                if (preference != null)
                {
                    value.Preference = preference;
                    _preferences[preference] = value;
                }
			}
		}

		public ICollection SearchPreferences
		{
			get
			{
				return _preferences.Keys;
			}
		}

	    public MemberCollection GetSearchResults(Int32 startRow, Int32 pageSize, out Int32 totalResults)
	    {
	        Dictionary<int, int> matchPercentages = null;
	        return GetSearchResults(startRow, pageSize, out totalResults, Matchnet.Search.Interfaces.SearchType.WebSearch, Matchnet.Search.Interfaces.SearchEntryPoint.None, matchPercentages);
	    }

	    public MemberCollection GetSearchResults(int startRow, int pageSize, out int totalResults, Matchnet.Search.Interfaces.SearchType searchType, SearchEntryPoint pSearchEntryPoint, Dictionary<int, int> matchPercentages)
		{
		    SearchVO.MatchnetQueryResults queryResults = MemberSearchSA.Instance.Search(getSearchPreferenceCollection(this),
		        GroupID, Constants.NULL_INT, startRow, pageSize, this.MemberID,
		        Matchnet.Search.Interfaces.SearchEngineType.FAST,
		        searchType,
		        pSearchEntryPoint, false, false, false);

			MemberCollection memberCollection = new MemberCollection();
			foreach (SearchVO.MatchnetResultItem item in queryResults.Items.List)
			{
				memberCollection.Add(MemberSA.Instance.GetMember(item.MemberID, MemberLoadFlags.None));
			    if (null == matchPercentages) matchPercentages = new Dictionary<int, int>();
			    matchPercentages.Add(item.MemberID, item.MatchScore);
		    }   
			
			totalResults = queryResults.MatchesFound;
			return memberCollection;
		}

		public static MemberSearch GetMemberSearch(SearchVO.SearchPreferenceCollection preferences, Int32 memberID, Int32 communityID, string searchName)
		{
			SearchType searchType = (SearchType) Enum.Parse(typeof(SearchType), preferences["SearchTypeID"]);
			Int32 genderMask = Conversion.CInt(preferences["GenderMask"]);
			Int32 minAge = Conversion.CInt(preferences["MinAge"]);
			Int32 maxAge = Conversion.CInt(preferences["MaxAge"]);
			Int32 regionID = Conversion.CInt(preferences["RegionID"]);
			Int32 distance = Conversion.CInt(preferences["Distance"]);

			MemberSearch memberSearch;
			if (memberID > 0)
			{
				memberSearch = new MemberSearch(memberID, communityID, searchType, genderMask, minAge, maxAge, regionID, distance);
			}
			else
			{
				memberSearch = new MemberSearch(searchType, genderMask, minAge, maxAge, regionID, distance);
			}

            if (!String.IsNullOrEmpty(searchName))
                memberSearch.SearchName = searchName;
            else
                memberSearch.SearchName = "Default";

			MemberSearchPreferenceRange mspHeight = MemberSearchPreference.CreateInstance(memberSearch.MemberSearchID, Preference.GetInstance("Height")) as MemberSearchPreferenceRange;
			mspHeight.MinValue = Conversion.CInt(preferences["minheight"]);
			mspHeight.MaxValue = Conversion.CInt(preferences["maxheight"]);
			memberSearch[Preference.GetInstance("Height")] = mspHeight;

			foreach (string key in preferences.Keys())
			{
				Preference preference = Preference.GetInstance(key);

				if (preference != null)
				{
					MemberSearchPreference memberSearchPreference = memberSearch[preference];

					if (memberSearchPreference == null)
					{
						memberSearchPreference = MemberSearchPreference.CreateInstance(memberSearch.MemberSearchID, preference);
					}

					switch (preference.PreferenceType)
					{
						case PreferenceType.Int:
							Int32 val = Conversion.CInt(preferences[key]);
							(memberSearchPreference as MemberSearchPreferenceInt).Value = val;
							break;
						case PreferenceType.Mask:
							Int32 maskVal = Conversion.CInt(preferences[key]);
							(memberSearchPreference as MemberSearchPreferenceMask).Value = maskVal;
							break;
						case PreferenceType.Range:
							//the two range prefs are hard-coded above
							break;
						case PreferenceType.Text:
							(memberSearchPreference as MemberSearchPreferenceText).Value = preferences[key];
							break;
						default:
							throw new Exception("Unrecognized PreferenceType.");
					}

					memberSearch[preference] = memberSearchPreference;
				}
			}

			return memberSearch;
		}

		public static SearchVO.SearchPreferenceCollection getSearchPreferenceCollection(MemberSearch memberSearch)
		{
			SearchVO.SearchPreferenceCollection searchPreferenceCollection = new SearchVO.SearchPreferenceCollection();

			foreach (Preference Preference in memberSearch.SearchPreferences)
			{
				string prefName = Preference.Attribute.Name;

				switch (Preference.PreferenceType)
				{
					case PreferenceType.Int:
					case PreferenceType.Mask:
						MemberSearchPreferenceInt prefInt = (MemberSearchPreferenceInt) memberSearch[Preference];
						if (prefInt.Value != Constants.NULL_INT && prefInt.Value != 0)
						{
							searchPreferenceCollection.Add(prefName, prefInt.Value.ToString());
						}
						break;
					case PreferenceType.Range:
						if (prefName.ToLower() == "height")
						{
							MemberSearchPreferenceRange prefRange = (MemberSearchPreferenceRange) memberSearch[Preference];
							searchPreferenceCollection.Add("Min" + prefName, prefRange.MinValue.ToString());
							searchPreferenceCollection.Add("Max" + prefName, prefRange.MaxValue.ToString());
						}
						break;
					case PreferenceType.Text:
						MemberSearchPreferenceText prefText = (MemberSearchPreferenceText) memberSearch[Preference];
						searchPreferenceCollection.Add(prefName, prefText.Value);
						break;
				}
			}

			MemberSearchPreferenceRange agePref = memberSearch[Spark.SAL.Preference.GetInstance("Birthdate")] as MemberSearchPreferenceRange;
			searchPreferenceCollection.Add("MinAge", agePref.MinValue.ToString());
			searchPreferenceCollection.Add("MaxAge", agePref.MaxValue.ToString());

			return searchPreferenceCollection;
		}

		public static MemberSearch Load(Int32 memberSearchID)
		{
			return null;
		}

		public void Save()
		{
			if (USE_OLD_SCHEMA)
			{
				SearchVO.SearchPreferenceCollection preferences = getSearchPreferenceCollection(this);

				if (_isVisitor)
				{
					UserSession session = SessionSA.Instance.GetSession(Settings.IsDevMode);
					session.Add(SearchVO.SearchPreferencesKey.GetCacheKey(session.Key.ToString())
						, preferences
						, SessionPropertyLifetime.TemporaryDurable);
				}
				else
				{
					SearchPreferencesSA.Instance.Save(_memberID, _groupID, preferences);
				}
			}
			else
			{
				throw new NotImplementedException("Implement yo self, foo.");
			}
		}

		public static MemberSearch GetDefaultSearch(Brand brand)
		{
			MemberSearch memberSearch = new MemberSearch((SearchType) Enum.Parse(typeof(SearchType), brand.Site.DefaultSearchTypeID.ToString()),
				GenderUtils.GetGenderMask(Gender.Female, Gender.Male), brand.DefaultAgeMin, brand.DefaultAgeMax,
				brand.Site.DefaultRegionID, brand.DefaultSearchRadius);

			MemberSearchPreferenceMask languageMask = memberSearch[Preference.GetInstance("LanguageMask")] as MemberSearchPreferenceMask;
			languageMask.ClearMask();
			languageMask.AddValue(brand.Site.LanguageID);

			return memberSearch;
		}
	}
}
