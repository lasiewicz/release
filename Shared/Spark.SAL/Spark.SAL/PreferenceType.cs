using System;

namespace Spark.SAL
{
	/// <summary>
	/// Summary description for SearchPreferenceType.
	/// </summary>
	public enum PreferenceType
	{
		Int = 1,
		Mask = 2,
		Range = 3,
		Text = 4
	}
}
