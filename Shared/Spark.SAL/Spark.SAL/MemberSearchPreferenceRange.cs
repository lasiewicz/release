using System;

namespace Spark.SAL
{
	public class MemberSearchPreferenceRange : MemberSearchPreference
	{
		public Int32 MinValue
		{
			get
			{
				return _memberSearchPreference.MinValue;
			}
			set
			{
				_memberSearchPreference.MinValue = value;
			}
		}

		public Int32 MaxValue
		{
			get
			{
				return _memberSearchPreference.MaxValue;
			}
			set
			{
				_memberSearchPreference.MaxValue = value;
			}
		}
	}
}
