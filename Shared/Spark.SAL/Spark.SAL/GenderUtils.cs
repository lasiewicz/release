using System;

namespace Spark.SAL
{
	/// <summary>
	/// Summary description for GenderUtils.
	/// </summary>
	public class GenderUtils
	{
		private const Int32 SEEKING_MALE = 4;
		private const Int32 SEEKING_FEMALE = 8;

		private GenderUtils()
		{
		}

		public static Int32 GetGenderMask(Gender gender, Gender seekingGender)
		{
			return (Int32) seekingGender + (gender == Gender.Male ? SEEKING_MALE : SEEKING_FEMALE);
		}

		public static Gender GetGender(Int32 genderMask)
		{
			if ((genderMask & SEEKING_MALE) == SEEKING_MALE)
			{
				return Gender.Male;
			}
			else
			{
				return Gender.Female;
			}
		}

		public static Gender GetSeekingGender(Int32 genderMask)
		{
			if ((genderMask & (Int32) Gender.Male) == (Int32) Gender.Male)
			{
				return Gender.Male;
			}
			else
			{
				return Gender.Female;
			}
		}
	}
}
