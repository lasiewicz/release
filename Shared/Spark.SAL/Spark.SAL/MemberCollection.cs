using System;
using System.Collections;

using Matchnet.Member.ServiceAdapters;

namespace Spark.SAL
{
	public class MemberCollection : System.Collections.IEnumerable
	{
		private ArrayList _members = new ArrayList();

		public void Add(Member member)
		{
			_members.Add(member);
		}

		#region IEnumerable Members

		public System.Collections.IEnumerator GetEnumerator()
		{
			return _members.GetEnumerator();
		}

		#endregion
	}
}
