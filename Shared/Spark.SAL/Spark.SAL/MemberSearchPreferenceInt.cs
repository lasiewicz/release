using System;

using Matchnet;

namespace Spark.SAL
{
	public class MemberSearchPreferenceInt : MemberSearchPreference
	{
		public Int32 Value
		{
			get
			{
				return Conversion.CInt(_memberSearchPreference.Value);
			}
			set
			{
				_memberSearchPreference.Value = value.ToString();
			}
		}
	}
}
