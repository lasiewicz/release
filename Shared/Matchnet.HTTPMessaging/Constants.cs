using System;

namespace Matchnet.HTTPMessaging
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Constants
	{
		public enum SMSStatus
        {
            Unknown = 0,
            Success = 1,
        }

		public enum MessmoButtonType : int
		{
			Reply = 1,
			Send = 2,
			Compose = 3,
			Continue = 4
		}

		[Flags()]
		public enum MessmoAlertMask : int
		{
			None = 0,
			Message = 1,
			CheckingMeOut = 2, // Someone viewed your profile
			NewMembersInArea = 4,
			DailyMatches = 8,
			MembersYouViewed = 16,
			Hotlisted = 32, // when someone hot lists the member
			Favorites = 64 // member's default hot list (favorites)
		}

		public enum MessmoAlertSetting : int
		{
			DoNotSend = 0,
			Always = 1,
			WhenLoggedOff = 2,
		}

		public enum MessmoMessageType : int
		{
			None = 0,
			Email = 1,
			Flirt = 2,
			Ecard = 3,
			MissedIM = 4,
			ViewedProfile = 5,
			Hotlisted = 6,
			NewMembersInArea = 7,
			Matches = 8,
			Favorites = 9,
			MembersYouViewed = 10
		}

		public enum MessmoCapableStatus : int
		{
			NotCapable = 0,
			Capable = 1,
			Pending =2,
			Failed = 3
		}

        public const string REQUEST_TYPE_VALIDATION = "VALIDATION";
        public const string REQUEST_TYPE_MESSAGE = "MESSAGE";
        public const string REQUEST_TYPE_STATUS = "STATUS";
		public const string MESSMO_UNSUBSCRIBE_COMMAND = "off";
		public const string MESSMO_SUBSCRIBE_COMMAND = "on";
		public const string MESSMO_USER_TRANS_STATUS_SET_VALID_UNTIL = "setValidUntil";
	}
}
