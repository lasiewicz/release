using System;
using System.Text;
using System.Xml;

using Matchnet.HTTPMessaging.ResponseTypes;

namespace Matchnet.HTTPMessaging
{
	/// <summary>
	/// Summary description for SMSParser.
	/// </summary>
	public class SMSParser
    {
		public static MessmoSubscribeResponse ParseMessmoSubscribePost(string rawXml)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(rawXml);
	
			string subscriberId = doc.SelectSingleNode("response/subscriberId").InnerText;
			string isMessmoUser = doc.SelectSingleNode("response/isMessmoUser").InnerText;

			return new MessmoSubscribeResponse(subscriberId, isMessmoUser);
		}

		public static TeleclalResponse ParseTeleclalGet(string rawXml)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(rawXml);
			
			string phoneNum = doc.SelectSingleNode("Jdate/PhoneNumber").InnerText;
			string extCode = doc.SelectSingleNode("Jdate/Code").InnerText;

			return new TeleclalResponse(phoneNum, extCode);
		}

		public static TeleclalSMSMessageResponse ParseTeleclalPost(string rawXml)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(rawXml);

			string status = doc.SelectSingleNode("response").FirstChild.Attributes["code"].Value;
			string reason = doc.SelectSingleNode("response").FirstChild.InnerText;

			return new TeleclalSMSMessageResponse(status, reason);
		}

		public static SMSValidationResponse ParseValidation(string rawXml)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(rawXml);

			Constants.SMSStatus status = (Constants.SMSStatus)int.Parse(doc.SelectSingleNode("response/status/id").InnerText);
			string confirmCode = (status == Constants.SMSStatus.Success) ? doc.SelectSingleNode("response/confCode").InnerText : "";

			return new SMSValidationResponse(status, confirmCode);
		}

		public static SMSMessageResponse ParseMessage(string rawXml)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(rawXml);

			Constants.SMSStatus status = (Constants.SMSStatus)int.Parse(doc.SelectSingleNode("response/status/id").InnerText);
			return new SMSMessageResponse(status);
		}
    }
}
