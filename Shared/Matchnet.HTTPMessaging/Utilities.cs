using System;
using System.Text;
using System.Web;
using Matchnet.HTTPMessaging.RequestTypes;

namespace Matchnet.HTTPMessaging
{
	/// <summary>
	/// Summary description for Utilities.
	/// </summary>
	public class Utilities
	{
		public static string EncodeString(object val, bool urlEncode)
		{
			if(val == null)
				return string.Empty;
			
			string ret = string.Empty;

			if(val is bool)
			{
				ret = val.ToString().ToLower();
			}
			else if(val is NumericBoolean)
			{
				ret = ((NumericBoolean)val).StringVal;
			}
			else
			{
				ret = val.ToString();
			}

			if(urlEncode)
				return HttpUtility.UrlEncode(ret);

			return ret;
		}

		
		public static string Make2Digits(int val)
		{
			string valString = val.ToString();

			if(valString.Length < 2)
				return "0" + valString;

			return valString;
		}
	
		
		#region Messmo-Specific Stuff
		public static int GetSiteIDFromMessmoChannelNumber(string messmoChannelNumber)
		{
			int siteID = Matchnet.Constants.NULL_INT;

			if(messmoChannelNumber == "53283")
			{
				siteID = 103;
			}

			return siteID;
		}

		public static Matchnet.HTTPMessaging.Constants.MessmoButtonType GetButtonTypeFromMessmoMessageType(Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType)
		{
			Matchnet.HTTPMessaging.Constants.MessmoButtonType messmoButtonType = Matchnet.HTTPMessaging.Constants.MessmoButtonType.Reply;

			switch(messmoMsgType)
			{
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Email:
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Flirt:
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Ecard:
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.MissedIM:
					messmoButtonType = Matchnet.HTTPMessaging.Constants.MessmoButtonType.Reply;
					break;
				default:
					messmoButtonType = Matchnet.HTTPMessaging.Constants.MessmoButtonType.Compose;
					break;                
			}

			return messmoButtonType;
		}
		
		public static string GetMessageSubject(Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType)
		{
			string ret = "Replied message";
			
			switch(messmoMsgType)
			{				
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.ViewedProfile:
					ret = "You viewed me";
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Hotlisted:
					ret = "You hot listed me";
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea:
					ret = "I liked your profile";
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Matches:
					ret = "I liked your profile";
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Favorites:
					ret = "I liked your profile";
					break;
			}

			return ret;
		}

		public static string GetMessmoMessageBody(Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType, string messageBody, string messageSubject)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("Subject: ");

			switch(messmoMsgType)
			{
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Email:
					sb.Append(messageSubject);
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Flirt:
					sb.Append("You have received a new flirt");
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Ecard:
					sb.Append("You have received a new Ecard");
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.MissedIM:
					sb.Append("You missed an IM");
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.ViewedProfile:
					sb.Append("Someone has viewed your profile");
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Hotlisted:
					sb.Append("Someone has hot listed you");
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.MembersYouViewed:
					sb.Append("You viewed this profile");
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea:
					sb.Append("Here is a new member in your area");
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Matches:
					sb.Append("Your match");
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Favorites:
					sb.Append("You hot listed this member");
					break;					
			}

			if(messageBody != string.Empty)
			{
				if(messmoMsgType == Matchnet.HTTPMessaging.Constants.MessmoMessageType.Ecard)
				{
					sb.Append("<br />Login to JDate.com to view your Ecard. ");
				}
				else
				{
					sb.Append("<br />" + messageBody);
				}
			}

			return sb.ToString();
		}

		
		public static Matchnet.HTTPMessaging.Constants.MessmoAlertMask MapToMessmoAlertMask(Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType)
		{
			Matchnet.HTTPMessaging.Constants.MessmoAlertMask ret = Matchnet.HTTPMessaging.Constants.MessmoAlertMask.None;
			
			switch(messmoMsgType)
			{
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Email:
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Flirt:
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Ecard:
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.MissedIM:
					ret = Matchnet.HTTPMessaging.Constants.MessmoAlertMask.Message;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.ViewedProfile:
					ret = Matchnet.HTTPMessaging.Constants.MessmoAlertMask.CheckingMeOut;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Hotlisted:
					ret = Matchnet.HTTPMessaging.Constants.MessmoAlertMask.Hotlisted;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea:
					ret = Matchnet.HTTPMessaging.Constants.MessmoAlertMask.NewMembersInArea;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Matches:
					ret = Matchnet.HTTPMessaging.Constants.MessmoAlertMask.DailyMatches;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Favorites:
					ret = Matchnet.HTTPMessaging.Constants.MessmoAlertMask.Favorites;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.MembersYouViewed:
					ret = Matchnet.HTTPMessaging.Constants.MessmoAlertMask.MembersYouViewed;
					break;
			}

			return ret;
		}

		
		public static string GetMessmoSmartMessageTag(Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType, string fromUsername)
		{
			string ret = string.Empty;

			switch(messmoMsgType)
			{				
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.ViewedProfile:
					ret = "Viewed You::" + fromUsername;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Hotlisted:
					ret = "Hot Listed You::" + fromUsername;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea:
					ret = "New Members::" + fromUsername;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Matches:
					ret = "Your Matches::" + fromUsername;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Favorites:
					ret = "You Hot Listed::" + fromUsername;
					break;
				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.MembersYouViewed:
					ret = "You Viewed::" + fromUsername;
					break;				
				default:
					ret = "Inbox::" + fromUsername;
					break;
			}

			return ret;
		}

		
		public static string GetMessmoTaggleValue(int senderMemberID, int receiverMemberID, Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType,
			int messageGroupID, int messageListIDOfRecipient)
		{
			string ret = string.Empty;


			ret =  string.Format("{0}::{1}::{2}::{3}::{4}", senderMemberID.ToString(), receiverMemberID.ToString(), (int)messmoMsgType,
				messageGroupID.ToString(), messageListIDOfRecipient.ToString());

//			switch(messmoMsgType)
//			{				
//				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Email:
//				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Flirt:
//				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.Ecard:
//				case Matchnet.HTTPMessaging.Constants.MessmoMessageType.MissedIM:
//					// These are the types that arrive in our Inbox, thus it should have a messageListID for the message
//					ret =  string.Format("{0}::{1}::{2}::{3}::{4}", senderMemberID.ToString(), receiverMemberID.ToString(), (int)messmoMsgType,
//						messageGroupID.ToString(), messageListIDOfRecipient.ToString());
//					break;
//
//				default:				
//					ret = string.Format("{0}::{1}::{2}::{3}::{4}", senderMemberID.ToString(), receiverMemberID.ToString(), (int)messmoMsgType,
//						Matchnet.Constants.NULL_INT.ToString(), Matchnet.Constants.NULL_INT.ToString());
//					break;		
//			}

			return ret;
		}

		
		public static void ParseMessmoTaggleValue(string taggle, out int fromMemberID, out int toMemberID, out Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType,
			out int groupID, out int messageListID)
		{
			string[] taggleSplits = taggle.Split(new char[] {':'});

			if(taggleSplits.Length == 9)
			{
				fromMemberID = int.Parse(taggleSplits[0]);
				toMemberID = int.Parse(taggleSplits[2]);
				messmoMsgType = (Matchnet.HTTPMessaging.Constants.MessmoMessageType)int.Parse(taggleSplits[4]);
				groupID = int.Parse(taggleSplits[6]);
				messageListID = int.Parse(taggleSplits[8]);
			}
			else
			{
				fromMemberID = Matchnet.Constants.NULL_INT;
				toMemberID = Matchnet.Constants.NULL_INT;
				messmoMsgType = Matchnet.HTTPMessaging.Constants.MessmoMessageType.None;
				groupID = Matchnet.Constants.NULL_INT;
				messageListID = Matchnet.Constants.NULL_INT;
			}            
		}

		
		public static string GetMessmoFormatTimestamp()
		{
			DateTime gmtTime = DateTime.Now.ToUniversalTime();

			int day = gmtTime.Day;
			int month = gmtTime.Month;
			int year = gmtTime.Year;

			int hour = gmtTime.Hour;
			int minute = gmtTime.Minute;
			int second = gmtTime.Second;

			return string.Format("{0}/{1}/{2} {3}:{4}:{5}", 
				Utilities.Make2Digits(day), Utilities.Make2Digits(month), year, Utilities.Make2Digits(hour), Utilities.Make2Digits(minute),
				Utilities.Make2Digits(second));
		}

		
		public static string GetMessmoFormatTimestamp(DateTime dtValue)
		{
			DateTime gmtTime = dtValue.ToUniversalTime();

			int day = gmtTime.Day;
			int month = gmtTime.Month;
			int year = gmtTime.Year;

			int hour = gmtTime.Hour;
			int minute = gmtTime.Minute;
			int second = gmtTime.Second;

			return string.Format("{0}/{1}/{2} {3}:{4}:{5}", 
				Utilities.Make2Digits(day), Utilities.Make2Digits(month), year, Utilities.Make2Digits(hour),
				Utilities.Make2Digits(minute), Utilities.Make2Digits(second));
		}

		
		public static DateTime ConvertMessmoTimestampToDateTime(string timestamp, bool convertToLocalTime)
		{
			DateTime ret = DateTime.MinValue;
			DateTime gmtTime = DateTime.MinValue;

			// dd/MM/yyyy HH:MM:SS is the timestamp format
			string day = timestamp.Substring(0, 2);
			string month = timestamp.Substring(3, 2);
			string year = timestamp.Substring(6, 4);
			string hour = timestamp.Substring(11, 2);
			string minute = timestamp.Substring(14, 2);
			string seconds = timestamp.Substring(17, 2);

			gmtTime = new DateTime(int.Parse(year), int.Parse(month), int.Parse(day), int.Parse(hour), int.Parse(minute), int.Parse(seconds));

			if(convertToLocalTime)
			{
				ret = gmtTime.ToLocalTime();
				if(ret > DateTime.Now)
				{
					ret = DateTime.Now;
				}
			}
			else
			{
				ret = gmtTime;
			}
			
			return ret;			
		}		

		#endregion
	}
}
