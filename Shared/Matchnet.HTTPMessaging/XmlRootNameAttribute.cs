using System;

namespace Matchnet.HTTPMessaging
{
	/// <summary>
	/// Summary description for XmlRootNameAttribute.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
     public class XmlRootNameAttribute : Attribute
     {
          public XmlRootNameAttribute()
          {}
          public XmlRootNameAttribute(string name)
          {
               this.name = name;
          }
          public string name = string.Empty;
     }
}
