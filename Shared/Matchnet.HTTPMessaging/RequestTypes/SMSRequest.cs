using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Summary description for SMSRequest.
	/// </summary>
	/// 
	[Serializable]
	public class SMSRequest : IXmlSerializable
    {
        private string _clientId;
        private string _clientKey;
        private string _requestType;

        public string ClientID
        {
            get { return _clientId; }
            set { _clientId = value; }
        }

        public string ClientKey
        {
            get { return _clientKey; }
            set { _clientKey = value; }
        }

        public string RequestType
        {
            get { return _requestType; }
            set { _requestType = value; }
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {

        }

        public virtual void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("clientId", this.ClientID);
            writer.WriteAttributeString("clientKey", this.ClientKey);
            writer.WriteAttributeString("type", this.RequestType);
        }

        #endregion
    }

}
