using System;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Summary description for SMSMessageRequest.
	/// </summary>
	
	[Serializable]
	[XmlRootName("request")]
	public class SMSMessageRequest : SMSRequest
	{
		private string _message;
		private int _carrierId;
		private string _carrierName;
		private string _phoneNumber;

		public string PhoneNumber
		{
			get { return _phoneNumber; }
			set { _phoneNumber = value; }
		}

		public int CarrierId
		{
			get { return _carrierId; }
			set { _carrierId = value; }
		}

		public string CarrierName
		{
			get { return _carrierName; }
			set { _carrierName = value; }
		}

		public string Message
		{
			get { return _message; }
			set { _message = value; }
		}

		/// <summary>
		/// This emtpy constructor is needed for reflection
		/// </summary>
		public SMSMessageRequest()
		{}

		public SMSMessageRequest(string message, string clientID, string clientKey)
		{
			_message = message;

			// Set defaults here, consumer of this object can still override this it wants to
			this.ClientID = clientID;
			this.ClientKey = clientKey;
			this.RequestType = Constants.REQUEST_TYPE_MESSAGE;
		}		

		public override void WriteXml(XmlWriter writer)
		{
			base.WriteXml(writer);

			writer.WriteStartElement("message");
			writer.WriteStartElement("recipient");
			
			writer.WriteStartElement("type");
			writer.WriteString("5");
			writer.WriteEndElement();	// closing tag for type
			
			writer.WriteStartElement("id");
			writer.WriteString(this.PhoneNumber);
			writer.WriteEndElement();	// closing tag for id

//			writer.WriteStartElement("property");
//			writer.WriteStartElement("name");
//			writer.WriteString(HttpUtility.UrlEncode(this.CarrierName));
//			writer.WriteEndElement();	// closing tag for name
//			
//			writer.WriteStartElement("value");
//			writer.WriteString(this.CarrierId.ToString());
//			writer.WriteEndElement();	// closing tag for value
//			writer.WriteEndElement();	// closing tag for property
			
			writer.WriteEndElement();	// closing tag for recipient
			
			writer.WriteStartElement("text");
			writer.WriteString(this.Message);
			writer.WriteEndElement();	// closing tag for text

			writer.WriteEndElement();	// closing tag for message
		}
	}
}
