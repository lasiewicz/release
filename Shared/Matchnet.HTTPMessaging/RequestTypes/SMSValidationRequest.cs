using System;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Summary description for SMSValidationRequest.
	/// </summary>
	/// 
	[Serializable]
	[XmlRootName("request")]
    public class SMSValidationRequest : SMSRequest
    {
        private int _carrierId;
        private string _carrierName;
        private string _phoneNumber;

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }

        public int CarrierId
        {
            get { return _carrierId; }
            set { _carrierId = value; }
        }

        public string CarrierName
        {
            get { return _carrierName; }
            set { _carrierName = value; }
        }

		/// <summary>
		/// This emtpy constructor is needed for reflection
		/// </summary>
		public SMSValidationRequest()
		{}

        public SMSValidationRequest(string clientID, string clientKey)
		{
			// Set defaults here, consumer of this object can still override this it wants to
			this.ClientID = clientID;
			this.ClientKey = clientKey;
			this.RequestType = Constants.REQUEST_TYPE_VALIDATION;
		}

        public override void WriteXml(XmlWriter writer)
        {
            base.WriteXml(writer);

            writer.WriteStartElement("validation");
            writer.WriteStartElement("recipient");

            writer.WriteStartElement("type");
            writer.WriteString("5");
            writer.WriteEndElement();

            writer.WriteStartElement("id");
            writer.WriteString(this.PhoneNumber);
            writer.WriteEndElement();

//            writer.WriteStartElement("property");
//
//            writer.WriteStartElement("name");
//            writer.WriteString(HttpUtility.UrlEncode(this.CarrierName));
//            writer.WriteEndElement();
//
//            writer.WriteStartElement("value");
//            writer.WriteString(this.CarrierId.ToString());
//            writer.WriteEndElement();
//
//            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndElement();
        }
    }
}
