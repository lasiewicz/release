using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterPhotoCaptionRejected.
	/// </summary>
	public class EnvParameterPhotoCaptionRejected : EnvParameterToMemberBase
	{
		private string m_email;

		public string emailaddr
		{
			get
			{
				return m_email;
			}
		}

        public EnvParameterPhotoCaptionRejected(string siteName, string email, string memberID, DateTime brandInsertDate)
			: base(siteName, memberID, email, brandInsertDate)
		{
			m_email = email;
		}
	}
}
