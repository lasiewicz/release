using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterEcardToMember.
	/// </summary>
	public class EnvParameterEcardToMember : EnvParameterUsernameTextBase
	{
		private string m_cardUrl;
		private string m_senderUsername;
		private string m_senderThumbnail;
		private string m_cardThumbnail;
		private string m_recipientUsername;
		private string m_recipientEmail;
		private string m_sentDate;
		private string m_senderAge;
		private string m_senderLocation;

		#region Properties
		public string cardurl
		{
			get { return m_cardUrl; }
		}

		public string senderusername
		{
			get { return m_senderUsername; }
		}

		public string senderthumbnail
		{
			get { return m_senderThumbnail; }
		}

		public string cardthumbnail
		{
			get { return m_cardThumbnail; }
		}

		public string recipientusername
		{
			get { return m_recipientUsername; }
		}

		public string recipientemailaddress
		{
			get { return m_recipientEmail; }
		}

		public string sentdate
		{
			get { return m_sentDate; }
		}

		public string senderage
		{
			get { return m_senderAge; }
		}

		public string senderlocation
		{
			get { return m_senderLocation; }
		}
		#endregion

		public EnvParameterEcardToMember(string siteName, string cardUrl, string senderUsername, string senderThumbnail,
			string cardThumbnail, string recipientUsername, string recipientEmail, string sentDate, string senderAge,
            string senderLocation, string memberID, string emailAddress, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_cardUrl = cardUrl;
			m_senderUsername = senderUsername;
			m_senderThumbnail = senderThumbnail;
			m_cardThumbnail = cardThumbnail;
			m_recipientUsername = recipientUsername;
			m_recipientEmail = recipientEmail;
			m_sentDate = sentDate;
			m_senderAge = senderAge;
			m_senderLocation = senderLocation;
		}
	}
}
