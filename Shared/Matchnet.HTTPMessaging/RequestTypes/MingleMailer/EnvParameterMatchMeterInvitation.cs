using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterMatchMeterInvitation.
	/// </summary>
	public class EnvParameterMatchMeterInvitation : EnvParameterBase
	{
		private string m_senderUsername;
		private string m_emailAddress;
		private string m_recipientEmail;
		private string m_sendToFriendMemberID;

		#region Properties
		public string senderusername
		{
			get { return m_senderUsername; }
		}

		public string emailaddr
		{
			get { return m_emailAddress; }
		}

		public string recipientemailaddress
		{
			get { return m_recipientEmail; }
		}
		
		public string s2fmemberid
		{
			get { return m_sendToFriendMemberID; }
		}

		#endregion

		public EnvParameterMatchMeterInvitation(string siteName, string senderUsername, string emailAddress, string recipientEmail,
			string sendToFriendMemberID) : base(siteName, emailAddress)
 		{
			m_senderUsername = senderUsername;
			m_emailAddress = emailAddress;
			m_recipientEmail = recipientEmail;
			m_sendToFriendMemberID = sendToFriendMemberID;
		}
	}
}
