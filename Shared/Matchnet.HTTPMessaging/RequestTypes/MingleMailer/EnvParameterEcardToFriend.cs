using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterEcardToFriend.
	/// </summary>
	public class EnvParameterEcardToFriend : EnvParameterBase
	{
		private string m_cardUrl;
		private string m_cardThumbnail;
		private string m_senderUsername;
		private string m_recipientEmail;
		private string m_sentDate;
		private string m_senderAge;
		private string m_senderThumbnail;
		private string m_senderLocation;

		#region Properties
		public string cardurl
		{
			get { return m_cardUrl; }
		}

		public string cardthumbnail
		{
			get { return m_cardThumbnail; }
		}

		public string senderusername
		{
			get { return m_senderUsername; }
		}

		public string recipientemailaddress
		{
			get { return m_recipientEmail; }
		}

		public string sentdate
		{
			get { return m_sentDate; }
		}

		public string senderage
		{
			get { return m_senderAge; }
		}

		public string senderthumbnail
		{
			get { return m_senderThumbnail; }
		}

		public string senderlocation
		{
			get { return m_senderLocation; }
		}
		#endregion

		public EnvParameterEcardToFriend(string siteName, string cardUrl, string cardThumbnail, string senderUsername, string recipientEmail,
			string sentDate, string senderAge, string senderThumbnail, string senderLocation, string emailAddress) : base(siteName, emailAddress)
		{
			m_cardUrl = cardUrl;
			m_cardThumbnail = cardThumbnail;
			m_senderUsername = senderUsername;
			m_recipientEmail = recipientEmail;
			m_sentDate = sentDate;
			m_senderAge = senderAge;
			m_senderThumbnail = senderThumbnail;
			m_senderLocation = senderLocation;
		}
	}
}
