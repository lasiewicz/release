using System;
using System.Collections;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterNewMember.
	/// </summary>
	public class EnvParameterNewMember : EnvParameterUsernameTextBase
	{
		private string m_emailAddress;
		private string m_NMIATimestamp;
		private ArrayList m_miniProfiles;

		#region Properties
		public string emailaddress
		{
			get { return m_emailAddress; }
		}

		public string nmia_timestamp
		{
			get { return m_NMIATimestamp; }
		}

		public string miniprofilecount
		{
			get
			{
				int count = 0;
				if(m_miniProfiles!= null)
				{
					count = m_miniProfiles.Count;
				}

				return count.ToString();
			}
		}

		public ArrayList miniprofiles
		{
			get { return m_miniProfiles; }
		}
		#endregion

        public EnvParameterNewMember(string siteName, string email, string nmiaTimestamp, string memberID, string emailAddress, DateTime brandInsertDate)
			: base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_emailAddress = email;
			m_NMIATimestamp = nmiaTimestamp;
			m_miniProfiles = new ArrayList();
		}

		public void AddMiniProfile(NewMemberMiniProfile miniProfile)
		{
			m_miniProfiles.Add(miniProfile);
		}
	}

	public class NewMemberMiniProfile
	{
		private string m_memberID;
		private string m_thumbNail;
		private string m_name;
		private string m_age;
		private string m_location;

		#region Properties
		public string memberid
		{
			get { return m_memberID; }
		}

		public string thumbnail
		{
			get { return m_thumbNail; }
		}

		public string name
		{
			get { return m_name; }
		}

		public string age
		{
			get { return m_age; }
		}

		public string location
		{
			get { return m_location; }
		}
		#endregion

		public NewMemberMiniProfile(string memberID, string thumbNail, string name, string age, string location)
		{
			m_memberID = memberID;
			m_thumbNail = thumbNail;
			m_name = name;
			m_age = age;
			m_location = location;
		}
	}
}
