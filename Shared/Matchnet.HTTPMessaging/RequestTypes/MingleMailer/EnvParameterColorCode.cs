using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterColorCode.
	/// </summary>
	public class EnvParameterColorCode : EnvParameterBase
	{
		private string m_sendToFriendBody;
		private string m_friendsName;
		private string m_sendersName;
		private string m_sendersMemberID;

		public string sendtofriendbody
		{
			get { return m_sendToFriendBody; }
		}

		/// <summary>
		/// friend's name
		/// </summary>
		public string firstname
		{
			get { return m_friendsName; }
		}

		/// <summary>
		/// sender's name
		/// </summary>
		public string s2ffriendsname
		{
			get { return m_sendersName; }
		}

		/// <summary>
		/// sender's memberid
		/// </summary>
		public string sf2memberid
		{
			get { return m_sendersMemberID; }
		}

		public EnvParameterColorCode(string siteName, string messageBody, string friendsName, string sendersName, string sendersMemberID,
			string emailAddress) : base(siteName, emailAddress)
		{
			m_sendToFriendBody = messageBody;
			m_friendsName = friendsName;
			m_sendersName = sendersName;
			m_sendersMemberID = sendersMemberID;
		}
	}
}
