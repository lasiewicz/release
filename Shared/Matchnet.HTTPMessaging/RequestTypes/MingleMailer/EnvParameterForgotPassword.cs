using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterForgotPassword.
	/// </summary>
	public class EnvParameterForgotPassword : EnvParameterToMemberBase
	{
		private string m_emailAddress;
		private string m_sparkPassword;

		public string emailaddr
		{
			get { return m_emailAddress; }
		}

		public string sparkpassword
		{
			get { return m_sparkPassword; }
		}

        public EnvParameterForgotPassword(string siteName, string emailAddress, string sparkPassword, string memberID, DateTime brandInsertDate)
			: base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_emailAddress = emailAddress;
			m_sparkPassword = sparkPassword;
		}
	}
}
