using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterPhotoRejection.
	/// </summary>
	public class EnvParameterPhotoRejection : EnvParameterToMemberBase
	{
		private string m_comment;

		public string comment
		{
			get { return m_comment; }
		}

        public EnvParameterPhotoRejection(string siteName, string comment, string memberID, string emailAddress, DateTime brandInsertDate)
			: base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_comment = comment;
		}
	}
}
