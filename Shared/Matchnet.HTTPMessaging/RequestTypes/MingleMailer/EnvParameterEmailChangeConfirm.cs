using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterEmailChangeConfirm.
	/// </summary>
	public class EnvParameterEmailChangeConfirm : EnvParameterToMemberBase
	{
		private string m_verifyCode;

		public string verifycode
		{
			get { return m_verifyCode; }
		}
        
		public EnvParameterEmailChangeConfirm(string siteName, string verifyCode, string memberID, string emailAddress, DateTime brandInsertDate)
			: base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_verifyCode = verifyCode;
		}
	}
}
