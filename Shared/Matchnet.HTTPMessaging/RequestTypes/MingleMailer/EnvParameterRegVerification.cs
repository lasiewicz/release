using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterRegVerification.
	/// </summary>
	public class EnvParameterRegVerification : EnvParameterToMemberBase
	{
		private string m_verifyCode;

		public string verifycode
		{
			get { return m_verifyCode; }
		}

		public EnvParameterRegVerification(string siteName, string verifyCode, string memberID, string emailAddress, DateTime brandInsertDate) :
            base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_verifyCode = verifyCode;
		}
	}
}
