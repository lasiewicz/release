﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
    public class EnvParameterAllAccessNudgeEmail : EnvParameterFromToMemberBase
    {
        private string m_messageListId;
        private string m_pixelUID;
        private string m_emailSubject;
        private string m_emailContent;
        private string m_initialEmailDateTime;
        private string m_senderMemberId;

        public string membermailid
        {
            get { return m_messageListId; }
        }

        public string boardid
        {
            get { return m_pixelUID; }
        }

        public string boardname
        {
            get { return m_emailSubject; }
        }

        public string sendtofriendbody
        {
            get { return m_emailContent; }
        }

        public string newmailtime
        {
            get { return m_initialEmailDateTime; }
        }

        public string s2fmemberid
        {
            get { return m_senderMemberId; } 
        }

        public EnvParameterAllAccessNudgeEmail(string messageListId, string pixelUID, string emailSubject, string emailContent, 
            string initialEmailDateTime, string senderMemberId, string senderUsername, string senderThumbnailUrl, string senderAge, string senderGenderSeeking,
            string senderLocation, string siteName, string memberID, string emailAddress, DateTime brandInsertDate) :
            base(senderUsername, senderThumbnailUrl, senderAge, senderGenderSeeking, senderLocation, siteName, memberID, emailAddress, brandInsertDate)
        {
            m_messageListId = messageListId;
            m_pixelUID = pixelUID;
            m_emailSubject = emailSubject;
            m_emailContent = emailContent;
            m_initialEmailDateTime = initialEmailDateTime;
        }
    }
}
