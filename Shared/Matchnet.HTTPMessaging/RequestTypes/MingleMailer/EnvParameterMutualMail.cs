using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterMutualMail.
	/// </summary>
	public class EnvParameterMutualMail : EnvParameterUsernameTextBase
	{
		private string m_username;
		private string m_fromMemberName;
		private string m_mutualMemberNameText;
		private string m_mutualMemberUsername;
		private string m_mutualMemberID;
		private string m_mutualMemberAboutMe;
		private string m_mutualMemberAge;
		private string m_mutualMemberThumbnail;
		private string m_mutualMemberLocation;
		private string m_paidNonPaid;

		#region Properties
		public string username
		{
			get { return m_username; }
		}

		public string frommembername
		{
			get { return m_fromMemberName; }
		}

		public string mutualmembernametext 
		{
			get { return m_mutualMemberNameText; }
		}

		public string mutualmemberusername 
		{
			get { return m_mutualMemberUsername; }
		}

		public string mutualmemberid
		{
			get { return m_mutualMemberID; }
		}

		public string mutualmemberaboutme
		{
			get { return m_mutualMemberAboutMe; }
		}

		public string mutualmemberage
		{
			get { return m_mutualMemberAge; }
		}

		public string mutualmemberthumbnail
		{
			get { return m_mutualMemberThumbnail; }
		}

		public string mutualmemberlocation
		{
			get { return m_mutualMemberLocation; }
		}

		public string plantype
		{
			get { return m_paidNonPaid; }
		}
		#endregion

		public EnvParameterMutualMail(string siteName, string username, string fromMemberName, string mutualMemberNameText,
			string mutualMemberUsername, string mutualMemberID, string mutualMemberAboutMe, string mutualMemberAge,
            string mutualMemberThumbnail, string mutualMemberLocation, string memberID, string emailAddress, string paidNonPaid, DateTime brandInsertDate)
			: base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_username = username;
			m_fromMemberName = fromMemberName;
			m_mutualMemberNameText = mutualMemberNameText;
			m_mutualMemberUsername = mutualMemberUsername;
			m_mutualMemberID = mutualMemberID;
			m_mutualMemberAboutMe = mutualMemberAboutMe;
			m_mutualMemberAge = mutualMemberAge;
			m_mutualMemberThumbnail = mutualMemberThumbnail;
			m_mutualMemberLocation = mutualMemberLocation;
			m_paidNonPaid = paidNonPaid;
		}
	}
}
