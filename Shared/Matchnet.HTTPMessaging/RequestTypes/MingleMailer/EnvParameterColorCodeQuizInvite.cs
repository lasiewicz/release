using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterColorCodeQuizInvite.
	/// </summary>
	public class EnvParameterColorCodeQuizInvite : EnvParameterUsernameTextBase
	{
		private string m_recipientName;
		private string m_sendersUsername;
		private string m_sendersMemberID;
		private string m_recipientGender;

		public string firstname
		{
			get { return m_recipientName; }
		}

		public string s2ffriendsname
		{
			get { return m_sendersUsername; }
		}

		public string s2fmemberid
		{
			get { return m_sendersMemberID; }
		}

		public string comment
		{
			get { return m_recipientGender; }
		}

		public EnvParameterColorCodeQuizInvite(string siteName, string recipientName, string sendersUsername, string sendersMemberID,
            string memberID, string emailAddress, string recipientGender, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_recipientName = recipientName;
			m_sendersUsername = sendersUsername;
			m_sendersMemberID = sendersMemberID;
			m_recipientGender = recipientGender;
		}
	}
}
