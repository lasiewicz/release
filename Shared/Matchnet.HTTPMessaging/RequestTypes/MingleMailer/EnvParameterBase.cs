using System;
using AjaxPro;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for MingleMailerEnvParameterBase.
	/// </summary>
	public class EnvParameterBase
	{
		private string m_siteShortName;
		private string m_emailAddress;

		public string site_shortname
		{
			get
			{
				return m_siteShortName;
			}
		}

		/// <summary>
		/// We need the email address for bounced email processing.
		/// We can't rely on memberID because not all the mail requests will have a memberID.
		/// </summary>
		public string pass_through
		{
			get
			{
				return m_emailAddress;
			}
		}

		/// <summary>
		/// For testing purposes only, this parameter allows us to get through the velocity rule
		/// </summary>
		public string force
		{
			get
			{
				return "true";
			}
		}

		/// <summary>
		/// Send email as html
		/// </summary>
		public string send_html_email
		{
			get 
			{
				return "1";
			}
		}

		public EnvParameterBase(string siteShortName, string emailAddress)
		{
			m_siteShortName = siteShortName;
			m_emailAddress = emailAddress;
		}

		public virtual string GetJsonString()
		{
			return AjaxPro.JavaScriptSerializer.Serialize(this);
		}
	}
}
