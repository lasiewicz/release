using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterSendToFriend.
	/// </summary>
	public class EnvParameterSendToFriend : EnvParameterBase
	{
		private string m_sendToFriendBody;
		private string m_sendToFriendName;
		private string m_sendToFriendMemberID;

		public string sendtofriendbody
		{
			get { return m_sendToFriendBody; }
		}

		public string s2ffriendsname
		{
			get { return m_sendToFriendName; }
		}

		public string s2fmemberid
		{
			get { return m_sendToFriendMemberID; }
		}

		public EnvParameterSendToFriend(string siteName, string messageBody, string friendName, string friendMemberID,
			string emailAddress) : base(siteName, emailAddress)
		{
			m_sendToFriendBody = messageBody;
			m_sendToFriendName = friendName;
			m_sendToFriendMemberID = friendMemberID;
		}
	}
}
