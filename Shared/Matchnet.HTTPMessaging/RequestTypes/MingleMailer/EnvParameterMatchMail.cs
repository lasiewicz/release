using System;
using System.Collections;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterMatchMail.
	/// </summary>
	public class EnvParameterMatchMail	: EnvParameterUsernameTextBase
	{
		private string m_emailAddress;
		private string m_siteID;
		private string m_username;
		private string m_userAge;
		private string m_memberLocation;
		
		private string m_userThumbnail;
		private string m_numberYouHotlisted;
		private string m_numberYouContacted;
		private string m_numberYouTeased;
		private string m_numberYouIM;
		private string m_numberYouViewed;
		private string m_memberSearchUrl;
        private string m_numberHotlistedYou;
		private string m_numberContactedYou;
		private string m_numberTeasedYou;
		private string m_numberIMYou;
		private string m_numberViewedYou;
		private string m_religion;
		private string m_jdateReligion;
		private string m_ethnicity;
		private string m_jdateEthnicity;
		private string m_maritalStatus;
		private string m_childrenCount;
		private string m_custody;
		private string m_isPayingMember;
		private string m_isVerifiedEmail;
		private string m_hasApprovedPhoto;
		private string m_gender;
		private string m_seekingGender;
		private string m_emailVerificationCode;

		private ArrayList m_miniProfiles;

		#region Properties
		public string emailaddress
		{
			get { return m_emailAddress; }
		}

		public string siteid
		{
			get { return m_siteID; }
		}

		public string username
		{
			get { return m_username; }
		}

		public string userage
		{
			get { return m_userAge; }
		}

		public string memberlocation
		{
			get { return m_memberLocation; }
		}

		public string userthumbnail
		{
			get { return m_userThumbnail; }
		}

		public string numberyouhotlisted
		{
			get { return m_numberYouHotlisted; }
		}

		public string numberyoucontacted
		{
			get { return m_numberYouContacted; }
		}

		public string numberyouteased
		{
			get { return m_numberYouTeased; }
		}

		public string numberyouim
		{
			get { return m_numberYouIM; }
		}

		public string numberyouviewed
		{
			get { return m_numberYouViewed; }
		}

		public string membersearchurl
		{
			get { return m_memberSearchUrl; }
		}
		
		public string numberhotlistedyou
		{
			get { return m_numberHotlistedYou; }
		}

		public string numbercontactedyou
		{
			get { return m_numberContactedYou; }
		}

		public string numberteasedyou
		{
			get { return m_numberTeasedYou; }
		}

		public string numberimyou
		{
			get { return m_numberIMYou; }
		}

		public string numberviewedyou
		{
			get { return m_numberViewedYou; }
		}

		public string religion
		{
			get { return m_religion; }
		}

		public string jdatereligion
		{
			get { return m_jdateReligion; }
		}

		public string ethnicity
		{
			get { return m_ethnicity; }
		}

		public string jdateethnicity
		{
			get { return m_jdateEthnicity; }
		}

		public string maritalstatus
		{
			get { return m_maritalStatus; }
		}

		public string childrencount
		{
			get { return m_childrenCount; }
		}

		public string custody
		{
			get { return m_custody; }
		}

		public string ispayingmember
		{
			get { return m_isPayingMember; }
		}

		public string isverifiedemail
		{
			get { return m_isVerifiedEmail; }
		}

		public string hasapprovedphoto
		{
			get { return m_hasApprovedPhoto; }
		}
		
		public string gender
		{
			get { return m_gender; }
		}

		public string seekinggender
		{
			get { return m_seekingGender; }
		}

		public string emailverificationcode 
		{
			get { return m_emailVerificationCode; }
		}

		public ArrayList miniprofiles
		{
			get { return m_miniProfiles; }
		}

		#endregion



		public EnvParameterMatchMail(string siteName, string emailAddress, string siteID, string memberID, string username,
			string userAge, string memberLocation, string userThumbnail, string numberYouHotlisted, string numberYouContacted,
			string numberYouTeased, string numberYouIM, string numberYouViewed, string memberSearchUrl, string numberHotlistedYou,
			string numberContactedYou, string numberTeasedYou, string numberIMYou, string numberViewedYou, string religion,
			string jdateReligion, string ethnicity, string jdateEthnicity, string maritalStatus, string childrenCount, string custody,
			string isPayingMember, string isVerifiedEmail, string hasApprovedPhoto, string gender, string seekingGender,
            string emailVerificationCode, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_emailAddress = emailAddress;
			m_siteID = siteID;
			m_username = username;
			m_userAge = userAge;
			m_memberLocation = memberLocation;
			m_userThumbnail = userThumbnail;
			m_numberYouHotlisted =  numberYouHotlisted;
			m_numberYouContacted = numberYouContacted;
			m_numberYouTeased = numberYouTeased;
			m_numberYouIM = numberYouIM;
			m_numberYouViewed = numberYouViewed; 
			m_memberSearchUrl = memberSearchUrl; 
			m_numberHotlistedYou = numberHotlistedYou;
			m_numberContactedYou = numberContactedYou; 
			m_numberTeasedYou = numberTeasedYou; 
			m_numberIMYou = numberIMYou; 
			m_numberViewedYou = numberViewedYou; 
			m_religion = religion;			
			m_jdateReligion = jdateReligion; 
			m_ethnicity = ethnicity; 
			m_jdateEthnicity = jdateEthnicity; 
			m_maritalStatus = maritalStatus; 
			m_childrenCount = childrenCount; 
			m_custody = custody;
			m_isPayingMember = isPayingMember; 
			m_isVerifiedEmail = isVerifiedEmail; 
			m_hasApprovedPhoto = hasApprovedPhoto; 
			m_gender = gender; 
			m_seekingGender = seekingGender;
			m_emailVerificationCode = emailVerificationCode;

			m_miniProfiles = new ArrayList();
		}

		public void AddMiniProfile(MatchMiniProfile miniProfile)
		{
			miniProfile.indexSetter = m_miniProfiles.Count + 1;
			m_miniProfiles.Add(miniProfile);
		}
	}

	public class MatchMiniProfile
	{
		private string m_memberID;
		private string m_thumbNail;
		private string m_name;
		private string m_age;
		private string m_location;
		private string m_mmvid;
		private string m_matchText;
		private int m_index;

		#region Properties
		public string memberid
		{
			get { return m_memberID; }
		}

		public string thumbnail
		{
			get { return m_thumbNail; }
		}

		public string name
		{
			get { return m_name; }
		}

		public string age
		{
			get { return m_age; }
		}

		public string location
		{
			get { return m_location; }
		}

		/// <summary>
		/// Match Mail Viewing MemberID
		/// </summary>
		public string mmvid
		{
			get { return m_mmvid; }
		}

		public string matchtext
		{
			get { return m_matchText; }
			set { m_matchText = value; }
		}

		public string index
		{
			get { return m_index.ToString(); }
		}

		public int indexSetter
		{
			set { m_index = value; }
		}

		public string column
		{
			get
			{
				int i = m_index % 2;

				if(i == 0)
					i = 2;

				return i.ToString();
			}
		}
		#endregion

		public MatchMiniProfile(string memberID, string thumbNail, string name, string age, string location, string mmvid)
		{
			m_memberID = memberID;
			m_thumbNail = thumbNail;
			m_name = name;
			m_age = age;
			m_location = location;
			m_mmvid = mmvid;			
		}
	}
}
