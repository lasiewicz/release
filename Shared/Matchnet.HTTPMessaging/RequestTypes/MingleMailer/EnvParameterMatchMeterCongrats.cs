using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterMatchMeterCongrats.
	/// </summary>
	public class EnvParameterMatchMeterCongrats : EnvParameterToMemberBase
	{
		private string m_username;
		private string m_emailAddress;
		private string m_recipientEmail;
		private string m_sendToFriendMemberID;

		#region Properties
		public string username
		{
			get { return m_username; }
		}

		public string emailaddr
		{
			get { return m_emailAddress; }
		}

		public string recipientemailaddress
		{
			get { return m_recipientEmail; }
		}

		public string s2fmemberid
		{
			get { return m_sendToFriendMemberID; }
		}
		#endregion

		public EnvParameterMatchMeterCongrats(string siteName, string username, string emailAddress, string recipientEmail,
            string sendToFriendMemberID, string memberID, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_username = username;
			m_emailAddress = emailAddress;
			m_recipientEmail = recipientEmail;
			m_sendToFriendMemberID = sendToFriendMemberID;
		}
	}
}
