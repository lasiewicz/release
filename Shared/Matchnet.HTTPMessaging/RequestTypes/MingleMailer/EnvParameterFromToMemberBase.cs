﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
    public class EnvParameterFromToMemberBase : EnvParameterUsernameTextBase
    {
        private string m_senderUsername;
        private string m_senderThumbnailUrl;
        private string m_senderAge;
        private string m_senderGenderSeeking;
        private string m_senderLocation;

        public string frommembername
        {
            get { return m_senderUsername; }
        }

        public string frommemberthumbnail
        {
            get { return m_senderThumbnailUrl; }
        }

        public string frommemberage
        {
            get { return m_senderAge; }
        }

        public string frommemberseeking
        {
            get { return m_senderGenderSeeking; }
        }

        public string frommemberlocation
        {
            get { return m_senderLocation; }
        }

        public EnvParameterFromToMemberBase(string senderUsername, string senderThumbnailUrl, string senderAge, string senderGenderSeeking,
            string senderLocation, string siteName, string memberID, string emailAddress, DateTime brandInsertDate) :
            base(siteName, memberID, emailAddress, brandInsertDate)
        {
            m_senderUsername = senderUsername;
            m_senderThumbnailUrl = senderThumbnailUrl;
            m_senderAge = senderAge;
            m_senderGenderSeeking = senderGenderSeeking;
            m_senderLocation = senderLocation;
        }
    }
}
