﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
    public class EnvParameterRegistrationCapture:EnvParameterBase
    {
        string _registrationGuid = null;
        int _siteid;
        public int siteid
        {
            get { return _siteid; }
        }

         public string boardid
        {
            get { return _registrationGuid; }
        }
        public EnvParameterRegistrationCapture(int siteId,string siteName, string emailAddress, string registrationguid) : base(siteName, emailAddress)
		{
            _siteid = siteId;
            _registrationGuid = registrationguid;

		}
    }
}
