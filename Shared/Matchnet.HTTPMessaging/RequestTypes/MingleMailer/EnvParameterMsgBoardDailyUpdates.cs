using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterMessageBoardDailyUpdates.
	/// </summary>
	public class EnvParameterMsgBoardDailyUpdates : EnvParameterToMemberBase
	{
		private string m_boardName;
		private string m_username;
		private string m_topicSubject;
		private string m_boardID;
		private string m_threadID;
		private string m_unsubscribeUrl;
		private string m_newReplies;
		private string m_lastMessageID;

		#region Properties
		public string boardname
		{
			get { return m_boardName; }
		}

		public string username
		{
			get { return m_username; }
		}

		public string topicsubject
		{
			get { return m_topicSubject; }
		}

		public string boardid
		{
			get { return m_boardID; }
		}

		public string threadid
		{
			get { return m_threadID; }
		}

		public string unsubscribeurl
		{
			get { return m_unsubscribeUrl; }
		}

		public string newreplies
		{
			get { return m_newReplies; }
		}

		public string lastmessageid
		{
			get { return m_lastMessageID; }
		}
		#endregion

		public EnvParameterMsgBoardDailyUpdates(string siteName, string boardName, string username, string topicSubject,
			string boardID, string threadID, string unsubscribeUrl, string newReplies, string lastMessageID, string memberID,
            string emailAddress, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_boardName = boardName;
			m_username = username;
			m_topicSubject = topicSubject;
			m_boardID = boardID;
			m_threadID = threadID;
			m_unsubscribeUrl = unsubscribeUrl;
			m_newReplies = newReplies;
			m_lastMessageID = lastMessageID;
		}
	}
}
