using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterMemberQuestion.
	/// </summary>
	public class EnvParameterMemberQuestion : EnvParameterBase
	{
		private string m_siteID;
		private string m_username;
		private string m_sendersName;
		private string m_message;
		private string m_sendersMemberID;
		private string m_boardID;
		private string m_boardName;

		#region Properties
		public string siteid
		{
			get { return m_siteID; }
		}

		public string username
		{
			get { return m_username; }
		}

		public string s2ffriendsname
		{
			get { return m_sendersName; }
		}

		public string sendtofriendbody
		{
			get { return m_message; }
		}

		public string s2fmemberid
		{
			get { return m_sendersMemberID; }
		}

		public string boardid
		{
			get { return m_boardID; }
		}

		public string boardname
		{
			get { return m_boardName; }
		}

		#endregion

		public EnvParameterMemberQuestion(string siteName, string siteID, string username, string sendersName, string message,
			string sendersMemberID, string boardID, string boardName, string emailAddress) : base(siteName, emailAddress)
		{
			m_siteID = siteID;
			m_username = username;
			m_sendersName = sendersName;
			m_message = message;
			m_sendersMemberID = sendersMemberID;
			m_boardID = boardID;
			m_boardName = boardName;
		}
	}
}
