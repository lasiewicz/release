using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterMessageBoardInstantNotification.
	/// </summary>
	public class EnvParameterMsgBoardInstantNotification : EnvParameterToMemberBase
	{
		private string m_replyUrl;
		private string m_boardName;
		private string m_username;
		private string m_topicSubject;
		private string m_unsubscribeUrl;
		private string m_newReplies;

		#region Properties
		public string replyurl
		{
			get { return m_replyUrl; }
		}

		public string boardname
		{
			get { return m_boardName; }
		}

		public string username
		{
			get { return m_username; }
		}

		public string topicsubject
		{
			get { return m_topicSubject; }
		}

		public string unsubscribeurl
		{
			get { return m_unsubscribeUrl; }
		}

		public string newreplies
		{
			get { return m_newReplies; }
		}
		#endregion

		public EnvParameterMsgBoardInstantNotification(string siteName, string replyUrl, string boardName, string username,
            string topicSubject, string unsubscribeUrl, string newReplies, string memberID, string emailAddress, DateTime brandInsertDate)
			: base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_replyUrl = replyUrl;
			m_boardName = boardName;
			m_username = username;
			m_topicSubject = topicSubject;
			m_unsubscribeUrl = unsubscribeUrl;
			m_newReplies = newReplies;
		}
	}
}
