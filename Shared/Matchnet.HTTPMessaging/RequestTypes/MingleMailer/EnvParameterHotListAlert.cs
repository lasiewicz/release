using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterHotListAlert.
	/// </summary>
	public class EnvParameterHotListAlert : EnvParameterUsernameTextBase
	{
		private string m_username;
		private string m_hotListedMemberUsername;
		private string m_hotListedMemberAge;
		private string m_hotListedMemberSeeking;
		private string m_hotListedMemberThumbnail;
		private string m_hotListedMemberLocation;
		private string m_genderSelf;
		private string m_subStatus;
		
		#region Properties
		public string username
		{
			get { return m_username; }
		}

		public string hotlistedbymemberusername
		{
			get { return m_hotListedMemberUsername; }

		}

		public string hotlistedbymemberage
		{
			get { return m_hotListedMemberAge; }
		}

		public string hotlistedbymemberseeking
		{
			get { return m_hotListedMemberSeeking; }
		}

		public string hotlistedbymemberthumbnail
		{
			get { return m_hotListedMemberThumbnail; }
		}

		public string hotlistedbymemberlocation
		{
			get { return m_hotListedMemberLocation; }
		}

		/// <summary>
		/// recipient's gender
		/// </summary>
		public string comment
		{
			get { return m_genderSelf; }
		}

		/// <summary>
		/// recipient's sub status
		/// </summary>
		public string plantype
		{
			get { return m_subStatus; }
		}
		#endregion

		public EnvParameterHotListAlert(string siteName, string username, string hotListedMemberUserName, string hotListedMemberAge,
			string hotListedMemberSeeking, string hotListedMemberThumbnail, string hotListedMemberLocation,
            string memberID, string genderSelf, string subStatus, string emailAddress, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_username = username;
			m_hotListedMemberUsername = hotListedMemberUserName;
			m_hotListedMemberAge = hotListedMemberAge;
			m_hotListedMemberSeeking = hotListedMemberSeeking;
			m_hotListedMemberThumbnail = hotListedMemberThumbnail;
			m_hotListedMemberLocation = hotListedMemberLocation;
			m_genderSelf = genderSelf;
			m_subStatus = subStatus;
		}
	}
}
