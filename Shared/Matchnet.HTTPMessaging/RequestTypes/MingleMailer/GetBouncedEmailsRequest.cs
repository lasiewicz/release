using System;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for GetBouncedEmailsRequest.
	/// </summary>
	public class GetBouncedEmailsRequest
	{
		private string m_site;
		private DateTime m_sinceymdt;
		private string m_key;

		public string site
		{
			get { return m_site; }
		}

		public string sinceymdt
		{
			get { return m_sinceymdt.ToString("yyyy/MM/dd HH:mm:ss"); }
		}

		public string key
		{
			get { return m_key; }
		}

		public GetBouncedEmailsRequest(string lSite, DateTime lSinceymdt, string lKey)
		{
			m_site = lSite;
			m_sinceymdt = lSinceymdt;
			m_key = lKey;
		}

		public string GetPostData()
		{
			StringBuilder sb = new StringBuilder();
			bool urlEncode = true;

			sb.Append("site=");
			sb.Append(Utilities.EncodeString(site, urlEncode));
			sb.Append("&sinceymdt=");
			sb.Append(Utilities.EncodeString(sinceymdt, urlEncode));
			sb.Append("&key=");
			sb.Append(Utilities.EncodeString(key, urlEncode));

			return sb.ToString();
		}
	}
}
