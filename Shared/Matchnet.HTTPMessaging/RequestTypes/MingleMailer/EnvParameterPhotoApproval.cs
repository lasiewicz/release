using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for MingleMailerEnvParameterPhotoApproval.
	/// </summary>
	public class EnvParameterPhotoApproval : EnvParameterToMemberBase
	{
		private string m_email;

		public string emailaddr
		{
			get
			{
				return m_email;
			}
		}

        public EnvParameterPhotoApproval(string siteName, string email, string memberID, DateTime brandInsertDate)
			: base(siteName, memberID, email, brandInsertDate)
		{			            
			m_email = email;
		}
	}
}
