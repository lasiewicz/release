﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
    public class EnvParameterAllAccessInitialEmail : EnvParameterFromToMemberBase
    {
        private string m_messageListId;
        private string m_pixelUID;
        private string m_emailSubject;
        private string m_emailContent;
        private string m_plantype;

        public string membermailid
        {
            get { return m_messageListId; }
        }

        public string boardid
        {
            get { return m_pixelUID; }
        }

        public string boardname
        {
            get { return m_emailSubject; }
        }

        public string sendtofriendbody
        {
            get { return m_emailContent; }
        }

        public string plantype
        {
            get { return m_plantype; }
        }

        public EnvParameterAllAccessInitialEmail(string messageListId, string pixelUID, string emailSubject, string emailContent,
            string planType, string senderUsername, string senderThumbnailUrl, string senderAge, string senderGenderSeeking,
            string senderLocation, string siteName, string memberID, string emailAddress, DateTime brandInsertDate) :
            base(senderUsername, senderThumbnailUrl, senderAge, senderGenderSeeking, senderLocation, siteName, memberID, emailAddress, brandInsertDate)
        {
            m_messageListId = messageListId;
            m_pixelUID = pixelUID;
            m_emailSubject = emailSubject;
            m_emailContent = emailContent;
        }

    }
}
