using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterNewMailAlert.
	/// </summary>
	public class EnvParameterNewMailAlert : EnvParameterUsernameTextBase
	{
		private string m_toMemberUsername;
		private string m_numUnreadMessages;
		private string m_newMailTime;
		private string m_fromMemberAge;
		private string m_fromMemberLocation;
		private string m_fromMemberUsername;
		private string m_fromMemberSeeking;
		private string m_fromMemberThumbnail;
		private string m_memberMailID;
		private string m_paidNonPaid;

		#region Properties
		public string username
		{
			get { return m_toMemberUsername; }
		}

		public string numunreadmessages
		{
			get { return m_numUnreadMessages; }
		}

		public string newmailtime
		{
			get { return m_newMailTime; }
		}

		public string frommemberage
		{
			get { return m_fromMemberAge; }
		}

		public string frommemberlocation
		{
			get { return m_fromMemberLocation; }
		}

		public string frommembername
		{
			get { return m_fromMemberUsername; }
		}

		public string frommemberseeking
		{
			get { return m_fromMemberSeeking; }
		}

		public string frommemberthumbnail
		{
			get { return m_fromMemberThumbnail; }
		}

		public string membermailid
		{
			get { return m_memberMailID; }
		}

		public string plantype
		{
			get { return m_paidNonPaid; }
		}
		#endregion

		public EnvParameterNewMailAlert(string siteName, string toMemberUsername, string numUnreadMessages, string newMailTime, 
			string fromMemberAge, string fromMemberLocation, string fromMemberUsername, string fromMemberSeeking, string fromMemberThumbnail,
            string memberMailID, string memberID, string emailAddress, string paidNonPaid, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_toMemberUsername = toMemberUsername;
			m_numUnreadMessages = numUnreadMessages;
			m_newMailTime = newMailTime;
			m_fromMemberAge = fromMemberAge;
			m_fromMemberLocation = fromMemberLocation;
			m_fromMemberUsername = fromMemberUsername;
			m_fromMemberSeeking = fromMemberSeeking; 
			m_fromMemberThumbnail = fromMemberThumbnail;
			m_memberMailID = memberMailID;
			m_paidNonPaid = paidNonPaid;
		}
	}
}
