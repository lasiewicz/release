﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
    public class EnvParameterProfileChangedConfirm : EnvParameterToMemberBase
    {
        private string m_firstname;
        private string m_username;
        private ProfileField[] m_field_change;
        private string m_email;

        #region Properties
        public string firstname
        {
            get { return m_firstname; }
        }

        public string username
        {
            get { return m_username; }
        }

        public string email
        {
            get { return m_email; }
        }

        public ProfileField[] field
        {
            get { return m_field_change; }
        }
        #endregion

        public EnvParameterProfileChangedConfirm(string siteName, string firstName, string memberID,
            string emailAddress, string userName, string[] fields, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_firstname = firstName;
            m_username = userName;
            m_email = emailAddress;

            List<ProfileField> profileFields = new List<ProfileField>();
            foreach (string s in fields)
            {
                profileFields.Add(new ProfileField() { field_name = s });
            }
            m_field_change = profileFields.ToArray();
		}
    }

    public class ProfileField
    {
        public string field_name { get; set; }
    }
}
