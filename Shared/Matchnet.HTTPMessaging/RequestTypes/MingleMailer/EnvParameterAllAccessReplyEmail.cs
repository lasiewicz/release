﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
    public class EnvParameterAllAccessReplyEmail : EnvParameterFromToMemberBase
    {
        private string m_messageListId;
        private string m_emailSubject;
        private string m_emailContent;

        public string membermailid
        {
            get { return m_messageListId; }
        }

        public string boardname
        {
            get { return m_emailSubject; }
        }

        public string sendtofriendbody
        {
            get { return m_emailContent; }
        }

        public EnvParameterAllAccessReplyEmail(string messageListId, string emailSubject, string emailContent,
            string senderUsername, string senderThumbnailUrl, string senderAge, string senderGenderSeeking,
            string senderLocation, string siteName, string memberID, string emailAddress, DateTime brandInsertDate) :
            base(senderUsername, senderThumbnailUrl, senderAge, senderGenderSeeking, senderLocation, siteName, memberID, emailAddress, brandInsertDate)
        {
            m_messageListId = messageListId;
            m_emailSubject = emailSubject;
            m_emailContent = emailContent;
        }

    }
}
