using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterColorCodeQuizConfirm.
	/// </summary>
	public class EnvParameterColorCodeQuizConfirm : EnvParameterUsernameTextBase
	{
		private string m_username;
		private string m_color;

		/// <summary>
		/// member username
		/// </summary>
		public string firstname
		{
			get { return m_username; }
		}

		/// <summary>
		/// color
		/// </summary>
		public string boardid
		{
			get { return m_color; }
		}

        public EnvParameterColorCodeQuizConfirm(string siteName, string username, string color, string memberID, string emailAddress, DateTime brandInsertDate)
			: base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_username = username;
			m_color = color;
		}
	}
}
