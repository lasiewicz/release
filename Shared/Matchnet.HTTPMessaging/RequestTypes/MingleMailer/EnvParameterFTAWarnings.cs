﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
    public class EnvParameterFTAWarnings : EnvParameterToMemberBase
    {
        private FTAWarning[] m_warnings;
        private string m_firstName;
        private string m_lastName;
        private string m_siteName;
        private string m_siteDomain;
        private string m_supportEmail;

        public string userid { get { return memberid; } }
        public string firstname { get { return m_firstName; } }
        public string lastname { get { return m_lastName; } }
        public FTAWarning[] warning
        {
            get { return m_warnings; }
        }
        public string to_email { get { return pass_through; } }
        public string email { get { return pass_through; } }
        public string site_name { get { return m_siteName; } }
        public string site_domain { get { return m_siteDomain; } }
        public string support_email { get { return m_supportEmail; } }

        public EnvParameterFTAWarnings(string siteShortName, string memberID, string emailAddress,
            string firstName, string lastName, string[] warnings, string siteName, string siteDomain, string supportEmail, DateTime brandInsertDate)
            : base(siteShortName, memberID, emailAddress, brandInsertDate)
        {
            m_firstName = firstName;
            m_lastName = lastName;
            m_siteName = siteName;
            m_siteDomain = siteDomain;
            m_supportEmail = supportEmail;

            List<FTAWarning> w = new List<FTAWarning>();
            foreach(string s in warnings)
            {
                w.Add(new FTAWarning() { warning_reason=s});
            }
            m_warnings = w.ToArray();
        }
    }

    public class FTAWarning
    {
        public string warning_reason { get; set; }
    }
}
