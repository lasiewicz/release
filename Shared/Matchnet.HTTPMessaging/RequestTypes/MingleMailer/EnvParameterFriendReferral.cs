﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
    /// <summary>
    /// Parameters for send friend referral email
    /// </summary>
    public class EnvParameterFriendReferral : EnvParameterBase
    {
        private string _messageBody;
		private string _senderName;
        private string _senderMemberID;
        private string _friendsEmail;
        private string _prm;
        private string _lgid;


		public string messageBody
		{
			get { return _messageBody; }
		}

		/// <summary>
		/// sender's name
		/// </summary>
		public string senderName
		{
			get { return _senderName; }
		}

		/// <summary>
		/// sender's memberid
		/// </summary>
		public string senderMemberID
		{
			get { return _senderMemberID; }
		}

        public string friendsEmail
        {
            get { return _friendsEmail; }
        }

        public string prm
        {
            get { return _prm; }
        }

        public string lgid
        {
            get { return _lgid; }
        }

		public EnvParameterFriendReferral(string siteName, string messageBody, string sendersName, string sendersMemberID,
			string emailAddress, string prm, string lgid) : base(siteName, emailAddress)
		{
            _messageBody = messageBody;
            _senderName = sendersName;
            _senderMemberID = sendersMemberID;
            _friendsEmail = emailAddress;
            _lgid = lgid;
            _prm = prm;
		}
    }
}
