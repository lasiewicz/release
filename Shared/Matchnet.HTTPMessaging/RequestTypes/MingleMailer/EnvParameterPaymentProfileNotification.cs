using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterPaymentProfileNotification.
	/// </summary>
	public class EnvParameterPaymentProfileNotification : EnvParameterToMemberBase
	{
		private string m_lastName;
		private string m_firstName;
		private string m_dateOfPurchase;
		private string m_last4CC;
		private string m_confirmationNumber;

		#region Properties
		public string lastname
		{
			get { return m_lastName; }
		}

		public string firstname
		{
			get { return m_firstName; }
		}

		public string dateofpurchase
		{
			get { return m_dateOfPurchase; }
		}

		public string last4cc
		{
			get { return m_last4CC; }
		}

		public string confirmationnumber
		{
			get { return m_confirmationNumber; }
		}

		#endregion

		public EnvParameterPaymentProfileNotification(string siteName, string lastName, string firstName, string dateOfPurchase,
            string last4cc, string confirmationNumber, string memberID, string emailAddress, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_lastName = lastName;
			m_firstName = firstName;
			m_dateOfPurchase = dateOfPurchase;
			m_last4CC = last4cc;
			m_confirmationNumber = confirmationNumber;
		}
	}
}
