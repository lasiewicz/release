using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterRenewalFailureNotification.
	/// </summary>
	public class EnvParameterRenewalFailureNotification : EnvParameterToMemberBase
	{
		private string m_username;

		public string username
		{
			get { return m_username; }
		}

        public EnvParameterRenewalFailureNotification(string siteName, string username, string memberID, string emailAddress, DateTime brandInsertDate)
			: base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_username = username;
		}
	}
}
