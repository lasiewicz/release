using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterJDateMobileRegConfirm.
	/// </summary>
	public class EnvParameterJDateMobileRegConfirm : EnvParameterToMemberBase
	{
		private string m_username;
		private string m_mobileNumber;

		public string username
		{
			get { return m_username; }
		}

		/// <summary>
		/// This is the token name YesMail uses so we have to use this token name
		/// </summary>
		public string confirmationnumber
		{
			get { return m_mobileNumber; }
		}

        public EnvParameterJDateMobileRegConfirm(string siteName, string username, string mobileNumber, string memberID, string emailAddress, DateTime brandInsertDate)
			: base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_username = username;
			m_mobileNumber = mobileNumber;
		}
	}
}
