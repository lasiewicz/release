using System;
using System.Collections;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for MingleMailerSendMailRequest.
	/// </summary>
	public class SendMailRequest
	{
		#region Private Members
		private string m_templateName;
		private EnvParameterBase m_env;
		private ArrayList m_emails;
		private string m_key;
		#endregion

		public SendMailRequest(string templateName, EnvParameterBase envParameter, string email, string key)
		{
			m_templateName = templateName;			
			m_env = envParameter;

			m_emails = new ArrayList();
			AddEmailAddress(email);
			m_key = key;
		}

		public void AddEmailAddress(string emailAddress)
		{
			m_emails.Add(emailAddress);
		}

		public string GetPostData()
		{
			StringBuilder sb = new StringBuilder();
			bool urlEncode = true;

            sb.Append("template=");
			sb.Append(Utilities.EncodeString(m_templateName, urlEncode));
            sb.Append("&env=");
			sb.Append(Utilities.EncodeString(m_env.GetJsonString(), urlEncode));
            sb.Append("&emails=");
			sb.Append(Utilities.EncodeString(AjaxPro.JavaScriptSerializer.Serialize(m_emails), urlEncode));
            sb.Append("&key=");
			sb.Append(Utilities.EncodeString(m_key, urlEncode));

			return sb.ToString();
		}

	}

}
