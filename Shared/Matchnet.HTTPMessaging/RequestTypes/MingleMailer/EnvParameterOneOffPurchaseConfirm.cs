using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterOneOffPurchaseConfirm.
	/// </summary>
	public class EnvParameterOneOffPurchaseConfirm : EnvParameterToMemberBase
	{
		private string m_lastName;
		private string m_firstName;
		private string m_dateOfPurchase;
		private string m_initialCost;
		private string m_initialDuration;
		private string m_initialDurationType;
		private string m_last4CC;
		private string m_creditCardTypeID;
		private string m_confirmationNumber;

		#region Properties
		public string lastname
		{
			get { return m_lastName; }
		}

		public string firstname
		{
			get { return m_firstName; }
		}

		public string dateofpurchase
		{
			get { return m_dateOfPurchase; }
		}

		public string initialcost
		{
			get { return m_initialCost; }
		}

		public string initialduration
		{
			get { return m_initialDuration; }
		}

		public string initialdurationtype
		{
			get { return m_initialDurationType; }
		}

		public string last4cc
		{
			get { return m_last4CC; }
		}

		public string creditcardtypeid
		{
			get { return m_creditCardTypeID; }
		}

		public string confirmationnumber
		{
			get { return m_confirmationNumber; }
		}
		#endregion

		public EnvParameterOneOffPurchaseConfirm(string siteName, string lastName, string firstName, string dateOfPurchase,
			string intialCost, string initialDuration, string initialDurationType, string last4cc, string creditCardTypeID,
            string confirmationNumber, string memberID, string emailAddress, DateTime brandInsertDate)
            : base(siteName, memberID, emailAddress, brandInsertDate)
		{
			m_lastName = lastName;
			m_firstName = firstName;
			m_dateOfPurchase = dateOfPurchase;
			m_initialCost = intialCost;
			m_initialDuration = initialDuration;
			m_initialDurationType = initialDurationType;
			m_last4CC = last4cc;
			m_creditCardTypeID = creditCardTypeID;
			m_confirmationNumber = confirmationNumber;
		}
	}
}
