using System;
using System.Collections;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterClickMail.
	/// </summary>
	public class EnvParameterClickMail : EnvParameterUsernameTextBase
	{
		private string m_email;
		private string m_siteID;
		private string m_username;
		private ArrayList m_miniProfiles;

		#region Properties
		public string emailaddress
		{
			get { return m_email; }
		}

		public string siteid
		{
			get { return m_siteID; }
		}

		public string username
		{
			get { return m_username; }
		}

		public ArrayList miniprofiles
		{
			get { return m_miniProfiles; }
		}
		#endregion

        public EnvParameterClickMail(string siteName, string email, string siteID, string memberID, string userName, DateTime brandInsertDate)
			: base(siteName, memberID, email, brandInsertDate)
		{
			m_email = email;
			m_siteID = siteID;
			m_username = userName;
			m_miniProfiles = new ArrayList();
		}

		public void AddMiniProfile(ClickMailMiniProfile miniProfile)
		{
			miniProfile.indexSetter = m_miniProfiles.Count + 1;
			m_miniProfiles.Add(miniProfile);
		}
	}

	public class ClickMailMiniProfile
	{
		private string m_memberID;
		private string m_thumbNail;
		private string m_name;
		private string m_age;
		private string m_location;
		private string m_nameText;
		private int m_index;

		#region Properties
		public string memberid
		{
			get { return m_memberID; }
		}

		public string thumbnail
		{
			get { return m_thumbNail; }
		}

		public string name
		{
			get { return m_name; }
		}

		public string age
		{
			get { return m_age; }
		}

		public string location
		{
			get { return m_location; }
		}

		public string nametext
		{
			get { return m_nameText; }
		}

		public string index
		{
			get { return m_index.ToString(); }
		}

		public int indexSetter
		{
				set { m_index = value; }
		}

		public string column
		{
			get
			{
				int i = m_index % 2;

				if(i == 0)
					i = 2;

				return i.ToString();
			}
		}

		#endregion

		public ClickMailMiniProfile(string memberID, string thumbNail, string name, string age, string location, string nameText)
		{
			m_memberID = memberID;
			m_thumbNail = thumbNail;
			m_name = name;
			m_age = age;
			m_location = location;
			m_nameText = nameText;

		}

	}
}
