using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterOptOutNotification.
	/// </summary>
	public class EnvParameterOptOutNotification : EnvParameterBase
	{
		public EnvParameterOptOutNotification(string siteName, string emailAddress) : base(siteName, emailAddress)
		{
			
		}
	}
}
