using System;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Base class for email types that have MemberID (recipient's memberID)
	/// </summary>
	public class EnvParameterToMemberBase : EnvParameterBase
	{
		private string m_memberID;
	    private DateTime m_brandInsertDate;

		public string memberid
		{
			get { return m_memberID; }
		}

	    public string join_ymdt
	    {
            get
            {
                return m_brandInsertDate.ToString("yyyy-MM-dd HH:mm:ss");
            }
	    }

		public EnvParameterToMemberBase(string siteName, string memberID, string emailAddress, DateTime brandInsertDate) : base(siteName, emailAddress)
		{
			m_memberID = memberID;
		    m_brandInsertDate = brandInsertDate;
		}
	}
}
