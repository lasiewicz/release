using System;
using Matchnet.Security;

namespace Matchnet.HTTPMessaging.RequestTypes.MingleMailer
{
	/// <summary>
	/// Summary description for EnvParameterUsernameTextBase.
	/// </summary>
	public class EnvParameterUsernameTextBase : EnvParameterToMemberBase
	{
		public const string OUTBOUND_EMAIL_CRYPT_KEY = "A3G6K2S9";

		public string usernametext
		{
			get
			{
				return Crypto.Encrypt(OUTBOUND_EMAIL_CRYPT_KEY, base.memberid);
			}
		}

        public EnvParameterUsernameTextBase(string siteName, string memberID, string emailAddress, DateTime brandInsertDate) : 
			base (siteName, memberID, emailAddress, brandInsertDate)
		{
			
		}
	}
}
