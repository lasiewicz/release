using System;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Summary description for TeleclalSMSMessageRequest.
	/// </summary>
	public class TeleclalSMSMessageRequest 
	{
		private string _Account;
		private string _User;
		private string _Password;
		private string _From;
		private string _To;
		private string _Text;
		private string _Port;
		private string _MessageType;
		private string _URL;

		public string Account
		{
			get { return _Account; }
			set { _Account = value;}
		}
		public string User
		{
			get { return _User; }
			set { _User = value; }
		}
		public string Password
		{
			get { return _Password; }
			set { _Password = value; }
		}
		public string From
		{
			get { return _From; }
			set { _From = value; }
		}
		public string To
		{
			get { return _To; }
			set { _To = value; }
		}
		public string Text
		{
			get { return _Text; }
			set { _Text = value; }
		}
		public string Port
		{
			get { return _Port; }
			set { _Port = value; }
		}
		public string MessageType
		{
			get { return _MessageType; }
			set { _MessageType = value ;}
		}
		public string URL
		{
			get { return _URL; }
			set { _URL = value; }
		}

		public TeleclalSMSMessageRequest()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public TeleclalSMSMessageRequest(string account, string user, string password, string from, string to, string text, string port, string messageType, string url)
		{
			this.Account = account;
			this.User = user;
			this.Password = password;
			this.From = from;
			this.To = to;
			this.Text = text;
			this.Port = port;
			this.MessageType = messageType;
			this.URL = url;
		}

		public string GetPostData()
		{
			StringBuilder sb= new StringBuilder();

			sb.Append("account=");
			sb.Append(Account);
			sb.Append("&user=");
			sb.Append(User);
			sb.Append("&pass=");
			sb.Append(Password);
			sb.Append("&from=");
			sb.Append(From);
			sb.Append("&to=");
			sb.Append(To);
			sb.Append("&text=");
			sb.Append(Text);
			sb.Append("&port=");
			sb.Append(Port);
			if(URL.Length!=0) //If this is a wap link message.
			{
				sb.Append("&msgtype=");
				sb.Append(MessageType);
				sb.Append("&url=");
				sb.Append(URL);
			}
			return sb.ToString();
		}
	}
}
