using System;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Summary description for MessmoSendSMSRequest.
	/// </summary>
	public class MessmoSendSMSRequest : MessmoRequest
	{
		private string _number;
		private string _message;

		public MessmoSendSMSRequest() : base()
		{
			
		}

		public MessmoSendSMSRequest(string command, string messmoKey, string channelNumber, string timeStamp,
			string number, string message, string apiversion) : base (command, messmoKey, channelNumber, timeStamp, apiversion)
		{
			_number = number;
			_message = message;
		}

		public override string GetPostData(bool urlEncode)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("command=");
			sb.Append(Utilities.EncodeString(_command, urlEncode));
			sb.Append("&messmoKey=");
			sb.Append(Utilities.EncodeString(_messmoKey, urlEncode));
			sb.Append("&channelNumber=");
			sb.Append(Utilities.EncodeString(_channelNumber, urlEncode));
			sb.Append("&timestamp=");
			sb.Append(Utilities.EncodeString(_timestamp, urlEncode));

			sb.Append("&number=");
			sb.Append(Utilities.EncodeString(_number, urlEncode));
			sb.Append("&message=");
			sb.Append(Utilities.EncodeString(_message, urlEncode));

			sb.Append("&version=");
			sb.Append(Utilities.EncodeString(_apiVersion, urlEncode));

			return sb.ToString();
		}
	}
}
