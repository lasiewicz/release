using System;
using System.Web;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// This is used for Messmo API 2.5 A Channel Sends Content to a User
	/// </summary>
	public class MessmoSendMessageRequest : MessmoRequest
	{
		#region Private Members		
		private string _subscriberId;
		private string _message;
		private string _attachmentUrl;
		private string _attachmentType;
		private string _taglist;
		private bool _isSmartMessage = false;
		private string _smartMessageId;
		private string _smartMessageTag;
		private bool _isBroadcast = false;
		private bool _deliver = true;
		private bool _isForced = false;
		private string _taggle;
		private string _supplementary;
		private string _expiryDate;
		private NumericBoolean _makeOpen = new NumericBoolean(true);
		private NumericBoolean _notify = new NumericBoolean(true);
		private NumericBoolean _replace = new NumericBoolean(false);
		#endregion

		#region Constructors
		public MessmoSendMessageRequest() : base()
		{
			
		}

		public MessmoSendMessageRequest(string command, string messmoKey, string channelNumber, string timestamp, string subscriberId,
			string message, string taggle, string apiversion) : base (command, messmoKey, channelNumber, timestamp, apiversion)
		{
			_subscriberId = subscriberId;
			_message = message;
			_taggle = taggle;
		}
		
		public MessmoSendMessageRequest(string command, string messmoKey, string channelNumber, string timestamp, string subscriberId,
			string message, string attachmentUrl, string attachmentType, string taglist, bool smartMessage, string smartMessageId,
			string smartMessageTag, bool broadcast, bool deliver, bool force, string taggle, string supplementary, string expiryDate,
			bool makeOpen, bool notify, bool replace, string apiversion)
			: base (command, messmoKey, channelNumber, timestamp, apiversion)
		{
			_subscriberId = subscriberId;
			_message = message;
			_attachmentUrl = attachmentUrl;
			_attachmentType = attachmentType;
			_taglist = taglist;
			_isSmartMessage = smartMessage;
			_smartMessageId = smartMessageId;
			_smartMessageTag = smartMessageTag;
			_isBroadcast = broadcast;
			_deliver = deliver;
			_isForced = force;
			_taggle = taggle;
			_supplementary = supplementary;
			_expiryDate = expiryDate;
			_makeOpen.BoolVal = makeOpen;
			_notify.BoolVal = notify;
			_replace.BoolVal = replace;
		}
		#endregion

		public override string GetPostData(bool urlEncode)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("command=");
			sb.Append(Utilities.EncodeString(_command, urlEncode));
			sb.Append("&messmoKey=");
			sb.Append(Utilities.EncodeString(_messmoKey, urlEncode));
			sb.Append("&channelNumber=");
			sb.Append(Utilities.EncodeString(_channelNumber, urlEncode));
			sb.Append("&timestamp=");
			sb.Append(Utilities.EncodeString(_timestamp, urlEncode));
			sb.Append("&subscriberId=");
			sb.Append(Utilities.EncodeString(_subscriberId, urlEncode));
			sb.Append("&message=");
			sb.Append(Utilities.EncodeString(_message, urlEncode));
			sb.Append("&attachmentUrl=");
			sb.Append(Utilities.EncodeString(_attachmentUrl, urlEncode));
			sb.Append("&attachmentType=");
			sb.Append(Utilities.EncodeString(_attachmentType, urlEncode));
			sb.Append("&taglist=");
			sb.Append(Utilities.EncodeString(_taglist, urlEncode));
			sb.Append("&smartmessage=");
			sb.Append(Utilities.EncodeString(_isSmartMessage, urlEncode));

			sb.Append("&smartmessageId=");
			sb.Append(Utilities.EncodeString(_smartMessageId, urlEncode));

			sb.Append("&smartmessageTag=");
			sb.Append(Utilities.EncodeString(_smartMessageTag, urlEncode));
			sb.Append("&broadcast=");
			sb.Append(Utilities.EncodeString(_isBroadcast, urlEncode));
			sb.Append("&deliver=");
			sb.Append(Utilities.EncodeString(_deliver, urlEncode));
			sb.Append("&forced=");
			sb.Append(Utilities.EncodeString(_isForced, urlEncode));
			sb.Append("&taggle=");
			sb.Append(Utilities.EncodeString(_taggle, urlEncode));
			sb.Append("&supplementary=");
			sb.Append(Utilities.EncodeString(_supplementary, urlEncode));
			sb.Append("&expiryDate=");
			sb.Append(Utilities.EncodeString(_expiryDate, urlEncode));

			sb.Append("&makeOpen=");
			sb.Append(Utilities.EncodeString(_makeOpen, urlEncode));
			sb.Append("&notify=");
			sb.Append(Utilities.EncodeString(_notify, urlEncode));
			sb.Append("&replace=");
			sb.Append(Utilities.EncodeString(_replace, urlEncode));

			sb.Append("&version=");
			sb.Append(Utilities.EncodeString(_apiVersion, urlEncode));
									 
			return sb.ToString();
		}
	
	}
}
