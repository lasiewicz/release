using System;
using System.Web;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Messmo API 2.20 - A Channel Sends a Multi-Part Message to a User
	/// </summary>
	public class MessmoSendMultiPartMessageRequest : MessmoRequest
	{
		#region Private Members		
		private string _subscriberId;
		private string _message;
		private string _attachmentUrl;
		private string _attachmentType;		
		private string _smartMessageXml = string.Empty;
		private string _smartMessageId = "500100"; 
		private string _smartMessageTag;
		private bool _sendCheck = true;		
		private string _taggle;
		private string _supplementary;
		private string _setDate;	// not using it yet but it's a parameter supported by Messmo API 2.20 	
		private NumericBoolean _makeOpen = new NumericBoolean(false);
		private NumericBoolean _notify = new NumericBoolean(true);
		private NumericBoolean _replace = new NumericBoolean(false);
		private NumericBoolean _profile = new NumericBoolean(true);
		private Matchnet.HTTPMessaging.Constants.MessmoButtonType _messmoButtonType = Matchnet.HTTPMessaging.Constants.MessmoButtonType.Reply;
		#endregion

		#region Properties
		public string AttachmentURL
		{
			get { return _attachmentUrl; }
			set { _attachmentUrl = value; }
		}

		public string AttachmentType
		{
			set { _attachmentType = value; }
		}
		#endregion

		#region Constructors
		public MessmoSendMultiPartMessageRequest() : base()
		{
				
		}

		/// <summary>
		/// There are times where you would manually want to control the notify flag (i.e. nightly/daily feeds).  This constructor is for that
		/// purpose.
		/// </summary>
		/// <param name="command">Command parameter that Messmo API uses to identify what command this request should perform</param>
		/// <param name="messmoKey">Key assigned to us by Messmo to user their API</param>
		/// <param name="channelNumber">A channel is Messmo's way of grouping messages (similar to our community concept).
		/// JDate.com has 1 channell associated with it.</param>
		/// <param name="timestamp"></param>
		/// <param name="subscriberId">Messmo subscriberID</param>
		/// <param name="message">Message body</param>
		/// <param name="attachmentUrl">Profile image url</param>
		/// <param name="attachmentType">Profile image file type</param>
		/// <param name="smartMessageTag">Concatenation of the folder name and the user name.  Messmo uses this value to place the message
		/// in the appropriate folder within their app.</param>
		/// <param name="taggle">Taggle holds many values that we need once the user performs an action on this message using their mobile.
		/// It holds sending memberID, recipient memberID, message type (ecard, message, favorites, etc.), and RecipientMessageListID if this
		/// is of regular message (our internal email) type.</param>
		/// <param name="supplementary">Profile information</param>
		/// <param name="replace">Folder name + username acts a key.  If this is set to true, a message is replaced if there exists a
		/// message already at folder name + username.</param>
		/// <param name="notify">If this is set, user will be notified of this message's arrival.  For iPhone users, this would be the
		/// push notification.</param>
		/// <param name="apiversion"></param>
		/// <param name="messmoButtonType">The label text that must be displayed on the button within the message</param>
		public MessmoSendMultiPartMessageRequest(string command, string messmoKey, string channelNumber, string timestamp, string subscriberId,
			string message, string attachmentUrl, string attachmentType, string smartMessageTag, string taggle, string supplementary, bool replace,
			bool notify, Matchnet.HTTPMessaging.Constants.MessmoButtonType messmoButtonType, string apiversion)
			: base (command, messmoKey, channelNumber, timestamp, apiversion)
		{
			_subscriberId = subscriberId;
			_message = message;
			_attachmentUrl = attachmentUrl;
			_attachmentType = attachmentType;
			_smartMessageTag = smartMessageTag;
			_taggle = taggle;
			_supplementary = supplementary;
			_replace.BoolVal = replace;
			_notify.BoolVal = notify;
			_messmoButtonType = messmoButtonType;
		}
	
		#endregion

		public override string GetPostData(bool urlEncode)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("command=");
			sb.Append(Utilities.EncodeString(_command, urlEncode));
			sb.Append("&messmoKey=");
			sb.Append(Utilities.EncodeString(_messmoKey, urlEncode));
			sb.Append("&channelNumber=");
			sb.Append(Utilities.EncodeString(_channelNumber, urlEncode));
			sb.Append("&timestamp=");
			sb.Append(Utilities.EncodeString(_timestamp, urlEncode));
			sb.Append("&subscriberId=");
			sb.Append(Utilities.EncodeString(_subscriberId, urlEncode));
			sb.Append("&message=");
			sb.Append(Utilities.EncodeString(_message, urlEncode));
			sb.Append("&attachmentUrl=");
			sb.Append(Utilities.EncodeString(_attachmentUrl, urlEncode));
			sb.Append("&attachmentType=");
			sb.Append(Utilities.EncodeString(_attachmentType, urlEncode));
			sb.Append("&smartmessage=");
			sb.Append(Utilities.EncodeString(_smartMessageXml, urlEncode));
			sb.Append("&smartmessageId=");
			sb.Append(Utilities.EncodeString(_smartMessageId, urlEncode));
			sb.Append("&smartmessageTag=");
			sb.Append(Utilities.EncodeString(_smartMessageTag, urlEncode));
			sb.Append("&sendCheck=");
			sb.Append(Utilities.EncodeString(_sendCheck, urlEncode));
			sb.Append("&taggle=");
			sb.Append(Utilities.EncodeString(_taggle, urlEncode));
			sb.Append("&supplementary=");
			sb.Append(Utilities.EncodeString(_supplementary, urlEncode));
			sb.Append("&setDate=");
			sb.Append(Utilities.EncodeString(_setDate, urlEncode));
			sb.Append("&makeOpen=");
			sb.Append(Utilities.EncodeString(_makeOpen, urlEncode));
			sb.Append("&notify=");
			sb.Append(Utilities.EncodeString(_notify, urlEncode));
			sb.Append("&replace=");
			sb.Append(Utilities.EncodeString(_replace, urlEncode));
			sb.Append("&profile=");
			sb.Append(Utilities.EncodeString(_profile, urlEncode));
			sb.Append("&version=");
			sb.Append(Utilities.EncodeString(_apiVersion, urlEncode));
			sb.Append("&buttonType=");
			sb.Append(Utilities.EncodeString((int)_messmoButtonType, urlEncode));
									 
			return sb.ToString();
		}
	}
}
