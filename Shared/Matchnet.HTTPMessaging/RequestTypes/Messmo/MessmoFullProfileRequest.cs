using System;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Messmo API 2.23 - A Channel Provides Full Profile Details to Messmo
	/// </summary>
	public class MessmoFullProfileRequest : MessmoRequest
	{
		private string _attachmentUrl;
		private string _attachmentType;
		private string _taggleToken1;
		private string _supplementary;

		#region Properties
		public string AttachmentURL
		{
			get { return _attachmentUrl; }
			set { _attachmentUrl = value; }
		}

		public string AttachmentType
		{
			get { return _attachmentType; }
			set { _attachmentType = value; }
		}
		#endregion

		public MessmoFullProfileRequest() : base()
		{
		
		}

		public MessmoFullProfileRequest(string command, string messmoKey, string channelNumber, string timeStamp, string attachmentUrl,
			string attachmentType, string taggleToken1, string supplementary, string apiversion)
			: base (command, messmoKey, channelNumber, timeStamp, apiversion)
		{
			_attachmentUrl = attachmentUrl;
			_attachmentType = attachmentType;
			_taggleToken1 = taggleToken1;
			_supplementary = supplementary;
		}

		public override string GetPostData(bool urlEncode)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("command=");
			sb.Append(Utilities.EncodeString(_command, urlEncode));
			sb.Append("&messmoKey=");
			sb.Append(Utilities.EncodeString(_messmoKey, urlEncode));
			sb.Append("&channelNumber=");
			sb.Append(Utilities.EncodeString(_channelNumber, urlEncode));
			sb.Append("&timestamp=");
			sb.Append(Utilities.EncodeString(_timestamp, urlEncode));
			
			sb.Append("&attachmentUrl=");
			sb.Append(Utilities.EncodeString(_attachmentUrl, urlEncode));
			sb.Append("&attachmentType=");
			sb.Append(Utilities.EncodeString(_attachmentType, urlEncode));
			sb.Append("&taggleToken1=");
			sb.Append(Utilities.EncodeString(_taggleToken1, urlEncode));
			sb.Append("&supplementary=");
			sb.Append(Utilities.EncodeString(_supplementary, urlEncode));

			sb.Append("&version=");
			sb.Append(Utilities.EncodeString(_apiVersion, urlEncode));

			return sb.ToString();
		}
	}
}
