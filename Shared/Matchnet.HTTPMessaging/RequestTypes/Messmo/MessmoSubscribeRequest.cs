using System;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Messmo API 2.2 - A Channel Subscribes a User
	/// </summary>
	public class MessmoSubscribeRequest : MessmoRequest
	{
		#region Private Members
		private string _number;
		private string _encryptedPassword;		
		#endregion

		#region Constructors
		public MessmoSubscribeRequest() : base()
		{
		}

		public MessmoSubscribeRequest(string command, string messmoKey, string channelNumber, string timestamp,
			string number, string encryptedPassword, string apiversion)
			: base (command, messmoKey, channelNumber, timestamp, apiversion)
		{
			_number = number;
			_encryptedPassword = encryptedPassword;
		}
		#endregion
		
		public override string GetPostData(bool urlEncode)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("command=");
			sb.Append(Utilities.EncodeString(_command, urlEncode));
			sb.Append("&messmoKey=");
			sb.Append(Utilities.EncodeString(_messmoKey, urlEncode));
			sb.Append("&channelNumber=");
			sb.Append(Utilities.EncodeString(_channelNumber, urlEncode));
			sb.Append("&timestamp=");
			sb.Append(Utilities.EncodeString(_timestamp, urlEncode));
			sb.Append("&number=");
			sb.Append(Utilities.EncodeString(_number, urlEncode));
			sb.Append("&password=");
			sb.Append(Utilities.EncodeString(_encryptedPassword, urlEncode));
			sb.Append("&version=");
			sb.Append(Utilities.EncodeString(_apiVersion, urlEncode));

			return sb.ToString();
		}        		
	}
}
