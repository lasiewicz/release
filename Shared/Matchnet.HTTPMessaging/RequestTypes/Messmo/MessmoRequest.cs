using System;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Summary description for MessmoRequest.
	/// </summary>
	abstract public class MessmoRequest
	{
		protected string _command;
		protected string _messmoKey;
		protected string _channelNumber;
		protected string _timestamp;
		protected string _apiVersion;

		public MessmoRequest()
		{
			
		}

		public MessmoRequest(string command, string messmoKey, string channelNumber, string timeStamp, string apiversion)
		{
			_command = command;
			_messmoKey = messmoKey;
			_channelNumber = channelNumber;
			_timestamp = timeStamp;
			_apiVersion = apiversion;
		}

		abstract public string GetPostData(bool urlEncode);
	}

	public class NumericBoolean
	{
		private bool _boolValue;

		public bool BoolVal
		{
			set
			{
				_boolValue = value;
			}
		}
		
		public string StringVal
		{
			get
			{
				if(_boolValue)
					return "1";
				else
					return "0";
			}
		}

		public int IntVal
		{
			get
			{
				if(_boolValue)
					return 1;
				else
					return 0;
			}
		}

		public NumericBoolean(bool boolValue)
		{
			_boolValue = boolValue;
		}
	}
}
