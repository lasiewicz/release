using System;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Messmo API 2.21 - A Channel Sets a User's Transaction Status
	/// </summary>
	public class MessmoTranStatusRequest : MessmoRequest
	{
		private string _subscriberId;
		private string _status;
		private string _setDate;

		public MessmoTranStatusRequest() : base()
		{
			
		}

		public MessmoTranStatusRequest(string command, string messmoKey, string channelNumber, string timestamp, string subscriberId,
			string status, string setDate, string apiVersion)
			: base (command, messmoKey, channelNumber, timestamp, apiVersion)
		{
			_subscriberId = subscriberId;
			_status = status;
			_setDate = setDate;
		}

		public override string GetPostData(bool urlEncode)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("command=");
			sb.Append(Utilities.EncodeString(_command, urlEncode));
			sb.Append("&messmoKey=");
			sb.Append(Utilities.EncodeString(_messmoKey, urlEncode));
			sb.Append("&channelNumber=");
			sb.Append(Utilities.EncodeString(_channelNumber, urlEncode));
			sb.Append("&subscriberId=");
			sb.Append(Utilities.EncodeString(_subscriberId, urlEncode));

			sb.Append("&timestamp=");
			sb.Append(Utilities.EncodeString(_timestamp, urlEncode));
			sb.Append("&status=");
			sb.Append(Utilities.EncodeString(_status, urlEncode));
			sb.Append("&setDate=");
			sb.Append(Utilities.EncodeString(_setDate, urlEncode));

			sb.Append("&version=");
			sb.Append(Utilities.EncodeString(_apiVersion, urlEncode));

			return sb.ToString();
		}
	}
}
