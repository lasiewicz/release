using System;
using System.Text;

namespace Matchnet.HTTPMessaging.RequestTypes
{
	/// <summary>
	/// Messmo API 2.4 call - A Channel Unsubscribes a User
	/// </summary>
	public class MessmoUnsubscribeRequest : MessmoRequest
	{
		private string _subscriberId;

		#region Contructors
		public MessmoUnsubscribeRequest() : base()
		{
			
		}

		public MessmoUnsubscribeRequest(string command, string messmoKey, string channelNumber, string timestamp,
			string subscriberId, string apiVersion) : base(command, messmoKey, channelNumber, timestamp, apiVersion)
		{
			_subscriberId = subscriberId;
		}
		#endregion

		public override string GetPostData(bool urlEncode)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("command=");
			sb.Append(Utilities.EncodeString(_command, urlEncode));
			sb.Append("&messmoKey=");
			sb.Append(Utilities.EncodeString(_messmoKey, urlEncode));
			sb.Append("&channelNumber=");
			sb.Append(Utilities.EncodeString(_channelNumber, urlEncode));
			sb.Append("&timestamp=");
			sb.Append(Utilities.EncodeString(_timestamp, urlEncode));
			
			sb.Append("&subscriberId=");
			sb.Append(Utilities.EncodeString(_subscriberId, urlEncode));

			sb.Append("&version=");
			sb.Append(Utilities.EncodeString(_apiVersion, urlEncode));

			return sb.ToString();
		}
	}
}
