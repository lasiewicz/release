using System;

namespace Matchnet.HTTPMessaging.ResponseTypes
{
	/// <summary>
	/// TeleclalResponse is a value container object to store the return XML from Teleclal get request.
	/// </summary>
	public class TeleclalResponse
	{
		private string _phoneNumber;
        private string _code;
		private string _text;

		public string PhoneNumber
		{
			get { return _phoneNumber; }
		}

		public string ExtensionCode
		{
			get { return _code; }
		}

		public string Text
		{
			get { return _text; }
		}

		public TeleclalResponse(string PhoneNumber, string Code)
		{
			_phoneNumber = PhoneNumber;
			_code = Code;
		}

		public TeleclalResponse(string PhoneNumber, string Code, string Text)
		{
			_phoneNumber = PhoneNumber;
			_code = Code;
			_text = Text;
		}
	}
}
