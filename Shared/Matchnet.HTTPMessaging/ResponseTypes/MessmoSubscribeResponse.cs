using System;

namespace Matchnet.HTTPMessaging.ResponseTypes
{
	/// <summary>
	/// Summary description for MessmoSubscribeResponse.
	/// </summary>
	public class MessmoSubscribeResponse : MessmoResponse
	{
		protected string _subscriberId;
		protected string _isMessmoUser;

		public string SubscriberID
		{
			get
			{
				return _subscriberId;
			}
		}

		public bool IsMessmoUser
		{
			get
			{
				if(_isMessmoUser == null)
					return false;

				return Convert.ToBoolean(_isMessmoUser);
			}

		}
	
		public MessmoSubscribeResponse() : base()
		{
			
		}

		public MessmoSubscribeResponse(string subscriberId, string isMessmoUser) : base()
		{
			_subscriberId = subscriberId;
			_isMessmoUser = isMessmoUser;
		}
	}
}
