using System;

namespace Matchnet.HTTPMessaging.ResponseTypes
{
	/// <summary>
	/// Summary description for TeleclalSMSMessageResponse.
	/// </summary>
	public class TeleclalSMSMessageResponse
	{
		private string _Status;
		private string _Reason;

		public string Status
		{
			get { return _Status; }
		}
		public string Reason
		{
			get { return _Reason; }
		}

		public TeleclalSMSMessageResponse(string status, string reason)
		{
			_Status = status;
			_Reason = reason;
		}
	}
}
