using System;

namespace Matchnet.HTTPMessaging.ResponseTypes
{
	/// <summary>
	/// Summary description for SMSCarrierResponse.
	/// </summary>
	public class SMSCarrierResponse
    {
        private int _carrierId;
        private string _carrierName;

        public SMSCarrierResponse(int carrierId, string carrierName)
        {
            _carrierId = carrierId;
            _carrierName = carrierName;
        }

        public int CarrierId
        {
            get { return _carrierId; }
        }

        public string CarrierName
        {
            get { return _carrierName; }
        }
    }
}
