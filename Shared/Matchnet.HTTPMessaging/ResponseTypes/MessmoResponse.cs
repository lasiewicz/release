using System;

namespace Matchnet.HTTPMessaging.ResponseTypes
{
	/// <summary>
	/// Summary description for MessmoResponse.
	/// </summary>
	public class MessmoResponse
	{
		protected string _status;
		protected string _reason;

		public string Status
		{
			get { return _status; }
		}
		public string Reason
		{
			get { return _reason; }
		}

		public MessmoResponse()
		{
			
		}

		public MessmoResponse(string status, string reason)
		{
			_status = status;
			_reason = reason;
		}
	}
}
