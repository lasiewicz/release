using System;

namespace Matchnet.HTTPMessaging.ResponseTypes
{
	/// <summary>
	/// Summary description for SMSValidationResponse.
	/// </summary>
	public class SMSValidationResponse
    {
        private Constants.SMSStatus _status;
        private string _confirmCode;

        public SMSValidationResponse(Constants.SMSStatus status, string confirmCode)
        {
            _status = status;
            _confirmCode = confirmCode;
        }

        public Constants.SMSStatus Status
        {
            get { return _status; }
        }

        public string ConfirmationCode
        {
            get { return _confirmCode; }
        }
    }
}
