using System;

namespace Matchnet.HTTPMessaging.ResponseTypes
{
	/// <summary>
	/// Summary description for SMSMessageResponse.
	/// </summary>
	public class SMSMessageResponse
    {
        private Constants.SMSStatus _status;

        public SMSMessageResponse(Constants.SMSStatus status)
        {
            _status = status;
        }

        public Constants.SMSStatus Status
        {
            get
            {
                return _status;
            }
        }
    }
}
