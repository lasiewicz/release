using System;
using System.IO;
using System.Text;
using System.Net;
using System.Xml.Serialization;

using Matchnet.HTTPMessaging.RequestTypes;

namespace Matchnet.HTTPMessaging
{
	/// <summary>
	/// Summary description for SMSProxy.
	/// </summary>
	public class SMSProxy
	{
		#region Public Methods
		public static string SendGet(string url)
		{
			HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(url);
			HttpWebResponse rp = (HttpWebResponse)rq.GetResponse();

			using (StreamReader reader = new StreamReader(rp.GetResponseStream()))
			{
				return reader.ReadToEnd();
			}
		}

		public static string SendPost(SMSRequest obj, string PostURL)
		{
			string postData = SerializeObject(obj);

			return SendPost(postData, PostURL);
		}

		public static string SendPost(string postData, string postURL, int requestTimeout, string contentType)
		{
			string responseString = string.Empty;

			// Set all the header information here
			HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(postURL);
			rq.Method = "POST";
			//rq.ContentType = "text/xml";
			rq.ContentType = contentType;

			rq.KeepAlive = false;
			if(requestTimeout!=0)
			{
				rq.Timeout = requestTimeout;
			}
			// Actual message to send
			byte[] bytes = Encoding.ASCII.GetBytes(postData);
			rq.ContentLength = bytes.Length;

			// Sending request
			using (Stream oStreamOut = rq.GetRequestStream())
			{
				oStreamOut.Write(bytes, 0, bytes.Length);
				oStreamOut.Close();
			}

			// Reading the response
			HttpWebResponse rp = (HttpWebResponse)rq.GetResponse();
			Encoding enc = System.Text.Encoding.GetEncoding(1252);
			using (StreamReader oResponseStream = new StreamReader(rp.GetResponseStream(), enc))
			{
				responseString = oResponseStream.ReadToEnd();
				oResponseStream.Close();
				rp.Close();
			}

			return responseString;
		}
		public static string SendPost(string postData, string postURL, int requestTimeout, string contentType, out HttpStatusCode statusCode)
		{
			string responseString = string.Empty;
			statusCode = HttpStatusCode.BadRequest;

			// Set all the header information here
			HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(postURL);
			rq.Method = "POST";
			//rq.ContentType = "text/xml";
			rq.ContentType = contentType;

			rq.KeepAlive = false;
			if(requestTimeout!=0)
			{
				rq.Timeout = requestTimeout;
			}

			// for Fiddler debugging, uncomment this line
//			rq.Proxy = new WebProxy("127.0.0.1", 8888);

			// Actual message to send
			byte[] bytes = Encoding.ASCII.GetBytes(postData);
			rq.ContentLength = bytes.Length;

			// Sending request
			using (Stream oStreamOut = rq.GetRequestStream())
			{
				oStreamOut.Write(bytes, 0, bytes.Length);
				oStreamOut.Close();
			}

			// Reading the response
			HttpWebResponse rp = null;
			Encoding enc = System.Text.Encoding.GetEncoding(1252);
			try
			{
				rp = (HttpWebResponse)rq.GetResponse();
				statusCode = rp.StatusCode;
				
				using (StreamReader oResponseStream = new StreamReader(rp.GetResponseStream(), enc))
				{
					responseString = oResponseStream.ReadToEnd();
					oResponseStream.Close();					
				}
			}
			catch(WebException ex)
			{
				if(ex.Message.ToLower().IndexOf("the operation has timed out") >= 0)
				{
					throw ex;
				}
				else
				{
					// if exception is thrown, get the response from the WebException object
					rp = (HttpWebResponse)ex.Response;
					statusCode = rp.StatusCode;				
					using (StreamReader oResponseStream = new StreamReader(rp.GetResponseStream(), enc))
					{
						responseString = oResponseStream.ReadToEnd();
						oResponseStream.Close();					
					}
				}
			}
			finally
			{
				if(rp != null)
					rp.Close();
			}

			return responseString;
		}
		#endregion

		#region Private Methods
		private static string SendPost(string postData, string PostURL)
		{
			return SendPost(postData, PostURL,  0, "text/xml");
			/*
			string responseString = string.Empty;

			// Set all the header information here
			HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(PostURL);
			rq.Method = "POST";
			rq.ContentType = "text/xml";
			rq.KeepAlive = false;
			// Actual message to send
			byte[] bytes = Encoding.ASCII.GetBytes(postData);
			rq.ContentLength = bytes.Length;

			// Sending request
			using (Stream oStreamOut = rq.GetRequestStream())
			{
				oStreamOut.Write(bytes, 0, bytes.Length);
				oStreamOut.Close();
			}

			// Reading the response
			HttpWebResponse rp = (HttpWebResponse)rq.GetResponse();
			Encoding enc = System.Text.Encoding.GetEncoding(1252);
			using (StreamReader oResponseStream = new StreamReader(rp.GetResponseStream(), enc))
			{
				responseString = oResponseStream.ReadToEnd();
				oResponseStream.Close();
				rp.Close();
			}

			return responseString;
			*/
		}

		private static string SerializeObject(object o)
		{
			string ret = string.Empty;
			Stream writer = new MemoryStream();
			XmlSerializer ser = null;
			XmlRootAttribute ra = new XmlRootAttribute();
			Type objectType = o.GetType();
			// determines if the custom attribute was found
			bool bAttFound = false;

			ra.Namespace = string.Empty;               
			// try and find the custom attribute that will determine the root namespace.
			object [] oAtt = objectType.GetCustomAttributes(typeof(Matchnet.HTTPMessaging.XmlRootNameAttribute), false);
			if (oAtt != null)
			{
				if (oAtt.Length > 0)
				{
					bAttFound = true;
					// use the given root name
					ra.ElementName = ((Matchnet.HTTPMessaging.XmlRootNameAttribute)oAtt[0]).name;
				}
			}

			if (!bAttFound)
			{
				// if there is no root name provided, then just use the default
				ser = new XmlSerializer(objectType);
			}
			else
			{
				// otherwise use the provided root name
				ser = new XmlSerializer(objectType, ra);
			}

			XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

			ns.Add("","");  

			if (bAttFound)
			{
				// use the given root when provided
				ser.Serialize(writer, o, ns);
			}
			else
			{               
				// otherwise use the default (no root provided)
				ser.Serialize(writer, o);
			}

			// read the string from the serialized stream
			writer.Position = 0;
			StreamReader sr = new StreamReader(writer,System.Text.Encoding.ASCII);
			ret = sr.ReadToEnd();

			// Close all the stream objects
			writer.Close();
			sr.Close();

			return ret;
		}
		#endregion
	}
}
