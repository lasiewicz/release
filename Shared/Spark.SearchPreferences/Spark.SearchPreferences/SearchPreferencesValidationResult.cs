﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchPreferences
{
    public class SearchPreferencesValidationResult
    {
        private static readonly SearchPreferencesValidationResult _succesResult = new SearchPreferencesValidationResult();
		private PreferencesValidationStatus _status;
		private PreferencesValidationError _validationError;

		/// <summary>
		/// Default constructor will initialize this instance as a 'successful' validation result.
		/// </summary>
		public SearchPreferencesValidationResult()
		{
			_status = PreferencesValidationStatus.Success;
			_validationError = PreferencesValidationError.Success;
		}

		/// <summary>
		/// Overloaded constructor used for initializing a 'failed' validation with the appropriate Validation Error.
		/// </summary>
		/// <param name="pValidationError"></param>
		public SearchPreferencesValidationResult(PreferencesValidationError pValidationError)
		{
			_status = PreferencesValidationStatus.Failed;
			_validationError = pValidationError;
		}

		/// <summary>
		/// Indicates the status (success/fail) of this validation result.
		/// </summary>
		public PreferencesValidationStatus Status
		{
			get
			{
				return _status;
			}
		}

		/// <summary>
		/// Indicates the validation error for this validation result.
		/// </summary>
		public PreferencesValidationError ValidationError
		{
			get
			{
				return _validationError;
			}
		}

		/// <summary>
		/// A ValidationResult indicating successful validation of search preferences..
		/// </summary>
		/// <returns></returns>
		public static SearchPreferencesValidationResult Success()
		{
			return _succesResult;
		}
	}

	/// <summary>
	/// Status of search preferences validation (Success / Fail).
	/// </summary>
	public enum PreferencesValidationStatus
	{
		/// <summary>
		/// Indicates successful validation of search preferences.
		/// </summary>
		Success,
		/// <summary>
		/// Indicates failed validation of search preferences.
		/// </summary>
		Failed
	}

	/// <summary>
	/// Validation Error type for results with a Failed validation status.
	/// </summary>
	public enum PreferencesValidationError
	{
		/// <summary>
		/// Indicates that Distance is a required field.
		/// </summary>
		DistanceRequired,

		/// <summary>
		/// Indicates a successful validation, no error.
		/// </summary>
		Success,

		/// <summary>
		/// Indicates that SearchTypeID is a required field.
		/// </summary>
		SearchTypeIDRequired,
		
		/// <summary>
		/// Indicates that SearchOrderBy (QuerySorting) is a required field.
		/// </summary>
		SearchOrderByRequired,

		/// <summary>
		/// Indicates taht SchoolID is a required field.
		/// </summary>
		SchoolIDRequired,

		/// <summary>
		/// Indicates that CountryRegionID is required for AreaCode search type.
		/// </summary>
		AreaCodeRequiresCountryRegionID,

		/// <summary>
		/// Indicates that a valid area code field is required.
		/// </summary>
		AreaCodeRequired,

		/// <summary>
		/// Indicates that RegionID is a required field for the selected type of search..
		/// </summary>
		RegionIDRequired
    }
}
