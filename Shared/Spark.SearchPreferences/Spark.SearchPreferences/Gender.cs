﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchPreferences
{
    /// <summary>
    /// Summary description for Gender.
    /// </summary>
    public enum Gender
    {
        Male = 1,
        Female = 2
    }
}
