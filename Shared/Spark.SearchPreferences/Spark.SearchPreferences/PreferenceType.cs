﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchPreferences
{
    /// <summary>
    /// Summary description for SearchPreferenceType.
    /// </summary>
    public enum PreferenceType
    {
        Int = 1,
        Mask = 2,
        Range = 3,
        Text = 4
    }
}
