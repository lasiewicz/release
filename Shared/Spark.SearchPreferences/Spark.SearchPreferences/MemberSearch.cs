﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchPreferences
{
    [Serializable]
    public class MemberSearch
    {
        public int MemberID { get; set; }
        public int GroupID { get; set; }
        public string SearchName { get; set; }
        public bool IsPrimary { get; set; }
        public SearchPreferenceCollection SearchPreferenceCollection { get; private set; }

        public MemberSearch(int memberID, 
            int groupID, 
            string searchName, 
            bool isPrimary, 
            SearchPreferenceCollection searchPreferenceCollection)
        {
            if (searchPreferenceCollection == null || !searchPreferenceCollection.Any())
            {
                throw new ArgumentNullException("searchPreferenceCollection");
            }

            /*var validationResult = searchPreferenceCollection.Validate();
            if(validationResult.Status != PreferencesValidationStatus.Success)
            {
                 throw new Exception("Invalid search parameters");
            }*/

            MemberID = memberID;
            GroupID = groupID;
            SearchName = searchName;
            IsPrimary = isPrimary;
            SearchPreferenceCollection = searchPreferenceCollection;
        }

    }
}
