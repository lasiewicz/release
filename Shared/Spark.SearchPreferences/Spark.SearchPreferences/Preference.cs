﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchPreferences
{
    public class Preference
    {
        public int PreferenceId { get; set; }
        public PreferenceType PreferenceType { get; set; }
        public int AttributeId { get; set; }
        public string PreferenceName { get; set; }
    }
}
