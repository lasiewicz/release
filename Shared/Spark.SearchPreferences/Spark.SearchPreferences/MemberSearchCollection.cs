﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;

namespace Spark.SearchPreferences
{
    [Serializable]
    public class MemberSearchCollection : IEnumerable<MemberSearch>, IReplicable
    {
        private List<MemberSearch> _memberSearches;
        public static string DEFAULTSEARCHNAME = "Default";

        public MemberSearchCollection()
        {
            _memberSearches = new List<MemberSearch>();
        }

        public void Add(MemberSearch memberSearch)
        {
            _memberSearches.Add(memberSearch);
        }

        public void AddRange(List<MemberSearch> memberSearches)
        {
            _memberSearches.AddRange(memberSearches);
        }

        public void Remove(MemberSearch memberSearch)
        {
            _memberSearches.RemoveAll(ms => ms.SearchName.ToLower() == memberSearch.SearchName.ToLower());
        }

        public void RemovePrimary()
        {
            _memberSearches.RemoveAll(ms => ms.IsPrimary);
        }

        IEnumerator<MemberSearch> IEnumerable<MemberSearch>.GetEnumerator()
        {
            return _memberSearches.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _memberSearches.GetEnumerator();
        }

        public MemberSearch GetPrimarySearch()
        {
            return (from ms in _memberSearches where ms.IsPrimary == true select ms).FirstOrDefault();
        }

        #region ICacheable Members

        private int _CacheTTLSeconds;
        private CacheItemPriorityLevel _CachePriority;
        private string _CacheKey;

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _CacheTTLSeconds;
            }
            set
            {
                _CacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Sliding;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _CachePriority;
            }
            set
            {
                _CachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return _CacheKey;
        }

        public string SetCacheKey
        {
            set
            {
                _CacheKey = value;
            }
        }

        #endregion

        #region IReplicable Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion
    }
}
