﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Matchnet;

namespace Spark.SearchPreferences
{
    [Serializable]
    public class SearchPreferenceCollection : IEnumerable<SearchPreference>, IReplicable
    {
        public bool IsDefaultPreferences { get; set; }
        private Dictionary<string, SearchPreference> _preferencesByName;

        public SearchPreferenceCollection()
        {
            IsDefaultPreferences = false;
            _preferencesByName = new Dictionary<string, SearchPreference>();
        }

        public void Add(SearchPreference searchPreference)
        {
            if (_preferencesByName.ContainsKey(searchPreference.Name.ToLower()))
            {
                _preferencesByName[searchPreference.Name.ToLower()] = searchPreference;
            }
            else
            {
                _preferencesByName.Add(searchPreference.Name.ToLower(), searchPreference);
            }
        }

        public SearchPreference this[string name]
        {
            get
            {
                if (_preferencesByName.ContainsKey(name.ToLower()))
                {
                   return _preferencesByName[name.ToLower()];
                }
                return null;
            }

            set
            {
                if (_preferencesByName.ContainsKey(name.ToLower()))
                {
                    _preferencesByName[name.ToLower()] = value;
                }
                else
                {
                    _preferencesByName.Add(name.ToLower(), value);
                }
            }
        }

        public int GetIntValue(string key)
        {
            if (this[key] == null)
            {
                return Constants.NULL_INT;
            }
            return Conversion.CInt(this[key].Value);
        }

        public bool Contains(string preferenceName)
        {
            return _preferencesByName.ContainsKey(preferenceName.ToLower());
        }

        public void Remove(string preferenceName)
        {
            if (Contains(preferenceName))
            {
                _preferencesByName.Remove(preferenceName.ToLower());
            }
        }

        public SearchPreferencesValidationResult Validate()
        {
            int iSearchTypeID = GetIntValue(SearchPreferenceConstants.SearchType);
            int iSearchOrderBy = GetIntValue(SearchPreferenceConstants.OrderBy);
            
            int iRegionID = GetIntValue(SearchPreferenceConstants.PreferredRegionId) != Constants.NULL_INT ?
                GetIntValue(SearchPreferenceConstants.PreferredRegionId) :
                GetIntValue(SearchPreferenceConstants.RegionID);
            int iDistance = GetIntValue(SearchPreferenceConstants.PreferredDistance) != Constants.NULL_INT ?
                GetIntValue(SearchPreferenceConstants.PreferredDistance) :
                GetIntValue(SearchPreferenceConstants.Distance);
            int iRadius = GetIntValue(SearchPreferenceConstants.PreferredRadius) != Constants.NULL_INT ?
               GetIntValue(SearchPreferenceConstants.PreferredRadius) :
               GetIntValue(SearchPreferenceConstants.Radius);

            if (iSearchTypeID == Constants.NULL_INT)
            {
                return new SearchPreferencesValidationResult(PreferencesValidationError.SearchTypeIDRequired);
            }

            // SearchOrderBy  is always required.
            if (iSearchOrderBy == Constants.NULL_INT)
            {
                return new SearchPreferencesValidationResult(PreferencesValidationError.SearchOrderByRequired);
            }

            // Validate College type
            if (iSearchTypeID == (int)SearchTypeID.College)
            {
                int iSchoolID = GetIntValue(SearchPreferenceConstants.SchoolID);
                if (iSchoolID == Constants.NULL_INT)
                {
                    return new SearchPreferencesValidationResult(PreferencesValidationError.SchoolIDRequired);
                }
                return SearchPreferencesValidationResult.Success();
            }

            // Validate AreaCode
            if (iSearchTypeID == (int)SearchTypeID.AreaCode)
            {
                if ((this[SearchPreferenceConstants.PreferredAreaCodes] != null && string.IsNullOrEmpty(this[SearchPreferenceConstants.PreferredAreaCodes].Value))
                    || (this[SearchPreferenceConstants.AreaCode1] != null && string.IsNullOrEmpty(this[SearchPreferenceConstants.AreaCode1].Value)))
                {
                    return new SearchPreferencesValidationResult(PreferencesValidationError.AreaCodeRequired);
                }
                return SearchPreferencesValidationResult.Success();
            }

            // City / Region Search
            // RegionID is always required (need to compute Longitude and Latitude values).
            if (iRegionID <= 0)
            {
                return new SearchPreferencesValidationResult(PreferencesValidationError.RegionIDRequired);
            }

            // SearchOrderBy was tested for null above.
            if (iSearchOrderBy != (int)QuerySorting.Proximity)
            {
                if (iDistance == Constants.NULL_INT && iRadius == Constants.NULL_INT)
                {
                    //either distance or radius can be used, but at least one must be present
                    return new SearchPreferencesValidationResult(PreferencesValidationError.DistanceRequired);
                }
            }
            return SearchPreferencesValidationResult.Success();
        }

        public string SerializeToXML()
        {
            var searchPreferences = new SearchPreferences();
            searchPreferences.Preferences.AddRange(this);

            var serializer = new XmlSerializer(typeof(SearchPreferences));
            var textWriter = new StringWriter();
            serializer.Serialize(textWriter, searchPreferences);
            return textWriter.ToString();
        }

        public static SearchPreferenceCollection DeserializeFromXML(string preferencesXML)
        {
            var searchPreferenceCollection = new SearchPreferenceCollection();
            var serializer = new XmlSerializer(typeof(SearchPreferences));
            SearchPreferences preferences;

            using (TextReader reader = new StringReader(preferencesXML))
            {
                preferences = (SearchPreferences)serializer.Deserialize(reader);
            }

            if (preferences == null || preferences.Preferences == null || preferences.Preferences.Count == 0)
            {
                return null;
            }

            foreach (var searchPreference in preferences.Preferences)
            {
                searchPreferenceCollection.Add(searchPreference);
            }

            return searchPreferenceCollection;
        }

        IEnumerator<SearchPreference> IEnumerable<SearchPreference>.GetEnumerator()
        {
            return ((IEnumerable<SearchPreference>) _preferencesByName.Values).GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<SearchPreference>)_preferencesByName.Values).GetEnumerator();
        }

        [Serializable]
        //this class only exists to enable the xml serializer to easily serialize/deserialize a searchpreferencecollection. should not be used outside this library. 
        public class SearchPreferences
        {
            public SearchPreferences()
            {
                Preferences = new List<SearchPreference>();
            }

            [XmlElement("SearchPreference")]
            public List<SearchPreference> Preferences { get; set; }
        }

        #region ICacheable Members

        private int _CacheTTLSeconds;
        private CacheItemPriorityLevel _CachePriority;
        private string _CacheKey;

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _CacheTTLSeconds;
            }
            set
            {
                _CacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Sliding;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _CachePriority;
            }
            set
            {
                _CachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return _CacheKey;
        }

        public string SetCacheKey
        {
            set
            {
                _CacheKey = value;
            }
        }

        #endregion

        #region IReplicable Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion
    }
}
