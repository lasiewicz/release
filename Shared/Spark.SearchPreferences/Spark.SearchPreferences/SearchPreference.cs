﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Spark.SearchPreferences
{
    [Serializable]
    [XmlRoot(ElementName = "SearchPreference")]
    public class SearchPreference
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("Value")]
        public string Value { get; set; }

        [XmlAttribute("Weight")]
        public int Weight { get; set; }
    }
}
