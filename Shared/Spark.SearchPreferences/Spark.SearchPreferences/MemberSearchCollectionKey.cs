﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchPreferences
{
    public class MemberSearchCollectionKey
    {
        private const string CACHE_KEY_MEMBER = "~MEMBERSEARCHCOLLECTION^{0}^{1}";
        private const string CACHE_KEY_SESSION = "~MEMBERSEARCHCOLLECTION^{0}";
        private MemberSearchCollectionKey()
        { }

        /// <summary>
        /// Generates a unique key for caching an instance of SearchPreferenceCollection for a member / community.
        /// </summary>
        /// <param name="pMemberID"></param>
        /// <param name="pCommunityID"></param>
        /// <returns></returns>
        public static string GetCacheKey(int pMemberID, int pCommunityID)
        {
            return string.Format(CACHE_KEY_MEMBER, pMemberID, pCommunityID);
        }

        /// <summary>
        /// Returns a cache key for preferences related to a user session.
        /// </summary>
        /// <param name="pSessionKey"></param>
        /// <returns></returns>
        public static string GetCacheKey(string pSessionKey)
        {
            return CACHE_KEY_SESSION + "^" + pSessionKey;
        }
    }
}
