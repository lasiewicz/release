﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchPreferences
{
    public enum QuerySorting : int
    {
        JoinDate = 1,
        LastLogonDate = 2,
        Proximity = 3,
        Popularity = 4,
        ColorCode = 5,
        KeywordRelevance = 6,
        MutualMatch = 7
    }

    public enum SearchTypeID
    {
        PostalCode = 1,
        Region = 4,
        AreaCode = 2,
        College = 8
    }
}
