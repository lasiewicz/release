﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchPreferences
{
    public static class SearchPreferenceConstants
    {
        public const string RegionID = "RegionID";
        public const string Distance = "Distance";
        public const string Radius = "Radius";
        public const string MinAge = "MinAge";
        public const string MaxAge = "MaxAge";
        public const string MinHeight = "MinHeight";
        public const string MaxHeight = "MaxHeight";
        public const string GenderMask = "GenderMask";
        public const string AreaCode1 = "AreaCode1";
        public const string SearchType = "SearchTypeID";
        public const string OrderBy = "SearchOrderBy";
        public const string CountryRegionID = "CountryRegionID";
        public const string SchoolID = "SchooldID";
        public const string PreferredRegionId = "preferredregionid";
        public const string PreferredDistance = "preferreddistance";
        public const string PreferredRadius = "preferredradius";
        public const string PreferredAreaCodes = "preferredareacodes";
        
        
        public static List<string> GetAllSearchPreferenceConstants()
        {
            var constants = new List<string> { RegionID, Distance, MinAge, MaxAge, GenderMask, SearchType, OrderBy };

            return constants;
        }
    }

    public static class RangePreferenceConstants
    {
        public const string Birthdate = "Birthdate";
        public const string Height = "Height";
    }
}
