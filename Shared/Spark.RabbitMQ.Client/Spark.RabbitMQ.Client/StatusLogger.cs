﻿#region

using System;
using EasyNetQ;
using log4net;
using log4net.Config;
using Matchnet.Configuration.ValueObjects;
using Spark.Managers.Managers;

#endregion

namespace Spark.RabbitMQ.Client
{
    public class StatusLogger : IEasyNetQLogger
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (StatusLogger));
        private readonly bool _printDebug;

        public StatusLogger()
        {
            _printDebug = new SettingsManager().GetSettingBool(SettingConstants.RABBITMQ_LIBRARY_PRINT_DEBUG_MESSAGES,
                false);
            XmlConfigurator.Configure();
        }

        public void DebugWrite(string format, params object[] args)
        {
            if (_printDebug)
            {
                Log.DebugFormat(format, args);
            }
        }

        public void ErrorWrite(Exception exception)
        {
            Log.Error(exception.Message);
        }

        public void ErrorWrite(string format, params object[] args)
        {
            Log.ErrorFormat(format, args);
        }

        public void InfoWrite(string format, params object[] args)
        {
            Log.DebugFormat(format, args);
        }
    }
}