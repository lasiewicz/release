﻿#region

using System;
using EasyNetQ;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;

#endregion

namespace Spark.RabbitMQ.Client
{
    public class Subscriber : IDisposable
    {
        private readonly IBus _bus;
        private readonly string _hostName;

        /// <summary>
        ///     Use this constructor to use the default values stored in the database
        ///     We're using ActivityRecording RabbitMQ servers but we only have one cluster anway.
        /// </summary>
        public Subscriber()
            : this(RuntimeSettings.Instance.GetSettingFromSingleton(
                SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_SERVER),
                RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_USER_ACCOUNT),
                RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_PASSWORD))
        {
        }

        public Subscriber(string hostName, string userName, string password, int prefectchCount = 1)
        {
            _hostName = hostName;

            if (string.IsNullOrEmpty(hostName))
            {
                throw new ArgumentNullException("hostName", "HostName must be supplied. ");
            }
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException("userName", "UserName must be supplied. ");
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("password", "Password must be supplied. ");
            }

            _bus = RabbitHutch.CreateBus(string.Format("host={0};username={1};password={2};prefetchcount={3}",
                hostName,
                userName,
                password,
                prefectchCount), x => x.Register<IEasyNetQLogger>(_ => new StatusLogger()));

            if (!_bus.IsConnected)
            {
                //couldn't connect with supplied connectionString, so throw exception
                throw new Exception("Could not connect to RabbitMQ server: " + hostName);
            }
        }

        public string HostName
        {
            get { return _hostName; }
        }

        public void Dispose()
        {
            _bus.Dispose();
        }

        public void Subscribe<T>(string subscriptionId, Action<T> action) where T : class
        {
            _bus.Subscribe(subscriptionId, action);
        }

        public void SubscribeToTopic<T>(string subscriptionId, Action<T> action, string topic) where T : class
        {
            _bus.Subscribe(subscriptionId, action, x => x.WithTopic(topic));
        }
    }
}