﻿#region

using System;
using EasyNetQ;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;

#endregion

namespace Spark.RabbitMQ.Client
{
    public class Publisher : IDisposable
    {
        private readonly IBus _bus;

        /// <summary>
        ///     Use this constructor to use the default values stored in the database.
        ///     We're using ActivityRecording RabbitMQ servers but we are using the same cluster for all.
        /// </summary>
        public Publisher()
            : this(RuntimeSettings.Instance.GetSettingFromSingleton(
                SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_SERVER),
                RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_USER_ACCOUNT),
                RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_PASSWORD))
        {
        }

        public Publisher(string hostName, string userName, string password)
        {
            if (string.IsNullOrEmpty(hostName))
            {
                throw new ArgumentNullException("hostName", "HostName must be supplied. ");
            }
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException("userName", "UserName must be supplied. ");
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("password", "Password must be supplied. ");
            }

            _bus = RabbitHutch.CreateBus(string.Format("host={0};username={1};password={2}",
                hostName,
                userName,
                password), x => x.Register<IEasyNetQLogger>(_ => new StatusLogger()));

            if (!_bus.IsConnected)
            {
                //couldn't connect with supplied connectionString, so throw exception
                throw new Exception("Could not connect to RabbitMQ server: " + hostName);
            }
        }

        public void Dispose()
        {
            _bus.Dispose();
        }

        public void Publish<T>(T message) where T : class
        {
            _bus.Publish(message);
        }

        public void PublishTopic<T>(T message, string topic) where T : class
        {
            _bus.Publish(message, topic);
        }
    }
}