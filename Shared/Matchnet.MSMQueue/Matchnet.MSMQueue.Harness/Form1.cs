using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Messaging;
using System.Windows.Forms;
using System.Data;

namespace Matchnet.MSMQueue.Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(88, 264);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(208, 32);
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(16, 16);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textBox1.Size = new System.Drawing.Size(392, 184);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "This is a message";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(40, 208);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(336, 20);
			this.textBox2.TabIndex = 2;
			this.textBox2.Text = "verification_request";
			this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 240);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(392, 16);
			this.label1.TabIndex = 3;
			this.label1.Text = "label1";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(424, 309);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				string queueName = new QueueManager( false, 5000, "CHARGE_SVC", textBox2.Text, Format.Http ).GetLogicalQueue();
				//string queueName = @"FormatName:Direct=http://devapp02/msmq/private$/" + textBox2.Text;

				MessageQueue queue = new MessageQueue( queueName );
				object message = textBox1.Text;
				
				new Queue().Send(
					queue,
					System.Messaging.Message.InfiniteTimeout,
					System.Messaging.Message.InfiniteTimeout,
					new MessageQueue( @"FormatName:Direct=http://dv-chenyard/msmq/private$/subscription_administration" ), //new MessageQueue( @"FormatName:Direct=http://192.168.3.171/msmq/private$/subscription_administration" ),
					System.Messaging.AcknowledgeTypes.FullReachQueue,
					new BinaryMessageFormatter(),
					true,
					true,
					true,
					message);
				queue.Close();
			}
			catch( Exception ex )
			{
				MessageBox.Show( ex.Message );
			}
		}

		private void textBox2_TextChanged(object sender, System.EventArgs e)
		{
			label1.Text = @"FormatName:Direct=OS:192.168.3.51\private$\" + textBox2.Text;
		}
	}
}
