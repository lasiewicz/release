using System;
using System.Diagnostics;
using System.IO;
using System.Messaging;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.InitialConfiguration;

namespace Matchnet.MSMQueue
{
	#region Format Enum

	public enum Format : int
	{
		None,
		Http,
		OS,
		Tcp
	}

	#endregion

	public sealed class QueueManager
	{
		#region Private Members

		ServicePartition servicePartition = null;
		string serviceConstant = Constants.NULL_STRING;
		string queueName = Constants.NULL_STRING;
		bool ssl = false;
		bool isAlive = false;
		int timeOut = 5000;
		Format format = Format.None;
	
		#endregion 

		#region Constructors

		public QueueManager()
		{
		}

		public QueueManager(
			bool ssl,
			int timeOut,
			string serviceConstant,
			string queueName,
			Format format)
		{
			this.ssl = ssl;
			this.timeOut = timeOut;
			this.serviceConstant = serviceConstant;
			this.queueName = queueName;
			this.format = format;
		}

		#endregion

		#region Methods

		public string GetLogicalQueue()
		{
			servicePartition = AdapterConfigurationSA.GetServicePartition( serviceConstant, PartitionMode.None );

			/*
			if ( servicePartition.Count == 1 )
			{
				serviceInstance = servicePartition[ 0 ];
			}
			else
			{
				if ( servicePartition[ 0 ].IsEnabled &&
					 servicePartition[ 1 ].IsEnabled )
				{
					Random rnd = new System.Random();
					int instanceIndex = rnd.Next(0, servicePartition.Count);
					serviceInstance = servicePartition[ instanceIndex ];

					IsUrlAlive();

					if ( ! isAlive )
					{
						serviceInstance = servicePartition[ instanceIndex ^ 1 ];
					}
				}
				else
				{
					serviceInstance = servicePartition[ 0 ].IsEnabled ? servicePartition[ 0 ] : servicePartition[ 1 ];
				}
			}
			*/

			return GetQueueName();
		}

		private string GetUrl()
		{
			string url = "http://";

			if ( ssl )
			{
				url = "https://";
			}

			return url + servicePartition.HostName + "/";
		}

		private string GetQueueName()
		{
			string hostName = servicePartition.HostName;
			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/ChargeServiceMachineNameOverride");
			if ( overrideMachineName != null && overrideMachineName.Length > 0 )
			{
				hostName = overrideMachineName;
			}

			switch ( format )
			{
				case Format.Http:
					if ( ssl )
					{
						return "FormatName:Direct=https://" + hostName + "/msmq/private$/" + queueName;
					}
					else
					{
						return "FormatName:Direct=http://" + hostName + "/msmq/private$/" + queueName;
					}
				case Format.OS:
					return "FormatName:Direct=OS:" + hostName + "/private$/" + queueName;
				case Format.Tcp:
					return "FormatName:Direct=TCP:" + hostName + "/private$/" + queueName;
				default:
					throw new Exception("Connect format not set");
			}

		}

		private void IsUrlAlive()
		{
			string url = Constants.NULL_STRING;

			try
			{
				url = GetUrl();

				if ( url.StartsWith("https") ) 
				{
					ServicePointManager.CertificatePolicy = new OpenCertificatePolicy();
				}

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create( url );
				request.Timeout = timeOut;
				WebResponse response = request.GetResponse();

				StreamReader sr = new StreamReader( response.GetResponseStream(), Encoding.ASCII );    
				string source =  url + " Content: " + sr.ReadToEnd();    
				sr.Close();
				response.Close();
				isAlive = true;
			}
			catch(WebException webEx)
			{
				string message = url + "\r\n" + webEx.Message;

				if ( webEx.Status != WebExceptionStatus.ConnectFailure )
				{
					isAlive = true;
				}
				else
				{
					isAlive = false;
					WriteToEventLog( message );
				}
			}
			catch(Exception ex)
			{
				isAlive = false;
				string message = url + "\r\n" + ex.Message;
				WriteToEventLog( message );
			}
		}

		public class OpenCertificatePolicy : System.Net.ICertificatePolicy 
		{
			public bool CheckValidationResult(ServicePoint srvPoint,
				X509Certificate certificate, WebRequest request, int
				certificateProblem) 
			{
				return true;
			}
		}

		private void WriteToEventLog( string message )
		{
			try
			{
				EventLog el = new EventLog();            
				el.WriteEntry( message, EventLogEntryType.Error );
				el.Close();
			}
			catch{}
		}

		#endregion
	}
}
