using System;
using System.EnterpriseServices;
using System.Messaging;


using Matchnet;

namespace Matchnet.MSMQueue
{
	[Transaction(TransactionOption.Supported, Isolation = TransactionIsolationLevel.ReadCommitted )]
	sealed public class Queue : ServicedComponent
	{

		#region Constructors

		public Queue()
		{
		}

		#endregion

		#region Methods

		public void Send( 
			MessageQueue queue,
			TimeSpan timeToReachQueue,
			TimeSpan timeToBeReceived,
			MessageQueue administrationQueue,
			AcknowledgeTypes acknowledgeType,
			IMessageFormatter formatter,
			bool recoverable,
			bool useDeadLetterQueue,
			bool transactional,
			object msg )
		{
			try
			{
				queue.DefaultPropertiesToSend.Recoverable = recoverable;
				queue.DefaultPropertiesToSend.UseDeadLetterQueue = useDeadLetterQueue;
				queue.DefaultPropertiesToSend.TimeToReachQueue = timeToReachQueue;
				queue.DefaultPropertiesToSend.TimeToBeReceived = timeToBeReceived;
				queue.DefaultPropertiesToSend.AdministrationQueue = administrationQueue;
				queue.DefaultPropertiesToSend.AcknowledgeType = acknowledgeType;
				queue.Formatter = formatter;
				if ( transactional )
				{
					queue.Send( msg, MessageQueueTransactionType.Single );
				}
				else
				{
					queue.Send( msg );
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Unable to send message to queue " + queue.Path, ex);
			}
		}

		#endregion
	}
}
