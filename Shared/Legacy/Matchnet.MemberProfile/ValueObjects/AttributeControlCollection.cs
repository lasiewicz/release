//using System;
//using System.Collections;
//
//using Matchnet.Member;
//using Matchnet.MemberProfile.AttributeControls;
//
//namespace Matchnet.MemberProfile.ValueObjects
//{
//	public class AttributeControlCollection : CollectionBase
//	{
//		public AttributeControlCollection()
//		{
//
//		}
//
//		public IAttributeControl this[int index]
//		{
//			get{return (IAttributeControl)base.InnerList[index];}
//		}
//
//		public int Add(IAttributeControl attributeControl)
//		{
//			return base.InnerList.Add(attributeControl);
//		}
//
//		public void AttachAttribute(Matchnet.Member.AttributeConfig attributeConfig)
//		{
//			foreach(IAttributeControl attributeControl in this)
//			{
//				if(attributeControl.AttributeID == attributeConfig.ID)
//				{
//					attributeControl.AttachAttribute(attributeConfig);
//					// NOTE: Do not bail out of the function because a given attribute
//					//		could be assigned to more than one control
//				}
//			}
//		}
//	}
//}
