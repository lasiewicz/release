//using System;
//using System.Collections;
//using System.ComponentModel;
//using System.Data;
//using System.Web;
//using System.Web.SessionState;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//
//using Matchnet.MemberProfile.AttributeControls;
//using Matchnet.MemberProfile.ValueObjects;
//
//using Matchnet.Lib;
//using Matchnet.Lib.Util;
//using Matchnet.Content;
//using Matchnet.Lib.Data;
//using Matchnet.Lib.Option;
//using Matchnet.Web.Framework;
//using Matchnet.Member;
//
//namespace Matchnet.MemberProfile
//{
//	public class RegStepTemplate : FrameworkControl
//	{
//		private AttributeControlCollection _AttributeControls = new AttributeControlCollection();
//		private Matchnet.Member.Member _Member = null;
//		
//		private void Page_Load(object sender, System.EventArgs e)
//		{
//
//		
//		}
//
//		#region Web Form Designer generated code
//		override protected void OnInit(EventArgs e)
//		{
//			//
//			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
//			//
//			InitializeComponent();
//			base.OnInit(e);
//		}
//		
//		/// <summary>
//		/// Required method for Designer support - do not modify
//		/// the contents of this method with the code editor.
//		/// </summary>
//		private void InitializeComponent()
//		{    
//			this.Load += new System.EventHandler(this.Page_Load);
//		}
//		#endregion
//
//
//		protected override void OnPreRender(EventArgs e)
//		{
//			outputTrace("RegStepControl.Page_Load()");
//
//			// Load the attribute meta data
//			loadAttributeMetaData();
//			
//			if(IsPostBack)
//			{
//				loadPostBack();
//
//				//!!!!!!!!!!!! BEGIN TEST CODE
//				foreach(string key in Request.Form)
//				{
//				//	Response.Write(key + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Request.Form[key] + "<BR>");
//				}
//				//!!!!!!!!!!!! END TEST CODE
//			}
//			else
//			{
//				loadMemberData();
//			}
//
//			base.OnPreRender (e);
//		}
//
//
//		private void loadPostBack()
//		{
//			foreach(IAttributeControl attributeControl in this._AttributeControls)
//			{
//				attributeControl.LoadPostBack(Request.Form);
//			}
//		}
//
//
//		private void loadMemberData()
//		{
//			foreach(IAttributeControl attributeControl in this._AttributeControls)
//			{
//				//attributeControl.LoadPostBack(Request.Form);
//			}
//		}
//
//		protected internal void registerAttributeControl(IAttributeControl control)
//		{
//			_AttributeControls.Add(control);
//			outputTrace("RegStepControl.registerAttributeControl(" + control.GetType() + ")");
//
//		}
//
//		private void validate()
//		{
//			outputTrace("RegStepControl.validate()<BR>");
//
//
//			// STEP 1: Validate custom (non-attribute) controls
//			validateCustom();
//
//			// STEP 2: Validate attribute controls
//			foreach(IAttributeControl attributeControl in _AttributeControls)
//			{
//				attributeControl.Validate(Request.Form);				
//			}
//		}
//
//		protected virtual void validateCustom()
//		{
//			// Derived class may optionally implement this method
//			outputTrace("RegStepControl.validateCustom()");
//		}
//
//		protected internal void save()
//		{
//			outputTrace("RegStepControl.save()<BR>");
//
//			// STEP 1: Validate form post
//			validate();
//
//			// STEP 2: Save custom data from (non-attribute) controls
//			saveCustom();
//
//			// STEP 3: Save attribute control data
//			foreach(IAttributeControl attributeControl in _AttributeControls)
//			{
//				attributeControl.Save();				
//			}
//		}
//
//		protected virtual void saveCustom()
//		{
//			// Derived class may optionally implement this method
//			outputTrace("RegStepControl.saveCustom()");
//		}
//
//
//		private void loadAttributeMetaData()
//		{
//			//-----------------------------------------------
//			//-----------------------------------------------
//			//------- MUST FIX THE FOLLOWING VALUES LATER
//			//-----------------------------------------------
//			//-----------------------------------------------
//			int stepNumber = 1;
//			int privateLabelID = 12;  
//
//			// Load the attribute meta data for this attribute group
//			Member.AttributeConfig.AttributeGroupType attributeGroupForThisStep;
//
//			// Determine the appropriate attribute group for the step
//			switch(stepNumber)
//			{
//				case(1):
//					attributeGroupForThisStep = Member.AttributeConfig.AttributeGroupType.RegStep1;
//					break;
//				case(2):
//					attributeGroupForThisStep = Member.AttributeConfig.AttributeGroupType.RegStep2;
//					break;
//				case(3):
//					attributeGroupForThisStep = Member.AttributeConfig.AttributeGroupType.RegStep3;
//					break;
//				case(4):
//					attributeGroupForThisStep = Member.AttributeConfig.AttributeGroupType.RegStep4;
//					break;
//				case(5):
//					attributeGroupForThisStep = Member.AttributeConfig.AttributeGroupType.RegStep5;
//					break;
//				case(6):
//					attributeGroupForThisStep = Member.AttributeConfig.AttributeGroupType.RegStep6;
//					break;
//				default:
//					attributeGroupForThisStep = Member.AttributeConfig.AttributeGroupType.RegStep1;
//					break;
//			}
//
//            // Load the attribute meta data for this step
//			ArrayList attributeConfigs = Matchnet.Member.AttributeConfig.GetAttributeGroup(privateLabelID, attributeGroupForThisStep);
//
//			if(attributeConfigs != null)
//			{
//				// Assign attribute to the appropriate attribute control(s)
//				foreach(object obj in attributeConfigs)
//				{
//					int test = (int)obj;
//					AttributeConfig attributeConfig = AttributeConfig.GetAttributeConfig(test);
//					_AttributeControls.AttachAttribute(attributeConfig);
//				}
//			}
//
//			//-----------------------------------------------
//			//-----------------------------------------------
//			//-- MUST CODE THE FOLLOWING MEMBER DATA LOAD
//			//-----------------------------------------------
//			//-----------------------------------------------
//
//		}
//
//
//		private void outputTrace(string message)
//		{
//			//Response.Write("<LI>" + message + "<BR>");
//		}
//
//	}
//}
