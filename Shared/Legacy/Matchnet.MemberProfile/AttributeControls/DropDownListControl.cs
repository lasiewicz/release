//using System;
//using System.Collections.Specialized;
//using System.ComponentModel;
//using System.ComponentModel.Design;
//using System.Web.UI;
//using System.Web.UI.Design;
//using System.Web.UI.WebControls;
//
//
//using Matchnet.Member;
//
//namespace Matchnet.MemberProfile.AttributeControls
//{
//
//	[Description("DropDownListControl control that has built-in Attribute capabilities."), Designer(typeof(Matchnet.MemberProfile.AttributeControls.DropDownListControlDesigner)), ToolboxData("<{0}:DropDownListControl runat=server></{0}:DropDownListControl>")]
//
//	public class DropDownListControl : System.Web.UI.WebControls.DropDownList, IAttributeControl
//	{
//		private AttributeConfig _AttributeConfig = null;
//		private int _AttributeID;
//         
//		public DropDownListControl()
//		{
//
//		}
//  
//		[Browsable(true), Bindable(true), Category("Appearance"), Description("ID of the attribute assigned to this control."), DefaultValue("0")] 
//		public int AttributeID
//		{
//			get{return _AttributeID;}
//			set{_AttributeID = value;}
//		}
//
//
//		protected override void OnInit(EventArgs e)
//		{
//			base.OnInit (e);
//
//			// Register this control with the parent control
//			((RegStepTemplate)Parent).registerAttributeControl(this);
//		}
//
//		protected override void Render(HtmlTextWriter writer)
//		{
//			// Write out an error if the Attribute object is null
//			if(_AttributeConfig == null)
//			{
//				writer.Write("<font size=1>ERROR!!!: Cannot display <B>DropDownListControl</B> because no member Attribute object was supplied.</font>");
//				return;
//			}
//
//			// Write out an error if the Attribute object is not in "bit mask" format
//			if(_AttributeConfig.Type != Matchnet.Member.AttributeConfig.AttributeType.SingleSelect)
//			{
//				writer.Write("<font size=1>ERROR!!!: Cannot display <B>DropDownListControl</B> because no member Attribute object is not in 'BIT MASK' format (AttributeID = " + _AttributeConfig.ID + ").</font>");
//				return;
//			}
////
////			// Write out an error if the Attribute object is not in "bit mask" format
////			if(_AttributeConfig.AttributeType2 != AttributeType2.Mask)
////			{
////				writer.Write("<font size=1>ERROR!!!: Cannot display <B>" + this.GetType() + "</B> because no member Attribute object is not in 'BIT MASK' format (AttributeID = " + _AttributeConfig.ID + ").</font>");
////				return;
////			}
//
//
//			// Render...
//			base.Render(writer);
//
//		}
//
//		void IAttributeControl.Validate(NameValueCollection formData)
//		{
//			
//		}
//
//
//		void IAttributeControl.LoadMemberData(string memberAttributeData)
//		{
//
//		}
//
//		void IAttributeControl.Save()
//		{
//
//		}
//
//		      
//		void IAttributeControl.LoadPostBack(NameValueCollection formData)
//		{
//			// Loop through and find the control's key
//			// NOTE: Looping is less dangerous then coding to the auto-generated key
//			foreach(string key in formData)
//			{
//				if(key.EndsWith(this.ID))
//				{
//					// PARSE THIS BAD BOY FOR MULTI-SELECT
//					if(this.Items.FindByValue(formData[key]) != null)
//					{
//						this.Items.FindByValue(formData[key]).Selected = true;
//					}
//					return;
//				}
//			}
//		}
//
//		void IAttributeControl.AttachAttribute(Matchnet.Member.AttributeConfig attributeConfig)
//		{
//			// Attach the Attribute value object to the control
//			_AttributeConfig = attributeConfig;
//
//			// Build the id for this control			
//			this.ID = "_attrib_" + _AttributeConfig.ID + "_";
//
//			// Clear any pre-existing items
//			this.Items.Clear();
//
//			// Loop through and attach the items
////////			foreach(string attributeKey in _AttributeConfig.Options)
////////			{
////////				this.Items.Add(new ListItem(_AttributeConfig.Options[attributeKey], attributeKey));	
////////			}
//		}
//
//	}
//
//	/// <summary>
//	/// Provides support for rendering the DropDownListControl in the "Design" mode through the VisualStudio IDE
//	/// </summary>
//	public class DropDownListControlDesigner : System.Web.UI.Design.ControlDesigner
//	{
//		
//		public DropDownListControlDesigner() : base()
//		{
//
//		}
//
//		public override string GetDesignTimeHtml(  ) 
//		{
//			//return base.GetEmptyDesignTimeHtml();
//			return "<select style='color: #999999;'><option id='1'>[attribute option 1]</option>";
//		}
//	}
//}
//	
