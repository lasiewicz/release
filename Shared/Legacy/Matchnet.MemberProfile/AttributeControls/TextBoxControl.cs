//using System;
//using System.Collections.Specialized;
//using System.ComponentModel;
//using System.Web.UI;
//using System.Web.UI.Design;
//using System.Web.UI.WebControls;
//
//using Matchnet.Member;
//
//namespace Matchnet.MemberProfile.AttributeControls
//{
//	[Description("Simple TextBox control that has built-in Attribute capabilities."), Designer(typeof(Matchnet.MemberProfile.AttributeControls.TextBoxControlDesigner)), DefaultProperty("Text"), ToolboxData("<{0}:TextBoxControl runat=server></{0}:TextBoxControl>")]
//	public class TextBoxControl : System.Web.UI.WebControls.TextBox, IAttributeControl
//	{
//		private AttributeConfig _AttributeConfig = null;
//		private int _AttributeID;
//	
//  
//		[Browsable(true), Bindable(true), Category("Appearance"), Description("ID of the attribute assigned to this control."), DefaultValue("0")] 
//		public int AttributeID
//		{
//			get{return _AttributeID;}
//			set{_AttributeID = value;}
//		}
//
//
//		protected override void OnInit(EventArgs e)
//		{
//			base.OnInit (e);
//
//			// Register this control with the parent control
//			((RegStepTemplate)Parent).registerAttributeControl(this);
//		}
//
//
//		/// <summary> 
//		/// Render this control to the output parameter specified.
//		/// </summary>
//		/// <param name="output"> The HTML writer to write out to </param>
//		protected override void Render(HtmlTextWriter writer)
//		{
//			// Write out an error if the Attribute object is null
//			if(_AttributeConfig == null)
//			{
//				//writer.Write("<font size=1>ERROR!!!: Cannot display <B>" + this.GetType() + "</B> because no member Attribute object was not supplied.</font>");
//				return;
//			}
//
//
//			// Write out an error if the Attribute object is in "bit mask" format
//			if(_AttributeConfig.Type != Matchnet.Member.AttributeConfig.AttributeType.FreeText)
//			{
//				writer.Write("<font size=1>ERROR!!!: Cannot display TextBoxControl because the member AttributeConfig.Type is not in FREE TEXT (AttributeID = " + _AttributeConfig.ID + ").</font>");
//				return;
//			}
//
////			// Write out an error if the Attribute object is in "bit mask" format
////			if(_AttributeConfig.Type == Matchnet.Member.AttributeConfig.AttributeType.Mask)
////			{
////				writer.Write("<font size=1>ERROR!!!: Cannot display TextBoxControl because the member Attribute object is in 'BIT MASK' format (AttributeID = " + _AttributeConfig.ID + ").</font>");
////				return;
////			}
////
////			// Write out an error if the Attribute object is in "date" format
////			if(_AttributeConfig.Type == Matchnet.Member.AttributeConfig.AttributeType.Date)
////			{
////				writer.Write("<font size=1>ERROR!!!: Cannot display TextBoxControl because the member Attribute object is in 'DATE' format ... use the DateControl (AttributeID = " + _AttributeConfig.ID + ").</font>");
////				return;
////			}
////
//			base.Render(writer);
//		}
//
//		      
//		void IAttributeControl.Validate(NameValueCollection formData)
//		{
//			
//		}
//
//
//		void IAttributeControl.LoadMemberData(string memberAttributeData)
//		{
//
//		}
//
//		void IAttributeControl.Save()
//		{
//
//		}
//				      
//		void IAttributeControl.LoadPostBack(NameValueCollection formData)
//		{
//			// Loop through and find the control's key
//			// NOTE: Looping is less dangerous then coding to the auto-generated key
//			foreach(string key in formData)
//			{
//				if(key.EndsWith(this.ID))
//				{
//					this.Text = formData[key];
//					return;
//				}
//			}
//		}
//
//		void IAttributeControl.AttachAttribute(Matchnet.Member.AttributeConfig attributeConfig)
//		{
//			// Attach the Attribute value object to the control
//			_AttributeConfig = attributeConfig;
//
//			// Build the id for this control			
//			this.ID = "_attrib_" + _AttributeConfig.ID + "_";
//		}
//
//	}
//	
//	/// <summary>
//	/// Provides support for rendering the TextBoxControl in the "Design" mode through the VisualStudio IDE
//	/// </summary>
//	public class TextBoxControlDesigner : System.Web.UI.Design.ControlDesigner
//	{
//
//		public TextBoxControlDesigner() : base()
//		{
//
//		}
//
//		// Returns the html to use to represent the control at design time.
//		public override string GetDesignTimeHtml()
//		{
//			return base.GetDesignTimeHtml().Replace("type=\"text\"", "type=\"text\" style=\"color:#666666;background-color:#F0F0F0;\"");
//		}        
//   
//	}
//
//
//}
