using System;

using Matchnet.Content;

namespace Matchnet.AdSense
{
	public class AdSenseConfig
	{

		public static bool IsEnabled(int languageID, int privateLabelID)
		{
			Matchnet.Content.Translator translator = new Translator(Matchnet.Caching.Cache.GetInstance());
			string resourceValue = translator.Value("ADSENSE_DISPLAY_PERCENT", languageID, privateLabelID);
			double displayPercent = 0;

			if (double.TryParse(resourceValue, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out displayPercent))
			{
				if (displayPercent  == 100)
				{
					return true;
				}
				else if (displayPercent > 0)
				{
					int rand = (int)(Math.Floor(100 * (new Random((int)DateTime.Now.Ticks).NextDouble())));

					if (rand <= (int)displayPercent)
					{
						return true;
					}
				}
			}

			return false;
		}


	}
}
