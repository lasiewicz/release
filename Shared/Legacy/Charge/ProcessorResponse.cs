using System;

namespace Matchnet.Core.Charge
{
	public class ProcessorResponse
	{
		private bool mSuccess;
		private string mResponseCode;
		private string mMessage;

		public bool Success 
		{
			get 
			{
				return mSuccess;
			}
			set 
			{
				mSuccess = value;
			}
		}

		public string ResponseCode 
		{
			get 
			{
				return mResponseCode;
			}
			set 
			{
				mResponseCode = value;
			}
		}

		public string Message 
		{
			get 
			{
				return mMessage;
			}
			set 
			{
				mMessage = value;
			}
		}
	}
}
