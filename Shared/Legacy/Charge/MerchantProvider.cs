using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Data;

namespace Matchnet.Core.Charge
{
	public class MerchantProvider
	{
		private int mProviderID;
		private string mProviderName;
		private Hashtable mCache;

		public MerchantProvider(Hashtable cache) 
		{
			mCache = cache;
		}

		public int ProviderID 
		{
			get 
			{
				return mProviderID;
			}
		}

		public string ProviderName 
		{
			get 
			{
				return mProviderName;
			}
		}

		public void Populate(int merchantID) 
		{
			string cacheKey = "MID:" + merchantID;
			if (mCache.ContainsKey(cacheKey)) 
			{
				try 
				{
					MerchantProvider provider = (MerchantProvider)mCache[cacheKey];
					mProviderID = provider.ProviderID;
					mProviderName = provider.ProviderName;
				} 
				catch
				{
					PopulateFromDB(merchantID, cacheKey);
				}
			} 
			else 
			{
				PopulateFromDB(merchantID, cacheKey);
			} 
		}

		private void PopulateFromDB(int merchantID, string cacheKey) 
		{
			try 
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnCharge", 0)); // doesn't matter which one
				string sqlText = "select pr.ProviderID, pr.[Description] from Merchant m (nolock) join Provider pr (nolock) on m.ProviderID = pr.ProviderID where m.MerchantID = " + merchantID;
				DataTable table = client.GetDataTable(sqlText, CommandType.Text);

				if (table != null && table.Rows.Count > 0) 
				{
					mProviderID = System.Convert.ToInt32(table.Rows[0]["ProviderID"]);
					mProviderName = System.Convert.ToString(table.Rows[0]["Description"]);
					mCache.Add(cacheKey, this);
				} 
				else 
				{
					mProviderName = "Unknown";
				}
			} 
			catch 
			{
				mProviderName = "Unknown";
			}
		}
	}
}
