using System;
using System.Collections;

using Matchnet.Core.Charge.JetPay;

namespace Matchnet.Core.Charge
{
	public class ProcessorFactory
	{
		public const int PROVIDER_JETPAY = 6000;

		private static Hashtable mSupportedProcessor = GetSupported();

		private static Hashtable GetSupported() 
		{
			Hashtable supported = new Hashtable();

			// add merchant id's for supported 
			supported.Add(PROVIDER_JETPAY, PROVIDER_JETPAY);

			return supported;
		}

		public static bool HasProcessor(int providerID) 
		{
			return mSupportedProcessor.ContainsKey(providerID);
		}

		public static Processor GetProcessor(int providerID) 
		{
			if (!HasProcessor(providerID)) 
			{
				throw new Exception("Processor not supported for Provider ID " + providerID);
			}

			switch (providerID) 
			{
				case PROVIDER_JETPAY:
					return new JetPayProcessor();
				default:
					throw new Exception("Processor not configured for Provider ID " + providerID);
			}
		}
	}
}
