using System;

namespace Matchnet.Core.Charge
{
	public class MemberTran
	{
		private int mMemberTranID;
		private int mReferenceMemberTranID;
		private int mMemberID;
		private int mTranTypeID;
		private int mMemberSubID;
		private int mPlanID;
		private int mDiscountID;
		private int mMemberPaymentID;
		private int mMemberTranStatusID;
		private int mAdminMemberID;
		private decimal mAmount;
		private int mNumDays;
		private string mAccountNumber;
		private int mTerminationReasonID;
		private DateTime mInsertDate;
		private DateTime mUpdateDate;

		public int MemberTranID 
		{
			get 
			{
				return mMemberTranID;
			}
			set 
			{
				mMemberTranID = value;
			}
		}
		
		public int ReferenceMemberTranID 
		{
			get 
			{
				return mReferenceMemberTranID;
			}
			set 
			{
				mReferenceMemberTranID = value;
			}
		}

		public int MemberID 
		{
			get 
			{
				return mMemberID;
			}
			set 
			{
				mMemberID = value;
			}
		}

		public int TranTypeID 
		{
			get 
			{
				return mTranTypeID;
			}
			set 
			{		
				mTranTypeID = value;
			}
		}

		public int MemberSubID 
		{
			get 
			{
				return mMemberSubID;
			}
			set 
			{
				mMemberSubID = value;
			}
		}

		public int PlanID 
		{
			get 
			{
				return mPlanID;
			}
			set 
			{
				mPlanID = value;
			}
		}

		public int DiscountID 
		{
			get 
			{
				return mDiscountID;
			}
			set 
			{
				mDiscountID = value;
			}
		}

		public int MemberPaymentID 
		{
			get 
			{
				return mMemberPaymentID;
			}
			set 
			{
				mMemberPaymentID = value;
			}
		}

		public int MemberTranStatusID 
		{
			get 
			{
				return mMemberTranStatusID;
			}
			set 
			{
				mMemberTranStatusID = value;
			}
		}

		public int AdminMemberID 
		{
			get 
			{
				return mAdminMemberID;
			}
			set 
			{
				mAdminMemberID = value;
			}
		}

		public decimal Amount 
		{
			get 
			{
				return mAmount;
			}
			set 
			{
				mAmount = value;
			}
		}

		public int NumDays 
		{
			get 
			{
				return mNumDays;
			}
			set 
			{
				mNumDays = value;
			}
		}

		public string AccountNumber 
		{
			get 
			{
				return mAccountNumber;
			}
			set 
			{
				mAccountNumber = value;
			}
		}

		public int TerminationReasonID 
		{
			get 
			{
				return mTerminationReasonID;
			}
			set 
			{
				mTerminationReasonID = value;
			}
		}

		public DateTime InsertDate 
		{
			get 
			{
				return mInsertDate;
			}
			set 
			{
				mInsertDate = value;
			}
		}

		public DateTime UpdateDate 
		{
			get 
			{
				return mUpdateDate;
			}
			set 
			{
				mUpdateDate = value;
			}
		}
	}
}
