using System;

namespace Matchnet.Core.Charge
{
	public class Constants
	{
		public const int TRANTYPE_CUSTOMER_SERVICE_CREDIT = 1;
		public const int NULL_NUMBER = -2147483647;
	}
}
