using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Data;

namespace Matchnet.Core.Charge
{
	public class MemberChargeLog
	{
		private int mMemberChargeLogID;
		private int mMemberChargeID = Constants.NULL_NUMBER;
		private string mName;
		private string mValue;

		public void FromDataRow(DataRow row) 
		{
			try 
			{
				mMemberChargeLogID = System.Convert.ToInt32(row["MemberChargeLogID"]);
				mMemberChargeID = System.Convert.ToInt32(row["MemberChargeID"]);
				mName = System.Convert.ToString(row["Name"]);
				mValue = System.Convert.ToString(row["Value"]);
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to populate MemberChargeLog item", ex);
			}
		}

		private bool ValidateInsert() 
		{
			bool isValid = true;

			if (mMemberChargeID == Constants.NULL_NUMBER) 
			{
				isValid = false;
			}

			if (mName == null || mName.Trim().Length == 0) 
			{
				isValid = false;
			}

			if (mValue == null || mValue.Trim().Length == 0) 
			{
				isValid = false;
			}
			return isValid;
		}

		public void Insert(int memberID) 
		{
			if (!ValidateInsert()) 
			{
				throw new Exception("Name, Value, and MemberChargeID must be defined");
			}
			try 
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnMember", memberID));
				client.AddParameter("@MemberChargeID", SqlDbType.Int, ParameterDirection.Input, mMemberChargeID);
				client.AddParameter("@Name", SqlDbType.VarChar, ParameterDirection.Input, mName, 50);
				client.AddParameter("@Value", SqlDbType.VarChar, ParameterDirection.Input, mValue, 255);

				client.ExecuteNonQuery("up_MemberChargeLog_Insert", CommandType.StoredProcedure);
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to Insert MemberChargeLog item", ex);
			}
		}

		public int MemberChargeLogID 
		{
			get 
			{
				return mMemberChargeLogID;
			}
			set 
			{
				mMemberChargeLogID = value;
			}
		}

		public int MemberChargeID 
		{
			get 
			{
				return mMemberChargeID;
			}
			set 
			{
				mMemberChargeID = value;
			}
		}

		public string Name 
		{
			get 
			{
				return mName;
			}
			set 
			{
				mName = value;
			}
		}

		public string Value 
		{
			get 
			{
				return mValue;
			}
			set 
			{
				mValue = value;
			}
		}
	}
}
