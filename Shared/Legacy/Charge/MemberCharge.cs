using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Data;

namespace Matchnet.Core.Charge
{
	public class MemberCharge
	{
		private int mMemberChargeID = Constants.NULL_NUMBER;
		private int mMerchantID;
		private int mMemberID;
		private int mDomainID;
		private int mPrivateLabelID;
		private int mAdminMemberID;
		private int mTranTypeID;
		private double mAmount;
		private int mNumMonths;
		private DateTime mInsertDate;
		private int mCreditMemberChargeID = Constants.NULL_NUMBER;

		public int MemberChargeID 
		{
			get 
			{
				return mMemberChargeID;
			}
			set 
			{
				mMemberChargeID = value;
			}
		}

		public int MerchantID 
		{
			get 
			{
				return mMerchantID;
			}
			set 
			{
				mMerchantID = value;
			}
		}
		
		public int MemberID 
		{
			get 
			{
				return mMemberID;
			}
			set 
			{
				mMemberID = value;
			}
		}

		public int DomainID 
		{
			get 
			{
				return mDomainID;
			}
			set 
			{
				mDomainID = value;
			}
		}

		public int PrivateLabelID 
		{
			get 
			{
				return mPrivateLabelID;
			}
			set 
			{
				mPrivateLabelID = value;
			}
		}

		public int AdminMemberID 
		{
			get 
			{
				return mAdminMemberID;
			}
			set 
			{
				mAdminMemberID = value;
			}
		}

		public int TranTypeID 
		{
			get 
			{
				return mTranTypeID;
			}
			set 
			{
				mTranTypeID = value;
			}
		}

		public double Amount 
		{
			get 
			{
				return mAmount;
			}
			set 
			{
				mAmount = value;
			}
		}

		public int NumMonths 
		{
			get 
			{
				return mNumMonths;
			}
			set 
			{
				mNumMonths = value;
			}
		}

		public DateTime InsertDate 
		{
			get 
			{
				return mInsertDate;
			}
			set 
			{
				mInsertDate = value;
			}
		}

		public int CreditMemberChargeID 
		{
			get 
			{	
				return mCreditMemberChargeID;
			}
			set 
			{
				mCreditMemberChargeID = value;
			}
		}

		public void FromDataRow(DataRow row) 
		{
			try 
			{
				mMemberChargeID = System.Convert.ToInt32(row["MemberChargeID"]);
				mMemberID = System.Convert.ToInt32(row["MemberID"]);
				mDomainID = System.Convert.ToInt32(row["DomainID"]);
				mPrivateLabelID = System.Convert.ToInt32(row["PrivateLabelID"]);
				mAdminMemberID = System.Convert.ToInt32(row["AdminMemberID"]);
				mMerchantID = System.Convert.ToInt32(row["MerchantID"]);
				mTranTypeID = System.Convert.ToInt32(row["TranTypeID"]);
				mAmount = System.Convert.ToDouble(row["Amount"]);
				mNumMonths = System.Convert.ToInt32(row["NumMonths"]);
				mInsertDate = System.Convert.ToDateTime(row["InsertDate"]);
				if (row["CreditMemberChargeID"] != DBNull.Value) 
				{
					mCreditMemberChargeID = System.Convert.ToInt32(row["CreditMemberChargeID"]);
				} 
				else 
				{
					mCreditMemberChargeID = Constants.NULL_NUMBER;
				}
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to populate MemberCharge", ex);
			}
		}

		public void PopulateByID(int memberChargeID) 
		{
			try 
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnMember", memberChargeID));
				client.AddParameter("@MemberChargeID", SqlDbType.Int, ParameterDirection.Input, memberChargeID);
				
				DataTable table = client.GetDataTable("up_MemberCharge_List", CommandType.StoredProcedure);

				if (table != null && table.Rows.Count > 0) 
				{
					FromDataRow(table.Rows[0]);
				}
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to populate MemberCharge by ID", ex);
			}
		}

		public void Save() 
		{
			try 
			{
				/*
				procedure dbo.up_MemberCharge_Save
				(
					@MemberChargeID int = null output,
					@MemberID int,
					@DomainID int,
					@AdminMemberID int,
					@MerchantID int,
					@TranTypeID int,
					@Amount money,
					@NumMonths int,
					@CreditMemberChargeID int = null
				)
				*/
				SQLClient client = new SQLClient(new SQLDescriptor("mnMember", mMemberID));
				if (mMemberChargeID != Constants.NULL_NUMBER) 
				{
					client.AddParameter("@MemberChargeID", SqlDbType.Int, ParameterDirection.InputOutput, mMemberChargeID);
				} 
				else 
				{
					client.AddParameter("@MemberChargeID", SqlDbType.Int, ParameterDirection.InputOutput, DBNull.Value);
				}
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, mMemberID);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, mDomainID);
				client.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, mAdminMemberID);
				client.AddParameter("@MerchantID", SqlDbType.Int, ParameterDirection.Input, mMerchantID);
				client.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, mTranTypeID);
				client.AddParameter("@Amount", SqlDbType.Money, ParameterDirection.Input, mAmount);
				client.AddParameter("@NumMonths", SqlDbType.Int, ParameterDirection.Input, mNumMonths);

				if (mCreditMemberChargeID != Constants.NULL_NUMBER) 
				{
					client.AddParameter("@CreditMemberChargeID", SqlDbType.Int, ParameterDirection.Input, mCreditMemberChargeID);
				}

				SqlCommand command = new SqlCommand();
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "up_MemberCharge_Save";
				client.PopulateCommand(command);
				client.ExecuteNonQuery(command);

				mMemberChargeID = System.Convert.ToInt32(command.Parameters["@MemberChargeID"].Value);
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to save MemberCharge", ex);
			}
		}

		public void Delete() 
		{
			try 
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnMember", mMemberChargeID));
				client.AddParameter("@MemberChargeID", SqlDbType.Int, ParameterDirection.Input, mMemberChargeID);
				client.ExecuteNonQuery("up_MemberCharge_Delete", CommandType.StoredProcedure);
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to delete MemberCharge", ex);
			}
		}

		public ArrayList MemberChargeLog() 
		{
			ArrayList log = new ArrayList();
			try 
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnMember", mMemberID));
				client.AddParameter("@MemberChargeID", SqlDbType.Int, ParameterDirection.Input, mMemberChargeID);
				DataTable table = client.GetDataTable("up_MemberChargeLog_List", CommandType.StoredProcedure);

				if (table != null && table.Rows.Count > 0) 
				{
					foreach (DataRow row in table.Rows) 
					{
						MemberChargeLog chargeLog = new MemberChargeLog();
						chargeLog.FromDataRow(row);
					}
				}
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to populate MemberChargeLog", ex);				
			}
			return log;
		}
	}
}