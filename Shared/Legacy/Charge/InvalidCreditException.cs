using System;

namespace Matchnet.Core.Charge
{
	/// <summary>
	/// Signifies an exception that occurs when a Credit request was not
	/// valid for a provider.
	/// </summary>
	public class InvalidCreditException  : System.Exception
	{
		public InvalidCreditException(string message) 
			: base (message) 
		{

		}

		public InvalidCreditException(string message, Exception rootCause) 
			: base (message, rootCause) 
		{

		}
	}
}
