using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;

using Matchnet.Lib.Util;

namespace Matchnet.Core.Charge.JetPay
{
	public class JetPayProcessor : Processor
	{
		public const string FIELD_TRANSACTIONTYPE = "TransactionType";
		public const string FIELD_TRANSACTIONID = "TransactionID";
		public const string FIELD_TOTALAMOUNT = "TotalAmount";
		public const string FIELD_CARDEXPYEAR = "CardExpYear";
		public const string FIELD_BILLINGCOUNTRY = "BillingCountry";
		public const string FIELD_ORIGIN = "Origin";
		
		public const string TRANSACTIONTYPE_SALE = "SALE";
		public const string TRANSACTIONTYPE_CREDIT = "CREDIT";

		public const string RESPONSE_SUCCESS = "000";

		private string mXmlResponse;

		public override string RawResponse()
		{
			return mXmlResponse;
		}

		//protected override bool DoCredit(int merchantID, int memberID, int domainID, int chargeID)
		protected override ProcessorResponse DoCredit(MemberCharge memberCharge)
		{
			Hashtable properties = MerchantList(memberCharge.MerchantID, memberCharge.CreditMemberChargeID);
			// the credit request is the same as the charge request,
			// except for this field
			if (properties.ContainsKey(FIELD_TRANSACTIONTYPE)) 
			{
				properties.Remove(FIELD_TRANSACTIONTYPE);
			}
			properties.Add(FIELD_TRANSACTIONTYPE, TRANSACTIONTYPE_CREDIT);

			string referer = string.Empty;

			if (properties.ContainsKey(FIELD_REFER)) 
			{
				referer = (string)properties[FIELD_REFER];
			}

			string xmlDoc = GetXMLDoc(ref properties);
			
			//System.Console.WriteLine(xmlDoc);
			
			string url = (string)properties[FIELD_URL];
			JetPayResponse response = ParseResult(SendXML(url, xmlDoc, referer));
			return response;
		}

		private string FormatValue(string key, string val, bool renewFlag) 
		{
			string retVal = val;
			switch (key) 
			{
				case FIELD_TRANSACTIONID:
					retVal = val.PadLeft(18, '0');
					break;
				case FIELD_TOTALAMOUNT:
					retVal = System.Convert.ToString(System.Convert.ToDouble(val) * 100);
					break;
				case FIELD_CARDEXPYEAR:
					retVal = val.Length == 2 ? val : val.Substring(val.Length - 2);
					break;
				case FIELD_BILLINGCOUNTRY:
					retVal = GetISOCountryCode3(val);
					break;
				case FIELD_ORIGIN:
					retVal = renewFlag ? "RECURRING" : val;
					break;
			}
			return System.Web.HttpUtility.UrlEncode(retVal);		
		}

		private string GetXMLDoc(ref Hashtable properties) 
		{
			bool renewFlag = false;

			if (properties.ContainsKey(FIELD_RENEW_FLAG)) 
			{
				renewFlag = System.Convert.ToBoolean(System.Convert.ToSingle(properties[FIELD_RENEW_FLAG]));
			}

			StringBuilder builder = new StringBuilder();
			builder.Append("<JetPay>\r\n");
			
			foreach (string key in properties.Keys) 
			{
				if (!IsExcludedField(key)) 
				{
					builder.Append("\t<" + key + ">" + FormatValue(key, (string)properties[key], renewFlag) + "</" + key + ">\r\n");
				}
			}

			builder.Append("</JetPay>");
			return builder.ToString();
		}

		private string SendXML(string url, string xmlDoc, string referer) 
		{
			try 
			{
				return HttpUtil.PostData(url, xmlDoc, referer);
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to post data to Provider [JetPay - " + url + "]", ex);
			}
		}

		private JetPayResponse ParseResult(string xmlResponse) 
		{
			JetPayResponse response = new JetPayResponse();
			mXmlResponse = xmlResponse;
			response.FromXml(xmlResponse);
			return response;
		}
	}
}
