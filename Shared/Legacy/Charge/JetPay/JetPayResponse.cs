using System;
using System.IO;
using System.Xml;

namespace Matchnet.Core.Charge.JetPay
{
	public class JetPayResponse : ProcessorResponse
	{
		private string mTransactionID;
		private string mApproval;

		// left padded to fill 18 characters
		public string TransactionID 
		{
			get 
			{
				return mTransactionID;
			}
		}

		// not always available
		public string Approval 
		{
			get 
			{
				return mApproval;
			}
		}

		public void FromXml(string xmlResponse) 
		{
			MemoryStream stream = new MemoryStream(System.Text.Encoding.ASCII.GetBytes(xmlResponse), false);
			XmlTextReader reader = new XmlTextReader(stream);

			while (reader.Read()) 
			{
				string nodeName = reader.Name;

				switch (nodeName) 
				{
					case "TransactionID":
						mTransactionID = reader.ReadString();
						break;
					case "ActionCode":
						ResponseCode = reader.ReadString();
						Success = ResponseCode == JetPayProcessor.RESPONSE_SUCCESS;
						break;
					case "Approval":
						mApproval = reader.ReadString();
						break;
					case "ResponseText":
						Message = reader.ReadString();
						break;
				}
			}
		}
	}
}
