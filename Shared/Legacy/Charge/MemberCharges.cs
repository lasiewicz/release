using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Data;

namespace Matchnet.App.Charge
{
	public class MemberCharges
	{
		private ArrayList mMemberCharges;

		public void PopulateByMember(int memberID, int domainID) 
		{
			try 
			{
				mMemberCharges = new ArrayList();

				SQLClient client = new SQLClient(new SQLDescriptor("mnMember", memberID));
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);

				DataTable table = client.GetDataTable("up_MemberCharge_List_ByMember", CommandType.StoredProcedure);

				if (table != null && table.Rows.Count > 0) 
				{
					foreach (DataRow row in table.Rows) 
					{
						MemberCharge charge = new MemberCharge();
						charge.FromDataRow(row);
						mMemberCharges.Add(charge);
					}
				}
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to populate MemberCharges by Member", ex);
			}
		}
	}
}
