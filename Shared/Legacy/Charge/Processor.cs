using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Data;

namespace Matchnet.Core.Charge
{
	public abstract class Processor
	{
		// internal fields
		public const string FIELD_URL = "Url";
		public const string FIELD_PORT = "Port";
		public const string FIELD_REFER = "Referer";
		public const string FIELD_PURCHASE_TYPE = "PurchaseTypeID";
		public const string FIELD_RENEW_FLAG = "RenewFlag";

		public static Hashtable ExcludedFields = GetExcludedFields();
		public static Hashtable GetExcludedFields() 
		{
			Hashtable excluded = new Hashtable();
			excluded.Add(FIELD_URL,"");
			excluded.Add(FIELD_PORT,"");
			excluded.Add(FIELD_REFER,"");
			excluded.Add(FIELD_PURCHASE_TYPE,"");
			excluded.Add(FIELD_RENEW_FLAG, "");
			return excluded;
		}

		public bool IsExcludedField(string name) 
		{
			return ExcludedFields.ContainsKey(name);			
		}

		public bool Credit(MemberCharge memberCharge) 
		{
			ValidateStatus validate = ValidateCredit();
			bool success = false;

			if (validate.Valid)
			{
				try 
				{
					// we are crediting an existing MemberCharge, so we
					// need to set the MemberChargeID and CreditMemberChargeID's
					// accordingly:
					memberCharge.CreditMemberChargeID = memberCharge.MemberChargeID;
					memberCharge.MemberChargeID = Constants.NULL_NUMBER;

					// make sure neg numbers for months and amount
					if (memberCharge.Amount > 0) 
					{
						memberCharge.Amount = -memberCharge.Amount;
					}

					if (memberCharge.NumMonths > 0) 
					{
						memberCharge.NumMonths = -memberCharge.NumMonths;
					}
					
					// if this is successful, then the provider
					// has credited the member
					ProcessorResponse response = DoCredit(memberCharge);

					success = response.Success;
					if (success) 
					{
						memberCharge.Save();
					}
				} 
				catch (Exception ex)
				{
					if (success) 
					{
						throw new CreditPersistException("Unable to save successful credit", ex);
					}
					return false;
				}
				return success;
			} 
			else 
			{
				throw new InvalidCreditException(validate.Message);
			}
		}

		protected Hashtable MerchantList(int merchantID, int chargeID) 
		{
			try 
			{
				Hashtable properties = new Hashtable();

				SQLClient client = new SQLClient(new SQLDescriptor("mnCharge", chargeID));
				client.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, chargeID);
				client.AddParameter("@MerchantID", SqlDbType.Int, ParameterDirection.Input, merchantID);
				
				DataTable table = client.GetDataTable("up_Charge_Merchant_List", CommandType.StoredProcedure);

				if (table != null && table.Rows.Count > 0) 
				{
					foreach (DataRow row in table.Rows) 
					{
						string name = System.Convert.ToString(row["Name"]);
						string val = System.Convert.ToString(row["Value"]);

						if (properties.ContainsKey(name)) 
						{
							// go with the last found one....this probably never happens
							properties.Remove(name);
						}
						properties.Add(name, val);
					}
				}
				return properties;
			} 
			catch (Exception ex) 
			{
				throw ex;
			}
		}

		protected string GetISOCountryCode3(string countryCode) 
		{
			try 
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnRegion", 0));
				client.AddParameter("@ISOCountryCode2", SqlDbType.Char, ParameterDirection.Input, countryCode, 10);

				DataTable table = client.GetDataTable("up_Country_List_ISOCountryCode3", CommandType.StoredProcedure);

				if (table != null && table.Rows.Count > 0) 
				{
					return System.Convert.ToString(table.Rows[0]["ISOCountryCode3"]);
				}
				else 
				{
					throw new Exception("Unable to get ISOCountryCode2 for " + countryCode);
				}
			} 
			catch (Exception ex) 
			{
				throw new Exception("Error Getting ISOCountryCode2 for " + countryCode, ex);
			}
		}

		// override for processor specific validation
		protected virtual ValidateStatus ValidateCredit() 
		{
			ValidateStatus status = new ValidateStatus();
			status.Valid = true;
			status.Message = string.Empty;
			return status;
		}

		protected abstract ProcessorResponse DoCredit(MemberCharge memberCharge);
		
		//protected abstract bool DoPurchase(MemberCharge memberCharge);
		public abstract string RawResponse();

		public struct ValidateStatus 
		{
			public bool Valid;
			public string Message;
		}
	}
}
