using System;

namespace Matchnet.Core.Charge
{
	/// <summary>
	/// Signifies that an exception occured saving a MemberCharge
	/// for a credit, but the Provider returned success for 
	/// processing the credit.
	/// </summary>
	public class CreditPersistException : Exception
	{
		public CreditPersistException(string message) 
			: base (message) 
		{

		}

		public CreditPersistException(string message, Exception rootCause) 
			: base (message, rootCause) 
		{

		}
	}
}
