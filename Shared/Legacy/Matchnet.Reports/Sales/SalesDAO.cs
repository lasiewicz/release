using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib;
using Matchnet.DataTemp;

namespace Matchnet.Reports.Sales
{
	public class SalesDAO
	{
		private const int COMMAND_TIMEOUT = 120;

		public static DataSet ReportSales(int domainID, int privateLabelID, DateTime startDate, DateTime endDate) 
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnAnalysis");
			ExtendedSQLClient client = new ExtendedSQLClient(descriptor);

			if (domainID != Matchnet.Constants.NULL_INT)
			{
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			}

			if (privateLabelID != Matchnet.Constants.NULL_INT)
			{
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			}
			
			if (startDate != DateTime.MinValue)
			{
				client.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
			}
			
			if (endDate != DateTime.MinValue)
			{
				client.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
			}

			return client.GetDataSet("up_Report_Sales", CommandType.StoredProcedure, COMMAND_TIMEOUT);
		}
	}
}
