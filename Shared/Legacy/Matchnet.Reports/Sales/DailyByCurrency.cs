using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

using Matchnet.Lib;
using Matchnet.Reports.Charts;


namespace Matchnet.Reports.Sales
{
	public class DailyByCurrency
	{
		public static Chart GetReport(ChartType type) 
		{
			return GetReport(type, DateTime.Now);
		}

		public static Chart GetReport(ChartType type, DateTime now) 
		{
			DateTime startDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
			DateTime endDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
			
			DataSet dataSet = SalesDAO.ReportSales(Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT, startDate, endDate);
			switch (type) 
			{
				case ChartType.BarGraph:
					return GetBarGraph(dataSet);
				case ChartType.PieChart:
					return GetPieChart(dataSet);
				default:
					return GetBarGraph(dataSet);
			}
		}

		private static BarGraph GetBarGraph(DataSet dataSet) 
		{
			BarGraph bar = new BarGraph(Color.FromArgb(255,253,244));
						
			bar.VerticalLabel = "";
			bar.VerticalTickCount = 5;
			bar.ShowLegend = true;
			bar.ShowData = true;
			bar.Height = 400;
			bar.Width = 600;

			DataTable table = dataSet.Tables[0];

			string[] xValues = new string[table.Rows.Count];
			string[] yValues = new string[table.Rows.Count];

			int count = 0;
			foreach (DataRow row in table.Rows) 
			{
				string currency = System.Convert.ToString(row["Description"]);
				string total = System.Convert.ToString(row["CombinedTotal"]);
				xValues[count] = currency;
				yValues[count] = total;
				count++;
			}

			bar.CollectDataPoints(xValues, yValues);
			return bar;
		}

		private static PieChart GetPieChart(DataSet dataSet) 
		{
			PieChart pieChart = new PieChart(Color.FromArgb(255,253,244));
			pieChart.Perimeter = 125;
			DataTable table = dataSet.Tables[0];
			

			string[] xValues = new string[table.Rows.Count];
			string[] yValues = new string[table.Rows.Count];

			int count = 0;
			foreach (DataRow row in table.Rows) 
			{
				string currency = System.Convert.ToString(row["Description"]);
				string total = System.Convert.ToString(row["CombinedTotal"]);
				xValues[count] = currency;
				yValues[count] = total;
				count++;
			}
			pieChart.CollectDataPoints(xValues, yValues);
			return pieChart;
		}
	}
}
