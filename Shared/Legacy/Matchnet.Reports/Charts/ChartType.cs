using System;

namespace Matchnet.Reports.Charts
{
	public enum ChartType 
	{
		BarGraph = 1,
		PieChart = 2
	}	
}
