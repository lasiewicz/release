using System;
using System.Collections;
using System.Configuration;
using System.Data;

namespace Matchnet.Content
{
    public sealed class Translator
    {
        private const int CacheTimeout = 60;
        const string KEY_DEVELOPMENTMODE = "DevelopmentMode";
        private static Matchnet.CachingTemp.Cache _Cache;
        private Hashtable _Tokens = new Hashtable();
        private bool _DevelopmentMode = false;
        private bool _ResourceHints = false;

        
        public Translator( Matchnet.CachingTemp.Cache cache )
        {
            _Cache = cache;
            _DevelopmentMode = Convert.ToBoolean(ConfigurationSettings.AppSettings.Get(KEY_DEVELOPMENTMODE));
        }


        public string Value(int resourceID, int languageID, int privateLabelID)
        {
            try 
            {
                // First off, if we are in development mode and ResourceHints are on, don't even get from the DB
                if ( _DevelopmentMode && _ResourceHints )
                {
                    // Never cache hinted resources...
                    return String.Format("[{0}]{1}",
                        resourceID,
                        LoadFromDB(resourceID, languageID, privateLabelID));

                }

                string key = "RSRC:" + resourceID + ":" + languageID + ":" + privateLabelID;
                string content = (string)_Cache.Get(key);

                if (content == null) 
                {
                    content = LoadFromDB(resourceID, languageID, privateLabelID);
                    if (content != null && content != string.Empty) 
                    {
                        _Cache.Insert(key, content, null, DateTime.Now.AddMinutes(CacheTimeout), 
                                        TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
                    }
                }
                return ExpandTokens(content);

            } 
            catch (Exception ex)
            {
                Matchnet.Lib.Exceptions.MatchnetException exception = new Matchnet.Lib.Exceptions.MatchnetException("Unable to load resource", "Translator.Value", ex);
                throw exception;
            }
        }

        private string LoadFromDB(int resourceID, int languageID, int privateLabelID) 
        {

            string retval = string.Empty;
            Matchnet.DataTemp.SQLDescriptor descriptor = new Matchnet.DataTemp.SQLDescriptor("mnShared", 0);
            Matchnet.DataTemp.SQLClient client = new Matchnet.DataTemp.SQLClient(descriptor);
			//TODO: Remove SQL 
            client.AddParameter("@ResourceID", SqlDbType.Int, ParameterDirection.Input, resourceID);
            client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
            client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);

            DataTable table = client.GetDataTable("up_Resource_List", CommandType.StoredProcedure);

            if (table.Rows.Count > 0) 
            {
                retval = table.Rows[0]["Content"].ToString();	
            } 
            else 
            {
                if (_DevelopmentMode)
                {
                    retval = "Missing Resource rsrc[" + resourceID.ToString() + "] ";
                    retval += "lang[" + languageID.ToString() + "] ";
                    retval += "plid[" + privateLabelID.ToString() + "] ";
                }
                else
                {
                    retval = string.Empty;
                }
            }
            return retval;
        }

        private string ExpandTokens(string text)
        {
            IDictionaryEnumerator item = _Tokens.GetEnumerator();
            while (item.MoveNext())
            {
                if ( item.Key != null && item.Value != null )
                    text = text.Replace(item.Key.ToString(), item.Value.ToString());
            }
			
            return text;
        }
    }
}
