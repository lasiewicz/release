using System;

using System.Data.SqlClient;
using System.Data;

using Matchnet.Lib;
using Matchnet.Content;
using Matchnet.DataTemp;
using Matchnet.Lib.Util;
using Matchnet.CachingTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Labels;

using System.Web;

namespace Matchnet.LandingPage
{
	/// <summary>
	/// Summary description for LandingPage.
	/// </summary>

		public class LandingPage
		{
			private const string DB_SHAREDADMIN = "mnSharedAdmin";
			private const int PROPERTY_OWNER = 50;
			private const int PROPERTY_NAME = 50;

			#region Private Properties
			private int _LandingPageID = Matchnet.Constants.NULL_INT;
			private int _BasePrivateLabelID = Matchnet.Constants.NULL_INT;
			private int _PrivateLabelID = Matchnet.Constants.NULL_INT;
			private string _DefaultPage = "";
			private string _Description = "";
			private int _AdminMemberID = Matchnet.Constants.NULL_INT;
			private bool _PublishFlag = false;
			private bool _ActiveFlag = false;
			private bool _ApprovalFlag = false;
			private int _ApproverMemberID = Matchnet.Constants.NULL_INT;
			private DateTime _ApprovedDate;
			private string _PhysicalRoot = "";
			private string _HttpRoot = "";
			private DateTime _UpdateDate;
			#endregion

			#region Public Properties

			public int LandingPageID 
			{
				get 
				{
					return _LandingPageID;
				}
				set
				{
					_LandingPageID = value;
				}
			}

			public int BasePrivateLabelID 
			{
				get 
				{
					return _BasePrivateLabelID;
				}
				set
				{
					_BasePrivateLabelID = value;
				}
			}

			public int PrivateLabelID 
			{
				get 
				{
					return _PrivateLabelID;
				}
				set
				{
					_PrivateLabelID = value;
				}
			}
			public string DefaultPage 
			{
				get 
				{
					return _DefaultPage;
				}
				set
				{
					_DefaultPage = value;
				}
			}
			public string Description 
			{
				get 
				{
					return _Description;
				}
				set
				{
					_Description = value;
				}
			}

			public int AdminMemberID
			{
				get 
				{
					return _AdminMemberID;
				}
				set
				{
					_AdminMemberID = value;
				}
			}

			public bool PublishFlag
			{
				get 
				{
					return _PublishFlag;
				}
				set
				{
					_PublishFlag = value;
				}
			}

			public bool ActiveFlag
			{
				get 
				{
					return _ActiveFlag;
				}
				set
				{
					_ActiveFlag = value;
				}
			}

			public bool ApprovalFlag
			{
				get 
				{
					return _ApprovalFlag;
				}
				set
				{
					_ApprovalFlag = value;
				}
			}

			
			public int ApproverMemberID
			{
				get 
				{
					return _ApproverMemberID;
				}
				set
				{
					_ApproverMemberID = value;
				}
			}

			public DateTime ApprovedDate
			{
				get 
				{
					return _ApprovedDate;
				}
				set
				{
					_ApprovedDate = value;
				}
			}



			public string PhysicalPath
			{
				get
				{
					if (PrivateLabelID == Matchnet.Constants.NULL_INT)
					{
						return _PhysicalRoot + 
							Util.CString(BasePrivateLabelID) + "\\" + 
							Util.CString(LandingPageID);
					}
					else
					{
						return _PhysicalRoot + 
							Util.CString(PrivateLabelID) + "\\" + 
							Util.CString(LandingPageID);
					}
				}
			}

			public string HttpPath
			{
				get
				{
					if (PrivateLabelID == Matchnet.Constants.NULL_INT)
					{
						return _HttpRoot + Util.CString(BasePrivateLabelID) + "/" + 
							Util.CString(LandingPageID) + "/" + 
							DefaultPage;
					}
					else
					{
						return _HttpRoot + Util.CString(PrivateLabelID) + "/" + 
							Util.CString(LandingPageID) + "/" +
							DefaultPage;
					}
				}
			}

			public DateTime UpdateDate
			{
				get
				{
					return _UpdateDate;
				}
				set 
				{
					_UpdateDate = value;
				}
			}
			#endregion

			#region Public Methods
			public LandingPage()
			{
				SetRoot();
			}

			public bool Populate(int landingPageID)
			{
				return Populate(landingPageID, false);
			}

			public bool Populate(int landingPageID, bool mustActive)
			{
				if (landingPageID > 0)
				{
					SQLDescriptor descriptor = new SQLDescriptor(DB_SHAREDADMIN);
					SQLClient client = new SQLClient(descriptor);

					client.AddParameter("@LandingPageID", SqlDbType.Int, ParameterDirection.Input, landingPageID);
					if (mustActive == true)
					{
						client.AddParameter("@ActiveFlag", SqlDbType.Bit, ParameterDirection.Input, true);
					}
					DataTable dt = client.GetDataTable("up_LandingPage_List", CommandType.StoredProcedure);
					if (dt.Rows.Count > 0)
					{
						SetProperty(dt.Rows[0]);
						return true;
					}
				}

				return false;
			}

			public bool Save()
			{
				SQLDescriptor descriptor = new SQLDescriptor(DB_SHAREDADMIN);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter("@LandingPageID", SqlDbType.Int, ParameterDirection.Input, LandingPageID);
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, PrivateLabelID);
				client.AddParameter("@DefaultPage", SqlDbType.NVarChar, ParameterDirection.Input, DefaultPage, DefaultPage.Length);
				client.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, Description, Description.Length);
				client.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, AdminMemberID);
				
				DataTable dt = client.GetDataTable("up_LandingPage_Save", CommandType.StoredProcedure);
				if (dt.Rows.Count > 0)
				{
					SetProperty(dt.Rows[0]);
					return true;
				}
				return false;
			}
			#endregion

			#region Private Methods
			private void SetProperty(DataRow r)
			{
				LandingPageID = Util.CInt(r["LandingPageID"]);
				PrivateLabelID = Util.CInt(r["PrivateLabelID"]);
				DefaultPage = Util.CString(r["DefaultPage"]);
				Description = Util.CString(r["Description"]);
				AdminMemberID = Util.CInt(r["AdminMemberID"]);
				PublishFlag = Util.CBool(r["PublishFlag"], false);
				ActiveFlag = Util.CBool(r["ActiveFlag"], false);
				ApprovalFlag = Util.CBool(r["ApprovalFlag"], false);
				ApproverMemberID = Util.CInt(r["ApproverMemberID"]);
				ApprovedDate = Util.CDateTime(r["ApprovedDate"]);
				UpdateDate = Util.CDateTime(r["UpdateDate"]);

				//Get the base privatelabel id
				if (PrivateLabelID > 0)
				{
					Matchnet.Labels.PrivateLabel pl = new Matchnet.Labels.PrivateLabel();
					pl.Load(PrivateLabelID);
					if (pl.BasePrivateLabelID > 0)
					{
						BasePrivateLabelID = pl.BasePrivateLabelID;
					}
					else
					{
						//this is a base privatelabel
						BasePrivateLabelID = pl.PrivateLabelID;
						PrivateLabelID = Matchnet.Constants.NULL_INT;
					}
				}
			}


			private void SetRoot()
			{

				string cacheKey = "LandingPagePhysicalRoot";

				object val = Cache.GetInstance().Get(cacheKey);

				if (val == null) 
				{

					SQLDescriptor conf = new SQLDescriptor("mnMaster");
					SQLClient client = new SQLClient(conf);
					client.AddParameter("@Owner", SqlDbType.VarChar, ParameterDirection.Input, "LandingPage", PROPERTY_OWNER);
					client.AddParameter("@Name", SqlDbType.VarChar, ParameterDirection.Input, "PhysicalRoot", PROPERTY_NAME);
					DataTable table = client.GetDataTable("up_Property_List", CommandType.StoredProcedure);
					if(table.Rows.Count > 0) 
					{
						_PhysicalRoot = table.Rows[0]["Value"].ToString();
						// Insert to cache
						Cache cache = Cache.GetInstance();

						cache.Insert(
							cacheKey,
							_PhysicalRoot,
							null,
							cache.NoAbsoluteExpiration,
							TimeSpan.FromSeconds(60));
					} 
					else 
					{
						throw new MatchnetException("Failed to load Landing Page Physical Root");
					}

				
				}
				else 
				{
					_PhysicalRoot = ((String) val);
				}


				// Get the http root
				cacheKey = "LandingPageHttpRoot";

				val = Cache.GetInstance().Get(cacheKey);

				if (val == null) 
				{

					SQLDescriptor conf = new SQLDescriptor("mnMaster");
					SQLClient client = new SQLClient(conf);
					client.AddParameter("@Owner", SqlDbType.VarChar, ParameterDirection.Input, "LandingPage", PROPERTY_OWNER);
					client.AddParameter("@Name", SqlDbType.VarChar, ParameterDirection.Input, "HttpRoot", PROPERTY_NAME);
					DataTable table = client.GetDataTable("up_Property_List", CommandType.StoredProcedure);
					if(table.Rows.Count > 0) 
					{
						_HttpRoot = table.Rows[0]["Value"].ToString();
						Cache cache = Cache.GetInstance();

						cache.Insert(
							cacheKey,
							_HttpRoot,
							null,
							cache.NoAbsoluteExpiration,
							TimeSpan.FromSeconds(60));
					} 
					else 
					{
						throw new MatchnetException("Failed to load Landing Page Http Root");
					}
				}
				else 
				{
					_HttpRoot = ((String) val);
				}
			}

			#endregion

			#region Static Methods
			public static DataTable GetLandingPageList()
			{
				//DataTable dt = new DataTable();

				SQLDescriptor descriptor = new SQLDescriptor(DB_SHAREDADMIN);
				SQLClient client = new SQLClient(descriptor);

				DataTable dt = client.GetDataTable("up_LandingPage_ListAll", CommandType.StoredProcedure);
			
				return dt;
			}

			public static bool Delete(int landingPageID)
			{
				SQLDescriptor descriptor = new SQLDescriptor(DB_SHAREDADMIN);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter("@LandingPageID", SqlDbType.Int, ParameterDirection.Input, landingPageID);
				SqlCommand command = new SqlCommand();
			
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "up_LandingPage_Delete";
				client.PopulateCommand(command);
				client.ExecuteNonQuery(command);
				return true;
			}

			public static bool Publish(int landingPageID, bool publishFlag)
			{
				SQLDescriptor descriptor = new SQLDescriptor(DB_SHAREDADMIN);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter("@LandingPageID", SqlDbType.Int, ParameterDirection.Input, landingPageID);
				client.AddParameter("@PublishFlag", SqlDbType.Bit, ParameterDirection.Input, publishFlag);
				SqlCommand command = new SqlCommand();
			
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "up_LandingPage_Publish";
				client.PopulateCommand(command);
				client.ExecuteNonQuery(command);
				return true;
			}

			public static bool Approve(int landingPageID, int approverMemberID, bool approvalFlag)
			{
				SQLDescriptor descriptor = new SQLDescriptor(DB_SHAREDADMIN);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter("@LandingPageID", SqlDbType.Int, ParameterDirection.Input, landingPageID);
				client.AddParameter("@ApproverMemberID", SqlDbType.Int, ParameterDirection.Input, approverMemberID);
				client.AddParameter("@ApprovalFlag", SqlDbType.Bit, ParameterDirection.Input, approvalFlag);

				SqlCommand command = new SqlCommand();
			
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "up_LandingPage_Approve";
				client.PopulateCommand(command);
				client.ExecuteNonQuery(command);
				return true;
			}

			public static bool DisApproveDeactivate(int landingPageID)
			{
				SQLDescriptor descriptor = new SQLDescriptor(DB_SHAREDADMIN);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter("@LandingPageID", SqlDbType.Int, ParameterDirection.Input, landingPageID);
			
				SqlCommand command = new SqlCommand();
			
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "up_LandingPage_DisApproveDeactivate";
				client.PopulateCommand(command);
				client.ExecuteNonQuery(command);
				return true;
			}

			#endregion
		}
	
}
