using System;
using System.Text;

namespace Matchnet.Text.Conversion 
{
	/// <summary>
	/// Summary description for CP1255Encoding.
	/// </summary>
	/// 
	
	public class CP1255Encoding 
	{
		#region Codepage 1255 decoding

		/// <summary>
		/// Converts / Maps a Codepage 1255 string to a Hebrew unicode string. 
		/// The string is assumed to be composed of codepage 1255 bytes, lower portion is CP1255 and hight portion is 00 for each character in the string.
		/// </summary>
		/// <param name="CP1255String">Codepage 1255 (Hebrew) string to convert</param>
		/// <returns>Unicode string mapped to Hebrew Unicode</returns>
		public static string ToUnicode(string CP1255String)
		{
			StringBuilder result = new StringBuilder(CP1255String.Length);
			for (int i = 0; i < CP1255String.Length; i++)
			{
				result.Append(ToUnicode(CP1255String[i]));
			}

			return result.ToString();
		}

		/// <summary>
		/// Converts / Maps a Codepage 1255 byte array to a Hebrew unicode string. 
		/// The byte array is assumed to be composed of codepage 1255 bytes.
		/// </summary>
		/// <param name="CP1255ByteArray">Codepage 1255 (Hebrew) byte array to convert</param>
		/// <returns>Unicode string mapped to Hebrew Unicode</returns>
		public static string ToUnicode(byte [] CP1255ByteArray)
		{
			StringBuilder result = new StringBuilder(CP1255ByteArray.Length);
			for (int i = 0; i < CP1255ByteArray.Length; i++)
			{
				result.Append(ToUnicode(CP1255ByteArray[i]));
			}

			return result.ToString();
		}

		/// <summary>
		/// Converts / Maps a Codepage 1255 char array to a Hebrew unicode string. 
		/// The byte array is assumed to be composed of codepage 1255 bytes in the lower portion of each char, and 00 in the upper portion of each char.
		/// </summary>
		/// <param name="CP1255CharArray">Codepage 1255 (Hebrew) char array to convert</param>
		/// <returns>Unicode string mapped to Hebrew Unicode</returns>
		public static string ToUnicode(char [] CP1255CharArray)
		{
			StringBuilder result = new StringBuilder(CP1255CharArray.Length);
			for (int i = 0; i < CP1255CharArray.Length; i++)
			{
				result.Append(ToUnicode(CP1255CharArray[i]));
			}

			return result.ToString();
		}

		/// <summary>
		/// Converts / Maps a Codepage 1255 byte to a Hebrew unicode char.
		/// </summary>
		/// <param name="CP1255Byte">ASCII Byte from codepage 1255 (Hebrew) to convert</param>
		/// <returns>Unicode char mapped to Hebrew Unicode</returns>
		public static char ToUnicode(byte CP1255Byte)
		{
			return ToUnicode((char)CP1255Byte);
		}


		/// <summary>
		/// Converts / Maps a Codepage 1255 char to a Hebrew unicode char. The lower portion of the char should be ASCII 1255 byte. The higher portion should be 00.
		/// </summary>
		/// <param name="CP1255Char">Unicode char from codepage 1255 (Hebrew) to convert.</param>
		/// <returns>Unicode char mapped to Hebrew Unicode</returns>
		public static char ToUnicode(char CP1255Char)
		{
			char UnicodeChar;

			switch (CP1255Char) 
			{
					// 				case '\x0000': UnicodeChar = CP1255Char; break; /* NULL  */
					// 				case '\x0001': UnicodeChar = CP1255Char; break; /* START OF HEADING  */
					// 				case '\x0002': UnicodeChar = CP1255Char; break; /* START OF TEXT  */
					// 				case '\x0003': UnicodeChar = CP1255Char; break; /* END OF TEXT  */
					// 				case '\x0004': UnicodeChar = CP1255Char; break; /* END OF TRANSMISSION  */
					// 				case '\x0005': UnicodeChar = CP1255Char; break; /* ENQUIRY  */
					// 				case '\x0006': UnicodeChar = CP1255Char; break; /* ACKNOWLEDGE  */
					// 				case '\x0007': UnicodeChar = CP1255Char; break; /* BELL  */
					// 				case '\x0008': UnicodeChar = CP1255Char; break; /* BACKSPACE  */
					// 				case '\x0009': UnicodeChar = CP1255Char; break; /* HORIZONTAL TABULATION  */
					// 				case '\x000A': UnicodeChar = CP1255Char; break; /* LINE FEED  */
					// 				case '\x000B': UnicodeChar = CP1255Char; break; /* VERTICAL TABULATION  */
					// 				case '\x000C': UnicodeChar = CP1255Char; break; /* FORM FEED  */
					// 				case '\x000D': UnicodeChar = CP1255Char; break; /* CARRIAGE RETURN  */
					// 				case '\x000E': UnicodeChar = CP1255Char; break; /* SHIFT OUT  */
					// 				case '\x000F': UnicodeChar = CP1255Char; break; /* SHIFT IN  */
					// 				case '\x0010': UnicodeChar = CP1255Char; break; /* DATA LINK ESCAPE  */
					// 				case '\x0011': UnicodeChar = CP1255Char; break; /* DEVICE CONTROL ONE  */
					// 				case '\x0012': UnicodeChar = CP1255Char; break; /* DEVICE CONTROL TWO  */
					// 				case '\x0013': UnicodeChar = CP1255Char; break; /* DEVICE CONTROL THREE  */
					// 				case '\x0014': UnicodeChar = CP1255Char; break; /* DEVICE CONTROL FOUR  */
					// 				case '\x0015': UnicodeChar = CP1255Char; break; /* NEGATIVE ACKNOWLEDGE  */
					// 				case '\x0016': UnicodeChar = CP1255Char; break; /* SYNCHRONOUS IDLE  */
					// 				case '\x0017': UnicodeChar = CP1255Char; break; /* END OF TRANSMISSION BLOCK  */
					// 				case '\x0018': UnicodeChar = CP1255Char; break; /* CANCEL  */
					// 				case '\x0019': UnicodeChar = CP1255Char; break; /* END OF MEDIUM  */
					// 				case '\x001A': UnicodeChar = CP1255Char; break; /* SUBSTITUTE  */
					// 				case '\x001B': UnicodeChar = CP1255Char; break; /* ESCAPE  */
					// 				case '\x001C': UnicodeChar = CP1255Char; break; /* FILE SEPARATOR  */
					// 				case '\x001D': UnicodeChar = CP1255Char; break; /* GROUP SEPARATOR  */
					// 				case '\x001E': UnicodeChar = CP1255Char; break; /* RECORD SEPARATOR  */
					// 				case '\x001F': UnicodeChar = CP1255Char; break; /* UNIT SEPARATOR  */
					// 				case '\x0020': UnicodeChar = CP1255Char; break; /* SPACE  */
					// 				case '\x0021': UnicodeChar = CP1255Char; break; /* EXCLAMATION MARK  */
					// 				case '\x0022': UnicodeChar = CP1255Char; break; /* QUOTATION MARK  */
					// 				case '\x0023': UnicodeChar = CP1255Char; break; /* NUMBER SIGN  */
					// 				case '\x0024': UnicodeChar = CP1255Char; break; /* DOLLAR SIGN  */
					// 				case '\x0025': UnicodeChar = CP1255Char; break; /* PERCENT SIGN  */
					// 				case '\x0026': UnicodeChar = CP1255Char; break; /* AMPERSAND  */
					// 				case '\x0027': UnicodeChar = CP1255Char; break; /* APOSTROPHE  */
					// 				case '\x0028': UnicodeChar = CP1255Char; break; /* LEFT PARENTHESIS  */
					// 				case '\x0029': UnicodeChar = CP1255Char; break; /* RIGHT PARENTHESIS  */
					// 				case '\x002A': UnicodeChar = CP1255Char; break; /* ASTERISK  */
					// 				case '\x002B': UnicodeChar = CP1255Char; break; /* PLUS SIGN  */
					// 				case '\x002C': UnicodeChar = CP1255Char; break; /* COMMA  */
					// 				case '\x002D': UnicodeChar = CP1255Char; break; /* HYPHEN-MINUS  */
					// 				case '\x002E': UnicodeChar = CP1255Char; break; /* FULL STOP  */
					// 				case '\x002F': UnicodeChar = CP1255Char; break; /* SOLIDUS  */
					// 				case '\x0030': UnicodeChar = CP1255Char; break; /* DIGIT ZERO  */
					// 				case '\x0031': UnicodeChar = CP1255Char; break; /* DIGIT ONE  */
					// 				case '\x0032': UnicodeChar = CP1255Char; break; /* DIGIT TWO  */
					// 				case '\x0033': UnicodeChar = CP1255Char; break; /* DIGIT THREE  */
					// 				case '\x0034': UnicodeChar = CP1255Char; break; /* DIGIT FOUR  */
					// 				case '\x0035': UnicodeChar = CP1255Char; break; /* DIGIT FIVE  */
					// 				case '\x0036': UnicodeChar = CP1255Char; break; /* DIGIT SIX  */
					// 				case '\x0037': UnicodeChar = CP1255Char; break; /* DIGIT SEVEN  */
					// 				case '\x0038': UnicodeChar = CP1255Char; break; /* DIGIT EIGHT  */
					// 				case '\x0039': UnicodeChar = CP1255Char; break; /* DIGIT NINE  */
					// 				case '\x003A': UnicodeChar = CP1255Char; break; /* COLON  */
					// 				case '\x003B': UnicodeChar = CP1255Char; break; /* SEMICOLON  */
					// 				case '\x003C': UnicodeChar = CP1255Char; break; /* LESS-THAN SIGN  */
					// 				case '\x003D': UnicodeChar = CP1255Char; break; /* EQUALS SIGN  */
					// 				case '\x003E': UnicodeChar = CP1255Char; break; /* GREATER-THAN SIGN  */
					// 				case '\x003F': UnicodeChar = CP1255Char; break; /* QUESTION MARK  */
					// 				case '\x0040': UnicodeChar = CP1255Char; break; /* COMMERCIAL AT  */
					// 				case '\x0041': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER A  */
					// 				case '\x0042': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER B  */
					// 				case '\x0043': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER C  */
					// 				case '\x0044': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER D  */
					// 				case '\x0045': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER E  */
					// 				case '\x0046': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER F  */
					// 				case '\x0047': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER G  */
					// 				case '\x0048': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER H  */
					// 				case '\x0049': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER I  */
					// 				case '\x004A': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER J  */
					// 				case '\x004B': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER K  */
					// 				case '\x004C': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER L  */
					// 				case '\x004D': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER M  */
					// 				case '\x004E': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER N  */
					// 				case '\x004F': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER O  */
					// 				case '\x0050': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER P  */
					// 				case '\x0051': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER Q  */
					// 				case '\x0052': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER R  */
					// 				case '\x0053': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER S  */
					// 				case '\x0054': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER T  */
					// 				case '\x0055': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER U  */
					// 				case '\x0056': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER V  */
					// 				case '\x0057': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER W  */
					// 				case '\x0058': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER X  */
					// 				case '\x0059': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER Y  */
					// 				case '\x005A': UnicodeChar = CP1255Char; break; /* LATIN CAPITAL LETTER Z  */
					// 				case '\x005B': UnicodeChar = CP1255Char; break; /* LEFT SQUARE BRACKET  */
					// 				case '\x005C': UnicodeChar = CP1255Char; break; /* REVERSE SOLIDUS  */
					// 				case '\x005D': UnicodeChar = CP1255Char; break; /* RIGHT SQUARE BRACKET  */
					// 				case '\x005E': UnicodeChar = CP1255Char; break; /* CIRCUMFLEX ACCENT  */
					// 				case '\x005F': UnicodeChar = CP1255Char; break; /* LOW LINE  */
					// 				case '\x0060': UnicodeChar = CP1255Char; break; /* GRAVE ACCENT  */
					// 				case '\x0061': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER A  */
					// 				case '\x0062': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER B  */
					// 				case '\x0063': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER C  */
					// 				case '\x0064': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER D  */
					// 				case '\x0065': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER E  */
					// 				case '\x0066': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER F  */
					// 				case '\x0067': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER G  */
					// 				case '\x0068': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER H  */
					// 				case '\x0069': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER I  */
					// 				case '\x006A': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER J  */
					// 				case '\x006B': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER K  */
					// 				case '\x006C': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER L  */
					// 				case '\x006D': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER M  */
					// 				case '\x006E': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER N  */
					// 				case '\x006F': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER O  */
					// 				case '\x0070': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER P  */
					// 				case '\x0071': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER Q  */
					// 				case '\x0072': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER R  */
					// 				case '\x0073': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER S  */
					// 				case '\x0074': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER T  */
					// 				case '\x0075': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER U  */
					// 				case '\x0076': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER V  */
					// 				case '\x0077': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER W  */
					// 				case '\x0078': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER X  */
					// 				case '\x0079': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER Y  */
					// 				case '\x007A': UnicodeChar = CP1255Char; break; /* LATIN SMALL LETTER Z  */
					// 				case '\x007B': UnicodeChar = CP1255Char; break; /* LEFT CURLY BRACKET  */
					// 				case '\x007C': UnicodeChar = CP1255Char; break; /* VERTICAL LINE  */
					// 				case '\x007D': UnicodeChar = CP1255Char; break; /* RIGHT CURLY BRACKET  */
					// 				case '\x007E': UnicodeChar = CP1255Char; break; /* TILDE  */
					// 				case '\x007F': UnicodeChar = CP1255Char; break; /* DELETE  */

				case '\x0080': UnicodeChar = '\x20AC'; break; /* EURO SIGN  */
					// 				case '\x0081': UnicodeChar = CP1255Char; break; /*UNDEFINED*/
				case '\x0082': UnicodeChar = '\x201A'; break; /* SINGLE LOW-9 QUOTATION MARK  */
				case '\x0083': UnicodeChar = '\x0192'; break; /* LATIN SMALL LETTER F WITH HOOK  */
				case '\x0084': UnicodeChar = '\x201E'; break; /* DOUBLE LOW-9 QUOTATION MARK  */
				case '\x0085': UnicodeChar = '\x2026'; break; /* HORIZONTAL ELLIPSIS  */
				case '\x0086': UnicodeChar = '\x2020'; break; /* DAGGER  */
				case '\x0087': UnicodeChar = '\x2021'; break; /* DOUBLE DAGGER  */
				case '\x0088': UnicodeChar = '\x02C6'; break; /* MODIFIER LETTER CIRCUMFLEX ACCENT  */
				case '\x0089': UnicodeChar = '\x2030'; break; /* PER MILLE SIGN  */
					// 				case '\x008A': UnicodeChar = CP1255Char; break; /* UNDEFINED */
				case '\x008B': UnicodeChar = '\x2039'; break; /* SINGLE LEFT-POINTING ANGLE QUOTATION MARK  */
					// 				case '\x008C': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x008D': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x008E': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x008F': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x0090': UnicodeChar = CP1255Char; break; /* UNDEFINED */
				case '\x0091': UnicodeChar = '\x2018'; break; /* LEFT SINGLE QUOTATION MARK  */
				case '\x0092': UnicodeChar = '\x2019'; break; /* RIGHT SINGLE QUOTATION MARK  */
				case '\x0093': UnicodeChar = '\x201C'; break; /* LEFT DOUBLE QUOTATION MARK  */
				case '\x0094': UnicodeChar = '\x201D'; break; /* RIGHT DOUBLE QUOTATION MARK  */
				case '\x0095': UnicodeChar = '\x2022'; break; /* BULLET  */
				case '\x0096': UnicodeChar = '\x2013'; break; /* EN DASH  */
				case '\x0097': UnicodeChar = '\x2014'; break; /* EM DASH  */
				case '\x0098': UnicodeChar = '\x02DC'; break; /* SMALL TILDE  */
				case '\x0099': UnicodeChar = '\x2122'; break; /* TRADE MARK SIGN  */
					// 				case '\x009A': UnicodeChar = CP1255Char; break; /* UNDEFINED */
				case '\x009B': UnicodeChar = '\x203A'; break; /* SINGLE RIGHT-POINTING ANGLE QUOTATION MARK  */
					// 				case '\x009C': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x009D': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x009E': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x009F': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x00A0': UnicodeChar = CP1255Char; break; /* NO-BREAK SPACE  */
					// 				case '\x00A1': UnicodeChar = CP1255Char; break; /* INVERTED EXCLAMATION MARK  */
					// 				case '\x00A2': UnicodeChar = CP1255Char; break; /* CENT SIGN  */
					// 				case '\x00A3': UnicodeChar = CP1255Char; break; /* POUND SIGN  */
				case '\x00A4': UnicodeChar = '\x20AA'; break; /* NEW SHEQEL SIGN  */
					// 				case '\x00A5': UnicodeChar = CP1255Char; break; /* YEN SIGN  */
					// 				case '\x00A6': UnicodeChar = CP1255Char; break; /* BROKEN BAR  */
					// 				case '\x00A7': UnicodeChar = CP1255Char; break; /* SECTION SIGN  */
					// 				case '\x00A8': UnicodeChar = CP1255Char; break; /* DIAERESIS  */
					// 				case '\x00A9': UnicodeChar = CP1255Char; break; /* COPYRIGHT SIGN  */
				case '\x00AA': UnicodeChar = '\x00D7'; break; /* MULTIPLICATION SIGN  */
					// 				case '\x00AB': UnicodeChar = CP1255Char; break; /* LEFT-POINTING DOUBLE ANGLE QUOTATION MARK  */
					// 				case '\x00AC': UnicodeChar = CP1255Char; break; /* NOT SIGN  */
					// 				case '\x00AD': UnicodeChar = CP1255Char; break; /* SOFT HYPHEN  */
					// 				case '\x00AE': UnicodeChar = CP1255Char; break; /* REGISTERED SIGN  */
					// 				case '\x00AF': UnicodeChar = CP1255Char; break; /* MACRON  */
					// 				case '\x00B0': UnicodeChar = CP1255Char; break; /* DEGREE SIGN  */
					// 				case '\x00B1': UnicodeChar = CP1255Char; break; /* PLUS-MINUS SIGN  */
					// 				case '\x00B2': UnicodeChar = CP1255Char; break; /* SUPERSCRIPT TWO  */
					// 				case '\x00B3': UnicodeChar = CP1255Char; break; /* SUPERSCRIPT THREE  */
					// 				case '\x00B4': UnicodeChar = CP1255Char; break; /* ACUTE ACCENT  */
					// 				case '\x00B5': UnicodeChar = CP1255Char; break; /* MICRO SIGN  */
					// 				case '\x00B6': UnicodeChar = CP1255Char; break; /* PILCROW SIGN  */
					// 				case '\x00B7': UnicodeChar = CP1255Char; break; /* MIDDLE DOT  */
					// 				case '\x00B8': UnicodeChar = CP1255Char; break; /* CEDILLA  */
					// 				case '\x00B9': UnicodeChar = CP1255Char; break; /* SUPERSCRIPT ONE  */
				case '\x00BA': UnicodeChar = '\x00F7'; break; /* DIVISION SIGN  */
					// 				case '\x00BB': UnicodeChar = CP1255Char; break; /* RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK  */
					// 				case '\x00BC': UnicodeChar = CP1255Char; break; /* VULGAR FRACTION ONE QUARTER  */
					// 				case '\x00BD': UnicodeChar = CP1255Char; break; /* VULGAR FRACTION ONE HALF  */
					// 				case '\x00BE': UnicodeChar = CP1255Char; break; /* VULGAR FRACTION THREE QUARTERS  */
					// 				case '\x00BF': UnicodeChar = CP1255Char; break; /* INVERTED QUESTION MARK  */
				case '\x00C0': UnicodeChar = '\x05B0'; break; /* HEBREW POINT SHEVA  */
				case '\x00C1': UnicodeChar = '\x05B1'; break; /* HEBREW POINT HATAF SEGOL  */
				case '\x00C2': UnicodeChar = '\x05B2'; break; /* HEBREW POINT HATAF PATAH  */
				case '\x00C3': UnicodeChar = '\x05B3'; break; /* HEBREW POINT HATAF QAMATS  */
				case '\x00C4': UnicodeChar = '\x05B4'; break; /* HEBREW POINT HIRIQ  */
				case '\x00C5': UnicodeChar = '\x05B5'; break; /* HEBREW POINT TSERE  */
				case '\x00C6': UnicodeChar = '\x05B6'; break; /* HEBREW POINT SEGOL  */
				case '\x00C7': UnicodeChar = '\x05B7'; break; /* HEBREW POINT PATAH  */
				case '\x00C8': UnicodeChar = '\x05B8'; break; /* HEBREW POINT QAMATS  */
				case '\x00C9': UnicodeChar = '\x05B9'; break; /* HEBREW POINT HOLAM  */
					// 				case '\x00CA': UnicodeChar = CP1255Char; break; /* UNDEFINED */
				case '\x00CB': UnicodeChar = '\x05BB'; break; /* HEBREW POINT QUBUTS  */
				case '\x00CC': UnicodeChar = '\x05BC'; break; /* HEBREW POINT DAGESH OR MAPIQ  */
				case '\x00CD': UnicodeChar = '\x05BD'; break; /* HEBREW POINT METEG  */
				case '\x00CE': UnicodeChar = '\x05BE'; break; /* HEBREW PUNCTUATION MAQAF  */
				case '\x00CF': UnicodeChar = '\x05BF'; break; /* HEBREW POINT RAFE  */
				case '\x00D0': UnicodeChar = '\x05C0'; break; /* HEBREW PUNCTUATION PASEQ  */
				case '\x00D1': UnicodeChar = '\x05C1'; break; /* HEBREW POINT SHIN DOT  */
				case '\x00D2': UnicodeChar = '\x05C2'; break; /* HEBREW POINT SIN DOT  */
				case '\x00D3': UnicodeChar = '\x05C3'; break; /* HEBREW PUNCTUATION SOF PASUQ  */
				case '\x00D4': UnicodeChar = '\x05F0'; break; /* HEBREW LIGATURE YIDDISH DOUBLE VAV  */
				case '\x00D5': UnicodeChar = '\x05F1'; break; /* HEBREW LIGATURE YIDDISH VAV YOD  */
				case '\x00D6': UnicodeChar = '\x05F2'; break; /* HEBREW LIGATURE YIDDISH DOUBLE YOD  */
				case '\x00D7': UnicodeChar = '\x05F3'; break; /* HEBREW PUNCTUATION GERESH  */
				case '\x00D8': UnicodeChar = '\x05F4'; break; /* HEBREW PUNCTUATION GERSHAYIM  */
					// 				case '\x00D9': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x00DA': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x00DB': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x00DC': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x00DD': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x00DE': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x00DF': UnicodeChar = CP1255Char; break; /* UNDEFINED */
				case '\x00E0': UnicodeChar = '\x05D0'; break; /* HEBREW LETTER ALEF  */
				case '\x00E1': UnicodeChar = '\x05D1'; break; /* HEBREW LETTER BET  */
				case '\x00E2': UnicodeChar = '\x05D2'; break; /* HEBREW LETTER GIMEL  */
				case '\x00E3': UnicodeChar = '\x05D3'; break; /* HEBREW LETTER DALET  */
				case '\x00E4': UnicodeChar = '\x05D4'; break; /* HEBREW LETTER HE  */
				case '\x00E5': UnicodeChar = '\x05D5'; break; /* HEBREW LETTER VAV  */
				case '\x00E6': UnicodeChar = '\x05D6'; break; /* HEBREW LETTER ZAYIN  */
				case '\x00E7': UnicodeChar = '\x05D7'; break; /* HEBREW LETTER HET  */
				case '\x00E8': UnicodeChar = '\x05D8'; break; /* HEBREW LETTER TET  */
				case '\x00E9': UnicodeChar = '\x05D9'; break; /* HEBREW LETTER YOD  */
				case '\x00EA': UnicodeChar = '\x05DA'; break; /* HEBREW LETTER FINAL KAF  */
				case '\x00EB': UnicodeChar = '\x05DB'; break; /* HEBREW LETTER KAF  */
				case '\x00EC': UnicodeChar = '\x05DC'; break; /* HEBREW LETTER LAMED  */
				case '\x00ED': UnicodeChar = '\x05DD'; break; /* HEBREW LETTER FINAL MEM  */
				case '\x00EE': UnicodeChar = '\x05DE'; break; /* HEBREW LETTER MEM  */
				case '\x00EF': UnicodeChar = '\x05DF'; break; /* HEBREW LETTER FINAL NUN  */
				case '\x00F0': UnicodeChar = '\x05E0'; break; /* HEBREW LETTER NUN  */
				case '\x00F1': UnicodeChar = '\x05E1'; break; /* HEBREW LETTER SAMEKH  */
				case '\x00F2': UnicodeChar = '\x05E2'; break; /* HEBREW LETTER AYIN  */
				case '\x00F3': UnicodeChar = '\x05E3'; break; /* HEBREW LETTER FINAL PE  */
				case '\x00F4': UnicodeChar = '\x05E4'; break; /* HEBREW LETTER PE  */
				case '\x00F5': UnicodeChar = '\x05E5'; break; /* HEBREW LETTER FINAL TSADI  */
				case '\x00F6': UnicodeChar = '\x05E6'; break; /* HEBREW LETTER TSADI  */
				case '\x00F7': UnicodeChar = '\x05E7'; break; /* HEBREW LETTER QOF  */
				case '\x00F8': UnicodeChar = '\x05E8'; break; /* HEBREW LETTER RESH  */
				case '\x00F9': UnicodeChar = '\x05E9'; break; /* HEBREW LETTER SHIN  */
				case '\x00FA': UnicodeChar = '\x05EA'; break; /* HEBREW LETTER TAV  */
					// 				case '\x00FB': UnicodeChar = CP1255Char; break; /* UNDEFINED */
					// 				case '\x00FC': UnicodeChar = CP1255Char; break; /* UNDEFINED */
				case '\x00FD': UnicodeChar = '\x200E'; break; /* LEFT-TO-RIGHT MARK  */
				case '\x00FE': UnicodeChar = '\x200F'; break; /* RIGHT-TO-LEFT MARK  */
					// 				case '\x00FF': UnicodeChar = CP1255Char; break; /* UNDEFINED */
				default: UnicodeChar = CP1255Char; break;
			}
			return UnicodeChar;
		}


		#endregion

		#region Encoding into CP1255

		public static char ToCP1255(char UnicodeChar)
		{
			char CP1255Char;

			switch (UnicodeChar) 
			{
				case '\x20AC': CP1255Char = '\x0080'; break; /* EURO SIGN  */
				case '\x201A': CP1255Char = '\x0082'; break; /* SINGLE LOW-9 QUOTATION MARK  */
				case '\x0192': CP1255Char = '\x0083'; break; /* LATIN SMALL LETTER F WITH HOOK  */
				case '\x201E': CP1255Char = '\x0084'; break; /* DOUBLE LOW-9 QUOTATION MARK  */
				case '\x2026': CP1255Char = '\x0085'; break; /* HORIZONTAL ELLIPSIS  */
				case '\x2020': CP1255Char = '\x0086'; break; /* DAGGER  */
				case '\x2021': CP1255Char = '\x0087'; break; /* DOUBLE DAGGER  */
				case '\x02C6': CP1255Char = '\x0088'; break; /* MODIFIER LETTER CIRCUMFLEX ACCENT  */
				case '\x2030': CP1255Char = '\x0089'; break; /* PER MILLE SIGN  */
				case '\x2039': CP1255Char = '\x008B'; break; /* SINGLE LEFT-POINTING ANGLE QUOTATION MARK  */
				case '\x2018': CP1255Char = '\x0091'; break; /* LEFT SINGLE QUOTATION MARK  */
				case '\x2019': CP1255Char = '\x0092'; break; /* RIGHT SINGLE QUOTATION MARK  */
				case '\x201C': CP1255Char = '\x0093'; break; /* LEFT DOUBLE QUOTATION MARK  */
				case '\x201D': CP1255Char = '\x0094'; break; /* RIGHT DOUBLE QUOTATION MARK  */
				case '\x2022': CP1255Char = '\x0095'; break; /* BULLET  */
				case '\x2013': CP1255Char = '\x0096'; break; /* EN DASH  */
				case '\x2014': CP1255Char = '\x0097'; break; /* EM DASH  */
				case '\x02DC': CP1255Char = '\x0098'; break; /* SMALL TILDE  */
				case '\x2122': CP1255Char = '\x0099'; break; /* TRADE MARK SIGN  */
				case '\x203A': CP1255Char = '\x009B'; break; /* SINGLE RIGHT-POINTING ANGLE QUOTATION MARK  */
				case '\x20AA': CP1255Char = '\x00A4'; break; /* NEW SHEQEL SIGN  */
				case '\x00D7': CP1255Char = '\x00AA'; break; /* MULTIPLICATION SIGN  */
				case '\x00F7': CP1255Char = '\x00BA'; break; /* DIVISION SIGN  */
				case '\x05B0': CP1255Char = '\x00C0'; break; /* HEBREW POINT SHEVA  */
				case '\x05B1': CP1255Char = '\x00C1'; break; /* HEBREW POINT HATAF SEGOL  */
				case '\x05B2': CP1255Char = '\x00C2'; break; /* HEBREW POINT HATAF PATAH  */
				case '\x05B3': CP1255Char = '\x00C3'; break; /* HEBREW POINT HATAF QAMATS  */
				case '\x05B4': CP1255Char = '\x00C4'; break; /* HEBREW POINT HIRIQ  */
				case '\x05B5': CP1255Char = '\x00C5'; break; /* HEBREW POINT TSERE  */
				case '\x05B6': CP1255Char = '\x00C6'; break; /* HEBREW POINT SEGOL  */
				case '\x05B7': CP1255Char = '\x00C7'; break; /* HEBREW POINT PATAH  */
				case '\x05B8': CP1255Char = '\x00C8'; break; /* HEBREW POINT QAMATS  */
				case '\x05B9': CP1255Char = '\x00C9'; break; /* HEBREW POINT HOLAM  */
				case '\x05BB': CP1255Char = '\x00CB'; break; /* HEBREW POINT QUBUTS  */
				case '\x05BC': CP1255Char = '\x00CC'; break; /* HEBREW POINT DAGESH OR MAPIQ  */
				case '\x05BD': CP1255Char = '\x00CD'; break; /* HEBREW POINT METEG  */
				case '\x05BE': CP1255Char = '\x00CE'; break; /* HEBREW PUNCTUATION MAQAF  */
				case '\x05BF': CP1255Char = '\x00CF'; break; /* HEBREW POINT RAFE  */
				case '\x05C0': CP1255Char = '\x00D0'; break; /* HEBREW PUNCTUATION PASEQ  */
				case '\x05C1': CP1255Char = '\x00D1'; break; /* HEBREW POINT SHIN DOT  */
				case '\x05C2': CP1255Char = '\x00D2'; break; /* HEBREW POINT SIN DOT  */
				case '\x05C3': CP1255Char = '\x00D3'; break; /* HEBREW PUNCTUATION SOF PASUQ  */
				case '\x05F0': CP1255Char = '\x00D4'; break; /* HEBREW LIGATURE YIDDISH DOUBLE VAV  */
				case '\x05F1': CP1255Char = '\x00D5'; break; /* HEBREW LIGATURE YIDDISH VAV YOD  */
				case '\x05F2': CP1255Char = '\x00D6'; break; /* HEBREW LIGATURE YIDDISH DOUBLE YOD  */
				case '\x05F3': CP1255Char = '\x00D7'; break; /* HEBREW PUNCTUATION GERESH  */
				case '\x05F4': CP1255Char = '\x00D8'; break; /* HEBREW PUNCTUATION GERSHAYIM  */
				case '\x05D0': CP1255Char = '\x00E0'; break; /* HEBREW LETTER ALEF  */
				case '\x05D1': CP1255Char = '\x00E1'; break; /* HEBREW LETTER BET  */
				case '\x05D2': CP1255Char = '\x00E2'; break; /* HEBREW LETTER GIMEL  */
				case '\x05D3': CP1255Char = '\x00E3'; break; /* HEBREW LETTER DALET  */
				case '\x05D4': CP1255Char = '\x00E4'; break; /* HEBREW LETTER HE  */
				case '\x05D5': CP1255Char = '\x00E5'; break; /* HEBREW LETTER VAV  */
				case '\x05D6': CP1255Char = '\x00E6'; break; /* HEBREW LETTER ZAYIN  */
				case '\x05D7': CP1255Char = '\x00E7'; break; /* HEBREW LETTER HET  */
				case '\x05D8': CP1255Char = '\x00E8'; break; /* HEBREW LETTER TET  */
				case '\x05D9': CP1255Char = '\x00E9'; break; /* HEBREW LETTER YOD  */
				case '\x05DA': CP1255Char = '\x00EA'; break; /* HEBREW LETTER FINAL KAF  */
				case '\x05DB': CP1255Char = '\x00EB'; break; /* HEBREW LETTER KAF  */
				case '\x05DC': CP1255Char = '\x00EC'; break; /* HEBREW LETTER LAMED  */
				case '\x05DD': CP1255Char = '\x00ED'; break; /* HEBREW LETTER FINAL MEM  */
				case '\x05DE': CP1255Char = '\x00EE'; break; /* HEBREW LETTER MEM  */
				case '\x05DF': CP1255Char = '\x00EF'; break; /* HEBREW LETTER FINAL NUN  */
				case '\x05E0': CP1255Char = '\x00F0'; break; /* HEBREW LETTER NUN  */
				case '\x05E1': CP1255Char = '\x00F1'; break; /* HEBREW LETTER SAMEKH  */
				case '\x05E2': CP1255Char = '\x00F2'; break; /* HEBREW LETTER AYIN  */
				case '\x05E3': CP1255Char = '\x00F3'; break; /* HEBREW LETTER FINAL PE  */
				case '\x05E4': CP1255Char = '\x00F4'; break; /* HEBREW LETTER PE  */
				case '\x05E5': CP1255Char = '\x00F5'; break; /* HEBREW LETTER FINAL TSADI  */
				case '\x05E6': CP1255Char = '\x00F6'; break; /* HEBREW LETTER TSADI  */
				case '\x05E7': CP1255Char = '\x00F7'; break; /* HEBREW LETTER QOF  */
				case '\x05E8': CP1255Char = '\x00F8'; break; /* HEBREW LETTER RESH  */
				case '\x05E9': CP1255Char = '\x00F9'; break; /* HEBREW LETTER SHIN  */
				case '\x05EA': CP1255Char = '\x00FA'; break; /* HEBREW LETTER TAV  */
				case '\x200E': CP1255Char = '\x00FD'; break; /* LEFT-TO-RIGHT MARK  */
				case '\x200F': CP1255Char = '\x00FE'; break; /* RIGHT-TO-LEFT MARK  */
				default: CP1255Char = UnicodeChar; break;
			}
			return CP1255Char;

		}
		#endregion

	}
}
