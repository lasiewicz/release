using System;
using System.Web.Caching;

namespace Matchnet.Search
{

	public class MemberCache: MarshalByRefObject
	{
		private System.Web.Caching.Cache _Cache;

		public MemberCache()
		{
			_Cache = System.Web.HttpRuntime.Cache;
		}

		public Member GetMember(int memberID, int domainID, int privateLabelID, int languageID)
		{
			string key = string.Format("MiniProfile:{0}:{1}:{2}:{3}", memberID.ToString(), domainID.ToString(), privateLabelID.ToString(), languageID.ToString());
			
			object result = _Cache.Get(key);

			if (result != null)
			{
				return (Member)result;
			}
			else
			{
				Member member = new Member();
				member.Populate(memberID, domainID, privateLabelID, languageID);
				_Cache.Insert(key, member, null, DateTime.Now.AddHours(2), TimeSpan.Zero, CacheItemPriority.High, null);
				return member;
			}
		}

	}
}
