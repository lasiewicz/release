//using System;
//using System.Collections;
//using System.Data;
//using System.Data.SqlClient;
//using System.Collections.Specialized;
//using System.Text;
//
//using Matchnet.DataTemp;
//using Matchnet.Lib.Option;
//using Matchnet.Lib.Exceptions;
//using Matchnet.Lib.Util;
//using Matchnet.Lib;
//
//using Matchnet.Search.ServiceAdapters;
//using Matchnet.Search.ValueObjects;
//
//namespace Matchnet.Search
//{
//	public class Preferences
//	{
//		private Matchnet.Search.ValueObjects.SearchPreferenceCollection _searchPreferences; // name and index
//
//		public Preferences()
//		{
//			_searchPreferences = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
//		}
//
//		// Temporary accessor for the SearchPreferenceCollection, this entire class is being refactored out of the web solution.
//		public SearchPreferenceCollection SearchPreferences
//		{
//			get { return _searchPreferences; }
//		}
//		
//		public void Add(string name, string val) 
//		{
//			_searchPreferences[name] = val;
//		}
//
//		public void Add(string name, int val) 
//		{
//			_searchPreferences[name] = val.ToString();
//		}
//
//		public int Count
//		{
//			get 
//			{
//				return _searchPreferences.Count;
//			}
//		}
//
//		public string Value(string name) 
//		{
//			if (!_searchPreferences.ContainsKey(name)) 
//			{
//				return string.Empty;
//			}
//			return _searchPreferences[name];
//		}
//
//		public string Content(int domainID, int translationID, string name)
//		{
//			return Content(domainID, translationID, name, "");
//		}
//
//		// Where does this method belong???
//		// SearchPreferencesSA ?
//		public string Content(int domainID, int translationID, string name, string defaultValue)
//		{
//			StringBuilder sb = new StringBuilder();
//			Option option = new Option();
//			DataTable dt = option.GetOptions(name, domainID, translationID);
//			string val = Value(name);
//			int valInt = Matchnet.Constants.NULL_INT;
//
//			if (val.Length > 0)
//			{
//				valInt = Util.CInt(val);
//			}
//
//			if (valInt != Matchnet.Constants.NULL_INT)
//			{
//				foreach (DataRow row in dt.Rows)
//				{
//					if (row["Value"] != DBNull.Value)
//					{
//						int optionValue = Convert.ToInt32(row["Value"].ToString());
//						
//						if (name == "MajorType")
//						{
//							if (valInt == optionValue)
//							{
//								sb.Append(row["Content"].ToString());
//
//								break;
//							}
//						}
//						else if ((valInt & optionValue) == optionValue)
//						{
//							if (sb.Length != 0)
//							{
//								sb.Append(", ");
//							}
//							sb.Append(row["Content"].ToString());
//						}
//					}
//				}
//			}
//
//			if (sb.Length == 0)
//			{
//				return defaultValue;
//			}
//			else
//			{
//				return sb.ToString();
//			}
//		}
//
//		public bool Exists(string name) 
//		{
//			return _searchPreferences.ContainsKey(name);
//		}
//
//		public void Populate(int memberId, int domainId)
//		{
//			Populate(null, memberId, domainId);
//		}
//
//		public void Populate(string sessionKey, int memberId, int domainId)
//		{
//			if (sessionKey != null)
//			{
//				PopulateSearchPreference(sessionKey);
//			}
//
//			if (_searchPreferences.Count == 0 && memberId != 0)
//			{
//				PopulateMemberPreference(memberId, domainId);
//				if (sessionKey != null)
//				{
//					Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance.SaveSearchPreferences(sessionKey, _searchPreferences);
//				}
//			}
//		}
//
//		private void PopulateMemberPreference(int memberId, int domainId) 
//		{
//			try 
//			{
//				_searchPreferences = Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance.GetMemberSearchPrefs(memberId, domainId);
//			} 
//			catch (Exception ex) 
//			{
//				MatchnetException exception = new MatchnetException("Unable to populate Member Preference", "Preferences.Populate", ex);
//				throw exception;
//			}
//		}
//
//		private void PopulateSearchPreference(string sessionKey) 
//		{
//			try 
//			{
//				_searchPreferences = Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance.GetSessionSearchPrefs(sessionKey);
//			} 
//			catch (Exception ex) 
//			{
//				MatchnetException exception = new MatchnetException("Unable to populate Member Preference", "Preferences.Populate", ex);
//				throw exception;
//			}
//		}
//
//		public void Save(string sessionKey, int memberId, int domainId)
//		{
//			if (sessionKey.Length > 0)
//			{
//				Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance.SaveSearchPreferences(sessionKey, _searchPreferences);
//			}
//
//			if (memberId != 0)
//			{
//				Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance.SaveMemberPreferences(memberId, domainId, _searchPreferences);
//			}
//		}
//
//	}
//}
