using System;
using System.Data;

using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.Lib.Member;


namespace Matchnet.Search
{
	[Serializable]
	public class Member
	{
		private int mMemberID;
		private int mDomainID;
		private int mGenderMask;
		private int mRegionID;
		private DateTime mBirthDate;
		private DateTime mUpdateDate;
		private DateTime mInsertDate;
		private DateTime mLastLogonDate;
		private bool mDisplayPhotosToGuests;
		private string mUserName;
		private string mHeadline;
		private string mPrimaryThumbPath;

		public void Populate(int memberID, int domainID, int privateLabelID, int languageID)
		{
			MemberProfile memberProfile = new MemberProfile();
			memberProfile.Populate(memberID, domainID, privateLabelID, languageID);

			this.MemberID = memberID;
			this.DomainID = domainID;
			this.GenderMask = Util.CInt(memberProfile["GenderMask"]);
			this.RegionID = Util.CInt(memberProfile["RegionID"]);
			this.BirthDate = Convert.ToDateTime(memberProfile["BirthDate"]);
			this.UpdateDate = Convert.ToDateTime(memberProfile["UdpateDate"]);
			this.InsertDate = Convert.ToDateTime(memberProfile["InsertDate"]);
			this.LastLogonDate = Convert.ToDateTime(memberProfile["LastLogonDate"]);
			this.DisplayPhotosToGuests = Util.CInt(memberProfile["DisplayPhotosToGuests"]) == 1;
			this.UserName = memberProfile["UserName"];
			this.Headline = memberProfile["Headline"];
			this.PrimaryThumbPath = memberProfile["PrimaryThumbPath"];
		}

		public int MemberID 
		{
			get 
			{
				return mMemberID;
			}

			set 
			{
				mMemberID = value;
			}
		}

		public int DomainID 
		{
			get 
			{
				return mDomainID;
			}

			set 
			{
				mDomainID = value;
			}
		}

		public int GenderMask
		{
			get 
			{
				return mGenderMask;
			}

			set 
			{
				mGenderMask = value;
			}
		}

		public int RegionID
		{
			get 
			{
				return mRegionID;
			}

			set 
			{
				mRegionID = value;
			}
		}

		public DateTime BirthDate 
		{
			get 
			{
				return mBirthDate;
			}

			set 
			{
				mBirthDate = value;
			}
		}

		public DateTime InsertDate 
		{
			get 
			{
				return mInsertDate;
			}

			set 
			{
				mInsertDate = value;
			}
		}

		public DateTime UpdateDate 
		{
			get 
			{
				return mUpdateDate;
			}

			set 
			{
				mUpdateDate = value;
			}
		}
		
		public DateTime LastLogonDate 
		{
			get 
			{	
				return mLastLogonDate;
			}

			set 
			{
				mLastLogonDate = value;
			}
		}

		public bool DisplayPhotosToGuests
		{
			get 
			{
				return mDisplayPhotosToGuests;
			}

			set 
			{
				mDisplayPhotosToGuests = value;
			}
		}

		public string UserName
		{
			get 
			{
				return mUserName;
			}

			set 
			{
				mUserName = value;
			}
		}

		public string Headline
		{
			get 
			{
				return mHeadline;
			}

			set 
			{
				mHeadline = value;
			}
		}

		public string PrimaryThumbPath
		{
			get 
			{
				return mPrimaryThumbPath;
			}

			set 
			{
				mPrimaryThumbPath = value;
			}
		}
	}
}
