using System;

using Matchnet.Lib.Region;

namespace Matchnet.Member.MembersOnline
{
	public class MemberOnlineAddQueueItem : MemberOnlineQueueItemBase
	{
		private DateTime _BirthDate;
		private Int32 _GenderMask;
		private Int32 _LanguageMask;
		private bool _HasPhotoFlag;
		private decimal _Latitude;
		private decimal _Longitude;
		private Int32 _Depth1RegionID;
		private DateTime _InsertDate;
		private DateTime _LoginDate;
		private bool _AwayFlag;
		private Int32 _HideMask;

		public void Populate(Member member)
		{
			_BirthDate = member.Attributes.ValueDate(1, "BirthDate");
			_GenderMask = member.Attributes.ValueInt(1, "_GenderMask");
			_LanguageMask = member.Attributes.ValueInt(1, "_LanguageMask");
			_HasPhotoFlag = member.Attributes.ValueBool(1, "_HasPhotoFlag");
			_InsertDate = DateTime.Now;
			_LoginDate = member.Attributes.ValueDate(1, "LastLoginDate");
			_AwayFlag = member.Attributes.ValueBool(1, "AwayFlag");
			_HideMask = member.Attributes.ValueInt(1, "HideMask");

			//TODO:make region stuff work
			RegionLanguage region = new RegionLanguage();
			region.PopulateHierarchy(member.Attributes.ValueInt(1, "RegionID"), 1);
			_Latitude = region.Latitude;
			_Longitude = region.Longitude;
			_Depth1RegionID = region.Depth1RegionID;
		}

		public override void Execute()
		{
			MembersOnline.GetInstance().AddMember(base.MemberID,
													base.DomainID,
													_BirthDate,
													_GenderMask,
													_LanguageMask,
													_HasPhotoFlag,
													_Latitude,
													_Longitude,
													_Depth1RegionID,
													_InsertDate,
													_LoginDate,
													_AwayFlag,
													_HideMask);

		}


		#region public properties
		public DateTime BirthDate
		{
			get
			{
				return _BirthDate;
			}
			set
			{
				_BirthDate = value;
			}
		}

		public Int32 GenderMask
		{
			get
			{
				return _GenderMask;
			}
			set
			{
				_GenderMask = value;
			}
		}


		public Int32 LanguageMask
		{
			get
			{
				return _LanguageMask;
			}
			set
			{
				_LanguageMask = value;
			}
		}


		public bool HasPhotoFlag
		{
			get
			{
				return _HasPhotoFlag;
			}
			set
			{
				_HasPhotoFlag = value;
			}
		}


		public decimal Latitude
		{
			get
			{
				return _Latitude;
			}
			set
			{
				_Latitude = value;
			}
		}


		public decimal Longitude
		{
			get
			{
				return _Longitude;
			}
			set
			{
				_Longitude = value;
			}
		}


		public Int32 Depth1RegionID
		{
			get
			{
				return _Depth1RegionID;
			}
			set
			{
				_Depth1RegionID = value;
			}
		}


		public DateTime InsertDate
		{
			get
			{
				return _InsertDate;
			}
			set
			{
				_InsertDate = value;
			}
		}


		public DateTime LoginDate
		{
			get
			{
				return _LoginDate;
			}
			set
			{
				_LoginDate = value;
			}
		}


		public bool AwayFlag
		{
			get
			{
				return _AwayFlag;
			}
			set
			{
				_AwayFlag = value;
			}
		}


		public Int32 HideMask
		{
			get
			{
				return _HideMask;
			}
			set
			{
				_HideMask = value;
			}
		}
		#endregion


	}
}
