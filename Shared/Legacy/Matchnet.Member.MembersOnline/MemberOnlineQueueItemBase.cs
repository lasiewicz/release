using System;
using System.Collections;
using System.Diagnostics;

using Matchnet.MessageQueue;

namespace Matchnet.Member.MembersOnline
{
	public class MemberOnlineQueueItemBase : QueueItemBase
	{
		private int _DomainID;
		private int _MemberID;

		public static void Send(MemberOnlineQueueItemBase item)
		{
			ArrayList replicationList = MembersOnlineConfig.GetInstance().GetQueueList();
			
			foreach (QueueConfig queueConfig in replicationList)
			{
				item.QueuePath = queueConfig.QueuePath;
				QueueItemBase.Send(item);
			}
		}

		public int DomainID
		{
			get
			{
				return _DomainID;
			}
			set
			{
				_DomainID = value;
			}
		}

	
		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}
	}
}
