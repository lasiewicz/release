using System;

namespace Matchnet.Member.MembersOnline
{
	public class MemberOnlineRemoveQueueItem : MemberOnlineQueueItemBase
	{
		public override void Execute()
		{
			MembersOnline.GetInstance().RemoveMember(base.MemberID, base.DomainID);			
		}
	}
}
