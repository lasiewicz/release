using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Threading;

using Matchnet.MessageQueue;

namespace Matchnet.Member.MembersOnline
{
	public class MembersOnline : MarshalByRefObject	
	{
		private static MembersOnline _Singleton;
		private static string _Lock = "";

		private QueueProcessor _QueueProcessor;
		private Thread _ThreadSwap;
		private MembersOnlineConfig _Config;
		private DataTable _DTRead;
		private DataTable _DTWrite;

		public DataTable Table
		{
			get
			{
				return _DTRead;
			}
		}

		public static MembersOnline GetInstance()
		{
			if (_Singleton == null)
			{
				lock(_Lock)
				{
					if (_Singleton == null)
					{
						_Singleton = new MembersOnline();
					}
				}
			}

			return _Singleton;
		}


		private MembersOnline()
		{
			_Config = MembersOnlineConfig.GetInstance();

			_DTWrite = new DataTable();
			_DTWrite.Columns.Add("MemberID", typeof(Int32));
			_DTWrite.Columns.Add("DomainID", typeof(Int32));
			_DTWrite.Columns.Add("BirthDate", typeof(DateTime));
			_DTWrite.Columns.Add("GenderMask", typeof(Int32));
			_DTWrite.Columns.Add("LanguageMask", typeof(Int32));
			_DTWrite.Columns.Add("HasPhotoFlag", typeof(bool));
			_DTWrite.Columns.Add("Latitude", typeof(decimal));
			_DTWrite.Columns.Add("Longitude", typeof(decimal));
			_DTWrite.Columns.Add("Depth1RegionID", typeof(Int32));
			_DTWrite.Columns.Add("InsertDate", typeof(DateTime));
			_DTWrite.Columns.Add("LoginDate", typeof(DateTime));
			_DTWrite.Columns.Add("AwayFlag", typeof(bool));
			_DTWrite.Columns.Add("HideMask", typeof(Int32));

			DataColumn[] primaryKey = new DataColumn[2];
			primaryKey[0] = _DTWrite.Columns["MemberID"];
			primaryKey[1] = _DTWrite.Columns["DomainID"];
			_DTWrite.PrimaryKey = primaryKey;

			_DTRead = _DTWrite.Clone();

			_ThreadSwap = new Thread(new ThreadStart(SwapCycle));
			_ThreadSwap.Start();

			_QueueProcessor = new QueueProcessor(_Config.LocalServerQueuePath);
		}


		public void Startup()
		{
			Type[] queueItemTypes = new Type[]{
												  typeof(QueueItemBase),
												  typeof(MemberOnlineQueueItemBase),
												  typeof(MemberOnlineAddQueueItem),
												  typeof(MemberOnlineRemoveQueueItem)
											  };
			_QueueProcessor.Start(queueItemTypes);
		}

		
		public void Shutdown()
		{
			try
			{
				if (_ThreadSwap.IsAlive)
				{
					_ThreadSwap.Abort();
				}
			}
			catch {}
			_QueueProcessor.Stop();
			_Config.Dispose();
		}

		public void AddMember(int memberID,
								int domainID,
								DateTime birthDate,
								int genderMask,
								int languageMask,
								bool hasPhotoFlag,
								decimal latitude,
								decimal longitude,
								int depth1RegionID,
								DateTime insertDate,
								DateTime loginDate,
								bool awayFlag,
								int hideMask)
	{
			DataRow row = GetMemberRow(memberID, domainID);

			if (row != null)
			{
				row["InsertDate"] = insertDate;
				Trace.WriteLine("member updated");
			}
			else
			{
				_DTWrite.Rows.Add(new object[]{memberID,
								domainID,
								birthDate,
								genderMask,
								languageMask,
								hasPhotoFlag,
								latitude,
								longitude,
								depth1RegionID,
								insertDate,
								loginDate,
								awayFlag,
								hideMask});
				Trace.WriteLine("member added (" + _DTWrite.Rows.Count.ToString() + " rows)");
			}
		}


		public void RemoveMember(int memberID, int domainID)
		{
			DataRow row = GetMemberRow(memberID, domainID);
			
			if (row != null)
			{
				row.Delete();
				Trace.WriteLine("member removed (" + _DTWrite.Rows.Count.ToString() + " rows)");
			}
		}


		public DataRow GetMemberRow(int memberID, int domainID)
		{
			return _DTWrite.Rows.Find(new object[]{memberID, domainID});
		}


		private void SwapCycle()
		{
			while (true)
			{
				Thread.Sleep(_Config.ObjectTTL * 1000);
				try
				{
					lock (_DTRead)
					{
						Trace.WriteLine("swap");
						Trace.WriteLine("read: " + _DTRead.Rows.Count.ToString());
						Trace.WriteLine("write: " + _DTWrite.Rows.Count.ToString());
						_DTRead = _DTWrite.Copy();
						Trace.WriteLine("read: " + _DTRead.Rows.Count.ToString());
					}
				}
				catch (Exception ex)
				{
					EventLog.WriteEntry("mnMembersOnlineService", ex.ToString(), EventLogEntryType.Error);
				}
			}
		}

	}
}
