using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;

using Matchnet.DataTemp;

namespace Matchnet.Member.MembersOnline
{
	public class MembersOnlineConfig : IDisposable
	{
		private static MembersOnlineConfig _Singleton;
		private static string _LockSingleton = "";
		private static Thread _ThreadRefresh;

		private Hashtable _PartitionData;
		private int _PartitionCount;
		private int _LocalServerPort;
		private int _LocalServerOffset;
		private string _LocalServerQueuePath;
		private string _LocalPartitionQueuePath;
		private int _ObjectTTL;
		private int _ConfigTTL;
		
		public static MembersOnlineConfig GetInstance()
		{
			if (_Singleton == null)
			{
				lock (_LockSingleton)
				{
					if (_Singleton == null)
					{
						_Singleton = new MembersOnlineConfig();
						_ThreadRefresh = new Thread(new ThreadStart(_Singleton.RefreshCycle));
						_ThreadRefresh.Start();
					}
				}
			}

			return _Singleton;
		}
		

		private MembersOnlineConfig()
		{
			_PartitionData = new Hashtable();
			Refresh();
		}


		public string GetURI(int key)
		{
			return _PartitionData[GetURIKey(key % _PartitionCount)].ToString();
		}


		public int GetOffset(string serverName)
		{
			return Convert.ToInt32(_PartitionData[GetOffsetKey(serverName)].ToString());
		}

		public string GetPartitionQueuePath(int key)
		{
			return _PartitionData[GetPartitionQueueKey(key % _PartitionCount)].ToString();
		}

		public ArrayList GetQueueList()
		{
			return (ArrayList)_PartitionData[GetReplicationQueueKey(0)];
		}


		public void RefreshCycle()
		{
			while (true)
			{
				Thread.Sleep(_ConfigTTL * 1000);
				try
				{
					Refresh();
				}
				catch (Exception ex)
				{
					EventLog.WriteEntry("mnMembersOnlineService", ex.ToString(), EventLogEntryType.Error);
				}
			}
		}


		public void Refresh()
		{
			int localServerPortNew = 0;
			int localServerOffsetNew = 0;
			string localServerQueuePathNew = null;
			string localPartitionQueuePathNew = null;
			int partitionCountNew;
			int objectTTLNew = 1440;
			int configTTLNew = 30;

			Trace.WriteLine("config refresh");

			SQLDescriptor descriptor = new SQLDescriptor("mnSystem");
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@CacheName", SqlDbType.VarChar, ParameterDirection.Input, "MembersOnline");
			client.AddParameter("@PartitionCount", SqlDbType.Int, ParameterDirection.Output);

			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_CachePartition_List";
			client.PopulateCommand(command);
			DataTable dt = client.GetDataTable(command);

			Hashtable partitionDataNew = new Hashtable();
			partitionCountNew = Convert.ToInt32(command.Parameters["@PartitionCount"].Value);
			foreach (DataRow row in dt.Rows)
			{
				int offset = Convert.ToInt32(row["Offset"]);
				bool serverExists = false;
				string serverName = row["ServerName"].ToString();
				string offsetKey = GetOffsetKey(serverName);
				string uriKey = GetURIKey(offset);
				string queueKey = GetReplicationQueueKey(offset);
				string partitionQueueKey = GetPartitionQueueKey(offset);
				ArrayList queueList;

				//server offset
				if (!partitionDataNew.ContainsKey(offsetKey))
				{
					partitionDataNew.Add(offsetKey, offset);
				}

				//local server port/offset/queue
				if (serverName.ToLower() == System.Environment.MachineName.ToLower())
				{
					localServerPortNew = Convert.ToInt32(row["Port"]);
					localServerOffsetNew = offset;
					localServerQueuePathNew = row["QueuePathServer"].ToString();
					localPartitionQueuePathNew = row["QueuePathPartition"].ToString();
					objectTTLNew = Convert.ToInt32(row["ObjectTTL"]);
					configTTLNew = Convert.ToInt32(row["ConfigTTL"]);
				}

				//partition URI
				if (!partitionDataNew.ContainsKey(uriKey))
				{
					partitionDataNew.Add(uriKey, row["URI"].ToString());
				}

				//partition queue
				if (!partitionDataNew.ContainsKey(partitionQueueKey))
				{
					partitionDataNew.Add(partitionQueueKey, row["QueuePathPartition"].ToString());
				}

				//server queue
				if (!partitionDataNew.ContainsKey(queueKey))
				{
					queueList = new ArrayList();
					partitionDataNew.Add(queueKey, queueList);
				}
				else
				{
					queueList = (ArrayList)partitionDataNew[queueKey];
				}

				foreach (QueueConfig queueConfig in queueList)
				{
					if (queueConfig.ServerName == serverName)
					{
						serverExists = true;
						break;
					}
				}

				if (!serverExists)
				{
					queueList.Add(new QueueConfig(serverName, row["QueuePathServer"].ToString()));
				}
			}

			lock (_PartitionData)
			{	
				_PartitionData = partitionDataNew;
				_PartitionCount = partitionCountNew;
				_LocalServerPort = localServerPortNew;
				_LocalServerOffset = localServerOffsetNew;
				_LocalServerQueuePath = localServerQueuePathNew;
				_LocalPartitionQueuePath = localPartitionQueuePathNew;
				_ObjectTTL = objectTTLNew;
				_ConfigTTL = configTTLNew;
			}
		}


		private string GetURIKey(int offset)
		{
			return "URI:Partition:" + offset.ToString();
		}


		private string GetPartitionQueueKey(int offset)
		{
			return "PartitionQueue:" + offset.ToString();
		}
		

		private string GetOffsetKey(string serverName)
		{
			return "Offset:Server:" + serverName.ToLower();
		}


		private string GetReplicationQueueKey(int offset)
		{
			return "ReplicationQueue:Partition:" + offset;
		}


		public int LocalServerPort
		{
			get
			{
				return _LocalServerPort;
			}
		}


		public int LocalServerOffset
		{
			get
			{
				return _LocalServerOffset;
			}
		}


		public string LocalServerQueuePath
		{
			get
			{
				return _LocalServerQueuePath;
			}
		}


		public string LocalPartitionQueuePath
		{
			get
			{
				return _LocalPartitionQueuePath;
			}
		}

		public int ObjectTTL
		{
			get
			{
				return _ObjectTTL;
			}
		}


		#region IDisposable Members

		public void Dispose()
		{
			if (_ThreadRefresh.IsAlive)
			{
				_ThreadRefresh.Abort();
			}
		}

		#endregion
	}

	public class QueueConfig
	{
		private string _ServerName;
		private string _QueuePath;

		public QueueConfig(string serverName, string queuePath)
		{
			_ServerName = serverName;
			_QueuePath = queuePath;
		}

		public string ServerName
		{
			get
			{
				return _ServerName;
			}
		}

		public string QueuePath
		{
			get
			{
				return _QueuePath;
			}
		}
	}

}
