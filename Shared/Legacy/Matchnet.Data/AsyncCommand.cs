using System;
using System.Collections;
using System.Data;

namespace Matchnet.DataTemp
{
	/// <summary>
	/// Represents a SQL command that is to be run asynchronously. This object 
	/// will be serialized and saved to the file system for future execution.
	/// </summary>
	[Serializable]
	public class AsyncCommand
	{
		private SQLDescriptor m_descriptor;
		private string m_commandText;
		private string m_connectionString;
		private ArrayList m_parameters;
		private CommandType m_commandType;

		/// <summary>
		/// Creates a new instance with the specified connection string
		/// </summary>
		/// <param name="connectionString">The connectioin string to use</param>
		/// <param name="commandText">the text of the sql command to run</param>
		/// <param name="commandType">the type of sql command</param>
		public AsyncCommand(string connectionString, string commandText, CommandType commandType) 
		{
			m_descriptor = null;
			m_connectionString = connectionString;
			m_commandText = commandText;
			m_commandType = commandType;
		}

		/// <summary>
		/// Creates a new instance with a specified SQLDescriptor.
		/// </summary>
		/// <param name="descriptor">a Matchnet.Lib.Data.SQLDescriptor instance</param>
		/// <param name="commandText">the text of the sql command to run</param>
		/// <param name="commandType">the type of sql command</param>
		public AsyncCommand(SQLDescriptor descriptor, string commandText, CommandType commandType) 
		{
			m_descriptor = descriptor;
			m_connectionString = null;
			m_commandText = commandText;
			m_commandType = commandType;
			m_parameters = new ArrayList();
		}

		/// <summary>
		/// A predefined connection string to use when executing the command.
		/// </summary>
		public string ConnectionString 
		{
			get 
			{
				return m_connectionString;
			}
		}

		/// <summary>
		/// A SQLDescriptor that is used to obtain a connection string to 
		/// use when executing the command.
		/// </summary>
		public SQLDescriptor Descriptor 
		{
			get 
			{
				return m_descriptor;
			}
		}

		/// <summary>
		/// Adds a parameter to the collection of parameters to 
		/// be used when executing the command
		/// </summary>
		/// <param name="parameter">The parameter to add to the collection of parameters used when executing the command</param>
		public void AddParameter(AsyncParameter parameter) 
		{
			m_parameters.Add(parameter);
		}

		/// <summary>
		/// The command text to run
		/// </summary>
		public string CommandText 
		{
			get 
			{
				return m_commandText;
			}
		}

		/// <summary>
		/// The type of command to run
		/// </summary>
		public CommandType Type 
		{
			get 
			{
				return m_commandType;
			}
		}

		/// <summary>
		/// The collection of parameters to be used when
		/// executing the command
		/// </summary>
		public ArrayList Parameters 
		{
			get 
			{
				return m_parameters;
			}
		}
	}
}
