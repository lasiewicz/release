using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet.Data;

namespace Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblLogicalDatabase;
		private System.Windows.Forms.TextBox txtLogicalDatabase;
		private System.Windows.Forms.TextBox txtKey;
		private System.Windows.Forms.Label lblKey;
		private System.Windows.Forms.Button btnConnectionString;
		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.Button btnConnectionStrings;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblLogicalDatabase = new System.Windows.Forms.Label();
			this.txtLogicalDatabase = new System.Windows.Forms.TextBox();
			this.txtKey = new System.Windows.Forms.TextBox();
			this.lblKey = new System.Windows.Forms.Label();
			this.btnConnectionString = new System.Windows.Forms.Button();
			this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.btnConnectionStrings = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblLogicalDatabase
			// 
			this.lblLogicalDatabase.Location = new System.Drawing.Point(8, 8);
			this.lblLogicalDatabase.Name = "lblLogicalDatabase";
			this.lblLogicalDatabase.Size = new System.Drawing.Size(96, 16);
			this.lblLogicalDatabase.TabIndex = 0;
			this.lblLogicalDatabase.Text = "Logical Database";
			this.lblLogicalDatabase.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtLogicalDatabase
			// 
			this.txtLogicalDatabase.Location = new System.Drawing.Point(104, 8);
			this.txtLogicalDatabase.Name = "txtLogicalDatabase";
			this.txtLogicalDatabase.Size = new System.Drawing.Size(176, 20);
			this.txtLogicalDatabase.TabIndex = 1;
			this.txtLogicalDatabase.Text = "";
			// 
			// txtKey
			// 
			this.txtKey.Location = new System.Drawing.Point(104, 32);
			this.txtKey.Name = "txtKey";
			this.txtKey.Size = new System.Drawing.Size(56, 20);
			this.txtKey.TabIndex = 3;
			this.txtKey.Text = "";
			// 
			// lblKey
			// 
			this.lblKey.Location = new System.Drawing.Point(8, 32);
			this.lblKey.Name = "lblKey";
			this.lblKey.Size = new System.Drawing.Size(96, 16);
			this.lblKey.TabIndex = 2;
			this.lblKey.Text = "Key";
			this.lblKey.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// btnConnectionString
			// 
			this.btnConnectionString.Location = new System.Drawing.Point(168, 32);
			this.btnConnectionString.Name = "btnConnectionString";
			this.btnConnectionString.Size = new System.Drawing.Size(112, 20);
			this.btnConnectionString.TabIndex = 4;
			this.btnConnectionString.Text = "Connection String";
			this.btnConnectionString.Click += new System.EventHandler(this.btnConnectionString_Click);
			// 
			// txtOutput
			// 
			this.txtOutput.Location = new System.Drawing.Point(288, 8);
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(320, 264);
			this.txtOutput.TabIndex = 5;
			this.txtOutput.Text = "";
			// 
			// btnConnectionStrings
			// 
			this.btnConnectionStrings.Location = new System.Drawing.Point(168, 56);
			this.btnConnectionStrings.Name = "btnConnectionStrings";
			this.btnConnectionStrings.Size = new System.Drawing.Size(112, 20);
			this.btnConnectionStrings.TabIndex = 6;
			this.btnConnectionStrings.Text = "Connection Strings";
			this.btnConnectionStrings.Click += new System.EventHandler(this.btnConnectionStrings_Click);
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 277);
			this.Controls.Add(this.btnConnectionStrings);
			this.Controls.Add(this.txtOutput);
			this.Controls.Add(this.btnConnectionString);
			this.Controls.Add(this.txtKey);
			this.Controls.Add(this.lblKey);
			this.Controls.Add(this.txtLogicalDatabase);
			this.Controls.Add(this.lblLogicalDatabase);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "FormMain";
			this.Text = "Matchnet.Data Harness";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void AppendMessage(string message) 
		{
			txtOutput.AppendText(message + "\r\n");
		}

		private void btnConnectionString_Click(object sender, System.EventArgs e)
		{
			try 
			{
				SQLConfig config = SQLConfig.GetInstance();
				int key = txtKey.Text.Trim() == string.Empty ? 0 : System.Convert.ToInt32(txtKey.Text.Trim());
				AppendMessage(config.GetConnectionString(txtLogicalDatabase.Text, key, false));
			} 
			catch (Exception ex) 
			{
				AppendMessage(ex.ToString());
			}
		}

		private void btnConnectionStrings_Click(object sender, System.EventArgs e)
		{
			try 
			{
				SQLConfig config = SQLConfig.GetInstance();
				int key = txtKey.Text.Trim() == string.Empty ? 0 : System.Convert.ToInt32(txtKey.Text.Trim());

				ArrayList list = config.GetLogicalConnectionStrings(txtLogicalDatabase.Text, key);
				foreach (string connectionString in list) 
				{
					AppendMessage(connectionString);
				}
			} 
			catch (Exception ex) 
			{
				AppendMessage(ex.ToString());
			}
		
		}
	}
}
