using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Matchnet.DataTemp
{
	/// <summary>
	/// A SQLClient is a class that provides access to persisted
	/// SQL Data.
	/// </summary>
	public class SQLClient
	{
		/// <summary>
		/// The directory where asyncronous commands will be written to and read from
		/// </summary>
		public static string ASYNC_ROOT_PATH = @"c:\matchnet\async\net";

		private SQLConfig m_config;
		private ArrayList m_parameters;
		private string m_connectionString;

		/// <summary>
		/// A predefined connection string used when connecting to the database
		/// </summary>
		public string ConnectionString 
		{
			get 
			{
				return m_connectionString;
			}
		}

		/// <summary>
		/// Constructs a new instance with a predefined connection string
		/// </summary>
		/// <param name="connectionString"></param>
		public SQLClient(string connectionString) 
		{
			m_connectionString = connectionString;
			Init(null);
		}

		/// <summary>
		/// Constructs a new instance with a SQLDescriptor instance
		/// </summary>
		/// <param name="descriptor"></param>
		public SQLClient(SQLDescriptor descriptor) 
		{
			Init(descriptor);			
		}

		private void Init(SQLDescriptor descriptor) 
		{
			m_config = SQLConfig.GetInstance();
			m_parameters = new ArrayList();
			if (descriptor != null) 
			{
				m_connectionString = m_config.GetConnectionString(descriptor);
			}
		}

		/// <summary>
		/// Adds a parameter to the collection of parameters to be used when executing
		/// a command against the database with a default direction of input.
		/// </summary>
		/// <param name="name">The name of the parameter</param>
		/// <param name="dataType">The type of the parameter</param>
		public void AddParameter(string name, SqlDbType dataType) 
		{
			AddParameter(name, dataType, ParameterDirection.Input);
		}
	
		/// <summary>
		/// Adds a parameter to the collection of parameters to be used when executing
		/// a command against the database.
		/// </summary>
		/// <param name="name">The name of the parameter</param>
		/// <param name="dataType">The type of the parameter</param>
		/// <param name="parameterDirection">The direction of the parameter</param>
		public void AddParameter(string name, SqlDbType dataType, ParameterDirection parameterDirection) 
		{
			SqlParameter parameter = new SqlParameter(name, dataType);
			parameter.Direction = parameterDirection;
			m_parameters.Add(parameter);
		}

		/// <summary>
		/// Adds a parameter to the collection of parameters to be used when executing
		/// a command against the database.
		/// </summary>
		/// <param name="name">The name of the parameter</param>
		/// <param name="dataType">The type of the parameter</param>
		/// <param name="parameterDirection">The direction of the parameter</param>
		/// <param name="parameterValue">The value of the parameter</param>
		public void AddParameter(string name, SqlDbType dataType, ParameterDirection parameterDirection, Object parameterValue) 
		{
			SqlParameter parameter = new SqlParameter(name, dataType);
			parameter.Direction = parameterDirection;
			parameter.Value = parameterValue;
			m_parameters.Add(parameter);
		}

		/// <summary>
		/// Adds a parameter to the collection of parameters to be used when executing
		/// a command against the database.
		/// </summary>
		/// <param name="name">The name of the parameter</param>
		/// <param name="dataType">The type of the parameter</param>
		/// <param name="parameterDirection">The direction of the parameter</param>
		/// <param name="parameterValue">The value of the parameter</param>
		/// <param name="size">The size of the parameter</param>
		public void AddParameter(string name, SqlDbType dataType, ParameterDirection parameterDirection, Object parameterValue, int size) 
		{
			SqlParameter parameter = new SqlParameter(name, dataType, size);
			parameter.Direction = parameterDirection;
			parameter.Value = parameterValue;
			m_parameters.Add(parameter);
		}			


		/// <summary>
		/// Executes a command against the database and returns the result in a populated DataTable.
		/// A default CommandType of text is used.
		/// </summary>
		/// <param name="commandText">The text of the command to execute</param>
		/// <returns>A populated DataTable</returns>
		public DataTable GetDataTable(string commandText) 
		{
			return GetDataTable(commandText, CommandType.Text);
		}

		/// <summary>
		/// Executes a command against the database and returns the result in a populated DataTable.
		/// </summary>
		/// <param name="commandText">The text of the command to execute</param>
		/// <param name="commandType">The type of the command to execute</param>
		/// <returns>A populated DataTable</returns>
		public DataTable GetDataTable(string commandText, CommandType commandType) 
		{
			SqlCommand command = new SqlCommand(commandText); 
			command.CommandType = commandType;
			PopulateCommand(command);
			return GetDataTable(command);
		}

		/// <summary>
		/// Executes a command against the database and returns the result in a populated DataTable.
		/// </summary>
		/// <param name="command">A SqlCommand instance to use for executing the command</param>
		/// <returns>A populated DataTable</returns>
		public DataTable GetDataTable(SqlCommand command) 
		{
			if (m_connectionString == null || m_connectionString.Trim() == string .Empty) 
			{
				throw new Exception("ConnectionString is not defined");
			}

			SqlConnection connection = new SqlConnection(m_connectionString);
			command.Connection = connection;
			
			SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
			DataTable dataTable = new DataTable();

			try 
			{
				connection.Open();
				dataAdapter.Fill(dataTable);
				return dataTable;				
			} 
			catch (SqlException se) 
			{
				string strError = "Sql Error executing command [" + command.CommandText + "]\n";
				foreach (SqlParameter param in command.Parameters)
				{
					strError += "[" + param.ParameterName + "][" + param.SqlDbType.ToString() + "] = [";
					if (param.Value == null)
					{
						strError += "null]\n";
					}
					else
					{
						strError += param.Value.ToString() + "]\n";
					}
				}
				throw new Exception(strError, se);
			}
			catch (Exception e) 
			{
				throw new Exception("Error executing command [" + command.CommandText + "]", e);
			}
			finally 
			{
				if (connection.State != ConnectionState.Closed) 
				{
					connection.Close();
				}
			}			
		}

		/// <summary>
		/// Executes a command against the database.
		/// A default CommandType of text is used.
		/// </summary>
		/// <param name="commandText">The text of the command to execute</param>
		public void ExecuteNonQuery(string commandText) 
		{
			SqlCommand command = new SqlCommand(commandText); 
			command.CommandType = CommandType.Text;
			PopulateCommand(command);

			ExecuteNonQuery(command);		
		}

		/// <summary>
		/// Executes a command against the database.
		/// </summary>
		/// <param name="commandText">The text of the command to execute</param>
		/// <param name="commandTimeout">The timeout limit for the execution of the command</param>
		public void ExecuteNonQuery(string commandText, int commandTimeout) 
		{
			SqlCommand command = new SqlCommand(commandText); 
			command.CommandType = CommandType.Text;
			PopulateCommand(command);
			command.CommandTimeout = commandTimeout;
			ExecuteNonQuery(command);		
		}

		/// <summary>
		/// Executes a command against the database.
		/// </summary>
		/// <param name="commandText">The text of the command to execute</param>
		/// <param name="commandType">The type of the command to execute</param>
		public void ExecuteNonQuery(string commandText, CommandType commandType) 
		{
			SqlCommand command = new SqlCommand(commandText); 
			command.CommandType = commandType;
			PopulateCommand(command);
			ExecuteNonQuery(command);
		}

		/// <summary>
		/// Executes a command against the database.
		/// </summary>
		/// <param name="command">The command to execute</param>
		public void ExecuteNonQuery(SqlCommand command) 
		{
			if (m_connectionString == null || m_connectionString.Trim() == string.Empty) 
			{
				throw new Exception("ConnectionString is not defined");
			}
			
			SqlConnection connection = new SqlConnection(m_connectionString);

			try 
			{
				connection.Open();
				command.Connection = connection;
				command.ExecuteNonQuery();
				
				return;				
			} 
			catch (Exception e) 
			{
				throw new Exception("Error executing query [" + command.CommandText + "]", e);
			}
			finally 
			{
				if (connection.State != ConnectionState.Closed) 
				{
					connection.Close();
				}
			}			
		}

		/// <summary>
		/// Queues a command to be run against the database by serializing the request and writing
		/// it to the filesystem. It is expected that another process will read the request and
		/// execute the command. Only commands that don't expect to return data should be queued for
		/// asynchronous operation.
		/// </summary>
		/// <param name="commandText"></param>
		/// <param name="commandType"></param>
		public void QueueAsync(string commandText, CommandType commandType) 
		{
			if (m_connectionString == null || m_connectionString.Trim() == string.Empty) 
			{
				throw new Exception("ConnectionString is not defined");
			}
			try 
			{
				AsyncCommand command = new AsyncCommand(m_connectionString, commandText, commandType);
				foreach (SqlParameter param in m_parameters) 
				{
					AsyncParameter parameter = new AsyncParameter(param.ParameterName, param.SqlDbType, param.Direction, param.Value, param.Size);
					command.AddParameter(parameter);
				}
				WriteAsync(command);
			} 
			catch (Exception e) 
			{
				throw new Exception("Error queueing async [" + commandText + "]", e);
			}
		}

		/// <summary>
		/// Writes a serialized command to the filesystem.
		/// </summary>
		/// <param name="command">The command to serialize and write to the filesystem</param>
		public void WriteAsync(AsyncCommand command) 
		{
			Stream fileStream = null;
			try 
			{
				BinaryFormatter formatter = new BinaryFormatter();
				fileStream = System.IO.File.Create(ASYNC_ROOT_PATH + System.Guid.NewGuid() + ".async");
				formatter.Serialize(fileStream, command);
			} 
			catch (Exception e) 
			{
				throw new Exception("Error writing command [" + command.CommandText + "]", e);
			} 
			finally 
			{
				if (fileStream != null) 
				{
					fileStream.Close();
				}
			}
		}

		/// <summary>
		/// Executes a command that has been serialized and written to the filesystem.
		/// </summary>
		/// <param name="file">The full path of the file to be executed</param>
		public void ExecuteAsync(string file) 
		{
			Stream fileStream = null;
			try 
			{
				BinaryFormatter formatter = new BinaryFormatter();
				fileStream = System.IO.File.OpenRead(file);
				AsyncCommand command = (AsyncCommand)formatter.Deserialize(fileStream);
				ClearParameters();
				foreach (AsyncParameter param in command.Parameters) 
				{
					AddParameter(param.Name, param.Type, param.Direction, param.Value, param.Size);
				}
				m_connectionString = command.ConnectionString;
				ExecuteNonQuery(command.CommandText, command.Type);
			} 
			catch (Exception e) 
			{
				throw new Exception("Error executing command [" + file + "]", e);
			} 
			finally 
			{
				if (fileStream != null) 
				{
					fileStream.Close();
				}
			}
		}
		
		/// <summary>
		/// Returns a primary key by name with a default value of 1 for the minimum value
		/// to return.
		/// </summary>
		/// <param name="name">The name of the primary key</param>
		/// <returns>A primary key</returns>
		public static int GetPrimaryKey(string name) 
		{
			return GetPrimaryKey(name, 1);
		}

		/// <summary>
		/// Returns a primary key by name.
		/// </summary>
		/// <param name="name">The name of the primary key</param>
		/// <param name="minValue">The minimum value to return</param>
		/// <returns>A primary key</returns>
		public static int GetPrimaryKey(string name, int minValue) 
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMaster", 0);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@Name", SqlDbType.VarChar, ParameterDirection.Input, name, 50);
			client.AddParameter("@MinValue", SqlDbType.Int, ParameterDirection.Input, minValue);
			client.AddParameter("@PrimaryKey", SqlDbType.Int, ParameterDirection.Output);

			SqlCommand cmd = new SqlCommand("up_PrimaryKey_GetNext");
			cmd.CommandType = CommandType.StoredProcedure;
			client.PopulateCommand(cmd);

			
			client.ExecuteNonQuery(cmd);
			return (int) cmd.Parameters["@PrimaryKey"].Value;			
		}

		/// <summary>
		/// Populates a SqlCommand with the collection of parameters
		/// defined in this client.
		/// </summary>
		/// <param name="command">The SqlCommand to populate</param>
		public void PopulateCommand(SqlCommand command) 
		{
			if (m_parameters.Count > 0) 
			{
				foreach (SqlParameter param in m_parameters) 
				{
					command.Parameters.Add(param);
				}
			}
		}

		/// <summary>
		/// Clears the collection of parameters to be used when executing a command.
		/// </summary>
		public void ClearParameters() 
		{
			m_parameters.Clear();
		}
	}
}
