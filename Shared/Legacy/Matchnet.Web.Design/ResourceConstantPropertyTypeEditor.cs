using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;


namespace Matchnet.Web.Design
{
	/// <summary>
	/// Summary description for ResourceConstantEditor.
	/// </summary>
	public class ResourceConstantPropertyEditor : UITypeEditor
	{
		public ResourceConstantPropertyEditor()
		{
		}


		public override object EditValue(ITypeDescriptorContext context,
			IServiceProvider serviceProvider, object value)
		{

			if ( context != null && serviceProvider != null ) 
			{
				IWindowsFormsEditorService edSvc =
					(IWindowsFormsEditorService) serviceProvider.GetService(
					typeof(IWindowsFormsEditorService));

				if ( edSvc != null )
				{
					ResourceConstantPropertyTypeEditorForm form = new ResourceConstantPropertyTypeEditorForm();

					form.Value = (string)value;

					DialogResult result = edSvc.ShowDialog(form);

					if (result == DialogResult.OK)
					{
						value = form.Value;
					}
				}
			}
			return value;
		}

		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if ( context != null )
			{
				return UITypeEditorEditStyle.Modal;
			}

			return base.GetEditStyle(context);
		}
	}
}


