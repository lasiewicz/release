using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.Web.Design
{
	/// <summary>
	/// Summary description for ResourceConstantPropertyTypeEditorForm.
	/// </summary>
	public class ResourceConstantPropertyTypeEditorForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox _ResourceConstantText;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
		public ResourceConstantPropertyTypeEditorForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._ResourceConstantText = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// _ResourceConstantText
			// 
			this._ResourceConstantText.Location = new System.Drawing.Point(136, 24);
			this._ResourceConstantText.Name = "_ResourceConstantText";
			this._ResourceConstantText.Size = new System.Drawing.Size(456, 20);
			this._ResourceConstantText.TabIndex = 0;
			this._ResourceConstantText.Text = "textBox1";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(104, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Resource Constant";
			// 
			// ResourceConstantPropertyTypeEditorForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(648, 69);
			this.Controls.Add(this.label1);
			this.Controls.Add(this._ResourceConstantText);
			this.Name = "ResourceConstantPropertyTypeEditorForm";
			this.Text = "Resource Editor";
			this.ResumeLayout(false);

		}
		#endregion

		public string Value
		{
			get
			{
				return _ResourceConstantText.Text;
			}
			set
			{
				_ResourceConstantText.Text = value;
			}
		}
	}
}
