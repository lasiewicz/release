using System;
using System.Collections.Specialized;
using Matchnet.Lib;
using Matchnet.Lib.Util;

namespace Matchnet.Telephony.Collections
{
	/// <summary>
	/// Summary description for TalkUserPlan.
	/// </summary>
	[Serializable]
	public class TalkUserPlan
	{

		#region Private Memebers
		private int _talkuserplanID = Constants.NULL_NUMBER;
		private int _talkplanID = Constants.NULL_NUMBER;
		private int _talkuserID = Constants.NULL_NUMBER;
		#endregion
		
		#region Constructors
		public TalkUserPlan()
		{
		}

		public TalkUserPlan(int talkuserID, int talkplanID)
		{
			
		}

		public TalkUserPlan(string telephonenumber, int talkplanID)
		{
			
		}
		#endregion

		#region Properties
		public int TalkUserPlanID
		{
			get{return _talkuserplanID;}
			set{_talkuserplanID = value;}
		}

		public int TalkUserID
		{
			get{return _talkuserID;}
			set{_talkuserID = value;}
		}

		public int TalkPlanID
		{
			get{return _talkplanID;}
			set{_talkplanID = value;}
		}
		#endregion
	}
}
