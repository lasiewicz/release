using System;
using System.Collections.Specialized;
using Matchnet.Lib;
using Matchnet.Lib.Util;

namespace Matchnet.Telephony.ValueObjects
{
	/// <summary>
	/// Summary description for TalkPlan.
	/// </summary>
	[Serializable]
	public class TalkPlan
	{

		#region Private Members
		private int _TalkPlanID = Constants.NULL_NUMBER;
		private int _CurrencyId = Constants.NULL_NUMBER;
		private string _Currency= Constants.NULL_STRING;
		private int _ResourceId = Constants.NULL_NUMBER;
		private string _Resource= Constants.NULL_STRING;
		private decimal _InitialCost= Constants.NULL_DECIMAL;
		private int _InitialDuration = Constants.NULL_NUMBER;
		private int _InitialDurationTypeId = Constants.NULL_NUMBER;
		private string _InitialDurationType = Constants.NULL_STRING;
		private decimal _RenewCost= Constants.NULL_DECIMAL;
		private int _RenewDuration = Constants.NULL_NUMBER;
		private int _RenewDurationTypeId = Constants.NULL_NUMBER;
		private string _RenewDurationType= Constants.NULL_STRING;
		private int _PlanTypeMask = Constants.NULL_NUMBER;
		private string _PlanType= Constants.NULL_STRING;
		private int _RenewAttempts = Constants.NULL_NUMBER;
		private System.DateTime _UpdateDate = DateTime.MinValue;
		#endregion

		#region Constructors
		public TalkPlan()
		{
		}

		public TalkPlan(int talkplanID)
		{
			_TalkPlanID = talkplanID;
		}


		#endregion

		#region Properties
		//TODO Confirm Properties from DB
		public int TalkPlanID
		{
			get{return _TalkPlanID;}
		}

		public int CurrencyId
		{
			get{return _CurrencyId;}
			set{_CurrencyId = value;}
		}

		public string Currency
		{
			get{return _Currency;}
			set{_Currency = value;}
		}

		public int ResourceId
		{
			get{return _ResourceId;}
			set{_ResourceId = value;}
		}

		public string Resource
		{
			get{return _Resource;}
			set{_Resource = value;}
		}

		public decimal InitialCost
		{
			get{return _InitialCost;}
			set{_InitialCost = value;}
		}

		public int InitialDuration
		{
			get{return _InitialDuration;}
			set{_InitialDuration = value;}
		}

		public int InitialDurationTypeId
		{
			get{return _InitialDurationTypeId;}
			set{_InitialDurationTypeId = value;}
		}

		public string InitialDurationType
		{
			get{return _InitialDurationType;}
			set{_InitialDurationType = value;}
		}

		public decimal RenewCost
		{
			get{return _RenewCost;}
			set{_RenewCost = value;}
		}

		public int RenewDuration
		{
			get{return _RenewDuration;}
			set{_RenewDuration = value;}
		}

		public int RenewDurationTypeId
		{
			get{return _RenewDurationTypeId;}
			set{_RenewDurationTypeId = value;}
		}

		public string RenewDurationType
		{
			get{return _RenewDurationType;}
			set{_RenewDurationType = value;}
		}

		public int PlanTypeMask
		{
			get{return _PlanTypeMask;}
			set{_PlanTypeMask = value;}
		}

		public string PlanType
		{
			get{return _PlanType;}
			set{_PlanType = value;}
		}

		public int RenewAttempts
		{
			get{return _RenewAttempts;}
			set{_RenewAttempts = value;}
		}

		public System.DateTime UpdateDate
		{
			get{return _UpdateDate;}
			set{_UpdateDate = value;}
		}
		#endregion
	}
}
