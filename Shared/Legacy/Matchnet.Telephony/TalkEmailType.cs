using System;

using Matchnet.Lib;


namespace Matchnet.Telephony
{
	/// <summary>
	/// Summary description for TalkEmailType.
	/// </summary>
	public class TalkEmailType
	{
		public const int MembershipExpirationNotice = 605760;
		public const int MembershipExpirationWarning = 605758;
		public const int MembershipInstructions = 605764;
		public const int VoiceMailAttemptedMessage = 605762;
		public const int VoiceMailNewMessage = 605756;
	}
}
