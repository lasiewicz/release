using System;

using Matchnet.Content;
using Matchnet.Labels;
using Matchnet.Lib;

using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Telephony
{
	/// <summary>
	/// Summary description for Talk.
	/// </summary>
	public class Talk
	{

		private PrivateLabel _PrivateLabel = new PrivateLabel();
		private Matchnet.CachingTemp.Cache _Cache = Matchnet.CachingTemp.Cache.GetInstance();

		public Talk()
		{

		}		

		private Matchnet.CachingTemp.Cache Cache
		{
			get
			{
				return _Cache;
			}
		}

		public bool SendEmail(int siteID, int toMemberID, int fromMemberID, int resourceID)
		{
			try
			{
				Telephony.BusinessLogic.TalkNotificationBL oTalkBL = new Telephony.BusinessLogic.TalkNotificationBL();
				//TODO: Find out more re Email Classification for subject and content
				Matchnet.Member.ServiceAdapters.Member recipientMember = MemberSA.Instance.GetMember(
					GetDomainID(siteID)
					, siteID
					, GetLanguageID(siteID)
					, toMemberID
					, MemberLoadFlags.None);

				string MemberName = recipientMember.Username;
				string subject = "Talk Service Notice"; 
				string content = GetResource(resourceID, siteID, MemberName);
				return oTalkBL.SendEmail(GetDomainID(siteID), GetLanguageID(siteID), siteID, toMemberID, fromMemberID, subject, content);
			}
			catch(Exception x)
			{
				throw x;
			}
		}

		public bool SaveToHotList(int siteID, int toMemberID, int fromMemberID, HotListCategory category)
		{
			try
			{
				Telephony.BusinessLogic.TalkNotificationBL oTalkBL = new Telephony.BusinessLogic.TalkNotificationBL();
				return oTalkBL.SaveToHotList(GetDomainID(siteID), toMemberID, fromMemberID, category);
			}
			catch(Exception x)
			{
				throw x;
			}
		}

		public bool SaveToHotList(int siteID, int toMemberID, int fromMemberID, HotListType type, HotListCategory category)
		{
			try
			{
				Telephony.BusinessLogic.TalkNotificationBL oTalkBL = new Telephony.BusinessLogic.TalkNotificationBL();
				return oTalkBL.SaveToHotList(GetDomainID(siteID), toMemberID, fromMemberID, type, category);
			}
			catch(Exception x)
			{
				throw x;
			}
		}

		#region PrivateLabel & Resource
			private int GetLanguageID(int siteID)
			{
				_PrivateLabel.Load(siteID);
				return _PrivateLabel.LanguageID;
			}

			private int GetDomainID(int siteID)
			{
				_PrivateLabel.Load(siteID);
				return _PrivateLabel.DomainID;
			}

			private string GetResource(int resourceID, int siteID, string memberName)
			{
				string sValue = Matchnet.Constants.NULL_STRING;
				Translator _Translator = new Translator(Cache);

				_Translator = SetTranslator(siteID, memberName, ref _Translator);
				sValue = _Translator.Value(resourceID, GetLanguageID(siteID), siteID);
				return sValue;
			}

			private Translator SetTranslator(int siteID, string memberName, ref Translator translator) 
			{
				// Add substitution tokens
				// forces PrivateLabel initialization
				_PrivateLabel.Load(siteID);
				translator.AddToken("USERNAME", memberName);
				translator.AddToken("TALKANI", "310-555-1212"); //TODO: Need Lookup
				translator.AddToken("BRANDNAME", _PrivateLabel.SiteDescription);
				translator.AddToken("SITEURL", "http://" + _PrivateLabel.Host + "." + _PrivateLabel.URI);
				translator.AddToken("PLDOMAIN", _PrivateLabel.SiteDescription);
				translator.AddToken("PLSERVERNAME", _PrivateLabel.Host);
				translator.AddToken("YEAR", System.DateTime.Now.Year.ToString());
				translator.AddToken("PLPHONENUMBER", _PrivateLabel.PhoneNumber);
				translator.AddToken("CSSUPPORTPHONENUMBER", translator.Value(ConstantsTemp.CS_SUPPORT_PHONE_NUMBER, _PrivateLabel.LanguageID, _PrivateLabel.PrivateLabelID));
				
				return translator;
			}
		#endregion
	}
}
