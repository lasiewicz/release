using System;

using Matchnet.Lib;
using Matchnet.Email;
using Matchnet.Email.ValueObjects;

using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Telephony.BusinessLogic
{
	/// <summary>
	/// Summary description for TalkNotificationBL.
	/// </summary>
	public class TalkNotificationBL
	{
		public TalkNotificationBL()
		{
			//
			// TODO: Add constructor logic here
			//
		}



		#region Email Notification & Hotlist
		public bool SendEmail(int domainID, int languageID, int siteID, int toMemberID, int fromMemberID, string subject, string content) 
		{
			EmailMessage oMailMessage = null;
			string messageSubject = Matchnet.Constants.NULL_STRING;
			string messageBody = Matchnet.Constants.NULL_STRING;

			try 
			{
				#region FormatMessage
				messageSubject = subject;
				messageBody = content;
					
				oMailMessage = new EmailMessage();

				//Get Member info for person receiving voicemail
				oMailMessage.ToMemberID = toMemberID;
				Matchnet.Member.ServiceAdapters.Member recipientMember = MemberSA.Instance.GetMember(
					domainID
					, siteID
					, languageID
					, oMailMessage.ToMemberID
					, MemberLoadFlags.None);
				oMailMessage.ToUserName = recipientMember.Username;

				//Get Member info for person leaving voicemail
				oMailMessage.FromMemberID = fromMemberID;
				Matchnet.Member.ServiceAdapters.Member senderMember = MemberSA.Instance.GetMember(
					domainID
					, siteID
					, languageID
					, oMailMessage.FromMemberID
					, MemberLoadFlags.None);
				oMailMessage.FromUserName = senderMember.Username;

				int repliedMemberMessageID = 0; //TODO: What is this?  Need real Mail Message ID.
					
				oMailMessage.MemberMailID = repliedMemberMessageID; 
				oMailMessage.Subject = messageSubject;
				oMailMessage.Content = messageBody;
				oMailMessage.MailTypeID = Message.MAIL_TYPE.EMAIL;
				oMailMessage.MemberFolderID = (int)Folder.SYSTEM_TABLES.SENT; // Need real Member Folder ID.
				oMailMessage.LanguageID = languageID;
				#endregion

				bool bResponse = Message.getInstance().sendMessage(oMailMessage, domainID, repliedMemberMessageID, false, false);
				if (! bResponse)
				{
					throw(new Exception("Email notification did not send."));
				}
				return bResponse;
			} 
			catch (Exception x) 
			{
				throw x;
			}
		}

		public bool SaveToHotList(int domainID, int toMemberID, int fromMemberID, HotListCategory category)
		{
			ListSaveResult hotListResult = null;
			try
			{
				// save the to member in the current member's hotlist prior to sending message
				hotListResult = ListSA.Instance.AddListMember(
					category,
					domainID,
					fromMemberID,
					toMemberID,
					null,
					Matchnet.Constants.NULL_INT,
					false,
					true,
					false);

				if(hotListResult != null && hotListResult.ReturnValue == 0)
				{
					return true;
				}
				else
				{
					throw(new Exception("Hot List failed to update."));
				}
			}
			catch(Exception x)
			{
				throw x;
			}
		}

		public bool SaveToHotList(int domainID, int toMemberID, int fromMemberID, HotListType type, HotListCategory category)
		{
			ListSaveResult hotListResult = null;
			try
			{
				// save the to member in the current member's hotlist prior to sending message
				hotListResult = ListSA.Instance.AddListMember(
					category,
					domainID,
					fromMemberID,
					toMemberID,
					null,
					Matchnet.Constants.NULL_INT,
					false,
					true, 
					false);

				if(hotListResult != null && hotListResult.ReturnValue == 0)
				{
					return true;
				}
				else
				{
					throw(new Exception("Hot List failed to update."));
				}
			}
			catch(Exception x)
			{
				throw x;
			}
		}

		#endregion
	}
}
