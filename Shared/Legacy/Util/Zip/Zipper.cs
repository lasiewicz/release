using System;
using System.IO;

using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;

using Matchnet.Util.File;

namespace Matchnet.Util.Zip
{
	public class Zipper
	{
		public static void Zip(string archiveName, string directory, bool recursive, string filter) 
		{
			try 
			{
				ZipOutputStream outputStream = new ZipOutputStream(System.IO.File.Create(archiveName));
				outputStream.SetMethod(ZipOutputStream.STORED);

				string[] excluded = filter.Split(',');

				ZipFileCallback callback = new ZipFileCallback(outputStream, directory, excluded);
				FileWalker.Walk(directory, recursive, callback);

				outputStream.Finish();
				outputStream.Close();
			} 
			catch (Exception ex) 
			{
				throw ex;
			}
		}

		public static void UnZip(string archivePath, string extractDirectory) 
		{
			try 
			{ 
				ZipInputStream inputStream = new ZipInputStream(System.IO.File.OpenRead(archivePath));
				ZipEntry entry;

				while ((entry = inputStream.GetNextEntry()) != null) 
				{
					string directory = Path.GetDirectoryName(entry.Name);
					string fileName = Path.GetFileName(entry.Name);
					DirectoryInfo dir = Directory.CreateDirectory(Path.Combine(extractDirectory, directory));
					
					if (fileName != null && fileName.Length > 0) 
					{
						FileInfo fileInfo = new FileInfo(Path.Combine(dir.FullName, fileName));
						FileStream outputStream = fileInfo.Create();
						int size = 2048;
						byte[] data = new byte[2048];

						while (true) 
						{
							size = inputStream.Read(data, 0, data.Length);
							if (size > 0) 
							{
								outputStream.Write(data, 0, size);
							} 
							else 
							{
								break;
							}
						}

						outputStream.Close();
						fileInfo.LastWriteTime = entry.DateTime;
					}
				}
				inputStream.Close();
			} 
			catch (Exception ex) 
			{
				throw ex;
			}
		}
	}
}
