using System;
using System.Collections;
using System.IO;

using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Checksums;

using Matchnet.Util.File;

namespace Matchnet.Util.Zip
{
	public class ZipFileCallback : FileCallback
	{
		private ZipOutputStream mZipStream;
		private string mBasePath;
		private Hashtable mExclusionList;
		Crc32 mCrc = new Crc32();

		public ZipFileCallback(ZipOutputStream zipStream, string basePath, string[] excludedExtensions)
		{
			mZipStream = zipStream;
			mBasePath = basePath;
			mExclusionList = new Hashtable();
			if (excludedExtensions != null && excludedExtensions.Length > 0) 
			{
				for (int x = 0; x < excludedExtensions.Length; x++) 
				{
					mExclusionList.Add(excludedExtensions[x], "");
				}
			}
		}

		public void Callback(string path)
		{
			if (!mExclusionList.ContainsKey(Path.GetExtension(path)))
			{
				FileStream fileStream = System.IO.File.OpenRead(path);
				byte[] buffer = new byte[fileStream.Length];
				fileStream.Read(buffer, 0, buffer.Length);
				fileStream.Close();
			
				string entryName = path.Substring(mBasePath.Length + 1);
				ZipEntry entry = new ZipEntry(entryName);
				entry.DateTime = System.IO.File.GetLastWriteTime(path);
				entry.Size = buffer.Length;
				mCrc.Reset();
				mCrc.Update(buffer);
				entry.Crc  = mCrc.Value;
                    
				mZipStream.PutNextEntry(entry);
				mZipStream.Write(buffer, 0, buffer.Length);
			}
		}
	}
}
