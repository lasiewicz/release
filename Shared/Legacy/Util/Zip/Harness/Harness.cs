using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;

using Matchnet.Util.Zip;

namespace Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Harness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox grpHarness;
		private System.Windows.Forms.FolderBrowserDialog fldBrowser;
		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.GroupBox grpZip;
		private System.Windows.Forms.Button btnZip;
		private System.Windows.Forms.Button btnTarget;
		private System.Windows.Forms.Button btnSource;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtFilter;
		private System.Windows.Forms.Label lblFilter;
		private System.Windows.Forms.CheckBox chkRecursive;
		private System.Windows.Forms.TextBox txtExtractDirectory;
		private System.Windows.Forms.Label lblExtractDirectory;
		private System.Windows.Forms.TextBox txtSourceDirectory;
		private System.Windows.Forms.Label lblSourceDirectory;
		private System.Windows.Forms.GroupBox grpUnzip;
		private System.Windows.Forms.Button btnUnzip;
		private System.Windows.Forms.Label lblTarget;
		private System.Windows.Forms.Label lblArchive;
		private System.Windows.Forms.Button btnExtract;
		private System.Windows.Forms.Button btnArchive;
		private System.Windows.Forms.TextBox txtExtract;
		private System.Windows.Forms.TextBox txtArchive;
		private System.Windows.Forms.OpenFileDialog flDialog;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Harness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grpHarness = new System.Windows.Forms.GroupBox();
			this.grpUnzip = new System.Windows.Forms.GroupBox();
			this.btnUnzip = new System.Windows.Forms.Button();
			this.btnExtract = new System.Windows.Forms.Button();
			this.btnArchive = new System.Windows.Forms.Button();
			this.txtExtract = new System.Windows.Forms.TextBox();
			this.lblTarget = new System.Windows.Forms.Label();
			this.txtArchive = new System.Windows.Forms.TextBox();
			this.lblArchive = new System.Windows.Forms.Label();
			this.grpZip = new System.Windows.Forms.GroupBox();
			this.btnZip = new System.Windows.Forms.Button();
			this.btnTarget = new System.Windows.Forms.Button();
			this.btnSource = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.txtFilter = new System.Windows.Forms.TextBox();
			this.lblFilter = new System.Windows.Forms.Label();
			this.chkRecursive = new System.Windows.Forms.CheckBox();
			this.txtExtractDirectory = new System.Windows.Forms.TextBox();
			this.lblExtractDirectory = new System.Windows.Forms.Label();
			this.txtSourceDirectory = new System.Windows.Forms.TextBox();
			this.lblSourceDirectory = new System.Windows.Forms.Label();
			this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.fldBrowser = new System.Windows.Forms.FolderBrowserDialog();
			this.flDialog = new System.Windows.Forms.OpenFileDialog();
			this.grpHarness.SuspendLayout();
			this.grpUnzip.SuspendLayout();
			this.grpZip.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpHarness
			// 
			this.grpHarness.Controls.Add(this.grpUnzip);
			this.grpHarness.Controls.Add(this.grpZip);
			this.grpHarness.Controls.Add(this.txtOutput);
			this.grpHarness.Location = new System.Drawing.Point(8, 8);
			this.grpHarness.Name = "grpHarness";
			this.grpHarness.Size = new System.Drawing.Size(728, 456);
			this.grpHarness.TabIndex = 0;
			this.grpHarness.TabStop = false;
			// 
			// grpUnzip
			// 
			this.grpUnzip.Controls.Add(this.btnUnzip);
			this.grpUnzip.Controls.Add(this.btnExtract);
			this.grpUnzip.Controls.Add(this.btnArchive);
			this.grpUnzip.Controls.Add(this.txtExtract);
			this.grpUnzip.Controls.Add(this.lblTarget);
			this.grpUnzip.Controls.Add(this.txtArchive);
			this.grpUnzip.Controls.Add(this.lblArchive);
			this.grpUnzip.Location = new System.Drawing.Point(368, 16);
			this.grpUnzip.Name = "grpUnzip";
			this.grpUnzip.Size = new System.Drawing.Size(352, 144);
			this.grpUnzip.TabIndex = 13;
			this.grpUnzip.TabStop = false;
			this.grpUnzip.Text = "UnZip";
			// 
			// btnUnzip
			// 
			this.btnUnzip.Location = new System.Drawing.Point(104, 64);
			this.btnUnzip.Name = "btnUnzip";
			this.btnUnzip.Size = new System.Drawing.Size(64, 24);
			this.btnUnzip.TabIndex = 21;
			this.btnUnzip.Text = "UnZip";
			this.btnUnzip.Click += new System.EventHandler(this.btnUnzip_Click);
			// 
			// btnExtract
			// 
			this.btnExtract.Location = new System.Drawing.Point(304, 40);
			this.btnExtract.Name = "btnExtract";
			this.btnExtract.Size = new System.Drawing.Size(40, 20);
			this.btnExtract.TabIndex = 20;
			this.btnExtract.Text = "...";
			this.btnExtract.Click += new System.EventHandler(this.btnExtract_Click);
			// 
			// btnArchive
			// 
			this.btnArchive.Location = new System.Drawing.Point(304, 16);
			this.btnArchive.Name = "btnArchive";
			this.btnArchive.Size = new System.Drawing.Size(40, 20);
			this.btnArchive.TabIndex = 19;
			this.btnArchive.Text = "...";
			this.btnArchive.Click += new System.EventHandler(this.btnArchive_Click);
			// 
			// txtExtract
			// 
			this.txtExtract.Location = new System.Drawing.Point(104, 40);
			this.txtExtract.Name = "txtExtract";
			this.txtExtract.Size = new System.Drawing.Size(192, 20);
			this.txtExtract.TabIndex = 14;
			this.txtExtract.Text = "";
			// 
			// lblTarget
			// 
			this.lblTarget.Location = new System.Drawing.Point(8, 40);
			this.lblTarget.Name = "lblTarget";
			this.lblTarget.Size = new System.Drawing.Size(96, 16);
			this.lblTarget.TabIndex = 13;
			this.lblTarget.Text = "Extract Directory";
			this.lblTarget.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtArchive
			// 
			this.txtArchive.Location = new System.Drawing.Point(104, 16);
			this.txtArchive.Name = "txtArchive";
			this.txtArchive.Size = new System.Drawing.Size(192, 20);
			this.txtArchive.TabIndex = 12;
			this.txtArchive.Text = "";
			// 
			// lblArchive
			// 
			this.lblArchive.Location = new System.Drawing.Point(8, 16);
			this.lblArchive.Name = "lblArchive";
			this.lblArchive.Size = new System.Drawing.Size(96, 16);
			this.lblArchive.TabIndex = 11;
			this.lblArchive.Text = "Archive";
			this.lblArchive.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// grpZip
			// 
			this.grpZip.Controls.Add(this.btnZip);
			this.grpZip.Controls.Add(this.btnTarget);
			this.grpZip.Controls.Add(this.btnSource);
			this.grpZip.Controls.Add(this.label1);
			this.grpZip.Controls.Add(this.txtFilter);
			this.grpZip.Controls.Add(this.lblFilter);
			this.grpZip.Controls.Add(this.chkRecursive);
			this.grpZip.Controls.Add(this.txtExtractDirectory);
			this.grpZip.Controls.Add(this.lblExtractDirectory);
			this.grpZip.Controls.Add(this.txtSourceDirectory);
			this.grpZip.Controls.Add(this.lblSourceDirectory);
			this.grpZip.Location = new System.Drawing.Point(8, 16);
			this.grpZip.Name = "grpZip";
			this.grpZip.Size = new System.Drawing.Size(352, 144);
			this.grpZip.TabIndex = 12;
			this.grpZip.TabStop = false;
			this.grpZip.Text = "Zip";
			// 
			// btnZip
			// 
			this.btnZip.Location = new System.Drawing.Point(104, 112);
			this.btnZip.Name = "btnZip";
			this.btnZip.Size = new System.Drawing.Size(64, 24);
			this.btnZip.TabIndex = 21;
			this.btnZip.Text = "Zip";
			this.btnZip.Click += new System.EventHandler(this.btnZip_Click);
			// 
			// btnTarget
			// 
			this.btnTarget.Location = new System.Drawing.Point(304, 40);
			this.btnTarget.Name = "btnTarget";
			this.btnTarget.Size = new System.Drawing.Size(40, 20);
			this.btnTarget.TabIndex = 20;
			this.btnTarget.Text = "...";
			this.btnTarget.Click += new System.EventHandler(this.btnTarget_Click);
			// 
			// btnSource
			// 
			this.btnSource.Location = new System.Drawing.Point(304, 16);
			this.btnSource.Name = "btnSource";
			this.btnSource.Size = new System.Drawing.Size(40, 20);
			this.btnSource.TabIndex = 19;
			this.btnSource.Text = "...";
			this.btnSource.Click += new System.EventHandler(this.btnSource_Click);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(240, 64);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(104, 16);
			this.label1.TabIndex = 18;
			this.label1.Text = "(.exe,.scc,.csproj)";
			this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// txtFilter
			// 
			this.txtFilter.Location = new System.Drawing.Point(104, 64);
			this.txtFilter.Name = "txtFilter";
			this.txtFilter.Size = new System.Drawing.Size(128, 20);
			this.txtFilter.TabIndex = 17;
			this.txtFilter.Text = "";
			// 
			// lblFilter
			// 
			this.lblFilter.Location = new System.Drawing.Point(8, 64);
			this.lblFilter.Name = "lblFilter";
			this.lblFilter.Size = new System.Drawing.Size(96, 16);
			this.lblFilter.TabIndex = 16;
			this.lblFilter.Text = "Filter";
			this.lblFilter.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// chkRecursive
			// 
			this.chkRecursive.Location = new System.Drawing.Point(104, 88);
			this.chkRecursive.Name = "chkRecursive";
			this.chkRecursive.Size = new System.Drawing.Size(160, 16);
			this.chkRecursive.TabIndex = 15;
			this.chkRecursive.Text = "Recursive";
			// 
			// txtExtractDirectory
			// 
			this.txtExtractDirectory.Location = new System.Drawing.Point(104, 40);
			this.txtExtractDirectory.Name = "txtExtractDirectory";
			this.txtExtractDirectory.Size = new System.Drawing.Size(192, 20);
			this.txtExtractDirectory.TabIndex = 14;
			this.txtExtractDirectory.Text = "";
			// 
			// lblExtractDirectory
			// 
			this.lblExtractDirectory.Location = new System.Drawing.Point(8, 40);
			this.lblExtractDirectory.Name = "lblExtractDirectory";
			this.lblExtractDirectory.Size = new System.Drawing.Size(96, 16);
			this.lblExtractDirectory.TabIndex = 13;
			this.lblExtractDirectory.Text = "Create Directory";
			this.lblExtractDirectory.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtSourceDirectory
			// 
			this.txtSourceDirectory.Location = new System.Drawing.Point(104, 16);
			this.txtSourceDirectory.Name = "txtSourceDirectory";
			this.txtSourceDirectory.Size = new System.Drawing.Size(192, 20);
			this.txtSourceDirectory.TabIndex = 12;
			this.txtSourceDirectory.Text = "";
			// 
			// lblSourceDirectory
			// 
			this.lblSourceDirectory.Location = new System.Drawing.Point(8, 16);
			this.lblSourceDirectory.Name = "lblSourceDirectory";
			this.lblSourceDirectory.Size = new System.Drawing.Size(96, 16);
			this.lblSourceDirectory.TabIndex = 11;
			this.lblSourceDirectory.Text = "Source Directory";
			this.lblSourceDirectory.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtOutput
			// 
			this.txtOutput.Location = new System.Drawing.Point(8, 168);
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(712, 280);
			this.txtOutput.TabIndex = 11;
			this.txtOutput.Text = "";
			// 
			// Harness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(744, 469);
			this.Controls.Add(this.grpHarness);
			this.Name = "Harness";
			this.Text = "Zip Harness";
			this.grpHarness.ResumeLayout(false);
			this.grpUnzip.ResumeLayout(false);
			this.grpZip.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Harness());
		}

		private void btnSource_Click(object sender, System.EventArgs e)
		{
			fldBrowser.ShowDialog();
			txtSourceDirectory.Text = fldBrowser.SelectedPath;
		}

		private void btnTarget_Click(object sender, System.EventArgs e)
		{
			fldBrowser.ShowDialog();
			txtExtractDirectory.Text = fldBrowser.SelectedPath;
		}

		private void btnZip_Click(object sender, System.EventArgs e)
		{
			try 
			{
				string archiveName = txtExtractDirectory.Text + Path.DirectorySeparatorChar + Guid.NewGuid() + ".zip";
				Zipper.Zip(archiveName, txtSourceDirectory.Text, chkRecursive.Checked, txtFilter.Text);
				flDialog.FileName = archiveName;
				AppendOutput("Created " + archiveName);
			} 
			catch (Exception ex) 
			{
				AppendError(ex);
			}
		}

		private void AppendOutput(string message) 
		{
			txtOutput.AppendText(message + "\r\n");
		}

		private void AppendError(Exception ex) 
		{
			txtOutput.Text = "";
			AppendOutput(ex.ToString());
		}

		private void btnExtract_Click(object sender, System.EventArgs e)
		{
			fldBrowser.ShowDialog();
			txtExtract.Text = fldBrowser.SelectedPath;
		}

		private void btnArchive_Click(object sender, System.EventArgs e)
		{
			flDialog.Multiselect = false;
			flDialog.ShowDialog();
			txtArchive.Text = flDialog.FileName;
		}

		private void btnUnzip_Click(object sender, System.EventArgs e)
		{
			try 
			{
				Zipper.UnZip(txtArchive.Text, txtExtract.Text);
				AppendOutput("Unzipped successfully");
			} 
			catch (Exception ex) 
			{
				AppendError(ex);
			}
		}
	}
}
