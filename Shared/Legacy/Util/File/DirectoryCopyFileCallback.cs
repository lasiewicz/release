using System;
using System.IO;

namespace Matchnet.Util.File
{
	public class DirectoryCopyFileCallback : FileCallback
	{
		public event FileCopyEventHandler FileCopy;

		private string mSourceDirectory;
		private string mDestinationDirectory;
		private bool mOverwrite;

		public DirectoryCopyFileCallback(string source, string destination, bool overwrite)
		{
			if (!source.EndsWith("\\")) 
			{
				source = source + "\\";
			}

			if (!destination.EndsWith("\\")) 
			{
				destination = destination + "\\";
			}

			mSourceDirectory = source;
			mDestinationDirectory = destination;
			mOverwrite = overwrite;
		}

		public void Callback(string path)
		{
			string fileName = mDestinationDirectory + path.Substring(mSourceDirectory.Length);
			string destinationDir = Path.GetDirectoryName(fileName);

			if (!Directory.Exists(destinationDir))
			{
				Directory.CreateDirectory(destinationDir);
			}

			System.IO.File.Copy(path, fileName, mOverwrite);
			OnFileCopy(new FileCopyEventArgs(path, fileName));
		}

		protected virtual void OnFileCopy(FileCopyEventArgs args) 
		{
			if (FileCopy != null) 
			{
				FileCopy(this, args);
			}
		}
	}
}
