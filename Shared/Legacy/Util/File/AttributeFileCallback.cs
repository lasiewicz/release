using System;

namespace Matchnet.Util.File
{
	public class AttributeFileCallback : FileCallback
	{
		private System.IO.FileAttributes mAttributes;

		public AttributeFileCallback(System.IO.FileAttributes attributes)
		{
			mAttributes = attributes;
		}

		public void Callback(string path)
		{
			System.IO.File.SetAttributes(path, mAttributes);
		}
	}
}
