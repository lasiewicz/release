using System;

namespace Matchnet.Util.File
{
	public class FileCopyEventArgs : EventArgs
	{
		private readonly string mSourceFile;
		private readonly string mDestinationFile;

		public FileCopyEventArgs(string source, string destination) 
		{
			mSourceFile = source;
			mDestinationFile = destination;
		}

		public string SourceFile 
		{
			get 
			{
				return mSourceFile;
			}
		}

		public string DestinationFile 
		{
			get 
			{	
				return mDestinationFile;
			}
		}
	}
}
