using System;

namespace Matchnet.Util.File
{
	public class FileDoesntExistEventArgs : System.EventArgs 
	{
		private readonly string mMasterPath;
		private readonly string mTargetPath;

		public string MasterPath 
		{
			get 
			{
				return mMasterPath;
			}
		}

		public string TargetPath 
		{
			get 
			{
				return mTargetPath;
			}
		}

		public FileDoesntExistEventArgs(string masterPath, string targetPath)
		{
			mMasterPath = masterPath;
			mTargetPath = targetPath;
		}
	}
}
