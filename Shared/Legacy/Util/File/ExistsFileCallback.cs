using System;
using System.IO;

namespace Matchnet.Util.File
{
	public class ExistsFileCallback : FileCallback
	{
		public event FileDoesntExistEventHandler FileDoesntExist;

		private string mMasterRoot;
		private string mTargetRoot;

		public ExistsFileCallback(string masterRoot, string targetRoot)
		{
			if (!masterRoot.EndsWith("\\")) 
			{
				masterRoot = masterRoot + "\\";
			}

			if (!targetRoot.EndsWith("\\")) 
			{
				targetRoot = targetRoot + "\\";
			}

			mMasterRoot = masterRoot;
			mTargetRoot = targetRoot;
		}

		public void Callback(string path)
		{
			string relativeFilePath = path.Substring(mTargetRoot.Length);
			string existsFilePath = Path.Combine(mMasterRoot, relativeFilePath);

			if (!System.IO.File.Exists(existsFilePath)) 
			{
				OnFileDoesntExist(new FileDoesntExistEventArgs(existsFilePath, path));
			}
		}

		protected virtual void OnFileDoesntExist(FileDoesntExistEventArgs args) 
		{
			if (FileDoesntExist != null) 
			{
				FileDoesntExist(this, args);
			}
		}	
	}
}
