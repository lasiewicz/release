using System;

namespace Matchnet.Util.File
{
	public class AttributeSetter
	{
		public static void SetAttributes(string rootPath, System.IO.FileAttributes attributes, bool recursive) 
		{
			FileWalker.Walk(rootPath, recursive, new AttributeFileCallback(attributes));
		}
	}
}
