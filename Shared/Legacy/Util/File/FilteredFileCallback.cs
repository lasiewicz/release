using System;
using System.Collections;
using System.IO;

namespace Matchnet.Util.File
{
	public abstract class FilteredFileCallback : FileCallback
	{
		private string mFilter;
		private bool mFilterIsExclusion;
		private Hashtable mExtensions;

		public string Filter 
		{
			get 
			{
				return mFilter == null ? string.Empty : mFilter;
			}
			set 
			{
				InitializeLookup(value);
				mFilter = value;
			}
		}

		private void InitializeLookup(string filter) 
		{
			mExtensions = new Hashtable();

			if (filter != null && filter != string.Empty) 
			{
				string[] extensions = filter.Split(',');
				for (int x = 0; x < extensions.Length; x++) 
				{
					mExtensions.Add(extensions[x], "");
				}
			}
		}

		public bool FilterIsExclusion 
		{
			get 
			{
				return mFilterIsExclusion;
			}
			set 
			{
				mFilterIsExclusion = value;
			}
		}
		
		public virtual void Callback(string path)
		{
			if (mExtensions != null && mExtensions.Count > 0) 
			{
				string extension = Path.GetExtension(path);

				if (mExtensions.ContainsKey(extension)) 
				{
					if (!mFilterIsExclusion) 
					{
						FilteredCallback(path);
					}
				} 
				else 
				{
					if (mFilterIsExclusion) 
					{
						FilteredCallback(path);
					}
				}
			} 
			else 
			{
				FilteredCallback(path);				
			}
		}

		public abstract void FilteredCallback(string path);
	}
}
