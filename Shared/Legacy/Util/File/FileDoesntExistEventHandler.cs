using System;

namespace Matchnet.Util.File
{
	public delegate void FileDoesntExistEventHandler(object sender, FileDoesntExistEventArgs args);
}
