using System;
using System.IO;

namespace Matchnet.Util.File
{
	public interface FileCallback
	{
		void Callback(string path);
	}
}
