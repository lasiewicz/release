using System;
using System.IO;

namespace Matchnet.Util.File
{
	/// <summary>
	/// Walks a given directory and calls the given callback class for each 
	/// file that is found.
	/// </summary>
	public class FileWalker
	{
		/// <summary>
		/// Walks a given directory and calls the given callback for each 
		/// file that is found.
		/// </summary>
		/// <param name="directory">The directory to start in</param>
		/// <param name="recursive">Whether or not to recurse into sub-directories</param>
		/// <param name="callback">The FileCallback implementation to call</param>
		public static void Walk(string directory, bool recursive, FileCallback callback) 
		{
			if (!Directory.Exists(directory)) 
			{
				throw new FileNotFoundException("Directory does not exist", directory);
			}
			string[] files = Directory.GetFiles(directory);
			
			if (files != null && files.Length > 0) 
			{
				for (int x = 0; x < files.Length; x++) 
				{
					callback.Callback(files[x]);
				}
			}

			if (recursive) 
			{
				string[] directories = Directory.GetDirectories(directory);
				
				if (directories != null && directories.Length > 0) 
				{
					for (int x = 0; x < directories.Length; x++) 
					{
						Walk(directories[x], recursive, callback);
					}
				}
			}
		}
	}
}
