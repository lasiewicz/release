using System;

namespace Matchnet.Util.File
{
	public class DirectoryCopy
	{
		public static void Copy(string source, string destination, bool recursive, bool overwrite) 
		{
			FileWalker.Walk(source, recursive, new DirectoryCopyFileCallback(source, destination, overwrite));
		}

		public static void Copy(string source, string destination, bool recursive, bool overwrite, FileCopyEventHandler fileCopyEventHandler) 
		{
			DirectoryCopyFileCallback callback = new DirectoryCopyFileCallback(source, destination, overwrite);
			callback.FileCopy += fileCopyEventHandler;
			FileWalker.Walk(source, recursive, callback);
		}
	}
}
