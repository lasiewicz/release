using System;

namespace Matchnet.Util.File
{
	public class FileCountCallback : FilteredFileCallback
	{
		private int mCount;

		public int Count 
		{
			get 
			{
				return mCount;
			}
		}

		public override void FilteredCallback(string path)
		{
			mCount++;
		}
	}
}
