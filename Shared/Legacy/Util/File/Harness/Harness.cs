using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet.Util.File;

namespace Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Harness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.FolderBrowserDialog fldBrowser;
		private System.Windows.Forms.TabControl tcHarness;
		private System.Windows.Forms.TabPage tpFileWalker;
		private System.Windows.Forms.TabPage tpSync;
		private System.Windows.Forms.GroupBox grpHarness;
		private System.Windows.Forms.Button btnWalk;
		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.CheckBox chkRecursive;
		private System.Windows.Forms.Button btnLookup;
		private System.Windows.Forms.TextBox txtDirectory;
		private System.Windows.Forms.Label lblDirectory;
		private System.Windows.Forms.GroupBox gpSync;
		private System.Windows.Forms.Label lblMasterPath;
		private System.Windows.Forms.TextBox txtMasterDirectory;
		private System.Windows.Forms.Button btnMasterBrowse;
		private System.Windows.Forms.Button btnTargetBrowse;
		private System.Windows.Forms.Label lblTargetDirectory;
		private System.Windows.Forms.Button btnSyncDiscover;
		private System.Windows.Forms.RichTextBox txtSyncOutput;
		private System.Windows.Forms.TextBox txtTargetDirectory;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Harness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fldBrowser = new System.Windows.Forms.FolderBrowserDialog();
			this.tcHarness = new System.Windows.Forms.TabControl();
			this.tpFileWalker = new System.Windows.Forms.TabPage();
			this.tpSync = new System.Windows.Forms.TabPage();
			this.grpHarness = new System.Windows.Forms.GroupBox();
			this.btnWalk = new System.Windows.Forms.Button();
			this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.chkRecursive = new System.Windows.Forms.CheckBox();
			this.btnLookup = new System.Windows.Forms.Button();
			this.txtDirectory = new System.Windows.Forms.TextBox();
			this.lblDirectory = new System.Windows.Forms.Label();
			this.gpSync = new System.Windows.Forms.GroupBox();
			this.lblMasterPath = new System.Windows.Forms.Label();
			this.txtMasterDirectory = new System.Windows.Forms.TextBox();
			this.btnMasterBrowse = new System.Windows.Forms.Button();
			this.btnTargetBrowse = new System.Windows.Forms.Button();
			this.txtTargetDirectory = new System.Windows.Forms.TextBox();
			this.lblTargetDirectory = new System.Windows.Forms.Label();
			this.btnSyncDiscover = new System.Windows.Forms.Button();
			this.txtSyncOutput = new System.Windows.Forms.RichTextBox();
			this.tcHarness.SuspendLayout();
			this.tpFileWalker.SuspendLayout();
			this.tpSync.SuspendLayout();
			this.grpHarness.SuspendLayout();
			this.gpSync.SuspendLayout();
			this.SuspendLayout();
			// 
			// tcHarness
			// 
			this.tcHarness.Controls.Add(this.tpFileWalker);
			this.tcHarness.Controls.Add(this.tpSync);
			this.tcHarness.Location = new System.Drawing.Point(8, 8);
			this.tcHarness.Name = "tcHarness";
			this.tcHarness.SelectedIndex = 0;
			this.tcHarness.Size = new System.Drawing.Size(872, 728);
			this.tcHarness.TabIndex = 1;
			// 
			// tpFileWalker
			// 
			this.tpFileWalker.Controls.Add(this.grpHarness);
			this.tpFileWalker.Location = new System.Drawing.Point(4, 22);
			this.tpFileWalker.Name = "tpFileWalker";
			this.tpFileWalker.Size = new System.Drawing.Size(864, 702);
			this.tpFileWalker.TabIndex = 0;
			this.tpFileWalker.Text = "FileWalker";
			// 
			// tpSync
			// 
			this.tpSync.Controls.Add(this.gpSync);
			this.tpSync.Location = new System.Drawing.Point(4, 22);
			this.tpSync.Name = "tpSync";
			this.tpSync.Size = new System.Drawing.Size(864, 702);
			this.tpSync.TabIndex = 1;
			this.tpSync.Text = "Sync";
			// 
			// grpHarness
			// 
			this.grpHarness.Controls.Add(this.btnWalk);
			this.grpHarness.Controls.Add(this.txtOutput);
			this.grpHarness.Controls.Add(this.chkRecursive);
			this.grpHarness.Controls.Add(this.btnLookup);
			this.grpHarness.Controls.Add(this.txtDirectory);
			this.grpHarness.Controls.Add(this.lblDirectory);
			this.grpHarness.Location = new System.Drawing.Point(8, 8);
			this.grpHarness.Name = "grpHarness";
			this.grpHarness.Size = new System.Drawing.Size(664, 464);
			this.grpHarness.TabIndex = 1;
			this.grpHarness.TabStop = false;
			// 
			// btnWalk
			// 
			this.btnWalk.Location = new System.Drawing.Point(64, 64);
			this.btnWalk.Name = "btnWalk";
			this.btnWalk.Size = new System.Drawing.Size(80, 20);
			this.btnWalk.TabIndex = 5;
			this.btnWalk.Text = "Walk";
			// 
			// txtOutput
			// 
			this.txtOutput.Location = new System.Drawing.Point(8, 88);
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(648, 368);
			this.txtOutput.TabIndex = 4;
			this.txtOutput.Text = "";
			// 
			// chkRecursive
			// 
			this.chkRecursive.Location = new System.Drawing.Point(64, 40);
			this.chkRecursive.Name = "chkRecursive";
			this.chkRecursive.Size = new System.Drawing.Size(120, 16);
			this.chkRecursive.TabIndex = 3;
			this.chkRecursive.Text = "Recursive";
			// 
			// btnLookup
			// 
			this.btnLookup.Location = new System.Drawing.Point(384, 16);
			this.btnLookup.Name = "btnLookup";
			this.btnLookup.Size = new System.Drawing.Size(48, 20);
			this.btnLookup.TabIndex = 2;
			this.btnLookup.Text = "...";
			// 
			// txtDirectory
			// 
			this.txtDirectory.Location = new System.Drawing.Point(64, 16);
			this.txtDirectory.Name = "txtDirectory";
			this.txtDirectory.Size = new System.Drawing.Size(312, 20);
			this.txtDirectory.TabIndex = 1;
			this.txtDirectory.Text = "";
			// 
			// lblDirectory
			// 
			this.lblDirectory.Location = new System.Drawing.Point(8, 16);
			this.lblDirectory.Name = "lblDirectory";
			this.lblDirectory.Size = new System.Drawing.Size(56, 16);
			this.lblDirectory.TabIndex = 0;
			this.lblDirectory.Text = "Directory";
			this.lblDirectory.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// gpSync
			// 
			this.gpSync.Controls.Add(this.txtSyncOutput);
			this.gpSync.Controls.Add(this.btnSyncDiscover);
			this.gpSync.Controls.Add(this.btnTargetBrowse);
			this.gpSync.Controls.Add(this.txtTargetDirectory);
			this.gpSync.Controls.Add(this.lblTargetDirectory);
			this.gpSync.Controls.Add(this.btnMasterBrowse);
			this.gpSync.Controls.Add(this.txtMasterDirectory);
			this.gpSync.Controls.Add(this.lblMasterPath);
			this.gpSync.Location = new System.Drawing.Point(8, 8);
			this.gpSync.Name = "gpSync";
			this.gpSync.Size = new System.Drawing.Size(848, 688);
			this.gpSync.TabIndex = 0;
			this.gpSync.TabStop = false;
			// 
			// lblMasterPath
			// 
			this.lblMasterPath.Location = new System.Drawing.Point(8, 16);
			this.lblMasterPath.Name = "lblMasterPath";
			this.lblMasterPath.Size = new System.Drawing.Size(96, 16);
			this.lblMasterPath.TabIndex = 0;
			this.lblMasterPath.Text = "Master Directory";
			this.lblMasterPath.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtMasterDirectory
			// 
			this.txtMasterDirectory.Location = new System.Drawing.Point(104, 16);
			this.txtMasterDirectory.Name = "txtMasterDirectory";
			this.txtMasterDirectory.Size = new System.Drawing.Size(400, 20);
			this.txtMasterDirectory.TabIndex = 1;
			this.txtMasterDirectory.Text = "";
			// 
			// btnMasterBrowse
			// 
			this.btnMasterBrowse.Location = new System.Drawing.Point(512, 16);
			this.btnMasterBrowse.Name = "btnMasterBrowse";
			this.btnMasterBrowse.Size = new System.Drawing.Size(56, 20);
			this.btnMasterBrowse.TabIndex = 2;
			this.btnMasterBrowse.Text = "...";
			this.btnMasterBrowse.Click += new System.EventHandler(this.btnMasterBrowse_Click);
			// 
			// btnTargetBrowse
			// 
			this.btnTargetBrowse.Location = new System.Drawing.Point(512, 40);
			this.btnTargetBrowse.Name = "btnTargetBrowse";
			this.btnTargetBrowse.Size = new System.Drawing.Size(56, 20);
			this.btnTargetBrowse.TabIndex = 5;
			this.btnTargetBrowse.Text = "...";
			this.btnTargetBrowse.Click += new System.EventHandler(this.btnTargetBrowse_Click);
			// 
			// txtTargetDirectory
			// 
			this.txtTargetDirectory.Location = new System.Drawing.Point(104, 40);
			this.txtTargetDirectory.Name = "txtTargetDirectory";
			this.txtTargetDirectory.Size = new System.Drawing.Size(400, 20);
			this.txtTargetDirectory.TabIndex = 4;
			this.txtTargetDirectory.Text = "";
			// 
			// lblTargetDirectory
			// 
			this.lblTargetDirectory.Location = new System.Drawing.Point(8, 40);
			this.lblTargetDirectory.Name = "lblTargetDirectory";
			this.lblTargetDirectory.Size = new System.Drawing.Size(96, 16);
			this.lblTargetDirectory.TabIndex = 3;
			this.lblTargetDirectory.Text = "Target Directory";
			this.lblTargetDirectory.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// btnSyncDiscover
			// 
			this.btnSyncDiscover.Location = new System.Drawing.Point(104, 64);
			this.btnSyncDiscover.Name = "btnSyncDiscover";
			this.btnSyncDiscover.Size = new System.Drawing.Size(80, 20);
			this.btnSyncDiscover.TabIndex = 6;
			this.btnSyncDiscover.Text = "Discover";
			this.btnSyncDiscover.Click += new System.EventHandler(this.btnSyncDiscover_Click);
			// 
			// txtSyncOutput
			// 
			this.txtSyncOutput.Location = new System.Drawing.Point(16, 88);
			this.txtSyncOutput.Name = "txtSyncOutput";
			this.txtSyncOutput.Size = new System.Drawing.Size(824, 592);
			this.txtSyncOutput.TabIndex = 7;
			this.txtSyncOutput.Text = "";
			// 
			// Harness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(888, 741);
			this.Controls.Add(this.tcHarness);
			this.Name = "Harness";
			this.Text = "File Harness";
			this.tcHarness.ResumeLayout(false);
			this.tpFileWalker.ResumeLayout(false);
			this.tpSync.ResumeLayout(false);
			this.grpHarness.ResumeLayout(false);
			this.gpSync.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Harness());
		}

		private void btnLookup_Click(object sender, System.EventArgs e)
		{
			fldBrowser.Description = "Pick a directory";
			fldBrowser.ShowDialog();
			txtDirectory.Text = fldBrowser.SelectedPath;
		}

		public void AppendOutput(string message) 
		{
			txtOutput.AppendText(message + "\r\n");
		}

		private void AppendError(Exception ex) 
		{
			txtOutput.Text = "";
			AppendOutput(ex.ToString());
		}

		private void btnWalk_Click(object sender, System.EventArgs e)
		{
			txtOutput.Text = "";
			try 
			{
				LogFileCallback callback = new LogFileCallback(this);
				FileWalker.Walk(txtDirectory.Text, chkRecursive.Checked, callback);				
			} 
			catch (Exception ex) 
			{
				AppendError(ex);
			}
		}

		private void btnMasterBrowse_Click(object sender, System.EventArgs e)
		{
			fldBrowser.ShowDialog();
			txtMasterDirectory.Text = fldBrowser.SelectedPath;
		}

		private void btnTargetBrowse_Click(object sender, System.EventArgs e)
		{
			fldBrowser.ShowDialog();
			txtTargetDirectory.Text = fldBrowser.SelectedPath;
		}

		private void btnSyncDiscover_Click(object sender, System.EventArgs e)
		{
			txtSyncOutput.Text = "";
			try 
			{
				ExistsFileCallback fileCallback = new ExistsFileCallback(txtMasterDirectory.Text, txtTargetDirectory.Text);
				fileCallback.FileDoesntExist += new FileDoesntExistEventHandler(fileCallback_FileDoesntExist);
				FileWalker.Walk(txtTargetDirectory.Text, true, fileCallback);

				fileCallback = new ExistsFileCallback(txtTargetDirectory.Text, txtMasterDirectory.Text);
				fileCallback.FileDoesntExist += new FileDoesntExistEventHandler(fileCallback_FileDoesntExist);
				FileWalker.Walk(txtMasterDirectory.Text, true, fileCallback);

				MessageBox.Show("Finished Discovering Differences");
			} 
			catch (Exception ex) 
			{
				txtSyncOutput.AppendText(ex.ToString());
			}
		}

		private void fileCallback_FileDoesntExist(object sender, FileDoesntExistEventArgs args)
		{
			txtSyncOutput.AppendText(args.TargetPath + " --> " + args.MasterPath + "\r\n");
		}
	}
}
