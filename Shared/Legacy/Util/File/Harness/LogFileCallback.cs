using System;
using Matchnet.Util.File;

namespace Harness
{
	public class LogFileCallback : FileCallback
	{
		private Harness mHarness;

		public LogFileCallback(Harness harness) 
		{
			mHarness = harness;
		}

		public void Callback(string path) 
		{
			mHarness.AppendOutput(path);
		}
	}
}
