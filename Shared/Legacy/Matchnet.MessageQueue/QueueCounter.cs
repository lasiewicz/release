using System;

namespace Matchnet.MessageQueue
{
	/// <summary>
	/// Object that gathers perfmon stats for a given MSMQ perfmon path.
	/// This object wraps up the knowledge of how to translate an MSMQ
	/// configuration string to a performance monitor object, and knows
	/// what specific stuff we want to get from those objects.
	/// 
	/// Currently the object only collects total messages in queue, but
	/// other stats could be returned (Bytes in Queue and Bytes/Messages
	/// in Journal Queue), and even perhaps summary stats like total
	/// bytes used by queues on a machine.
	/// </summary>
	public class QueueCounter
	{
		string _path;
		string _instance;
		string _machine;
		public QueueCounter( string path )
		{
			_path = path;
			string[] parts = _path.Split(':');
			_instance = parts[parts.Length - 1].ToLower();
			parts = _instance.Split('\\');
			_machine = parts[0];
		}

		/// <summary>
		/// Return the raw counter value.
		/// NOTE: the first time this is called, it will be very slow (1-2 sec) as it internally
		/// creates network connections and enumerates counters. After the first time, it appears
		/// that some information is cached, so subsequent calls are quicker.
		/// This getter will throw various exceptions generated by the PerformanceCounter constructor
		/// if it is unable to locate the category, counter, instance or machine requested.
		/// </summary>
		public long RawValue
		{
			get 
			{
				// If we wanted to check up front if a queue was accessible, returning an error
				// value rather than an exception, this code would do it.
//				System.Diagnostics.PerformanceCounterCategory cat =
//					new System.Diagnostics.PerformanceCounterCategory("MSMQ Queue", _machine);
//				if (! (cat.CounterExists("Messages in Queue") && (cat.InstanceExists(_instance))) ) 
//				{
//					return -1;
//				}
				System.Diagnostics.PerformanceCounter counter =
					new System.Diagnostics.PerformanceCounter("MSMQ Queue", "Messages in Queue", _instance, _machine);
				return counter.RawValue;
			}
		}
	}
}
