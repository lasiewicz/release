using System;
using System.Messaging;

namespace Matchnet.MessageQueue
{
	[Serializable]
	public abstract class QueueItemBase : IDisposable
	{
		private string _QueuePath = "";
		private int _Attempts = 0;
		protected bool _Recoverable = false;
		protected PriorityType _Priority = PriorityType.Medium;

		public QueueItemBase() {}

		//we are doing this mapping so classes that use this class won't have to reference System.Messaging.DLL
		public enum PriorityType : int
		{
			Low = (int)System.Messaging.MessagePriority.Low,
			Medium = (int)System.Messaging.MessagePriority.Normal,
			High = (int)System.Messaging.MessagePriority.High
		}

		protected string QueuePath
		{
			get
			{
				return _QueuePath;
			}
			set
			{
				_QueuePath = value;
			}
		}


		protected bool Recoverable
		{
			get
			{
				return _Recoverable;
			}
			set
			{
				_Recoverable = value;
			}
		}


		protected PriorityType Priority
		{
			get
			{
				return _Priority;
			}
			set
			{
				_Priority = value;
			}
		}


		public void IncrementAttempts()
		{
			_Attempts++;
		}


		public static void Send(QueueItemBase item)
		{
			Send(item, QueueProcessor.MessageFormatterType.XML, null);
		}


		public static void Send(QueueItemBase item, MessageQueueTransaction transaction)
		{
			Send(item, QueueProcessor.MessageFormatterType.XML, transaction);
		}

		
		public static void Send(QueueItemBase item, QueueProcessor.MessageFormatterType messageFormatter)
		{
			Send(item, messageFormatter, null);
		}
	
		
		public static void Send(QueueItemBase item, QueueProcessor.MessageFormatterType messageFormatter, MessageQueueTransaction transaction)
		{
			System.Messaging.MessageQueue queue = new System.Messaging.MessageQueue(item.QueuePath);

			System.Messaging.Message message = new System.Messaging.Message();
			message.Body = item;
			message.Recoverable = item.Recoverable;
			message.Priority = (System.Messaging.MessagePriority)item.Priority;

			if (messageFormatter == QueueProcessor.MessageFormatterType.Binary)
			{
				message.Formatter = new BinaryMessageFormatter();
			}

			if (transaction != null)
			{
				queue.Send(message, item.GetType().ToString(), transaction);
			}
			else
			{
				queue.Send(message, item.GetType().ToString());
			}
			queue.Close();
		}


		public virtual void Execute()
		{
		}


		#region IDisposable Members

		public void Dispose()
		{
			// TODO:  Add QueueItemBase.Dispose implementation
		}

		#endregion
	}
}
