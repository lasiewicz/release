using System;
using System.Diagnostics;
using System.IO;
using System.Messaging;
using System.Threading;
using System.Xml.Serialization;

using Matchnet.Lib.Exceptions;

namespace Matchnet.MessageQueue
{
	public class QueueProcessor
	{
		private const string ERROR_BASE_PATH = @"c:\matchnet\QueueProcessorError\";

		private string _QueuePath;
		private System.Messaging.MessageQueue _Queue;
		private bool _IsRunning;
		private Thread _ThreadReceive;

		public enum MessageFormatterType
		{
			XML,
			Binary
		};

		public QueueProcessor(string queuePath)
		{
			_QueuePath = queuePath;
		}


		public void Start(Type[] messageTypes)
		{
			Start(messageTypes, MessageFormatterType.XML);
		}


		public void Start(Type[] messageTypes, MessageFormatterType messageFormatter)
		{
			if (!Directory.Exists(ERROR_BASE_PATH))
			{
				Directory.CreateDirectory(ERROR_BASE_PATH);
			}

			_Queue = new System.Messaging.MessageQueue(_QueuePath);

			if (messageFormatter == MessageFormatterType.XML)
			{
				_Queue.Formatter = new XmlMessageFormatter(messageTypes);
			}
			else
			{
				_Queue.Formatter = new BinaryMessageFormatter();
			}

			Start(false);
			_IsRunning = true;
		}


		private void Start(bool clearConnectionCache)
		{
			try
			{
				if (clearConnectionCache)
				{
					Thread.Sleep(10000);
					_Queue.Close();
					System.Messaging.MessageQueue.EnableConnectionCache = false;
					System.Messaging.MessageQueue.ClearConnectionCache();
					System.Messaging.MessageQueue.EnableConnectionCache = true;
				}

				StopProcessing();
				_ThreadReceive = new Thread(new ThreadStart(ReceiveCycle));
				_ThreadReceive.Start();
			}
			catch (Exception ex)
			{
				ExceptionLogger.LogToEventLog(new MatchnetException(ex), "QueueProcessor", System.Diagnostics.EventLogEntryType.Error);
				Start(true);
			}
		}


		private void StopProcessing()
		{
			try
			{
				if (_ThreadReceive != null)
				{
					if (_ThreadReceive.IsAlive)
					{
						_ThreadReceive.Abort();
					}
				}
			}
			catch {}
		}


		public void Stop()
		{
			StopProcessing();
			_Queue.Dispose();
			_IsRunning = false;
		}


		private void ReceiveCycle()
		{
			Message message = null;
			QueueItemBase queueItem = null;
			string itemType = "unknown";

			while (true)
			{
				try
				{
					message = _Queue.Receive(MessageQueueTransactionType.Automatic);

					itemType = message.Label;

					//save body to a text file for debugging
					//System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\stream.txt");
					//sw.Write(message.BodyStream);
					//sw.Close();

					queueItem = (QueueItemBase)message.Body;
					queueItem.IncrementAttempts();
					queueItem.Execute();

					queueItem.Dispose();
					message.Dispose();
				}
				catch (Exception ex)
				{
					Trace.WriteLine(ex.ToString());
					if (ex.GetType().ToString() == "System.Messaging.MessageQueueException")
					{
						EventLog.WriteEntry("QueueProcessor", "queue error\n\n" + ex.ToString(), EventLogEntryType.Error);
						Start(true);
					}
					else
					{
						LogError(queueItem, new MatchnetException(string.Format("Error processing queue item\nobject type: {0}\nqueue path: {1}\n\n", itemType, _QueuePath),
							"ReceiveCycle()",
							ex));
					}
				}

			}
		}


		private void LogError(QueueItemBase queueItem, MatchnetException ex)
		{
			string fileName = Guid.NewGuid().ToString();

			Trace.WriteLine(ex.ToString());

			try
			{
				ExceptionLogger.LogToEventLog(ex, "QueueProcessor", System.Diagnostics.EventLogEntryType.Error);

				string errorPath = string.Format(@"{0}\{1}\", ERROR_BASE_PATH, queueItem.GetType().ToString());
				if (!Directory.Exists(errorPath))
				{
					Directory.CreateDirectory(errorPath);
				}

				//serialize queue item
				XmlSerializer xmlSer = new XmlSerializer(queueItem.GetType());
				FileStream fs = new FileStream(string.Format(@"{0}\{1}.xml", errorPath, fileName), System.IO.FileMode.Create);
				xmlSer.Serialize(fs, queueItem);
				fs.Close();

				//write text description
				StreamWriter sw = new StreamWriter(string.Format(@"{0}\{1}.txt", errorPath, fileName));
				sw.WriteLine(_QueuePath);
				sw.WriteLine(ex.ToString());
				sw.Close();	
			}
			catch (Exception exLocal)
			{
				ExceptionLogger.LogToEventLog(new MatchnetException("QueueProcessor", "LogError()", exLocal));
			}
		}


		public bool IsRunning
		{
			get
			{
				return _IsRunning;
			}
		}
	}
}
