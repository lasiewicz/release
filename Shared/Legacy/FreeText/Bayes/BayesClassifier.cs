using System;
using System.Collections;
using System.Data;
using System.Diagnostics;


namespace Bayes
{
	/// <summary>
	/// Summary description for BayesClassifier.
	/// </summary>
	public class BayesClassifier
	{
		private Hashtable _ReasonsList = new Hashtable();
		private static Buckets _bayBuckets = new Buckets();

		public BayesClassifier()
		{
		}
		
		static BayesClassifier()
		{
			_bayBuckets.Load();
		}

		public DataTable Classify(Corpus crp)
		{
			return Classify(_bayBuckets,crp);
		}

		public DataTable Classify(Buckets bks, Corpus crp){
			DataTable dt = new DataTable("Classification");
			double p = 0.0;
			

			dt.Columns.Add(new DataColumn("Bucket",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("Probability",System.Type.GetType("System.Double")));
			dt.Columns.Add(new DataColumn("BucketWeight",System.Type.GetType("System.Double")));
			dt.DefaultView.Sort = "Probability DESC, BucketWeight DESC";

			foreach (Bucket b in bks.BucketList.Values)	{
				p = BucketProbability(crp,b,bks.WordCount);
				dt.Rows.Add(new object[] {b.Name,p,b.Probability});
			}
			return dt;
		}

		public Hashtable ReasonList
		{
			get	{
				return _ReasonsList;
			}
		}

		private double BucketProbability(Corpus crp, Bucket b, double WordCountInAllBuckets){
			double PCumulative;
			double Enumerator = 1.0D;
			double EnumeratorUnlikely = 1.0D;
			double Denominator = 0.0D;
			double WordFrequencyCorpus;
			double WordFrequencyBucket;
			double WordFrequencyUnlikely = 1 + 0.001/WordCountInAllBuckets;

			DataTable Reasons = new DataTable("Reasons");

			Reasons.Columns.Add(new DataColumn("Word",System.Type.GetType("System.String")));
			Reasons.Columns.Add(new DataColumn("Weight",System.Type.GetType("System.Double")));
			Reasons.Columns.Add(new DataColumn("CrpFrq(w)",System.Type.GetType("System.Double")));
			Reasons.Columns.Add(new DataColumn("BktPrb(w)",System.Type.GetType("System.Double")));
			Reasons.DefaultView.Sort = "Weight DESC";
			
			foreach (string s in crp.WordTable.Keys) {
				if (b.Words.ContainsKey(s) ) {
					WordFrequencyBucket = ((Word)b.Words[s]).Frequency ;
				} 
				else {
					WordFrequencyBucket = WordFrequencyUnlikely;
				}
				
				WordFrequencyCorpus = (double)crp.WordTable[s];
				
				if ( WordFrequencyBucket != WordFrequencyUnlikely 
					&& s.StartsWith("Nx[") && b.Name != "Normal") {

					WordFrequencyCorpus *= 1000; // Boost Nx patterns weight
				}

				if (WordFrequencyBucket != WordFrequencyUnlikely) {
					Enumerator *= WordFrequencyBucket * WordFrequencyCorpus;
				}
				else{
					EnumeratorUnlikely *= WordFrequencyBucket * WordFrequencyCorpus;
				}
				
				Reasons.Rows.Add(new object[] {s, WordFrequencyBucket * WordFrequencyCorpus, WordFrequencyCorpus, WordFrequencyBucket }); 
			}
			
			Denominator = /* (crp.WordCount) * */ Math.Log(b.WordCount); // log(a^b) = b * log(a)
			PCumulative = (Enumerator == 1.0D) ? WordFrequencyUnlikely -1 : Enumerator * EnumeratorUnlikely / Denominator;

			_ReasonsList.Add(b.Name, Reasons);

			return PCumulative;
		}

	}
}
