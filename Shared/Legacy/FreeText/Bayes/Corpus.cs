using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Bayes;

namespace Bayes
{
	/// <summary>
	/// Summary description for Corpus.
	/// </summary>
	public class Corpus
	{
		private double _WordCount = 0.0D;
		private Hashtable _WordTable;
		private StringCollection StopWords = new StringCollection();
		private StringCollection TLD = new StringCollection();
		
		private static string[] Digits =  {"Zero","One","Two","Three","Four","Five","Six","Seven","Eight","Nine"};
		private static string[] _TLD = {"c\\W{0,}o\\W{0,}m","n\\W{0,}e\\W{0,}t","o\\W{0,}r\\W{0,}g","e\\W{0,}d\\W{0,}u","gov","mil","b\\W{0,}i\\W{0,}z","info" };
		private static string[] _ISP = {"yahoo","aol","hotmail","msn","earthlink","excite"};
		private static string[] _IM = {"aim","icq"," im me "," an im "};
		private static string[] _URL = {"http",".htm","www",".gif",".jpg",".php",".asp",".cgi",".exe"};
		private static string[] _StopWords = {"you","strike","date","form","him","code","also","sun","your","but","and","small","area",
										"all","being","jan","she","color","have","will","received","going","can","were","would",
										"off","ask","doing","tue","person","his","big","yes","has","not","that","gone","our","its",
										"out","with","go","did","range","from","her","any","spot","the","done","yet","its","went","could",
										"does","this","for","are","having","been","had","was","very","like"};

		private static Regex objRegex = new Regex("[ \n\r\t;:,\\(\\)\\[\\]]|<[^@]+>");
		private static Regex objRegex0 = new Regex("(.)\\1{4,}");
		private static Regex objRegex1 = new Regex("(.)\\1{4,}");
		private static Regex objRegex2 = new Regex("\\d{2,}");
		private static Regex objRegex3 = new Regex("[^0-9a-zA-Z._\\-@]");
		private static Regex objRegex4 = new Regex("[bcdfghjklmnpqrstvwxz]{5,}",RegexOptions.IgnoreCase);
		private static Regex objRegex5 = new Regex("[aeiouy]{4,}", RegexOptions.IgnoreCase);
		private static Regex objRegex6 = new Regex("@|(([^\\W]+\\W?)?([^\\w ]a[\\W]{0,}t[^\\w ]))", RegexOptions.IgnoreCase);
		private static Regex objRegex8 = new Regex("(\\d\\W{0,}){7}", RegexOptions.IgnoreCase);


		public Corpus(string source)
		{
			string source_lc = source.ToLower();
			string LastToken = "";
			int SameTokenCount = 0;
			_WordTable = new Hashtable();

			LoadStopWords();
			foreach(string token in objRegex.Split(source_lc)){
			//foreach (string token in Regex.Split(source_lc,"[ \n\r\t;:,\\(\\)\\[\\]]|<[^@]+>")) {
				if (token == LastToken) {
					SameTokenCount++;
				}
				else {
					LastToken = token;
					SameTokenCount = 0;
				}
				if (SameTokenCount == 4)	{
					AddToken("Nx[RepeatToken]");
				}
				AddToken(token);
			}		

			if (AtSignIndication(source_lc)) {
				AddToken("Nx[AtSign]");
			}

			if (DomainIndication(source_lc)) { 
				AddToken("Nx[EmailAddress]");
			}

			if (URLIndication(source_lc)) {
				AddToken("Nx[URL]");
			}

			if (InstantMessageIndication(source_lc)) {
				AddToken("Nx[InstantMessage]");
			}
			
			if (PhoneNumberIndication(source_lc)) {
				AddToken("Nx[PhoneNumber]");
			}

		}

		private void LoadStopWords(){
			StopWords.AddRange(_StopWords);
			PorterStemmer ps = new PorterStemmer();
			foreach (string s in _StopWords){
				StopWords.Add(ps.stemTerm(s));
			}
			
		}

		private string ParseToken(string token){
			string s = token;
			if (objRegex0.Match(s).Success)
			//if (Regex.Match(s,"(.)\\1{4,}").Success) 
			{ 
				string sub = objRegex1.Match(s).Captures[0].Value;
				//string sub = Regex.Match(s,"(.)\\1{4,}").Captures[0].Value;
				//				AddToken( string.Format("Nx[{0}]",sub[0]));
				if (sub.Length != s.Length) 
				{
					AddToken(s.Substring(s.IndexOf(sub) + sub.Length));
				}
				return "Nx[SameChar]";
			}
			else if (s.IndexOf('@') > 0) {
				AddToken(s.Substring(s.IndexOf('@') + 1));
				return "Nx[AtSign]";
			}
			else {
				if (! s.StartsWith("Nx[")) {
					if(objRegex2.Match(s).Success){
					//if (Regex.Match(s,"\\d{2,}").Success ) {
						for (int i = 0; i < 10; i++) {
							s = s.Replace(i.ToString(),Digits[i]);
						}
					}
					else {
						s = objRegex3.Replace(s,"");
						//s = Regex.Replace(s,"[^0-9a-zA-Z._\\-@]","");
						if (s.Length > 42) { 
							s = "LONGWORD"; 
						}
						else if(objRegex4.Match(s).Success){
						//else if (Regex.Match(s,"[bcdfghjklmnpqrstvwxz]{5,}",RegexOptions.IgnoreCase).Success) {
							s = "Nx[Consonants]";
						}
						else if(objRegex5.Match(s).Success){
						//else if (Regex.Match(s,"[aeiouy]{4,}",RegexOptions.IgnoreCase).Success) {
							s = "Nx[Vowels]";
						} 
						else {
							PorterStemmer ps = new PorterStemmer();
							s = ps.stemTerm(s).ToLower();
						}
					}
				}
			}
			return s;
		}

		private void AddToken(string token){
			if (token == "") { return; }

			string s = ParseToken(token);
			
			// ignore short words
			if (s.Length < 3)			{ return; }
			// ignore stop words
			if (StopWords.Contains(s))	{ return; }
					
			if (_WordTable.ContainsKey(s))
			{
				_WordTable[s] = (double)_WordTable[s] + 1.0;
				_WordCount += 1.0;
					
			}
			else 
			{
				_WordTable.Add(s, 1.0);
				_WordCount += 1.0;
			}
		}
	
		private bool AtSignIndication(string Text){
			try{
				return objRegex6.IsMatch(Text);
			}
			catch {
				return false;
			}
		}

		private bool DomainIndication(string Text){
			try {
				foreach (string isp in _ISP) {
					if (Text.IndexOf(isp) > 1) {
						return true;
					}
				}
				foreach (string tld in _TLD) {
					Regex objRegex7 = new Regex("(\\.\\W{0,}|\\W+d\\W{0,}o\\W{0,}t\\W+)" + tld + "(\\W|$)",RegexOptions.IgnoreCase);

					if (objRegex7.IsMatch(Text)) 
					{
						return true;
					}
				}
			}
			catch{
				return false;
			}
			return false;
		}

		private bool InstantMessageIndication(string Text)
		{	
			try {
				foreach (string imname in _IM) {
					if (Text.IndexOf(imname) >= 0) {
						return true;
					}
				}
				return false;
			}
			catch {
				return false;
			}
		}

		private bool PhoneNumberIndication(string Text){
			try {
				return objRegex8.IsMatch(Text);
			}
			catch {
				return false;
			}
		}

		private bool URLIndication(string Text){
			try {
				foreach (string url in _URL) {
					if (Text.IndexOf(url) >= 0) {
						return true;
					}
				}
				return false;
			}
			catch {
				return false;
			}
				
		}

		public Hashtable WordTable{
			get	{
				return _WordTable;
			}
		}

		public Double WordCount 
		{
			get	
			{
				return _WordCount;
			}
		}
		
		public Double DistinctWordCount {
			get	{
				return _WordTable.Count;
			}
		}
		

	}
}
