using System;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Xml.Serialization;

namespace Bayes
{
	/// <summary>
	/// Summary description for Bucket.
	/// </summary>
	[Serializable]
	public class Bucket
	{
		/// <summary>
		/// Properties
		/// </summary>
		private string _Name;
		private double _WordCount = 0.0D;
		public double Probability = 0.0D;
		
		[XmlIgnoreAttribute]
		public Hashtable Words = new Hashtable();
		public Word []_Words;

		public Bucket(){
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="BucketName">The name of the bucket. This is the classification label</param>
		public Bucket(string BucketName)
		{
			_Name = BucketName;
		}


		public string Name{
			get{
				return _Name;
			}
			set {
				_Name = value;
			}
		}
		

		public double WordCount{
			get {
				return _WordCount;
			}
			set {
				_WordCount = value;
			}
		}


		public void AddWord(string Token, double Frequency){
			_WordCount += Frequency;

			if (!Words.ContainsKey(Token)) {
				Word w = new Word(Token,Frequency,0.0);
				Words[Token] = w;
			} 
			else {
				//Word w = (Word)Words[Token];
				((Word)Words[Token]).Frequency += Frequency;
			}

		}


		public void AddCorpus(Hashtable Corpus){
			foreach(string w in Corpus.Keys )	{
				AddWord(w,(double)Corpus[w]);
			}
			UpdateWordProbabilities();
		}


		public bool RemoveWord(string Token, double Frequency){
			if (Words.ContainsKey(Token) && ((Word)Words[Token]).Frequency >= Frequency){
				((Word)Words[Token]).Frequency -= Frequency;
				if (((Word)Words[Token]).Frequency == 0){
					Words.Remove(Token);
				}
				return true;
			}
			else {
				return false;
			}
		}


		public bool RemoveCorpus(Hashtable Corpus){
			foreach(Word w in Corpus.Keys) {
				if (!Words.ContainsKey(w) || ((Word)Words[w]).Frequency < (double)Corpus[w]){
					return false;
				}
			}
			
			foreach(string w in Corpus.Keys) {
				RemoveWord(w,(double)Corpus[w]);
			}

			UpdateWordProbabilities();
			return true;
		}
		

		public void UpdateWordProbabilities(){
			_WordCount = 0.0;

			foreach (Word w in Words.Values){
				_WordCount += w.Frequency;
			}

			foreach (Word w in Words.Values){
				w.Probability = w.Frequency / _WordCount;
			}
		}


		public bool SaveToFile(){
			try 
			{
				_Words = new Word[Words.Count];
				Words.Values.CopyTo(_Words,0);
				XmlSerializer ser = new XmlSerializer(typeof(Bucket));    
				TextWriter writer = new StreamWriter(BucketFileName);
				ser.Serialize(writer, this);
				writer.Close();
				return true;
			}
			catch (Exception ex){
				//throw ex;
				return false;
			}
			
		}


		public bool LoadFromFile(){
			XmlSerializer ser;
			FileStream fs;
			try
			{
				ser = new XmlSerializer(typeof(Bucket));
				string strFilePath = Buckets.BUCKETS_PATH + BucketFileName;
				fs = new FileStream(strFilePath, FileMode.Open);
				// Declare an object variable of the type to be deserialized.
				Bucket b;
				b = (Bucket)ser.Deserialize(fs);
				this._Name = b.Name;
				this._WordCount = b._WordCount;
				this.Probability = b.Probability;
				if (b._Words != null){
					foreach (Word w in b._Words) {
						this.Words.Add(w.Value,w);
					}
				}
				fs.Close();
				UpdateWordProbabilities();
				return true;
			}
			catch (Exception ex){
				throw ex;
			}

		}


		public string BucketFileName{
			get
			{
				return "Bucket_" + Name + ".xml";
			}
		}
	}
}
