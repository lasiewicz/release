using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Text;

namespace Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Harness1 : System.Windows.Forms.Form
	{
		private Bayes.Buckets _Buckets = new Bayes.Buckets();
		private Hashtable ReasonsList;
		private System.Windows.Forms.Button CreateBucket;
		private System.Windows.Forms.ComboBox cmbBucketList;
		private System.Windows.Forms.TextBox TextSource;
		private System.Windows.Forms.Button Classify;

		private System.Windows.Forms.Button Analyze;
		private System.Windows.Forms.DataGrid ResultGrid;
		private System.Windows.Forms.CheckBox AnalyzeOnDrop;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Harness1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			_Buckets.Load();
			foreach (Bayes.Bucket  b in _Buckets.BucketList.Values){
				cmbBucketList.Items.Add(b.Name);
//				TextSource.Text += String.Format("{0}: {1}\n",b.Name,b.Probability );
			}
			cmbBucketList.Refresh();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Harness1));
			this.CreateBucket = new System.Windows.Forms.Button();
			this.cmbBucketList = new System.Windows.Forms.ComboBox();
			this.TextSource = new System.Windows.Forms.TextBox();
			this.Classify = new System.Windows.Forms.Button();
			this.Analyze = new System.Windows.Forms.Button();
			this.ResultGrid = new System.Windows.Forms.DataGrid();
			this.AnalyzeOnDrop = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.ResultGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// CreateBucket
			// 
			this.CreateBucket.Location = new System.Drawing.Point(520, 8);
			this.CreateBucket.Name = "CreateBucket";
			this.CreateBucket.Size = new System.Drawing.Size(88, 23);
			this.CreateBucket.TabIndex = 1;
			this.CreateBucket.Text = "Create &Bucket";
			this.CreateBucket.Click += new System.EventHandler(this.CreateBucket_Click);
			// 
			// cmbBucketList
			// 
			this.cmbBucketList.Location = new System.Drawing.Point(248, 8);
			this.cmbBucketList.Name = "cmbBucketList";
			this.cmbBucketList.Size = new System.Drawing.Size(160, 21);
			this.cmbBucketList.TabIndex = 1;
			// 
			// TextSource
			// 
			this.TextSource.AllowDrop = true;
			this.TextSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.TextSource.Location = new System.Drawing.Point(8, 40);
			this.TextSource.Multiline = true;
			this.TextSource.Name = "TextSource";
			this.TextSource.Size = new System.Drawing.Size(600, 400);
			this.TextSource.TabIndex = 3;
			this.TextSource.Text = "";
			this.TextSource.DragDrop += new System.Windows.Forms.DragEventHandler(this.TextSource_DragDrop);
			this.TextSource.DragEnter += new System.Windows.Forms.DragEventHandler(this.TextSource_DragEnter);
			// 
			// Classify
			// 
			this.Classify.Location = new System.Drawing.Point(416, 8);
			this.Classify.Name = "Classify";
			this.Classify.Size = new System.Drawing.Size(96, 23);
			this.Classify.TabIndex = 4;
			this.Classify.Text = "&Classify";
			this.Classify.Click += new System.EventHandler(this.Classify_Click);
			// 
			// Analyze
			// 
			this.Analyze.Location = new System.Drawing.Point(8, 8);
			this.Analyze.Name = "Analyze";
			this.Analyze.Size = new System.Drawing.Size(72, 23);
			this.Analyze.TabIndex = 0;
			this.Analyze.Text = "&Analyze";
			this.Analyze.Click += new System.EventHandler(this.Analyze_Click);
			// 
			// ResultGrid
			// 
			this.ResultGrid.DataMember = "";
			this.ResultGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.ResultGrid.Location = new System.Drawing.Point(8, 448);
			this.ResultGrid.Name = "ResultGrid";
			this.ResultGrid.PreferredColumnWidth = 150;
			this.ResultGrid.ReadOnly = true;
			this.ResultGrid.Size = new System.Drawing.Size(600, 200);
			this.ResultGrid.TabIndex = 5;
			this.ResultGrid.DoubleClick += new System.EventHandler(this.ResultGrid_DoubleClick);
			// 
			// AnalyzeOnDrop
			// 
			this.AnalyzeOnDrop.Location = new System.Drawing.Point(88, 8);
			this.AnalyzeOnDrop.Name = "AnalyzeOnDrop";
			this.AnalyzeOnDrop.Size = new System.Drawing.Size(112, 24);
			this.AnalyzeOnDrop.TabIndex = 6;
			this.AnalyzeOnDrop.Text = "Analyze on Drop";
			// 
			// Harness1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 653);
			this.Controls.Add(this.AnalyzeOnDrop);
			this.Controls.Add(this.ResultGrid);
			this.Controls.Add(this.Analyze);
			this.Controls.Add(this.Classify);
			this.Controls.Add(this.TextSource);
			this.Controls.Add(this.cmbBucketList);
			this.Controls.Add(this.CreateBucket);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Harness1";
			this.Text = "Bayes Test Harness";
			((System.ComponentModel.ISupportInitialize)(this.ResultGrid)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Harness1());
		}


		private void button1_Click(object sender, System.EventArgs e)
		{
//			TextReader tr = new TextReader();
		
		}


		private void CreateBucket_Click(object sender, System.EventArgs e)
		{
			string FileName = "Bucket_" + cmbBucketList.Text + ".xml";
			string BucketName = cmbBucketList.Text;

			if (File.Exists(FileName))
			{
				MessageBox.Show("Bucket [" + FileName + "] already exists.");
			}
			else{
				Bayes.Bucket b = new Bayes.Bucket(BucketName);
				b.SaveToFile();
				_Buckets.BucketList.Add(BucketName,b);
				cmbBucketList.Items.Add(BucketName);
				cmbBucketList.Refresh();
				MessageBox.Show("Created bucket [" + FileName + "] for [" + cmbBucketList.Text + "]");				
			}

		}


		private void Classify_Click(object sender, System.EventArgs e)
		{
			if (TextSource.Text.Length < 3) return;

			Bayes.Corpus c = new Bayes.Corpus(TextSource.Text);
			Bayes.Bucket b = (Bayes.Bucket)_Buckets.BucketList[cmbBucketList.Text];
			b.AddCorpus(c.WordTable );
			_Buckets.UpdateBucketProbabilities(true);
			_Buckets.Save();
			MessageBox.Show(String.Format("This corpus classified as {0}",b.Name));
		}


		private void TextSource_DragDrop(object Sender, DragEventArgs e)
		{
			try {
				if (e.Data.GetDataPresent(DataFormats.FileDrop))
				{
					StringBuilder sb = new StringBuilder();
					foreach (string FileName in (Array)e.Data.GetData(DataFormats.FileDrop)) {
						StreamReader sr = new StreamReader(FileName);
						sb.Append(sr.ReadToEnd());
						sr.Close();
					}
					TextSource.Text = sb.ToString();
					if (AnalyzeOnDrop.Checked) {
						Analyze_Click(null,null);
					}
				}
			}
			catch {
				TextSource.Text = "<Failed aquiring dragged file content>";
			}
			TextSource.Refresh();
		}


		private void TextSource_DragEnter(object Sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) 
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;

		}


		private void Analyze_Click(object sender, System.EventArgs e)
		{
			Bayes.BayesClassifier bc = new Bayes.BayesClassifier();
			ResultGrid.DataSource = null;
			ResultGrid.DataSource =  bc.Classify(_Buckets,new Bayes.Corpus(TextSource.Text));
			ReasonsList = bc.ReasonList;
		}

		private void ResultGrid_DoubleClick(object sender, System.EventArgs e){
			if ( ((DataTable)((DataGrid)sender).DataSource).TableName.Equals("Classification")) {
				string s = ResultGrid[ new DataGridCell(((DataGrid)sender).CurrentRowIndex,0)].ToString();
				ResultGrid.DataSource = (DataTable)ReasonsList[s];
			}
			else {
				Analyze_Click(null,null);
			}
		}



	}
}
