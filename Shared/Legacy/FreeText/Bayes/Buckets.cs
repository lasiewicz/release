using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Data;


namespace Bayes
{
	/// <summary>
	/// Summary description for Buckets.
	/// </summary>
	public class Buckets
	{
		public Hashtable BucketList = new Hashtable();
		public double Probability = 0.0D;
		public double WordCount = 0.0D;
		public const string BUCKETS_PATH = @"C:\matchnet\bin\net\Core\FreeText\Bayes\Buckets\";

		public Buckets()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void UpdateBucketProbabilities(bool DeepUpdate){
			WordCount = 0.0;
			
			foreach ( Bucket b in BucketList.Values)
			{
				if (DeepUpdate){
					b.UpdateWordProbabilities();
				}
				WordCount += b.WordCount;
			}

			foreach ( Bucket b in BucketList.Values)
			{
				b.Probability = b.WordCount / WordCount;
			}
			
			Save();

		}

		public void Load(){
			DirectoryInfo objDirectoryInfo = new DirectoryInfo(BUCKETS_PATH);
			foreach(FileSystemInfo objFileInfo in objDirectoryInfo.GetFileSystemInfos("Bucket_*"))
			{
				string strBucketName = objFileInfo.Name;
				strBucketName = strBucketName.Substring(7, strBucketName.Length - 11);
				Bucket objBucket = new Bucket(strBucketName);
				if (objBucket.LoadFromFile())
				{
					BucketList.Add(strBucketName, objBucket);
				}
			}

//			string []dir = Directory.GetFiles(".","Bucket_*");
//
//			foreach (string s in dir)
//			{
//				string BucketName = s.Substring(9, s.Length - 13);
//				Bucket b = new Bucket(BucketName);
//				if (b.LoadFromFile()){
//					BucketList.Add(BucketName,b);
//				}
//			}

			UpdateBucketProbabilities(true);
		}

		public void Save(){
			foreach (string s in BucketList.Keys){
				((Bucket)BucketList[s]).SaveToFile();
			}
		}
	}
}
