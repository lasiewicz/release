using System;

namespace Bayes
{
	/// <summary>
	/// Summary description for Word.
	/// </summary>
	public class Word
	{
		public double Frequency;
		public double Probability;
		public string Value;
		
		public Word(){

		}

		public Word(string word,double Frequency, double Probability){
			this.Value = word;
			this.Frequency = Frequency;
			this.Probability = Probability;
		}
		public override string ToString(){
			return string.Format("{0}: [F{1},P{2}",Value,Frequency,Probability);
		}

	}
}
