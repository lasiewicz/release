#region System References
using System;
using System.Collections;
using System.Data;
#endregion

#region Matchnet Web App References
using Matchnet.CachingTemp;
using Matchnet.SharedLib;
#endregion

namespace Matchnet.MembersOnline
{
	public class MembersOnline
	{
		private const int PAGE_SIZE = 10;

		private ArrayList _MemberIDs;
		private bool _MorePagesExist = false;

		public void Populate(int domainID,
			int privateLabelID,
			int genderMask,
			int regionID,
			int minAge,
			int maxAge,
			int languageMask,
			DateTime maxLogonDate,
			string orderBy,
			int startRow,
			string sessionKey)
		{
			Populate(domainID, 
				privateLabelID, 
				genderMask, 
				regionID, 
				minAge, 
				maxAge, 
				languageMask, 
				maxLogonDate, 
				orderBy, 
				startRow, 
				sessionKey, 
				PAGE_SIZE);
		}

		public void Populate(int domainID,
								int privateLabelID,
								int genderMask,
								int regionID,
								int minAge,
								int maxAge,
								int languageMask,
								DateTime maxLogonDate,
								string orderBy,
								int startRow,
								string sessionKey,
								int pageSize)
		{
			// ensure guid
			int foo3 = Math.Abs(Environment.TickCount);

			_MemberIDs = new ArrayList();
		
			//TODO: Remove SQL 
			Matchnet.DataTemp.SQLDescriptor descriptor = new Matchnet.DataTemp.SQLDescriptor("mnSession", foo3);
			Matchnet.DataTemp.SQLClient client = new Matchnet.DataTemp.SQLClient(descriptor);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			if (genderMask != SharedLib.Constants.DefaultValues.Integer)
			{
				client.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, genderMask);
			}
			if (regionID != SharedLib.Constants.DefaultValues.Integer)
			{
				client.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
			}
			client.AddParameter("@MinAge", SqlDbType.Int, ParameterDirection.Input, minAge);
			client.AddParameter("@MaxAge", SqlDbType.Int, ParameterDirection.Input, maxAge);
			if (languageMask != SharedLib.Constants.DefaultValues.Integer)
			{
				client.AddParameter("@LanguageMask", SqlDbType.Int, ParameterDirection.Input, languageMask);
			}
			client.AddParameter("@MaxLogonDate", SqlDbType.DateTime, ParameterDirection.Input, maxLogonDate);
			client.AddParameter("@OrderBy", SqlDbType.VarChar, ParameterDirection.Input, GetOrderClause(orderBy));
			client.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
			client.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize + 1);

			DataTable dt = client.GetDataTable("up_MemberOnline_List", CommandType.StoredProcedure);

			int recordNum = 1;
			foreach (DataRow row in dt.Rows)
			{
				_MemberIDs.Add(Convert.ToInt32(row["MemberID"]));

				if (recordNum == pageSize)
				{
					_MorePagesExist = true;
					return;
				}
				recordNum++;
			}
		}

		public DataTable PopulateFeatureMemberOnline(int domainID,
			int privateLabelID,
			int genderMask,
			int regionID,
			int minAge,
			int maxAge,
			int languageMask,
			bool mustHavePhoto,
			DateTime maxLogonDate,
			string orderBy,
			int startRow,
			string sessionKey)
		{
			// ensure guid
			int foo3 = Math.Abs(Environment.TickCount);

			_MemberIDs = new ArrayList();
			DataTable dt = null;
			
			string cacheKey = "molbox:" + domainID + ":" + genderMask + ":" + minAge + ":" + maxAge;

			object val = Cache.GetInstance().Get(cacheKey);

			if (val == null) 
			{
				//TODO: Remove SQL 
				Matchnet.DataTemp.SQLDescriptor descriptor = new Matchnet.DataTemp.SQLDescriptor("mnSession", foo3);
				Matchnet.DataTemp.SQLClient client = new Matchnet.DataTemp.SQLClient(descriptor);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
				if (genderMask != SharedLib.Constants.DefaultValues.Integer)
				{
					client.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, genderMask);
				}
				if (regionID != SharedLib.Constants.DefaultValues.Integer)
				{
					client.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
				}
				client.AddParameter("@MinAge", SqlDbType.Int, ParameterDirection.Input, minAge);
				client.AddParameter("@MaxAge", SqlDbType.Int, ParameterDirection.Input, maxAge);
				if (languageMask != SharedLib.Constants.DefaultValues.Integer)
				{
					client.AddParameter("@LanguageMask", SqlDbType.Int, ParameterDirection.Input, languageMask);
				}
				client.AddParameter("@HasPhotoFlag", SqlDbType.Bit, ParameterDirection.Input, mustHavePhoto);
				if (maxLogonDate != DateTime.MinValue)
				{
					client.AddParameter("@MaxLogonDate", SqlDbType.DateTime, ParameterDirection.Input, maxLogonDate);
				}
				client.AddParameter("@OrderBy", SqlDbType.VarChar, ParameterDirection.Input, GetOrderClause(orderBy));
				client.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
				client.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, PAGE_SIZE + 1);

				dt = client.GetDataTable("up_MemberOnline_List", CommandType.StoredProcedure);

				// Insert DataTable to cache
				Cache cache = Cache.GetInstance();

				cache.Insert(
					cacheKey,
					dt,
					null,
					cache.NoAbsoluteExpiration,
					TimeSpan.FromSeconds(60));
			} 
			else 
			{
				dt = ((DataTable) val);
			}

			return dt;

		}

		private string GetOrderClause(string orderBy)
		{
			switch (orderBy.ToLower())
			{
				case "insertdate desc":
				case "insertdate asc":
					return orderBy;

				case "lastupdated desc":
				case "hasphotoflag desc":
				case "username desc":
				case "birthdate desc":
				case "depth3regionid desc":
				case "updatedate desc":
					return orderBy + ", insertdate desc";

				case "lastupdated asc":
				case "hasphotoflag asc":
				case "username asc":
				case "birthdate asc":
				case "depth3regionid asc":
				case "updatedate asc":
					return orderBy + ", insertdate desc";

				case "gendermask desc":
					return "gendermask & 1 asc, insertdate desc";

				case "gendermask asc":
					return "gendermask & 1 desc, insertdate desc";

				default:
					return "updatedate desc, insertdate desc";
			}
		}


		public ArrayList MemberIDs
		{
			get
			{
				return _MemberIDs;
			}
		}

		public bool MorePagesExist
		{
			get
			{
				return _MorePagesExist;
			}
		}

		public int GetCount(int domainID, string sessionKey)
		{
			// ensure session key
			sessionKey = Guid.NewGuid().ToString();

			int totalCount = 0;
			Matchnet.DataTemp.SQLDescriptor descriptor = new Matchnet.DataTemp.SQLDescriptor("mnSession", Matchnet.Lib.Util.Util.CRC32(sessionKey));
			Matchnet.DataTemp.SQLClient client = new Matchnet.DataTemp.SQLClient(descriptor);
			//TODO: Remove SQL 

			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			DataTable table = client.GetDataTable("up_Session_ListCount", CommandType.StoredProcedure);

			totalCount = 0;
			if (table != null && table.Rows.Count > 0)
			{
				// todo: temporary hack to convert to an int (ConvertToInt)
				if(table.Rows[0]["TotalRows"] != System.DBNull.Value)
				{
					double dResult;
					if(Double.TryParse(table.Rows[0]["TotalRows"].ToString(), 
						System.Globalization.NumberStyles.Integer, 
						System.Globalization.NumberFormatInfo.InvariantInfo, 
						out dResult))
					{
						totalCount =  Convert.ToInt32(dResult);
					}
					else
					{
						totalCount = Constants.NULL_INT;
					}
				}
				else
				{
					totalCount = Constants.NULL_INT;
				}
			}
			
			return totalCount;
		}
	}
}
