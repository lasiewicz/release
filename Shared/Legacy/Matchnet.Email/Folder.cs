using Matchnet.DataTemp;
using Matchnet.Email.ValueObjects;
using Matchnet.Lib.Exceptions;

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace Matchnet.Email
{

	public class Folder 
	{
		public enum SYSTEM_TABLES
		{
			INBOX = 1,
			DRAFT = 2,
			SENT = 3,
			TRASH = 4
		}

		private const string FOLDER_DATABASE_FILE = "mnMember";
		private const string SAVE_FOLDER_STORED_PROCEDURE = "up_MemberFolder_Save";
		private const string DELETE_FOLDER_STORED_PROCEDURE = "up_MemberFolder_Delete";
		private const string EMPTY_FOLDER_STORED_PROCEDURE = "up_MemberMail_ClearFolder";
		private const string GET_FOLDERS_STORED_PROCEDURE = "up_MemberMail_Folders_List";
		private const int MAX_MEMBER_FOLDER_DESCRIPTION_LENGTH = 20;
		private const int MAX_SYSTEM_FOLDER_ID = 100;
		private static Folder instance = new Folder();

		public ICollection retrieveFolders(int memberID, int domainID, bool statsFlag) 
		{
			SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;
			DataTable retrievedFolders = null;
			DataRow currentRow = null;
			ArrayList foundFolders = null;
			EmailFolder currentFolder = null;
			IEnumerator folderEnumerator = null;
			int totalDisplayCount = 0;

			try
			{
				aSqlDescriptor = new SQLDescriptor(FOLDER_DATABASE_FILE, memberID);
				client = new SQLClient(aSqlDescriptor);
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
				client.AddParameter("@StatsFlag", SqlDbType.Bit, ParameterDirection.Input, statsFlag);
				
				foundFolders = new ArrayList();

				retrievedFolders = client.GetDataTable(GET_FOLDERS_STORED_PROCEDURE, CommandType.StoredProcedure);
				if(retrievedFolders != null && retrievedFolders.Rows.Count > 0) 
				{
					folderEnumerator = retrievedFolders.Rows.GetEnumerator();
					while(folderEnumerator.MoveNext())
					{
						currentRow = (DataRow)folderEnumerator.Current;
						currentFolder = new EmailFolder();
						currentFolder.MemberFolderID = int.Parse(currentRow["memberFolderID"].ToString());
						currentFolder.MemberID = memberID;
						currentFolder.DomainID = domainID;
						currentFolder.Description = currentRow["description"].ToString();
						currentFolder.SystemFolder = isSystemFolderID(currentFolder.MemberFolderID);

						if((currentRow["folderDisplayKBytes"] != null) && (currentRow["folderDisplayKBytes"].ToString().Length > 0)) 
						{
							currentFolder.DisplayKBytes = Convert.ToInt32(currentRow["folderDisplayKBytes"].ToString());
						} 
						else 
						{
							currentFolder.DisplayKBytes = 0;
						}

						if((currentRow["messages"] != null) && (currentRow["messages"].ToString().Length > 0)) 
						{
							currentFolder.TotalMessages = Convert.ToInt32(currentRow["messages"].ToString());
						} 
						else 
						{
							currentFolder.TotalMessages = 0;
						}

						if((currentRow["unread"] != null) && (currentRow["unread"].ToString().Length > 0)) 
						{
							currentFolder.UnreadMessages = Convert.ToInt32(currentRow["unread"].ToString());
						} 
						else 
						{
							currentFolder.UnreadMessages = 0;
						}
						
						totalDisplayCount += currentFolder.DisplayKBytes;

						foundFolders.Add(currentFolder);
					}

					// go back thru the found folders and set the total account bytes used
					for(int i = 0; i < foundFolders.Count; i++) 
					{
						currentFolder = (EmailFolder)foundFolders[i];
						currentFolder.TotalDisplayKBytes = totalDisplayCount;
					}
				}
				return foundFolders;
			} 
			catch(Exception anException) 
			{
				throw new MatchnetException("Error Encountered Retrieving Member Folder(s)", "retrieveFolders", anException);
			}
		}

		public void emptyFolder(int memberID, int domainID, int memberFolderID) 
		{
			SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;

			try
			{
				aSqlDescriptor = new SQLDescriptor(FOLDER_DATABASE_FILE, memberID);
				client = new SQLClient(aSqlDescriptor);

				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
				client.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, memberFolderID);

				client.ExecuteNonQuery(EMPTY_FOLDER_STORED_PROCEDURE, CommandType.StoredProcedure);
			} 
			catch(Exception anException) 
			{
				throw new MatchnetException("Error Encountered Purging Member Folder", "emptyFolder", anException);
			}
		}

		public void delete(int memberFolderID, int memberID) 
		{
			SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;

			try
			{
				aSqlDescriptor = new SQLDescriptor(FOLDER_DATABASE_FILE, memberID);
				client = new SQLClient(aSqlDescriptor);

				client.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, memberFolderID);
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);

				client.ExecuteNonQuery(DELETE_FOLDER_STORED_PROCEDURE, CommandType.StoredProcedure);
			} 
			catch(Exception anException) 
			{
				throw new MatchnetException("Error Encountered Deleting Member Folder", "delete", anException);
			}
		}

		// provided for new folder creations
		public int save(int memberID, int domainID, string description) 
		{
			try 
			{
				return save(memberID, domainID, description, -1);
			} 
			catch (Exception anException) 
			{
				throw anException;
			}
		}

		// provided for folder renames
		public int save(int memberID, int domainID, string description, int memberFolderID) 
		{
			SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;
			SqlCommand command = null;
			
			int returnValue = 0;

			try
			{
				description = description.Trim();
				if(description.Length <= MAX_MEMBER_FOLDER_DESCRIPTION_LENGTH) 
				{
					aSqlDescriptor = new SQLDescriptor(FOLDER_DATABASE_FILE, memberID);
					client = new SQLClient(aSqlDescriptor);

					client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue, returnValue);
					client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
					client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
					client.AddParameter("@Description", SqlDbType.VarChar, ParameterDirection.Input, description);
					if(memberFolderID > 0) 
					{
						client.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, memberFolderID);
					} 
					else 
					{
						client.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, DBNull.Value);
					}
					
					command = new SqlCommand(SAVE_FOLDER_STORED_PROCEDURE);
					command.CommandType = CommandType.StoredProcedure;
					client.PopulateCommand(command);
					client.ExecuteNonQuery(command);

					return Convert.ToInt32(command.Parameters["@ReturnValue"].Value);
				} 
				else 
				{
					throw new MatchnetException("Folder Description has Too Many Letters.");
				}
			} 
			catch(Exception anException) 
			{
				throw new MatchnetException("Error Encountered Saving Member Folder", "save", anException);
			}
		}

		public bool isSystemFolderID(int folderID) 
		{
			return (folderID <= MAX_SYSTEM_FOLDER_ID);
		}

		public static Folder getInstance() 
		{
			return instance;
		}

		private Folder()
		{
		}
	}
}
