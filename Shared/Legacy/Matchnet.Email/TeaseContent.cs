using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.Caching;

using Matchnet.DataTemp;
using Matchnet.CachingTemp;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Email
{
	/// <summary>
	/// Summary description for TeaseContent.
	/// </summary>
	public class TeaseContent
	{
		#region Static variables

		private const int CACHE_TTL					= 3600;
		private const string OPTION_CACHEKEY_PREFIX	= "Option:";		

		#endregion

		#region Class members

		private int _teaseCategoryID;
		private int _privateLabelID;
		private int _languageID;
		private Matchnet.CachingTemp.Cache _Cache;

		#endregion

		#region Public methods

		public TeaseContent(int teaseCategoryID, int privateLabelID, 
			int languageID)
		{
			this._teaseCategoryID	= teaseCategoryID;
			this._privateLabelID	= privateLabelID;
			this._languageID		= languageID;
			this._Cache				= Matchnet.CachingTemp.Cache.GetInstance();
		}

		public ArrayList getTeaseContentItems(int domainID, int translationID)
		{
			string key						= null;
			ArrayList teaseContentItems		= null;
			SqlCommand cmd					= null;
			DataTable dt					= null;
			SQLClient client				= null;
			Matchnet.Email.ValueObjects.TeaseContentItem tci		
				= null;
			
			key = OPTION_CACHEKEY_PREFIX + "TeaseContentItems:" 
				+ this._teaseCategoryID.ToString() + ":" 
				+ domainID.ToString() + ":" + translationID.ToString();
			dt = (DataTable) _Cache[ key ];

			if (dt == null)
			{
				client = new SQLClient(new SQLDescriptor("mnShared"));

				client.AddParameter("@TeaseCategoryID", SqlDbType.Int, 
					ParameterDirection.Input, this._teaseCategoryID);
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, 
					ParameterDirection.Input, this._privateLabelID);
				client.AddParameter("@LanguageID", SqlDbType.Int, 
					ParameterDirection.Input, this._languageID);

				cmd				= new SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "up_Tease_List";

				client.PopulateCommand(cmd);
			
				dt = client.GetDataTable(cmd);
				
				_Cache.Insert(key, dt, null, 
					DateTime.Now.AddSeconds(CACHE_TTL), TimeSpan.Zero,
					CacheItemPriority.High, null);
			}
			teaseContentItems = new ArrayList();

			foreach (DataRow row in dt.Rows)
			{
				int teaseID;
				string content;

				teaseID = Convert.ToInt32(row["TeaseID"]);
				content	= Convert.ToString(row["Content"]);
				tci				
					= new TeaseContentItem(teaseID, content);
				
				teaseContentItems.Add(tci);
			}

			return teaseContentItems;
		}

		#endregion

		#region Protected methods

		#endregion

		#region Private methods

		#endregion

		#region Static methods

		#endregion

		#region Test container

		#endregion
	}
}
