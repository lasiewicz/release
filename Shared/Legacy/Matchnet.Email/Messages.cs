using System;
using System.Data;
using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.DataTemp;
using System.Data.SqlClient;

namespace Matchnet.Email
{
	/// <summary>
	/// Summary description for Messages.
	/// </summary>
	public class Messages
	{
		public Messages()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public bool HasNewMail(int memberID, int domainID)
		{
			
		
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);

			SqlCommand cmd = new SqlCommand("up_MemberMail_HasNewMail");
			cmd.CommandType = CommandType.StoredProcedure;
			client.PopulateCommand(cmd);

			DataTable table = client.GetDataTable(cmd);

			if (table != null && table.Rows.Count > 0)
			{
			
				if (Util.CBool(table.Rows[0]["HasNewMail"], false) == true)
				{
					return true;
				}
			}
			
			return false;
		}
	}
}

