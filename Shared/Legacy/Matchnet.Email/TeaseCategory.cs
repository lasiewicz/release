using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Caching;

using Matchnet.DataTemp;
using Matchnet.CachingTemp;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Email
{
	/// <summary>
	/// Summary description for TeaseCategory.
	/// </summary>
	public class TeaseCategory
	{
		#region Static variables

		private const int CACHE_TTL					= 3600;
		private const string OPTION_CACHEKEY_PREFIX	= "Option:";		

		#endregion

		#region Class members

		private int _privateLabelID;
		private int _languageID;
		private Matchnet.CachingTemp.Cache _Cache;

		#endregion

		#region Public methods

		public TeaseCategory(int privateLabelID, int languageID)
		{
			this._privateLabelID	= privateLabelID;
			this._languageID		= languageID;
			this._Cache				= Matchnet.CachingTemp.Cache.GetInstance();
		}

		public ArrayList getTeaseCategoryItems(int domainID, int translationID)
		{
			string key						= null;
			ArrayList teaseCategoryItems	= null;
			DataTable dt					= null;
			SqlCommand cmd					= null;
			SQLClient client				= null;
			Matchnet.Email.ValueObjects.TeaseCategoryItem tci 
				= null;
			
			key = OPTION_CACHEKEY_PREFIX + "TeaseCategoryItems:" 
				+ domainID.ToString() + ":" + translationID.ToString();
			dt = (DataTable) _Cache[ key ];			
			
			if (dt == null)
			{
				client = new SQLClient(new SQLDescriptor("mnShared"));

				client.AddParameter("@PrivateLabelID", SqlDbType.Int, 
					ParameterDirection.Input, this._privateLabelID);
				client.AddParameter("@LanguageID", SqlDbType.Int, 
					ParameterDirection.Input, this._languageID);

				cmd				= new SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "up_TeaseCategory_List";

				client.PopulateCommand(cmd);
			
				dt = client.GetDataTable(cmd);

				_Cache.Insert(key, dt, null, 
					DateTime.Now.AddSeconds(CACHE_TTL), TimeSpan.Zero,
					CacheItemPriority.High, null);
			}			
							
			teaseCategoryItems = new ArrayList();

			foreach (DataRow row in dt.Rows)
			{
				int teaseCategoryID;
				string content;

				teaseCategoryID = Convert.ToInt32(row["TeaseCategoryID"]);
				content			= Convert.ToString(row["Content"]);
				tci				
					= new TeaseCategoryItem(teaseCategoryID, content);
				
				teaseCategoryItems.Add(tci);
			}

			return teaseCategoryItems;
		}

		#endregion

		#region Protected methods

		#endregion

		#region Private methods

		#endregion

		#region Static methods

		#endregion

		#region Test container

		#endregion
	}
}
