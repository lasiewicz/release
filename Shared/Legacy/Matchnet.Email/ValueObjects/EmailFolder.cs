using Matchnet.Lib;
using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// EmailFolder represents a single email folder.
	/// </summary>
	// EmailFolder should implement IValueObject once the new core is established.
	public class EmailFolder //: IValueObject
	{
		private int _memberFolderID = Matchnet.Constants.NULL_INT;
		private int _memberID = Matchnet.Constants.NULL_INT;
		private int _domainID = Matchnet.Constants.NULL_INT;
		private string _description = Matchnet.Constants.NULL_STRING;
		private bool _systemFolder = false;
		private int _totalMessages = Matchnet.Constants.NULL_INT;
		private int _unreadMessages = Matchnet.Constants.NULL_INT;
		private int _displayKBytes = Matchnet.Constants.NULL_INT;
		private int _totalDisplayKBytes = Matchnet.Constants.NULL_INT;

		public int MemberFolderID
		{
			get{return _memberFolderID;}
			set{_memberFolderID = value;}
		}

		public int MemberID
		{
			get{return _memberID;}
			set{_memberID = value;}
		}

		public int DomainID
		{
			get{return _domainID;}
			set{_domainID = value;}
		}

		public string Description
		{
			get{return _description;}
			set{_description = value;}
		}

		public bool SystemFolder
		{
			get{return _systemFolder;}
			set{_systemFolder = value;}
		}

		public int TotalMessages
		{
			get{return _totalMessages;}
			set{_totalMessages = value;}
		}

		public int UnreadMessages
		{
			get{return _unreadMessages;}
			set{_unreadMessages = value;}
		}

		public int DisplayKBytes
		{
			get{return _displayKBytes;}
			set{_displayKBytes = value;}
		}

		public int TotalDisplayKBytes
		{
			get{return _totalDisplayKBytes;}
			set{_totalDisplayKBytes = value;}
		}

		public EmailFolder(int memberFolderID, int memberID, int domainID, string description,
			bool systemFolder, int totalMessages, int unreadMessages, int displayKBytes) 
		{
			_memberFolderID = memberFolderID;
			_memberID = MemberID;
			_domainID = domainID;
			_description = description;
			_systemFolder = systemFolder;
			_totalMessages = totalMessages;
			_unreadMessages = unreadMessages;
			_displayKBytes = displayKBytes;
			_totalDisplayKBytes = TotalDisplayKBytes;
		}

		public EmailFolder()
		{
		}
	}
}
