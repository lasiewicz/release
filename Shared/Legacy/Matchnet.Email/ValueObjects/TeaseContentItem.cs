using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for TeaseContentItem.
	/// </summary>
	public class TeaseContentItem
	{
		#region Static variables

		#endregion

		#region Class members

		private int _teaseID;
		private string _content;

		// ACCESSORS

		public int TeaseID
		{
			get
			{
				return this._teaseID;
			}
		}
		
		public string Content
		{
			get
			{
				return this._content;
			}
		}

		#endregion

		#region Public methods

		public TeaseContentItem(int teaseID, string content)
		{
			this._teaseID = teaseID;
			this._content = content;
		}

		#endregion

		#region Protected methods

		#endregion

		#region Private methods

		#endregion

		#region Static methods

		#endregion

		#region Test container

		#endregion
	}
}
