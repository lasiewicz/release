using Matchnet.Lib;

using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// EmailMessage represents a single email message.
	/// </summary>
	// EmailMessage should implement IValueObject once the new core is established.
	public class EmailMessage //: IValueObject
	{
		private int _memberMailID = Matchnet.Constants.NULL_INT;
		private Message.MAIL_TYPE _mailTypeID = Message.MAIL_TYPE.EMAIL;
		private int _toMemberID = Matchnet.Constants.NULL_INT;
		private string _toUserName = Matchnet.Constants.NULL_STRING;
		private int _fromMemberID = Matchnet.Constants.NULL_INT;
		private string _fromUserName = Matchnet.Constants.NULL_STRING;
		private int _statusMask = Matchnet.Constants.NULL_INT;
		private DateTime _insertDate;
		private DateTime _openDate;
		private string _subject = Matchnet.Constants.NULL_STRING;
		private int _displayKBytes = Matchnet.Constants.NULL_INT;
		private int _resourceID = Matchnet.Constants.NULL_INT;
		private int _fileID = Matchnet.Constants.NULL_INT;
		private int _languageID = Matchnet.Constants.NULL_INT;
		private string _content = Matchnet.Constants.NULL_STRING;
		private int _memberFolderID = Matchnet.Constants.NULL_INT;
		
		public int MemberMailID
		{
			get{return _memberMailID;}
			set{_memberMailID = value;}
		}

		public Message.MAIL_TYPE MailTypeID 
		{
			get{return _mailTypeID;}
			set{_mailTypeID = value;}
		}

		public int ToMemberID 
		{
			get{return _toMemberID;}
			set{_toMemberID = value;}
		}

		public string ToUserName
		{
			get{return _toUserName;}
			set{_toUserName = value;}
		}

		public int FromMemberID
		{
			get{return _fromMemberID;}
			set{_fromMemberID = value;}
		}
		
		public string FromUserName
		{
			get{return _fromUserName;}
			set{_fromUserName = value;}
		}

		public int StatusMask
		{
			get{return _statusMask;}
			set{_statusMask = value;}
		}

		public DateTime InsertDate
		{
			get{return _insertDate;}
			set{_insertDate = value;}
		}

		public DateTime OpenDate
		{
			get{return _openDate;}
			set{_openDate = value;}
		}

		public string Subject
		{
			get{return _subject;}
			set{_subject = value;}
		}

		public int DisplayKBytes
		{
			get{return _displayKBytes;}
			set{_displayKBytes = value;}
		}

		public int ResourceID
		{
			get{return _resourceID;}
			set{_resourceID = value;}
		}

		public int FileID 
		{
			get{return _fileID;}
			set{_fileID = value;}
		}

		public int LanguageID
		{
			get{return _languageID;}
			set{_languageID = value;}
		}

		public string Content 
		{
			get{return _content;}
			set{_content = value;}
		}

		public int MemberFolderID 
		{
			get{return _memberFolderID;}
			set{_memberFolderID = value;}
		}

		public EmailMessage(int memberMailID, Message.MAIL_TYPE mailTypeID, int toMemberID, string toUserName,
			int fromMemberID, string fromUserName, int statusMask, DateTime insertDate, DateTime openDate,
			string subject, int displayKBytes, int resourceID, int fileID, int languageID, string content,
			int memberFolderID) 
		{
			_memberMailID = memberMailID;
			_mailTypeID = mailTypeID;
			_toMemberID = toMemberID;
			_toUserName = toUserName;
			_fromMemberID = fromMemberID;
			_fromUserName = fromUserName;
			_statusMask = statusMask;
			_insertDate = insertDate;
			_openDate = openDate;
			_subject = subject;
			_displayKBytes = displayKBytes;
			_resourceID = resourceID;
			_fileID = fileID;
			_languageID = languageID;
			_content = content;
			_memberFolderID = memberFolderID;
		}

		public EmailMessage()
		{
		}
	}
}
