using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for TeaseCategoryItem.
	/// </summary>
	public class TeaseCategoryItem
	{
		#region Static variables

		#endregion

		#region Class members

		private int _teaseCategoryID;
		private string _content;

		// ACCESSORS

		public int TeaseCategoryID
		{
			get
			{
				return this._teaseCategoryID;
			}
		}
		
		public string Content
		{
			get
			{
				return this._content;
			}
		}

		#endregion

		#region Public methods

		public TeaseCategoryItem(int teaseCategoryID, string content)
		{
			this._teaseCategoryID	= teaseCategoryID;
			this._content			= content;
		}

		#endregion

		#region Protected methods

		#endregion

		#region Private methods

		#endregion

		#region Static methods

		#endregion

		#region Test container

		#endregion
	}
}
