using Matchnet.DataTemp;
using Matchnet.Email.ValueObjects;
using Matchnet.File;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Util;

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace Matchnet.Email
{
	/// <summary>
	/// Singleton Email Message service.
	/// </summary>
	public class Message
	{
		public enum DIRECTION
		{
			NONE = 0,
			PREVIOUS = 1,
			NEXT = 2
		}

		public enum MESSAGE_STATUS
		{
			NOTHING = 0,
			READ = 1,
			REPLIED = 2,
			SENT = 4
		}

		public enum MAIL_TYPE 
		{
			EMAIL = 1,
			TEASE = 2,
			MISSES_IM = 3,
			SYSTEM_ANNOUNCEMENT = 4,
			DECLINE_EMAIL = 5,
			IGNORE_EMAIL = 6,
			YES_EMAIL = 7,
			PRIVATE_PHOTO_MAIL = 8
		}

		private const int MAXIMUM_EMAIL_CONTENT_SIZE_BYTES = 1000;
		private const string MAXIMUM_EMAIL_FOLDER_DESCRIPTION = "20";
		private const string EMAIL_DATABASE_FILE = "mnMember";
		private const string SEND_MESSAGE_STORED_PROCEDURE = "up_MemberMail_Save";
		private const string RETRIEVE_MESSAGE_STORED_PROCEDURE = "up_MemberMail_List";
		private const string RETRIEVE_MESSAGES_STORED_PROCEDURE = "up_MemberMail_Folder_List";
		private const string SET_REPLIED_BIT_STORED_PROCEDURE = "up_MemberMail_ReplyStatus_Save";
		private const string MOVE_MESSAGES_TO_FOLDER_STORED_PROCEDURE = "up_MemberMail_MoveToFolder";
		private const string IGNORE_MESSAGES_STORED_PROCEDURE = "up_MemberMail_Ignore";
		private const string MARK_READ_MESSAGES_STORED_PROCEDURE = "up_MemberMail_MarkRead";

		private static Message instance = new Message();

		// handles building and sending(storing) an email message
		public bool sendMessage(Message.MAIL_TYPE mailTypeID, int domainID, int fromMemberID, string fromUserName,
			int toMemberID, string toUserName, string subject, string content, int resourceID,
			DateTime parentInsertDate, int memberMailID, int languageID) 
		{
			EmailMessage aMessage = null;
			int contentSize = 0;

			try 
			{
				if(content == null) 
				{
					return false;
				} 
				else 
				{
					contentSize = content.Length;
				}

				aMessage = new EmailMessage();
				aMessage.MailTypeID = mailTypeID;
				aMessage.FromMemberID = fromMemberID;
				aMessage.FromUserName = fromUserName;
				aMessage.ToMemberID = toMemberID;
				aMessage.ToUserName = toUserName;
				aMessage.Subject = subject;
				aMessage.ResourceID = resourceID;
				aMessage.MemberMailID = memberMailID;
				aMessage.LanguageID = languageID;

				return sendMessage(aMessage,0,0,false, false);
			}
			catch(Exception encounteredException) 
			{
				throw new MatchnetException("Error Sending Message", "sendMessage", encounteredException);
			}
			
		}

		// handles sending(storing) a composed message
		public bool sendMessage(EmailMessage aMessage, int domainID, int repliedMemberMailID, bool saveInSent,
			bool saveAsDraft) 
		{
			Matchnet.DataTemp.SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;
			FileSaveResult referenceFileResult = null;
			int memberFolderID = (int)Folder.SYSTEM_TABLES.SENT;
			int contentSize = 0;

			try 
			{
				aSqlDescriptor = new SQLDescriptor(EMAIL_DATABASE_FILE, aMessage.FromMemberID);
				client = new Matchnet.DataTemp.SQLClient(aSqlDescriptor);

				aMessage.StatusMask = 0;
				contentSize = aMessage.Content.Length;

				// if the content of the message exceeds the predetermined content size constraint,
				// create a separate file record for needed storage
				if(contentSize > MAXIMUM_EMAIL_CONTENT_SIZE_BYTES) 
				{
					referenceFileResult = FileBL.Instance.Save(aMessage.Content);
					if(referenceFileResult.Success) 
					{
						aMessage.FileID = referenceFileResult.File.FileID;
					} 
					else 
					{
						aMessage.ResourceID = referenceFileResult.ResourceID;
					}
					aMessage.Content = null;
				}
				
				if(repliedMemberMailID > 0) 
				{
					setRepliedBit(aMessage.FromMemberID, repliedMemberMailID);
				}

				if(saveAsDraft) 
				{
					memberFolderID = (int)Folder.SYSTEM_TABLES.DRAFT;
				}

				if(saveInSent) 
				{
					aMessage.StatusMask = (aMessage.StatusMask | (int)MESSAGE_STATUS.SENT);
				}

				client.AddParameter("@MailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)aMessage.MailTypeID);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
				client.AddParameter("@FromMemberID", SqlDbType.Int, ParameterDirection.Input, aMessage.FromMemberID);
				client.AddParameter("@ToMemberID", SqlDbType.Int, ParameterDirection.Input, aMessage.ToMemberID);
				client.AddParameter("@FromUserName", SqlDbType.VarChar, ParameterDirection.Input, aMessage.FromUserName);
				client.AddParameter("@ToUserName", SqlDbType.VarChar, ParameterDirection.Input, aMessage.ToUserName);
				client.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, memberFolderID);
				client.AddParameter("@NumBytes", SqlDbType.Int, ParameterDirection.Input, contentSize);
				client.AddParameter("@Subject", SqlDbType.VarChar, ParameterDirection.Input, aMessage.Subject);
				client.AddParameter("@Content", SqlDbType.VarChar, ParameterDirection.Input, aMessage.Content);
				client.AddParameter("@StatusMask", SqlDbType.Int, ParameterDirection.Input, aMessage.StatusMask);
				
				// TODO: Determine what ParentInsertDate is utilized for and resolve where to place the parameter
				client.AddParameter("@ParentInsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
				if(aMessage.ResourceID > 0) 
				{
					client.AddParameter("@ResourceID", SqlDbType.Int, ParameterDirection.Input, aMessage.ResourceID);
				} 
				else 
				{
					client.AddParameter("@ResourceID", SqlDbType.Int, ParameterDirection.Input, null);
				}
				if(aMessage.FileID > 0) 
				{
					client.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, aMessage.FileID);
				} 
				else 
				{
					client.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, null);
				}
				if(aMessage.MemberMailID > 0) 
				{
					client.AddParameter("@MemberMailID", SqlDbType.Int, ParameterDirection.Input, aMessage.MemberMailID);
				} 
				else 
				{
					client.AddParameter("@MemberMailID", SqlDbType.Int, ParameterDirection.Input, null);
				}

				if(aMessage.LanguageID > 0) 
				{
					client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, aMessage.LanguageID);
				} 
				else 
				{
					client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, null);
				}
			
				client.ExecuteNonQuery(SEND_MESSAGE_STORED_PROCEDURE, CommandType.StoredProcedure);

				return true;
			} 
			catch (Exception encounteredException) 
			{
				throw new MatchnetException("Error Sending Message", "sendMessage", encounteredException);
			}
		}

		// handles pulling the specified member's folder's message(s)
		public ICollection retrieveMessages(int memberID, int domainID, int memberFolderID, int startRow, int pageSize, string orderBy) 
		{
			ArrayList foundMessages = new ArrayList();
			EmailMessage aMessage = null;
			Matchnet.DataTemp.SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;
			DataTable retrievedRecords = null;
			DataRow currentRow = null;
			IEnumerator currentMessageEnumerator = null;
			
			try
			{
				aSqlDescriptor = new SQLDescriptor(EMAIL_DATABASE_FILE, memberID);
				client = new Matchnet.DataTemp.SQLClient(aSqlDescriptor);
				
				client.AddParameter("@MemberID",SqlDbType.Int, ParameterDirection.Input, memberID);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
				client.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, memberFolderID);
				client.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
				client.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
				client.AddParameter("@OrderBy", SqlDbType.VarChar, ParameterDirection.Input, orderBy);
				client.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output);
				client.AddParameter("@FolderDescription", SqlDbType.VarChar, ParameterDirection.Output, MAXIMUM_EMAIL_FOLDER_DESCRIPTION);
			
				retrievedRecords = client.GetDataTable(RETRIEVE_MESSAGES_STORED_PROCEDURE, CommandType.StoredProcedure);

				if(retrievedRecords.Rows.Count > 0) 
				{
					currentMessageEnumerator = retrievedRecords.Rows.GetEnumerator();
					while(currentMessageEnumerator.MoveNext()) 
					{
						currentRow = (DataRow)currentMessageEnumerator.Current;
						aMessage = buildMessage(currentRow);
						aMessage.MemberFolderID = memberFolderID;

						if(aMessage != null) 
						{
							foundMessages.Add(aMessage);
						}
					}
				}
				return foundMessages;
			}
			catch (Exception anException) 
			{
				throw new MatchnetException("Exception Encountered during Message Retrieval", "retrieveMessages", anException);
			}
		}

		// handles pulling the specified member's message(s)
		public EmailMessage retrieveMessage(int memberID, int memberMailID, string orderBy, 
			DIRECTION messageDirection, bool updateReadFlag) 
		{
			EmailMessage aMessage = null;
			Matchnet.DataTemp.SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;
			DataTable retrievedRecord = null;
			Matchnet.File.File messageFile = null;
			System.IO.TextReader contentReader = null;
			System.Net.WebClient fileRetriever = null;

			try 
			{
				aMessage = new EmailMessage();
				aSqlDescriptor = new SQLDescriptor(EMAIL_DATABASE_FILE, memberID);
				client = new Matchnet.DataTemp.SQLClient(aSqlDescriptor);
				
				client.AddParameter("@MemberID",SqlDbType.Int, ParameterDirection.Input, memberID);
				client.AddParameter("@MemberMailID", SqlDbType.Int, ParameterDirection.Input, memberMailID);
				client.AddParameter("@OrderBy", SqlDbType.VarChar, ParameterDirection.Input, orderBy);
				client.AddParameter("@Direction", SqlDbType.Char, ParameterDirection.Input, resolveDirection(messageDirection));
				client.AddParameter("@UpdateReadFlag", SqlDbType.Bit, ParameterDirection.Input, updateReadFlag);
			
				retrievedRecord = client.GetDataTable(RETRIEVE_MESSAGE_STORED_PROCEDURE, CommandType.StoredProcedure);
			
				// determine there was a mail record retrieved and build out the EmailMessage object
				if(retrievedRecord.Rows.Count > 0) 
				{
					aMessage.MemberMailID = int.Parse(retrievedRecord.Rows[0]["MemberMailID"].ToString());
					aMessage.MailTypeID = (Message.MAIL_TYPE)int.Parse(retrievedRecord.Rows[0]["MailTypeID"].ToString());
					aMessage.ToMemberID = int.Parse(retrievedRecord.Rows[0]["ToMemberID"].ToString());
					aMessage.ToUserName = retrievedRecord.Rows[0]["ToUserName"].ToString();
					aMessage.FromMemberID = int.Parse(retrievedRecord.Rows[0]["FromMemberID"].ToString());
					aMessage.FromUserName = retrievedRecord.Rows[0]["FromUserName"].ToString();
					if(retrievedRecord.Rows[0]["StatusMask"] != null && retrievedRecord.Rows[0]["StatusMask"] != DBNull.Value) 
					{
						aMessage.StatusMask = int.Parse(retrievedRecord.Rows[0]["StatusMask"].ToString());
					}
					if(retrievedRecord.Rows[0]["InsertDate"] != null && retrievedRecord.Rows[0]["InsertDate"] != DBNull.Value) 
					{
						aMessage.InsertDate = DateTime.Parse(retrievedRecord.Rows[0]["InsertDate"].ToString());
					}
					if(retrievedRecord.Rows[0]["OpenDate"] != null && retrievedRecord.Rows[0]["OpenDate"] != DBNull.Value) 
					{
						aMessage.OpenDate = DateTime.Parse(retrievedRecord.Rows[0]["OpenDate"].ToString());
					}
					if(retrievedRecord.Rows[0]["Subject"] != null && retrievedRecord.Rows[0]["Subject"] != DBNull.Value) 
					{
						aMessage.Subject = retrievedRecord.Rows[0]["Subject"].ToString();
					}
					if(retrievedRecord.Rows[0]["DisplayKBytes"] != null && retrievedRecord.Rows[0]["DisplayKBytes"] != DBNull.Value) 
					{
						aMessage.DisplayKBytes = int.Parse(retrievedRecord.Rows[0]["DisplayKBytes"].ToString());
					}
					if(retrievedRecord.Rows[0]["ResourceID"] != null && retrievedRecord.Rows[0]["ResourceID"] != DBNull.Value) 
					{
						aMessage.ResourceID = int.Parse(retrievedRecord.Rows[0]["ResourceID"].ToString());
					}
					if(retrievedRecord.Rows[0]["LanguageID"] != null && retrievedRecord.Rows[0]["LanguageID"] != DBNull.Value) 
					{
						aMessage.LanguageID = int.Parse(retrievedRecord.Rows[0]["LanguageID"].ToString());
					}
					if(retrievedRecord.Rows[0]["MemberFolderID"] != null && retrievedRecord.Rows[0]["MemberFolderID"] != DBNull.Value) 
					{
						aMessage.MemberFolderID = int.Parse(retrievedRecord.Rows[0]["MemberFolderID"].ToString());
					}

					// check to see if there is an associated File record for this mail record
					if((retrievedRecord.Rows[0]["FileID"] == null) || (retrievedRecord.Rows[0]["FileID"] == DBNull.Value) || (int.Parse(retrievedRecord.Rows[0]["FileID"].ToString()) == 0)) 
					{
						aMessage.Content = retrievedRecord.Rows[0]["Content"].ToString();
					} 
					else 
					{
						aMessage.FileID = int.Parse(retrievedRecord.Rows[0]["FileID"].ToString());
						messageFile = FileBL.Instance.Load(aMessage.FileID);
						
						fileRetriever = new System.Net.WebClient();
						contentReader = new System.IO.StreamReader(fileRetriever.OpenRead(messageFile.WebPath));
						if(contentReader != null)
						{
							aMessage.Content = contentReader.ReadToEnd();
						}
					}
				}
			
				return aMessage;
			} 
			catch (Exception anException) 
			{
				throw new MatchnetException("Error Retrieving Message", "retrieveMessage", anException);
			} 
			finally 
			{
				if(contentReader != null) 
				{
					contentReader.Close();
				}
			}
		}

		// handles setting the specified message(s) to unread or read
		public bool markRead(int memberID, bool read, string memberMailIDs) 
		{
			SqlCommand command = null;
			Matchnet.DataTemp.SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;
			
			try
			{
				if(memberMailIDs != null && memberMailIDs.Length > 0) 
				{
					aSqlDescriptor = new SQLDescriptor(EMAIL_DATABASE_FILE, memberID);
					client = new Matchnet.DataTemp.SQLClient(aSqlDescriptor);
					
					client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
					client.AddParameter("@ReadFlag", SqlDbType.Bit, ParameterDirection.Input, read);
					client.AddParameter("@MemberMailIDs", SqlDbType.VarChar, ParameterDirection.Input, memberMailIDs);
					command = new SqlCommand(MARK_READ_MESSAGES_STORED_PROCEDURE);
					command.CommandType = CommandType.StoredProcedure;
					client.PopulateCommand(command);
					client.ExecuteNonQuery(command);

					return true;
				}
			}
			catch(Exception anException) 
			{
				throw new MatchnetException("Error marking messages read.", "markRead", anException);
			}
			return false;
		}

		// handles ignoring specified member's message(s)
		public bool ignoreMessage(int memberID, int domainID, string memberMailIDs) 
		{
			SqlCommand command = null;
			Matchnet.DataTemp.SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;
			int returnValue = 0;

			try
			{
				if(memberMailIDs != null && memberMailIDs.Length > 0) 
				{
					aSqlDescriptor = new SQLDescriptor(EMAIL_DATABASE_FILE, memberID);
					client = new Matchnet.DataTemp.SQLClient(aSqlDescriptor);
					
					client.AddParameter("@ReturnValue",SqlDbType.Int, ParameterDirection.ReturnValue, returnValue);
					client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
					client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
					client.AddParameter("@MemberMailIDs", SqlDbType.VarChar, ParameterDirection.Input, memberMailIDs);
					command = new SqlCommand(IGNORE_MESSAGES_STORED_PROCEDURE);
					command.CommandType = CommandType.StoredProcedure;
					client.PopulateCommand(command);
					client.ExecuteNonQuery(command);
					returnValue = Convert.ToInt32(command.Parameters["@ReturnValue"].Value);
					if(returnValue == 0) 
					{
						return true;
					} 
					else 
					{
						return false;
					}
				}
			}
			catch(Exception anException) 
			{
				throw new MatchnetException("Error Ignoring Message", "deleteMessage", anException);
			}
			return false;
		}

		// handles moving all specified member messages to the specified member folder
		public bool moveToFolder(int memberID, string memberMailIDs, int memberFolderID) 
		{
			SqlCommand command = null;
			Matchnet.DataTemp.SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;
			int returnValue = 0;

			try
			{
				if(memberMailIDs != null && memberMailIDs.Length > 0 && memberFolderID > 0) 
				{
					aSqlDescriptor = new SQLDescriptor(EMAIL_DATABASE_FILE, memberID);
					client = new Matchnet.DataTemp.SQLClient(aSqlDescriptor);
					
					client.AddParameter("@ReturnValue",SqlDbType.Int, ParameterDirection.ReturnValue, returnValue);
					client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
					client.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, memberFolderID);
					client.AddParameter("@MemberMailIDs", SqlDbType.VarChar, ParameterDirection.Input, memberMailIDs);
					command = new SqlCommand(MOVE_MESSAGES_TO_FOLDER_STORED_PROCEDURE);
					command.CommandType = CommandType.StoredProcedure;
					client.PopulateCommand(command);
					client.ExecuteNonQuery(command);
					returnValue = Convert.ToInt32(command.Parameters["@ReturnValue"].Value);
					if(returnValue == 0) 
					{
						return true;
					} 
					else 
					{
						return false;
					}
				}
			}
			catch(Exception anException) 
			{
				throw new MatchnetException("Error Moving Message to Folder", "deleteMessage", anException);
			}
			return false;
		}

		// move specified message(s) to the trash
		public bool deleteMessage(int memberID, string memberMailIDs) 
		{
			return moveToFolder(memberID, memberMailIDs, (int)Folder.SYSTEM_TABLES.TRASH);
		}

		// build out the message from the given datarow
		private EmailMessage buildMessage(DataRow currentRow) 
		{
			EmailMessage currentMessage = null;

			try 
			{
				if(currentRow != null) 
				{
					currentMessage = new EmailMessage();
					currentMessage.MailTypeID = (Message.MAIL_TYPE)int.Parse(currentRow["MailTypeID"].ToString());
					currentMessage.MemberMailID = int.Parse(currentRow["MemberMailID"].ToString());
					currentMessage.FromMemberID = int.Parse(currentRow["FromMemberID"].ToString());
					currentMessage.FromUserName = currentRow["FromUserName"].ToString();
					currentMessage.ToMemberID = int.Parse(currentRow["ToMemberID"].ToString());
					currentMessage.ToUserName = currentRow["ToUserName"].ToString();
					currentMessage.StatusMask = int.Parse(currentRow["StatusMask"].ToString());
					currentMessage.InsertDate = DateTime.Parse(currentRow["InsertDate"].ToString());
					currentMessage.Subject = currentRow["Subject"].ToString();
					currentMessage.DisplayKBytes = int.Parse(currentRow["DisplayKBytes"].ToString());
					if(currentRow["OpenDate"] != DBNull.Value) 
					{
						currentMessage.OpenDate = DateTime.Parse(currentRow["OpenDate"].ToString());
					}
					if(currentRow["LanguageID"] != DBNull.Value) 
					{
						currentMessage.LanguageID = int.Parse(currentRow["LanguageID"].ToString());
					}
					
					return currentMessage;
				}

				return null;
			} 
			catch (Exception anException) 
			{
				throw new MatchnetException("Exception Encountered during Message Build Out", "buildMessage", anException);
			}
		}

		// Indicates whether or not the specified member has any new email messages
		public bool hasNewMail(int memberID, int domainID)
		{
			SQLDescriptor aSqlDescriptor = new SQLDescriptor(EMAIL_DATABASE_FILE, memberID);
			Matchnet.DataTemp.SQLClient client = new Matchnet.DataTemp.SQLClient(aSqlDescriptor);

			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);

			SqlCommand cmd = new SqlCommand("up_MemberMail_HasNewMail");
			cmd.CommandType = CommandType.StoredProcedure;
			client.PopulateCommand(cmd);

			DataTable table = client.GetDataTable(cmd);

			if (table != null && table.Rows.Count > 0)
			{
				if (Util.CBool(table.Rows[0]["HasNewMail"], false) == true)
				{
					return true;
				}
			}
			
			return false;
		}

		// Handles setting the replied bit for the Member/MemberMail specified
		private bool setRepliedBit(int memberID, int memberMailID) 
		{
			Matchnet.DataTemp.SQLClient client = null;
			SQLDescriptor aSqlDescriptor = null;

			try
			{
				aSqlDescriptor = new SQLDescriptor(EMAIL_DATABASE_FILE, memberID);
				client = new Matchnet.DataTemp.SQLClient(aSqlDescriptor);
			
				client.AddParameter("@MemberID",SqlDbType.Int, ParameterDirection.Input, memberID);
				client.AddParameter("@MemberMailID", SqlDbType.Int, ParameterDirection.Input, memberMailID);

				client.ExecuteNonQuery(SET_REPLIED_BIT_STORED_PROCEDURE, CommandType.StoredProcedure);

				// TODO: Verify return value...
				return true;
			} 
			catch (Exception encounteredException) 
			{
				throw new MatchnetException("Error Setting Message Reply", "setRepliedBit", encounteredException);
			}
		}

		// handles forming the message direction enumeration to their understood format
		private string resolveDirection(DIRECTION messageDirection) 
		{
			switch (messageDirection)
			{
				case DIRECTION.PREVIOUS:
					return "P";
				case DIRECTION.NEXT:
					return "N";
				default:
					// NONE direction is default
					return null;
			}
		}

		public static Message getInstance() 
		{
			return instance;
		}

		private Message()
		{
		}
	}
}
