using System;

namespace Matchnet.Email.Classification
{

	/// <summary>
	/// 
	/// </summary>
	public sealed class Constants
	{

		#region ASSEMBLY RULES
		public enum SendFilter_DomainOptOutStrictness
		{
			ExcludeDomainOptOuts = 0,
			IncludeCurrentDomainOptOut = 1,
			IncludeAnyDomainOptOut = 2
		}

		public enum SendFilter_GlobalOptOutStrictness
		{
			ExcludeGlobalOptOut = 0,
			IncludeGlobalOptOut = 1
		}

		public enum SendFilter_Result
		{
			MemberDataNotFound = -1,
			DoNotSendEmail = 0,
			SendEmail = 1,
		}
		#endregion

		#region GLOBAL STATUS MASK
		// global status mask settings
		// these mark some of the factors in regards to members receiving emails
		/// <summary>
		/// Value used to represent the GlobalStatusMask AttributeID in mnShared.Attribute
		/// </summary>
		public const int GLOBALSTATUSMASK_ATTRUBUTEID = 268;

		/// <summary>
		/// Value used to represent the GlobalStatusMask bit that will be turned on if the user is AdminSuspended
		/// </summary>
		public const int GLOBALSTATUSMASK_ADMINSUSPENDED = 1;

		/// <summary>
		/// Value used to represent the GlobalStatusMask bit that will be turned on if the user is BadEmailSuspended
		/// </summary>
		public const int GLOBALSTATUSMASK_BADEMAILSUSPENDED = 2;

		/// <summary>
		/// Value used to represent the GlobalStatusMask bit that will be turned on if the user's email has been verified
		/// </summary>
		public const int GLOBALSTATUSMASK_EMAILISVERIFIED = 4;
		#endregion

		#region EMAIL OPT OUT SETTINGS
		// Email Opt Out settings
		// these mark some of the factors in regards to members receiving emails

		/// <summary>
		/// Value used to represent the AttributeGroupID in mnShared.AttributeGroup
		/// </summary>
		public const int EMAILOPTOUT_ATTRIBUTEGROUPID = 44;

		/// <summary>
		/// Value used to represent the MemberEmailOptOutMask AttributeID in mnShared.Attribute
		/// </summary>
		public const int EMAILOPTOUT_GLOBAL_ATTRUBUTEID = 488;

		public const string MEMBEREMAIL_OPTOUT_MASK = "MemberEmailOptOutMask";

		/// <summary>
		/// Values contained by MemberEmailOptOutMask attribute that have global and domain specific email opt out settings
		/// </summary>
		public enum MemberEmailOptOutMask
		{
			NoOptOutSetting = 0,
			GLOBAL = 1,
			AmericanSingles = 2,
			Glimpse = 4,
			Jdate = 8,
			Facelink = 16,
			Cupid = 32,
			College = 64
		}

		public enum DomainIDMapping
		{
			AmericanSingles = 1,
			Glimpse = 2,
			JDate = 3,
			FaceLink = 9,
			Cupid = 10,
			College = 12
		}
		#endregion



		#region STORED PROCEDURES
		public enum BitwiseOperations
		{
			BITWISE_CLEAR = 100,
			BITWISE_AND = 101,
			BITWISE_OR = 103
		}

		internal enum PROCDEF_up_MemberAttribute_List
		{
			AttributeID = 0,
			AttributeTypeID = 1,
			AttributeName = 2,
			Value = 3,
			StatusMask = 4
		}

		internal enum PROCDEF_up_MemberDomain_List
		{
			MemberID = 0,
			DomainID = 1,
			PrivateLabelID = 2
		}

		#endregion
	}
}
