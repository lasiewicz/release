using System;

namespace Matchnet.Email.Classification
{
	public sealed class EmailConstants
	{
		#region GLOBAL STATUS MASK
		// global status mask settings
		// these mark some of the factors in regards to members receiving emails
		/// <summary>
		/// Value used to represent the GlobalStatusMask AttributeID in mnShared.Attribute
		/// </summary>
		public const int GLOBALSTATUSMASK_ATTRUBUTEID = 268;

		/// <summary>
		/// Value used to represent the GlobalStatusMask bit that will be turned on if the user is AdminSuspended
		/// </summary>
		public const int GLOBALSTATUSMASK_ADMINSUSPENDED = 1;

		/// <summary>
		/// Value used to represent the GlobalStatusMask bit that will be turned on if the user is BadEmailSuspended
		/// </summary>
		public const int GLOBALSTATUSMASK_BADEMAILSUSPENDED = 2;

		/// <summary>
		/// Value used to represent the GlobalStatusMask bit that will be turned on if the user's email has been verified
		/// </summary>
		public const int GLOBALSTATUSMASK_EMAILISVERIFIED = 4;
		#endregion

		#region STORED PROCEDURES
//		public enum BitwiseOperations
//		{
//			BITWISE_CLEAR = 100,
//			BITWISE_AND = 101,
//			BITWISE_OR = 103
//		}

		internal enum PROCDEF_up_MemberAttribute_List
		{
			AttributeID = 0,
			AttributeTypeID = 1,
			AttributeName = 2,
			Value = 3,
			StatusMask = 4
		}

		internal enum PROCDEF_up_MemberDomain_List
		{
			MemberID = 0,
			DomainID = 1,
			PrivateLabelID = 2
		}

		#endregion
	}
}
