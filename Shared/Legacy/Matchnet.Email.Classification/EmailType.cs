using System;

namespace Matchnet.Email.Classification
{
	public enum EmailType 
	{
		MatchNewsLetter = 1,
		MutualMail = 2,
		ViralNewsLetter = 3,
		NewMailAlert = 4,
		FreeTrial30 = 5,
		FreeTrial60 = 6,
		FreeTrial90 = 7,
		PremiumSubscriberWelcome = 8,
		CheckConfirmation = 9,
		ActivationLetter = 10,
		ForgotPassword = 11,
		PhotoAlbumRequest = 12,
		PhotoAlbumApprove = 13,
		SendToFriend = 14,
		CrossPromotion = 15,
		PhotoApproval = 16,
		ContactUs = 17,
		EventComments = 18,
		EmailChangeConfirmation = 19,
		Broadcast = 20
	}

	public class EmailTypeConverter 
	{
		public int ToInt(EmailType type) 
		{
			return (int)type;
		}
		public EmailType FromInt(int type) 
		{
			return (EmailType)type;
		}
	}
}