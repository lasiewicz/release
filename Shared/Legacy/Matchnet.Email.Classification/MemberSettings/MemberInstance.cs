using System;

using Matchnet.Lib;

namespace Matchnet.Email.Classification.MemberSettings
{
	/// <summary>
	/// MemberInstance is a value object of email-settings related member information
	/// </summary>
	public class MemberInstance
	{
		private string _memberEmailAddress =Matchnet.Constants.NULL_STRING;
		private int _memberID =Matchnet.Constants.NULL_INT;
		private int _basePrivateLabelID =Matchnet.Constants.NULL_INT;
		private int _globalStatusMask =Matchnet.Constants.NULL_INT;
		private bool _emailOptOut = false;
		private bool _memberOptOutSettingsSet = false;

		#region PUBLIC PROPERTIES
		public int MemberID
		{
			get{return _memberID;} 
			set{_memberID = value;}
		}

		public int BasePrivateLabelID
		{
			get{return _basePrivateLabelID;} 
			set
			{
				_basePrivateLabelID = value;
			}
		}

		public bool BasePrivateLabelEmailOptedOut
		{
			get{return _emailOptOut;}
			set
			{
				// TODO CRP - Get rid of this crap; need to make a way of knowing whether the global values were set or not
				_emailOptOut = value;
				_memberOptOutSettingsSet = true;
			}
		}

		public string EmailAddress
		{
			get{return _memberEmailAddress;}
			set{_memberEmailAddress = value;}
		}

		public int GlobalStatusMaskValue
		{
			get{return _globalStatusMask;}
			set
			{
				_globalStatusMask = value;
			}
		}

		public bool EmailIVerified
		{
			get
			{
				return (_globalStatusMask & EmailConstants.GLOBALSTATUSMASK_EMAILISVERIFIED) == EmailConstants.GLOBALSTATUSMASK_EMAILISVERIFIED;
			}
		}

		public bool BadEmaiSuspended
		{
			get
			{
				return (_globalStatusMask & EmailConstants.GLOBALSTATUSMASK_BADEMAILSUSPENDED) == EmailConstants.GLOBALSTATUSMASK_BADEMAILSUSPENDED;
			}
		}

		public bool EmailAdminSuspended
		{
			get
			{
				return (_globalStatusMask & EmailConstants.GLOBALSTATUSMASK_ADMINSUSPENDED) == EmailConstants.GLOBALSTATUSMASK_ADMINSUSPENDED;
			}
		}
		#endregion

		public MemberInstance()
		{
		}

		public bool IsValid()
		{
			// TODO - replace all instances in code of the 0's and -1's to value-specific MinValues and place in constants file
			if (_memberOptOutSettingsSet == true &&
				_basePrivateLabelID > 0 &&
				_globalStatusMask > -1 &&
				_memberID > -1)
			{
				return true;
			}

			return false;
		}
	}
}
