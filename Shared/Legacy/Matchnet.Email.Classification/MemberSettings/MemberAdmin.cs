using System;
using System.Collections;

namespace Matchnet.Email.Classification.MemberSettings
{
	/// <summary>
	/// MemberAdmin provides facade for methods which will get or set a Matchnet members email setting sin the database
	/// </summary>
	public class MemberAdmin
	{
		public MemberAdmin()
		{
		}

		public static MemberInstance GetMemberSettings(int memberID, int basePrivateLabelID)
		{
			try
			{
				return MemberProcessor.PopulateMember(memberID, basePrivateLabelID);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
	}
}
