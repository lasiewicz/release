using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib;
using Matchnet.Lib.Exceptions;
using Matchnet.DataTemp;

namespace Matchnet.Email.Classification.MemberSettings
{
	/// <summary>
	/// Member Information will be looked up regarding email settings
	/// </summary>
	internal class MemberProcessor
	{

		#region CONSTRUCTOR
		internal MemberProcessor()
		{
		}
		#endregion

		#region PUBLIC METHODS
		

		/// <summary>
		/// Looks up the Members MemberID by the Email Address provided
		/// </summary>
		internal static int GetMemberIDFromEmailAddress(string emailAddress)
		{
			try
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnODS");
				SQLClient sqlClient = new SQLClient(descriptor);
				sqlClient.AddParameter("@EmailAddress", SqlDbType.Int, ParameterDirection.Input, emailAddress, 192);
				sqlClient.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Output);

				SqlCommand cmd = new SqlCommand("up_MemberEmail_GetMemberIDByEmail");
				cmd.CommandType = CommandType.StoredProcedure;
				sqlClient.PopulateCommand(cmd);

				sqlClient.ExecuteNonQuery(cmd);
					
				// if the return value is null because member is not found with that email, then leave Message.MemberID the default
				if (cmd.Parameters["@MemberID"].Value != DBNull.Value)
				{
					return Convert.ToInt32(cmd.Parameters["@MemberID"].Value);
				}
			}
			catch (Exception ex)
			{
				// TODO Call appropriate exception handler
				throw new MatchnetException("Error retrieving MemberID from Email Address for user", "GetMemberIDFromEmailAddress", ex, false);
			}

			return Matchnet.Constants.NULL_INT;
		}

		/// <summary>
		/// Provide Lib.ConstantsTemp.NULL_x for any values that cannot be supplied
		/// Providing only memberID will require internal lookup for globalStatusMask (and domainID if applicable)
		/// Providing memberID and domainID will require internal lookup for globalStatusMask
		/// </summary>
		internal static MemberInstance PopulateMember(int memberID, int basePrivateLabelID)
		{
			MemberInstance member = new MemberInstance();
			member.MemberID = memberID;
			member.BasePrivateLabelID = basePrivateLabelID;
			
			if (member.MemberID > -1)
			{
				getEmailValues(ref member);
			}
			return member;
		}
		#endregion

		
		#region PRIVATE METHODS
		/// <summary>
		/// Gets the GlobalStatusMask and GlobalOptOut settings. Gets the DomainOptOut setting if CurrentDomainID is specified in MemberInstance
		/// Return true if an email settings have been set, else false if they not found
		/// This is because Every user should have these email settings 
		/// </summary>
		private static void getEmailValues(ref MemberInstance member)
		{
			try
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnMember", member.MemberID);
				SQLClient sqlClient = new SQLClient(descriptor);
				sqlClient.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, member.MemberID);
				sqlClient.AddParameter("@privateLabelID", SqlDbType.Int, ParameterDirection.Input, member.BasePrivateLabelID);

				// TODO - will be new proc call to the new proc in mnMember for getting GSM and Opt out setting
				DataTable dataTable = sqlClient.GetDataTable("up_MailOption_ListByMemberIDPrivateLabelID", CommandType.StoredProcedure);
			
				// only one row should be returned
				if (dataTable.Rows.Count > 0)
				{
					DataRow row =  dataTable.Rows[0];
					// If the opt out value has not been set then we will treat it as 0
					// because the user has not opted out
					// we do not want to update the value though because that will screw up reporting
					int basePrivateLabelOptedOut = (row["OptOutValue"] == DBNull.Value) ? 0 : Convert.ToInt32(row["OptOutValue"]);
					member.GlobalStatusMaskValue = (row["GlobalStatusMask"] == DBNull.Value) ? 0 : Convert.ToInt32(row["GlobalStatusMask"]);
					
					// do not set the opt out value if it is null or 
					switch (basePrivateLabelOptedOut)
					{
						case 1:
							member.BasePrivateLabelEmailOptedOut = true;
							break;
						default:
							member.BasePrivateLabelEmailOptedOut = false;
							break;
					}
				}
			}
			catch (Exception ex)
			{
				// TODO Call appropriate exception handler
				throw new MatchnetException("Error retrieving EmailOptOut or GlobalStatusMask setting for user", "getEmailValues", ex, false);
			}
		}
		#endregion
	}
}
