using System;

namespace Matchnet.Email.Classification.BehaviorFilter.FilterSettings
{
	/// <summary>
	/// Filter reults for sending emails will be contained here and passed back to the external calling assembly
	/// </summary>
	public class SendResults
	{
		private Constants.SendFilter_Result _sendFilterResult;

		public Constants.SendFilter_Result FilterResult
		{
			get{return _sendFilterResult;}
			set{_sendFilterResult = value;}
		}

	}
}
