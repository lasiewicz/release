using System;

namespace Matchnet.Email.Classification.BehaviorFilter.FilterSettings
{
	/// <summary>
	/// Any requirements that must be met for the message to be sent should be entered here.
	/// </summary>
	public class SendSettings
	{
		// defaulted to false because we should always prevent an email from being sent to a user if they dont want to receive emails from us.
		private bool _ignorePrivateLabelOptOut = false;
		private bool _adminSuspendFilterOn = true;
		private bool _badEmailSuspendFilterOn = true;
		// defaulted to false for send a friend type emails
		private bool _mustBeMatchnetMemberToReceiveEmail = false;

		/// <summary>
		/// IgnorePrivateLabelOptOut - if you want to send an email to a member from a private label even if they have opted out of receiving emails from that
		/// private label, set this to true
		/// Default is false
		/// </summary>
		public bool IgnorePrivateLabelOptOut
		{
			get{return _ignorePrivateLabelOptOut;}
			set{_ignorePrivateLabelOptOut = value;}
		}

		/// <summary>
		/// SendOnlyIfReceiverIsMatchnetMember - if you want to send an email to a user even if they do not show up in our database
		/// as a subscriber, then set this to true
		/// Default is false
		/// </summary>
		public bool SendOnlyIfReceiverIsMatchnetMember
		{
			get{return _mustBeMatchnetMemberToReceiveEmail;}
			set{_mustBeMatchnetMemberToReceiveEmail = value;}
		}

		/// <summary>
		/// AdminSuspenFilterOn - if you want to prevent the sending of emails to users when they are admin suspended, set this to true.
		/// Default is true
		/// </summary>
		public bool AdminSuspendFilterOn
		{
			get{return _adminSuspendFilterOn;}
			set{_adminSuspendFilterOn = value;}
		}

		/// <summary>
		/// BadEmailSuspendFilterOn - if you want to prevent the sending of emails to users when their email has been set to Bad Email due to bouncebacks or other
		/// , set this to true.
		/// Default is true
		/// </summary>
		public bool BadEmailSuspendFilterOn
		{
			get{return _badEmailSuspendFilterOn;}
			set{_badEmailSuspendFilterOn = value;}
		}
	}
}
