using System;

using Matchnet.Email.Classification.MemberSettings;

namespace Matchnet.Email.Classification.BehaviorFilter.FilterProcessor
{
	/// <summary>
	/// SendProcessor holds business rules for creating a SendResult Based on a members email settings and SendSettings passed in form the external assembly
	/// Send result is then used by the calling assembly to determine what to do with the email.
	/// </summary>
	internal struct SendFilter
	{
		internal static bool MemberCanReceiveEmail(MemberInstance member, FilterSettings.SendSettings filterSettings)
		{
			// if the member is not valid that means we did not find the correct info in the database for them.
			if (member.IsValid() == false && 
				filterSettings.SendOnlyIfReceiverIsMatchnetMember == true)
			{
				return false;
			}

			if (member.IsValid() == true)
			{
				// if the mail requires respecting the users private label opt out setting
				// and the user is opted out then return a false
				if (filterSettings.IgnorePrivateLabelOptOut == false && 
					member.BasePrivateLabelEmailOptedOut == true)
				{
					return false;
				}

				if (filterSettings.AdminSuspendFilterOn)
				{
					if (member.EmailAdminSuspended == true)
					{
						return false;
					}
				}

				if (filterSettings.BadEmailSuspendFilterOn)
				{
					if (member.BadEmaiSuspended == true)
					{
						return false;
					}
				}
			}

			return true;
		}

	}
}
