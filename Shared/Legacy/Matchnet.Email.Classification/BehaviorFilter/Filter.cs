using System;

using Matchnet.Lib.Exceptions;
using Matchnet.Email.Classification.BehaviorFilter.FilterSettings;
using Matchnet.Email.Classification.MemberSettings;

namespace Matchnet.Email.Classification.BehaviorFilter
{
	/// <summary>
	/// Master class for determining whether emails can behave in certain ways with members (be sent to, forwarded, replied...)
	/// </summary>
	public class Filter
	{
		#region PUBLIC METHODS - SEND
		/// <summary>
		/// If SendSettings are not specified then default settings will be used
		/// </summary>
		public static bool SendMailFilter(int memberID, int basePrivateLabelID)
		{
			SendSettings sendSettings = new SendSettings();
			return sendMailFilter(memberID, basePrivateLabelID, sendSettings);
		}
		public static bool SendMailFilter(int memberID, int basePrivateLabelID, SendSettings sendSettings)
		{
			return sendMailFilter(memberID, basePrivateLabelID, sendSettings);
		}
		#endregion


		#region PRIVATE METHODS - SEND
		private static bool sendMailFilter(int memberID, int basePrivateLabelID, SendSettings sendSettings)
		{
			MemberInstance member = MemberAdmin.GetMemberSettings(memberID, basePrivateLabelID);

			return sendMailFilter(member, sendSettings);
		}

		private static bool sendMailFilter(MemberInstance member, SendSettings sendSettings)
		{
			return FilterProcessor.SendFilter.MemberCanReceiveEmail(member, sendSettings);
		}
		#endregion
	}
}
