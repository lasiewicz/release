using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet.Email.Classification;

namespace Harness
{
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox gbPopulate;
		private System.Windows.Forms.GroupBox gbValidate;
		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.Label lblEmailClassificationID;
		private System.Windows.Forms.Label lblMemberID;
		private System.Windows.Forms.Label lblPrivateLabelID;
		private System.Windows.Forms.Label lblHeaderVal;
		private System.Windows.Forms.TextBox txtEmailClassificationID;
		private System.Windows.Forms.TextBox txtMemberID;
		private System.Windows.Forms.TextBox txtPrivateLabelID;
		private System.Windows.Forms.TextBox txtHeaderVal;
		private System.Windows.Forms.Button btnPopulate;
		private System.Windows.Forms.Button btnValidate;
		private System.Windows.Forms.Button btnHash;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbPopulate = new System.Windows.Forms.GroupBox();
			this.gbValidate = new System.Windows.Forms.GroupBox();
			this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.lblEmailClassificationID = new System.Windows.Forms.Label();
			this.lblMemberID = new System.Windows.Forms.Label();
			this.lblPrivateLabelID = new System.Windows.Forms.Label();
			this.lblHeaderVal = new System.Windows.Forms.Label();
			this.txtEmailClassificationID = new System.Windows.Forms.TextBox();
			this.txtMemberID = new System.Windows.Forms.TextBox();
			this.txtPrivateLabelID = new System.Windows.Forms.TextBox();
			this.txtHeaderVal = new System.Windows.Forms.TextBox();
			this.btnPopulate = new System.Windows.Forms.Button();
			this.btnValidate = new System.Windows.Forms.Button();
			this.btnHash = new System.Windows.Forms.Button();
			this.gbPopulate.SuspendLayout();
			this.gbValidate.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbPopulate
			// 
			this.gbPopulate.Controls.Add(this.btnPopulate);
			this.gbPopulate.Controls.Add(this.txtPrivateLabelID);
			this.gbPopulate.Controls.Add(this.txtMemberID);
			this.gbPopulate.Controls.Add(this.txtEmailClassificationID);
			this.gbPopulate.Controls.Add(this.lblPrivateLabelID);
			this.gbPopulate.Controls.Add(this.lblMemberID);
			this.gbPopulate.Controls.Add(this.lblEmailClassificationID);
			this.gbPopulate.Location = new System.Drawing.Point(8, 8);
			this.gbPopulate.Name = "gbPopulate";
			this.gbPopulate.Size = new System.Drawing.Size(280, 128);
			this.gbPopulate.TabIndex = 0;
			this.gbPopulate.TabStop = false;
			this.gbPopulate.Text = "Populate";
			// 
			// gbValidate
			// 
			this.gbValidate.Controls.Add(this.btnHash);
			this.gbValidate.Controls.Add(this.btnValidate);
			this.gbValidate.Controls.Add(this.txtHeaderVal);
			this.gbValidate.Controls.Add(this.lblHeaderVal);
			this.gbValidate.Location = new System.Drawing.Point(296, 8);
			this.gbValidate.Name = "gbValidate";
			this.gbValidate.Size = new System.Drawing.Size(280, 128);
			this.gbValidate.TabIndex = 1;
			this.gbValidate.TabStop = false;
			this.gbValidate.Text = "Validate";
			// 
			// txtOutput
			// 
			this.txtOutput.Location = new System.Drawing.Point(8, 144);
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(568, 320);
			this.txtOutput.TabIndex = 2;
			this.txtOutput.Text = "";
			// 
			// lblEmailClassificationID
			// 
			this.lblEmailClassificationID.Location = new System.Drawing.Point(8, 16);
			this.lblEmailClassificationID.Name = "lblEmailClassificationID";
			this.lblEmailClassificationID.Size = new System.Drawing.Size(136, 16);
			this.lblEmailClassificationID.TabIndex = 0;
			this.lblEmailClassificationID.Text = "Email Classification ID";
			this.lblEmailClassificationID.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblMemberID
			// 
			this.lblMemberID.Location = new System.Drawing.Point(8, 40);
			this.lblMemberID.Name = "lblMemberID";
			this.lblMemberID.Size = new System.Drawing.Size(136, 16);
			this.lblMemberID.TabIndex = 1;
			this.lblMemberID.Text = "Member ID";
			this.lblMemberID.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblPrivateLabelID
			// 
			this.lblPrivateLabelID.Location = new System.Drawing.Point(8, 64);
			this.lblPrivateLabelID.Name = "lblPrivateLabelID";
			this.lblPrivateLabelID.Size = new System.Drawing.Size(128, 16);
			this.lblPrivateLabelID.TabIndex = 2;
			this.lblPrivateLabelID.Text = "Private Label ID";
			this.lblPrivateLabelID.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblHeaderVal
			// 
			this.lblHeaderVal.Location = new System.Drawing.Point(8, 16);
			this.lblHeaderVal.Name = "lblHeaderVal";
			this.lblHeaderVal.Size = new System.Drawing.Size(64, 16);
			this.lblHeaderVal.TabIndex = 1;
			this.lblHeaderVal.Text = "Header Val";
			this.lblHeaderVal.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtEmailClassificationID
			// 
			this.txtEmailClassificationID.Location = new System.Drawing.Point(152, 16);
			this.txtEmailClassificationID.Name = "txtEmailClassificationID";
			this.txtEmailClassificationID.Size = new System.Drawing.Size(64, 20);
			this.txtEmailClassificationID.TabIndex = 3;
			this.txtEmailClassificationID.Text = "";
			// 
			// txtMemberID
			// 
			this.txtMemberID.Location = new System.Drawing.Point(152, 40);
			this.txtMemberID.Name = "txtMemberID";
			this.txtMemberID.Size = new System.Drawing.Size(64, 20);
			this.txtMemberID.TabIndex = 4;
			this.txtMemberID.Text = "";
			// 
			// txtPrivateLabelID
			// 
			this.txtPrivateLabelID.Location = new System.Drawing.Point(152, 64);
			this.txtPrivateLabelID.Name = "txtPrivateLabelID";
			this.txtPrivateLabelID.Size = new System.Drawing.Size(64, 20);
			this.txtPrivateLabelID.TabIndex = 5;
			this.txtPrivateLabelID.Text = "";
			// 
			// txtHeaderVal
			// 
			this.txtHeaderVal.Location = new System.Drawing.Point(80, 16);
			this.txtHeaderVal.Name = "txtHeaderVal";
			this.txtHeaderVal.Size = new System.Drawing.Size(192, 20);
			this.txtHeaderVal.TabIndex = 4;
			this.txtHeaderVal.Text = "";
			// 
			// btnPopulate
			// 
			this.btnPopulate.Location = new System.Drawing.Point(152, 88);
			this.btnPopulate.Name = "btnPopulate";
			this.btnPopulate.Size = new System.Drawing.Size(64, 20);
			this.btnPopulate.TabIndex = 6;
			this.btnPopulate.Text = "Populate";
			this.btnPopulate.Click += new System.EventHandler(this.btnPopulate_Click);
			// 
			// btnValidate
			// 
			this.btnValidate.Location = new System.Drawing.Point(80, 40);
			this.btnValidate.Name = "btnValidate";
			this.btnValidate.Size = new System.Drawing.Size(80, 20);
			this.btnValidate.TabIndex = 7;
			this.btnValidate.Text = "Validate";
			this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
			// 
			// btnHash
			// 
			this.btnHash.Location = new System.Drawing.Point(168, 40);
			this.btnHash.Name = "btnHash";
			this.btnHash.Size = new System.Drawing.Size(80, 20);
			this.btnHash.TabIndex = 8;
			this.btnHash.Text = "Hash";
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(584, 469);
			this.Controls.Add(this.txtOutput);
			this.Controls.Add(this.gbValidate);
			this.Controls.Add(this.gbPopulate);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FormMain";
			this.Text = "Email Classification Harness";
			this.gbPopulate.ResumeLayout(false);
			this.gbValidate.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void AppendError(Exception ex) 
		{	
			AppendMessage(ex.ToString());
		}

		private void AppendMessage(string message) 
		{
			txtOutput.AppendText(message + "\r\n");
		}

		private void btnPopulate_Click(object sender, System.EventArgs e)
		{
			try 
			{
				txtOutput.Text = "";
				Classification classification = new Classification();
				classification.Populate(
					Convert.ToInt32(txtEmailClassificationID.Text),
					Convert.ToInt32(txtPrivateLabelID.Text),
					Convert.ToInt32(txtMemberID.Text));

				AppendMessage("From: " + classification.Account + "@<private label uri>");
				HeaderCollectionEnumerator enumerator = classification.Headers.GetHeaderEnumerator();

				while (enumerator.MoveNext()) 
				{
					AppendMessage(enumerator.FullHeader);
				}
			} 
			catch (Exception ex) 
			{
				AppendError(ex);
			}
		}

		private void btnValidate_Click(object sender, System.EventArgs e)
		{
			try 
			{
				txtOutput.Text = "";
				Classification classification = new Classification();
				AppendMessage(txtHeaderVal.Text + " is Valid? " + classification.IsValidHeader(txtHeaderVal.Text));
			} 
			catch (Exception ex) 
			{
				AppendError(ex);
			}		
		}
	}
}
