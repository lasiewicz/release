using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

using Matchnet.CachingTemp;
using Matchnet.DataTemp;
using Matchnet.Labels;
using Matchnet.Lib.Member;

namespace Matchnet.Email.Classification
{
	public class Classification
	{
		private const string HASH_SEED = "GotDimSum";
		private const string HEADER_PRIORITY = "X-MatchnetPriority";
		private const string HEADER_MAILTYPE = "X-MatchnetMailType";

		private int _EmailClassificationID;
		private int _MemberID;
		private int _PrivateLabelID;
		private string _EmailType;
		private int _DeliveryTypeID;
		private int _Verified;
		private string _Account;
		private string _Signature;
		private HeaderCollection _HeaderCollection;
		private bool _isValidToSend = false;
		private BehaviorFilter.FilterSettings.SendSettings _sendSettings;

		public Classification() 
		{
			_HeaderCollection = new HeaderCollection();
			_isValidToSend = false;
			_sendSettings = new Matchnet.Email.Classification.BehaviorFilter.FilterSettings.SendSettings();
		}

		#region PUBLIC PROPERTIES
		public BehaviorFilter.FilterSettings.SendSettings SendFilterSettings 
		{
			get 
			{
				return _sendSettings;
			}
		}

		public int EmailClassificationID 
		{
			get 
			{
				return _EmailClassificationID;
			}
		}

		public int MemberID 
		{
			get 
			{
				return _MemberID;
			}
		}	

		public int PrivateLabelID 
		{
			get 
			{
				return _PrivateLabelID;
			}
		}

		public string EmailType 
		{
			get 
			{
				return _EmailType;
			}
		}

		public int DeliveryTypeID 
		{
			get 
			{
				return _DeliveryTypeID;
			}
		}

		public bool Verified 
		{
			get 
			{
				return _Verified == 1;
			}
		}

		public string Signature 
		{
			get 
			{
				return _Signature;
			}
		}

		public string Account 
		{
			get 
			{
				return _Account;
			}
		}

		public HeaderCollection Headers 
		{
			get 
			{
				return _HeaderCollection;
			}
		}
		
		public bool IsValidToSend
		{
			get{return _isValidToSend;}
		}
		#endregion

		public void Populate(int emailClassificationID, int privateLabelID, int memberID)
		{
			_EmailClassificationID = emailClassificationID;
			_PrivateLabelID = privateLabelID;
			_MemberID = memberID;

			PrivateLabel label = new PrivateLabel();
			label.Load(privateLabelID);

			MemberProfile memberProfile = new MemberProfile();
			memberProfile.Populate(memberID, 
				label.DomainID, 
				privateLabelID, 
				label.LanguageID);

			// need to set do a check here for whether the member should be receiving mail or not
			// TODO - Move this out to somewhere else - should not be in classification. There should be some other central component 
			// that is responsible for creating the reuqest to send an email in the first place that should short-circuit the entire envdeavor
			// if the member should not be receiving emails
			_isValidToSend = BehaviorFilter.Filter.SendMailFilter(_MemberID, _PrivateLabelID, _sendSettings);
			
			int globalStatusMask = Matchnet.Lib.Util.Util.CInt(memberProfile["GlobalStatusMask"]);

			_Verified = (globalStatusMask & Matchnet.Lib.ConstantsTemp.EMAIL_VERIFIED) == Matchnet.Lib.ConstantsTemp.EMAIL_VERIFIED 
				? 1 : 0;
			
			string cacheKey = "EMAILCLASSIFICATION:" + emailClassificationID;

			object val = Cache.GetInstance().Get(cacheKey);

			if (val == null) 
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnShared", 0));
				client.AddParameter("@EmailClassificationID", SqlDbType.Int, ParameterDirection.Input, emailClassificationID);
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
				DataTable table = client.GetDataTable("up_EmailClassification_List", CommandType.StoredProcedure);

				if (table != null && table.Rows.Count > 0) 
				{
					DataRow row = table.Rows[0];
					_EmailType = Convert.ToString(row["Description"]);
					_Account = Convert.ToString(row["Account"]);
					_DeliveryTypeID = Convert.ToInt32(row["DeliveryTypeID"]);
				}

				Cache cache = Cache.GetInstance();

				cache.Insert(
					cacheKey,
					ToCacheString(),
					null,
					cache.NoAbsoluteExpiration,
					TimeSpan.FromSeconds(360));
			} 
			else 
			{
				FromCacheString((string)val);
			}
			PopulateHeaders();
		}

		private string ToCacheString() 
		{
			return _EmailType + ":" + _DeliveryTypeID + ":" + _Account;
		}

		private void FromCacheString(string cacheString) 
		{
			string[] tokens = cacheString.Split(new char[] { ':' });
			_EmailType = tokens[0];
			_DeliveryTypeID = Convert.ToInt32(tokens[1]);
			_Account = tokens[2];
		}

		private void PopulateHeaders() 
		{
			Header priority = new Header();
			priority.Name = HEADER_PRIORITY;
			priority.Value = Convert.ToString(_DeliveryTypeID);
			_HeaderCollection.Add(priority);

			Header type = new Header();
			type.Name = HEADER_MAILTYPE;
			string prefix = MailTypePrefix();
			type.Value = prefix + "-" + GetSignature(prefix);
			_HeaderCollection.Add(type);
		}

		private string MailTypePrefix() 
		{
			return _PrivateLabelID + "-" + _MemberID + "-" + _EmailClassificationID + "-" + _Verified;
		}

		private string GetSignature(string prefix) 
		{
			MD5 md5 = MD5.Create();
			_Signature = BitConverter.ToString(md5.ComputeHash(Encoding.ASCII.GetBytes(HASH_SEED + prefix))).Replace("-",null);
			return _Signature;
		}

		public bool IsValidHeader(string header) 
		{
			int delimeterPos = header.LastIndexOf("-");
			string prefix = header.Substring(0, delimeterPos);
			string suffix = header.Substring(delimeterPos + 1);
			return GetSignature(prefix) == suffix;
		}

	}
}
