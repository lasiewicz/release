using System;
using System.Collections;
using System.Diagnostics;

using Matchnet.Lib;
using Matchnet.MessageQueue;

namespace Matchnet.Member.MemberCache
{
	[Serializable]
	public class MemberKeepAliveQueueItem : CacheQueueItemBase
	{

		public static void Send(MemberKeepAliveQueueItem item)
		{
			CacheQueueItemBase.Send(item, CacheQueueItemBase.SendType.Broadcast, QueueProcessor.MessageFormatterType.XML);
		}


		public override void Execute()
		{
			//don't bother if this queue item is more than 10 minutes old
			if (_Date > DateTime.Now.AddMinutes(-10))
			{
				MemberCache.GetInstance().InsertMemberKeepAlive(_MemberID, _PrivateLabelID, _Date);
			}
		}

	}
}
