using System;
using System.Collections;
using System.Diagnostics;

using Matchnet.MessageQueue;

namespace Matchnet.Member.MemberCache
{
	[Serializable]
	public class CachedMemberReplicationQueueItem : CacheQueueItemBase
	{
		public CachedMember _CachedMember;

		public static void Send(CachedMemberReplicationQueueItem item)
		{
			if (item.CachedMember == null)
			{
				throw new Exception("CachedMemberReplicationQueueItem.Member must be set before calling Send()");
			}
	
			CacheQueueItemBase.Send(item, CacheQueueItemBase.SendType.Broadcast, QueueProcessor.MessageFormatterType.Binary);
		}


		public override void Execute()
		{
			MemberCache.GetInstance().InsertMember(_CachedMember, false);
		}


		#region public properties
		public CachedMember CachedMember
		{
			get
			{
				return _CachedMember;
			}
			set
			{
				_CachedMember = value;
				this.MemberID = _CachedMember.GetHashCode();
			}
		}
		#endregion
	}
}
