using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.Xml.Serialization;

using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Member;
using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.Labels;
using Matchnet.MessageQueue;

namespace Matchnet.Member.MemberCache
{
	[Serializable]
	public class MemberUpdateQueueItem : CacheQueueItemBase
	{
		private Matchnet.Member.MemberAttributes _Attributes;

		
		public MemberUpdateQueueItem()
		{
			_Attributes = new MemberAttributes();
		}


		public static void Send(MemberUpdateQueueItem item)
		{
			if (item.MemberID == Constants.NULL_NUMBER || item.PrivateLabelID == Constants.NULL_NUMBER)
			{
				throw new Exception("MemberID and PrivateLabelID must be set before calling Send()");
			}
		
			CacheQueueItemBase.Send(item, CacheQueueItemBase.SendType.Broadcast, QueueProcessor.MessageFormatterType.Binary);
		}


		public override void Execute()
		{
			Matchnet.Caching.Cache cache = Matchnet.Caching.Cache.GetInstance();

			object o = cache.Get(MemberCache.GetMemberKey(_MemberID));

			PrivateLabel pl = PrivateLabel.Get(_PrivateLabelID);
			int domainID = pl.DomainID;
			int languageID = pl.LanguageID;

			if (o == null)
			{
				return;
			}
			else
			{
				CachedMember cachedMember = (CachedMember)o;
				foreach (string key in _Attributes.AllKeys)
				{
					MemberAttribute attribute = _Attributes[key];
					AttributeConfig attributeConfig = AttributeConfig.GetAttributeConfig(attribute.Name);

					string val = StringUtil.HebrewToUTF(attribute.Value);
					cachedMember.Attributes.Add(domainID,
													languageID,
													attributeConfig.IsGlobal,
													attributeConfig.DataType == AttributeConfig.AttributeDataType.Text,
													attribute.Name,
													val,
													attribute.ApprovalStatus);
				}
			}
		}


		#region public properties
		public MemberAttributes Attributes
		{
			get
			{
				return _Attributes;
			}
			set
			{
				_Attributes = value;
			}
		}
		#endregion
	}
}
