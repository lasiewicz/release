using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Runtime.Serialization;


namespace Matchnet.Member.MemberCache
{
	[Serializable]
	public class CachedMemberAttributes
	{
		private const int NULL_NUMBER = -2147483647;

		private Hashtable _HT;

		public CachedMemberAttributes()
		{
			_HT = new Hashtable();
		}


		#region value methods

		public void Add(int domainID, int languageID, bool isGlobal, bool isText, string name, string value)
		{
			Add(domainID, languageID, isGlobal, isText, AttributeConfig.GetAttributeID(name), value);
		}


		public void Add(int domainID, int languageID, bool isGlobal, bool isText, int attributeID, string value)
		{
			Add(domainID, languageID, isGlobal, isText, attributeID, value, Matchnet.Member.MemberAttribute.ApprovalStatusType.None);
		}


		public void Add(int domainID, int languageID, bool isGlobal, bool isText, string name, string value, Matchnet.Member.MemberAttribute.ApprovalStatusType approvalStatus)
		{
			Add(domainID, languageID, isGlobal, isText, AttributeConfig.GetAttributeID(name), value, approvalStatus);
		}


		public void Add(int domainID, int languageID, bool isGlobal, bool isText, int attributeID, string value, Matchnet.Member.MemberAttribute.ApprovalStatusType approvalStatus)
		{
			string key = GetKey(attributeID, domainID, languageID, isGlobal, isText);

			if (!_HT.ContainsKey(key))
			{
				if (!isText)
				{
					_HT.Add(key, value);
				}
				else
				{
					_HT.Add(key, new TextValue(value, approvalStatus));
				}
			}
			else
			{
				if (!isText)
				{
					_HT[key] = value;
				}
				else
				{
					TextValue textValue = ((TextValue)_HT[key]);
					textValue.Value = value;
					textValue.ApprovalStatus = approvalStatus;
				}
			}

		}


		private string GetKey(int attributeID, int domainID, int languageID, bool isGlobal, bool isText)
		{
			string key;

			if (isGlobal)
			{
				key = attributeID.ToString();
			}
			else
			{
				key = attributeID.ToString() + ":" + domainID.ToString();
			}

			if (isText)
			{
				key = key + ":" + languageID.ToString();
			}

			return key;
		}

		
		public string Value(int domainID, int languageID, string attributeName)
		{
			return Value(domainID, languageID, attributeName, string.Empty);
		}


		public string Value(int domainID, int languageID, string attributeName, out Matchnet.Member.MemberAttribute.ApprovalStatusType approvalStatus)
		{
			approvalStatus = Matchnet.Member.MemberAttribute.ApprovalStatusType.None;
			return Value(domainID, languageID, attributeName, string.Empty, out approvalStatus);
		}


		public string Value(int domainID, int languageID, string attributeName, string defaultValue)
		{
			Matchnet.Member.MemberAttribute.ApprovalStatusType approvalStatus;
			return Value(domainID, languageID, attributeName, defaultValue, out approvalStatus);
		}
			

		public string Value(int domainID, int languageID, string attributeName, string defaultValue, out Matchnet.Member.MemberAttribute.ApprovalStatusType approvalStatus)
		{
			int attributeID = AttributeConfig.GetAttributeID(attributeName);
			return Value(domainID, languageID, attributeID, defaultValue, out approvalStatus);
		}


		public string Value(int domainID, int languageID, int attributeID, out Matchnet.Member.MemberAttribute.ApprovalStatusType approvalStatus)
		{
			return Value(domainID, languageID, attributeID, string.Empty, out approvalStatus);
		}


		public string Value(int domainID, int languageID, int attributeID, string defaultValue, out Matchnet.Member.MemberAttribute.ApprovalStatusType approvalStatus)
		{
			string val;

			AttributeConfig attributeConfig = AttributeConfig.GetAttributeConfig(attributeID);
			string key = GetKey(attributeID, domainID, languageID, attributeConfig.IsGlobal, attributeConfig.DataType == AttributeConfig.AttributeDataType.Text);

			approvalStatus = Matchnet.Member.MemberAttribute.ApprovalStatusType.None;
	
			object o = _HT[key];

			if (o != null)
			{
				if (attributeConfig.DataType != AttributeConfig.AttributeDataType.Text)
				{
					val = o.ToString();
				}
				else
				{
					TextValue textValue = (TextValue)o;
					val = textValue.Value;
					approvalStatus = textValue.ApprovalStatus;
				}
			}
			else
			{
				val = defaultValue;
			}

			return val;
			
		}


		public int ValueInt(int domainID, string attributeName)
		{
			return ValueInt(domainID, attributeName, NULL_NUMBER);
		}


		public int ValueInt(int domainID, string attributeName, int defaultValue)
		{
			double val;

			if (double.TryParse(Value(domainID, NULL_NUMBER, attributeName), System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out val))
			{
				return (int)val;
			}

			return defaultValue;
		}


		public DateTime ValueDate(int domainID, string attributeName)
		{
			return ValueDate(domainID, attributeName, DateTime.MinValue);
		}


		public DateTime ValueDate(int domainID, string attributeName, DateTime defaultValue)
		{
			string val;

			val = Value(domainID, NULL_NUMBER, attributeName);

			if (val.Length > 0)
			{
				return DateTime.Parse(val);
			}

			return defaultValue;
		}
        

		public bool ValueBool(int domainID, string attributeName)
		{
			return ValueBool(domainID, attributeName, false);
		}


		public bool ValueBool(int domainID, string attributeName, bool defaultValue)
		{
			string val;

			val = Value(domainID, NULL_NUMBER, attributeName);

			if (val.Length > 0)
			{
				return (val == "1");
			}

			return defaultValue;
		}
		#endregion
	}

	[Serializable]
	public class TextValue
	{
		private string _Val;
		private Matchnet.Member.MemberAttribute.ApprovalStatusType _AprovalStatus;

		public TextValue(string val, Matchnet.Member.MemberAttribute.ApprovalStatusType approvalStatus)
		{
			_Val = val;
			_AprovalStatus = approvalStatus;
		}


		public string Value
		{
			get
			{
				return _Val;
			}
			set
			{
				_Val = value;
			}
		}


		public Matchnet.Member.MemberAttribute.ApprovalStatusType ApprovalStatus
		{
			get
			{
				return _AprovalStatus;
			}
			set
			{
				_AprovalStatus = value;
			}
		}

	}

}
