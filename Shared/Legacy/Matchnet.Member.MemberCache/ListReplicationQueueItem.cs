using System;
using System.Collections;
using System.Diagnostics;

using System.Xml.Serialization;
using System.IO;

using Matchnet.Lib;
using Matchnet.List;
using Matchnet.MessageQueue;

namespace Matchnet.Member.MemberCache
{
	[Serializable]
	public class ListReplicationQueueItem : CacheQueueItemBase
	{
		private YNMList _List;
		private int _DomainID = Constants.NULL_NUMBER;

		public static void Send(ListReplicationQueueItem item)
		{
			if (item.List == null)
			{
				throw new Exception("ListReplicationQueueItem.List must be set before calling Send()");
			}

			if (item.DomainID == Constants.NULL_NUMBER)
			{
				throw new Exception("ListReplicationQueueItem.DomainID must be set before calling Send()");
			}

			CacheQueueItemBase.Send(item, CacheQueueItemBase.SendType.Broadcast, QueueProcessor.MessageFormatterType.Binary);
		}


		public override void Execute()
		{
			MemberCache.GetInstance().InsertList(_List, false);
		}


		#region public properties
		public YNMList List
		{
			get
			{
				return _List;
			}
			set
			{
				_List = value;
				this.MemberID = _List.MemberID;
				_DomainID = _List.DomainID;
			}
		}

		public int DomainID
		{
			get
			{
				return _DomainID;
			}
		}
		#endregion
	}
}
