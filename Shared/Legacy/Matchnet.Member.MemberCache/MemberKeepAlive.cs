using System;

namespace Matchnet.Member.MemberCache
{
	public class MemberKeepAlive
	{
		private int _MemberID;
		private int _DomainID;
		private int _PrivateLabelID;
		private DateTime _Date;

		public MemberKeepAlive(int memberID, int domainID, int privateLabelID, DateTime date)
		{
			_MemberID = memberID;
			_DomainID = domainID;
			_PrivateLabelID = privateLabelID;
			_Date = date;
		}

		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}

		public int DomainID
		{
			get
			{
				return _DomainID;
			}
			set
			{
				_DomainID = value;
			}
		}

		public int PrivateLabelID
		{
			get
			{
				return _PrivateLabelID;
			}
			set
			{
				_PrivateLabelID = value;
			}
		}

		public DateTime Date
		{
			get
			{
				return _Date;
			}
			set
			{
				_Date = value;
			}
		}


	}
}
