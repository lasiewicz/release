using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Web.Caching;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.HotList;
using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.Lib.Exceptions;
using Matchnet.List;
using Matchnet.MessageQueue;
using Matchnet.Member.MembersOnline;
using Matchnet.Member;


namespace Matchnet.Member.MemberCache
{
	public class MemberCache : MarshalByRefObject, IDisposable
	{
		private const string _AppEventLogName = "mnMemberCacheService";
		private const string _AppEventSourceName = "mnMemberCacheService";

		private static MemberCache _Singleton;
		private static string _Lock = "";

		private System.Web.Caching.Cache _Cache;
		private QueueProcessor _QueueProcessorServer;
		private QueueProcessor _QueueProcessorReplication;
		private MemberCacheConfig _Config;
		private PerformanceCounter _MemberItems;
		private PerformanceCounter _MemberOnlineItems;
		private PerformanceCounter _MemberListItems;
		private PerformanceCounter _HitRate;
		private PerformanceCounter _HitRate_Base;
		private PerformanceCounter _RequestsSec;
		private CacheItemRemovedCallback _ExpireMember;
		private CacheItemRemovedCallback _ExpireMemberKeepAlive;
		private CacheItemRemovedCallback _ExpireMemberList;
		private Thread _ThreadReplicationRequest;
		private bool _MOLUpdateEnabled = false;
		private ObjRef _ObjRef;
		private MemberCacheAdmin _MemberCacheAdmin;
		private bool _IsEnabled = true;
		private TcpChannel _Channel;
		private bool _IsReplicating = false;
		private string _ExpiredCountLock = "";
		private int _ExpiredCount = 0;

		public static MemberCache GetInstance()
		{
			if (_Singleton == null)
			{
				lock(_Lock)
				{
					if (_Singleton == null)
					{
						_Singleton = new MemberCache();
					}
				}
			}

			return _Singleton;
		}


		private MemberCache()
		{
			_MemberItems = new PerformanceCounter(_AppEventSourceName, "MemberItems", false);
			_MemberOnlineItems = new PerformanceCounter(_AppEventSourceName, "MemberOnlineItems", false);
			_MemberListItems = new PerformanceCounter(_AppEventSourceName, "MemberListItems", false);
			_HitRate = new PerformanceCounter(_AppEventSourceName, "HitRate", false);
			_HitRate_Base = new PerformanceCounter(_AppEventSourceName, "HitRate_Base", false);
			_RequestsSec = new PerformanceCounter(_AppEventSourceName, "RequestsSec", false);

			_ExpireMember = new CacheItemRemovedCallback(this.ExpireMemberCallback);
			_ExpireMemberKeepAlive = new CacheItemRemovedCallback(this.ExpireMemberKeepAliveCallback);
			_ExpireMemberList = new CacheItemRemovedCallback(this.ExpireMemberListCallback);

			_MemberItems.RawValue = 0;
			_MemberOnlineItems.RawValue = 0;
			_MemberListItems.RawValue = 0;
			_RequestsSec.RawValue = 0;

			_Cache = System.Web.HttpRuntime.Cache;
			_Config = MemberCacheConfig.GetInstance();
			_MemberCacheAdmin = new MemberCacheAdmin(this);

			if (_Config.LocalServerQueuePath == null)
			{
				throw new Exception("LocalServerQueuePath not configured, please check mnSystem");
			}

			if (_Config.LocalPartitionQueuePath == null)
			{
				throw new Exception("LocalPartitionQueuePath not configured, please check mnSystem");
			}

			if (ConfigurationSettings.AppSettings["MOLUpdateEnabled"] == "1")
			{
				_MOLUpdateEnabled = true;
			}

			Type[] queueItemTypes = new Type[]{
												  typeof(QueueItemBase),
												  typeof(MemberUpdateQueueItem),
												  typeof(MemberKeepAliveQueueItem),
												  typeof(ListAddQueueItem)
											  };

			_QueueProcessorServer = new QueueProcessor(_Config.LocalServerQueuePath);
			_QueueProcessorServer.Start(queueItemTypes);

			_QueueProcessorReplication = new QueueProcessor(@"FormatName:DIRECT=OS:" + System.Environment.MachineName + @"\private$\MemberCacheReplication");
			_QueueProcessorReplication.Start(queueItemTypes, QueueProcessor.MessageFormatterType.Binary);

			RemotingConfiguration.Configure(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

			Hashtable props = new Hashtable();
			props["port"] = MemberCacheConfig.GetInstance().LocalServerPort;
			props["name"] = "MemberCache";
			_Channel = new TcpChannel(props, null, null);

			_ThreadReplicationRequest = new Thread(new ThreadStart(ReplicationRequest));
			_ThreadReplicationRequest.Start();

			BeginListen();
		}


		private void ReplicateCachedMember(CachedMember cachedMember)
		{
			ArrayList replicationList = MemberCacheConfig.GetInstance().GetQueueList(cachedMember.MemberID);
			
			foreach (QueueConfig queueConfig in replicationList)
			{
				MemberCache memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), "tcp://" + queueConfig.ServerName + ":" + _Config.LocalServerPort.ToString() + "/MemberCache");
				memberCache.InsertMember(cachedMember, false);
			}
		}


		private void ReplicateList(YNMList list)
		{
			ArrayList replicationList = MemberCacheConfig.GetInstance().GetQueueList(list.MemberID);
			
			foreach (QueueConfig queueConfig in replicationList)
			{
				MemberCache memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), "tcp://" + queueConfig.ServerName + ":" + _Config.LocalServerPort.ToString() + "/MemberCache");
				memberCache.InsertList(list, false);
			}
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}

		
		private void ReplicationRequest()
		{
			Thread.Sleep(1000);
			string uri = "";

			_IsReplicating = true;

			try
			{				
				MemberCacheConfig memberCacheConfig = MemberCacheConfig.GetInstance();
				ArrayList replicationList = memberCacheConfig.GetQueueList(memberCacheConfig.LocalServerOffset);

				foreach (QueueConfig queueConfig in replicationList)
				{
					if (queueConfig.ServerName.ToLower() != System.Environment.MachineName.ToLower())
					{
						uri = "tcp://" + queueConfig.ServerName + ":" + memberCacheConfig.LocalServerPort.ToString() + "/MemberCache";
						EventLog.WriteEntry("mnMemberCacheService", "beginning replication from " + uri, EventLogEntryType.Information);
						Matchnet.Member.MemberCache.MemberCache memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), uri);
						memberCache.ReplicateAll();
						EventLog.WriteEntry("mnMemberCacheService", "replication from " + uri + " completed", EventLogEntryType.Information);
					}
				}
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry("mnMemberCacheService", "replication from " + uri + " failed\n\n" + ex.ToString(), EventLogEntryType.Error);
			}
			finally
			{
				_IsReplicating = false;
			}
		}		


		private void IncrementExpirationCounter()
		{
			lock (_ExpiredCountLock)
			{
				_ExpiredCount++;

				if (_ExpiredCount > _Config.CollectionThreshold)
				{
					_ExpiredCount = 0;
					//EventLog.WriteEntry("mnMemberCacheService", "GC.Collect()", EventLogEntryType.Warning);
					GC.Collect();
				}
			}
		}


		#region Member caching
		public static string GetMemberKey(int hashCode)
		{
			return "Member" + hashCode;
		}


		public void InsertMember(CachedMember cachedMember)
		{
			InsertMember(cachedMember, true);
		}


		public void InsertMember(CachedMember cachedMember, bool mirrorable)
		{
			string key  = GetMemberKey(cachedMember.GetHashCode());

			object o = _Cache.Add(key,
				cachedMember,
				null,
				cachedMember.CacheInsertDate.AddSeconds(_Config.ObjectTTL),
				TimeSpan.Zero,
				CacheItemPriority.High,
				_ExpireMember);

			if (o == null) {
				_MemberItems.Increment();
			}

			if (mirrorable)
			{
				CachedMemberReplicationQueueItem item = new CachedMemberReplicationQueueItem();
				item.CachedMember = cachedMember;
				CachedMemberReplicationQueueItem.Send(item);
			}
		}


		public Member GetMember(int memberID, int privateLabelID)
		{
			return GetMember(memberID, privateLabelID, AttributeConfig.AttributeGroupType.MiniProfile, false);
		}


		public Member GetMember(int memberID, int privateLabelID, bool forceReload)
		{
			return GetMember(memberID, privateLabelID, AttributeConfig.AttributeGroupType.MiniProfile, forceReload);
		}


		public Member GetMember(int memberID, int privateLabelID, AttributeConfig.AttributeGroupType attributeGroup)
		{
			return GetMember(memberID, privateLabelID, attributeGroup, false);
		}


		public Member GetMember(int memberID, int privateLabelID, AttributeConfig.AttributeGroupType attributeGroup, bool forceReload)
		{
			try
			{
				string key = GetMemberKey(memberID);
			
				CachedMember cachedMember = null;

				_HitRate_Base.Increment();
				_RequestsSec.Increment();

				if (forceReload)
				{
					LoadMember(memberID, privateLabelID, ref cachedMember);
				}
				else
				{
					object result = _Cache.Get(key);

					if (result != null)
					{
						cachedMember = (CachedMember)result;
						if (cachedMember.IsPrivateLabelLoaded(Labels.PrivateLabel.GetBasePrivateLabelID(privateLabelID)))
						{
							_HitRate.Increment();
						}
						else
						{
							LoadMember(memberID, privateLabelID, ref cachedMember);
						}
					}
					else
					{
						LoadMember(memberID, privateLabelID, ref cachedMember);
					}
				}

				return GetPrivateLabelMember(cachedMember, privateLabelID, attributeGroup);
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString());
				EventLog.WriteEntry("mnMemberCacheService", ex.ToString(), EventLogEntryType.Error);
				return null;
			}
		}


		private void LoadMember(int memberID,
			int privateLabelID,
			ref CachedMember cachedMember)
		{
			const int ATTRIBUTEID_USERNAME = -2;

			if (cachedMember == null)
			{
				cachedMember = new CachedMember();
			}

			PrivateLabel privateLabel = PrivateLabel.Get(privateLabelID);
			int basePrivateLabelID = privateLabel.BasePrivateLabelID;
			if (basePrivateLabelID == Constants.NULL_NUMBER)
			{
				basePrivateLabelID = privateLabelID;
			}
			int domainID = privateLabel.DomainID;
			int languageID = privateLabel.LanguageID;

			if (!cachedMember.IsPrivateLabelLoaded(basePrivateLabelID))
			{
				cachedMember.MemberID = memberID;

				try
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
					client.AddParameter("@BasePrivateLabelID", SqlDbType.Int, ParameterDirection.Input, basePrivateLabelID);
					DataTable dtAttributes = client.GetDataTable("up_MemberAttribute_List_Cache", CommandType.StoredProcedure);

					foreach (DataRow row in dtAttributes.Rows)
					{
						int attributeID = Convert.ToInt32(row["AttributeID"]);
						string val = row["Value"].ToString();

						if (languageID == Constants.LANG_HEBREW && attributeID != ATTRIBUTEID_USERNAME)
						{
							val = StringUtil.HebrewToUTF(val);
						}

						AttributeConfig attributeConfig = AttributeConfig.GetAttributeConfig(attributeID);

						if (row["StatusMask"] == System.DBNull.Value)
						{
							cachedMember.Attributes.Add(domainID,
								languageID,
								attributeConfig.IsGlobal,
								attributeConfig.DataType == AttributeConfig.AttributeDataType.Text,
								attributeID,
								val);
						}
						else
						{
							cachedMember.Attributes.Add(domainID,
								languageID,
								attributeConfig.IsGlobal,
								attributeConfig.DataType == AttributeConfig.AttributeDataType.Text,
								attributeID,
								val,
								(MemberAttribute.ApprovalStatusType)Enum.Parse(typeof(MemberAttribute.ApprovalStatusType), row["StatusMask"].ToString()));
						}
					}
					cachedMember.LoadedBasePrivateLabels.Add(basePrivateLabelID);
				}
				catch (Exception ex)
				{
					EventLog.WriteEntry("mnMemberCacheService", ex.ToString(), EventLogEntryType.Error);
				}
			}


			InsertMember(cachedMember);
		}

		
		private Member GetPrivateLabelMember(CachedMember cachedMember, int privateLabelID, AttributeConfig.AttributeGroupType attributeGroup)
		{
			const int ONLINE_HIDE = 4;
			Member member = new Member();
			PrivateLabel pl = PrivateLabel.Get(privateLabelID);
			int basePrivateLabelID = pl.BasePrivateLabelID;
			int domainID = pl.DomainID;
			int languageID = pl.LanguageID;

			member.MemberID = cachedMember.MemberID;
			member.PrivateLabelID = privateLabelID;

			if ((cachedMember.Attributes.ValueInt(basePrivateLabelID, "HideMask") & ONLINE_HIDE) != ONLINE_HIDE)
			{
				member.IsOnline = MemberKeepAliveExists(cachedMember.MemberID, privateLabelID);
			}

			foreach (int attributeID in AttributeConfig.GetAttributeGroup(privateLabelID, attributeGroup))
			{
				MemberAttribute.ApprovalStatusType approvalStatus;
				string val = cachedMember.Attributes.Value(domainID, languageID, attributeID, out approvalStatus);
				if (val != string.Empty)
				{
					AttributeConfig attributeConfig = AttributeConfig.GetAttributeConfig(attributeID);
					member.Attributes.Add(privateLabelID, attributeConfig.Name, val, approvalStatus);
				}
			}

			return member;
		}


		private void ExpireMemberCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			if (_MemberItems.RawValue > 0)
			{
				_MemberItems.Decrement();
			}

			IncrementExpirationCounter();
		}
		#endregion

		#region Member Online caching
		private static string GetMemberKeepAliveKey(int memberID, int domainID)
		{
			return "MemberKeepAlive" + memberID.ToString() + ":" + domainID.ToString();
		}
		

		public bool InsertMemberKeepAlive(int memberID, int privateLabelID, DateTime date)
		{
			PrivateLabel pl = PrivateLabel.Get(privateLabelID);
			int domainID = pl.DomainID;
			string key = GetMemberKeepAliveKey(memberID, domainID);
			int ttl;

			switch (domainID)
			{
				case 1:
					ttl = 10;
					break;

				default:
					ttl = 3;
					break;
			}

			object o = _Cache.Add(key,
				new MemberKeepAlive(memberID, domainID, privateLabelID, date),
				null,
				date.AddMinutes(ttl),
				TimeSpan.Zero,
				CacheItemPriority.High,
				_ExpireMemberKeepAlive);

			if (o == null)
			{
				_MemberOnlineItems.Increment();

				if (_MOLUpdateEnabled)
				{
					MemberOnlineAddQueueItem onlineAddItem = new MemberOnlineAddQueueItem();
					onlineAddItem.MemberID = memberID;
					onlineAddItem.DomainID = domainID;
					onlineAddItem.Populate(GetMember(memberID, privateLabelID, Matchnet.Member.AttributeConfig.AttributeGroupType.MiniProfile));
					MemberOnlineAddQueueItem.Send(onlineAddItem);
				}
				return true;
			}

			return false;
		}


		public static bool MemberKeepAliveExists(int memberID, int privateLabelID)
		{
			PrivateLabel pl = PrivateLabel.Get(privateLabelID);
			int domainID = pl.DomainID;
			string key = GetMemberKeepAliveKey(memberID, domainID);
			object o = System.Web.HttpRuntime.Cache.Get(key);
			return (o != null);
		}

		
		private void ExpireMemberKeepAliveCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			if (_MemberOnlineItems.RawValue > 0)
			{
				_MemberOnlineItems.Decrement();
			}

			if (_MOLUpdateEnabled)
			{
				MemberKeepAlive memberKeepAlive = (MemberKeepAlive)value;
				MemberOnlineRemoveQueueItem onlineRemoveItem = new MemberOnlineRemoveQueueItem();
				onlineRemoveItem.MemberID = memberKeepAlive.MemberID;
				onlineRemoveItem.DomainID = memberKeepAlive.DomainID;
				MemberOnlineRemoveQueueItem.Send(onlineRemoveItem);
			}

			IncrementExpirationCounter();
		}
		#endregion

		#region Member List caching
		public void InsertList(YNMList list)
		{
			InsertList(list, true);
		}


		public void InsertList(YNMList ynmList, bool mirrorable)
		{
			string key  = GetMemberListKey(ynmList.MemberID, ynmList.DomainID);
			object o = _Cache.Add(key,
				ynmList,
				null,
				ynmList.CacheInsertDate.AddSeconds(_Config.ObjectTTL),
				TimeSpan.Zero,
				CacheItemPriority.High,
				_ExpireMemberList);

			if (o == null) 
			{
				_MemberListItems.Increment();
			}	

			if (mirrorable)
			{
				ListReplicationQueueItem item = new ListReplicationQueueItem();
				item.List = ynmList;
				ListReplicationQueueItem.Send(item);
			}
		}


		private string GetMemberListKey(int memberID, int domainID)
		{
			return "MemberList" + memberID.ToString() + ":" + domainID.ToString();
		}


		public YNMList GetList(int memberID, int domainID)
		{
			string key = GetMemberListKey(memberID, domainID);
			YNMList ynmList;

			object o = _Cache.Get(key);

			if (o != null)
			{
				ynmList = (YNMList)o;
			}
			else
			{
				ynmList = new YNMList();
				ynmList.Populate(memberID, domainID);

				InsertList(ynmList);
			}

			return ynmList;
		}


		public DataTable GetMemberListStatus(int memberID, int domainID, ArrayList targetMemberIDs)
		{
			try
			{
				YNMList ynmList = GetList(memberID, domainID);
				DataTable dt;

				dt = new DataTable();
				dt.Columns.Add("TargetMemberID", typeof(System.Int32));
				dt.Columns.Add("YNMMask", typeof(YNMMask));

				foreach (int targetMemberID in targetMemberIDs)
				{
					dt.Rows.Add(new object[]{targetMemberID, ynmList.GetMask(targetMemberID)});
				}
	
				return dt;
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString());
				EventLog.WriteEntry("mnMemberCacheService", ex.ToString(), EventLogEntryType.Error);
				return null;
			}
		}


		public DataTable GetListMembers(int memberID, int domainID, int startRow, int pageSize, out int totalRows)
		{
			YNMList list = GetList(memberID, domainID);
			return list.GetMutualYesMembers(memberID, domainID, startRow, pageSize, out totalRows);
		}


		private void ExpireMemberListCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			if (_MemberListItems.RawValue > 0)
			{
				_MemberListItems.Decrement();
			}

			IncrementExpirationCounter();
		}
		#endregion

		#region Administration

		protected internal bool IsEnabled
		{
			get
			{
				return _IsEnabled;
			}
		}


		protected internal bool IsReplicating
		{
			get
			{
				return _IsReplicating;
			}
		}


		protected internal void BeginListen()
		{
				ChannelServices.RegisterChannel(_Channel);
				_ObjRef = RemotingServices.Marshal(this, "MemberCache");
				EventLog.WriteEntry("mnMemberCacheService", "BeginListen()", EventLogEntryType.Information);
		}


		protected internal void EndListen()
		{
				RemotingServices.Disconnect(this);
				_Channel.StopListening(null);
				ChannelServices.UnregisterChannel(_Channel);
				EventLog.WriteEntry("mnMemberCacheService", "EndListen()", EventLogEntryType.Information);
		}


		protected internal void Clear()
		{
			IDictionaryEnumerator cacheEnum = _Cache.GetEnumerator();
			while (cacheEnum.MoveNext())
			{
				_Cache.Remove(cacheEnum.Key.ToString());
			}
			EventLog.WriteEntry("mnMemberCacheService", "BeginListen()", EventLogEntryType.Information);
		}


		public void ReplicateAll()
		{
			IDictionaryEnumerator cacheEnum = _Cache.GetEnumerator();
			int itemCount = 0;

			EventLog.WriteEntry("mnMemberCacheService", "beginning replication push request", EventLogEntryType.Information);

			try
			{
				while (cacheEnum.MoveNext())
				{
					object o = cacheEnum.Value;
					if (o.GetType() == typeof(CachedMember))
					{
						ReplicateCachedMember((CachedMember)o);
						itemCount++;
					}
					else if (o.GetType() == typeof(YNMList))
					{
						ReplicateList((YNMList)o);
						itemCount++;
					}
				}
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry("mnMemberCacheService", "replication push request failed\r\n\r\n" + ex.ToString(), EventLogEntryType.Error);
			}

			EventLog.WriteEntry("mnMemberCacheService", "replication push request completed (" + itemCount + " items)", EventLogEntryType.Information);
		}


		internal long MemberItemCount
		{
			get
			{
				return _MemberItems.RawValue;
			}
		}

		internal long MemberOnlineItemCount
		{
			get
			{
				return _MemberOnlineItems.RawValue;
			}
		}

		internal long MemberListItemCount
		{
			get
			{
				return _MemberListItems.RawValue;
			}
		}

		internal float RequestsPerSec
		{
			get
			{
				return _RequestsSec.NextValue();
			}
		}

		#endregion

		#region static methods
		/// <summary>
		/// DEPRECATED - use GetMembers() because it will get the YNM list status as well
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="privateLabelID"></param>
		/// <param name="attributeGroupID"></param>
		/// <returns></returns>
		public static Member GetCachedMember(int memberID, int privateLabelID, AttributeConfig.AttributeGroupType attributeGroup)
		{
			string uri = MemberCacheConfig.GetInstance().GetURI(memberID);
			MemberCache memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), uri);		
			Member member = memberCache.GetMember(memberID, privateLabelID, attributeGroup);
			return member;
		}


		public static ArrayList GetCachedMembers(int memberID, int privateLabelID, ArrayList targetMemberIDs, bool populateHotList)
		{
			return GetCachedMembers(memberID, privateLabelID, targetMemberIDs, AttributeConfig.AttributeGroupType.MiniProfile, populateHotList, true);
		}


		public static ArrayList GetCachedMembers(int memberID, int privateLabelID, ArrayList targetMemberIDs)
		{
			return GetCachedMembers(memberID, privateLabelID, targetMemberIDs, AttributeConfig.AttributeGroupType.MiniProfile, true, true);
		}


		public static ArrayList GetCachedMembers(int memberID, int privateLabelID, ArrayList targetMemberIDs, AttributeConfig.AttributeGroupType attributeGroup, bool populateHotList, bool populateYNMList)
		{
			ArrayList members = new ArrayList();
			string uri;
			MemberCache memberCache;
			int memberNum;

			foreach (int targetMemberID in targetMemberIDs)
			{
				try
				{
					uri = MemberCacheConfig.GetInstance().GetURI(targetMemberID);
					memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), uri);		
					Member member = memberCache.GetMember(targetMemberID, privateLabelID, attributeGroup);
					members.Add(member);
				}
				catch
				{
					members.Add(null);
				}
			}

			if (memberID > 0)
			{
				try
				{
					PrivateLabel pl = PrivateLabel.Get(privateLabelID);
					int domainID = pl.DomainID;
					uri = MemberCacheConfig.GetInstance().GetURI(memberID);
					memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), uri);
					DataTable dt = memberCache.GetMemberListStatus(memberID, domainID, targetMemberIDs);

					memberNum = 0;
					if (dt.Rows != null)
					{
						foreach (DataRow row in dt.Rows)
						{
							((Member)members[memberNum]).ViewerHotList.YNMMask = (YNMMask)row["YNMMask"];
							memberNum++;
						}
					}

					if (populateHotList)
					{
						PopulateHotListValues(memberID, domainID, targetMemberIDs, members);
					}
				}
				catch
				{
					// ignore
				}
			}

			return members;
		}


		/*
		public static List.YNMMask GetYNMMask(int memberID, int domainID, int targetMemberID)
		{
			string uri = MemberCacheConfig.GetInstance().GetURI(memberID);
			MemberCache memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), uri);
			ArrayList targetMemberIDs = new ArrayList();
			targetMemberIDs.Add(targetMemberID);
			DataTable dt = memberCache.GetMemberListStatus(memberID, domainID, targetMemberIDs);

			if (dt.Rows.Count == 1)
			{
				return (List.YNMMask)dt.Rows[0]["YNMMask"];
			}

			return List.YNMMask.None;
		}


		public static DataTable GetCachedListMembers(int memberID, int domainID, int startRow, int pageSize, out int totalRows)
		{
			string uri = MemberCacheConfig.GetInstance().GetURI(memberID);
			MemberCache memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), uri);
			return memberCache.GetListMembers(memberID, domainID, startRow, pageSize, out totalRows);
		}
		*/
		#endregion

		private static void PopulateHotListValues(int memberID, int domainID, ArrayList targetMemberIDs, ArrayList members)
		{
			StringBuilder sb = new StringBuilder();

			foreach (int targetMemberID in targetMemberIDs)
			{
				if (sb.Length > 0)
				{
					sb.Append(",");
				}
				sb.Append(targetMemberID.ToString());
			}			

			//all this stuff will be cached eventually
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@MemberHotList", SqlDbType.VarChar, ParameterDirection.Input, sb.ToString());
			DataTable dt = client.GetDataTable("up_HotList_Member_List", CommandType.StoredProcedure);

			foreach (DataRow row in dt.Rows)
			{
				foreach (Member member in members)
				{
					if (member.MemberID == Convert.ToInt32(row["MemberID"]))
					{
						member.ViewerHotList.Add((Matchnet.HotList.HotList.HotListAttributeTypes)Enum.Parse(typeof(Matchnet.HotList.HotList.HotListAttributeTypes),
										row["HotListName"].ToString()),
										(Matchnet.HotList.HotList.HotListDirection)Enum.Parse(typeof(Matchnet.HotList.HotList.HotListDirection),
										row["Value"].ToString()));
						break;
					}
				}
			}
		}


		#region perf counters
		public static void PerformanceCounterInstall()
		{	
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														 new CounterCreationData("MemberItems", "Member Items", PerformanceCounterType.NumberOfItems64),
														 new CounterCreationData("MemberOnlineItems", "Member Online Items", PerformanceCounterType.NumberOfItems64),
														 new CounterCreationData("MemberListItems", "Member List Items", PerformanceCounterType.NumberOfItems64),

														 new CounterCreationData("HitRate", "Cache Hit Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("HitRate_Base","Count of all requests", PerformanceCounterType.RawBase),

														 new CounterCreationData("RequestsSec", "Requests/second", PerformanceCounterType.RateOfCountsPerSecond32)
													 });
			PerformanceCounterCategory.Create(_AppEventSourceName, "Member Profile Cache", ccdc);
		}


		public static void PerformanceCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(_AppEventSourceName);
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			_IsEnabled = false;

			Thread.Sleep(10000);

			EndListen();

			try
			{
				_MemberCacheAdmin.Dispose();
			}
			catch {}

			try
			{
				_Config.Dispose();
			}
			catch {}

			try
			{
				_ThreadReplicationRequest.Abort();
			}
			catch {}

			_QueueProcessorServer.Stop();
			_QueueProcessorReplication.Stop();
			_MemberItems.RawValue = 0;
			_MemberOnlineItems.RawValue = 0;
			_MemberListItems.RawValue = 0;
			_RequestsSec.RawValue = 0;
		}

		#endregion
	}
}
