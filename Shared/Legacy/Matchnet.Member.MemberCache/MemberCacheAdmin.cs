using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Web.Caching;
using System.Runtime.Remoting;

using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels.Http;

namespace Matchnet.Member.MemberCache
{
	public class MemberCacheAdmin : MarshalByRefObject, IDisposable
	{
		private MemberCache _MemberCache;
		private ObjRef _ObjRef;

		public MemberCacheAdmin(MemberCache memberCache)
		{
			_MemberCache = memberCache;

			Hashtable props = new Hashtable();
			props["port"] = MemberCacheConfig.GetInstance().LocalServerPort + 100;
			props["name"] = "MemberCacheAdmin";
			HttpChannel chan = new HttpChannel(props, null, null);
			ChannelServices.RegisterChannel(chan);
			_ObjRef = RemotingServices.Marshal(this, "MemberCacheAdmin");
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}

		public string GetHealthCheck()
		{
			if (_MemberCache.IsEnabled)
			{
				return "Matchnet Server Enabled";
			}
			else
			{
				return "Matchnet Server Down";
			}
		}

		public bool IsEnabled
		{
			get
			{
				return _MemberCache.IsEnabled;
			}
		}

		public void RequestReplication()
		{
			_MemberCache.ReplicateAll();
		}


		public bool IsReplicating
		{
			get
			{
				return _MemberCache.IsReplicating;
			}
		}


		public void Clear()
		{
			_MemberCache.Clear();
		}

		public long MemberItemCount
		{
			get
			{
				return _MemberCache.MemberItemCount;
			}
		}

		public long MemberOnlineItemCount
		{
			get
			{
				return _MemberCache.MemberOnlineItemCount;
			}
		}

		public long MemberListItemCount
		{
			get
			{
				return _MemberCache.MemberListItemCount;
			}
		}

		public float RequestsPerSec
		{
			get
			{
				return _MemberCache.RequestsPerSec;
			}
		}

		#region IDisposable Members

		public void Dispose()
		{
			RemotingServices.Disconnect(this);
		}

		#endregion
	}
}
