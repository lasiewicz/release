using System;
using System.Data;

using Matchnet.Caching;
using Matchnet.Data;

namespace Matchnet.Member.MemberCache
{
	internal class PrivateLabel
	{
		private int _privateLabelID;
		private int _basePrivateLabelID;
		private int _domainID;
		private int _languageID;
			
		public PrivateLabel(int privateLabelID, int basePrivateLabelID, int domainID, int languageID)
		{
			_privateLabelID = privateLabelID;
			_basePrivateLabelID = basePrivateLabelID;
			_domainID = domainID;
			_languageID = languageID;
		}

		
		public static PrivateLabel Get(int privateLabelID)
		{
			string key = "cpl:" + privateLabelID.ToString();

			Cache cache = Cache.GetInstance();
			PrivateLabel pl = cache.Get(key) as PrivateLabel;

			if (pl == null)
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnShared", 0));
				client.AddParameter("@PrivateLabelID", SqlDbType.Int,ParameterDirection.Input, privateLabelID);
				DataTable dt = client.GetDataTable("up_PrivateLabel_SelectByID", CommandType.StoredProcedure);

				if (dt.Rows.Count == 1) 
				{
					int basePrivateLabelID;
					if (dt.Rows[0]["BasePrivateLabelID"] != DBNull.Value)
					{
						basePrivateLabelID = Convert.ToInt32(dt.Rows[0]["BasePrivateLabelID"]);
					}
					else
					{
						basePrivateLabelID = privateLabelID;
					}

					pl = new PrivateLabel(privateLabelID,
											basePrivateLabelID,
											Convert.ToInt32(dt.Rows[0]["DomainID"]),
											Convert.ToInt32(dt.Rows[0]["LanguageID"]));

					cache.Insert(key, pl, null, DateTime.Now.AddHours(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
				}
				else
				{
					throw new Exception("unable to load privatelabel (" + privateLabelID.ToString() + ")");
				}
			}

			return pl;
		}


		#region public properties
		public int PrivateLabelID
		{
			get
			{
				return _privateLabelID;
			}
		}


		public int BasePrivateLabelID
		{
			get
			{
				return _basePrivateLabelID;
			}
		}


		public int DomainID
		{
			get
			{
				return _domainID;
			}
		}


		public int LanguageID
		{
			get
			{
				return _languageID;
			}
		}
		#endregion

	}
}
