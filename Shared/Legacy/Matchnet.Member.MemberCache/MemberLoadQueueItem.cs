using System;
using System.Diagnostics;

using Matchnet.Lib;
using Matchnet.MessageQueue;

namespace Matchnet.Member.MemberCache
{
	public class MemberLoadQueueItem : QueueItemBase
	{
		private int _MemberID = Constants.NULL_NUMBER;
		private int _PrivateLabelID = Constants.NULL_NUMBER;
		private DateTime _Date;


		public static void Send(CachedMemberReplicationQueueItem item)
		{
			CacheQueueItemBase.Send(item, CacheQueueItemBase.SendType.Broadcast, QueueProcessor.MessageFormatterType.XML);
		}


		public override void Execute()
		{
			//don't bother if this queue item is more than a minute old
			if (_Date > DateTime.Now.AddMinutes(-1))
			{
				MemberCache.GetInstance().GetMember(_MemberID, _PrivateLabelID);
			}
		}


		#region public properties
		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}


		public int PrivateLabelID
		{
			get
			{
				return _PrivateLabelID;
			}
			set
			{
				_PrivateLabelID = value;
			}
		}


		public DateTime Date
		{
			get
			{
				return _Date;
			}
			set
			{
				_Date = value;
			}
		}
		#endregion
	}
}
