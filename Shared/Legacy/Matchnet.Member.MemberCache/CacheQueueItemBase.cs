using System;
using System.Collections;
using System.Diagnostics;

using Matchnet.Lib;
using Matchnet.MessageQueue;

namespace Matchnet.Member.MemberCache
{
	[Serializable]
	public class CacheQueueItemBase : QueueItemBase
	{
		protected int _MemberID = Constants.NULL_NUMBER;
		protected int _PrivateLabelID = Constants.NULL_NUMBER;
		protected DateTime _Date;

		public enum SendType
		{
			Broadcast,
			CentralQueue
		}

		public static void Send(CacheQueueItemBase item, SendType sendType, MessageQueue.QueueProcessor.MessageFormatterType messageFormatter)
		{
			string typeName = item.GetType().ToString();
			if (typeName != "Matchnet.Member.MemberCache.CachedMemberReplicationQueueItem" && typeName != "Matchnet.Member.MemberCache.ListReplicationQueueItem")
			{
				if (item.MemberID == Constants.NULL_NUMBER)
				{
					throw new Exception("MemberID must be set on " + typeName);
				}
				if (item.PrivateLabelID == Constants.NULL_NUMBER)
				{
					throw new Exception("PrivateLabelID must be set on " + typeName);
				}
			}

			if (sendType == SendType.Broadcast)
			{
				item.Date = DateTime.Now;

				ArrayList replicationList = MemberCacheConfig.GetInstance().GetQueueList(item.MemberID);
			
				foreach (QueueConfig queueConfig in replicationList)
				{
					if (queueConfig.ServerName.ToLower() != System.Environment.MachineName.ToLower() || AppDomain.CurrentDomain.FriendlyName.ToLower().IndexOf("mnmembercache") == -1)
					{
						if (messageFormatter == QueueProcessor.MessageFormatterType.Binary)
						{
							item.QueuePath = @"FormatName:DIRECT=OS:" + queueConfig.ServerName + @"\private$\MemberCacheReplication";
						}
						else
						{
							item.QueuePath = queueConfig.QueuePath;
						}

						QueueItemBase.Send(item, messageFormatter);
					}
				}
			}
			else
			{
				item.QueuePath = MemberCacheConfig.GetInstance().LocalPartitionQueuePath;
				QueueItemBase.Send(item, messageFormatter);
			}
		}


		#region public properties
		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}


		public int PrivateLabelID
		{
			get
			{
				return _PrivateLabelID;
			}
			set
			{
				_PrivateLabelID = value;
			}
		}


		public DateTime Date
		{
			get
			{
				return _Date;
			}
			set
			{
				_Date = value;
			}
		}
		#endregion

	}
}
