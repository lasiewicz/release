using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

using Matchnet.Member;
using Matchnet.HotList;

namespace Matchnet.Member.MemberCache
{
	[Serializable]
	[XmlInclude(typeof(ViewerHotList))]
	public class Member : Matchnet.Member.Member
	{
		private ViewerHotList _ViewerHotList;
		private bool _IsOnline;

		public Member()
		{
			_ViewerHotList = new ViewerHotList();
		}

		public ViewerHotList ViewerHotList
		{
			get
			{
				return _ViewerHotList;
			}
			set
			{
				_ViewerHotList = value;
			}
		}

		public bool IsOnline
		{
			get
			{
				return _IsOnline;
			}
			set
			{
				_IsOnline = value;
			}
		}


	}
}
