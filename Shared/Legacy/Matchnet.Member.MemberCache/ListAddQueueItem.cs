using System;
using System.Data;
using System.Diagnostics;

using Matchnet.Lib;
using Matchnet.List;

namespace Matchnet.Member.MemberCache
{
	public class ListAddQueueItem : CacheQueueItemBase
	{
		private int _DomainID = Constants.NULL_NUMBER;
		private int _SenderMemberID = Constants.NULL_NUMBER; 
		private int _TargetMemberID = Constants.NULL_NUMBER;
		private YNMListType _YNMListTypeID;
		private bool _Mirrorable = true;

		public static void Send(ListAddQueueItem item)
		{
			if (item.DomainID == Constants.NULL_NUMBER)
			{
				throw new Exception("ListAddQueueItem.DomainID must be set before calling ListAddQueueItem.Send()");
			}

			CacheQueueItemBase.Send(item, CacheQueueItemBase.SendType.Broadcast, Matchnet.MessageQueue.QueueProcessor.MessageFormatterType.XML);

			
			if (item.Mirrorable)
			{
				//MutualCheck(item);

				// create a new Matchnet.List queue item to persist to database
				Matchnet.List.ListAddQueueItem listItem = new Matchnet.List.ListAddQueueItem();
				listItem.MemberID = item.MemberID;
				listItem.DomainID = item.DomainID;
				listItem.PrivateLabelID = item.PrivateLabelID;
				listItem.SenderMemberID = item.SenderMemberID;
				listItem.TargetMemberID = item.TargetMemberID;
				listItem.YNMListTypeID = item.YNMListTypeID;
				Matchnet.List.ListAddQueueItem.Send(listItem);

				// flip the member that owns this vote so we have 
				// the reciprocal item in the cache
				ListAddQueueItem itemNew = new ListAddQueueItem();
				itemNew.MemberID = item.TargetMemberID;
				itemNew.DomainID = item.DomainID;
				itemNew.PrivateLabelID = item.PrivateLabelID;
				itemNew.SenderMemberID = item.SenderMemberID;
				itemNew.TargetMemberID = item.TargetMemberID;
				itemNew.YNMListTypeID = item.YNMListTypeID;
				itemNew.Mirrorable = false;
				ListAddQueueItem.Send(itemNew);
			}
		}

		/*
		private static void MutualCheck(ListAddQueueItem item)
		{
			if (item.YNMListTypeID == YNMListType.Yes)
			{
				if ((MemberCache.GetYNMMask(item.MemberID, item.DomainID, item.TargetMemberID) & YNMMask.TargetMemberYes) == YNMMask.TargetMemberYes)
				{
					MemberUpdateQueueItem memberUpdate = new MemberUpdateQueueItem();
					memberUpdate.MemberID = item.MemberID;
					memberUpdate.PrivateLabelID = item.PrivateLabelID;
					memberUpdate.Attributes.Add(item.PrivateLabelID, "HasNewMutualYes", "1");
					MemberAttributesBL.Save(item.MemberID, item.PrivateLabelID, memberUpdate.Attributes);
					MemberUpdateQueueItem.Send(memberUpdate);

					MemberUpdateQueueItem memberUpdateTarget = new MemberUpdateQueueItem();
					memberUpdateTarget.MemberID = item.TargetMemberID;
					memberUpdateTarget.PrivateLabelID = item.PrivateLabelID;
					memberUpdateTarget.Attributes.Add(item.PrivateLabelID, "HasNewMutualYes", "1");
					MemberAttributesBL.Save(item.TargetMemberID, item.PrivateLabelID, memberUpdateTarget.Attributes);
					MemberUpdateQueueItem.Send(memberUpdateTarget);
				}
			}
		}
		*/


		public override void Execute()
		{
			YNMList list = MemberCache.GetInstance().GetList(this.MemberID, this.DomainID);
			list.Add(this.MemberID, this.SenderMemberID, this.TargetMemberID, this.YNMListTypeID, this.Date);
		}


		#region public properties
		public int DomainID
		{
			get
			{
				return _DomainID;
			}
			set
			{
				_DomainID = value;
			}
		}

		public int SenderMemberID
		{
			get
			{
				return _SenderMemberID;
			}
			set
			{
				_SenderMemberID = value;
			}
		}

		public int TargetMemberID
		{
			get
			{
				return _TargetMemberID;
			}
			set
			{
				_TargetMemberID = value;
			}
		}

		public YNMListType YNMListTypeID
		{
			get
			{
				return _YNMListTypeID;
			}
			set
			{
				_YNMListTypeID = value;
			}
		}

		public bool Mirrorable
		{
			get
			{
				return _Mirrorable;
			}
			set
			{
				_Mirrorable = value;
			}
		}
		#endregion
	}
}
