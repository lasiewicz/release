using System;
using System.Collections;

using Matchnet.List;

namespace Matchnet.Member.MemberCache
{
	public class Members
	{
		private ArrayList _MemberList;

		public Members()
		{
			_MemberList = new ArrayList();
		}

		public void Populate(int viewerMemberID, int privateLabelID, int[] memberIDs)
		{
			string uri = MemberCacheConfig.GetInstance().GetURI(viewerMemberID);
			MemberCache memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), uri);
			//YNMList ynmList = memberCache.GetMemberListStatus(viewerMemberID, privateLabelID, memberIDs);

			foreach (int memberID in memberIDs)
			{
				uri = MemberCacheConfig.GetInstance().GetURI(memberID);
				memberCache = (MemberCache)Activator.GetObject(typeof(MemberCache), uri);
				Member member = memberCache.GetMember(memberID, privateLabelID, AttributeConfig.AttributeGroupType.MiniProfile); 

				//populate hotlist values

				//populate YNM values

				_MemberList.Add(member);
			}
		}

		public ArrayList MemberList
		{
			get
			{
				return _MemberList;
			}
		}

	}
}
