using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace Matchnet.Member.MemberCache
{
	[Serializable]
	public class CachedMember
	{
		private ArrayList _LoadedBasePrivateLabels;
		private int _MemberID;
		private DateTime _CacheInsertDate;
		private CachedMemberAttributes _Attributes;

		public CachedMember()
		{
			_CacheInsertDate = DateTime.Now;
			_LoadedBasePrivateLabels = new ArrayList();
			_Attributes = new CachedMemberAttributes();
		}


		public bool IsPrivateLabelLoaded(int basePrivateLabelID)
		{
			return _LoadedBasePrivateLabels.Contains(basePrivateLabelID);
		}


		public ArrayList LoadedBasePrivateLabels
		{
			get
			{
				return _LoadedBasePrivateLabels;
			}
			set
			{
				_LoadedBasePrivateLabels = value;
			}
		}


		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}


		public CachedMemberAttributes Attributes
		{
			get
			{
				return _Attributes;
			}
			set
			{
				_Attributes = value;
			}
		}


		public DateTime CacheInsertDate
		{
			get
			{
				return _CacheInsertDate;
			}
			set
			{
				_CacheInsertDate = value;
			}
		}


		public override int GetHashCode()
		{
			return _MemberID;
		}
	}
}
