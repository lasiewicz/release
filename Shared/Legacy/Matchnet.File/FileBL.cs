using System;
using System.Data;
using System.IO;
using System.Web;

using Matchnet.Lib;
using Matchnet.Content;
using Matchnet.DataTemp;

namespace Matchnet.File 
{
	public class FileBL
	{
		public const int MAX_FILE_SIZE = 512000;

		public static FileBL Instance = new FileBL();

		private FileBL() 
		{

		}


		public File Load(int fileID)
		{
			// insert into mnFile get info
			SQLDescriptor descriptor = new SQLDescriptor("mnFile", fileID);
			SQLClient client = new SQLClient(descriptor);
	
			client.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
			
			DataTable dt = client.GetDataTable("up_File_List", CommandType.StoredProcedure);

			if (dt.Rows.Count == 1)
			{
				DataRow row = dt.Rows[0];

				File file = new File();
				file.FileID = System.Convert.ToInt32(row["FileID"].ToString());
				file.RefCount = System.Convert.ToInt32(row["RefCount"].ToString());
				file.InsertDate = System.Convert.ToDateTime(row["InsertDate"].ToString());
				file.ActiveDate = System.Convert.ToDateTime(row["ActiveDate"].ToString());
				file.Path = System.Convert.ToString(row["Path"].ToString());
				file.WebPath = System.Convert.ToString(row["WebPath"].ToString());

				return file;
			}

			return null;
		}


		public FileSaveResult Save(string textData)
		{
			return Save(System.Text.Encoding.ASCII.GetBytes(textData), false, "txt");
		}


		public FileSaveResult Save(byte[] fileData, bool isImage, string extension)
		{
			FileSaveResult fileSaveResult = new FileSaveResult();

			if (fileData.Length > MAX_FILE_SIZE)
			{
				fileSaveResult.Success = false;
				fileSaveResult.ResourceID = Translator.GetResourceID("MAXIMUM_FILE_SIZE_EXCEEDED");
				return fileSaveResult;
			}

			if (isImage)
			{
				extension = "jpg";
			}
				
			GetNewFileInfo(extension, fileSaveResult.File);

			// create directory 
			FileInfo info = new FileInfo(fileSaveResult.File.Path);
			Directory.CreateDirectory(info.Directory.ToString());

			if (isImage)
			{
				try
				{
					System.Drawing.Image image = System.Drawing.Image.FromStream(new MemoryStream(fileData));
					image.Save(fileSaveResult.File.Path, System.Drawing.Imaging.ImageFormat.Jpeg); // all image formats are converted to jpg
				}
				catch(System.ArgumentException)
				{ 
					fileSaveResult.Success = false;
					fileSaveResult.ResourceID = Translator.GetResourceID("INVALID_PHOTO_TYPE");
					return fileSaveResult;
				}
			}
			else
			{
				FileStream fs = new FileStream(fileSaveResult.File.Path, FileMode.CreateNew);
				fs.Write(fileData, 0, fileData.Length);
				fs.Close();
			}

			fileSaveResult.Success = true;
			return fileSaveResult;
		}


		private void GetNewFileInfo(string extension, File file)
		{
			// get new primary key for file
			int fileID = SQLClient.GetPrimaryKey("FileID");

			// insert into mnFile get info
			SQLDescriptor descriptor = new SQLDescriptor("mnFile", fileID);
			SQLClient client = new SQLClient(descriptor);
	
			client.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
			client.AddParameter("@Extension", SqlDbType.VarChar, ParameterDirection.Input, extension); 
			
			DataTable dt = client.GetDataTable("up_file_insert", CommandType.StoredProcedure);
			DataRow row = dt.Rows[0];

			file.FileID = System.Convert.ToInt32(row["FileID"].ToString());
			file.RefCount = System.Convert.ToInt32(row["RefCount"].ToString());
			file.InsertDate = System.Convert.ToDateTime(row["InsertDate"].ToString());
			file.ActiveDate = System.Convert.ToDateTime(row["ActiveDate"].ToString());
			file.Path = System.Convert.ToString(row["Path"].ToString());
			file.WebPath = System.Convert.ToString(row["WebPath"].ToString());
		}


	}
}
