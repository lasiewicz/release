using System;

using Matchnet.Lib;

namespace Matchnet.File
{
	public class FileSaveResult
	{
		private int _resourceID = Matchnet.Constants.NULL_INT;
		private bool _success = false;
		private File _file;

		public FileSaveResult()
		{
			_file = new File();
		}


		public int ResourceID
		{
			get
			{
				return _resourceID;
			}
			set
			{
				_resourceID = value;
			}
		}


		public bool Success
		{
			get
			{
				return _success;
			}
			set
			{
				_success = value;
			}
		}


		public File File
		{
			get
			{
				return _file;
			}
		}
	}
}
