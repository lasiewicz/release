using System;

using Matchnet.Lib;


namespace Matchnet.File
{
	public class File
	{
		private int _fileID = Matchnet.Constants.NULL_INT;
		private int _thumbFileID = Matchnet.Constants.NULL_INT;
		private int _refCount = Matchnet.Constants.NULL_INT;
		private DateTime _insertDate;
		private DateTime _activeDate;
		private string _path = "";
		private string _webPath = "";
		private int _fileSize = 0;

		#region public properties

		public int FileID
		{
			get
			{
				return _fileID;
			}
			set
			{
				_fileID = value;
			}
		}


		public int ThumbFileID
		{
			get
			{
				return _thumbFileID;
			}
			set
			{
				_thumbFileID = value;
			}
		}


		public int RefCount
		{
			get
			{
				return _refCount;
			}
			set
			{
				_refCount = value;
			}
		}


		public DateTime InsertDate
		{
			get
			{
				return _insertDate;
			}
			set
			{
				_insertDate = value;
			}
		}


		public DateTime ActiveDate
		{
			get
			{
				return _activeDate;
			}
			set
			{
				_activeDate = value;
			}
		}


		public string Path
		{
			get
			{
				return _path;
			}
			set
			{
				_path = value;
			}
		}


		public string WebPath
		{
			get
			{
				return _webPath;
			}
			set
			{
				_webPath = value;
			}
		}


		public int FileSize
		{
			get
			{
				return _fileSize;
			}
			set
			{
				_fileSize = value;
			}
		}

		#endregion

	}
}
