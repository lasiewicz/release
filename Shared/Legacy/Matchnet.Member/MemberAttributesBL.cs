using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mail;

using Matchnet.Lib;
using Matchnet.Content;
using Matchnet.Lib.Util;
using Matchnet.DataTemp;
using Matchnet.Labels;
using Matchnet.CachingTemp;
using Matchnet.Email.Classification;


namespace Matchnet.Member
{
	public class MemberAttributesBL
	{
		private int _MemberID;
		private string _UserName;
		private static Matchnet.CachingTemp.Cache _Cache;
		private static Matchnet.Content.Translator _Translator;

		public MemberAttributesBL()
		{
			_Translator = new Translator(_Cache);
		}
		
		static MemberAttributesBL()
		{
			_Cache = Cache.GetInstance();
		}

		public string UserName
		{
			get
			{
				return _UserName;
			}
			set
			{
				_UserName = value;
			}
		}

		public int MemberID 
		{
			get 
			{
				return _MemberID;
			}

			set 
			{
				_MemberID = value;
			}
		}


		public static void Save(int memberID, int privateLabelID, MemberAttributes memberAttributes)
		{
			int domainID;
			int languageID;
			string emailAddress = string.Empty;
			string username = string.Empty;
			string password = string.Empty;
			PrivateLabel privateLabel = new PrivateLabel();

			privateLabel.Load(privateLabelID);
			domainID = privateLabel.DomainID;
			languageID = privateLabel.LanguageID;

			foreach (string key in memberAttributes.AllKeys)
			{
				MemberAttribute attribute = memberAttributes[key];
				switch (attribute.Name.ToLower())
				{
					case "emailaddress":
						emailAddress = attribute.Value;
						break;

					case "username":
						username = attribute.Value;
						break;

					case "password":
						password = attribute.Value;
						break;

					default:
						SaveAttribute(memberID, privateLabelID, domainID, languageID, attribute);
						break;
				}
			}
		
			if (emailAddress != string.Empty || username != string.Empty || password != string.Empty)
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnLogon", 0);
				SQLClient client = new SQLClient(descriptor);
		
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);

				if (emailAddress != string.Empty)
				{
					client.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailAddress);
				}

				if (username != string.Empty)
				{
					client.AddParameter("@Username", SqlDbType.VarChar, ParameterDirection.Input, username);
				}

				if (password != string.Empty)
				{
					client.AddParameter("@Username", SqlDbType.VarChar, ParameterDirection.Input, password);
				}
			}
		}


		internal static void SaveAttribute(int memberID, int privateLabelID, int domainID, int languageID, MemberAttribute attribute)
		{
			Matchnet.Lib.Util.Util.menmOperatorIDs op = Matchnet.Lib.Util.Util.menmOperatorIDs.opNone;
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
		
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter("@AttributeName",SqlDbType.VarChar, ParameterDirection.Input, attribute.Name);
			client.AddParameter("@Value",SqlDbType.NVarChar, ParameterDirection.Input, attribute.Value);
			// 1 = broadcast immediately, used only for GlobalStatusMask
			client.AddParameter("@ActionMask", SqlDbType.Int, ParameterDirection.Input, (attribute.Name.ToLower() == "globalstatusmask") ? 1 : 0);

			switch (AttributeConfig.GetAttributeConfig(attribute.Name).DataType)
			{
				case AttributeConfig.AttributeDataType.Number:
					op = Matchnet.Lib.Util.Util.menmOperatorIDs.opEqual;
					break;

				case AttributeConfig.AttributeDataType.Mask:
					op = Matchnet.Lib.Util.Util.menmOperatorIDs.opEqual;
					break;

				case AttributeConfig.AttributeDataType.Text:
					client.AddParameter("@ApprovedFlag", SqlDbType.Bit, ParameterDirection.Input, (attribute.ApprovalStatus == MemberAttribute.ApprovalStatusType.Auto || attribute.ApprovalStatus == MemberAttribute.ApprovalStatusType.Human) ? true : false);
					client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
					break;
			}

			client.AddParameter("@OperationTypeId", SqlDbType.Int, ParameterDirection.Input, (int)op);
			client.ExecuteNonQuery("up_MemberAttribute_Save",CommandType.StoredProcedure);
		}

		public static void SaveAttribute(int memberID, int privateLabelID, MemberAttribute attribute)
		{
			PrivateLabel pl = new PrivateLabel();
			pl.Load(privateLabelID);
			int domainID = pl.DomainID;
			int languageID = pl.LanguageID;

			AttributeConfig config = AttributeConfig.GetAttributeConfig(attribute.Name);

			switch (config.Type)
			{
				case AttributeConfig.AttributeType.FreeText:
					SaveAttributeText(memberID,
						domainID,
						privateLabelID,
						languageID,
						attribute.Name,
						attribute.Value,
						0,
						false,
						0);
					break;
				case AttributeConfig.AttributeType.SingleSelect:
					SaveAttributeInt(memberID,
						domainID,
						privateLabelID,
						attribute.Name,
						Convert.ToInt32(attribute.Value),
						0);
					break;
				case AttributeConfig.AttributeType.MultiSelect:
					SaveAttributeMultiSelect(memberID,
						domainID,
						privateLabelID,
						languageID,
						attribute.Name,
						attribute.Value,
						0,
						0);
					break;
			}
		}

		private static void SaveAttributeText(int memberID, int domainID, int privateLabelID, int languageID, string attributeName,
			string attributeValue, int operationTypeID, bool approvedFlag, int actionMask)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
		
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
			client.AddParameter("@AttributeName",SqlDbType.VarChar, ParameterDirection.Input, attributeName);
			client.AddParameter("@Value",SqlDbType.NVarChar, ParameterDirection.Input, attributeValue);
			client.AddParameter("@OperationTypeId", SqlDbType.Int, ParameterDirection.Input, operationTypeID);
			client.AddParameter("@ApprovedFlag", SqlDbType.Bit, ParameterDirection.Input, approvedFlag);
			client.AddParameter("@ActionMask", SqlDbType.Int, ParameterDirection.Input, actionMask);
            
			client.ExecuteNonQuery("up_MemberAttribute_Save",CommandType.StoredProcedure);
		}

		private static void SaveAttributeInt(int memberID, int domainID, int privateLabelID, string attributeName, int attributeValue, int actionMask)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
		
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter("@AttributeName",SqlDbType.VarChar, ParameterDirection.Input, attributeName);
			client.AddParameter("@Value",SqlDbType.NVarChar, ParameterDirection.Input, attributeValue.ToString());            
			client.AddParameter("@ActionMask", SqlDbType.Int, ParameterDirection.Input, actionMask);
			
			client.ExecuteNonQuery("up_MemberAttribute_Save",CommandType.StoredProcedure);
		}

		public static void SaveAttributeMultiSelect(int memberID, int domainID, int privateLabelID, int languageID, string attributeName,
			string attributeValue, int operationTypeID, int actionMask)
		{

			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);

			attributeValue = Util.GetMaskValue(attributeValue);
		
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
			client.AddParameter("@AttributeName",SqlDbType.VarChar, ParameterDirection.Input, attributeName);
			client.AddParameter("@Value",SqlDbType.NVarChar, ParameterDirection.Input, attributeValue);
			client.AddParameter("@OperationTypeId", SqlDbType.Int, ParameterDirection.Input, operationTypeID);
			client.AddParameter("@ActionMask", SqlDbType.Int, ParameterDirection.Input, actionMask);
            
			client.ExecuteNonQuery("up_MemberAttribute_Save",CommandType.StoredProcedure);
		}


		public void Update(int memberID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnLogon");
			SQLClient client = new SQLClient(descriptor);
			SqlCommand cmd = new SqlCommand();

			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "Up_member_Update";

			client.AddParameter("@MemberID",
				SqlDbType.Int,
				ParameterDirection.Input, memberID);
			client.AddParameter("@ReturnValue",
				SqlDbType.Int,
				ParameterDirection.ReturnValue);

			client.PopulateCommand(cmd);
			client.ExecuteNonQuery(cmd);
		
		}

		public int Register(string emailAddress, string userName,
			string passWord, bool sendConfirmation, int privateLabelID)
		{
			
			
			int chkUsername = IsValidUserName(userName);
			if (chkUsername != 0) return chkUsername;
			
			
			//check for the password length
			if (!IsPasswordLengthOk(passWord))
				return Translator.GetResourceID("Password_must_be_between_4_and_16_characters_long");
			
			//check for the correct emailaddress format
			if (! IsValidEmailAddress(emailAddress))
			{
				return Translator.GetResourceID("EMAIL_ADDRESS_NOT_ALLOWED");
			}
			SQLDescriptor descriptor = new SQLDescriptor("mnLogon");	
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@EmailAddress" 
				, SqlDbType.NVarChar 
				, ParameterDirection.Input
				, emailAddress , 250);
			client.AddParameter("@UserName" 
				, SqlDbType.NVarChar 
				, ParameterDirection.Input 
				, userName , 50);
			client.AddParameter("@Password" 
				, SqlDbType.NVarChar 
				, ParameterDirection.Input 
				, passWord , 50);
			client.AddParameter("@ReturnValue"
				, SqlDbType.Int
				, ParameterDirection.ReturnValue);
			SqlCommand command = new SqlCommand();
			command.CommandType=CommandType.StoredProcedure;
			command.CommandText="Up_Member_Register";
			client.PopulateCommand(command);
			DataTable dt = client.GetDataTable(command);
			//check if username contained any part of email address and user was assigned a different username
			int err = Util.CInt(command.Parameters["@ReturnValue"].Value);
			if ((err == ConstantsTemp.ERROR_SUCCESS) & (dt.Rows.Count > 0))
			{
				_MemberID = Util.CInt(dt.Rows[0]["MemberID"]);
				_UserName = Util.CString(dt.Rows[0]["UserName"]);

				if ( _UserName != userName & 
					!usernameHasEmail(userName, emailAddress) )
				{
					//send a confirmation email
					SendRemoteRegEmail(dt, privateLabelID);
					//
					return Translator.GetResourceID("USER_NAME_IN_USE");
				}
				else if (_UserName != userName & 
					usernameHasEmail(userName, emailAddress))
				{
					//send a confirmation email
					SendRemoteRegEmail(dt, privateLabelID);
					return Translator.GetResourceID("USERNAME_CONTAINS_EMAIL_ADDRESS");	
				}
				//send a confirmation email
				//Temporarily commented out
				SendRemoteRegEmail(dt,privateLabelID);
				return ConstantsTemp.ERROR_SUCCESS;
			}

			return err;
		}


		private static bool IsValidEmailAddress(string emailAddress)
		{
			//Do we have any work to do?
			if (emailAddress.Length == 0) return false;
			//check for the @ character
			int atsignPos = emailAddress.IndexOf("@");
			if (atsignPos <= 0 ) return false;
			//check for "." after "@"
			int dotPos=emailAddress.IndexOf(".");
			if (dotPos < atsignPos) return false;
			//'postmaster' addresses are blocked
			if (emailAddress.Substring(0,10) == "postmaster" ) return false;
			//check list of blocked domains for this address
			string domainName = emailAddress.Substring(atsignPos + 1);
			SQLDescriptor descriptor = new SQLDescriptor("mnShared");
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@DomainName"
				, SqlDbType.NVarChar
				, ParameterDirection.Input
				, domainName , 64);
			client.AddParameter("@IsBlocked"
				, SqlDbType.Bit
				, ParameterDirection.Output);
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_isBlockedDomain";
			client.PopulateCommand(command);
			client.ExecuteNonQuery(command);
			bool isBlocked = Util.CBool(command.Parameters["@IsBlocked"].ToString(), false);
			if (isBlocked) return false;
			// If we have reached this far the email address is Ok
			return true;
			
		}

		private static int IsValidUserName(string userName)
		{
			//username can't contain "@" or "." 
			if (userName.IndexOf("@") != -1 || userName.IndexOf(".") != -1)
			{
				return Translator.GetResourceID("Username_can_not_contain__or_");
			}
			else
			{
				return ConstantsTemp.ERROR_SUCCESS;
			}
		}

		private bool usernameHasEmail(string userName, string emailAddress)
		{
			//let's get the username part of email address
			string emailUsername = emailAddress.Substring(0,emailAddress.IndexOf("@")-1);
			if (userName.IndexOf(emailUsername) != -1 )
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private static bool IsPasswordLengthOk(string passWord)
		{
			if (passWord.Length <= ConstantsTemp.MAX_PASSWORD_LENGTH & 
				passWord.Length >= ConstantsTemp.MIN_PASSWORD_LENGTH)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private void SendRemoteRegEmail(DataTable memberTable, int privateLabelID)
		{
			int memberID;
			string toAddress;
			string userName;
			string passWord;
			string msgBody;
			string verfiyCode;
			string urlRoot;
			string verifyURL;
			String brandName;
			memberID = (int) memberTable.Rows[0]["MemberID"];
			userName = (string) memberTable.Rows[0]["userName"];
			passWord = (string) memberTable.Rows[0]["Password"];
			toAddress = (string) memberTable.Rows[0]["EmailAddress"];
			PrivateLabel pl = new PrivateLabel();
			pl.Load(privateLabelID);
			//making VERIFY_CODE
			//-----------------
			akxString.EncryptClass objEncrypt = new akxString.EncryptClass();
			objEncrypt.Initialize(ConstantsTemp.ENCRYPT_KEY);
			verfiyCode = ConstantsTemp.ENCRYPT_PREFIX + objEncrypt.Encode(toAddress + ConstantsTemp.ENCRYPT_DELIM + DateTime.Now.ToString());
			 
			//
			brandName = pl.URI;
			brandName = brandName.Substring(0,1).ToUpper() + brandName.Substring(1);
			urlRoot = "http://www." + pl.URI;
			verifyURL = urlRoot + "/default.asp?a=" + ConstantsTemp.ACTION_MEMBER_SERVICES_VERIFY_EMAIL + "&VerifyCode=" + verfiyCode;
			_Translator.AddToken("VERIFY_CODE",verfiyCode);
			_Translator.AddToken("VERIFY_EMAIL_LINK", verifyURL);
			_Translator.AddToken("SITEURL", urlRoot + "/default.asp?a=" + ConstantsTemp.ACTION_MEMBER_SERVICES_VERIFY_EMAIL);
			_Translator.AddToken("USERNAME",userName);
			_Translator.AddToken("BRANDNAME",brandName);
			_Translator.AddToken("TO_ADDRESS",toAddress);
			_Translator.AddToken("PASSWORD",passWord);
			msgBody = _Translator.Value("CONFIRMATION_EMAIL_EXTERNAL"
				,pl.LanguageID
				,pl.PrivateLabelID);
			MailMessage confirmMail = new MailMessage();
			confirmMail.Subject = _Translator.Value("APPLET_WELCOME",pl.LanguageID,pl.PrivateLabelID)
				+ pl.URI;
			confirmMail.Body = msgBody;
			confirmMail.From = "MemberServices@" + pl.URI;
			confirmMail.To = toAddress;
			confirmMail.BodyFormat = MailFormat.Text;

			// X-Header Stuff
			try 
			{
				Classification classification = new Classification();
				classification.Populate(new EmailTypeConverter().ToInt(EmailType.ActivationLetter),
					pl.PrivateLabelID, memberID);

				confirmMail.From = classification.Account + "@" + pl.URI;

				HeaderCollectionEnumerator enumerator = classification.Headers.GetHeaderEnumerator();

				while (enumerator.MoveNext()) 
				{
					Header header = (Header)enumerator.Current;
					confirmMail.Headers.Add(header.Name, header.Value);
				}
			} 
			catch {} 

			SmtpMail.SmtpServer = "localhost";
			SmtpMail.Send(confirmMail);
		}

		/// <summary>
		/// mnLogon DB - Checks login and password
		/// </summary>
		/// <param name="login">Email Address</param>
		/// <param name="password"></param>
		/// <returns></returns>
		public int Logon(string login, string password)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnLogon");
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@EmailAddress"
				,SqlDbType.NVarChar
				,ParameterDirection.Input
				,login
				,255);
			client.AddParameter("@Password"
				,SqlDbType.NVarChar
				,ParameterDirection.Input
				,password
				,50);
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_Member_logon";
			client.PopulateCommand(command);
			DataTable dt = client.GetDataTable(command);
			if  (dt.Rows.Count == 0)
			{
				return Translator.GetResourceID("INVALID_LOGIN_CREDENTIALS");
			}
			else
			{
				_MemberID = Util.CInt(dt.Rows[0]["MemberID"]);

				return ConstantsTemp.ERROR_SUCCESS;
			}
			
		}

		/// <summary>
		/// mnMember - Records Domain and PrivateLabels(MemberDomain, MemberPrivateLabel)
		/// To do - Populate member data from the returned datatable
		/// </summary>
		public void Logon(string emailAddress, int memberID, int privateLabelID, int domainID, string userName)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter
				("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter
				("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter
				("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter
				("@EmailAddress", SqlDbType.NVarChar, ParameterDirection.Input, emailAddress);
			client.AddParameter
				("@UserName", SqlDbType.NVarChar, ParameterDirection.Input, userName);

			DataTable dt = client.GetDataTable("up_Member_Logon", CommandType.StoredProcedure);
		}

	}
}
