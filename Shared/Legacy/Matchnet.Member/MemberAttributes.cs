using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml.Serialization;

using Matchnet.Labels;
using Matchnet.Lib;
using Matchnet.Lib.Util;

namespace Matchnet.Member
{
	[Serializable]
	[XmlInclude(typeof(MemberAttribute))]
	public class MemberAttributes : System.Collections.Specialized.NameObjectCollectionBase
	{
		private DictionaryEntry _de = new DictionaryEntry();

		public MemberAttributes()
		{
		}


		#region collection members
		public MemberAttributes(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) 
		{
			System.Runtime.Serialization.SerializationInfoEnumerator
				infoItems = info.GetEnumerator();
			while(infoItems.MoveNext()) 
			{
				Add((MemberAttribute)infoItems.Value);
			}
		}


		public override void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
		{
			foreach(string name in base.BaseGetAllKeys()) 
			{
				try 
				{
					info.AddValue(name, base.BaseGet(name));
				} 
				catch(Exception){}
			}
		}
		

		public MemberAttributes( IDictionary d, Boolean bReadOnly )  
		{
			foreach ( DictionaryEntry de in d )  
			{
				this.BaseAdd( (String) de.Key, de.Value );
			}
			this.IsReadOnly = bReadOnly;
		}


		public DictionaryEntry this[ int index ]  
		{
			get  
			{
				_de.Key = this.BaseGetKey(index);
				_de.Value = this.BaseGet(index);
				return( _de );
			}
		}


		public MemberAttribute this[ String key ]  
		{
			get  
			{
				return((MemberAttribute)this.BaseGet(key.ToLower()));
			}
			set  
			{
				this.BaseSet( key, value );
			}
		}


		public String[] AllKeys  
		{
			get  
			{
				return( this.BaseGetAllKeys() );
			}
		}


		public Array AllValues  
		{
			get  
			{
				return( this.BaseGetAllValues() );
			}
		}


		public String[] AllStringValues  
		{
			get  
			{
				return( (String[]) this.BaseGetAllValues( Type.GetType( "System.String" ) ) );
			}
		}


		public Boolean HasKeys  
		{
			get  
			{
				return( this.BaseHasKeys() );
			}
		}


		public void Add(int privateLabelID, string name, string value)
		{
			Add(privateLabelID, name, value, MemberAttribute.ApprovalStatusType.None);
		}


		public void Add(int privateLabelID, string name, string value, MemberAttribute.ApprovalStatusType approvalStatus)
		{
			MemberAttribute attribute = new MemberAttribute();
			attribute.Name = name;
			attribute.Value = value;
			attribute.PrivateLabelID = privateLabelID;
			attribute.ApprovalStatus = approvalStatus;
			Add(attribute);
		}


		public void Add(MemberAttribute attribute)
		{
			string key = GetAttributeKey(attribute);

			MemberAttribute existingAttribute = this[key];
			if (existingAttribute != null)
			{
				existingAttribute.Value = attribute.Value;
				existingAttribute.ApprovalStatus = attribute.ApprovalStatus;
			}
			else
			{
				this.BaseAdd(key, attribute);
			}
		}


		public void Add(System.Collections.DictionaryEntry de)
		{
			this.BaseAdd((String)de.Key, de.Value);
		}


		public void Remove( String key )  
		{
			this.BaseRemove( key );
		}


		public void Remove( int index )  
		{
			this.BaseRemoveAt( index );
		}


		public void Clear()  
		{
			this.BaseClear();
		}

		#endregion

		#region value methods
		public string ValueApproved(int privateLabelID, int memberID, int viewerMemberID, string resourceVal, string attributeName)
		{
			return ValueApproved(privateLabelID, memberID, viewerMemberID, resourceVal, new string[] {attributeName});
		}


		public string ValueApproved(int privateLabelID, int memberID, int viewerMemberID, string resourceVal, string[] attributeNames)
		{
			bool foundUnapproved = false;
			string val = string.Empty;

			foreach (string attributeName in attributeNames)
			{
				MemberAttribute.ApprovalStatusType approvalStatus;
				val = Value(privateLabelID, attributeName, out approvalStatus);
				if (viewerMemberID != memberID && approvalStatus != MemberAttribute.ApprovalStatusType.Auto && approvalStatus != MemberAttribute.ApprovalStatusType.Human)
				{
					foundUnapproved = true;
					val = string.Empty;
				}
				else
				{
					break;
				}
			}

			if (val == string.Empty && foundUnapproved)
			{
				val = resourceVal;
			}

			return val;
		}

		public string Value(int privateLabelID, string attributeName)
		{
			return Value(privateLabelID, attributeName, string.Empty);
		}


		public string Value(int privateLabelID, string attributeName, out MemberAttribute.ApprovalStatusType approvalStatus)
		{
			approvalStatus = MemberAttribute.ApprovalStatusType.None;
			return Value(privateLabelID, attributeName, string.Empty, out approvalStatus);
		}


		public string Value(int privateLabelID, string attributeName, string defaultValue)
		{
			MemberAttribute.ApprovalStatusType approvalStatus;
			return Value(privateLabelID, attributeName, defaultValue, out approvalStatus);
		}
			

		public string Value(int privateLabelID, string attributeName, string defaultValue, out MemberAttribute.ApprovalStatusType approvalStatus)
		{
			string attributeKey;
			MemberAttribute memberAttribute = null;
			int basePrivateLabelID = Labels.PrivateLabel.GetBasePrivateLabelID(privateLabelID);

			approvalStatus = MemberAttribute.ApprovalStatusType.None;

			//look for non-global value
			attributeKey = GetAttributeKey(attributeName, basePrivateLabelID, false);
			memberAttribute = this[attributeKey];
			if (memberAttribute != null)
			{
				approvalStatus = memberAttribute.ApprovalStatus;
				return memberAttribute.Value;
			}

			//look for global value
			attributeKey = GetAttributeKey(attributeName, basePrivateLabelID, true);
			memberAttribute = this[attributeKey];
			if (memberAttribute != null)
			{
				approvalStatus = memberAttribute.ApprovalStatus;
				return memberAttribute.Value;
			}

			//return default value
			return defaultValue;
		}


		public int ValueInt(int privateLabelID, string attributeName)
		{
			return ValueInt(privateLabelID, attributeName, Matchnet.Constants.NULL_INT);
		}


		public int ValueInt(int privateLabelID, string attributeName, int defaultValue)
		{
			double val;

			if (double.TryParse(Value(privateLabelID, attributeName), System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out val))
			{
				return (int)val;
			}

			return defaultValue;
		}


		public DateTime ValueDate(int privateLabelID, string attributeName)
		{
			return ValueDate(privateLabelID, attributeName, DateTime.MinValue);
		}


		public DateTime ValueDate(int privateLabelID, string attributeName, DateTime defaultValue)
		{
			string val;

			val = Value(privateLabelID, attributeName);

			if (val.Length > 0)
			{
				return DateTime.Parse(val);
			}

			return defaultValue;
		}
        

		public bool ValueBool(int privateLabelID, string attributeName)
		{
			return ValueBool(privateLabelID, attributeName, false);
		}


		public bool ValueBool(int privateLabelID, string attributeName, bool defaultValue)
		{
			string val;

			val = Value(privateLabelID, attributeName);

			if (val.Length > 0)
			{
				return (val == "1");
			}

			return defaultValue;
		}
		#endregion

		#region static members
		private static string GetAttributeKey(MemberAttribute attribute)
		{

			return GetAttributeKey(attribute.Name, attribute.BasePrivateLabelID, attribute.IsGlobal);
		}


		private static string GetAttributeKey(string attributeName, int basePrivateLabelID, bool isGlobal)
		{
			if (!isGlobal)
			{
				return attributeName.ToLower() + ":" + basePrivateLabelID;
			}
			else
			{
				return attributeName.ToLower();
			}
		}
		#endregion
	}
}
