using System;
using System.Data;

using Matchnet.CachingTemp;
using Matchnet.Lib;

namespace Matchnet.Member
{
	[Serializable]
	public class MemberAttribute
	{
		private string _Name = "";
		private string _Value = string.Empty;
		private int _PrivateLabelID;
		private int _BasePrivateLabelID;
		private bool _BasePrivateLabelIDSet;
		private ApprovalStatusType _ApprovalStatus = ApprovalStatusType.None;
		private bool _IsGlobal = false;
		//private bool _IsGlobalSet = false;

		public enum ApprovalStatusType : int
		{
			None = Matchnet.Constants.NULL_INT,
			Pending = 1,
			Auto = 2,
			Human = 4
		}

		public string Name
		{
			get
			{
				return _Name;
			}
			set
			{
				_Name = value;
			}
		}


		public string Value
		{
			get
			{
				return _Value;
			}
			set
			{
				_Value = value;
			}
		}


		public int PrivateLabelID
		{
			get
			{
				return _PrivateLabelID;
			}
			set
			{
				_PrivateLabelID = value;
			}
		}

		
		public int BasePrivateLabelID
		{
			get
			{
				if (!_BasePrivateLabelIDSet)
				{
					_BasePrivateLabelID = Labels.PrivateLabel.GetBasePrivateLabelID(_PrivateLabelID);
					_BasePrivateLabelIDSet = true;
				}
				return _BasePrivateLabelID;
			}
		}
		
		public ApprovalStatusType ApprovalStatus
		{
			get
			{
				return _ApprovalStatus;
			}
			set
			{
				_ApprovalStatus = value;
			}
		}

		public bool IsGlobal
		{
			get
			{
				/*
				if (!_IsGlobalSet)
				{
					_IsGlobal = AttributeConfig.GetAttributeConfig(_Name).IsGlobal;
					_IsGlobalSet = true;
				}
				*/
				return _IsGlobal;
			}
		}


	}
}
