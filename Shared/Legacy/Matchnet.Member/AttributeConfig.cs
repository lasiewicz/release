using System;
using System.Collections;
using System.Data;
using System.Diagnostics;

using Matchnet.Lib;
using Matchnet.DataTemp;
using Matchnet.Labels;

namespace Matchnet.Member
{
	public class AttributeConfig
	{
		private int _ID;
		private string _Name;
		private bool _IsGlobal;
		private AttributeType _Type;
		private AttributeDataType _DataType;


		public enum AttributeType : int
		{
			FreeText = 1,
			SingleSelect = 2,
			MultiSelect = 3
		}
	

		public enum AttributeDataType
		{
			Bit,
			Date,
			Mask,
			Number,
			Text
		}


		public enum AttributeGroupType : int
		{
			AdminProfile = 16,
			AwayStatus = 36,
			ChangeEmail = 64,
			Changeemailpassword = 60,
			ContactMember = 38,
			CrossPromotionCheck = 42,
			EmailSettings = 44,
			FreeTextBayesLong = 88,
			FreeTextBayesShort = 84,
			FreeText = 18,
			Gallery = 8,
			HasNewMutualYes = 66,
			HomeAttributes = 56,
			IMAttributes = 34,
			MailboxSettings = 48,
			MemberOnlineProfile = 14,
			MemberProfileEdit = 10,
			MemberSearchForm = 6,
			MemberShipping = 58,
			MemberStatus = 12,
			MembersOnline = 70,
			MemberServicesProfileDisplay = 50,
			MiniProfile = 4,
			NewIM = 52,
			NewMailProfile = 54,
			PhotoEditor = 2,
			PhotoEditorRejectReasons = 40,
			RegStep1 = 22,
			RegStep2 = 24,
			RegStep3 = 26,
			RegStep4 = 28,
			RegStep5 = 30,
			RegStep6 = 32,
			RegWelcome = 62,
			Search = 68,
			ViewProfile = 46
		}
							

		public AttributeConfig(int attributeID, string name, AttributeType type, AttributeDataType dataType, bool isGlobal)
		{
			_ID = attributeID;
			_Name = name;
			_Type = type;
			_DataType = dataType;
			_IsGlobal = isGlobal;
		}


		public static AttributeConfig GetAttributeConfig(string attributeName)
		{
			return GetAttributeConfig(GetAttributeID(attributeName));
		}

		public static AttributeConfig GetAttributeConfig(int attributeID)
		{
			string key = "AttributeConfig:" + attributeID.ToString();
			Matchnet.CachingTemp.Cache cache = Matchnet.CachingTemp.Cache.GetInstance();
			AttributeConfig config;
			bool isGlobal = false;

			object o = cache.Get(key);

			if (o != null)
			{
				return (AttributeConfig)o;
			}

			if (attributeID > 0)
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnShared");
				SQLClient client = new SQLClient(descriptor);
				client.AddParameter("@AttributeID", SqlDbType.Int, ParameterDirection.Input, attributeID);
				DataTable dt = client.GetDataTable("up_Attribute_List", CommandType.StoredProcedure);

				if (dt.Rows.Count == 1)
				{
					DataRow row = dt.Rows[0];

					if (row["AttributeStatusMask"] != DBNull.Value)
					{
						if ((Convert.ToInt32(row["AttributeStatusMask"]) & 2) == 2)
						{
							isGlobal = true;
						}
					}

					config = new AttributeConfig(Convert.ToInt32(row["AttributeID"]),
						row["AttributeName"].ToString(),
						(AttributeConfig.AttributeType)Enum.Parse(typeof(AttributeConfig.AttributeType), row["AttributeTypeID"].ToString()),
						(AttributeConfig.AttributeDataType)Enum.Parse(typeof(AttributeConfig.AttributeDataType), row["DataType"].ToString(), true),
						isGlobal);

					cache.Add(key, config, null, DateTime.Now.AddHours(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
				}
				else
				{
					throw new Exception(string.Format("Invalid attributeID [{0}]", attributeID));
				}
			}
			else
			{
				//"fake" attributes
				string attributeName = String.Empty;
				AttributeConfig.AttributeType type = AttributeType.FreeText;
				AttributeConfig.AttributeDataType dataType = AttributeDataType.Text;

				if (attributeID <= -1000)
				{
					type = AttributeType.FreeText;
					dataType = AttributeDataType.Text;
					isGlobal = false;

					if  (attributeID <= -5000)
					{
						attributeName = "ThumbPath" + ((int)System.Math.Abs(attributeID + 5000)).ToString();
					}
					else if  (attributeID <= -4000)
					{
						attributeName = "FilePath" + ((int)System.Math.Abs(attributeID + 4000)).ToString();
					}
					else if  (attributeID <= -3000)
					{
						attributeName = "PrivateFlag" + ((int)System.Math.Abs(attributeID + 3000)).ToString();
					}
					else if  (attributeID <= -2000)
					{
						attributeName = "ThumbFileID"+ ((int)System.Math.Abs(attributeID + 2000)).ToString();
					}
					else if  (attributeID <= -1000)
					{
						attributeName = "FileID" + ((int)System.Math.Abs(attributeID + 1000)).ToString();
					}
				}
				else
				{
					switch (attributeID)
					{
						case -1:
							attributeName = "EmailAddress";
							type = AttributeType.FreeText;
							dataType = AttributeDataType.Text;
							isGlobal = true;
							break;
						case -2:
							attributeName = "UserName";
							type = AttributeType.FreeText;
							dataType = AttributeDataType.Text;
							isGlobal = true;
							break;
						case -3:
							attributeName = "LastLoginDate";
							type = AttributeType.FreeText;
							dataType = AttributeDataType.Date;
							isGlobal = false;
							break;
						case -4:
							attributeName = "NumPhotos";
							type = AttributeType.FreeText;
							dataType = AttributeDataType.Number;
							isGlobal = false;
							break;
					}
				}

				if (attributeName != string.Empty)
				{
					config = new AttributeConfig(attributeID,
						attributeName,
						type,
						dataType,
						isGlobal);
				}
				else
				{
					throw new Exception(string.Format("Invalid attributeID [{0}]", attributeID));
				}
			}

			cache.Add(key, config, null, DateTime.Now.AddHours(24), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
			return config;
		}

		public static int GetAttributeID(string attributeName)
		{
			string attributeNameLower = attributeName.ToLower();
			string key = "AttributeName:" + attributeNameLower;
			Matchnet.CachingTemp.Cache cache = Matchnet.CachingTemp.Cache.GetInstance();
			int attributeID = Matchnet.Constants.NULL_INT;

			object o = cache.Get(key);

			if (o != null)
			{
				return (int)o;
			}
			else
			{
				if (attributeNameLower != "username")
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnShared");
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@AttributeName", SqlDbType.VarChar, ParameterDirection.Input, attributeName);
					DataTable dt = client.GetDataTable("up_Attribute_List_ByName", CommandType.StoredProcedure);

					if (dt.Rows.Count == 1)
					{
						attributeID = Convert.ToInt32(dt.Rows[0]["AttributeID"]);
					}
				}

				if (attributeID == Matchnet.Constants.NULL_INT)
				{
					switch (attributeNameLower)
					{
						case "emailaddress":
							attributeID = -1;
							break;

						case "username":
							attributeID = -2;
							break;

						case "lastlogindate":
							attributeID = -3;
							break;

						case "numphotos":
							attributeID = -4;
							break;

						default:
							if (attributeNameLower.IndexOf("thumbpath") == 0)
							{
								attributeID = -5000 - int.Parse(attributeName.Substring(9));
							}
							else if (attributeNameLower.IndexOf("filepath") == 0)
							{
								attributeID = -4000 - int.Parse(attributeName.Substring(8));
							}
							else if (attributeNameLower.IndexOf("privateflag") == 0)
							{
								attributeID = -3000 - int.Parse(attributeName.Substring(11));
							}
							else if (attributeNameLower.IndexOf("thumbfileid") == 0)
							{
								attributeID = -2000 - int.Parse(attributeName.Substring(11));
							}
							else if (attributeNameLower.IndexOf("fileid") == 0)
							{
								attributeID = -1000 - int.Parse(attributeName.Substring(6));
							}

							break;

					}
				}

				if (attributeID != Matchnet.Constants.NULL_INT)
				{
					cache.Add(key, attributeID, null, DateTime.Now.AddHours(24), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
					return attributeID;
				}

				throw new Exception(string.Format("Invalid attributeName [{0}]", attributeName));
			}
		}

		public static ArrayList GetAttributeGroup(int privateLabelID, AttributeGroupType attributeGroup)
		{
			string key = string.Format("AttributeGroup:{0}:{1}", attributeGroup.ToString(), privateLabelID);

			Matchnet.CachingTemp.Cache cache = Matchnet.CachingTemp.Cache.GetInstance();

			object o = cache.Get(key);

			if (o != null)
			{
				return (ArrayList)o;
			}
			else
			{
				ArrayList group = new ArrayList();

				PrivateLabel pl = new PrivateLabel();
				pl.Load(privateLabelID);
				int domainID = pl.DomainID;
				
				SQLDescriptor descriptor = new SQLDescriptor("mnShared");
				SQLClient client = new SQLClient(descriptor);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
				client.AddParameter("@AttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroup);
				DataTable dt = client.GetDataTable("up_AttributeGroupAttributeEx_List", CommandType.StoredProcedure);

				foreach (DataRow row in dt.Rows)
				{
					group.Add(Convert.ToInt32(row["AttributeID"]));
				}

				//"fake" attributes
				group.Add(-1);		//EmailAddress
				group.Add(-2);		//UserName
				group.Add(-3);		//LastLoginDate
				group.Add(-3000);	//PrivateFlag0
				group.Add(-5000);	//ThumbPath0

				cache.Add(key, group, null, DateTime.Now.AddHours(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);

				return group;
			}
		}


		public int ID
		{
			get
			{
				return _ID;
			}
		}

	
		public string Name
		{
			get
			{
				return _Name;
			}
		}

	
		public AttributeType Type
		{
			get
			{
				return _Type;
			}
		}

	
		public AttributeDataType DataType
		{
			get
			{
				return _DataType;
			}
		}


		public bool IsGlobal
		{
			get
			{
				return _IsGlobal;
			}
		}

	}
}
