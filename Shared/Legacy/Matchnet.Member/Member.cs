using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Web.Mail;

using Matchnet.Content;
using Matchnet.Labels;
using Matchnet.Lib;
using Matchnet.DataTemp;
using Matchnet.Lib.Member;
using Matchnet.MessageQueue;
using Matchnet.Lib.Util;
using Matchnet.CachingTemp;
using Matchnet.Email.Classification;

namespace Matchnet.Member
{
	[Serializable]
	[XmlInclude(typeof(MemberAttribute))]
	public class Member
	{
		private int _MemberID = Matchnet.Constants.NULL_INT;
		private string _UserName;
		private MemberAttributes _Attributes;
		private int _PrivateLabelID = Matchnet.Constants.NULL_INT;
		private int _BasePrivateLabelID = Matchnet.Constants.NULL_INT;				
		private static Matchnet.CachingTemp.Cache _Cache;
		private static Matchnet.Content.Translator _Translator;

		public Member()
		{
			_Attributes = new MemberAttributes();
			_Translator = new Translator(_Cache);
		}
		
		static Member()
		{
			_Cache = Cache.GetInstance();
		}

		public virtual void Populate(int memberID, int privateLabelID)
		{
			_MemberID = memberID;
			_BasePrivateLabelID = Labels.PrivateLabel.GetBasePrivateLabelID(privateLabelID);

			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@BasePrivateLabelID", SqlDbType.Int, ParameterDirection.Input, _BasePrivateLabelID);
			DataTable dtAttributes = client.GetDataTable("up_MemberAttribute_List_Cache", CommandType.StoredProcedure);

			foreach (DataRow row in dtAttributes.Rows)
			{
				if (row["StatusMask"] == System.DBNull.Value)
				{
					_Attributes.Add(privateLabelID,
										row["AttributeName"].ToString(),
										row["Value"].ToString());
				}
				else
				{
					_Attributes.Add(privateLabelID,
						row["AttributeName"].ToString(),
						row["Value"].ToString(),
						(MemberAttribute.ApprovalStatusType)Enum.Parse(typeof(MemberAttribute.ApprovalStatusType), row["StatusMask"].ToString()));
				}
			}
		}


		public override int GetHashCode()
		{
			return _MemberID;
		}


		public string UserName
		{
			get
			{
				return _UserName;
			}
			set
			{
				_UserName = value;
			}
		}

		public int MemberID 
		{
			get 
			{
				return _MemberID;
			}

			set 
			{
				_MemberID = value;
			}
		}

		public MemberAttributes Attributes
		{
			get
			{
				return _Attributes;
			}
			set
			{
				_Attributes = value;
			}
		}


		public bool IsNewMember(int privateLabelID)
		{
				return (DateTime.Now.Subtract(this.Attributes.ValueDate(privateLabelID, "LastUpdated", DateTime.Now)).TotalDays <= ConstantsTemp.NEW_DAY_LIMIT);
		}


		public int PrivateLabelID
		{
			get
			{
				return _PrivateLabelID;
			}
			set
			{
				_PrivateLabelID = value;
				_BasePrivateLabelID = Labels.PrivateLabel.GetBasePrivateLabelID(_PrivateLabelID);
			}
		}

		public int BasePrivateLabelID
		{
			get
			{
				return _BasePrivateLabelID;
			}
		}
		//Registering
		public int Register(string emailAddress, string userName,
							string passWord, bool sendConfirmation, int privateLabelID)
		{
			
			
			int chkUsername = IsValidUserName(userName);
			if (chkUsername != 0) return chkUsername;
			
			
			//check for the password length
			if (!IsPasswordLengthOk(passWord))
				return Translator.GetResourceID("Password_must_be_between_4_and_16_characters_long");
			
			//check for the correct emailaddress format
			if (! IsValidEmailAddress(emailAddress))
			{
				return Translator.GetResourceID("EMAIL_ADDRESS_NOT_ALLOWED");
			}
			SQLDescriptor descriptor = new SQLDescriptor("mnLogon");	
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@EmailAddress" 
								, SqlDbType.NVarChar 
								, ParameterDirection.Input
								, emailAddress , 250);
			client.AddParameter("@UserName" 
								, SqlDbType.NVarChar 
								, ParameterDirection.Input 
								, userName , 50);
			client.AddParameter("@Password" 
								, SqlDbType.NVarChar 
								, ParameterDirection.Input 
								, passWord , 50);
			client.AddParameter("@ReturnValue"
								, SqlDbType.Int
								, ParameterDirection.ReturnValue);
			SqlCommand command = new SqlCommand();
			command.CommandType=CommandType.StoredProcedure;
			command.CommandText="Up_Member_Register";
			client.PopulateCommand(command);
			DataTable dt = client.GetDataTable(command);
			//check if username contained any part of email address and user was assigned a different username
			int err = Util.CInt(command.Parameters["@ReturnValue"].Value);
			if ((err == ConstantsTemp.ERROR_SUCCESS) & (dt.Rows.Count > 0))
			{
				_MemberID = Util.CInt(dt.Rows[0]["MemberID"]);
				_UserName = Util.CString(dt.Rows[0]["UserName"]);

				if ( _UserName != userName & 
					!usernameHasEmail(userName, emailAddress) )
				{
					//send a confirmation email
					SendRemoteRegEmail(dt, privateLabelID);
					//
					return Translator.GetResourceID("USER_NAME_IN_USE");
				}
				else if (_UserName != userName & 
					usernameHasEmail(userName, emailAddress))
				{
					//send a confirmation email
					SendRemoteRegEmail(dt, privateLabelID);
					return Translator.GetResourceID("USERNAME_CONTAINS_EMAIL_ADDRESS");	
				}
				//send a confirmation email
				SendRemoteRegEmail(dt,privateLabelID);
				return ConstantsTemp.ERROR_SUCCESS;
			}

			return err;
		}

		public void Update(int memberID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnLogon");
			SQLClient client = new SQLClient(descriptor);
			SqlCommand cmd = new SqlCommand();

			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "Up_member_Update";

			client.AddParameter("@MemberID",
								 SqlDbType.Int,
								 ParameterDirection.Input, memberID);
			client.AddParameter("@ReturnValue",
								 SqlDbType.Int,
								 ParameterDirection.ReturnValue);

			client.PopulateCommand(cmd);
			client.ExecuteNonQuery(cmd);
		
		}
		private bool IsValidEmailAddress(string emailAddress)
		{
			//Do we have any work to do?
			if (emailAddress.Length == 0) return false;
			//check for the @ character
			int atsignPos = emailAddress.IndexOf("@");
			if (atsignPos <= 0 ) return false;
			//check for "." after "@"
			int dotPos=emailAddress.IndexOf(".");
			if (dotPos < atsignPos) return false;
			//'postmaster' addresses are blocked
			if (emailAddress.Substring(0,10) == "postmaster" ) return false;
			//check list of blocked domains for this address
			string domainName = emailAddress.Substring(atsignPos + 1);
			SQLDescriptor descriptor = new SQLDescriptor("mnShared");
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@DomainName"
								, SqlDbType.NVarChar
								, ParameterDirection.Input
								, domainName , 64);
			client.AddParameter("@IsBlocked"
								, SqlDbType.Bit
								, ParameterDirection.Output);
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_isBlockedDomain";
			client.PopulateCommand(command);
			client.ExecuteNonQuery(command);
			bool isBlocked = Util.CBool(command.Parameters["@IsBlocked"].ToString(), false);
			if (isBlocked) return false;
			// If we have reached this far the email address is Ok
			return true;
			
		}

		private int IsValidUserName(string userName)
		{
			//username can't contain "@" or "." 
			if (userName.IndexOf("@") != -1 || userName.IndexOf(".") != -1)
			{
				return Translator.GetResourceID("Username_can_not_contain__or_");
			}
			else
			{
				return ConstantsTemp.ERROR_SUCCESS;
			}
		}

		private bool usernameHasEmail(string userName, string emailAddress)
		{
			//let's get the username part of email address
			string emailUsername = emailAddress.Substring(0,emailAddress.IndexOf("@")-1);
			if (userName.IndexOf(emailUsername) != -1 )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		private bool IsPasswordLengthOk(string passWord)
		{
			if (passWord.Length <= ConstantsTemp.MAX_PASSWORD_LENGTH & 
				passWord.Length >= ConstantsTemp.MIN_PASSWORD_LENGTH)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		private void SendRemoteRegEmail(DataTable memberTable, int privateLabelID)
		{
			int memberID;
			string toAddress;
			string userName;
			string passWord;
			string msgBody;
			string verfiyCode;
			string urlRoot;
			string verifyURL;
			memberID = (int) memberTable.Rows[0]["MemberID"];
			userName = (string) memberTable.Rows[0]["userName"];
			passWord = (string) memberTable.Rows[0]["Password"];
			toAddress = (string) memberTable.Rows[0]["EmailAddress"];
			PrivateLabel pl = new PrivateLabel();
			pl.Load(privateLabelID);
			//making VERIFY_CODE
			//-----------------
			akxString.EncryptClass objEncrypt = new akxString.EncryptClass();
			objEncrypt.Initialize(ConstantsTemp.ENCRYPT_KEY);
			verfiyCode = ConstantsTemp.ENCRYPT_PREFIX + objEncrypt.Encode(toAddress + ConstantsTemp.ENCRYPT_DELIM + DateTime.Now.ToString());
			 
			//
			urlRoot = "http://www." + pl.URI;
//			verifyURL = urlRoot + "/default.asp?a=" + ConstantsTemp.ACTION_MEMBER_SERVICES_VERIFY_EMAIL + "&VerifyCode=" + verfiyCode;
verifyURL = urlRoot + "/applications/memberservices/verifyemail.aspx" + "&VerifyCode=" + verfiyCode;			
			_Translator.AddToken("VERIFY_CODE",verfiyCode);
			_Translator.AddToken("VERIFY_EMAIL_LINK", verifyURL);
//			_Translator.AddToken("SITEURL", urlRoot + "/default.asp?a=" + ConstantsTemp.ACTION_MEMBER_SERVICES_VERIFY_EMAIL);
			_Translator.AddToken("SITEURL", urlRoot + "/applications/memberservices/verifyemail.aspx");

			_Translator.AddToken("USERNAME",userName);
			_Translator.AddToken("BRANDNAME",pl.URI);
			_Translator.AddToken("TO_ADDRESS",toAddress);
			_Translator.AddToken("PASSWORD",passWord);
			msgBody = _Translator.Value("CONFIRMATION_EMAIL_EXTERNAL"
				,pl.LanguageID
				,pl.PrivateLabelID);
			MailMessage confirmMail = new MailMessage();
			confirmMail.Subject = _Translator.Value("APPLET_WELCOME",pl.LanguageID,pl.PrivateLabelID)
									+ pl.URI;
			confirmMail.Body = msgBody;
			confirmMail.From = "MemberServices@" + pl.URI;
			confirmMail.To = toAddress;
			confirmMail.BodyFormat = MailFormat.Text;

			// X-Header Stuff
			try 
			{
				Classification classification = new Classification();
				classification.Populate(new EmailTypeConverter().ToInt(EmailType.ActivationLetter),
					pl.PrivateLabelID, memberID);

				confirmMail.From = classification.Account + "@" + pl.URI;

				HeaderCollectionEnumerator enumerator = classification.Headers.GetHeaderEnumerator();

				while (enumerator.MoveNext()) 
				{
					Header header = (Header)enumerator.Current;
					confirmMail.Headers.Add(header.Name, header.Value);
				}
			} 
			catch {} 

			SmtpMail.SmtpServer = "localhost";
			SmtpMail.Send(confirmMail);
		}

		/// <summary>
		/// mnLogon DB - Checks login and password
		/// </summary>
		/// <param name="login">Email Address</param>
		/// <param name="password"></param>
		/// <returns></returns>
		public int Logon(string login, string password)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnLogon");
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@EmailAddress"
								,SqlDbType.NVarChar
								,ParameterDirection.Input
								,login
								,255);
			client.AddParameter("@Password"
								,SqlDbType.NVarChar
								,ParameterDirection.Input
								,password
								,50);
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_Member_logon";
			client.PopulateCommand(command);
			DataTable dt = client.GetDataTable(command);
			if  (dt.Rows.Count == 0)
			{
				return Translator.GetResourceID("INVALID_LOGIN_CREDENTIALS");
			}
			else
			{
				_MemberID = Util.CInt(dt.Rows[0]["MemberID"]);

				return ConstantsTemp.ERROR_SUCCESS;
			}
			
		}

		/// <summary>
		/// mnMember - Records Domain and PrivateLabels(MemberDomain, MemberPrivateLabel)
		/// To do - Populate member data from the returned datatable
		/// </summary>
		public void Logon(string emailAddress, int memberID, int privateLabelID, int domainID, string userName)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter
				("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter
				("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter
				("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter
				("@EmailAddress", SqlDbType.NVarChar, ParameterDirection.Input, emailAddress);
			client.AddParameter
				("@UserName", SqlDbType.NVarChar, ParameterDirection.Input, userName);

			DataTable dt = client.GetDataTable("up_Member_Logon", CommandType.StoredProcedure);
		}

	}
}
