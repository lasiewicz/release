using System;
using System.Data;
using System.Text;
using System.Web.Caching;

using Matchnet.CachingTemp;
using Matchnet.DataTemp;

namespace Matchnet.Article
{
	/// <summary>
	/// </summary>
	public class Categories
	{
		private SQLDescriptor _descriptor;

		private Matchnet.CachingTemp.Cache _Cache;
		private const int CACHE_TTL = 3600;
		private const string CACHEKEY_PREFIX = "Article:Categories:";

		public Categories()
		{
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
			_descriptor = new SQLDescriptor("mnShared", 0);
		}

		public static string MakeParentCacheKey(int CategoryID, int TranslationID)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(CACHEKEY_PREFIX);
			sb.Append("Parent:");
			sb.Append("CategoryID=");
			sb.Append(CategoryID);
			sb.Append(":TranslationID=");
			sb.Append(TranslationID);

			string retVal = sb.ToString();
			
			return(retVal);
		}

		public static string MakeChildCacheKey(int CategoryID, int TranslationID)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(CACHEKEY_PREFIX);
			sb.Append("Child:");
			sb.Append("CategoryID=");
			sb.Append(CategoryID);
			sb.Append(":TranslationID=");
			sb.Append(TranslationID);

			string retVal = sb.ToString();
			
			return(retVal);
		}

		/*
		public DataTable RetrieveParentInfo(int TranslationID, int CategoryID)
		{
			DataTable retVal = null;

			string key = MakeParentCacheKey(CategoryID, TranslationID);
			retVal = (DataTable)_Cache[key];

			if(retVal == null)
			{
				SQLClient client = new SQLClient(_descriptor);
				client.AddParameter("@CategoryExID", SqlDbType.Int, ParameterDirection.Input, CategoryID);
				client.AddParameter("@TranslationID", SqlDbType.Int, ParameterDirection.Input, TranslationID);
				retVal = client.GetDataTable("up_CategoryEx_ListParent", CommandType.StoredProcedure);

				_Cache.Insert(key,retVal,null,DateTime.Now.AddSeconds(CACHE_TTL),TimeSpan.Zero,CacheItemPriority.Normal,null);
			}

			return(retVal);
		}
		*/

		/*
		public DataTable RetrieveChildInfo(int TranslationID, int CategoryID)
		{
			DataTable retVal = null;

			string key = MakeChildCacheKey(CategoryID, TranslationID);
			retVal = (DataTable)_Cache[key];

			if(retVal == null)
			{
				SQLClient client = new SQLClient(_descriptor);
				client.AddParameter("@ParentCategoryExID", SqlDbType.Int, ParameterDirection.Input, CategoryID);
				client.AddParameter("@TranslationID", SqlDbType.Int, ParameterDirection.Input, TranslationID);
				client.AddParameter("@Constant", SqlDbType.VarChar, ParameterDirection.Output, "", 100);
				retVal = client.GetDataTable("up_CategoryEx_List", CommandType.StoredProcedure);

				_Cache.Insert(key,retVal,null,DateTime.Now.AddSeconds(CACHE_TTL),TimeSpan.Zero,CacheItemPriority.Normal,null);
			}

			return(retVal);
		}
		*/
	}
}
