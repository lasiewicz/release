using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Caching;

using Matchnet.CachingTemp;
using Matchnet.DataTemp;

namespace Matchnet.Article
{
	/// <summary>
	/// </summary>
	public class Articles
	{
		private SQLDescriptor _descriptor;

		private Matchnet.CachingTemp.Cache _Cache;
		private const int CACHE_TTL = 3600;
		private const string CACHEKEY_PREFIX = "Article:Articles:";
		private const int HEBREW_TRANSLATIONID = 1074;

		public static string MakeInfoCacheKey(int CategoryID, int TranslationID)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(CACHEKEY_PREFIX);
			sb.Append("Info:");
			sb.Append("CagegoryID=");
			sb.Append(CategoryID);
			sb.Append(":TranslationID=");
			sb.Append(TranslationID);
			
			string retVal = sb.ToString();
			
			return(retVal);
		}

		public static string MakeDataCacheKey(int ArticleID, int TranslationID)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(CACHEKEY_PREFIX);
			sb.Append("Data:");
			sb.Append("ArticleID=");
			sb.Append(ArticleID);
			sb.Append(":TranslationID=");
			sb.Append(TranslationID);

			string retVal = sb.ToString();
			
			return(retVal);
		}

		public static string MakeContentCacheKey(int ArticleID, int TranslationID)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(CACHEKEY_PREFIX);
			sb.Append("Content:");
			sb.Append("ArticleID=");
			sb.Append(ArticleID);
			sb.Append(":TranslationID=");
			sb.Append(TranslationID);

			string retVal = sb.ToString();
			
			return(retVal);
		}

		public Articles()
		{
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
			_descriptor = new SQLDescriptor("mnShared", 0);
		}

		/*
		public DataTable RetrieveInfo(int CategoryID, int TranslationID, int StartPage, int PageSize)
		{
			DataTable retVal;

			string key = MakeInfoCacheKey(CategoryID, TranslationID);
			retVal = (DataTable)_Cache.Get(key);
			if(retVal == null)
			{
				SQLClient client = new SQLClient(_descriptor);
				client.AddParameter("@CategoryExID", SqlDbType.Int, ParameterDirection.Input, CategoryID);
				client.AddParameter("@TranslationID", SqlDbType.Int, ParameterDirection.Input, TranslationID);
				client.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, StartPage);
				client.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, PageSize);
				client.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, 0);

				retVal = client.GetDataTable("up_ArticleEx_List_Page", CommandType.StoredProcedure);
				_Cache.Insert(key,retVal,null,DateTime.Now.AddSeconds(CACHE_TTL),TimeSpan.Zero,CacheItemPriority.Normal,null);
			}

			return(retVal);
		}
		*/

		/*
		public DataTable RetrieveData(int ArticleID, int TranslationID)
		{
			DataTable retVal;

			string key = MakeDataCacheKey(ArticleID, TranslationID);
			retVal = (DataTable)_Cache.Get(key);
			if(retVal == null)
			{
				SQLClient client = new SQLClient(_descriptor);
				client.AddParameter("@ArticleExID", SqlDbType.Int, ParameterDirection.Input, ArticleID);
				client.AddParameter("@TranslationID", SqlDbType.Int, ParameterDirection.Input, TranslationID);

				retVal = client.GetDataTable("up_ArticleData_Select", CommandType.StoredProcedure);
				_Cache.Insert(key,retVal,null,DateTime.Now.AddSeconds(CACHE_TTL),TimeSpan.Zero,CacheItemPriority.Normal,null);
			}

			return(retVal);
		}
		*/

		/// <summary>
		/// Retrieves the actual content for an article for a given translation.
		/// </summary>
		/// <param name="ArticleID">article identifier</param>
		/// <param name="TranslationID">translation of article to retrieve</param>
		/// <returns>article text without substitutions having been made</returns>
		/// 
		/*
		public string RetrieveArticleContent(int ArticleID, int TranslationID)
		{
			string retVal;

			string key = MakeContentCacheKey(ArticleID, TranslationID);
			retVal = (string)_Cache.Get(key);

			
			if(retVal == null)
			{
				DataTable articleData = RetrieveData(ArticleID, TranslationID);
				if(articleData.Rows.Count > 0)
				{
					object oContent = articleData.Rows[0]["Content"];
					if(oContent != null)
					{
						//	Found a good one.  Cache the result.
						retVal = oContent.ToString();

						if (TranslationID == HEBREW_TRANSLATIONID)
						{	// Translate to article unicode
							mnTranslator.IUnicodeEncoder encoder = new mnTranslator.UnicodeEncoderClass();
							try
							{
								encoder.SetMbText(retVal, 1255);
								retVal = encoder.GetUnicodeText();
							}
							catch(Exception)
							{}
						}
						_Cache.Insert(key,retVal,null,DateTime.Now.AddSeconds(CACHE_TTL),TimeSpan.Zero,CacheItemPriority.Normal,null);
					}
					else
					{
						//	We found an article, but it had a null content field
						//	Don't cache this
						retVal = "Found an article with ArticleID["
							+ ArticleID + "] and TranslationID[" + TranslationID + "] but it had null content";
					}
					
				}
				else
				{
					//	Couldn't find an article.  What to do now?
					//	Don't cache this
					retVal = "Unable to find article with ArticleID["
						+ ArticleID + "] and TranslationID[" + TranslationID + "]";
				}
			}

			return(retVal);
		}
		*/
	}
}
