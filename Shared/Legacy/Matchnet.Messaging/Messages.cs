using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;

using Matchnet.Data;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Threading;
using Matchnet.Lib.Util;

namespace Matchnet.Messaging
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Messages
	{
		public Messages()
		{
		}

        public bool HasNewEmail(int memberID, int domainID)
        {
            SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID );
            SQLClient client = new SQLClient(descriptor);

            client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input,memberID);
            client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input,domainID);

            DataTable dt = client.GetDataTable("up_MemberMail_HasNewMail", CommandType.StoredProcedure);
            
            if ( dt.Rows.Count > 0 )
            {
                return Convert.ToBoolean(dt.Rows[0]["HasNewMail"]);
            }
            else
                return false;
        }
	}
}
