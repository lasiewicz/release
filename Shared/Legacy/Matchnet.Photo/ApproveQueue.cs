using System;
using System.Data;
using System.Collections;

using Matchnet.Lib;
using Matchnet.DataTemp;
using Matchnet.Lib.Util;
using Matchnet.Lib.Exceptions;

namespace Matchnet.Photo
{
	/// <summary>
	/// Represents a queue item in member photo queue table
	/// mnMember..ApproveQueue
	/// </summary>
	public class ApproveQueue
	{
		#region Private Variables
		private int _ApproveQueueID = Matchnet.Constants.NULL_INT;
		private MemberPhoto _MemberPhoto = new MemberPhoto();
		private static ArrayList _MemberServers = new ArrayList();
		private static int _LastMemberServer = -1;
		private int _MemberServer = Matchnet.Constants.NULL_INT;

		#endregion

		#region Public Properties
		public int LastMemberServer
		{
			get
			{
				return _MemberServer;
			}
		}

		public ArrayList MemberServers
		{
			get
			{
				return _MemberServers;
			}
		}
		public int ApproveQueueID
		{
			get
			{
				return _ApproveQueueID;
			}
			set
			{
				_ApproveQueueID = value;
			}
		}
		
		public MemberPhoto MemberPhoto
		{
			get
			{
				return _MemberPhoto;
			}
			set
			{
				_MemberPhoto = value;
			}
		}

		#endregion

		public ApproveQueue()
		{
			if (_MemberServers.Count == 0)
			{
				GetMemberServers();
			}
		}

		#region Methods
		public bool GetNext(int adminMemberID)
		{
			return GetNext(adminMemberID, Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT);
		}

		public bool GetNext(int adminMemberID, int domainID)
		{
			return GetNext(adminMemberID, domainID, Matchnet.Constants.NULL_INT);
		}

		/// <summary>
		/// Returns true when a valid queue is found.
		/// </summary>
		/// <param name="adminMemberID"></param>
		/// <param name="domainID"></param>
		/// <param name="memberID"></param>
		public bool GetNext(int adminMemberID, int domainID, int memberID)
		{
			// Reinitialize photo data.
			// If nothing is found in any queue, these fields can be checked for default values.
			_ApproveQueueID = Matchnet.Constants.NULL_INT;
			_MemberPhoto = new MemberPhoto();

			// If we're not looking for a specific member, initialize the _MemberServers array
			// for the retry loop below.
			if (memberID == Matchnet.Constants.NULL_INT && _MemberServers.Count == 0) 
			{
				GetMemberServers();
			}

			// If we're not looking for a specific mebmer, and there's nothing in the current queue,
			// keep trying until we're out of queues.
			DataTable dt = null;
			for (int tries = 0; tries < (_MemberServers.Count) &&
					(dt == null || (dt.Rows.Count != 1 && memberID == Matchnet.Constants.NULL_INT)); ++tries)
			{
				SQLClient client = GetNextClient(adminMemberID, domainID, memberID);
				dt = client.GetDataTable("up_ApproveQueue_GetNext", CommandType.StoredProcedure);
			}

			// Nobody home
			if (dt.Rows.Count != 1)
			{
				return false;
			}

			// Populate the current queue item members
			DataRow row = dt.Rows[0];
			this._ApproveQueueID = Util.CInt(row["ApproveQueueID"]);
			this._MemberServer = _LastMemberServer;
			this._MemberPhoto.MemberPhotoID = Util.CInt(row["MemberPhotoID"]);
			this._MemberPhoto.MemberID = Util.CInt(row["MemberID"]);
			this._MemberPhoto.DomainID = Util.CInt(row["DomainID"]);
			// This get property will populate file
			this._MemberPhoto.FileID = Util.CInt(row["FileID"]);
			this._MemberPhoto.ApprovedFlag = Util.CInt(row["ApprovedFlag"]);
			this._MemberPhoto.ListOrder = Util.CInt(row["ListOrder"]);
			this._MemberPhoto.PrivateFlag = Util.CInt(row["PrivateFlag"]);
			//this._MemberPhoto.ThumbNail = PhotoUtil.GetAutoThumbNail(this._MemberPhoto.WebPath, 80, 104);
			// order of this is important. 
			this._MemberPhoto.ThumbNail.FileID = Util.CInt(row["ThumbFileID"]);

			return true;
		}

		/// <summary>
		/// Get a client for the queue from the next member partition.
		/// </summary>
		/// <param name="adminMemberID"></param>
		/// <param name="domainID"></param>
		/// <param name="memberID"></param>
		/// <returns>SQLClient prepopulated with arguments for up_ApproveQueue_GetNext</returns>
		private SQLClient GetNextClient(int adminMemberID, int domainID, int memberID)
		{
			SQLDescriptor descriptor;

			if (memberID == Matchnet.Constants.NULL_INT)
			{
				descriptor = new SQLDescriptor("mnMember", GetMemberPartition());
			}
			else
			{
				descriptor = new SQLDescriptor("mnMember", memberID);
			}

			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@ApproveTypeID", SqlDbType.Int, ParameterDirection.Input, 1);
			client.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);

			if (domainID != Matchnet.Constants.NULL_INT)
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);

			if (memberID != Matchnet.Constants.NULL_INT)
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);

			return client;
		}

		private void GetMemberServers()
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMaster", 0);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@LogicalDatabase", SqlDbType.NVarChar, ParameterDirection.Input, "mnMember");

			DataTable dt = client.GetDataTable("up_LogicalPhysicalDatabase_List", CommandType.StoredProcedure);
			if (dt.Rows.Count == 0) 
			{
				throw new MatchnetException("No mnMember Databases", "Matchnet.Photo.ApproveQueue.GetMemberPartition");
			}

			foreach (DataRow row in dt.Rows)
			{
				if (Util.CInt(row["Active"]) != 0)
					_MemberServers.Add(Util.CInt(row["AccessFlag"]));
			}
		}

		private int GetMemberPartition()
		{
			if (_MemberServers.Count == 0)
			{
				GetMemberServers();
			}
			
			// By using a static variable each member server will be hit by every thread.
			_LastMemberServer++;

			if (_LastMemberServer > _MemberServers.Count)
				_LastMemberServer = 0;

			return _LastMemberServer;
		}

	
		#endregion
	}

	public class ApproveQueues : System.Collections.ArrayList
	{
		/// <summary>
		/// Used for when retrieving queue items for approval
		/// </summary>
		public void Populate(int howMany, int adminMemberID)
		{
			int i = 0;
			int max = 0;

			ApproveQueue queueConfig = new ApproveQueue();

			while ((i < howMany) & (max < (queueConfig.MemberServers.Count * howMany)))
			{
				ApproveQueue queue = new ApproveQueue();
				if (queue.GetNext(adminMemberID))
				{
					this.Add(queue);
					++i;
				}
				++max;
			}
		}

		/// <summary>
		/// Used for when retrieving a specific member's photos
		/// </summary>
		public void Populate(int adminMemberID, int memberID, int domainID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@ApproveTypeID", SqlDbType.Int, ParameterDirection.Input, 1);
			client.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);

			DataTable dt = client.GetDataTable("up_ApproveQueue_GetNext", CommandType.StoredProcedure);
			if (dt.Rows.Count > 0)
			{
				foreach (DataRow row in dt.Rows)
				{
					ApproveQueue queue = new ApproveQueue();

					queue.ApproveQueueID = Util.CInt(row["ApproveQueueID"]);
					queue.MemberPhoto.MemberPhotoID = Util.CInt(row["MemberPhotoID"]);
					queue.MemberPhoto.MemberID = Util.CInt(row["MemberID"]);
					queue.MemberPhoto.DomainID = Util.CInt(row["DomainID"]);
					// This get property will populate file
					queue.MemberPhoto.FileID = Util.CInt(row["FileID"]);
					queue.MemberPhoto.ApprovedFlag = Util.CInt(row["ApprovedFlag"]);
					queue.MemberPhoto.PrivateFlag = Util.CInt(row["PrivateAlbum"]);
					//this._MemberPhoto.ThumbNail = PhotoUtil.GetAutoThumbNail(this._MemberPhoto.WebPath, 80, 104);
					// order of this is important. 
					queue.MemberPhoto.ThumbNail.FileID = Util.CInt(row["ThumbFileID"]);
					queue.MemberPhoto.ListOrder = Convert.ToInt32(row["ListOrder"]);

					this.Add(queue);
				}
			}
		}
	}
}
