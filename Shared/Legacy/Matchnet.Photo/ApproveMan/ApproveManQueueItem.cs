using System;
using System.Data;

using Matchnet.MessageQueue;
using Matchnet.Lib.Exceptions;

namespace Matchnet.Photo
{
	/// <summary>
	/// Once processed from web photoapproval,
	/// populate this item with processed data and hand off to 
	/// mnApproveman using Matchnet.MessageQueue
	/// </summary>
	[Serializable]
	public class ApproveManQueueItem : QueueItemBase
	{
		DataSet _QueueDataSet;

		public DataSet QueueDataSet
		{
			get
			{
				return _QueueDataSet;
			}
			set
			{
				_QueueDataSet = value;
			}
		}

		/// <summary>
		/// Sends a dataset with multiple datatables which contain web
		/// photo approval data
		/// </summary>
		/// <param name="item"></param>
		public static void Send(ApproveManQueueItem item)
		{					
			item.QueuePath = PhotoUtil.MSMQPath;
			item.Priority = QueueItemBase.PriorityType.High;
			QueueItemBase.Send(item);
		}

		/// <summary>
		/// Process each dataset.
		/// </summary>
		public override void Execute()
		{
			ApproveManQueues.Process(this.QueueDataSet);
		}
	}
}
