using System;
using System.IO;
using System.Web.Mail;
using System.Data;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;

using Matchnet.CachingTemp;
using Matchnet.Content;
using Matchnet.Email.Classification;
using Matchnet.Lib;
using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Util;
using Matchnet.Labels;
using Matchnet.Photo;

namespace Matchnet.Photo
{
	/// <summary>
	/// Picks up queues processed from Matchnet.Web.Applications.Admin.PhotoApproval
	/// With given set of instrucctions from PhotoApproval, process photo files
	/// and update databases accordingly.
	/// Sends out confirmation email to member and logs action to mnAdmin.
	/// </summary>
	public class ApproveManQueue : ApproveQueue
	{
		private int _EmailBodyResourceID = Matchnet.Constants.NULL_INT;
		private static Matchnet.CachingTemp.Cache _Cache;

		public int EmailBodyResourceID
		{
			get
			{
				return _EmailBodyResourceID;
			}
			set
			{
				_EmailBodyResourceID= value;
			}
		}
		static ApproveManQueue()
		{
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
		}

		public void Process(DataTable dt)
		{
			Populate(dt); // Populate this queue with data from MSMQ
			
			// Retrieving image via web uses VIP which is an extra overhead
			// and is making images time out when called within colo.
//			#region Get main image from web
//			try
//			{
//				this.MemberPhoto.ImageFile = PhotoUtil.GetImageFromWeb
//					(this.MemberPhoto.WebPath);
//			}
//			catch(Exception ex)
//			{
//				throw new MatchnetException("Error retrieving image from web",
//					"Matchnet.Photo.ApproveManQueue.Process()",
//					ex);
//			}
//			#endregion

			#region Get main image from UNC
			try
			{
				System.Drawing.Image image = 
					System.Drawing.Image.FromFile(this.MemberPhoto.FilePath);
				MemoryStream stream = new MemoryStream();
				image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
				image.Dispose();
				this.MemberPhoto.ImageFile = 
					System.Drawing.Image.FromStream(stream);
			}
			catch(Exception ex)
			{
				throw new MatchnetException("Error retrieving image via UNC [" +
					"FilePath: " + this.MemberPhoto.FilePath + "]",
					"Matchnet.Photo.ApprovaManQueue.Process()",
					ex);
			}
			#endregion

			#region Generate photos, rotate, crop, resize.
			try
			{
				//Rotate
				this.MemberPhoto.ImageFile =
					PhotoUtil.Rotate(this.MemberPhoto.ImageFile, 
					this.MemberPhoto.RotateAngle);

				// Crop Thumbnail
				this.MemberPhoto.ThumbNail.ImageFile = 
					PhotoUtil.Crop(this.MemberPhoto.ImageFile,
					this.MemberPhoto.ThumbNail.x,
					this.MemberPhoto.ThumbNail.y,
					this.MemberPhoto.ThumbNail.Width,
					this.MemberPhoto.ThumbNail.Height);

				// Crop Full Photo
				this.MemberPhoto.ImageFile = 
					PhotoUtil.Crop(this.MemberPhoto.ImageFile,
					this.MemberPhoto.x,
					this.MemberPhoto.y,
					this.MemberPhoto.Width,
					this.MemberPhoto.Height);

				// Resize Full Photo
				/// Resizing photos per ynm4 private labels
				PrivateLabel pl = new PrivateLabel();
				pl.Load(this.MemberPhoto.PrivateLabelID);

				if (pl.IsFeatureEnabled("YNM4PHOTO"))
				{
					this.MemberPhoto.ImageFile =
						PhotoUtil.Resize(this.MemberPhoto.ImageFile, 
						PhotoUtil.YNM4MaxPhotoWidth, 
						PhotoUtil.YNM4MaxPhotoHeight);
				}
				else
				{
					this.MemberPhoto.ImageFile =
						PhotoUtil.Resize(this.MemberPhoto.ImageFile, 
						PhotoUtil.MaxPhotoWidth, 
						PhotoUtil.MaxPhotoHeight);
				}

				// Resize Thumbnail
				this.MemberPhoto.ThumbNail.ImageFile = 
					PhotoUtil.Resize(this.MemberPhoto.ThumbNail.ImageFile,
					PhotoUtil.ThumbnailWidth,
					PhotoUtil.ThumbnailHeight);
			}
			catch(Exception ex)
			{
				throw new MatchnetException("Error processing photo. [" +
					"MemberID: " + this.MemberPhoto.MemberID +
					"DomainID: " + this.MemberPhoto.DomainID +
					"MemberPhotoID: " + this.MemberPhoto.MemberPhotoID +
					"MemberPhotoWebPath: " + this.MemberPhoto.WebPath +
					"]",
					"Matchnet.Photo.ApproveManQueue.Process()",
					ex);
			}
			#endregion

			#region Compress 
			this.MemberPhoto.ImageFile = 
				PhotoUtil.CompressJpeg(this.MemberPhoto.ImageFile);
			this.MemberPhoto.ThumbNail.ImageFile = 
				PhotoUtil.CompressJpeg(this.MemberPhoto.ThumbNail.ImageFile);
			#endregion
			
			#region Get filepath and save photos
			this.MemberPhoto.FilePath = 
				SaveFile(this.MemberPhoto.ImageFile, this.MemberPhoto.FileID);
			
			// Get filepath and save thumbnail
			// if new file generate new one
			if (this.MemberPhoto.ThumbNail.FileID == Matchnet.Constants.NULL_INT)
				this.MemberPhoto.ThumbNail.FileID = GetNewFile();

			this.MemberPhoto.ThumbNail.FilePath = 
				SaveFile(this.MemberPhoto.ThumbNail.ImageFile, this.MemberPhoto.ThumbNail.FileID);
			#endregion

			// Update mnFile
			SaveFile(this.MemberPhoto.FileID, this.MemberPhoto.FilePath);
			SaveFile(this.MemberPhoto.ThumbNail.FileID, this.MemberPhoto.ThumbNail.FilePath);


			// Update memberphoto and approve queue
			ApproveMemberPhoto();

			// Finally Process Email
			ProcessEmail();

			// Actually one last action :)
			SaveAdminAction(this.MemberPhoto.MemberID, 
				this.MemberPhoto.AdminMemberID, 
				this.MemberPhoto.DomainID,
				this.MemberPhoto.ApprovedFlag,
				Matchnet.Labels.PrivateLabel.GetBasePrivateLabelID(this.MemberPhoto.PrivateLabelID));

			// Release all resources used by image files
			this.MemberPhoto.ImageFile.Dispose();
			this.MemberPhoto.ThumbNail.ImageFile.Dispose();
	
		}
		private void ApproveMemberPhoto()
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", this.MemberPhoto.MemberID);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter
				("@MemberPhotoID", SqlDbType.Int, ParameterDirection.Input, this.MemberPhoto.MemberPhotoID);
			client.AddParameter
				("@ThumbFileID", SqlDbType.Int, ParameterDirection.Input, this.MemberPhoto.ThumbNail.FileID);
			client.AddParameter
				("@ApprovedFlag", SqlDbType.Int, ParameterDirection.Input, this.MemberPhoto.ApprovedFlag);

			client.ExecuteNonQuery("up_MemberPhoto_Approve", CommandType.StoredProcedure);			

			int listOrder = this.MemberPhoto.ListOrder;
			string filePath = string.Empty;
			string thumbPath = string.Empty;

			if (listOrder != Matchnet.Constants.NULL_INT)
			{
				listOrder--;

				//update member cache
				/*
				MemberUpdateQueueItem memberUpdateItem = new MemberUpdateQueueItem();
				memberUpdateItem.MemberID = this.MemberPhoto.MemberID;
				memberUpdateItem.PrivateLabelID = this.MemberPhoto.PrivateLabelID;
				memberUpdateItem.Attributes.Add(this.MemberPhoto.PrivateLabelID, "PrivateFlag" + listOrder.ToString(), this.MemberPhoto.PrivateFlag.ToString());
				if (this.MemberPhoto.ApprovedFlag == 1)
				{
					filePath = this.MemberPhoto.FilePath;
					thumbPath = this.MemberPhoto.ThumbNail.FilePath;
				}

				memberUpdateItem.Attributes.Add(this.MemberPhoto.PrivateLabelID, "FilePath" + listOrder.ToString(), filePath);
				memberUpdateItem.Attributes.Add(this.MemberPhoto.PrivateLabelID, "ThumbPath" + listOrder.ToString(), thumbPath);

				MemberUpdateQueueItem.Send(memberUpdateItem);
				*/
			}
			
		}

		/// <summary>
		/// Grab processed XML file from Photo Approval
		/// </summary>
		public void Populate(DataTable dt)
		{
			try
			{
				DataRow row = dt.Rows[0];
				
				this.ApproveQueueID = Util.CInt(row["ApproveQueueID"]);
				this.EmailBodyResourceID = Util.CInt(row["EmailBodyResourceID"]);
				this.MemberPhoto.WebPath = Util.CString(row["FileWebPath"]);
				this.MemberPhoto.FilePath = Util.CString(row["FilePath"]);
				this.MemberPhoto.MemberPhotoID = Util.CInt(row["MemberPhotoID"]);
				this.MemberPhoto.MemberID = Util.CInt(row["MemberID"]);
				this.MemberPhoto.AdminMemberID = Util.CInt(row["AdminMemberID"]);
				this.MemberPhoto.DomainID = Util.CInt(row["DomainID"]);
				this.MemberPhoto.EmailAddress = Util.CString(row["EmailAddress"]);
				this.MemberPhoto.FileID = Util.CInt(row["FileID"]);
				this.MemberPhoto.ThumbNail.FileID = Util.CInt(row["ThumbFileID"]);
				this.MemberPhoto.ApprovedFlag = Util.CInt(row["ApprovedFlag"]);
				this.MemberPhoto.ListOrder = Util.CInt(row["ListOrder"]);
				this.MemberPhoto.PrivateFlag = Util.CInt(row["PrivateFlag"]);
				this.MemberPhoto.PrivateLabelID = Util.CInt(row["PrivateLabelID"]);
				this.MemberPhoto.RotateAngle = Util.CInt(row["RotateAngle"]);

				// Get thumbnail information
				this.MemberPhoto.ThumbNail.x = Util.CInt(row["ThumbRectX"]);
				this.MemberPhoto.ThumbNail.y = Util.CInt(row["ThumbRectY"]);
				this.MemberPhoto.ThumbNail.Width = Util.CInt(row["ThumbRectWidth"]);
				this.MemberPhoto.ThumbNail.Height = Util.CInt(row["ThumbRectHeight"]);
			
				// Get fullsize information
				this.MemberPhoto.x = Util.CInt(row["CropRectX"]);
				this.MemberPhoto.y = Util.CInt(row["CropRectY"]);
				this.MemberPhoto.Width = Util.CInt(row["CropRectWidth"]);
				this.MemberPhoto.Height = Util.CInt(row["CropRectHeight"]);

				if ((this.MemberPhoto.FileID == Matchnet.Constants.NULL_INT) |
					(this.MemberPhoto.WebPath == Matchnet.Constants.NULL_STRING))
				{
					throw new MatchnetException("FileID is null" +
						"[MemberID: " + this.MemberPhoto.MemberID +
						" MemberPhotoID" + this.MemberPhoto.MemberPhotoID + "]",
						"ApproveManQueue.Populate()");
				}

			}
			catch(Exception ex)
			{
				throw new MatchnetException("Error populating from queue",
					"mnApproveMan.ApprovedQueue.Populate()",
					ex);
			}
		}

		private int GetNewFile()
		{
			int fileID = SQLClient.GetPrimaryKey("FileID");

			if (fileID == Matchnet.Constants.NULL_INT)
			{
				throw new MatchnetException("FileID is null" +
					"[MemberID: " + this.MemberPhoto.MemberID +
					" MemberPhotoID" + this.MemberPhoto.MemberPhotoID + "]",
					"ApproveManQueue.SaveFile()");
			}

			try
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnFile", fileID);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter
					("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
				client.AddParameter
					("@Extension", SqlDbType.NVarChar, ParameterDirection.Input, "jpg");

				client.ExecuteNonQuery("up_File_Insert", CommandType.StoredProcedure);
			}
			catch(Exception ex)
			{
				throw new MatchnetException("Unable to insert new file info" +
					"[FileID: " + fileID + " MemberID: " + this.MemberPhoto.MemberID +
					" MemberPhotoID: " + this.MemberPhoto.MemberPhotoID,
					"Matchnet.Photo.ApproveManQueue.GetNewFile()",
					ex);
			}

			return fileID;
		}

		/// <summary>
		/// For updating mnFile
		/// </summary>
		private void SaveFile(int fileID, string filePath)
		{
			try
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnFile", fileID);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter
					("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
				client.AddParameter
					("@Extension", SqlDbType.NVarChar, ParameterDirection.Input, "jpg");
				FileInfo file = new FileInfo(filePath);
				client.AddParameter
					("@Size", SqlDbType.Int, ParameterDirection.Input, file.Length);

				client.ExecuteNonQuery("up_File_Update", CommandType.StoredProcedure);
			}
			catch(Exception ex)
			{
				throw new MatchnetException("Unable to update file info" +
					"[FileID: " + fileID + " MemberID: " + this.MemberPhoto.MemberID +
					" MemberPhotoID: " + this.MemberPhoto.MemberPhotoID,
					"Matchnet.Photo.ApproveManQueue.SaveFile()",
					ex);
			}
		}
		/// <summary>
		/// For saving processed files
		/// </summary>
		/// <param name="image">Image File</param>
		/// <param name="fileID">FileID of the image</param>
		private string SaveFile(System.Drawing.Image image, int fileID)
		{
			string filePath = Matchnet.Constants.NULL_STRING;
			DataTable dt;

			try
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnFile", fileID);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter
					("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
				client.AddParameter
					("@Allpaths", SqlDbType.Int, ParameterDirection.Input, 1);
			
				dt = client.GetDataTable("up_File_List", CommandType.StoredProcedure); 

				if (dt.Rows.Count == 0)
				{
					throw new MatchnetException("Unable to populate file info" +
						"[FileID: " + fileID,
						"Matchnet.Photo.ApproveManQueue.SaveFile()");
				}

			}
			catch(Exception ex)
			{
				throw new MatchnetException("Unable to get file info" +
					"[FileID: " + fileID + " MemberID: " + this.MemberPhoto.MemberID +
					" MemberPhotoID: " + this.MemberPhoto.MemberPhotoID,
					"Matchnet.Photo.ApproveManQueue.SaveFile()",
					ex);
			}
				
			// There are multiple copies of a single photo
			foreach (DataRow row in dt.Rows)
			{
				// old code checks for activedate > now()
				// setting future active dates should stop 
				DateTime activeDate = Util.CDateTime(row["ActiveDate"]);
				filePath = Util.CString(row["Path"]);

				try
				{
					// Make sure directory exists
					FileInfo file = new FileInfo(filePath);
					if (!(Directory.Exists(file.DirectoryName)))
						Directory.CreateDirectory(file.DirectoryName);

					image.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg);
				}
				catch(Exception ex)
				{
					throw new MatchnetException("Unable to save file" +
						"[FileID: " + fileID + " MemberID: " + this.MemberPhoto.MemberID +
						" MemberPhotoID: " + this.MemberPhoto.MemberPhotoID +
						" FilePath: " + this.MemberPhoto.FilePath +
						" DB FilePath: " + filePath,
						"Matchnet.Photo.ApproveManQueue.SaveFile()",
						ex);
				}
			}
			
			image.Dispose();

			return filePath;
		}

		private void ProcessEmail()
		{
			// Populate the label to get translation and locale information
			Matchnet.Labels.PrivateLabel pl = new Matchnet.Labels.PrivateLabel();
			Translator translator = new Translator(_Cache);

			pl.Load(this.MemberPhoto.PrivateLabelID);

			string subject = Matchnet.Constants.NULL_STRING;

			// Subject line
			if (this.MemberPhoto.ApprovedFlag == 1)
			{
				subject = translator.Value(PhotoUtil.MEMBER_PHOTO, pl.LanguageID, pl.PrivateLabelID);
			}
			else
			{
				subject = translator.Value
					(PhotoUtil.PLEASE_LOGIN_AND_RESUBMIT_YOUR_PHOTOS, pl.LanguageID, pl.PrivateLabelID);
			}

			string from = "MemberServices@" + pl.URI;
			//
			string privateLabel = (string) pl.URI; 
			privateLabel = privateLabel.Substring(0,1).ToUpper() + privateLabel.Substring(1);
			
			translator.AddToken("BRANDNAME", privateLabel);
			translator.AddToken("PLDOMAIN", pl.SiteDescription);

			string body = translator.Value(this.EmailBodyResourceID, pl.LanguageID, pl.PrivateLabelID);
			body = translator.ExpandTokens(body);

			MailFormat format;
			if (pl.LCID == ConstantsTemp.LOCALE_HEBREW)
				format = MailFormat.Html;
			else
				format = MailFormat.Text;

			SendEmail(this.MemberPhoto.EmailAddress, 
				from, 
				subject, 
				format, 
				body, 
				this.MemberPhoto.FilePath,
				pl.PrivateLabelID,
				this.MemberPhoto.MemberID); 
		}

		private void SaveAdminAction(int memberID, int adminMemberID, int domainID, int approveFlag, int privateLabelID)
		{
			int actionMask;

			if (approveFlag == 1)
			{
				actionMask = PhotoUtil.ADMIN_ACTION_APPROVE_PHOTO;
			}
			else
			{
				actionMask = PhotoUtil.ADMIN_ACTION_REJECT_PHOTO;
			}

			SQLDescriptor descriptor = new SQLDescriptor("mnAdminWrite");
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter
				("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter
				("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
			client.AddParameter
				("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter
				("@AdminActionMask", SqlDbType.Int, ParameterDirection.Input, actionMask);
			client.AddParameter
				("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);

			client.ExecuteNonQuery("up_AdminActionLog_Insert", CommandType.StoredProcedure);
		}

		private void SendEmail(string to, 
			string from, 
			string subject, 
			MailFormat format, 
			string body, 
			string filePath,
			int privateLabelID,
			int memberID)
		{
			try
			{
				MailMessage msg = new MailMessage();
				MailAttachment attachment = new MailAttachment(filePath);

				// dynamic headers
				Classification classification = new Classification();
				classification.Populate(new EmailTypeConverter().ToInt(EmailType.PhotoApproval), privateLabelID, memberID);

				HeaderCollectionEnumerator enumerator = classification.Headers.GetHeaderEnumerator();

				while (enumerator.MoveNext()) 
				{
					Header header = (Header)enumerator.Current;
					msg.Headers.Add(header.Name, header.Value);
				}

				msg.To = to;
				msg.From = from;
				msg.Subject = subject;
				msg.BodyFormat = format;
				
				msg.Attachments.Add(attachment);
				
				// Hebrew..
				// Specifies Right to Left body direction
				// Content Charset 1255 for Hebrew
				if (msg.BodyFormat == MailFormat.Html)
				{
					msg.BodyEncoding = System.Text.Encoding.GetEncoding(1255);
					body = body.Replace(System.Environment.NewLine, "<BR>");
					body = "<head><html>" +
						"<meta http-equiv=\"Content-Type\" content=\"text/html\" charset=\"windows-1255\">" +
						"<meta http-equiv=\"Content-Language\" content=\"he\">" +
						"</head> + <body dir=\"rtl\">" + body + "</body></html>";
				}
				msg.Body = body;
				SmtpMail.Send(msg);	
			}
			catch(Exception ex)
			{
				throw new MatchnetException("Error Sending Email. [to: " + to +
					" from: " + from + " subject: " + subject + "]", 
					"ProcessedQueue.SendEmail()", 
					ex);
			}
		}

	}

	public class ApproveManQueues
	{
		public static void Process(DataSet ds)
		{
			try
			{
				foreach (DataTable dt in ds.Tables)
				{
					ApproveManQueue queue = new ApproveManQueue();
					queue.Process(dt);
				}
			}
			catch(Exception ex)
			{
				MatchnetException mex = new MatchnetException("Error Processing Approved Queues",
					"ApproveManQueueItem.Execute()",
					ex);

				ExceptionLogger.LogToEventLog(mex,
					"ApproveMan",
					System.Diagnostics.EventLogEntryType.Warning);
			}
		}
	}
}
