using System;
using System.Data;
using System.Drawing;
using System.IO;

using Matchnet.Lib;
using Matchnet.DataTemp;
using Matchnet.Lib.Util;


namespace Matchnet.Photo
{
	/// <summary>
	/// Generic photo object
	/// Classes that inherit
	/// Matchnet.Photo.MemberPhoto - Full Size photo
	/// Matchnet.Photo.Thumbnail - Thumbnail photo 
	/// </summary>
	public abstract class Photo
	{
		#region Private Variables
		private int _x = 0;
		private int _y = 0;
		private int _Width = Matchnet.Constants.NULL_INT;
		private int _Height = Matchnet.Constants.NULL_INT;
		private int _FileID = Matchnet.Constants.NULL_INT;
		private string _WebPath = Matchnet.Constants.NULL_STRING;
		private string _FilePath = Matchnet.Constants.NULL_STRING;
		private int _RotateAngle = 0;
		private System.Drawing.Image _ImageFile;
		#endregion

		#region Public Properties
		public int Width
		{
			get
			{
				return _Width;
			}
			set
			{
				_Width = value;
			}
		}
		public int Height
		{
			get
			{
				return _Height;
			}
			set
			{
				_Height = value;
			}
		}
		public System.Drawing.Image ImageFile
		{
			get
			{
				if (_ImageFile != null)
					return _ImageFile;
				else
					return null;
			}
			set
			{
				_ImageFile = value;
			}
		}
		public string FilePath
		{
			get
			{
				return _FilePath;
			}
			set
			{
				_FilePath = value;
			}
		}
		public string WebPath
		{
			get
			{
				return _WebPath;
			}
			set
			{
				_WebPath = value;
			}
		}
		public int FileID
		{
			get
			{
				return _FileID;
			}
			set
			{
				_FileID = value;
				if (_FileID != Matchnet.Constants.NULL_INT)
					GetFileInfo();
			}
		}
		public int x
		{
			get
			{
				return _x;
			}
			set
			{
				_x = value;
			}
		}

		public int y
		{
			get
			{
				return _y;
			}
			set
			{
				_y = value;
			}
		}

		public int RotateAngle
		{
			get
			{
				return _RotateAngle;
			}
			set
			{
				_RotateAngle = value;
			}
		}

		#endregion

		#region Methods
		private void GetFileInfo()
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnFile", this._FileID);
			SQLClient client = new SQLClient(descriptor);
			
			client.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, this._FileID);

			DataTable dt = client.GetDataTable("up_File_List", CommandType.StoredProcedure);

			if (dt.Rows.Count == 1)
			{
				DataRow row = dt.Rows[0];

				this._FilePath = Util.CString(row["Path"]);
				this._WebPath = Util.CString(row["WebPath"]);
				
				//this._ImageFile = System.Drawing.Image.FromFile(this._FilePath);
			}
			else
			{
				throw new Exception("File info not found in db. [FileID: " + _FileID + "]");
			}
		}
		#endregion
	}
}
