using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;

using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;

namespace Matchnet.Photo
{
	/// <summary>
	/// Various image related static functions.
	/// </summary>
	public class PhotoUtil
	{
		// Photo Approval Subject
		public const int MEMBER_PHOTO = 521414;
		public const int PLEASE_LOGIN_AND_RESUBMIT_YOUR_PHOTOS = 519384;

		// Photo Approval Email Body
		public const int PHOTO_APPROVED_RESOURCEID = 518162;
		public const int BAD_FILE_FORMAT_RESOURCEID = 518582;
		public const int COPYRIGHT_REJECT_LETTER_RESOURCEID = 518000;
		public const int BLURY_REJECT_LETTER_RESOURCEID = 518001;
		public const int FILE_SIZE_REJECT_LETTER_RESOURCEID = 518002;
		public const int GENERAL_REJECT_LETTER_RESOURCEID = 518004;
		public const int UNKNOWN_MEMBER_RESOURCEID = 518005;
		public const int SUGGESTIVE_PHOTO_RESOURCEID = 518006;

		private static int _MaxPhotoWidth = Matchnet.Constants.NULL_INT;
		private static int _MaxPhotoHeight = Matchnet.Constants.NULL_INT;
		private static int _YNM4MaxPhotoWidth = Matchnet.Constants.NULL_INT;
		private static int _YNM4MaxPhotoHeight = Matchnet.Constants.NULL_INT;
		private static int _MaxPhotoSizeBytes = Matchnet.Constants.NULL_INT;
		private static int _ThumbnailWidth = Matchnet.Constants.NULL_INT;
		private static int _ThumbnailHeight = Matchnet.Constants.NULL_INT;
		private static int _DefaultPhotoSizeBytes = Matchnet.Constants.NULL_INT;
		private static string _MSMQPath = Matchnet.Constants.NULL_STRING;

		// Adminaction mask
		public const int ADMIN_ACTION_REJECT_PHOTO = 16;
		public const int ADMIN_ACTION_APPROVE_PHOTO = 8;

		public static string MSMQPath
		{
			get
			{
				if (_MSMQPath == Matchnet.Constants.NULL_STRING)
					_MSMQPath = GetPropertyValue("MSMQPath");

				return _MSMQPath;
			}
		}

		public static int MaxPhotoWidth
		{
			get
			{
				if (_MaxPhotoWidth == Matchnet.Constants.NULL_INT)
					_MaxPhotoWidth = Util.CInt(GetPropertyValue("MaxPhotoWidth"));

				return _MaxPhotoWidth;
			}
		}

		public static int MaxPhotoHeight
		{
			get
			{
				if (_MaxPhotoHeight == Matchnet.Constants.NULL_INT)
					_MaxPhotoHeight = Util.CInt(GetPropertyValue("MaxPhotoHeight"));

				return _MaxPhotoHeight;
			}
		}

		public static int YNM4MaxPhotoWidth
		{
			get
			{
				if (_YNM4MaxPhotoWidth == Matchnet.Constants.NULL_INT)
					_YNM4MaxPhotoWidth = Util.CInt(GetPropertyValue("YNM4MaxPhotoWidth"));

				return _YNM4MaxPhotoWidth;
			}
		}

		public static int YNM4MaxPhotoHeight
		{
			get
			{
				if (_YNM4MaxPhotoHeight == Matchnet.Constants.NULL_INT)
					_YNM4MaxPhotoHeight = Util.CInt(GetPropertyValue("YNM4MaxPhotoHeight"));

				return _YNM4MaxPhotoHeight; 
			}
		}

		public static int MaxPhotoSizeBytes
		{
			get
			{
				if (_MaxPhotoSizeBytes == Matchnet.Constants.NULL_INT)
					_MaxPhotoSizeBytes = Util.CInt(GetPropertyValue("MaxPhotoSizeBytes"));

				return _MaxPhotoSizeBytes;
			}
		}
		public static int ThumbnailWidth
		{
			get
			{
				if (_ThumbnailWidth == Matchnet.Constants.NULL_INT)
					_ThumbnailWidth = Util.CInt(GetPropertyValue("ThumbnailWidth"));

				return _ThumbnailWidth;
			}
		}
		public static int ThumbnailHeight
		{
			get
			{
				if (_ThumbnailHeight == Matchnet.Constants.NULL_INT)
					_ThumbnailHeight = Util.CInt(GetPropertyValue("ThumbnailHeight"));

				return _ThumbnailHeight;
			}
		}
		public static int DefaultPhotoSizeBytes
		{
			get
			{
				if (_DefaultPhotoSizeBytes == Matchnet.Constants.NULL_INT)
					_DefaultPhotoSizeBytes = Util.CInt(GetPropertyValue("_DefaultPhotoSizeBytes"));
				
				return _DefaultPhotoSizeBytes;
			}
		}

		private static string GetPropertyValue(string propertyName)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMaster", 0);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@Owner", SqlDbType.NVarChar, ParameterDirection.Input,  "mnPhotoEditor");
			client.AddParameter("@Name", SqlDbType.NVarChar, ParameterDirection.Input,  propertyName);

			DataTable dt = client.GetDataTable("up_Property_List", CommandType.StoredProcedure);

			if (dt.Rows.Count == 1)
			{
				return Matchnet.Lib.Util.Util.CString(dt.Rows[0]["Value"]);
			}
			else
			{
				throw new MatchnetException("Property " + propertyName + " not found",
					"Matchnet.Photo.Util.GetPropertyValue()");		
			}
		}

		/// <summary>
		/// Compresses a jpg image file
		/// Start off with jpeg 90 quality factor and decrements by 2 in loop
		/// When not used in junction with resize it could come out with very
		/// poor quality.
		/// 0 - Lowest Quality, 100 - Highest Quality
		/// </summary>
		/// <param name="image">Jpg file</param>
		public static System.Drawing.Image CompressJpeg(System.Drawing.Image image)
		{
			MemoryStream stream;
			long length;
			int compression = 90;

			EncoderParameters eps = new EncoderParameters(1);
			eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);
			ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
			
			stream = new MemoryStream();
			image.Save(stream, ici, eps);
			length = stream.Length;

			while ((length > MaxPhotoSizeBytes) & (compression > 0))
			{
				eps = new EncoderParameters(1);
				eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);
				stream = new MemoryStream();
				image.Save(stream, ici, eps);
				length = stream.Length;
				compression -= 2;
				//stream.Close(); //http://support.microsoft.com/?id=814675
			}
			image = System.Drawing.Image.FromStream(stream);
			return image;
		}
		private static ImageCodecInfo GetEncoderInfo(String mimeType)
		{
			int j;
			ImageCodecInfo[] encoders;
			encoders = ImageCodecInfo.GetImageEncoders();
			for(j = 0; j < encoders.Length; ++j)
			{
				if(encoders[j].MimeType == mimeType)
					return encoders[j];
			}
			return null;
		}

		public static System.Drawing.Image Crop(System.Drawing.Image source, int x, int y, int width, int height)
		{
			Bitmap cropped = new Bitmap(width, height);
			Graphics g = Graphics.FromImage(cropped);
			g.CompositingQuality = CompositingQuality.HighQuality;
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.InterpolationMode = InterpolationMode.HighQualityBicubic;
			Rectangle rect = new Rectangle(0, 0, width, height);
			g.DrawImage(source, rect, x, y, width, height, GraphicsUnit.Pixel);
			
			return cropped;
		}
		public static System.Drawing.Image GetImageFromWeb(string webPath)
		{
			try
			{
				System.Net.WebClient wc = new System.Net.WebClient();
				System.IO.MemoryStream stream = new MemoryStream(wc.DownloadData(webPath));
				System.Drawing.Image image = System.Drawing.Image.FromStream(stream);
				//stream.Close(); //http://support.microsoft.com/?id=814675
				return image;
			}
			catch(Exception ex)
			{
				throw new MatchnetException("Unable to get image from webserver " +
					"[WebPath: " + webPath + "]",
					"Matchnet.Photo.PhotoUtil.GetImageFromWeb",
					ex);
			}
		}

		public static System.Drawing.Image Resize(System.Drawing.Image imgPhoto, int Width, int Height)
		{
			int sourceWidth = imgPhoto.Width;
			int sourceHeight = imgPhoto.Height;
			int sourceX = 0;
			int sourceY = 0;
			int destX = 0;
			int destY = 0; 

			float nPercent = 0;
			float nPercentW = 0;
			float nPercentH = 0;

			// If smaller than spec than return it back untouched.
			if ((sourceWidth <= Width) & (sourceHeight <= Height))
				return imgPhoto;

			nPercentW = ((float)Width/(float)sourceWidth);
			nPercentH = ((float)Height/(float)sourceHeight);

			if(nPercentH < nPercentW)
			{
				nPercent = nPercentH;
				destX = System.Convert.ToInt16((Width - 
					(sourceWidth * nPercent))/2);
			}
			else
			{
				nPercent = nPercentW;
				destY = System.Convert.ToInt16((Height - 
					(sourceHeight * nPercent))/2);
			}

			int destWidth  = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);
			
			Bitmap bmPhoto = new Bitmap(destWidth, destHeight, 
				PixelFormat.Format32bppRgb);
			bmPhoto.SetResolution(imgPhoto.HorizontalResolution, 
				imgPhoto.VerticalResolution);

			Graphics grPhoto = Graphics.FromImage(bmPhoto);
			grPhoto.Clear(Color.White);
			grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
			grPhoto.InterpolationMode = 
				InterpolationMode.HighQualityBicubic;

			grPhoto.DrawImage(imgPhoto,
				new Rectangle(sourceX,sourceY,destWidth,destHeight),
				new Rectangle(sourceX,sourceY,sourceWidth,sourceHeight),
				GraphicsUnit.Pixel);

			grPhoto.Dispose();
			return bmPhoto;
		}

		public static System.Drawing.Image Rotate(System.Drawing.Image photo, int angle)
		{
			System.Drawing.Imaging.EncoderValue angleValue;

			switch (angle)
			{
				case 0:
					return photo;
				case 90:
					angleValue = EncoderValue.TransformRotate90;
					break;
				case 180:
					angleValue = EncoderValue.TransformRotate180;
					break;
				case 270:
					angleValue = EncoderValue.TransformRotate270;
					break;
				default:
					throw new Exception("Angle must be 0, 90, 180, or 270. [Angle: " 
						+ angle + "]");
			}

			Encoder Enc = Encoder.Transformation; 
			EncoderParameters EncParms = new EncoderParameters(1); 
			EncoderParameter EncParm; 
			ImageCodecInfo CodecInfo = GetEncoderInfo("image/jpeg"); 

			// for rewriting without recompression we must rotate the image 90 degrees
			EncParm = new EncoderParameter(Enc,(long)angleValue); 
			EncParms.Param[0] = EncParm; 

			MemoryStream stream = new MemoryStream();
			photo.Save(stream, CodecInfo, EncParms);

			return System.Drawing.Image.FromStream(stream);
		}
	}
}
