using System;

using Matchnet.Lib;

namespace Matchnet.HotList
{
	[Serializable]
	public class ViewerHotListAttribute
	{
		private HotList.HotListAttributeTypes _Type;
		private HotList.HotListDirection _Direction = HotList.HotListDirection.NONE;

		public HotList.HotListAttributeTypes Type
		{
			get
			{
				return _Type;
			}
			set
			{
				_Type = value;
			}
		}


		public HotList.HotListDirection Direction
		{
			get
			{
				return _Direction;
			}
			set
			{
				_Direction = value;
			}
		}
	}
}
