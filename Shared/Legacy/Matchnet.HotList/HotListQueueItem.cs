using System;
using System.Collections;
using System.Data;

using Matchnet.Lib;
using Matchnet.CachingTemp;
using Matchnet.DataTemp;
using Matchnet.Lib.Member;
using Matchnet.MessageQueue;


namespace Matchnet.HotList
{
	public class HotListQueueItem : QueueItemBase
	{
		private int _memberID;
		private int _toMemberID;
		private int _domainID;
		private HotList.HotListType _hotListType;
		private int _categoryID = Matchnet.Constants.NULL_INT;
		private string _description;
		private int _teaseID = Matchnet.Constants.NULL_INT;
		private bool _ignoreQuota = false;

		public static void Send(HotListQueueItem item)
		{
			Cache cache = Cache.GetInstance();
			const string KEY = "ViewProfileQueueList";

			ArrayList queueList = cache.Get(KEY) as ArrayList;
			if (queueList == null)
			{
				queueList = new ArrayList();

				//TODO: Remove SQL 
				SQLClient client = new SQLClient(new SQLDescriptor("mnSystem"));
				client.AddParameter("@Description", SqlDbType.VarChar, ParameterDirection.Input, "VIEWPROFILE");
				DataTable dt = client.GetDataTable("up_QueueLocation_ListAll", CommandType.StoredProcedure);

				foreach (DataRow row in dt.Rows)
				{
					queueList.Add(row["FormatName"].ToString());
				}

				cache.Insert(KEY, queueList, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
			}
			
			item.QueuePath = "FormatName:" + queueList[(int)(Math.Floor(queueList.Count * (new Random((int)DateTime.Now.Ticks).NextDouble())))].ToString();
			item.Recoverable = true;
			QueueItemBase.Send(item);
		}


		public override void Execute()
		{
			HotList.Save(_memberID,
				_toMemberID,
				_domainID,
				_hotListType,
				_categoryID,
				_description,
				_teaseID,
				_ignoreQuota,
				false);
		}


		#region public properties

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}


		public int ToMemberID
		{
			get
			{
				return _toMemberID;
			}
			set
			{
				_toMemberID = value;
			}
		}


		public int DomainID
		{
			get
			{
				return _domainID;
			}
			set
			{
				_domainID = value;
			}
		}


		public HotList.HotListType HotListType
		{
			get
			{
				return _hotListType;
			}
			set
			{
				_hotListType = value;
			}
		}


		public int CategoryID
		{
			get
			{
				return _categoryID;
			}
			set
			{
				_categoryID = value;
			}
		}


		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}


		public int TeaseID
		{
			get
			{
				return _teaseID;
			}
			set
			{
				_teaseID = value;
			}
		}


		public bool IgnoreQuota
		{
			get
			{
				return _ignoreQuota;
			}
			set
			{
				_ignoreQuota = value;
			}
		}

		#endregion
	}
}
