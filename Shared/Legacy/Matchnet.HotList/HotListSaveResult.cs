using System;

namespace Matchnet.HotList
{
	public class HotListSaveResult
	{
		private int _returnValue;
		private int _counter;
		private int _maxAllowed;
		private int _maxAllowedDurationDays;

		public int ReturnValue 
		{
			get { return _returnValue; }
			set { _returnValue = value; }
		}

		public int Counter 
		{
			get { return _counter; }
			set { _counter = value; }
		}

		public int MaxAllowed 
		{
			get { return _maxAllowed; }
			set { _maxAllowed = value; }
		}

		public int MaxAllowedDurationDays 
		{
			get { return _maxAllowedDurationDays; }
			set { _maxAllowedDurationDays = value; }
		}
	}
}
