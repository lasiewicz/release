using System;
using System.Collections;
using System.Data;

using Matchnet.List;
using Matchnet.Lib;
using Matchnet.CachingTemp;
using Matchnet.DataTemp;

namespace Matchnet.HotList
{
	[Serializable]
	public class ViewerHotList : System.Collections.Specialized.NameObjectCollectionBase
	{
		private DictionaryEntry _de = new DictionaryEntry();
		private int _HotListID;
		private DateTime _ViewDate;
		private HotList.HotListDirection _Direction;
		private HotList.HotListType _HotListType;
		private HotList.HotListCategory _Category;
		private string _Comments;
		private int _TeaseID = Matchnet.Constants.NULL_INT;
		private YNMMask _YNMMask;

		
		public ViewerHotList()
		{}


		public void Populate(DataRow row,
			HotList.HotListType hotListType,
			HotList.HotListCategory category,
			HotList.HotListDirection direction)
		{
			_HotListID = Convert.ToInt32(row["HotListID"]);
			_HotListType = hotListType;
			_Category = category;
			_Direction = direction;
			_HotListID = Convert.ToInt32(row["HotListID"]);
			if (row["UpdateDate"] != System.DBNull.Value)
			{
				_ViewDate = Convert.ToDateTime(row["UpdateDate"]);
			}
			if (row["Description"] != System.DBNull.Value)
			{
				_Comments = row["Description"].ToString();
			}
			if (row["TeaseID"] != System.DBNull.Value)
			{
				_TeaseID = Convert.ToInt32(row["TeaseID"]);
			}
		}
	
		#region collection members
		public ViewerHotList(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) 
		{
			System.Runtime.Serialization.SerializationInfoEnumerator
				infoItems = info.GetEnumerator();
			while(infoItems.MoveNext()) 
			{
				Add((ViewerHotListAttribute)infoItems.Value);
			}
		}

		public override void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
		{
			foreach(string name in base.BaseGetAllKeys()) 
			{
				try 
				{
					info.AddValue(name, base.BaseGet(name));
				} 
				catch(Exception){}
			}
		}
		
		public ViewerHotList( IDictionary d, Boolean bReadOnly )  
		{
			foreach ( DictionaryEntry de in d )  
			{
				this.BaseAdd( (String) de.Key, de.Value );
			}
			this.IsReadOnly = bReadOnly;
		}

		public DictionaryEntry this[ int index ]  
		{
			get  
			{
				_de.Key = this.BaseGetKey(index);
				_de.Value = this.BaseGet(index);
				return( _de );
			}
		}

		public HotList.HotListDirection this[string key]  
		{
			get  
			{
				ViewerHotListAttribute viewerHotListAttribute = (ViewerHotListAttribute)this.BaseGet(key);
				if (viewerHotListAttribute != null)
				{
					return viewerHotListAttribute.Direction;
				}
				else
				{
					return HotList.HotListDirection.NONE;
				}
			}
			/*
			set  
			{
				this.BaseSet( key, value );
			}
			*/
		}


		public String[] AllKeys  
		{
			get  
			{
				return( this.BaseGetAllKeys() );
			}
		}

		public Array AllValues  
		{
			get  
			{
				return( this.BaseGetAllValues() );
			}
		}

		public String[] AllStringValues  
		{
			get  
			{
				return( (String[]) this.BaseGetAllValues( Type.GetType( "System.String" ) ) );
			}
		}

		public Boolean HasKeys  
		{
			get  
			{
				return( this.BaseHasKeys() );
			}
		}

		public void Add(ViewerHotListAttribute viewerHotListAttribute)
		{
			this.BaseAdd(viewerHotListAttribute.Type.ToString(), viewerHotListAttribute);
		}

		public void Add(HotList.HotListAttributeTypes type, HotList.HotListDirection direction)
		{
			ViewerHotListAttribute viewerHotListAttribute = new ViewerHotListAttribute();
			viewerHotListAttribute.Type = type;
			viewerHotListAttribute.Direction = direction;
			Add(viewerHotListAttribute);
		}

		public void Add(System.Collections.DictionaryEntry de)
		{
			this.BaseAdd( (String) de.Key, de.Value );
		}

		public void Remove( String key )  
		{
			this.BaseRemove( key );
		}

		public void Remove( int index )  
		{
			this.BaseRemoveAt( index );
		}

		public void Clear()  
		{
			this.BaseClear();
		}
		#endregion

		#region public properties
		public int HotListID
		{
			get
			{
				return _HotListID;
			}
		}

		public DateTime ViewDate
		{
			get
			{
				return _ViewDate;
			}
		}

		public HotList.HotListDirection Direction
		{
			get
			{
				return _Direction;
			}
		}

		public HotList.HotListType HotListType
		{
			get
			{
				return _HotListType;
			}
		}

		public HotList.HotListCategory Category
		{
			get
			{
				return _Category;
			}
		}

		public string Comments
		{
			get
			{
				return _Comments;
			}
		}

		public int TeaseID
		{
			get
			{
				return _TeaseID;
			}
		}

		public string GetTease(int privateLabelID, int languageID)
		{
			if (_TeaseID != Matchnet.Constants.NULL_INT)
			{
				Matchnet.CachingTemp.Cache cache = Matchnet.CachingTemp.Cache.GetInstance();
				string key = string.Format("Tease:{0}:{1}:{2}", _TeaseID, privateLabelID, languageID);

				object o = cache.Get(key);

				if (o != null)
				{
					return o.ToString();
				}

				SQLDescriptor descriptor = new SQLDescriptor("mnShared");
				SQLClient client = new SQLClient(descriptor);
				client.AddParameter("@TeaseID", SqlDbType.Int, ParameterDirection.Input, _TeaseID);
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
				client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
				DataTable dt = client.GetDataTable("up_Tease_List_ByID", CommandType.StoredProcedure);

				if (dt.Rows.Count == 1)
				{
					cache.Insert(key, dt.Rows[0]["Content"], null, DateTime.Now.AddHours(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
					return dt.Rows[0]["Content"].ToString();
				}
			}
			
			return "";
		}

		public YNMMask YNMMask
		{
			get
			{
				return _YNMMask;
			}
			set
			{
				_YNMMask = value;
			}
		}

		#endregion

	}
}
