using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

using Matchnet.CachingTemp;
using Matchnet.Content;
using Matchnet.DataTemp;
using Matchnet.Lib;
using Matchnet.Lib.Option;
using Matchnet.Lib.Util;

namespace Matchnet.HotList
{
	public class HotList
	{		
		#region GetCategories, GetCustomCategories: Requires new VO
		public static DataTable GetCategories(int memberID, int domainID, int privateLabelID, int languageID, Translator translator)
		{
			DataTable dtResult = new DataTable();
			dtResult.Columns.Add("HotListID", typeof(int));
			dtResult.Columns.Add("Description", typeof(string));
			
			dtResult.Rows.Add(new object[2]{Matchnet.Constants.NULL_INT, translator.Value("YOUR_HOT_LIST", languageID, privateLabelID) + translator.Value("YOUR_FAVORITES", languageID, privateLabelID)});

			DataTable dt = GetCustomCategories(memberID, domainID);
			foreach (DataRow row in dt.Rows)
			{
				dtResult.Rows.Add(new object[2]{row["CategoryID"], translator.Value("YOUR_HOT_LIST", languageID, privateLabelID) + row["Description"]});
			}

			dt = GetStandardCategories(privateLabelID, languageID, translator);
			foreach (DataRow row in dt.Rows)
			{
				dtResult.Rows.Add(new object[2]{row["HotListID"], row["Description"]});
			}

			return dtResult;
		}

		// almost ready for removal -wel
		public static DataTable GetCustomCategories(int memberID, int domainID)
		{
			//TODO: Remove SQL 
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@MemberID", SqlDbType.VarChar, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.VarChar, ParameterDirection.Input, domainID);
			return client.GetDataTable("up_Category_List", CommandType.StoredProcedure);
		}
		#endregion

		#region GetStandardCategoryName: Dependant on GetStandardCategories method
		public static string GetStandardCategoryName(List.ValueObjects.HotListCategory category, int privateLabelID, int languageID, Translator translator)
		{
			if ((int)category == 0)
			{
				return translator.Value("YOUR_FAVORITES", languageID, privateLabelID);
			}
			else
			{
				DataView dv = GetStandardCategories(privateLabelID, languageID, translator).DefaultView;

				dv.RowFilter = "HotListID = " + ((int)category).ToString();

				if (dv.Count == 1)
				{
					return dv[0]["Description"].ToString();
				}
			}

			return string.Empty;
		}
		#endregion		

		#region GetStandardCategoryNameFromResource: Dependant on GetStandardCategories method
		public static string GetStandardCategoryNameFromResource(List.ValueObjects.HotListCategory category, int privateLabelID, int languageID, Translator translator)
		{
			if ((int)category == 0)
			{
				return translator.Value("TXT_YOUR_FAVORITES", languageID, privateLabelID);		// using TXT_YOUR_FAVORITES as new convention
			}
			else
			{
				DataView dv = GetStandardCategories(privateLabelID, languageID, translator).DefaultView;

				dv.RowFilter = "HotListID = " + ((int)category).ToString();

				if (dv.Count == 1)
				{
					if(dv[0]["ResourceID"] != DBNull.Value)
					{
						return translator.Value(Convert.ToInt32(dv[0]["ResourceID"]), languageID, privateLabelID);
					}
				}
				
				return String.Empty;
			}
		}
		#endregion

		#region GetStandardCategories: Ready for svc, requires Matchnet.CachingTemp.Cache
		private static DataTable GetStandardCategories(int privateLabelID, int languageID, Translator translator)
		{
			DataTable dt = null;
			string key = string.Format("HotListCategories:{0}:{1}", privateLabelID, languageID);
			Cache cache = Matchnet.CachingTemp.Cache.GetInstance();

			object o = cache.Get(key);

			if (dt != null)
			{
				dt = (DataTable)o;
			}
			else
			{
				//TODO: Remove SQL 
				SQLDescriptor descriptor = new SQLDescriptor("mnShared");
				SQLClient client = new SQLClient(descriptor);
				client.AddParameter("@PrivateLabelID", SqlDbType.VarChar, ParameterDirection.Input, privateLabelID);
				dt = client.GetDataTable("up_HotList_List", CommandType.StoredProcedure);
				dt.Columns.Add("Description", typeof(string));

				foreach (DataRow row in dt.Rows)
				{
					row["Description"] = translator.Value(Convert.ToInt32(row["ResourceID"]), 
						languageID,
						privateLabelID);
				}

				cache.Insert(key, dt, null, DateTime.Now.AddHours(1), TimeSpan.Zero);
			}			

			return dt;
		}
		#endregion


	}
}
