﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using NUnit.Framework;

//namespace Matchnet.CachingTempTest
//{
//    [TestFixture]
//    public class CacheTest
//    {
//        private CachingTemp.Cache cacheInstance;
//        [TestFixtureSetUp]
//        public void StartUp()
//        {
//            cacheInstance = CachingTemp.Cache.GetInstance();
//        }

//        [TestFixtureTearDown]
//        public void Teardown()
//        {
//        }

//        [Test]
//        public void TestAddCacheableObject()
//        {
//            ICacheable cacheableObj = new TestCacheable(10);
//            cacheInstance.Remove(cacheableObj.GetCacheKey());
//            var val = cacheInstance.Add(cacheableObj);
//            Assert.Null(val);
//            //Add item again. If item already exists in cache, it returns the value
//            val = cacheInstance.Add(cacheableObj);
//            Assert.NotNull(val);
//            Assert.AreEqual(val,cacheableObj);
//            var actualval = cacheInstance.Get(cacheableObj.GetCacheKey()) as ICacheable;
//            Assert.AreEqual(cacheableObj,actualval);
//        }

//        [Test]
//        public void TestAddNullCacheableObject()
//        {
//            var val = cacheInstance.Add(null);
//            Assert.Null(val);
//        }

//        [Test]
//        public void TestAddNonSerializedObject()
//        {
//            //todo
//        }

//        [Test]
//        public void TestAddKeyValue()
//        {
//            string key = "Key_3";
//            string value = "MembaseUnitTest";
//            cacheInstance.Remove(key);
//            object cacheItem = cacheInstance.Add(key, value);
//            Assert.Null(cacheItem);
//            //Try adding the item again
//            cacheItem = cacheInstance.Add(key, value);
//            Assert.NotNull(cacheItem);
//            cacheItem = cacheInstance.Get(key);
//            Assert.AreEqual(value, cacheItem);
//        }

//        [Test]
//        public void TestAddNullValue()
//        {
//            string key = "Key_4";
//            cacheInstance.Remove(key);
//            var value = cacheInstance.Add(key, null);
//            Assert.Null(value);
//        }

//        [Test]
//        public void TestAddWithExpiration()
//        {
//            string key = "Key_5";
//            string value = "MembaseUnitTest";
//            cacheInstance.Remove(key);
//            var val = cacheInstance.Add(key, value, null, DateTime.Now.AddSeconds(10), System.Web.Caching.Cache.NoSlidingExpiration,
//                                        System.Web.Caching.CacheItemPriority.Normal, null);
//            Assert.NotNull(value);
//            val = cacheInstance.Get(key);
//            Assert.AreEqual(value, val);
//        }

//        [Test]
//        public void TestGet()
//        {
//            string key = "Key_6";
//            string value = "MembaseUnitTest";
//            cacheInstance.Insert(key,value);
//            var actual = cacheInstance.Get(key);
//            Assert.AreEqual(value,actual);
//        }

//        [Test]
//        public void TestGetEmptyNullKey()
//        {
//            Assert.Null(cacheInstance.Get(""));
//            Assert.Null(cacheInstance.Get(null));
//        }

//        [Test]
//        public void TestInsertCacheableObject()
//        {
//            ICacheable cacheableObj = new TestCacheable(20);
//            cacheInstance.Remove(cacheableObj.GetCacheKey());
//            cacheInstance.Insert(cacheableObj);
//            var actualval = cacheInstance.Get(cacheableObj.GetCacheKey()) as ICacheable;
//            Assert.AreEqual(cacheableObj, actualval);
//        }

//        [Test]
//        public void TestInsertNullCacheableObject()
//        {
//            cacheInstance.Insert(null);
//        }

//        [Test]
//        public void TestInsertAndGet()
//        {
//            string cacheKey = "Key_1";
//            string cacheValue = "MembaseUnitTest";
//            cacheInstance.Insert(cacheKey, cacheValue);
//            object cacheItem = cacheInstance.Get(cacheKey);
//            Assert.NotNull(cacheItem);
//            Assert.AreEqual(cacheValue, cacheItem);

//            cacheInstance.Insert(cacheKey, cacheValue,null);
//            cacheItem = cacheInstance.Get(cacheKey);
//            Assert.NotNull(cacheItem);
//            Assert.AreEqual(cacheValue, cacheItem);

//            cacheInstance.Insert(cacheKey, cacheValue, null,DateTime.Now.AddSeconds(10), System.Web.Caching.Cache.NoSlidingExpiration);
//            cacheItem = cacheInstance.Get(cacheKey);
//            Assert.NotNull(cacheItem);
//            Assert.AreEqual(cacheValue, cacheItem);

//            cacheInstance.Insert(cacheKey, cacheValue, null, DateTime.Now.AddSeconds(10), System.Web.Caching.Cache.NoSlidingExpiration,System.Web.Caching.CacheItemPriority.Normal,null);
//            cacheItem = cacheInstance.Get(cacheKey);
//            Assert.NotNull(cacheItem);
//            Assert.AreEqual(cacheValue, cacheItem);
//        }

//        [Test]
//        public void TestInsertWithNullValue()
//        {
//            string cacheKey = "Key_2";
//            cacheInstance.Insert(cacheKey,null);
//            Assert.Null(cacheInstance.Get(cacheKey));
//        }

//        [Test]
//        public void TestRemove()
//        {
//            string cacheKey = "Key_4";
//            string cacheValue = "MembaseUnitTest";
//            cacheInstance.Insert(cacheKey, cacheValue);
//            cacheInstance.Remove(cacheKey);
//            object cacheItem = cacheInstance.Get(cacheKey);
//            Assert.Null(cacheItem);
//        }
//    }

//    [Serializable]
//    public class TestCacheable : ICacheable
//    {
//        private string val = "MembaseUnitTest";
//        private int index = 0;
//        private int _cacheTTLSeconds = 540;
//        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
//        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;

//        public string Value { get { return val; } }

//        public TestCacheable(int index)
//        {
//            this.index = index;
//        }

//        public string GetCacheKey()
//        {
//            return "Key_Test_" + index;
//        }

//        public int CacheTTLSeconds
//        {
//            get { return _cacheTTLSeconds; }
//            set { _cacheTTLSeconds = value; }
//        }

//        public CacheItemMode CacheMode
//        {
//            get { return _cacheItemMode; }
//        }

//        public CacheItemPriorityLevel CachePriority
//        {
//            get { return _cachePriority; }
//            set { _cachePriority = value; }
//        }
//    }
//}
