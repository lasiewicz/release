using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;


namespace Matchnet.CachingTemp
{
    /// <summary>
    /// Summary description for Cache.
    /// </summary>

    public class Cache : System.Collections.IEnumerable, ICaching
    {
        // PT:  This variable is what Cache wraps.  In certain situations, the cache
        // will function differently.  For example, you need different functionality when
        // design time is used.
        private System.Web.Caching.Cache _InternalCache;
        private bool _UseMSCache;
        private static Matchnet.CachingTemp.Cache _Instance;
        private static string _Lock = "";

        public static ICaching GetInstance()
        {
            if (_Instance == null)
            {
                lock (_Lock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new Matchnet.CachingTemp.Cache();
                    }
                }
            }
            return _Instance;
        }

        // PT: This class attempts to autodetect the current running situation and decides
        // whether to use the internal Microsoft caching objects or not.  If it can't
        // figure out the runtime situation, then it'll default to not using the internal
        // caching structure, it will default to an internal implementation of sorts. 
        //
        // Currently, there is no internal implementation.  This is just a wrapper for
        // the Microsoft class.  We intend to create functionality within this class so
        // that we can support design time operations.

        private Cache()
        {
            _UseMSCache = true;
            try
            {
                _InternalCache = System.Web.HttpRuntime.Cache;
            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.WriteLine("System.Web.HttpRuntime==null or " +
                    "System.Web.HttpRuntime.Cache==null.");
                System.Diagnostics.Trace.WriteLine(ex);
                _UseMSCache = false;
            }
        }

        // PT: [Deprecated] Secondary Constructor.  Force utilization of a different internal
        // caching structure
        private Cache(bool usemscache)
        {
            _UseMSCache = usemscache;
            if (_UseMSCache == true)
            {
                _InternalCache = System.Web.HttpRuntime.Cache;
            }
            else
            {
                // PT: Implement this later to satisfy the DesignTime scenario.  For
                // now, just throw the exception.
                // throw new System.Exception( "Unsupported operation. (_UseMSCache is false)\n" );
            }
        }

        // Properties
        public int Count
        {
            get
            {
                if (_UseMSCache == true)
                    return _InternalCache.Count;
                else
                {
                    // PT: Implement this later to satisfy the DesignTime scenario
                    // For the time being, just throw an exception.
                    // throw new System.Exception( "Unsupported operation. (_UseMSCache ==false)\n" );
                    return 0;
                }
            }
        }

        // PT: We're aliasing the read only static field with a property.
        public DateTime NoAbsoluteExpiration
        {
            get
            {
                if (_UseMSCache == true)
                    return System.Web.Caching.Cache.NoAbsoluteExpiration;
                else
                {
                    throw new
                        System.Exception("Unsupported operation. (_UseMSCache==false)\n");
                }
            }
        }

        // PT: We're aliasing the read only static field with a property.
        public TimeSpan NoSlidingExpiration
        {
            get
            {
                if (_UseMSCache == true)
                    return System.Web.Caching.Cache.NoSlidingExpiration;
                else
                {
                    throw new System.Exception();
                }
            }
        }

        // Item Gets or sets the cache item at the specified key. 
        public object this[string key]
        {
            get
            {
                if (_UseMSCache == true)
                    return _InternalCache[key];
                else
                {
                    return null;
                    // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                }
            }
            set
            {
                if (_UseMSCache == true)
                    _InternalCache[key] = value;
                else
                {
                    //throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                }
            }
        }

        // Methods
        public object Add(string key,
            object value,
            System.Web.Caching.CacheDependency dependencies,
            DateTime absoluteExpiration,
            TimeSpan slidingExpiration,
            System.Web.Caching.CacheItemPriority priority,
            System.Web.Caching.CacheItemRemovedCallback onRemoveCallback)
        {
            if (_UseMSCache == false)
            {
                // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                return null;
            }

            return _InternalCache.Add(key,
                value,
                dependencies,
                absoluteExpiration,
                slidingExpiration,
                priority,
                onRemoveCallback);
        }

        public object Get(string key)
        {
            if (_UseMSCache == false)
            {
                // PT: Implement this later to satisfy the DesignTime scenario
                // For the time being, just throw an exception.
                // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                return null;
            }
            return _InternalCache.Get(key);
        }

        public IDictionary<string, object> Get(List<string> keys)
        {
            IDictionary<string, object> objects = new Dictionary<string, object>();

            foreach(string key in keys)
            {
                objects.Add(key,Get(key));
            }
            return objects;
        }

        public System.Collections.IEnumerator GetEnumerator()
        {
            if (_UseMSCache == false)
            {
                // PT: Implement this later to satisfy the DesignTime scenario
                // For the time being, just throw an exception.
                // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                return null;
            }
            return _InternalCache.GetEnumerator();
        }

        public void Insert(string key, object value)
        {
            if (_UseMSCache == false)
            {
                // PT: Implement this later to satisfy the DesignTime scenario
                // For the time being, just throw an exception.
                // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                return;
            }
            _InternalCache.Insert(key, value);
        }

        public void Insert(string key,
            object value,
            System.Web.Caching.CacheDependency dependencies)
        {
            if (_UseMSCache == false)
            {
                // PT: Implement this later to satisfy the DesignTime scenario
                // For the time being, just throw an exception.
                // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                return;
            }
            _InternalCache.Insert(key, value, dependencies);
        }

        public void Insert(string key,
            object value,
            System.Web.Caching.CacheDependency dependencies,
            DateTime absoluteExpiration,
            TimeSpan slidingExpiration)
        {
            if (_UseMSCache == false)
            {
                // PT: Implement this later to satisfy the DesignTime scenario
                // For the time being, just throw an exception.
                // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                return;
            }
            _InternalCache.Insert(key,
                value,
                dependencies,
                absoluteExpiration,
                slidingExpiration);
        }

        public void Insert(string key,
            object value,
            System.Web.Caching.CacheDependency dependencies,
            DateTime absoluteExpiration,
            TimeSpan slidingExpiration,
            System.Web.Caching.CacheItemPriority priority,
            System.Web.Caching.CacheItemRemovedCallback onRemoveCallback)
        {
            if (_UseMSCache == false)
            {
                // PT: Implement this later to satisfy the DesignTime scenario
                // For the time being, just throw an exception.
                // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                return;
            }
            _InternalCache.Insert(key,
                value,
                dependencies,
                absoluteExpiration,
                slidingExpiration,
                priority,
                onRemoveCallback);
        }

        public object Remove(string key)
        {
            if (_UseMSCache == false)
            {
                // PT: Implement this later to satisfy the DesignTime scenario
                // For the time being, just throw an exception.
                // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                return null;
            }
            return _InternalCache.Remove(key);
        }

        public ICacheable Add(ICacheable cacheableObject)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, null, cacheableObject.CachePriority, null);
        }

        public ICacheable Add(ICacheable cacheableObject, CacheItemPriorityLevel overrideItemPriority)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, null, overrideItemPriority, null);
        }

        public ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, cacheDependency, cacheableObject.CachePriority, null);
        }

        public ICacheable Add(ICacheable cacheableObject, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, null, cacheableObject.CachePriority, onRemoveCallback);
        }

        public ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }
            return Add(cacheableObject, cacheDependency, cacheableObject.CachePriority, onRemoveCallback);
        }

        public ICacheable Add(ICacheable cacheableObject, CacheDependency cacheDependency, CacheItemPriorityLevel overrideItemPriority, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return null;
            }

            // Set default values for cache removal variables
            DateTime absoluteExpiration = System.Web.Caching.Cache.NoAbsoluteExpiration;
            TimeSpan slidingExpiration = System.Web.Caching.Cache.NoSlidingExpiration;

            // Configure caching based of the specified cache mode
            switch (cacheableObject.CacheMode)
            {
                case (CacheItemMode.Absolute):
                    absoluteExpiration = DateTime.Now.AddSeconds(Math.Abs(cacheableObject.CacheTTLSeconds));
                    break;
                case (CacheItemMode.Sliding):
                    slidingExpiration = TimeSpan.FromSeconds(Math.Abs(cacheableObject.CacheTTLSeconds));
                    break;
                default:
                    throw (new NotImplementedException("The specified Matching.CacheItemMode enumeration value is not supported."));
            }


            // Add the object to the cache
            object cacheItem = _InternalCache.Add(cacheableObject.GetCacheKey(), cacheableObject, cacheDependency, absoluteExpiration, slidingExpiration, translatePriority(overrideItemPriority), onRemoveCallback) as IValueObject;

            if (cacheItem == null)
            {
                return null;
            }
            else
            {
                return (ICacheable)cacheItem;
            }
        }

        public void Insert(ICacheable cacheableObject)
        {
            if (cacheableObject == null)
            {
                return;
            }

            Insert(cacheableObject, null, cacheableObject.CachePriority, null);
        }

        public void Insert(ICacheable cacheableObject, CacheDependency cacheDependency)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return;
            }
            Insert(cacheableObject, cacheDependency, cacheableObject.CachePriority, null);
        }

        public void Insert(ICacheable cacheableObject, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return;
            }
            Insert(cacheableObject, null, cacheableObject.CachePriority, onRemoveCallback);
        }

        public void Insert(ICacheable cacheableObject, CacheDependency cacheDependency, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return;
            }
            Insert(cacheableObject, cacheDependency, cacheableObject.CachePriority, onRemoveCallback);
        }

        public void Insert(ICacheable cacheableObject, CacheItemPriorityLevel overrideItemPriority)
        {
            if (cacheableObject == null)
            {
                return;
            }
            Insert(cacheableObject, null, overrideItemPriority, null);
        }

        public void Insert(ICacheable cacheableObject, CacheDependency cacheDependency, CacheItemPriorityLevel overrideItemPriority, CacheItemRemovedCallback onRemoveCallback)
        {
            if (cacheableObject == null)
            {
                // Bail out if the object is null
                return;
            }

            // Set default values for cache removal variables
            DateTime absoluteExpiration = System.Web.Caching.Cache.NoAbsoluteExpiration;
            TimeSpan slidingExpiration = System.Web.Caching.Cache.NoSlidingExpiration;

            // Configure caching based of the specified cache mode
            switch (cacheableObject.CacheMode)
            {
                case (CacheItemMode.Absolute):
                    absoluteExpiration = DateTime.Now.AddSeconds(Math.Abs(cacheableObject.CacheTTLSeconds));
                    break;
                case (CacheItemMode.Sliding):
                    slidingExpiration = TimeSpan.FromSeconds(Math.Abs(cacheableObject.CacheTTLSeconds));
                    break;
                default:
                    throw (new NotImplementedException("The specified Matching.CacheItemMode enumeration value is not supported."));
            }


            // Insert the object into the cache
            _InternalCache.Insert(cacheableObject.GetCacheKey(), cacheableObject, cacheDependency, absoluteExpiration, slidingExpiration, translatePriority(overrideItemPriority), onRemoveCallback);
        }

        public void Clear()
        {
            IDictionaryEnumerator de = _InternalCache.GetEnumerator();
            while (de.MoveNext())
            {
                this.Remove(de.Key.ToString());
            }
        }

        private CacheItemPriority translatePriority(CacheItemPriorityLevel cacheItemPriorityLevel)
        {
            switch (cacheItemPriorityLevel)
            {
                case (Matchnet.CacheItemPriorityLevel.High):
                    return CacheItemPriority.High;
                case (Matchnet.CacheItemPriorityLevel.AboveNormal):
                    return CacheItemPriority.AboveNormal;
                case (Matchnet.CacheItemPriorityLevel.Normal):
                    return CacheItemPriority.Normal;
                case (Matchnet.CacheItemPriorityLevel.BelowNormal):
                    return CacheItemPriority.BelowNormal;
                case (Matchnet.CacheItemPriorityLevel.Low):
                    return CacheItemPriority.Low;
                case (Matchnet.CacheItemPriorityLevel.Default):
                    return CacheItemPriority.Default;
                default:
                    return CacheItemPriority.Default;
            }
        }

        public bool RemoveWithoutGet(string key)
        {
            if (_UseMSCache == false)
            {
                // PT: Implement this later to satisfy the DesignTime scenario
                // For the time being, just throw an exception.
                // throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
                return false;
            }
            return null != _InternalCache.Remove(key);
        }

    };
}
