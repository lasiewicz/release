using System;
using System.Web;

namespace Matchnet.MemberDirectory
{
	/// <summary>
	/// Summary description for Path.
	/// </summary>
	public class Path
	{
		public readonly static Path Instance = new Path();

		/// <summary>
		/// Property that specifies the location of the single page handling all requests coming into the MemberDirectory Web Project
		/// This is used to update the server context and parse the request URL
		/// </summary>
		public readonly static string MainFileName = "/staticresults.aspx";

		/// <summary>
		/// We need to extract the real url from this request
		/// and map it to the static results control with the
		/// appropriate request parameters. the links are formatted
		/// in the following manner:
		/// http://foo.americansingles.com/Applications/Search/[Page(1)][World Level RegionID(8)][Country Level RegionID(8)][State Level RegionID(8)][First Letter - City(1)][City Level RegionID(8)][GenderSeekingID(1)][Age(2)][ViewingMemberID(10)]/StaticResults.aspx
		/// where [Page] = See Matchnet.Web.Applications.Search\StaticResultsController\StaticResultsPageEnum (length is always 1).
		/// and [World Level RegionID] = a valid region id (length is always 8).
		/// and [Country Level RegionID] = a valid region id (length is always 8).
		/// and [First Letter - City] = a valid region id (length is always 1).
		/// and [State Level RegionID] = a valid region id (length is always 8).
		/// and [GenderSeekingID] = GenderSeekingID (length is always 1.  0 = Male; 1 = Female).
		/// and [Age] = age (length is always 2).
		/// and [ViewingMemberID] = MemberID of the Member being viewed (length is always 10).
		/// TOTAL LENGTH OF PARAMETERS = 39.
		/// </summary>
		/// <param name="pagePath"></param>
		/// <param name="fullURL"></param>
		public void ParseUrl(ref string pagePath, ref string fullUrl) 
		{
			const string DEFAULT_URL = "10000022300000000A0000000000000000000000";
			string parsedPath = "";

			fullUrl = fullUrl.ToLower();
			// locate the file name and first slash, then remove it
			int i = fullUrl.IndexOf(MainFileName);

			if(i > 0) 
			{
				fullUrl = fullUrl.Substring(0, i);
				// locate the slash immediately preceeding the file name and grab the string that follows
				i = fullUrl.LastIndexOf("/");
				if(i > 0 && i < (fullUrl.Length - 1)) 
				{
					// assign to parsedPath, signifying that it was correctly extracted
					parsedPath = fullUrl.Substring(i + 1);
				}
			}

			// all paths are fixed width, if they don't match, set to default (top level USA selection)
			if(parsedPath.Length != DEFAULT_URL.Length) 
			{
				parsedPath = DEFAULT_URL;
			}

			// extract the relevant portions of the path

			// Page.
			HttpContext.Current.Items.Add("SRPage", parsedPath.Substring(0, 1));

			// World Level RegionID.
			HttpContext.Current.Items.Add("SRWorldLvlRegionID", parsedPath.Substring(1, 8));

			// Country Level RegionID.
			HttpContext.Current.Items.Add("SRCountryLvlRegionID", parsedPath.Substring(9, 8));

			// City First Letter.
			HttpContext.Current.Items.Add("SRCityFirstLetter", parsedPath.Substring(17, 1));

			// State Level RegionID.
			HttpContext.Current.Items.Add("SRStateLvlRegionID", parsedPath.Substring(18, 8));

			// GenderSeekingID.
			HttpContext.Current.Items.Add("SRGenderMask", parsedPath.Substring(26, 2));

			// Age.
			HttpContext.Current.Items.Add("SRAge", parsedPath.Substring(28, 2));

			// MemberID.
			HttpContext.Current.Items.Add("SRViewingMemberID", parsedPath.Substring(30, 10));

			// cacheKey.
			HttpContext.Current.Items.Add("SRCacheKey", parsedPath);
			
			pagePath = MainFileName;
			fullUrl = MainFileName;
		}
	}
}
