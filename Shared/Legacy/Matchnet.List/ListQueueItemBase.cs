using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Runtime.Serialization;

using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.MessageQueue;

namespace Matchnet.List
{
	/// <summary>
	/// Summary description for ListQueueItemBase.
	/// </summary>
	public class ListQueueItemBase : QueueItemBase
	{
		private DateTime _UpdateDate = DateTime.Now;
		private bool _Mirrorable = true;
		private string _ConnectionString;
		private int _MemberID;
		private int _PrivateLabelID;

		public ListQueueItemBase() : base()
		{
			
		}

		public bool Mirrorable
		{
			get
			{
				return _Mirrorable;
			}
			set
			{
				_Mirrorable = value;
			}
		}

		public string ConnectionString
		{
			get
			{
				return _ConnectionString;
			}
			set
			{
				_ConnectionString = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}

		public int PrivateLabelID
		{
			get
			{
				return _PrivateLabelID;
			}
			set
			{
				_PrivateLabelID = value;
			}
		}
	
		protected class ListPartitionConfig
		{
			private DataTable _dt;
			private int _PartitionCount;

			public ListPartitionConfig()
			{
			}

			public void Populate()
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnMaster"));
				client.AddParameter("@LogicalDatabase", SqlDbType.VarChar, ParameterDirection.Input, "mnList");

				_dt = client.GetDataTable("up_LogicalPhysicalDatabases_List", CommandType.StoredProcedure);

				ArrayList accessFlagList = new ArrayList();
				foreach (DataRow row in _dt.Rows)
				{
					if (!accessFlagList.Contains(row["AccessFlag"]))
					{
						accessFlagList.Add(row["AccessFlag"]);
					}
				}
				_PartitionCount = accessFlagList.Count;
			}

			public PartitionQueue[] GetQueues(int memberID)
			{
				DataRow[] rows =  _dt.Select(string.Format("AccessFlag = {0}", memberID % _PartitionCount));
				PartitionQueue[] queues = new PartitionQueue[rows.Length];
				int i = 0;

				foreach (DataRow row in rows)
				{
					queues[i] = new PartitionQueue(row["QueuePath"].ToString(), row["ConnectionString"].ToString());
					i++;						
				}

				return queues;
			}
		}

		protected class PartitionQueue
		{
			private string _QueuePath;
			private string _ConnectionString;

			public PartitionQueue(string queuePath, string connectionString)
			{
				_QueuePath = queuePath;
				_ConnectionString = connectionString;
			}

			public string QueuePath
			{
				get
				{
					return _QueuePath;
				}
			}

			public string ConnectionString
			{
				get
				{
					return _ConnectionString;
				}
			}
		}

		public DateTime UpdateDate
		{
			get
			{
				return _UpdateDate;
			}
			set
			{
				_UpdateDate = value;
			}
		}

	}
}
