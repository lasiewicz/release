using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Data;

namespace Matchnet.List
{
	public class MailSetting
	{
		private int _MemberID;
		private int _PrivateLabelID;
		private DateTime _MutualLastAttemptDate;
		private int _MutualDurationTypeID;
		private int _MutualDuration;
		private DateTime _ViralLastAttemptDate;
		private int _ViralDurationTypeID;
		private int _ViralDuration;

		public int MemberID 
		{
			get 
			{
				return _MemberID;
			}
			set 
			{
				_MemberID = value;
			}
		}

		public int PrivateLabelID 
		{
			get 
			{
				return _PrivateLabelID;
			}
			set 
			{
				_PrivateLabelID = value;
			}
		}

		public DateTime MutualLastAttemptDate 
		{
			get 
			{
				return _MutualLastAttemptDate;
			}
			set 
			{
				_MutualLastAttemptDate = value;
			}
		}

		public int MutualDurationTypeID 
		{
			get 
			{
				return _MutualDurationTypeID;
			}
			set 
			{
				_MutualDurationTypeID = value;
			}
		}

		public int MutualDuration 
		{
			get 
			{
				return _MutualDuration;
			}
			set 
			{
				_MutualDuration = value;
			}
		}

		public DateTime ViralLastAttemptDate 
		{
			get 
			{
				return _ViralLastAttemptDate;
			}
			set 
			{
				_ViralLastAttemptDate = value;
			}
		}

		public int ViralDurationTypeID 
		{
			get 
			{
				return _ViralDurationTypeID;
			}
			set 
			{
				_ViralDurationTypeID = value;
			}
		}

		public int ViralDuration 
		{
			get 
			{
				return _ViralDuration;
			}
			set 
			{
				_ViralDuration = value;
			}
		}

		public void Populate(int memberID, int privateLabelID)
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnList", memberID));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			DataTable table = client.GetDataTable("up_MailSetting_List", CommandType.StoredProcedure);

			if (table != null && table.Rows.Count > 0) 
			{
				DataRow row = table.Rows[0];
				MutualLastAttemptDate = System.Convert.ToDateTime(row["MutualLastAttemptDate"]);
				MutualDurationTypeID = System.Convert.ToInt32(row["MutualDurationTypeID"]);
				MutualDuration = System.Convert.ToInt32(row["MutualDuration"]);
				ViralLastAttemptDate = System.Convert.ToDateTime(row["ViralLastAttemptDate"]);
				ViralDurationTypeID = System.Convert.ToInt32(row["ViralDurationTypeID"]);
				ViralDuration = System.Convert.ToInt32(row["ViralDuration"]);
			}
		}

		public void Save() 
		{
//			SQLClient client = new SQLClient(new SQLDescriptor("mnList", MemberID));
//			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, MemberID);
//			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, PrivateLabelID);
//			client.AddParameter("@MutualLastAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, MutualLastAttemptDate);
//			client.AddParameter("@MutualDurationTypeID", SqlDbType.Int, ParameterDirection.Input, MutualDurationTypeID);
//			client.AddParameter("@MutualDuration", SqlDbType.Int, ParameterDirection.Input, MutualDuration);
//			client.AddParameter("@ViralLastAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, ViralLastAttemptDate);
//			client.AddParameter("@ViralDurationTypeID", SqlDbType.Int, ParameterDirection.Input, ViralDurationTypeID);
//			client.AddParameter("@ViralDuration", SqlDbType.Int, ParameterDirection.Input, ViralDuration);
//			client.ExecuteNonQuery("up_MailSetting_Save", CommandType.StoredProcedure);


			// Instantiate object for saving/adding to queue.
			EmailSettingAddQueueItem queueItem = new EmailSettingAddQueueItem();
			queueItem.MemberID = _MemberID;
			queueItem.PrivateLabelID = _PrivateLabelID;
			queueItem.MutualLastAttemptDate = _MutualLastAttemptDate;
			queueItem.MutualDurationTypeID = _MutualDurationTypeID;
			queueItem.MutualDuration = _MutualDuration;
			queueItem.ViralLastAttemptDate = _ViralLastAttemptDate;
			queueItem.ViralDurationTypeID = _ViralDurationTypeID;
			queueItem.ViralDuration = _ViralDuration;
			//queueItem.Send();


			
		}
	}
}
