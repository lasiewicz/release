using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Matchnet.DataTemp;


namespace Matchnet.List
{
	[Serializable]
	public class YNMList
	{
		private Hashtable _HT;
		private int _MemberID;
		private int _PrivateLabelID;
		private int _DomainID;
		private DateTime _CacheInsertDate;

		public void Populate(int memberID, int domainID)
		{
			if (memberID == 0 || domainID == 0)
			{
				throw new Exception("invalid memberID (" + memberID.ToString() + ") or domainID (" + domainID.ToString() + ")");
			}

			_CacheInsertDate = DateTime.Now;

			_MemberID = memberID;
			_DomainID = domainID;

			SQLClient client = new SQLClient(new SQLDescriptor("mnList", memberID));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);

			DataTable list = client.GetDataTable("up_YNMList_List", CommandType.StoredProcedure);

			_HT = new Hashtable();

			foreach (DataRow row in list.Rows)
			{
				Add(memberID, Convert.ToInt32(row["TargetMemberID"]), 
					(YNMDirectionType)Enum.Parse(typeof(YNMDirectionType), row["DirectionFlag"].ToString()),
					(YNMListType)Enum.Parse(typeof(YNMListType), row["YNMTypeID"].ToString()),
					Convert.ToDateTime(row["UpdateDate"]));
			}
		}


		private string GetKey(int memberID, int targetMemberID, int directionFlag)
		{
			return memberID.ToString() + ":" + targetMemberID.ToString() + ":" + directionFlag;
		}


		public YNMMask GetMask(int targetMemberID)
		{
			if (_MemberID == 0 || _DomainID == 0 || targetMemberID == 0) 
			{
				throw new Exception("invalid memberID (" + _MemberID.ToString() + "), domainID (" + _DomainID.ToString() + "), or targetMemberID (" + targetMemberID.ToString() + ")");
			}

			YNMMask mask = YNMMask.None;
			ListValue listValue;

			object o = null;

			o = _HT[GetKey(_MemberID, targetMemberID, 1)];
			if (o != null)
			{
				listValue = (ListValue)o;
				switch (listValue.YNMListTypeID)
				{
					case YNMListType.Yes:
						mask = mask | YNMMask.MemberYes;
						break;
					case YNMListType.No:
						mask = mask | YNMMask.MemberNo;
						break;
					case YNMListType.Maybe:
						mask = mask | YNMMask.MemberMaybe;
						break;
				}
			}

			o = _HT[GetKey(_MemberID, targetMemberID, 0)];
			if (o != null)
			{
				listValue = (ListValue)o;
				switch (listValue.YNMListTypeID)
				{
					case YNMListType.Yes:
						mask = mask | YNMMask.TargetMemberYes;
						break;
					case YNMListType.No:
						mask = mask | YNMMask.TargetMemberNo;
						break;
					case YNMListType.Maybe:
						mask = mask | YNMMask.TargetMemberMaybe;
						break;
				}
			}

			return mask;
		}



		public DataTable GetMutualYesMembers(int memberID,
			int domainID,
			int startRow,
			int pageSize, 
			out int totalRows)
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("HotListID", typeof(int));
			dt.Columns.Add("MemberID", typeof(int));
			dt.Columns.Add("FromMemberID", typeof(int));
			dt.Columns.Add("ToMemberID", typeof(int));
			dt.Columns.Add("CategoryID", typeof(int));
			dt.Columns.Add("Description", typeof(string));
			dt.Columns.Add("UpdateDate", typeof(DateTime));
			dt.Columns.Add("TeaseID", typeof(int));
			DataTable dtReturn = dt.Copy();

			IDictionaryEnumerator sphincter = _HT.GetEnumerator();

			int hotListID = 1;
			while (sphincter.MoveNext())
			{
				ListValue listValue = (ListValue)sphincter.Value;

				if (listValue.YNMListTypeID == YNMListType.Yes && listValue.Direction == YNMDirectionType.Sender)
				{
					object o = _HT[GetKey(memberID, listValue.TargetMemberID, (int)YNMDirectionType.Recipient)];
	
					if (o != null)
					{
						if (((ListValue)o).YNMListTypeID == YNMListType.Yes)
						{
							dt.Rows.Add(new object[]{hotListID, memberID, memberID, listValue.TargetMemberID, null, null, listValue.UpdateDate, null});
							hotListID++;
						}
					}

				
				}

			}

			dt.DefaultView.Sort = "UpdateDate desc";

			int rowNum;
			for (rowNum = startRow - 1; rowNum < (startRow + pageSize); rowNum++)
			{
				if (rowNum > (dt.Rows.Count - 1))
				{
					break;
				}
				dtReturn.ImportRow(dt.Rows[rowNum]);
			}

			totalRows = dt.Rows.Count;
			return dtReturn;
		}

		public void Add(int memberID,
			int senderMemberID, 
			int targetMemberID,
			YNMListType ynmListTypeID,
			DateTime updateDate)
		{
			YNMDirectionType direction;

			if (memberID == targetMemberID)
			{
				direction = YNMDirectionType.Recipient;
				targetMemberID = senderMemberID;
			}
			else
			{
				direction = YNMDirectionType.Sender;
			}

			Add(memberID, targetMemberID, direction, ynmListTypeID, updateDate);
		}

		public void Add(int memberID,
							int targetMemberID,
							YNMDirectionType direction,
							YNMListType ynmListTypeID,
							DateTime updateDate)
		{
			object o = null;
			string key = GetKey(memberID, targetMemberID, (int)direction);

			o = _HT[key];
			if (o != null)
			{
				ListValue listValue = (ListValue)o;
				listValue.TargetMemberID = targetMemberID;
				listValue.Direction = direction;
				listValue.YNMListTypeID = ynmListTypeID;
				listValue.UpdateDate = updateDate;
				_HT[key] = listValue;
			}
			else
			{
				_HT.Add(key, new ListValue(targetMemberID, direction, ynmListTypeID, updateDate));
			}
		}

		
		public static int Save(
							int memberID,
							int domainID,
							int senderMemberID, 
							int targetMemberID,
							YNMListType ynmListTypeID,
							DateTime updateDate) 
		{
			if (memberID == 0 || domainID == 0 || senderMemberID == 0 || targetMemberID == 0) 
				throw new ArgumentException();

			SQLClient client = new SQLClient(new SQLDescriptor("mnList", memberID));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@SenderMemberID", SqlDbType.Int, ParameterDirection.Input, senderMemberID);
			client.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
			client.AddParameter("@YNMTypeID", SqlDbType.TinyInt, ParameterDirection.Input, System.Convert.ToByte(ynmListTypeID));
			client.AddParameter("@UpdateDate", SqlDbType.SmallDateTime, ParameterDirection.Input, updateDate);
			client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_YNMList_Save";
			client.PopulateCommand(command);
			client.ExecuteNonQuery(command);

			return System.Convert.ToInt32(command.Parameters["@ReturnValue"].Value);
		}


		public static void StatusSave(
							int memberID,
							int domainID,
							int targetMemberID,
							YNMMailType ynmMailTypeID)
		{
			if (memberID == 0 || domainID == 0 || targetMemberID == 0) 
				throw new ArgumentException();

			SQLClient client = new SQLClient(new SQLDescriptor("mnList", memberID));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@MailMask", SqlDbType.Int, ParameterDirection.Input, System.Convert.ToInt32(ynmMailTypeID));

			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_YNMList_Status_Save";
			client.PopulateCommand(command);
			client.ExecuteNonQuery(command);
		}

		
		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}


		public int DomainID
		{
			get
			{
				return _DomainID;
			}
			set
			{
				_DomainID = value;
			}
		}


		public int PrivateLabelID
		{
			get
			{
				return _PrivateLabelID;
			}
			set
			{
				_PrivateLabelID = value;
			}
		}


		public DateTime CacheInsertDate
		{
			get
			{
				return _CacheInsertDate;
			}
			set
			{
				_CacheInsertDate = value;
			}
		}


		[Serializable]
		internal class ListValue
		{
			private int _TargetMemberID;
			private YNMDirectionType _Direction;
			private YNMListType _YNMListTypeID;
			private DateTime _UpdateDate;

			public ListValue(int targetMemberID, YNMDirectionType direction, YNMListType ynmListTypeID, DateTime updateDate)
			{
				_TargetMemberID = targetMemberID;
				_Direction = direction;
				_YNMListTypeID = ynmListTypeID;
				_UpdateDate = updateDate;
			}

			public int TargetMemberID
			{
				get
				{
					return _TargetMemberID;
				}
				set
				{
					_TargetMemberID = value;
				}
			}

			public YNMDirectionType Direction
			{
				get
				{
					return _Direction;
				}
				set
				{
					_Direction = value;
				}
			}

			public YNMListType YNMListTypeID
			{
				get
				{
					return _YNMListTypeID;
				}
				set
				{
					_YNMListTypeID = value;
				}
			}

			public DateTime UpdateDate
			{
				get
				{
					return _UpdateDate;
				}
				set
				{
					_UpdateDate = value;
				}
			}

		}
	}
}
