using System;
using System.Collections;

namespace Matchnet.List
{
	public abstract class CollectionBase
	{
		private ArrayList _Collection = new ArrayList();

		// use wisely, this cannot be modified while enumerating
		// and will not be thread safe
		public ArrayList UnderlyingCollection 
		{
			get 
			{
				return _Collection;
			}
		}

		protected void Add(object obj) 
		{
			_Collection.Add(obj);
		}

		public bool IsSynchronized
		{
			get
			{
				return _Collection.IsSynchronized;
			}
		}

		public int Count
		{
			get
			{
				return _Collection.Count;
			}
		}

		public void CopyTo(Array array, int index)
		{
			_Collection.CopyTo(array, index);
		}

		public object SyncRoot
		{
			get
			{
				return _Collection.SyncRoot;
			}
		}

		public IEnumerator GetEnumerator()
		{
			return _Collection.GetEnumerator();
		}
	}
}
