using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet.List;
using Matchnet.Data;
using Applications.Timer;

namespace WindowsApplication10
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox YNMTargetMemberId;
		private System.Windows.Forms.TextBox YNMSenderMemberId;
		private System.Windows.Forms.TextBox YNMPrivateLabelId;
		private System.Windows.Forms.TextBox YNMMemberId;
		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.TextBox ViralMemberId;
		private System.Windows.Forms.TextBox ViralPrivateLabelId;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.CheckBox checkBox4;
		private System.Windows.Forms.CheckBox checkBox5;
		private System.Windows.Forms.CheckBox checkBox6;
		private System.Windows.Forms.CheckBox checkBox7;
		private System.Windows.Forms.CheckBox checkBox8;
		private System.Windows.Forms.CheckBox checkBox9;

		private YNMList ynm;
		private ViralMail vm;
		private System.Windows.Forms.DataGrid dataGrid2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox MutualMemberId;
		private System.Windows.Forms.TextBox MutualPrivateLabelId;
		private System.Windows.Forms.DataGrid dataGrid3;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox ViralMailTargetMemberIds;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.dataGrid2 = new System.Windows.Forms.DataGrid();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.checkBox9 = new System.Windows.Forms.CheckBox();
			this.checkBox8 = new System.Windows.Forms.CheckBox();
			this.checkBox7 = new System.Windows.Forms.CheckBox();
			this.checkBox6 = new System.Windows.Forms.CheckBox();
			this.checkBox5 = new System.Windows.Forms.CheckBox();
			this.checkBox4 = new System.Windows.Forms.CheckBox();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.button5 = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButton3 = new System.Windows.Forms.RadioButton();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.YNMTargetMemberId = new System.Windows.Forms.TextBox();
			this.YNMSenderMemberId = new System.Windows.Forms.TextBox();
			this.YNMPrivateLabelId = new System.Windows.Forms.TextBox();
			this.YNMMemberId = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.ViralPrivateLabelId = new System.Windows.Forms.TextBox();
			this.ViralMemberId = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.button6 = new System.Windows.Forms.Button();
			this.dataGrid3 = new System.Windows.Forms.DataGrid();
			this.MutualPrivateLabelId = new System.Windows.Forms.TextBox();
			this.MutualMemberId = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.ViralMailTargetMemberIds = new System.Windows.Forms.TextBox();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid2)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid3)).BeginInit();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Location = new System.Drawing.Point(8, 8);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(824, 480);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.dataGrid2);
			this.tabPage1.Controls.Add(this.groupBox2);
			this.tabPage1.Controls.Add(this.button5);
			this.tabPage1.Controls.Add(this.groupBox1);
			this.tabPage1.Controls.Add(this.button2);
			this.tabPage1.Controls.Add(this.button1);
			this.tabPage1.Controls.Add(this.YNMTargetMemberId);
			this.tabPage1.Controls.Add(this.YNMSenderMemberId);
			this.tabPage1.Controls.Add(this.YNMPrivateLabelId);
			this.tabPage1.Controls.Add(this.YNMMemberId);
			this.tabPage1.Controls.Add(this.label4);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(816, 454);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "YNMList";
			// 
			// dataGrid2
			// 
			this.dataGrid2.DataMember = "";
			this.dataGrid2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid2.Location = new System.Drawing.Point(8, 128);
			this.dataGrid2.Name = "dataGrid2";
			this.dataGrid2.Size = new System.Drawing.Size(800, 312);
			this.dataGrid2.TabIndex = 15;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.checkBox9);
			this.groupBox2.Controls.Add(this.checkBox8);
			this.groupBox2.Controls.Add(this.checkBox7);
			this.groupBox2.Controls.Add(this.checkBox6);
			this.groupBox2.Controls.Add(this.checkBox5);
			this.groupBox2.Controls.Add(this.checkBox4);
			this.groupBox2.Controls.Add(this.checkBox3);
			this.groupBox2.Controls.Add(this.checkBox2);
			this.groupBox2.Controls.Add(this.checkBox1);
			this.groupBox2.Location = new System.Drawing.Point(344, 16);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(352, 96);
			this.groupBox2.TabIndex = 14;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Mask";
			// 
			// checkBox9
			// 
			this.checkBox9.Location = new System.Drawing.Point(240, 64);
			this.checkBox9.Name = "checkBox9";
			this.checkBox9.Size = new System.Drawing.Size(96, 16);
			this.checkBox9.TabIndex = 8;
			this.checkBox9.Text = "MutualMaybe";
			// 
			// checkBox8
			// 
			this.checkBox8.Location = new System.Drawing.Point(240, 40);
			this.checkBox8.Name = "checkBox8";
			this.checkBox8.Size = new System.Drawing.Size(88, 16);
			this.checkBox8.TabIndex = 7;
			this.checkBox8.Text = "MutualNo";
			// 
			// checkBox7
			// 
			this.checkBox7.Location = new System.Drawing.Point(240, 16);
			this.checkBox7.Name = "checkBox7";
			this.checkBox7.Size = new System.Drawing.Size(88, 16);
			this.checkBox7.TabIndex = 6;
			this.checkBox7.Text = "MutualYes";
			// 
			// checkBox6
			// 
			this.checkBox6.Location = new System.Drawing.Point(104, 64);
			this.checkBox6.Name = "checkBox6";
			this.checkBox6.Size = new System.Drawing.Size(136, 16);
			this.checkBox6.TabIndex = 5;
			this.checkBox6.Text = "TargetMemberMaybe";
			// 
			// checkBox5
			// 
			this.checkBox5.Location = new System.Drawing.Point(104, 40);
			this.checkBox5.Name = "checkBox5";
			this.checkBox5.Size = new System.Drawing.Size(112, 16);
			this.checkBox5.TabIndex = 4;
			this.checkBox5.Text = "TargetMemberNo";
			// 
			// checkBox4
			// 
			this.checkBox4.Location = new System.Drawing.Point(104, 16);
			this.checkBox4.Name = "checkBox4";
			this.checkBox4.Size = new System.Drawing.Size(120, 16);
			this.checkBox4.TabIndex = 3;
			this.checkBox4.Text = "TargetMemberYes";
			// 
			// checkBox3
			// 
			this.checkBox3.Location = new System.Drawing.Point(8, 64);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.Size = new System.Drawing.Size(104, 16);
			this.checkBox3.TabIndex = 2;
			this.checkBox3.Text = "MemberMaybe";
			// 
			// checkBox2
			// 
			this.checkBox2.Location = new System.Drawing.Point(8, 40);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(80, 16);
			this.checkBox2.TabIndex = 1;
			this.checkBox2.Text = "MemberNo";
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(8, 16);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(88, 16);
			this.checkBox1.TabIndex = 0;
			this.checkBox1.Text = "MemberYes";
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(720, 16);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(88, 24);
			this.button5.TabIndex = 12;
			this.button5.Text = "Populate";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButton3);
			this.groupBox1.Controls.Add(this.radioButton2);
			this.groupBox1.Controls.Add(this.radioButton1);
			this.groupBox1.Location = new System.Drawing.Point(216, 16);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(112, 96);
			this.groupBox1.TabIndex = 11;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "YNM";
			// 
			// radioButton3
			// 
			this.radioButton3.Location = new System.Drawing.Point(8, 72);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(96, 16);
			this.radioButton3.TabIndex = 2;
			this.radioButton3.Text = "Maybe";
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(8, 48);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(96, 16);
			this.radioButton2.TabIndex = 1;
			this.radioButton2.Text = "No";
			// 
			// radioButton1
			// 
			this.radioButton1.Checked = true;
			this.radioButton1.Location = new System.Drawing.Point(8, 24);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(96, 16);
			this.radioButton1.TabIndex = 0;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Yes";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(720, 48);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(88, 24);
			this.button2.TabIndex = 9;
			this.button2.Text = "StatusSave";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(720, 88);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(88, 24);
			this.button1.TabIndex = 8;
			this.button1.Text = "Save";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// YNMTargetMemberId
			// 
			this.YNMTargetMemberId.Location = new System.Drawing.Point(120, 88);
			this.YNMTargetMemberId.Name = "YNMTargetMemberId";
			this.YNMTargetMemberId.Size = new System.Drawing.Size(88, 20);
			this.YNMTargetMemberId.TabIndex = 7;
			this.YNMTargetMemberId.Text = "290213";
			// 
			// YNMSenderMemberId
			// 
			this.YNMSenderMemberId.Location = new System.Drawing.Point(120, 64);
			this.YNMSenderMemberId.Name = "YNMSenderMemberId";
			this.YNMSenderMemberId.Size = new System.Drawing.Size(88, 20);
			this.YNMSenderMemberId.TabIndex = 6;
			this.YNMSenderMemberId.Text = "8833030";
			// 
			// YNMPrivateLabelId
			// 
			this.YNMPrivateLabelId.Location = new System.Drawing.Point(120, 40);
			this.YNMPrivateLabelId.Name = "YNMPrivateLabelId";
			this.YNMPrivateLabelId.Size = new System.Drawing.Size(88, 20);
			this.YNMPrivateLabelId.TabIndex = 5;
			this.YNMPrivateLabelId.Text = "1";
			// 
			// YNMMemberId
			// 
			this.YNMMemberId.Location = new System.Drawing.Point(120, 16);
			this.YNMMemberId.Name = "YNMMemberId";
			this.YNMMemberId.Size = new System.Drawing.Size(88, 20);
			this.YNMMemberId.TabIndex = 4;
			this.YNMMemberId.Text = "8833030";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 88);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(104, 16);
			this.label4.TabIndex = 3;
			this.label4.Text = "TargetMemberID";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 64);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(96, 16);
			this.label3.TabIndex = 2;
			this.label3.Text = "SenderMemberID";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 16);
			this.label2.TabIndex = 1;
			this.label2.Text = "PrivateLabelID";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "MemberID";
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.ViralMailTargetMemberIds);
			this.tabPage2.Controls.Add(this.label9);
			this.tabPage2.Controls.Add(this.button4);
			this.tabPage2.Controls.Add(this.button3);
			this.tabPage2.Controls.Add(this.dataGrid1);
			this.tabPage2.Controls.Add(this.ViralPrivateLabelId);
			this.tabPage2.Controls.Add(this.ViralMemberId);
			this.tabPage2.Controls.Add(this.label6);
			this.tabPage2.Controls.Add(this.label5);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(816, 454);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Viral Mail";
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(344, 16);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(88, 24);
			this.button4.TabIndex = 6;
			this.button4.Text = "Status Save";
			this.button4.Click += new System.EventHandler(this.button4_Click_1);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(240, 16);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(88, 24);
			this.button3.TabIndex = 5;
			this.button3.Text = "Populate";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// dataGrid1
			// 
			this.dataGrid1.DataMember = "";
			this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid1.Location = new System.Drawing.Point(8, 128);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.Size = new System.Drawing.Size(800, 304);
			this.dataGrid1.TabIndex = 4;
			// 
			// ViralPrivateLabelId
			// 
			this.ViralPrivateLabelId.Location = new System.Drawing.Point(104, 40);
			this.ViralPrivateLabelId.Name = "ViralPrivateLabelId";
			this.ViralPrivateLabelId.Size = new System.Drawing.Size(104, 20);
			this.ViralPrivateLabelId.TabIndex = 3;
			this.ViralPrivateLabelId.Text = "1";
			// 
			// ViralMemberId
			// 
			this.ViralMemberId.Location = new System.Drawing.Point(104, 16);
			this.ViralMemberId.Name = "ViralMemberId";
			this.ViralMemberId.Size = new System.Drawing.Size(104, 20);
			this.ViralMemberId.TabIndex = 2;
			this.ViralMemberId.Text = "8833030";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(16, 40);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(80, 16);
			this.label6.TabIndex = 1;
			this.label6.Text = "PrivateLabelID";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 0;
			this.label5.Text = "MemberID";
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.button6);
			this.tabPage3.Controls.Add(this.dataGrid3);
			this.tabPage3.Controls.Add(this.MutualPrivateLabelId);
			this.tabPage3.Controls.Add(this.MutualMemberId);
			this.tabPage3.Controls.Add(this.label8);
			this.tabPage3.Controls.Add(this.label7);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(816, 454);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Mutual Match";
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(240, 8);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(104, 24);
			this.button6.TabIndex = 5;
			this.button6.Text = "Populate";
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// dataGrid3
			// 
			this.dataGrid3.DataMember = "";
			this.dataGrid3.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid3.Location = new System.Drawing.Point(8, 64);
			this.dataGrid3.Name = "dataGrid3";
			this.dataGrid3.Size = new System.Drawing.Size(800, 384);
			this.dataGrid3.TabIndex = 4;
			// 
			// MutualPrivateLabelId
			// 
			this.MutualPrivateLabelId.Location = new System.Drawing.Point(112, 32);
			this.MutualPrivateLabelId.Name = "MutualPrivateLabelId";
			this.MutualPrivateLabelId.Size = new System.Drawing.Size(104, 20);
			this.MutualPrivateLabelId.TabIndex = 3;
			this.MutualPrivateLabelId.Text = "1";
			// 
			// MutualMemberId
			// 
			this.MutualMemberId.Location = new System.Drawing.Point(112, 8);
			this.MutualMemberId.Name = "MutualMemberId";
			this.MutualMemberId.Size = new System.Drawing.Size(104, 20);
			this.MutualMemberId.TabIndex = 2;
			this.MutualMemberId.Text = "8833030";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(8, 32);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(88, 16);
			this.label8.TabIndex = 1;
			this.label8.Text = "PrivateLabelID";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 8);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(88, 16);
			this.label7.TabIndex = 0;
			this.label7.Text = "MemberID";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(16, 72);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(104, 16);
			this.label9.TabIndex = 7;
			this.label9.Text = "TargetMemberIDs";
			// 
			// ViralMailTargetMemberIds
			// 
			this.ViralMailTargetMemberIds.Location = new System.Drawing.Point(136, 72);
			this.ViralMailTargetMemberIds.Name = "ViralMailTargetMemberIds";
			this.ViralMailTargetMemberIds.Size = new System.Drawing.Size(368, 20);
			this.ViralMailTargetMemberIds.TabIndex = 8;
			this.ViralMailTargetMemberIds.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(840, 493);
			this.Controls.Add(this.tabControl1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid2)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			this.tabPage3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid3)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				int returnValue;
				YNMListType ynmListType = YNMListType.Yes;
				
				if (radioButton1.Checked)
					ynmListType = YNMListType.Yes;
				
				if (radioButton2.Checked)
					ynmListType = YNMListType.No;

				if (radioButton3.Checked)
					ynmListType = YNMListType.Maybe;

				returnValue = YNMList.Save(
								System.Convert.ToInt32(YNMMemberId.Text),
								System.Convert.ToInt32(YNMPrivateLabelId.Text),
								System.Convert.ToInt32(YNMSenderMemberId.Text),
								System.Convert.ToInt32(YNMTargetMemberId.Text),
								ynmListType,
								DateTime.Now);
					
				if (returnValue != 0)
					MessageBox.Show(returnValue.ToString());
			}
			catch(Exception ex) { MessageBox.Show(ex.Message); }

		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			try
			{
				vm = new ViralMail();
				vm.Populate(
					System.Convert.ToInt32(ViralMemberId.Text),
					System.Convert.ToInt32(ViralPrivateLabelId.Text));

				if (vm.List != null)
				{
					dataGrid1.SetDataBinding(vm.List, string.Empty);
				}
			}
			catch(Exception ex) { MessageBox.Show(ex.Message); }
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			try
			{
				ClearMask();

				ynm = new YNMList();
				
				//HighPerformanceTimer ht = new HighPerformanceTimer();
				//ht.Start();

				ynm.Populate(
					System.Convert.ToInt32(YNMMemberId.Text), 
					System.Convert.ToInt32(YNMPrivateLabelId.Text));
				
				if (ynm.List != null)
				{
					dataGrid2.SetDataBinding(ynm.List, string.Empty);
					dataGrid2.CurrentCellChanged += new System.EventHandler(this.dataGrid2_CurrentCellChanged);
				}
				//ht.Stop();
				//MessageBox.Show(ht.Duration.ToString());
			}
			catch(Exception ex) { MessageBox.Show(ex.Message); }
		}

		private void SetMask(int mask)
		{
			checkBox1.Checked = (mask & 1) == 1;
			checkBox2.Checked = (mask & 2) == 2;
			checkBox3.Checked = (mask & 4) == 4;
			checkBox4.Checked = (mask & 8) == 8;
			checkBox5.Checked = (mask & 16) == 16;
			checkBox6.Checked = (mask & 32) == 32;
			checkBox7.Checked = (mask & 64) == 64;
			checkBox8.Checked = (mask & 128) == 128;
			checkBox9.Checked = (mask & 256) == 256;
		}

		private void ClearMask()
		{
			checkBox1.Checked = false;
			checkBox2.Checked = false;
			checkBox3.Checked = false;
			checkBox4.Checked = false;
			checkBox5.Checked = false;
			checkBox6.Checked = false;
			checkBox7.Checked = false;
			checkBox8.Checked = false;
			checkBox9.Checked = false;
		}

		private void dataGrid2_CurrentCellChanged(object sender, System.EventArgs e)
		{
			DataGrid dg = (DataGrid) sender;
			DataTable dt = (DataTable) dg.DataSource;
			DataRow dr = dt.Rows[dg.CurrentRowIndex];
			int targetMemberId = System.Convert.ToInt32(dr["TargetMemberID"]);
			
			ClearMask();
			
				if (ynm != null)
			{
				SetMask(ynm.GetMask(targetMemberId));
			}
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			if (vm != null)
			{
				vm.SaveMailStatus();
			}
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			int memberId = System.Convert.ToInt32(MutualMemberId.Text);
			int privateLabelId = System.Convert.ToInt32(MutualPrivateLabelId.Text);

			try
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnList", memberId));
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelId);

				DataTable list = client.GetDataTable("up_MutualMail_List", CommandType.StoredProcedure);

				if (list != null)
					dataGrid3.SetDataBinding(list, null);
			}
			catch(Exception ex) { MessageBox.Show(ex.Message); }
		}

		private void button4_Click_1(object sender, System.EventArgs e)
		{
			int memberId = System.Convert.ToInt32(ViralMemberId.Text);
			int privateLabelId = System.Convert.ToInt32(ViralPrivateLabelId.Text);

			try
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnList", memberId));
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
				client.AddParameter("@TargetMemberIDs", SqlDbType.VarChar, ParameterDirection.Input, ViralMailTargetMemberIds.Text);
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelId);
				client.AddParameter("@MailMask", SqlDbType.Int, ParameterDirection.Input, System.Convert.ToInt32(Matchnet.List.YNMMailType.Viral));

				DataTable list = client.GetDataTable("up_YNMList_Multiple_Status_Save", CommandType.StoredProcedure);
				MessageBox.Show("Updated");
			}
			catch(Exception ex) { MessageBox.Show(ex.Message); }
		}
	}
}
