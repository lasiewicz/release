using System;

namespace Matchnet.List
{
	public enum YNMListType 
	{
		Yes = 1,
		No = 2,
		Maybe = 3
	};

	public enum YNMDirectionType 
	{
		Sender = 1, 
		Recipient = 0
	};

	public enum YNMMailType 
	{
		Match = 1, 
		Viral = 2
	};

	[FlagsAttribute]
	public enum YNMMask
	{
		None = 0,
		MemberYes = 1,
		MemberNo = 2,
		MemberMaybe = 4,
		TargetMemberYes = 8,
		TargetMemberNo = 16,
		TargetMemberMaybe = 32
	}
}
