using System;

namespace Matchnet.List
{
	public abstract class Mail
	{
		private int _MemberID;
		private int _PrivateLabelID;
		private DateTime _NextAttemptDate;

		public int MemberID 
		{
			get 
			{
				return _MemberID;
			}
			set 
			{
				_MemberID = value;
			}
		}

		public int PrivateLabelID 
		{
			get 
			{
				return _PrivateLabelID;
			}
			set 
			{
				_PrivateLabelID = value;
			}
		}

		public DateTime NextAttemptDate 
		{
			get 
			{
				return _NextAttemptDate;
			}
			set 
			{
				_NextAttemptDate = value;
			}
		}
	}
}
