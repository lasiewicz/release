using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.DataTemp;

namespace Matchnet.List
{
	public class ViralMail : Mail
	{
		private DataTable _list;
		private int _memberId;
		private int _privateLabelId;

		public DataTable List
		{ 
			get { return _list; } 
		}

		public void Populate(int memberId, int privateLabelId)
		{
			if (memberId == 0 || privateLabelId == 0) 
				throw new ArgumentException();

			_memberId = memberId;
			_privateLabelId = privateLabelId;

			SQLClient client = new SQLClient(new SQLDescriptor("mnList", memberId));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelId);

			_list = client.GetDataTable("up_YNMList_ViralMail_List", CommandType.StoredProcedure);
		}

		public void SaveMailStatus() 
		{
			if (_memberId == 0 || _privateLabelId == 0) 
				throw new ArgumentException();

			if (_list.Rows.Count == 0) return;

			string members = string.Empty;

			foreach(DataRow dr in _list.Rows)
			{
				members += (members.Length > 0) ? "," : string.Empty;
				members += dr["TargetMemberID"].ToString();
			}
			
			SQLClient client = new SQLClient(new SQLDescriptor("mnList", _memberId));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _memberId);
			client.AddParameter("@TargetMemberIDs", SqlDbType.VarChar, ParameterDirection.Input, members);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, _privateLabelId);
			client.AddParameter("@MailMask", SqlDbType.Int, ParameterDirection.Input, System.Convert.ToInt32(YNMMailType.Viral));

			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_YNMList_Multiple_Status_Save";
			client.PopulateCommand(command);
			client.ExecuteNonQuery(command);
		}

	}
}
