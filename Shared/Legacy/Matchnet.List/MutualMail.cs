using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.DataTemp;

namespace Matchnet.List
{
	public class MutualMail : Mail
	{
		private int _MutualMailID;

		public int MutualMailID 
		{
			get 
			{
				return _MutualMailID;
			}
			set 
			{
				_MutualMailID = value;
			}
		}

		public void Save() 
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnList", MemberID));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, MemberID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, PrivateLabelID);
			client.ExecuteNonQuery("up_MutualMail_Save");
		}
	}
}
