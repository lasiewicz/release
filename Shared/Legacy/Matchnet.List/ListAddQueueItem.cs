using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Messaging;

using Matchnet.CachingTemp;
using Matchnet.DataTemp;
using Matchnet.Lib.Member;
using Matchnet.MessageQueue;


namespace Matchnet.List
{
	[Serializable]
	public class ListAddQueueItem : QueueItemBase
	{
		private int _SenderMemberID; 
		private int _TargetMemberID;
		private YNMListType _YNMListTypeID;
		private bool _QueueMutual = false;
		private DateTime _UpdateDate = DateTime.Now;
		private bool _Mirrorable = true;
		private string _ConnectionString;
		private int _MemberID;
		private int _PrivateLabelID;
		private int _DomainID;

		public ListAddQueueItem() : base() {}


		public bool QueueMutual 
		{
			get 
			{
				return _QueueMutual;
			}
			set 
			{
				_QueueMutual = value;
			}
		}
		

		public static void Send(ListAddQueueItem item)
		{
			Send(item, null);
		}
		

		public static void Send(ListAddQueueItem item, MessageQueueTransaction transaction)
		{
			Cache cache = Cache.GetInstance();
			const string KEY = "ListPartitionConfig";
			ListPartitionConfig config;

			object o = cache.Get(KEY);
			if (o == null)
			{
				config = new ListPartitionConfig();
				config.Populate();
				cache.Insert(KEY, config, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
			}
			else
			{
				config = (ListPartitionConfig)o;
			}

			try
			{
				foreach (PartitionQueue queue in config.GetQueues(item.MemberID))
				{
					//this is cheesy - i was going to use Queue.Transactional but it does not work on remote private queues
					if (transaction == null && queue.QueuePath.ToLower().IndexOf("transactional") != -1)
					{
						transaction = new MessageQueueTransaction();
						transaction.Begin();
					}

					item.Recoverable = true;
					item.QueuePath = queue.QueuePath;
					item.ConnectionString = queue.ConnectionString;

					QueueItemBase.Send(item, transaction);
					// turn off the queueing to alerts, because we only want to 
					// do it once.
					if (item.QueueMutual) 
					{
						item.QueueMutual = false;
					}
				}

				//save reciprocal list items
				if (item.Mirrorable)
				{
					ListAddQueueItem queueItem = new ListAddQueueItem();
					item.Recoverable = true;
					queueItem.PrivateLabelID = item.PrivateLabelID;
					queueItem.DomainID = item.DomainID;
					queueItem.MemberID = item.TargetMemberID;
					queueItem.SenderMemberID = item.SenderMemberID;
					queueItem.TargetMemberID = item.TargetMemberID;
					queueItem.YNMListTypeID = item.YNMListTypeID;
					queueItem.QueueMutual = true; // queue a mutual
					queueItem.Mirrorable = false; //important, this prevents infinite loop of saving mirrored entries
					ListAddQueueItem.Send(queueItem, transaction);
				}

				if (transaction != null)
				{
					if (transaction.Status == MessageQueueTransactionStatus.Pending)
					{
						transaction.Commit();
					}
				}
			}
			catch (Exception ex)
			{
				if (transaction != null)
				{
					transaction.Abort();
				}
				throw ex;
			}
		}


		public override void Execute()
		{
			SQLClient client = new SQLClient(_ConnectionString);
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _MemberID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, _PrivateLabelID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, _DomainID);
			client.AddParameter("@SenderMemberID", SqlDbType.Int, ParameterDirection.Input, _SenderMemberID);
			client.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, _TargetMemberID);
			client.AddParameter("@YNMTypeID", SqlDbType.TinyInt, ParameterDirection.Input, System.Convert.ToByte(_YNMListTypeID));
			client.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, _UpdateDate);
			client.ExecuteNonQuery("up_YNMList_Save", CommandType.StoredProcedure);

			if (_QueueMutual) 
			{
				if (_YNMListTypeID == YNMListType.Yes) 
				{
					if (_SenderMemberID != MemberID) 
					{
						SendViralAlert();
					}
					if (IsMutual())
					{
						SendMutualAlert();
					}
				}
			}
		}


		private bool IsMutual() 
		{
			try
			{
				SQLClient client = new SQLClient(new SQLDescriptor( "mnList", _SenderMemberID));
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _SenderMemberID);
				client.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, _TargetMemberID);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, _DomainID);
				DataTable table = client.GetDataTable("up_YNMList_MutualMail_List", CommandType.StoredProcedure);

				return System.Convert.ToInt32(table.Rows[0][0]) == 1;
			}
			catch (Exception ex)
			{
				throw new Exception("\nMemberID: " + _SenderMemberID.ToString() + "\nTargetMemberID: " + _TargetMemberID.ToString() + "\nDomainID: " + _DomainID.ToString() + "\n", ex);
			}
		}


		private int GetViralNewsletterPeriod() 
		{
			MemberProfile memberProfile = new MemberProfile();
			Matchnet.Labels.PrivateLabel privateLabel = new Matchnet.Labels.PrivateLabel();
			privateLabel.Load(PrivateLabelID);
			return memberProfile.GetMemberAttributeIntValue(TargetMemberID, "GotAClickEmailPeriod", privateLabel.DomainID, privateLabel.LanguageID, 7);
		}


		private void SendViralAlert() 
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnAlert", 0));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, TargetMemberID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, PrivateLabelID);
			client.AddParameter("@ViralNewsletterPeriod", SqlDbType.Int, ParameterDirection.Input, GetViralNewsletterPeriod());
			client.ExecuteNonQuery("up_ViralMail_Save", CommandType.StoredProcedure);
		}


		private void SendMutualAlert() 
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnAlert", 0));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _SenderMemberID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, PrivateLabelID);
			client.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, _TargetMemberID);
			client.AddParameter("@MutualMailID", SqlDbType.Int, ParameterDirection.Output);
			client.ExecuteNonQuery("up_MutualMail_Save", CommandType.StoredProcedure);
		}
		

		public bool Mirrorable
		{
			get
			{
				return _Mirrorable;
			}
			set
			{
				_Mirrorable = value;
			}
		}


		public string ConnectionString
		{
			get
			{
				return _ConnectionString;
			}
			set
			{
				_ConnectionString = value;
			}
		}


		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}


		public int PrivateLabelID
		{
			get
			{
				return _PrivateLabelID;
			}
			set
			{
				_PrivateLabelID = value;
			}
		}


		public int DomainID
		{
			get
			{
				return _DomainID;
			}
			set
			{
				_DomainID = value;
			}
		}


		public int SenderMemberID
		{
			get
			{
				return _SenderMemberID;
			}
			set
			{
				_SenderMemberID = value;
			}
		}


		public int TargetMemberID
		{
			get
			{
				return _TargetMemberID;
			}
			set
			{
				_TargetMemberID = value;
			}
		}


		public YNMListType YNMListTypeID
		{
			get
			{
				return _YNMListTypeID;
			}
			set
			{
				_YNMListTypeID = value;
			}
		}


		public DateTime UpdateDate
		{
			get
			{
				return _UpdateDate;
			}
			set
			{
				_UpdateDate = value;
			}
		}


		protected class ListPartitionConfig
		{
			private DataTable _dt;
			private int _PartitionCount;

			public ListPartitionConfig()
			{
			}


			public void Populate()
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnMaster"));
				client.AddParameter("@LogicalDatabase", SqlDbType.VarChar, ParameterDirection.Input, "mnList");

				_dt = client.GetDataTable("up_LogicalPhysicalDatabases_List", CommandType.StoredProcedure);

				ArrayList accessFlagList = new ArrayList();
				foreach (DataRow row in _dt.Rows)
				{
					if (!accessFlagList.Contains(row["AccessFlag"]))
					{
						accessFlagList.Add(row["AccessFlag"]);
					}
				}
				_PartitionCount = accessFlagList.Count;
			}


			public PartitionQueue[] GetQueues(int memberID)
			{
				DataRow[] rows =  _dt.Select(string.Format("AccessFlag = {0}", memberID % _PartitionCount));
				PartitionQueue[] queues = new PartitionQueue[rows.Length];
				int i = 0;

				foreach (DataRow row in rows)
				{
					queues[i] = new PartitionQueue(row["QueuePath"].ToString(), row["ConnectionString"].ToString());
					i++;						
				}

				return queues;
			}
		}

		protected class PartitionQueue
		{
			private string _QueuePath;
			private string _ConnectionString;

			public PartitionQueue(string queuePath, string connectionString)
			{
				_QueuePath = queuePath;
				_ConnectionString = connectionString;
			}


			public string QueuePath
			{
				get
				{
					return _QueuePath;
				}
			}


			public string ConnectionString
			{
				get
				{
					return _ConnectionString;
				}
			}
		}

	}
}
