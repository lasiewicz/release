using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.DataTemp;

namespace Matchnet.List
{
	public class MutualMailCollection : CollectionBase
	{
		public void Populate(int memberID) 
		{
			Populate(memberID, 0);
		}

		public void Populate(int memberID, int privateLabelID) 
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnList", memberID));
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			if (privateLabelID > 0) 
			{
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			}

			DataTable table = client.GetDataTable("up_MutualMail_List", CommandType.StoredProcedure);
			
			if (table != null && table.Rows.Count > 0) 
			{
				foreach (DataRow row in table.Rows) 
				{
					MutualMail mutualMail = new MutualMail();
					mutualMail.MemberID = memberID;

					if (privateLabelID > 0) 
					{
						mutualMail.PrivateLabelID = privateLabelID;
					} 
					else 
					{
						mutualMail.PrivateLabelID = System.Convert.ToInt32(row["PrivateLabelID"]);
					}

					mutualMail.MutualMailID = System.Convert.ToInt32(row["MutualMailID"]);
					mutualMail.NextAttemptDate = System.Convert.ToDateTime(row["NextAttemptDate"]);

					Add(mutualMail);
				}
			}
		}

		public void Save() 
		{
			foreach(MutualMail mail in this) 
			{
				mail.Save();
			}
		}
	}
}
