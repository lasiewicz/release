using System;

namespace Matchnet.Session
{
	/// <summary>
	/// An item that exists in a UserSession
	/// </summary>
	public class SessionItem
	{
		private string _name = "";
		private string _value = "";
		private SessionItemType _itemType = SessionItemType.Temporary;

		/// <summary>
		/// The unique name used as the key to 
		/// retrieve the value.
		/// </summary>
		public string Name 
		{
			get { return _name; }
			set { _name = value; }
		}

		/// <summary>
		/// The value of the item.
		/// </summary>
		public string Value 
		{
			get { return _value; }
			set { _value = value; }
		}

		/// <summary>
		/// The type identifier which defines how
		/// the item should be persisted.
		/// </summary>
		public SessionItemType ItemType 
		{
			get { return _itemType; }
			set { _itemType = value; }
		}

		/// <summary>
		/// Override Equals so we can define equality between
		/// objects based on the name property.
		/// </summary>
		/// <param name="obj">The object to compare</param>
		/// <returns>Whether or not instances are equal</returns>
		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			if (this.GetType() != obj.GetType()) return false;
			SessionItem other = (SessionItem)obj;

			// if the names are the same, they are equal in 
			// the context that we use it.
			return other.Name == this.Name;
		}

		public override int GetHashCode()
		{
			return _name.GetHashCode();
		}
	}
}