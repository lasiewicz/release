using System;
using System.Collections;

namespace Matchnet.Session
{
	public class SessionItemCollection : CollectionBase
	{
		public SessionItem this[int index]
		{
			get{ return (SessionItem)base.InnerList[index]; }
		}

		public int Add(SessionItem sessionItem)
		{
			if (base.InnerList.Contains(sessionItem)) 
			{
				base.InnerList.Remove(sessionItem);
			}
			return base.InnerList.Add(sessionItem);
		}

		public void Remove(SessionItem sessionItem) 
		{
			base.InnerList.Remove(sessionItem);
		}

		public SessionItem this[string name] 
		{
			get 
			{
				foreach (SessionItem item in this) 
				{
					if (item.Name.ToLower() == name.ToLower()) 
					{
						return item;
					}
				}
				return null;
			}
		}	
	}
}