using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.DataTemp;
using Matchnet.Lib;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Member;
using Matchnet.Lib.Util;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Session
{
	/// <summary>
	/// Provide Member data using session as
	/// the data store.
	/// </summary>
	public class SessionMember
	{
		private Matchnet.Session.ValueObjects.UserSession _userSession;

		public SessionMember(Matchnet.Session.ValueObjects.UserSession session)
		{
			_userSession = session;
		}

		public int MemberID 
		{
			get { return _userSession.MemberID; }
		}

		public bool IsLoggedIn 
		{
			get { return (MemberID != 0 && MemberID != Constants.NULL_INT); }
		}

		public DateTime Birthdate 
		{
			get 
			{
				return Util.CDateTime(_userSession["BirthDate"], DateTime.MinValue);
			}
		}

		public bool IsAdmin 
		{
			get 
			{
				return _userSession.Value("IsAdmin", "0") == "1";
			}
		}

		public bool IsPayingMemberFlag 
		{
			get 
			{
				return _userSession.Value("IsPayingMemberFlag", "0") == "1";
			} 
		}

		public string Username 
		{
			get 
			{
				return _userSession["UserName"];
			}
		}

		public int RegionID 
		{
			get 
			{
				return Util.CInt(_userSession["RegionID"], Matchnet.Constants.NULL_INT);
			}
		}

		public int GenderMask 
		{
			get 
			{
				return Util.CInt(_userSession["GenderMask"], Matchnet.Constants.NULL_INT);
			}
		}

		// TODO: Remove this when we can compile without it.

		/// <summary>
		/// 
		/// </summary>
		/// <exception cref="NotSupportedException">Not Supported. Use g.PrivateLabel.DomainID</exception>
/*		public int DomainID 
		{
			get 
			{
				throw new NotSupportedException("Use g.PrivateLabel.DomainID");
			}
		}*/

		public int PromotionID 
		{
			get 
			{
				return Util.CInt(_userSession["PromotionID"], Matchnet.Constants.NULL_INT);
			}
		}

		public int StartLogonSession(int memberID, int domainID, int privateLabelID, string emailAddress, string userName, bool IsNewRegistration)
		{
			Member member = new Member();
			member.Populate(memberID, domainID, privateLabelID, emailAddress, userName);

			if ((member.GlobalStatusMask & ConstantsTemp.ADMIN_SUSPEND) == ConstantsTemp.ADMIN_SUSPEND || member.AdminSuspendedFlag) 
			{
				return 519041;
			}
			else
			{
				_userSession.Clear();
				_userSession.Add("EmailAddress", emailAddress, SessionPropertyLifetime.Persistent);
				_userSession.Add("MemberID", Convert.ToString(memberID), SessionPropertyLifetime.Temporary);
				_userSession.Add("GenderMask", Convert.ToString(member.GenderMask), SessionPropertyLifetime.Temporary);
				_userSession.Add("HideMask", Convert.ToString(member.HideMask), SessionPropertyLifetime.Temporary);
				// HACK:  Do not save the PromotionID if this is a new registration or else the value won't get saved by
				// RegistrationStep1Handler.registerNewUser().
				if (!IsNewRegistration)
					_userSession.Add("PromotionID", Convert.ToString(member.PromotionID), SessionPropertyLifetime.Temporary);
				_userSession.Add("GlobalStatusMask", Convert.ToString(member.GlobalStatusMask), SessionPropertyLifetime.Temporary);
				_userSession.Add("BirthDate", Convert.ToString(member.Birthdate), SessionPropertyLifetime.Temporary);
				_userSession.Add("RegionID", Convert.ToString(member.RegionID), SessionPropertyLifetime.Temporary);
				_userSession.Add("UserName", userName, SessionPropertyLifetime.Temporary);
				_userSession.Add("HasPhotoFlag", member.HasPhotoFlag ? "1" : "0", SessionPropertyLifetime.Temporary);
				_userSession.Add("IsAdmin", member.IsAdmin ? "1" : "0", SessionPropertyLifetime.Temporary);
				_userSession.Add("SelfSuspendedFlag", member.SelfSuspendedFlag ? "1" : "0", SessionPropertyLifetime.Temporary);
				_userSession.Add("IsPayingMemberFlag", member.IsPayingMemberFlag ? "1" : "0", SessionPropertyLifetime.Temporary);
				_userSession.Add("nxtRegStp", Convert.ToString(member.NextRegistrationActionPageID), SessionPropertyLifetime.Temporary);

				// at login, we don't know of a saved search
				_userSession.Add("HasSavedSearchFlag", "0", SessionPropertyLifetime.Temporary);
				return 0;
			}
		}

		/// <summary>
		/// OVERLOADED:  If IsNewRegistration is not provided, assume this is not a new registration.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="domainID"></param>
		/// <param name="privateLabelID"></param>
		/// <param name="emailAddress"></param>
		/// <param name="userName"></param>
		/// <returns></returns>
		public int StartLogonSession(int memberID, int domainID, int privateLabelID, string emailAddress, string userName)
		{
			return StartLogonSession(memberID, domainID, privateLabelID, emailAddress, userName, false);
		}

		public int Logon(int domainID, int privateLabelID, string emailAddress, string password, bool IsNewRegistration)
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnLogon"));

			client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);
			client.AddParameter("@EmailAddress", SqlDbType.NVarChar, ParameterDirection.Input, emailAddress, 255);
			client.AddParameter("@Password", SqlDbType.NVarChar, ParameterDirection.Input, password, 50);
			
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "up_Member_Logon";
			client.PopulateCommand(cmd);

			DataTable dt = client.GetDataTable(cmd);
		
			// int resource = Content.Translator.ConvertToResource(cmd.Parameters["@ReturnValue"].Value);
			int resource = Convert.ToInt32( cmd.Parameters["@ReturnValue"].Value );

			//if (resource == Content.Translator.Resources.ERROR_SUCCESS)
			if( resource == 0 )
			{
				if (dt.Rows.Count > 0)
				{
					DataRow row = dt.Rows[0];

					int memberID = System.Convert.ToInt32(row["MemberID"]);
					string userName = row["UserName"].ToString();

					resource = StartLogonSession(memberID, domainID, privateLabelID, emailAddress, userName, IsNewRegistration);
				}
			}
			
			return resource;
		}

		/// <summary>
		/// OVERLOADED:  If IsNewRegistration is not provided, assume this is not a new registration.
		/// </summary>
		/// <param name="domainID"></param>
		/// <param name="privateLabelID"></param>
		/// <param name="emailAddress"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		public int Logon(int domainID, int privateLabelID, string emailAddress, string password)
		{
			return Logon(domainID, privateLabelID, emailAddress, password, false);
		}

		public int LogonCupidon(int domainID, int privateLabelID, string loginName,string password)
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnLogon"));

			client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);
			client.AddParameter("@LoginName", SqlDbType.NVarChar, ParameterDirection.Input, loginName, 50);
			client.AddParameter("@Password", SqlDbType.NVarChar, ParameterDirection.Input, password, 50);
			
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "up_Member_Logon_Cupidon";
			client.PopulateCommand(cmd);

			DataTable dt = client.GetDataTable(cmd);
		
			int resource = Convert.ToInt32( cmd.Parameters["@ReturnValue"].Value );

			//if (resource == Content.Translator.Resources.ERROR_SUCCESS)
			if (resource == 0 )
			{
				if (dt.Rows.Count > 0)
				{
					DataRow row = dt.Rows[0];

					string memberID = row["MemberID"].ToString();
					string emailAddress = row["EmailAddress"].ToString();
					string userName = row["UserName"].ToString();

					_userSession.Clear();
						
					_userSession.Add("CupidMemberId", memberID, SessionPropertyLifetime.Temporary);
					_userSession.Add("CupidEmailAddress", emailAddress, SessionPropertyLifetime.Temporary);
					_userSession.Add("CupidUserName", userName, SessionPropertyLifetime.Temporary);
				}
			}
			
			return resource;
		}
	}
}
