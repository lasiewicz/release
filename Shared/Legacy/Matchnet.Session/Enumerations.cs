using System;

namespace Matchnet.Session
{
	/// <summary>
	/// Types of supported SessionItems.
	/// </summary>
	public enum SessionItemType : int
	{
		/// <summary>
		/// Persisted to a cookie
		/// </summary>
		Permanent = 1,
		/// <summary>
		/// Persisted to the database
		/// </summary>
		Temporary = 2,
		/// <summary>
		/// Persisted to the database beyond clear
		/// </summary>
		TemporaryPersist = 4
	}

	public enum SessionOperatorType : int
	{
		Equal = 8,
		BitwiseAnd = 100,
		BitwiseOr = 101,
		Bitwise2sComplement = 102,
		BitwiseClear = 103
	}
}
