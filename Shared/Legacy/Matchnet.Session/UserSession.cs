using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

using Matchnet.DataTemp;
using Matchnet.Lib;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Member;
using Matchnet.Lib.Util;

namespace Matchnet.Session
{
	/// <summary>
	/// A UserSession contains persisted values for
	/// a particular session.
	/// </summary>
	public class UserSession
	{
		private const string MATCHNET_COOKIE = "mnc";
		private const int MATCHNET_COOKIE_VERSION = 4;
		private const string DEV_COOKIE = "dev";

		private const string SESSION_UID_KEY = "sid";
		private const string SESSION_LAST_UPDATE_KEY = "lupk";

		private const double DEFAULT_KEEP_ALIVE = 30;

		private const string ITEM_TOKEN = "|D|";
		private const string VALUE_TOKEN = "|=|";
		private const string PERSIST_TOKEN = "|~|";

		private const int GUID_LENGTH = 40;
		private const int IP_ADDRESS = 15;
		private const int SESSION_HOSTNAME = 15;
		private const int SESSION_VALUE = 3900;

		private SessionItemCollection _itemCollection;

		private double _keepAliveFrequencyInSeconds;
		private DateTime _keepAliveDate = DateTime.MinValue;
		private int _memberID;
		private string _matchnetCookieName;
		
		private bool _writeCookie = false;
		private bool _saveSession = false;
		private bool _isInitialized = false;

		private int _sessionID;
		private string _sessionKey;
		private int _dalKey;

		public void Init() 
		{
			Init(false, DEFAULT_KEEP_ALIVE);
		}

		public void Init(bool developmentMode) 
		{
			Init(developmentMode, DEFAULT_KEEP_ALIVE);
		}

		public void Init(bool developmentMode, double keepAliveSeconds) 
		{
			_itemCollection = new SessionItemCollection();
			_memberID = 0;
			_matchnetCookieName = developmentMode ? DEV_COOKIE : MATCHNET_COOKIE + MATCHNET_COOKIE_VERSION;
			_keepAliveFrequencyInSeconds = keepAliveSeconds;
			_writeCookie = false;
			_saveSession = false;
		}

		public void Save(int domainID) 
		{
			Save(domainID, "");
		}

		public void Save(int domainID, string domainName) 
		{
			Save(domainID, domainName, "");
		}

		public void Save(int domainID, string domainName, string ipAddress) 
		{
			bool keepAlive = GetKeepAlive();

			if (_saveSession || keepAlive) 
			{
				SaveSession(domainID, ipAddress);
			}

			if (_writeCookie || keepAlive) 
			{
				WriteCookie(domainName);
			}
		}

		private void SaveSession(int domainID, string ipAddress) 
		{
			// add a cookie for last update date, must be in same format as VB date format!
			// fortunately, DateTime.Now.ToString() produces the same format (7/21/2004 9:23:54 AM)
			Add(SESSION_LAST_UPDATE_KEY, DateTime.Now.ToString(), SessionItemType.Permanent, false);

			StringBuilder temporaryBuffer = new StringBuilder();
			StringBuilder temporaryPersistBuffer = new StringBuilder();

			foreach (SessionItem item in _itemCollection) 
			{
				switch (item.ItemType) 
				{
					case SessionItemType.Temporary:
						temporaryBuffer.Append(ITEM_TOKEN + item.Name + VALUE_TOKEN + item.Value);
						break;
					case SessionItemType.TemporaryPersist:
						temporaryPersistBuffer.Append(ITEM_TOKEN + item.Name + VALUE_TOKEN + item.Value);
						break;
				}
			}

			string sessionValue = temporaryBuffer.ToString() + PERSIST_TOKEN + temporaryPersistBuffer.ToString();

			try 
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnSession", DalKey));
				client.AddParameter("@SessionKey", SqlDbType.VarChar, ParameterDirection.Input, SessionKey, GUID_LENGTH);
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, MemberID);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
				client.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress, IP_ADDRESS);
				client.AddParameter("@Value", SqlDbType.NVarChar, ParameterDirection.Input, sessionValue, SESSION_VALUE);
				client.ExecuteNonQuery("up_Session_Save", CommandType.StoredProcedure);
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to save session.", "UserSession.Save", ex);
			}
		}

		private void WriteCookie(string domainName) 
		{
			try 
			{
				HttpContext current = HttpContext.Current;

				// clear the cookie
				current.Response.Cookies[_matchnetCookieName].Values.Clear();

				foreach (SessionItem item in _itemCollection) 
				{
					if (item.ItemType == SessionItemType.Permanent) 
					{
						current.Response.Cookies[_matchnetCookieName][item.Name] = HttpUtility.UrlEncode(item.Value);
					}
				}

				// expire next year
				current.Response.Cookies[_matchnetCookieName].Expires = DateTime.Now.AddYears(1);

				// set the domain of the cookie
				if (domainName != null && domainName != string.Empty) 
				{
					if (!domainName.StartsWith(".")) 
					{
						domainName = "." + domainName;
					}
					current.Response.Cookies[_matchnetCookieName].Domain = domainName;
				}
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to write cookie.", "UserSession.WriteCookie", ex);
			}
		}

		public SessionItem GetItem(string key) 
		{
			if (key == null || key == string.Empty) 
			{
				return new SessionItem();
			}

			InitializeSession();

			SessionItem item = _itemCollection[key.ToLower()];

			return item == null ? new SessionItem() : item;
		}

		public string this[string name] 
		{
			get 
			{
				return Value(name);
			}
		}

		public string Value(string key) 
		{
			return Value(key, "");
		}

		public string Value(string key, string defaultValue)
		{
			string val = GetItem(key).Value;

			if (val == null || val == string.Empty) 
			{
				return defaultValue;
			}
			return val;
		}

		public Int32 ValueInt(string key) 
		{
			return ValueInt(key, Matchnet.Constants.NULL_INT);			
		}

		public Int32 ValueInt(string key, int defaultValue) 
		{
			return Util.CInt(Value(key), defaultValue);
		}

		public double ValueDouble(string key) 
		{
			return ValueDouble(key, Matchnet.Lib.ConstantsTemp.NULL_DOUBLE);
		}

		public double ValueDouble(string key, double defaultValue) 
		{
			return Util.CDouble(Value(key), defaultValue);
		}

		public bool ValueBool(string key) 
		{
			return ValueBool(key, false);
		}

		public bool ValueBool(string key, bool defaultValue) 
		{
			return Util.CBool(Value(key), defaultValue);
		}
		
		public int Count 
		{
			get 
			{
				InitializeSession();
				return _itemCollection.Count;
			}
		}

		private void InitializeSession() 
		{
			try 
			{
				if (!_isInitialized) 
				{
					_isInitialized = true;

					HttpCookie cookie = HttpContext.Current.Request.Cookies[_matchnetCookieName];

					if (cookie != null) 
					{
						if (cookie.Values.Count > 0) 
						{
							foreach (string name in cookie.Values) 
							{
								Add(name, HttpUtility.UrlDecode(cookie.Values[name]), SessionItemType.Permanent, true);
							}
							Load();
						}
					}

					if (_itemCollection.Count == 0) 
					{
						StartSession();
					}
				}
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to initialize session.", "UserSession.InitializeSession", ex);
			}
		}

		public void Clear() 
		{
			try 
			{
				InitializeSession();

				// populate a new session collection with non-temporary items
				SessionItemCollection newCollection = new SessionItemCollection();

				foreach (SessionItem item in _itemCollection) 
				{
					if (item.ItemType != SessionItemType.Temporary) 
					{
						newCollection.Add(item);
					} 
					else 
					{
						// there was a temp item, we need to mark as dirty
						_saveSession = true;
					}
				}

				// flip the existing collection with the "cleared" one.
				_itemCollection = newCollection;

				_memberID = 0;
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to clear session.", "UserSession.Clear", ex);
			}
		}

		private void StartSession() 
		{
			// enforce upper on guid
			Add(SESSION_UID_KEY, System.Guid.NewGuid().ToString().ToUpper(), SessionItemType.Permanent, false);
			Add(SESSION_LAST_UPDATE_KEY, DateTime.Now.ToString(), SessionItemType.Permanent, false);
		}

		public void Load() 
		{
			try 
			{
				SQLClient client = new SQLClient(new SQLDescriptor("mnSession", DalKey));
				client.AddParameter("@SessionKey", SqlDbType.VarChar, ParameterDirection.Input, SessionKey, GUID_LENGTH);
				DataTable table = client.GetDataTable("up_Session_List", CommandType.StoredProcedure);

				string val = "";

				if (table != null && table.Rows.Count > 0) 
				{
					DataRow row = table.Rows[0];
					val = Convert.ToString(row["Value"]);
					_sessionID = Convert.ToInt32(row["SessionID"]);
					Add("HasSavedSearchFlag", Convert.ToString(row["HasSavedSearchFlag"]), SessionItemType.Temporary, true);
				}

				if (val != string.Empty) 
				{
					string[] values = Regex.Split(val,PERSIST_TOKEN.Replace("|",@"\174"));

					if (values.Length == 2) 
					{
						LoadItems(values[0], SessionItemType.Temporary);
						LoadItems(values[1], SessionItemType.TemporaryPersist);
					}
				}
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to load session.", "UserSession.Load", ex);
			}
		}

		private void LoadItems(string delimitedItems, SessionItemType itemType) 
		{
			// exception caught above
			if (delimitedItems != null && delimitedItems != string.Empty) 
			{
				string[] items = Regex.Split(delimitedItems,ITEM_TOKEN.Replace("|",@"\174"));

				for (int i = 0; i < items.Length; i++) 
				{
					
					string[] values = Regex.Split(items[i],VALUE_TOKEN.Replace("|",@"\174"));

					if (values.Length == 2) 
					{
						Add(values[0], values[1], itemType, true);
					}
				}
			}
		}

		public bool CookieExists() 
		{
			try 
			{
				return HttpContext.Current.Request.Cookies[_matchnetCookieName].Values.Count > 0;
			} 
			catch
			{
				return false;
			}
		}

		public void Add(string name, string val, SessionItemType itemType) 
		{
			Add(name, val, itemType, false);
		}

		public void Add(string name, string val, SessionItemType itemType, bool duringLoad) 
		{
			Add(name, val, itemType, duringLoad, SessionOperatorType.Equal);
		}

		public void Add(string name, string val, SessionItemType itemType, bool duringLoad, SessionOperatorType operatorType) 
		{
			try 
			{
				string key = name.ToLower();
				// returns an empty item if key doesn't exist
				SessionItem item = GetItem(key);

				if (item.Value != val || item.ItemType != itemType) 
				{
					if (item.Name == string.Empty) 
					{
						item.Name = name;
						item.Value = operatorType == SessionOperatorType.Equal ? val : Mask(Util.CInt(val, 0), Util.CInt(item.Value, 0), operatorType);
						item.ItemType = itemType;
						_itemCollection.Add(item);
					} 
					else 
					{
						item.Value = operatorType == SessionOperatorType.Equal ? val : Mask(Util.CInt(val, 0), Util.CInt(item.Value, 0), operatorType);
						item.ItemType = itemType;
					}
				}

				if (!duringLoad) 
				{
					// bug in com object prevents items from being temporary and permanent,
					// but I don't think we use them in that manner anyways
					if (itemType == SessionItemType.Permanent) 
					{
						_writeCookie = true;	
					} 
					else 
					{
						_saveSession = true;
					}
				}
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to add to session.", "UserSession.Add", ex);
			}
		}

		public int MemberID 
		{
			get 
			{
				if (_memberID == 0) 
				{
					_memberID = ValueInt("MemberID", 0);
				}
				return _memberID;
			}
		}
		
		public int DalKey 
		{
			get 
			{
				if (_dalKey == 0) 
				{
					// enforce to upper so we won't get a bum key
					_dalKey = Util.CRC32(SessionKey.ToUpper());
				}
				return _dalKey;
			}
		}

		public string SessionKey 
		{
			get 
			{
				if (_sessionKey == null || _sessionKey == string.Empty) 
				{
					_sessionKey = Value(SESSION_UID_KEY);
				}
				return _sessionKey;
			}
			set 
			{
				_sessionKey = value;
				Add(SESSION_UID_KEY, value, SessionItemType.Permanent, false);
				Load();
			}
		}

		// TODO: Remove when we can change HeaderYNM
		public string Guid 
		{
			get 
			{
				return SessionKey;
			}
		}

		// TODO: Remove when we can change HeaderYNM
		public bool IsLoggedIn 
		{
			get 
			{
				return ValueInt("MemberID", 0) != 0;
			}
		}

		public int SessionID 
		{
			get { return _sessionID; }
		}

		private bool GetKeepAlive() 
		{
			if (_keepAliveDate == DateTime.MinValue) 
			{
				try 
				{
					_keepAliveDate = Convert.ToDateTime(_itemCollection[SESSION_LAST_UPDATE_KEY].Value);
				} 
				catch 
				{
					_keepAliveDate = DateTime.MinValue;
				}
			}

			// if the date was invalid, assume dirty
			if (_keepAliveDate == DateTime.MinValue) return true;

			TimeSpan span = DateTime.Now - _keepAliveDate;
			return span.TotalSeconds >= _keepAliveFrequencyInSeconds;
		}

		public string CookieName 
		{
			get 
			{
				return _matchnetCookieName;
			}
		}

		private string Mask(int originalValue, int maskValue, SessionOperatorType operatorType) 
		{
			// we just support and, or, equal
			int maskedVal = maskValue;

			switch (operatorType) 
			{
				case SessionOperatorType.BitwiseOr:
					maskedVal = originalValue & maskValue;
					break;
				case SessionOperatorType.BitwiseAnd:
					maskedVal = originalValue | maskValue;
					break;
			}

			if (maskedVal < 0) 
			{
				return "0";
			}
			return Convert.ToString(maskedVal);
		}

		public SessionItemCollection Items 
		{
			get { return _itemCollection; }
		}

		public static void SessionKeepAlive(string sessionKey)
		{
			try 
			{
				// enforce upper on sessionKey
				int key = Util.CRC32(sessionKey.ToUpper());
			
				SQLDescriptor descriptor = new SQLDescriptor("mnSession", key);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter("@SessionKey", SqlDbType.VarChar, ParameterDirection.Input, sessionKey);
				client.ExecuteNonQuery("up_Session_ListIMKeepAlive", CommandType.StoredProcedure);
			} 
			catch
			{
				// eat the exception, if we are having probs the member will be kicked off
				// regardless. TODO: should we at least log this error?
			}
		}
	}
}
