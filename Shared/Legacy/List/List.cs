using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Data;
using Matchnet.Lib.Exceptions;

namespace Matchnet.App.List
{
	/// <summary>
	/// A base class for a collection of List items.
	/// </summary>
	public abstract class List
	{
		/// <summary>
		/// Members that have been viewed by a Member
		/// </summary>
		public const int LIST_TYPE_VIEW = 2;
		/// <summary>
		/// Members that have been listed as favorites by a Member
		/// </summary>
		public const int LIST_TYPE_FAVORITE = 4;
		/// <summary>
		/// Members that have been emailed by a Member
		/// </summary>
		public const int LIST_TYPE_EMAIL = 8;
		/// <summary>
		/// Members that have been Instant Messaged by a Member
		/// </summary>
		public const int LIST_TYPE_IM = 16;
		/// <summary>
		/// Members that have been Teased by a Member
		/// </summary>
		public const int LIST_TYPE_TEASE = 32;
		/// <summary>
		/// Members that have been sent as Matches to a Member
		/// </summary>
		public const int LIST_TYPE_MATCHES = 64;
		/// <summary>
		/// Members that are being Ignored by a Member
		/// </summary>
		public const int LIST_TYPE_IGNORE = 128;
		/// <summary>
		/// Members that a Member has said Yes to
		/// </summary>
		public const int LIST_TYPE_YES = 256;
		/// <summary>
		/// Members that a Member has said No to
		/// </summary>
		public const int LIST_TYPE_NO = 512;
		/// <summary>
		/// Members that a Member has said Maybe to
		/// </summary>
		public const int LIST_TYPE_MAYBE = 1024;

		private ArrayList mListItems;
		private Hashtable mListProcMapping;

		private void InitializeList() 
		{
			if (mListItems == null) 
			{
				mListItems = new ArrayList();
			}
		}

		private void InitializeMapping() 
		{
			if (mListProcMapping == null) 
			{
				mListProcMapping = new Hashtable();

				mListProcMapping.Add(LIST_TYPE_VIEW, "up_ViewList_ListGrid");
				mListProcMapping.Add(LIST_TYPE_FAVORITE, "up_FavoriteList_ListGrid");
				mListProcMapping.Add(LIST_TYPE_EMAIL, "up_EmailList_ListGrid");
				mListProcMapping.Add(LIST_TYPE_IM, "up_InstantMessageList_ListGrid");
				mListProcMapping.Add(LIST_TYPE_TEASE, "up_TeaseList_ListGrid");
				mListProcMapping.Add(LIST_TYPE_IGNORE, "up_BlockList_ListGrid");
				mListProcMapping.Add(LIST_TYPE_YES, "up_YesList_ListGrid");
				mListProcMapping.Add(LIST_TYPE_NO, "up_NoList_ListGrid");
				mListProcMapping.Add(LIST_TYPE_MAYBE, "up_MaybeList_ListGrid");
			}
		}

		/// <summary>
		/// Adds a List Item to the collection of
		/// List Items.
		/// </summary>
		/// <param name="listItem">The List Item to add to the collection of List Items.</param>
		protected void Add(ListItem listItem) 
		{
			InitializeList();
			mListItems.Add(listItem);
		}

		/// <summary>
		/// Clears the collection of List Items.
		/// </summary>
		protected void Clear() 
		{
			InitializeList();
			mListItems.Clear();
		}

		/// <summary>
		/// Retrieves a List Item by index in the collection of List Items.
		/// </summary>
		/// <param name="ndx">The index of the item to retrieve</param>
		/// <returns>A List Item</returns>
		public ListItem Item(int ndx) 
		{
			InitializeList();
			if (ndx > mListItems.Count - 1 || ndx < 0) 
			{
				return null; // caller will need to handle this.
			}
			return (ListItem)mListItems[ndx];
		}

		/// <summary>
		/// The Count of List Items in the collection of List Items.
		/// </summary>
		public int Count 
		{
			get 
			{
				InitializeList();
				return mListItems.Count;
			}
		}

		/// <summary>
		/// Populates the collection of List Items. Sub-classes must provide
		/// an implementation for this method.
		/// </summary>
		/// <param name="memberID">The ID of the Member who owns the list</param>
		/// <param name="domainID">The ID of the Domain in which the list exists</param>
		/// <param name="direction">The direction of the list to retrieve</param>
		public abstract void Populate(int memberID, int domainID, short direction);

		/// <summary>
		/// Loads persisted List Items from the database.
		/// </summary>
		/// <param name="listTypeID">The ID of the List Type to Populate</param>
		/// <param name="memberID">The ID of the Member who owns the list</param>
		/// <param name="domainID">The ID of the Domain in which the list exists</param>
		/// <param name="direction">The direction of the list to retrieve</param>
		/// <returns>A populated DataTable that contains the List Items.</returns>
		protected DataTable PopulateGrid(int listTypeID, int memberID, int domainID, short direction) 
		{
			try 
			{
				return PopulateGrid(listTypeID, memberID, domainID, direction, 0);
			} 
			catch (MatchnetException mex) 
			{
				throw mex;
			}
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to populate list", "List.Populate", ex);
			}
		}

		/// <summary>
		/// Loads persisted List Items from the database.
		/// </summary>
		/// <param name="listTypeID">The ID of the List Type to Populate</param>
		/// <param name="memberID">The ID of the Member who owns the list</param>
		/// <param name="domainID">The ID of the Domain in which the list exists</param>
		/// <param name="direction">The direction of the list to retrieve</param>
		/// <param name="categoryID">The ID of the category to retrieve</param>
		/// <returns>A populated DataTable that contains the List Items.</returns>
		protected DataTable PopulateGrid(int listTypeID, int memberID, int domainID, short direction, int categoryID) 
		{
			try 
			{
				InitializeMapping();

				string listProc = System.Convert.ToString(mListProcMapping[listTypeID]);

				SQLClient client = new SQLClient(new SQLDescriptor("mnList", memberID));
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
				client.AddParameter("@Direction", SqlDbType.Bit, ParameterDirection.Input, System.Convert.ToBoolean(direction));
				if (categoryID != 0) 
				{
					client.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, categoryID);
				}
				return client.GetDataTable(listProc, CommandType.StoredProcedure);
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to populate list", "List.Populate", ex);
			}
		}

		/// <summary>
		/// Populates the collection of  List Items from a DataTable.
		/// </summary>
		/// <param name="table">A populated DataTable</param>
		/// <param name="listTypeID">The type of ListItem to populate the collection with.</param>
		public void FromDataTable(DataTable table, int listTypeID) 
		{
			try 
			{
				if (table != null && table.Rows.Count > 0) 
				{
					foreach (DataRow row in table.Rows) 
					{
						ListItem item;

						switch (listTypeID) 
						{
							case LIST_TYPE_TEASE:
								item = new TeaseListItem();
								break;
							case LIST_TYPE_YES:
								item = new YesListItem();
								break;
							case LIST_TYPE_FAVORITE:
								item = new FavoriteListItem();
								break;
							default:
								item = new ListItem();
								break;
						}
						item.FromDataRow(row);
						Add(item);
					}
				}
			} 
			catch (MatchnetException mex) 
			{
				throw mex;
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to populate list", "List.FromDataTable", ex);
			}
		}
	}
}
