using System;
using System.Data;

using Matchnet.Lib.Exceptions;

namespace Matchnet.App.List
{
	/// <summary>
	/// Represents a Yes vote on a Member.
	/// </summary>
	public class YesListItem : ListItem
	{
		private bool mMailFlag;

		/// <summary>
		/// Whether or not there was a YesMail sent for this
		/// list item.
		/// </summary>
		public bool MailFlag 
		{
			get 
			{
				return mMailFlag;
			}
			set 
			{
				mMailFlag = value;
			}
		}

		/// <summary>
		/// Populates based on data in a DataRow.
		/// </summary>
		/// <param name="row">A DataRow that contains the list item data.</param>
		public override void FromDataRow(DataRow row) 
		{
			try 
			{
				base.FromDataRow(row);
				mMailFlag = System.Convert.ToBoolean(row["MailFlag"]);
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to populate list item", "YesListItem.FromDataRow", ex);
			}
			
		}
	}
}
