using System;
using System.Data;

using Matchnet.Lib.Exceptions;

namespace Matchnet.App.List
{
	/// <summary>
	/// Represents a tease.
	/// </summary>
	public class TeaseListItem : ListItem
	{
		private int mTeaseID;

		/// <summary>
		/// The ID of the tease that was sent
		/// </summary>
		public int TeaseID 
		{
			get 
			{	
				return mTeaseID;
			}
			set 
			{
				mTeaseID = value;
			}
		}

		/// <summary>
		/// Populates based on data in a DataRow.
		/// </summary>
		/// <param name="row">A DataRow that contains the list item data.</param>
		public override void FromDataRow(DataRow row) 
		{
			try 
			{
				base.FromDataRow(row);
				mTeaseID = System.Convert.ToInt32(row["TeaseID"]);
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to populate list item", "YesListItem.FromDataRow", ex);
			}
			
		}
	}
}
