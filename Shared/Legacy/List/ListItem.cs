using System;
using System.Data;

using Matchnet.Lib.Exceptions;

namespace Matchnet.App.List
{
	/// <summary>
	/// A base class for all lists. Contains common properties
	/// to all lists
	/// </summary>
	public class ListItem
	{
		private int mListID;
		private int mMemberID;
		private int mDomainID;
		private short mDirection;
		private int mTargetMemberID;
		private DateTime mInsertDate;

		/// <summary>
		/// The Unique ID for the list item
		/// </summary>
		public int ListID 
		{
			get 
			{
				return mListID;
			}
			set 
			{
				mListID = value;
			}
		}
		
		/// <summary>
		/// The ID of the Member that correlates to the
		/// direction of the list item
		/// </summary>
		public int MemberID 
		{
			get 
			{
				return mMemberID;
			}
			set 
			{
				mMemberID = value;
			}
		}

		/// <summary>
		/// The ID of the domain the list item was recorded on
		/// </summary>
		public int DomainID 
		{
			get 
			{
				return mDomainID;
			}
			set 
			{
				mDomainID = value;
			}
		}

		/// <summary>
		/// The direction of the the list items action. Corresponds
		/// to the TargetMemberID and the MemberID.
		/// <p>
		/// <ul>
		///		<li>0 - MemberID listed TargetMemberID</li>
		///		<li>1 - TargetMemberID listed MemberID</li>
		/// </ul>
		/// </p>
		/// </summary>
		public short Direction 
		{
			get 
			{	
				return mDirection;
			}
			set 
			{
				mDirection = value;
			}
		}

		/// <summary>
		/// The ID of the Member that correlates to the
		/// direction of the list item
		/// </summary>
		public int TargetMemberID 
		{
			get 
			{
				return mTargetMemberID;
			}
			set 
			{
				mTargetMemberID = value;
			}
		}

		/// <summary>
		/// The date and time the list item was recorded
		/// </summary>
		public DateTime InsertDate 
		{
			get 
			{
				return mInsertDate;
			}
			set 
			{
				mInsertDate = value;
			}
		}

		/// <summary>
		/// Populates based on data in a DataRow.
		/// </summary>
		/// <param name="row">A DataRow that contains the list item data.</param>
		public virtual void FromDataRow(DataRow row) 
		{
			try 
			{
				mListID = System.Convert.ToInt32(row["ListID"]);
				mMemberID = System.Convert.ToInt32(row["MemberID"]);
				mDomainID = System.Convert.ToInt32(row["DomainID"]);
				mDirection = System.Convert.ToInt16(row["Direction"]);
				mTargetMemberID = System.Convert.ToInt32(row["TargetMemberID"]);
				mInsertDate = System.Convert.ToDateTime(row["InsertDate"]);
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to populate list item", "ListItem.FromDataRow", ex);
			}
		}
	}
}
