using System;

namespace Matchnet.App.List
{
	/// <summary>
	/// A custom hot list category for a Member.
	/// </summary>
	public class Category
	{
		private int mCategoryID;
		private int mMemberID;
		private int mDomainID;
		private string mDescription;

		/// <summary>
		/// The unique identifier for the custom hot
		/// list category.
		/// </summary>
		public int CategoryID 
		{
			get 
			{
				return mCategoryID;
			}
			set 
			{
				mCategoryID = value;
			}
		}

		/// <summary>
		/// The ID of the Member that owns the 
		/// custom hot list category.
		/// </summary>
		public int MemberID 
		{
			get 
			{
				return mMemberID;
			}
			set 
			{
				mMemberID = value;
			}
		}

		/// <summary>
		/// The ID of the Domain the custom
		/// hot list category exists in.
		/// </summary>
		public int DomainID 
		{
			get 
			{
				return mDomainID;
			}
			set 
			{
				mDomainID = value;
			}
		}

		/// <summary>
		/// The name of the category.
		/// </summary>
		public string Description 
		{
			get 
			{
				return mDescription;
			}
			set 
			{
				mDescription = value;
			}
		}
	}
}
