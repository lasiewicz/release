using System;
using System.Data;

using Matchnet.Lib.Exceptions;

namespace Matchnet.App.List
{
	/// <summary>
	/// Represents a Member in a custom list.
	/// </summary>
	public class FavoriteListItem : ListItem
	{
		private int mCategoryID;
		private string mComments;

		/// <summary>
		/// The ID of the custom category the
		/// Member exists in.
		/// </summary>
		public int CategoryID 
		{
			get 
			{
				return mCategoryID;
			}
			set 
			{
				mCategoryID = value;
			}
		}

		/// <summary>
		/// Member specified comments for the List Item.
		/// </summary>
		public string Comments 
		{
			get 
			{
				return mComments;
			}
			set 
			{
				mComments = value;
			}
		}

		/// <summary>
		/// Populates based on data in a DataRow.
		/// </summary>
		/// <param name="row">A DataRow that contains the list item data.</param>
		public override void FromDataRow(DataRow row) 
		{
			try 
			{
				base.FromDataRow(row);
				mCategoryID = System.Convert.ToInt32(row["CategoryID"]);
				mComments = System.Convert.ToString(row["Comments"]);
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to populate list item", "YesListItem.FromDataRow", ex);
			}
		}
	}
}
