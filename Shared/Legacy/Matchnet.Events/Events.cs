using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.CachingTemp;
using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Util;

namespace Matchnet.Events
{
	public class EventsBL
	{
		private static Matchnet.CachingTemp.Cache oCache = Matchnet.CachingTemp.Cache.GetInstance();
		private const int PAGE_SIZE = 20;

		#region Constructors
			public EventsBL()
			{

			}
		#endregion

		#region Public Methods
			public EventCollection GetEvents(int DomainId, int EventTypeID)
			{
				EventCollection oEvents = new EventCollection();
					
				try
				{
					string key = "mnCommon:GetEvents:" + DomainId.ToString() + ":" + EventTypeID.ToString();
					DataTable dbTable = new DataTable();
					
					object cacheItem = oCache.Get(key);
					if (cacheItem != null) 
					{
						dbTable = (DataTable) cacheItem;
					}
					else
					{
						SQLDescriptor descriptor = new SQLDescriptor("mnCommon");
						SQLClient client = new SQLClient(descriptor);

						client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, DomainId);
						client.AddParameter("@OrderBy", SqlDbType.VarChar, ParameterDirection.Input, "EventDate", 100);
						client.AddParameter("@Sorting", SqlDbType.VarChar, ParameterDirection.Input, "asc", 8);
						client.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, 1);
						client.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, PAGE_SIZE);
						client.AddParameter("@ExtraSQL", SqlDbType.VarChar, ParameterDirection.Input, "PublishedFlag=1", 400);
						client.AddParameter("@EventTypeID", SqlDbType.Int, ParameterDirection.Input, EventTypeID);
						client.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output);

						dbTable = client.GetDataTable("up_Event_List", CommandType.StoredProcedure);
						if (dbTable.Rows.Count > 0) 
						{
						//	oCache.Insert(key, dbTable);
						}
						else
						{
							throw new Exception("Events do not exist.");
						}
					}

					oEvents = convertToEventCollection(dbTable);
				}
				catch(Exception ex)
				{
					throw new Exception(ex.Message);
				}	

				return oEvents;
			}


			public EventCollection GetEventPhotos(int DomainId)
			{
				EventCollection oEvents = new EventCollection();
						
				try
				{
					string key = "mnCommon:GetEventPhotos:" + DomainId.ToString();
					DataTable dbTable = new DataTable();
						
					object cacheItem = oCache.Get(key);
					if (cacheItem != null) 
					{
						dbTable = (DataTable) cacheItem;
					}
					else
					{
						SQLDescriptor descriptor = new SQLDescriptor("mnCommon");
						SQLClient client = new SQLClient(descriptor);

						client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, DomainId);
						client.AddParameter("@OrderBy", SqlDbType.VarChar, ParameterDirection.Input, "EventDate", 100);
						client.AddParameter("@Sorting", SqlDbType.VarChar, ParameterDirection.Input, "desc", 8);
						client.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, 1);
						client.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, PAGE_SIZE);
						client.AddParameter("@ExtraSQL", SqlDbType.VarChar, ParameterDirection.Input, "AlbumPublishedFlag=1", 400);
						client.AddParameter("@EventTypeID", SqlDbType.Int, ParameterDirection.Input, null);
						client.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output);

						dbTable = client.GetDataTable("up_Event_List", CommandType.StoredProcedure);
						if (dbTable.Rows.Count > 0) 
						{
							oCache.Insert(key, dbTable);
						}
						else
						{
							throw new Exception("Events do not exist.");
						}
					}

					oEvents = convertToEventCollection(dbTable);
				}
				catch(Exception ex)
				{
					throw new Exception(ex.Message);
				}	

				return oEvents;
			}

			
			public Event GetEvent(int EventId)
			{
				Event oEvent = new Event();

				try
				{
					string key = "mnCommon:GetEvent:" + EventId.ToString();
					DataTable dbTable = new DataTable();
					
					object cacheItem = oCache.Get(key);
					if (cacheItem != null) 
					{
						dbTable = (DataTable) cacheItem;
					}
					else
					{
						SQLDescriptor descriptor = new SQLDescriptor("mnCommon");
						SQLClient client = new SQLClient(descriptor);

						client.AddParameter("@EventID", SqlDbType.Int, ParameterDirection.Input, EventId);

						dbTable = client.GetDataTable("up_Event_Select", CommandType.StoredProcedure);
						if (dbTable.Rows.Count > 0) 
						{
							oCache.Insert(key, dbTable);
						}
						else
						{
							throw new Exception("Event does not exist.");
						}
					}
	
					oEvent = convertToEventObject(dbTable.Rows[0], true);

				}
				catch(Exception ex)	
				{
					throw new Exception(ex.Message); 
				}	

				return oEvent;
				
			}
		#endregion

		#region Private Methods
			private EventCollection convertToEventCollection(System.Data.DataTable dt)
			{
				EventCollection oEvents = new EventCollection();
					
				foreach(DataRow DataRow in dt.Rows)
				{
					// Do not include Event Content field per Proc.
					oEvents.Add(convertToEventObject(DataRow, false));
				}

				return oEvents;
			}

			private Event convertToEventObject(System.Data.DataRow dr, bool withContent)
			{
				Event oEvent = new Event(Util.CInt(dr["EventID"]), Util.CInt(dr["EventTypeId"]));
				oEvent.EventDate = Util.CDateTime(dr["EventDate"]);
				oEvent.EventDateText = Util.CString(dr["EventDateText"]);
				oEvent.Description = Util.CString(dr["Description"]);
				oEvent.DomainId = Util.CInt(dr["DomainId"]);
				oEvent.EventExtraInfo = Util.CString(dr["EventExtraInfo"]);
				oEvent.EventThumbPath = Util.CString(dr["EventThumbPath"]);
				oEvent.InsertedDate = Util.CDateTime(dr["InsertedDate"]);
				oEvent.ListOrderEvent = Util.CInt(dr["ListOrderEvent"]);
				oEvent.Location = Util.CString(dr["Location"]);
				oEvent.PublishedFlag = Util.CBool(dr["PublishedFlag"],false);
				oEvent.AlbumExtraInfo = Util.CString(dr["AlbumExtraInfo"]);
				oEvent.AlbumPublishedFlag = Util.CBool(dr["AlbumPublishedFlag"], false);
				oEvent.AlbumThumbPath = Util.CString(dr["AlbumThumbPath"]);
				oEvent.ListOrderAlbum = Util.CInt(dr["ListOrderAlbum"]);

				//Only populate Content if content exists
				if (withContent == true)
				{
					oEvent.Content = Util.CString(dr["Content"]);
					oEvent.AlbumContent = Util.CString(dr["AlbumContent"]);
				}

				return oEvent;
			}

		#endregion
	}
}
