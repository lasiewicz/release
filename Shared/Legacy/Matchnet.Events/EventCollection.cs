using System;
using System.Collections;

namespace Matchnet.Events
{
	/// <summary>
	/// Summary description for EventCollection.
	/// </summary>
	public class EventCollection : System.Collections.CollectionBase
	{
		#region Constructors
			public EventCollection()
			{

			}
		#endregion

		#region Public Methods
			public Event this[int index]
			{
				get{return (Event)base.InnerList[index];}
						
			}

			public int Add(Event oEvent)
			{
				return base.InnerList.Add(oEvent);
			}

			public Event FindByID(int eventID)
			{
				foreach(Event oEvent in this)
				{
					if(oEvent.EventId == eventID)
					{
						return oEvent;
					}
				}

				// Returns null if the specified Event cannot be found in the collection
				return null;
			}
		#endregion

	}
}
