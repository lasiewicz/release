using System;
using System.Collections.Specialized;

using Matchnet.Lib;

namespace Matchnet.Events
{
	/// <summary>
	/// Summary description for Event.
	/// </summary>
	public class Event
	{
		#region Private Members
			private int _eventId = Matchnet.Constants.NULL_INT;
			private int _eventTypeId = Matchnet.Constants.NULL_INT;
			private string _description = Matchnet.Constants.NULL_STRING;
			private string _location = Matchnet.Constants.NULL_STRING;
			private string _eventDateText = Matchnet.Constants.NULL_STRING;
			private System.DateTime _eventDate = System.DateTime.MinValue;
			private string _eventThumbPath = Matchnet.Constants.NULL_STRING;
			private int _domainId = Matchnet.Constants.NULL_INT;
			private int _listOrderEvent = Matchnet.Constants.NULL_INT;
			private bool _publishedFlag;
			private System.DateTime _insertedDate = System.DateTime.MinValue;
			private string _eventExtraInfo = Matchnet.Constants.NULL_STRING;
			private string _content = Matchnet.Constants.NULL_STRING;
			private int _listOrderAlbum= Matchnet.Constants.NULL_INT;
			private bool _albumPublishedFlag;
			private string _albumThumbPath = Matchnet.Constants.NULL_STRING;
			private string _albumExtraInfo = Matchnet.Constants.NULL_STRING;
			private string _albumContent = Matchnet.Constants.NULL_STRING;

		#endregion

		#region Constructors
			public Event()
			{
			
			}

			public Event(int eventId)
			{
				_eventId = eventId;
			}

			public Event(int eventId, int eventTypeId)
			{
				_eventId = eventId;
				_eventTypeId = eventTypeId;
			}
		#endregion

		#region Properties
			public int EventId 
			{
				get{return _eventId;}
				set{_eventId = value;}
			}

			public int EventTypeId
			{
				get{return _eventTypeId;}
				set{_eventTypeId = value;}
			}

			public string Description
			{
				get{return _description;}
				set{_description = value;}
			}

			public string Location
			{
				get{return _location;}
				set{_location = value;}
			}

			public string EventDateText
			{
				get{return _eventDateText;}
				set{_eventDateText = value;}
			}

			public System.DateTime EventDate
			{
				get{return _eventDate;}
				set{_eventDate = value;}
			}

			public string EventThumbPath
			{
				get{return _eventThumbPath;}
				set{_eventThumbPath = value;}
			}

			public int DomainId
			{
				get{return _domainId;}
				set{_domainId = value;}
			}

			public int ListOrderEvent
			{
				get{return _listOrderEvent;}
				set{_listOrderEvent = value;}
			}

			public bool PublishedFlag
			{
				get{return _publishedFlag;}
				set{_publishedFlag = value;}
			}

			public System.DateTime InsertedDate
			{
				get{return _insertedDate;}
				set{_insertedDate = value;}
			}

			public string EventExtraInfo
			{
				get{return _eventExtraInfo;}
				set{_eventExtraInfo = value;}
			}

			public string Content
			{
				get{return _content;}
				set{_content = value;}
			}

			public int ListOrderAlbum
			{
				get{return _listOrderAlbum;}
				set{_listOrderAlbum = value;}
			}

			public bool AlbumPublishedFlag
			{
				get{return _albumPublishedFlag;}
				set{_albumPublishedFlag = value;}
			}

			public string AlbumThumbPath
			{
				get{return _albumThumbPath;}
				set{_albumThumbPath = value;}
			}

			public string AlbumExtraInfo
			{
				get{return _albumExtraInfo;}
				set{_albumExtraInfo = value;}
			}
		
			public string AlbumContent
			{
				get{return _albumContent;}
				set{_albumContent = value;}
			}

		#endregion
	}
}
