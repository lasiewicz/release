using System;
using System.Threading;

using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Threading
{
	/// <summary>
	/// A Thread is an object that is run concurrently with other
	/// threads in an application. The Matchnet.Threading.Thread wraps
	/// the funcionality of System.Threading.Thread by providing a 
	/// strict means to create and control concurrent execution.
	/// </summary>
	public class Thread : Runnable
	{
		/// <summary>
		/// The runnable instance used by this thread.
		/// </summary>
		protected Runnable m_runnable;
		internal System.Threading.Thread m_thread;
		private string m_name;
		private ThreadPriority m_priority;

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public Thread() {
			m_runnable = this;
			m_name = null;
			m_priority = ThreadPriority.Normal;
		}

		/// <summary>
		///  Constructs a new, named, instance.
		/// </summary>
		/// <param name="name">The name of the Thread</param>
		public Thread(string name) {
			m_runnable = this;
			m_name = name;
			m_priority = ThreadPriority.Normal;
		}
		
		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="objRunnable">A concrete Matchnet.Threading.Runnable implementation</param>
		public Thread(Runnable objRunnable) {
			m_runnable = objRunnable;
			m_name = null;
			m_priority = ThreadPriority.Normal;
		}

		/// <summary>
		/// Constructs a new, named, instance.
		/// </summary>
		/// <param name="objRunnable">A concrete Matchnet.Threading.Runnable implementation</param>
		/// <param name="name">The name of the Thread</param>
		public Thread(Runnable objRunnable, string name) {
			m_runnable = objRunnable;
			m_name = name;
			m_priority = ThreadPriority.Normal;
		}

		/// <summary>
		/// Starts the concurrent execution of the Thread.
		/// </summary>
		public virtual void Start() {
			try {
				m_thread = new System.Threading.Thread(new ThreadStart(this.Run));

				if (m_name != null) {
					m_thread.Name = m_name;
				}

				m_thread.Priority = m_priority;
				m_thread.Start();
			} 
			catch (Exception e) {
				MatchnetException exception = new MatchnetException("Unable to start thread", "Thread.Start", e);
				//ExceptionLogger.LogToEventLog(exception, "Matchnet.Thread.Start", System.Diagnostics.EventLogEntryType.Warning);
				throw exception;
			}
		}

		/// <summary>
		/// Resumes exception of the Thread.
		/// </summary>
		public void Resume() {
			if (m_thread != null) {
				try {
					if (m_thread.IsAlive) {
						m_thread.Resume();
					}
				} 
				catch (Exception e) {	
					MatchnetException exception = new MatchnetException("Unable to resume execution of the thread", "Thread.Resume", e);
					throw exception;
				}
			} 
		}

		/// <summary>
		/// Suspends execution of the Thread
		/// </summary>
		public void Suspend() {
			if (m_thread != null) {
				try {
					if (m_thread.IsAlive) {
						m_thread.Suspend();
					}
				} 
				catch (Exception e) {
					MatchnetException exception = new MatchnetException("Unable to suspend execution of the thread", "Thread.Suspend", e);
					throw exception;
				}
			}
		}

		/// <summary>
		/// Aborts execution of the Thread
		/// </summary>
		public void Abort() {
			if (m_thread != null) {
				try {
					// the thread cannot be aborted when in the suspended state
					if ((m_thread.ThreadState & ThreadState.Suspended) != 0) {
						m_thread.Resume();
					}
					m_thread.Abort();
				} 
				catch (Exception e) {
					MatchnetException exception = new MatchnetException("Unable to abort execution of the thread", "Thread.Abort", e);
					throw exception;
				}
			}
		}

		/// <summary>
		/// Interrupts the execution of the Thread
		/// </summary>
		public void Interrupt() {
			try {
				if (m_thread != null && m_thread.IsAlive) {
					m_thread.Interrupt();
				}
			} catch (Exception e) {
				MatchnetException exception = new MatchnetException("Unable to interrupt execution of the thread", "Thread.Interrupt", e);
				throw exception;
			}
		}

		/// <summary>
		/// Whether or not the Thread is alive
		/// </summary>
		public bool IsAlive {
			get {
				if (m_thread != null) {
					return m_thread.IsAlive;
				}
				else {
					return false;
				}
			}
		}

		/// <summary>
		/// The current state of the Thread
		/// </summary>
		public ThreadState ThreadState {
			get {
				if (m_thread != null) {
					return m_thread.ThreadState;
				} 
				else {
					return ThreadState.Unstarted;
				}
			}
		}

		/// <summary>
		/// The priority the Thread is running at. This cannot be changed after the
		/// thread has been started.
		/// </summary>
		public ThreadPriority ThreadPriority {
			get {
				return m_priority;
			}

			set {	
				m_priority = value;
				if (m_thread != null && m_thread.ThreadState != ThreadState.Running) {
					m_thread.Priority = value;
				}
			}
		}

		/// <summary>
		/// Provides implementation of the Runnable interface. Classes that extend
		/// this object can override this method to provide logic that should be
		/// executed concurrently.
		/// </summary>
		public virtual void Run() {
			if (m_runnable != null) {
				m_runnable.Run();
			}
		}
	}
}
