using System;

namespace Matchnet.Lib.Threading
{
	/// <summary>
	/// Defines objects that have a Run method. Objects that have a Run method
	/// can be used for creating Threads.
	/// </summary>
	public interface Runnable {
		/// <summary>
		/// A method that can be used in a Thread
		/// </summary>
		void Run();
	}
}
