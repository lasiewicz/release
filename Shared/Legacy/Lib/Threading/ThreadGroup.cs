using System;
using System.Collections;

using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Threading
{
	/// <summary>
	/// A ThreadGroup acts similar to a System.Threading.ThreadPool in that
	/// Threads are queued for execution and there are, at one time, a defined
	/// maximum amount of Threads that are being executed.
	/// 
	/// There are some distinct differences between the ThreadGroup and the 
	/// System.Threading.ThreadPool. 
	/// 
	/// Threads executed in the ThreadGroup do not have the same system awareness 
	/// that the ThreadPool does. See the System.Threading.ThreadPool documentation
	/// for more information.
	/// 
	/// The ThreadGroup maintains the ability to have lifecycle control over the
	/// threads that are executing. This allows you to prematurely abort threads
	/// after they have been queued.
	/// 
	/// The ThreadGroup allows you to stop execution of all running Threads and
	/// prevent execution of queued Threads with a call to ThreadGroup.Stop.
	/// </summary>
	public class ThreadGroup
	{
		private ArrayList m_queue;
		private ArrayList m_list;
		private int m_max;
		private int m_queueRefresh; 
		private Thread m_queueWatcher;
		private bool m_exactMode;

		/// <summary>
		/// Provides a count of Threads that are currently queued and 
		/// waiting to run.
		/// </summary>
		/// <returns>The count of currently queued Threads.</returns>
		public int DebugQueueCount() {
			lock (m_queue.SyncRoot) {
				return m_queue.Count;
			}
		}

		/// <summary>
		/// Provides a count of currently running Threads.
		/// </summary>
		/// <returns>The count of currently running Threads.</returns>
		public int DebugListCount() {
			lock (m_list.SyncRoot) {
				return m_list.Count;
			}
		}

		/// <summary>
		/// Constructs a new instance with a default queue refresh time
		/// of 1/2 second.
		/// </summary>
		/// <param name="maxThreads">The maximum amount of simultaneously running Threads to allow</param>
		public ThreadGroup(int maxThreads) {
			Init(maxThreads, 5, false);
		}

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		/// <param name="maxThreads">The maximum amount of simultaneously running Threads to allow</param>
		/// <param name="queueRefresh">The interval at which to move Threads from the queue into the list of running Threads.</param>
		public ThreadGroup(int maxThreads, int queueRefresh) {
			Init(maxThreads, queueRefresh, false);
		}

		/// <summary>
		/// Constructs a new instance allowing you to specify wether or
		/// not the given MaxThreads is a fixed size for the group of
		/// threads that will be managed.
		/// </summary>
		/// <param name="maxThreads">The maximum amount of threads to run</param>
		/// <param name="exact">Whether or not the maximum is the exact size of threads to be managed.</param>
		public ThreadGroup(int maxThreads, bool exact) 
		{
			Init(maxThreads, 5, exact);
		}

		private void Init(int maxThreads, int queueRefresh, bool exact) {
			m_max = maxThreads;
			m_exactMode = exact;
			m_queueRefresh = queueRefresh;
			m_list = new ArrayList();
			m_queue = new ArrayList();
		}

		/// <summary>
		/// Adds a concrete Matchnet.Threading.Runnable implementation to the queue of
		/// Threads to start.
		/// </summary>
		/// <param name="runnable">The Matchnet.Threading.Runnable implementation to queue</param>
		public void AddRunnable(Runnable runnable) {
			Thread thread = new Thread(runnable);
			AddThread(thread);
		}
		
		/// <summary>
		/// Adds a Thread to the queue of Threads to start.
		/// </summary>
		/// <param name="thread">The Matchnet.Threading.Thread to queue</param>
		public void AddThread(Thread thread) {
			if (!m_exactMode) 
			{
				lock (m_queue.SyncRoot) 
				{
					m_queue.Add(thread);
				}
				if (m_queueWatcher == null) 
				{
					m_queueWatcher = new Thread(new QueueWatcher(this, m_queueRefresh));
					m_queueWatcher.Start();
				}
			} 
			else 
			{
				thread.Start();
				m_list.Add(thread);
			}
		}

		// internal so that the queue watcher can call it
		internal void MoveFromQueue() {
			if (Count() < m_max) {
				if (m_queue.Count > 0) {
					Thread tmp = (Thread)m_queue[0];
					tmp.Start();
					lock (m_list.SyncRoot) {
						m_list.Add(tmp);
					}

					lock (m_queue.SyncRoot) {
						m_queue.Remove(tmp);
					}
				}
			}
		}

		/// <summary>
		/// Retrieves the count of Threads currently running. If a previously
		/// run Thread is no longer running, it is moved from the list of 
		/// running Threads to make room for a queued Thread.
		/// </summary>
		/// <returns>The amount of currently running Threads</returns>
		public int Count() {
			int count = 0;
			try {
				lock (m_list.SyncRoot) {
					for (int x = 0; x < m_list.Count; x++) {
						Thread thread = (Thread)m_list[x];
						if (!thread.IsAlive) {
							thread.Abort();
							m_list.Remove(thread);
						} else {
							count++;
						}
					}
				}
			} catch (Exception ex) {
				throw ex;
			}
			return count;
		}

		/// <summary>
		/// Stops execution of the entire ThreadGroup. All running Threads
		/// are aborted and no Threads that exist in the queue will be started.
		/// </summary>
		public void Stop() {
			lock (m_list.SyncRoot) {
				IEnumerator enumerator = m_list.GetEnumerator();
				while (enumerator.MoveNext()) {
					Thread thread = (Thread)enumerator.Current;
					thread.Abort();
				}
				if (m_queueWatcher != null) {
					m_queueWatcher.Abort();
				}
			}
		}

		/// <summary>
		/// Periodically moves Threads from the queue to the list of running Threads.
		/// </summary>
		class QueueWatcher : Runnable {
			private ThreadGroup m_group;
			private int m_refresh;

			/// <summary>
			/// Constructs a new instance.
			/// </summary>
			/// <param name="group">The ThreadGroup to watch</param>
			/// <param name="refresh">The interval to use when looking at the ThreadGroup</param>
			public QueueWatcher(ThreadGroup group, int refresh) {
				m_group = group;
				m_refresh = refresh;
			}

			/// <summary>
			/// Watches the ThreadGroup queue by calling its MoveFromQueue method.
			/// </summary>
			public void Run() {
				while (true) {
					try {
						m_group.MoveFromQueue();
						System.Threading.Thread.Sleep(m_refresh);
					} catch (Exception ex) {
						MatchnetException exception = new MatchnetException("ThreadGroup Error, Shutting down.", "ThreadGroup.Run", ex);
						//ExceptionLogger.LogToEventLog(exception, "Matchnet.Threading.ThreadGroup", System.Diagnostics.EventLogEntryType.Warning);
						m_group.Stop();
						break; // fall through abort of thread
					}
				}
			}
		}
	}
}
