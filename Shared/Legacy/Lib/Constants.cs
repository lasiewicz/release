using System;

namespace Matchnet.Lib
{
	// Page Types
	public enum PageTypes
	{
		Normal = 0,
		Popup = 1,
		PopupOnce = 2,	//this makes it a popup once, all links on this page don't won't keep state (e.g. home page)
		Simple = 3,		//just the contents of the app space
		Profile = 4,
		TopOnly = 5,
		Binary = 6,
		Simple2 = 7
	};

	// ResultList Display Context Types
	public enum ResultListDisplayContextTypes
	{
		HotList, 
		SearchResult, 
		MembersOnline
	}

	/// <summary>
	/// Summary description for ConstantsTemp.
	/// </summary>
	public class ConstantsTemp
	{

		// Private Labeles
		public const int PRIVATELABELID_AS = 1;
		public const int PRIVATELABELID_GL = 2;
		public const int PRIVATELABELID_JD = 3;
		public const int PRIVATELABELID_JI = 4;
		public const int PRIVATELABELID_DE = 5;
		public const int PRIVATELABELID_UK = 6;
		public const int PRIVATELABELID_AU = 7;
		public const int PRIVATELABELID_FR = 11;
		public const int PRIVATELABELID_CA = 13;
		public const int PRIVATELABELID_CI = 15;
		public const int PRIVATELABELID_MATCHNET = 8;
		public const int PRIVATELABELID_FACELINK = 9;
		public const int PRIVATELABELID_CL = 12;
		public const int PRIVATELABELID_SS = 1450;
		public const int PRIVATELABELID_NANA = 1933;
		
		// Domains
		public const int DOMAINID_AS = 1;
		public const int DOMAINID_GY = 2;
		public const int DOMAINID_JD = 3;
		public const int DOMAINID_JI = 4;
		public const int DOMAINID_MN = 8;
		public const int DOMAINID_FL = 9;
		public const int DOMAINID_CP = 10;
		public const int DOMAINID_CL = 12;

		// Language
		public const int LANG_ENGLISH = 2;
		public const int LANG_FRENCH = 8;
		public const int LANG_GERMAN = 32;
		public const int LANG_HEBREW = 262144;

		// Locale
		public const int LOCALE_US_ENGLISH = 1033;
		public const int LOCALE_HEBREW = 1037;
		public const int LOCALE_GERMAN = 1031;
		public const int LOCALE_FRENCH = 1036;

		// Text Direction
		public const string TEXT_DIRECTION_LTR = "ltr";
		public const string TEXT_DIRECTION_RTL = "rtl";

		// Region
		public const int REGION_US = 223;
		public const int REGIONID_USA = 223;
		public const int REGIONID_CANADA = 38;
		public const int REGIONID_ISRAEL = 105;
		public const int REGIONID_GERMANY = 83;
		public const int REGIONID_UNITED_KINGDOM = 222;
		public const int REGIONID_AUSTRALIA = 13;
		public const int REGIONID_FRANCE = 76;
		public const int REGIONID_ARGENTINA = 10;
        public const int REGIONID_BELGIUM = 21;
        public const int REGIONID_LUXEMBURG = 125;
        public const int REGIONID_SWITZERLAND = 204;

		// Gender Mask
		public const int GENDERID_MALE = 1;
		public const int GENDERID_FEMALE = 2;
		public const int GENDERID_SEEKING_MALE = 4;
		public const int GENDERID_SEEKING_FEMALE = 8;
		public const int GENDERID_MTF = 16;
		public const int GENDERID_FTM = 32;
		public const int GENDERID_SEEKING_MTF = 64;
		public const int GENDERID_SEEKING_FTM = 128;

		// utility
		public const int NULL_NUMBER = -2147483647;
		public const string NULL_STRING = null;
		public const long NULL_LONG =  -2147483647;
		public const decimal NULL_DECIMAL = -2147483647;
		public const double NULL_DOUBLE = Double.MinValue;

		//Business rules
		public const int MAX_PASSWORD_LENGTH = 16;
		public const int MIN_PASSWORD_LENGTH = 4 ;

		public const int GUID_LENGTH = 40;
		public const int FIELD_SIZE = 2000;
		public const int ERROR_SUCCESS = 0;
		public const int RESOURCE_NOT_FOUND = 700;

		// application 
		public const int APP_HOME = 1;
		public const int APP_EMAIL = 27;
		public const int APP_MEMBER_PROFILE = 7;
		public const int APP_FREETEXTAPPROVAL= 525;

		// message types
		public const int TEXT_MESSAGE = 1;
		public const int HTML_MESSAGE = 2;
		public const int HTML_TEXT_MESSAGE = 3;

		// attribute groups
		public const int AG_PHOTO_EDITOR = 2;
		public const int AG_MINI_PROFILE = 4;
		public const int AG_MEMBER_SEARCH_FORM = 6;
		public const int AG_GALLERY = 8;
		public const int AG_MEMBER_PROFILE_EDIT = 10;
		public const int AG_MEMBER_STATUS = 12;
		public const int AG_MEMBER_ONLINE_PROFILE = 14;
		public const int AG_ADMIN_PROFILE = 16;
		public const int AG_FREETEXT = 18;
		public const int AG_REG_STEP_1 = 22;
		public const int AG_REG_STEP_2 = 24;
		public const int AG_REG_STEP_3 = 26;
		public const int AG_REG_STEP_4 = 28;
		public const int AG_REG_STEP_5 = 30;
		public const int AG_REG_STEP_6 = 32;
		public const int AG_REG_WELCOME = 62;
		public const int AG_IM_ATTRIBUTES = 34;
		public const int AG_AWAY_STATUS = 36;
		public const int AG_CONTACT_MEMBER = 38;
		public const int AG_PHOTO_EDITOR_REJECT_REASONS = 40;
		public const int AG_CROSS_PROMOTION_CHECK = 42;
		public const int AG_EMAIL_SETTINGS = 44;
		public const int AG_MAILBOX_SETTINGS = 48;
		public const int AG_MEMBERSERVICES_PROFILE_DISPLAY = 50;
		public const int AG_NEWMAILALERT = 54;
		public const int AG_HOME_ATTRIBUTES = 56;
		public const int AG_CHANGE_EMAIL_PASSWORD = 60;
		public const int AG_CHANGE_EMAIL = 64;

		// pages
		public const int PAGE_LOGON_DEFAULT = 2000;
		public const int PAGE_LOGON_RETRIEVE_PASSWORD = 2021;
		public const int PAGE_MEMBER_PHOTOS_DEFAULT = 4000;
		public const int PAGE_MEMBER_PROFILE_VIEW_PROFILE_MAIN = 7070;
		public const int PAGE_MEMBER_PROFILE_EDIT_PROFILE = 7080;
		public const int PAGE_MEMBER_SERVICES_EMAIL_SETTINGS = 18090;
		public const int PAGE_MEMBER_SERVICES_PROFILE_DISPLAY_SETTINGS = 18150;
		public const int PAGE_MEMBER_LIST_DEFAULT = 26000;
		public const int PAGE_HOME_DEFAULT = 1000;
		public const int PAGE_HOME_DEFAULT_VISITORCONTENT = 1005;
		public const int PAGE_HOME_SPLASH = 1030;

		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_DEFAULT_ = 522000;
		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_ADMIN_NOTES = 522010;
		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_CHANGE_EMAIL = 522020;
		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_EMAIL_LOG = 522030;
		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_INSERT_NOTE = 522040;
		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_PASSWORD_LOOKUP = 522050;
		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_SAVE_GLOBALSTATUSMASK = 522060;
		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_VIEW_ACTION_LOG = 522070;
		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_RESULTS = 522080;
		public const int PAGE_ADMIN_MEMBER_SEARCH_EX_CLOSE_WINDOW = 522090;
		public const int PAGE_SUBSCRIPTIONS_CREDITCARDFORM = 3000;
		public const int PAGE_SUBSCRIPTIONS_CONFIRM = 3010;
		public const int PAGE_SUBSCRIPTIONS_CHECKFORM = 3020;
		public const int PAGE_SUBSCRIPTIONS_CONFIRM_COMPLETE = 3030;
		public const int PAGE_SUBSCRIPTIONS_ONE_CLICK = 3040;
		public const int PAGE_SUBSCRIPTIONS_DEFAULT = 3050;
		public const int PAGE_SUBSCRIPTIONS_TRANSACTIONS = 3060;
		public const int PAGE_SUBSCRIPTIONS_FAILURE = 3080;
		public const int PAGE_SUBSCRIPTIONS_END = 3090;
		public const int PAGE_SUBSCRIPTIONS_END_RESULT = 3100;
		public const int PAGE_SUBSCRIPTIONS_ADMIN_INSERT = 3110;
		public const int PAGE_SUBSCRIPTIONS_ADMIN_ADJUST = 3120;
		public const int PAGE_SUBSCRIPTIONS_CREDIT_CARD_UPDATE = 3130;

		public const int PAGE_SEARCH_RESULTS = 22100;

		// PAGE_HOTLIST_DEFAULT - Hotlist.HotListCategory = AnalyticsID
		public const int PAGE_HOTLIST_DEFAULT = 26100;

		public const int PAGE_MEMBER_SERVICES_LOGON_ACTIVITY = 18080;

		public const int PAGE_ADMIN_FREE_TEXT_APPROVAL_DEFAULT = 525000;
		public const int PAGE_ADMIN_FREE_TEXT_APPROVAL_ATTRIBUTE_EDIT = 525010;
		public const int PAGE_ADMIN_FREE_TEXT_APPROVAL_SAVE_CONFIRMATION = 525020;
		public const int PAGE_ADMIN_FREE_TEXT_APPROVAL_CONFIRM_PAGE = 525030;
		public const int PAGE_MEMBER_SERVICES_SUSPEND = 18020;

		// actions
		public const int ACTION_SEARCH_LARGE_FORM_SUBMIT = 22500;
		public const int ACTION_SEARCH_MENU = 22510;
		public const int CS_SUPPORT_PHONE_NUMBER = 602782;

		public const int ACTION_ADMIN_MEMBER_SEARCH_EX_VALIDATE_SEARCH = 522500;
		public const int ACTION_ADMIN_MEMBER_SEARCH_EX_INSERT_NOTE = 522510;
		public const int ACTION_ADMIN_MEMBER_SEARCH_EX_SEND_PASSWORD = 522520;
		public const int ACTION_ADMIN_MEMBER_SEARCH_EX_SEND_WELCOME_EMAIL = 522530;
		public const int ACTION_ADMIN_MEMBER_SEARCH_EX_SEND_VERIFY_EMAIL_LETTER = 522540;
		public const int ACTION_ADMIN_MEMBER_SEARCH_EX_SAVE_GLOBALSTATUSMASK = 522550;
		public const int ACTION_ADMIN_MEMBER_SEARCH_EX_CHANGE_EMAIL = 522560;
		public const int ACTION_SUBSCRIPTIONS_INSERT = 3500;
		public const int ACTION_SUBSCRIPTIONS_ONE_CLICK_BUY = 3510;
		public const int ACTION_SUBSCRIPTIONS_DEFAULT = 3520;
		public const int ACTION_SUBSCRIPTIONS_END = 3530;
		public const int ACTION_SUBSCRIPTIONS_ADMIN_INSERT = 3540;
		public const int ACTION_SUBSCRIPTIONS_ADMIN_ADJUST = 3550;
		public const int ACTION_SUBSCRIPTIONS_GET_END_PAGE = 3560;
		public const int ACTION_SUBSCRIPTIONS_CREDIT_CARD_SAVE = 3570;

		// region
		public const int DEFAULT_CITY_REGION_ID = 3404470; // los angeles, ca

		// age
		public const int MIN_AGE_RANGE = 18;
		public const int MAX_AGE_RANGE = 99;
		public const int AGE_RANGE_YOUNG = 0;
		public const int AGE_RANGE_TWENTY_SOMETHING = 1;
		public const int AGE_RANGE_THIRTY_SOMETHING = 2;
		public const int AGE_RANGE_FORTY_SOMETHING = 3;
		public const int AGE_RANGE_FIFTY_SOMETHING = 4;
		public const int AGE_RANGE_ALL = 5;

		// AgeGroups
		public const int AGE_GROUP_18_TO_25 = 1;
		public const int AGE_GROUP_26_TO_34 = 2;
		public const int AGE_GROUP_35_TO_45 = 3;
		public const int AGE_GROUP_46_TO_60 = 4;
		public const int AGE_GROUP_60_TO_99 = 5;

		// height & weight
		public const double HEIGHT_MIN = 137.16;    //these are in cm
		public const double HEIGHT_MAX = 243.84;
		public const double WEIGHT_MIN = 36288;  //these are in grams
		public const double WEIGHT_MAX = 181500;

		// hot list 
		public const int HOTLIST_DEFAULT = 0;
		public const int HOTLIST_VIEWEDYOURPROFILE = -1;
		public const int HOTLIST_HOTLISTEDYOU = -2;
		public const int HOTLIST_TEASEDYOU = -3;
		public const int HOTLIST_EMAILEDYOU = -4;
		public const int HOTLIST_INSTANTMESSAGEDYOU = -5;
		public const int HOTLIST_YOUTEASED = -6;
		public const int HOTLIST_YOUEMAILED = -7;
		public const int HOTLIST_YOUINSTANTMESSAGED = -8;
		public const int HOTLIST_YOUVIEWED = -9;
		public const int HOTLIST_YOURMATCHES = -10;
		public const int HOTLIST_WHOSONLINE = -11;
		public const int HOTLIST_PROFILE = -12;
		public const int HOTLIST_YOURIGNORELIST = -13;
		public const int HOTLIST_MATCHEDYOU = -14;

		// bit operators
		public const int OP_NONE = 0;
		public const int OP_BETWEEN = 1;
		public const int OP_IN = 2;
		public const int OP_LIKE = 3;
		public const int OP_LESSTHAN = 4;
		public const int OP_GREATERTHAN = 5;
		public const int OP_LESSTHANEQUAL = 6;
		public const int OP_GREATERTHANEQUAL = 7;
		public const int OP_EQUAL = 8;
		public const int OP_NOTEQUAL = 9;
		public const int OP_BITWISEAND = 100;
		public const int OP_BITWISEOR = 101;
		public const int OP_BITWISE2SCOMPLEMENT = 102;
		public const int OP_BITWISECLEAR = 103;
		public const int OP_RADIUSRANGE = 200;

		// internal mail types
		public const int MT_EMAIL = 1;
		public const int MT_TEASE = 2;
		public const int MT_MISSEDIM = 3;
		public const int MT_SYSTEMANNOUNCEMENT = 4;
		public const int MT_DECLINEEMAIL = 5;
		public const int MT_IGNOREEMAIL = 6;
		public const int MT_YESEMAIL = 7;

		// internal mail system folders
		public const int SF_INBOX = 1;
		public const int SF_DRAFT = 2;
		public const int SF_SENT = 3;
		public const int SF_TRASH = 4;
	
		// internal mail status
		public const int MS_NOTHING = 0;
		public const int MS_READ = 1;
		public const int MS_REPLIED = 2;
		public const int MS_SHOWSENT = 4;

		// resource apps
		public const int RESOURCE_APP_TEASE = 2;

		// resource types
		public const int RESOURCE_TYPE_PAGE_CONTENT = 1;
		public const int RESOURCE_TYPE_ARTICLE = 2;
		public const int RESOURCE_TYPE_CATEGORY = 3;
		public const int RESOURCE_TYPE_ATTRIBUTE_OPTION = 6;
		public const int RESOURCE_TYPE_ERROR_MESSAGE = 7;
		public const int RESOURCE_TYPE_CONTROL_TEXT = 8;
		
		// object types
		public const int OBJECT_TYPE_TEASE = 2;
		public const int OBJECT_TYPE_TEASE_CATEGORY = 4;


		// needed for switching domains via query string
		public const string IMPERSONATE_MEMBERID = "impmid";
		public const string IMPERSONATE_PRIVATELABELID = "imppid";
		public const string IMPERSONATE_DOMAINID = "impdid";
		public const string	IMPERSONATE_LANGUAGEID = "implid";
		public const string SESSION_DOMAIN_NAME = "sdid";
		public const string SESSION_OVERRIDE_UID_NAME = "mnsosid";
		//public const string SESSION_OVERRIDE_UID_NAME = "mnsouid";

		//GlobalStatusMask options
		public const string GLOBAL_STATUS_MASK = "GlobalStatusMask";
		public const int GLOBAL_STATUS_MASK_DEFAULT = 0;
		public const int ADMIN_SUSPEND = 1;
		public const int BAD_EMAIL_SUSPEND = 2;
		public const int EMAIL_VERIFIED = 4;
		public const int ADMIN_SUSPEND_ACTION = 4; // MatchNet.Web.Applications.Admin.MemberSearch.ChangeGlobalStatusMask

		// StatusMask options
		public const string SUSPEND_MASK = "SuspendMask";
		public const int SUSPENDMASK_DEFAULT = 0;
		public const int SUSPENDMASK_ADMIN = 1;
		public const int SUSPENDMASK_SELF = 2;		

		// These are the different page types
		public const int PT_DEFAULT = 0;
		public const int PT_POPUP = 1;
		public const int PT_POPUP_ONCE = 2; // this makes it a popup once, all links on this page don't won't keep state (e.g. home page)
		public const int PT_SIMPLE = 3; // just the contents of the app space
		public const int PT_PROFILE = 4;
		public const int PT_TOPONLY = 5;

		// Admin Access Privilege
		public const int PRIVILEGE_ADD_EDIT_DELETE_PRIVILEGES = 1;
		public const int PRIVILEGE_GRANT_REVOKE_PRIVILEGES_TO_MEMBERS = 2;
		public const int PRIVILEGE_GRANT_REVOKE_PRIVILEGES_TO_GROUPS = 6;
		public const int PRIVILEGE_CHANGE_EMAIL = 10;
		public const int PRIVILEGE_VIEW_PASSWORD = 12;
		public const int PRIVILEGE_ADD_EDIT_DELETE_GROUPS = 13;
		public const int PRIVILEGE_VIEW_LIST_OF_GROUPS = 14;
		public const int PRIVILEGE_VIEW_CREDITCARD = 15;
		public const int PRIVILEGE_MEMBER_PHOTO_UPLOAD_ANOTHER_MEMBER = 24;
		public const int PRIVILEGE_APPROVAL_APPLICATION = 25;
		public const int PRIVILEGE_MEMBER_SEARCH = 32;
		public const int PRIVILEGE_CACHE_ADMIN = 34;
		public const int PRIVILEGE_ARTICLE_ADMIN = 36;
		public const int PRIVILEGE_PURCHASE_PACKAGE_FOR_ANOTHER_MEMBER = 40;
		public const int PRIVILEGE_DISCOUNTED_PACKAGES = 42;
		public const int PRIVILEGE_CLOSED_PACKAGES = 44;
		public const int PRIVILEGE_DOMAIN_LIST_ADMINISTRATION = 46;
		public const int PRIVILEGE_ADD_EDIT_DELETE_ATTRIBUTES = 48;
		public const int PRIVILEGE_ADD_EDIT_DELETE_RESOURCES = 50;
		public const int PRIVILEGE_PROMOTION_ADMIN = 52;
		public const int PRIVILEGE_EDIT_MEMBER_ATTRIBUTES = 54;
		public const int PRIVILEGE_ADD_EDIT_DELETE_MEMBERS_FROM_A_GROUP = 58;
		public const int PRIVILEGE_REPORTS = 62;
		public const int PRIVILEGE_VIEW_SITE_STATUS_PAGE = 74;
		public const int PRIVILEGE_EDIT_ADMIN_MEMBER_ATTRIBUTES = 76;
		public const int PRIVILEGE_DOMAIN_ALIASES_PARTIAL_ADMIN = 78;
		public const int PRIVILEGE_DOMAIN_ALIASES_DATA_ENTRY = 80;
		public const int PRIVILEGE_ADMINISTER_POLLS = 82;
		public const int PRIVILEGE_CATEGORIES = 84;
		public const int PRIVILEGE_VERIFY_EMAIL_ADDRESSES = 86;
		public const int PRIVILEGE_DOMAIN_ALIASES_SUPER_ADMIN = 90;
		public const int PRIVILEGE_EVENTS_ADMIN = 92;
		public const int PRIVILEGE_ADMINISTER_PAGETEXT = 96;
		public const int PRIVILEGE_CREDIT_CARD_UPDATE = 98;
		public const int PRIVILEGE_DEPLOYMENT_IMAGE_VIEW = 104;
		public const int PRIVILEGE_DEPLOYMENT_IMAGE_PUBLISH = 106;
		public const int PRIVILEGE_DEPLOYMENT_CONTENT_VIEW = 108;
		public const int PRIVILEGE_DEPLOYMENT_CONTENT_PUBLISH = 110;
		public const int PRIVILEGE_DEPLOYMENT_CACHE_CLEAR = 112;
		public const int PRIVILEGE_DEPLOYMENT_WEB_VIEW = 114;
		public const int PRIVILEGE_DEPLOYMENT_WEB_PUBLISH = 116;
		public const int PRIVILEGE_CHARGE_LOG_PERFORM_CREDIT = 118;
		public const int PRIVILEGE_DEPLOYMENT_DEPLOY_IMAGES = 120;
		public const int PRIVILEGE_DEPLOYMENT_DEPLOY_WEB = 122;
		public const int PRIVILEGE_CSR_BBS_POST_EDIT = 123;


		public const int PAGE_SIZE = 10;

		// Action mask
		public const int ACTIONMASK_DEFAULT = 0;
		public const int ACTIONMASK_SEARCHBROADCAST = 1; //Use this sparingly!! Broadcast this immediately to Search (e.g. GlobalStatusMask - make Member not searchable)
		
		//Trascation reason type.
		public const int TRANSACTIONREASONTYPE_INTERNALUSE = 1;
		public const int TRANSACTIONREASONTYPE_GOODREASON = 2;
		public const int TRANSACTIONREASONTYPE_BADREASON = 3;


		// application id
		public const int SUBSCRIPTION_APPID = 3;

		public const string USERNAME_SUPPORT_ACCOUNT = "comments";
		public const string USERNAME_EMAILBOUNCE_ACCOUNT = "bounce";
		public const string USERNAME_ACTIVATION_ACCOUNT = "Activations";
		public const string USERNAME_RETURN_ACCOUNT = "communications";
		public const string USERNAME_VERIFICATION_ACCOUNT = "VerifyEmail";

		// PrivateLabel status mask
		public const int PRIVATELABEL_STATUS_MASK_PAY_SITE = 1;
		public const int PRIVATELABEL_HAS_COBRAND_HEADER = 128;
		public const int PRIVATELABEL_STATUS_DISABLE_SSLURL = 4096;

		public const int MAX_REGION_STRING_LENGTH = 40;
		public const int MAX_HEADLINEORABOUT_STRING_LENGTH = 35;

		public const int LOOKUPTYPE_KEYWORDS = 1;
		public const int LOOKUPTYPE_USERNAME = 2;
		public const int LOOKUPTYPE_MEMBERID = 3;

		public const int ADMIN_NOTE_MAX_LENGTH = 3900;

		
		//Location search type constants
		public const int SEARCH_TYPE_POSTAL_CODE = 1;
		public const int SEARCH_TYPE_AREA_CODE = 2;
		public const int SEARCH_TYPE_REGION = 4;
		public const int SEARCH_TYPE_COLLEGE = 8;

		public const int PAYMENT_TYPE_NONE = 0;
		public const int PAYMENT_TYPE_CREDITCARD = 1;
		public const int PAYMENT_TYPE_CHECK = 2;
		public const int PAYMENT_TYPE_DIRECTDEBIT = 4;
		

		//The threshold for showing the New gif in Members Online and Search
		public const int NEW_DAY_LIMIT = 7;

		public const string HTTPIM_ENCRYPT_KEY = "8ffez102";

		// Free text approval status
		public const int MEMBERATTRIBUTE_STATUS_NOT_APPROVED = 1;
		public const int MEMBERATTRIBUTE_STATUS_AUTO_APPROVED = 2;
		public const int MEMBERATTRIBUTE_STATUS_HUMAN_APPROVED = 4;

		// Attribute data types
		public const int ATTRIBUTE_TYPE_FREE_TEXT = 1;
		public const int ATTRIBUTE_TYPE_SINGLE_SELECT = 2;
		public const int ATTRIBUTE_TYPE_MULTI_SELECT = 3;

		// Search order types
		public const int SEARCH_ORDERBY_REGISTER_DATE = 1;
		public const int SEARCH_ORDERBY_LOGON_DATE = 2;
		public const int SEARCH_ORDERBY_PROXIMITY = 3;
		public const int SEARCH_ORDERBY_POPULARITY = 4;

		public const int MAX_USERNAME_LENGTH = 12;


		//Cache time to live
		public const int CACHE_LONG = 3600;
		public const int CACHE_MEDIUM = 60;
		public const int CACHE_SHORT = 10; 
		public const int CACHE_DAY = 86400;
		public const int ARTICLE_CACHE_TTL = 3600;
		public const string ENCRYPT_KEY = "kdfajdsf";
		public const string ENCRYPT_PREFIX = "zX";
		public const string NEXT_REGISTRATION_ACTIONPAGEID = "nxtRegStp";
		public const string ENCRYPT_DELIM = "^";
		public const int ACTION_MEMBER_SERVICES_VERIFY_EMAIL = 18540;
		public ConstantsTemp()
		{
		}
	}
}