using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;

using Matchnet.Lib.Data;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Threading;
using Matchnet.Lib.Util;

namespace Matchnet.Lib.Session
{
	/// <summary>
	/// Summary description for TraySession.
	/// </summary>
	public class TraySession :
         Matchnet.Lib.Session.Session
	{
		public TraySession()
		{

		}

        public void KeepAlive(string uid)
        {
            // Update the session's timestamp
            SQLDescriptor sessionDesc = new SQLDescriptor("mnSession", Key );
            SQLClient sessionClient = new SQLClient(sessionDesc);

            sessionClient.AddParameter("@SessionKey", SqlDbType.VarChar, ParameterDirection.Input,Guid);
            sessionClient.ExecuteNonQuery("up_Session_ListIMKeepAlive", CommandType.StoredProcedure);
        }

        public void SetTrayStatus(bool enableTray)
        {
			// no op for now
		/*
			try
			{
                // Update the session status...
				SQLDescriptor sessionDesc = new SQLDescriptor("mnSession", Key );
				SQLClient sessionClient = new SQLClient(sessionDesc);

                sessionClient.AddParameter("@SessionID", SqlDbType.Int, ParameterDirection.Input,mSessionId);

                if ( enableTray )
                    sessionClient.AddParameter("@StatusMask", SqlDbType.Int, ParameterDirection.Input,1);
                else
                    sessionClient.AddParameter("@StatusMask", SqlDbType.Int, ParameterDirection.Input,0);

				sessionClient.ExecuteNonQuery("up_Session_Status_Update", CommandType.StoredProcedure);
			}
			catch(Exception ex)
			{
                System.Diagnostics.Debug.WriteLine(ex.ToString());
			}
		*/

        }
	}
}
