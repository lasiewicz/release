using System;
using System.Web;
using System.Web.Services;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;

using Matchnet.Lib.Data;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Threading;
using Matchnet.Lib.Util;

namespace Matchnet.Lib.Session
{
	/// <summary>
	/// Handles persistance of per session values. 
	/// Maintains mapping between cookie and sesion.
	/// </summary>
	public class Session 
	{
		protected String mCookieName;
		protected String mGuid;
		protected Int32 mSessionId;
		protected Int32 mMemberId;
		protected Int32 mDomainId;
		protected bool mWriteToCookie = false;
		protected bool mWriteToDatabase = false;

		public enum  SessionValueType { TemporaryValueType, PersistentValueType, TemporaryPersistentValueType };

		// Public transent properties
		public Hashtable mPersistentProperties;
		public Hashtable mTemporaryProperties;
		public Hashtable mTemporaryPersistentProperties;

		protected const string ITEM_TOKEN		= "|D|";
		protected const string VALUE_TOKEN		= "|=|";
		protected const string PERSIST_TOKEN	= "|~|";

		public const string MATCHNET_COOKIE = "mnc";
		public const Int32 MATCHNET_COOKIE_VERSION = 4;
		public const string DEV_COOKIE = "dev";
		public const string MATCHNET_COOKIE_LAST_UPDATE_NAME = "lupk";
		public const string MATCHNET_COOKIE_GUID_NAME = "sid";
		public const string SESSION_OVERRIDE_UID_NAME = "mnsouid";

		// Public interface
		public Session()
		{
			mPersistentProperties = new Hashtable();
			mTemporaryProperties = new Hashtable();
			mTemporaryPersistentProperties= new Hashtable();
		}

		public void InitializeSession(HttpContext Context, bool DevelopmentMode)
		{	
			if (DevelopmentMode == true)
			{
				mCookieName = DEV_COOKIE;
			}
			else
			{
				mCookieName = MATCHNET_COOKIE + Convert.ToString(MATCHNET_COOKIE_VERSION);
			}

			if (Context.Request[SESSION_OVERRIDE_UID_NAME] != null)
			{
				// Made new guid perisist into the cookie RAM
				mGuid = Context.Request[SESSION_OVERRIDE_UID_NAME].ToString();
				AddValue(MATCHNET_COOKIE_GUID_NAME, mGuid, SessionValueType.PersistentValueType, false);
				AddValue(MATCHNET_COOKIE_LAST_UPDATE_NAME, DateTime.Now.ToString(), SessionValueType.PersistentValueType, false);
			}
			else if (Context.Request.Cookies[mCookieName] != null)
			{
				// Just grab the guid
				mGuid = HttpUtility.UrlDecode(Context.Request.Cookies[mCookieName].Values[MATCHNET_COOKIE_GUID_NAME], System.Text.Encoding.ASCII);
				// Load the persistent values
				foreach( string persistentValueName in Context.Request.Cookies[mCookieName].Values )
				{
					AddValue(persistentValueName,HttpUtility.UrlDecode(Context.Request.Cookies[mCookieName].Values[persistentValueName]),SessionValueType.PersistentValueType, true);
				}
			}

			if (mGuid != null)
				// Now fetch our session data from the DB
				Load();
			else
				StartSession();
		}

		// Used for desktop IM, no cookies in that context
		public void InitializeSession(string guid)
		{	
			mGuid = guid;
			Load();
		}

		private void StartSession()
		{
			string guid = System.Guid.NewGuid().ToString().ToUpper();

			AddValue(MATCHNET_COOKIE_GUID_NAME, guid, SessionValueType.PersistentValueType, false);
			AddValue(MATCHNET_COOKIE_LAST_UPDATE_NAME, DateTime.Now.ToString(), SessionValueType.PersistentValueType, false);
		}

		public void AddValue(String name, String value, SessionValueType valueType)
		{
			AddValue(name, value, valueType, false);
		}

		private void AddValue(String name, String value, SessionValueType valueType, bool loadingMode)
		{
			Hashtable props;

			if (valueType == SessionValueType.PersistentValueType)
			{
				props = mPersistentProperties;
			}
			else if(valueType == SessionValueType.TemporaryPersistentValueType)
			{
				props = mTemporaryPersistentProperties;

			}
			else
			{
				props = mTemporaryProperties;
			}
		
			if (props.ContainsKey(name))
			{
				//update existing value
				props[name] = value;
			}
			else
			{
				//add new value
				props.Add(name,value);
			}

			if(loadingMode == false) 
			{
				if(valueType == SessionValueType.PersistentValueType) 
				{
					mWriteToCookie = true;
				} 
				else if(valueType == SessionValueType.TemporaryPersistentValueType || valueType == SessionValueType.TemporaryValueType) 
				{
					mWriteToDatabase = true;
				}
			}
		}

		private void SaveToDatabase(string ipAddress)
		{
			if (mDomainId == 0) return;

			StringBuilder buf;

			try
			{
				buf = new StringBuilder();

				IDictionaryEnumerator item = mTemporaryProperties.GetEnumerator();
				while (item.MoveNext())
				{
					buf.Append(ITEM_TOKEN);
					buf.Append(item.Key);
					buf.Append(VALUE_TOKEN);
					buf.Append(item.Value);
				}

				buf.Append(PERSIST_TOKEN);

				item =  mTemporaryPersistentProperties.GetEnumerator();
				while (item.MoveNext())
				{
					buf.Append(ITEM_TOKEN);
					buf.Append(item.Key);
					buf.Append(VALUE_TOKEN);
					buf.Append(item.Value);
				}

				SQLDescriptor descriptor = new SQLDescriptor("mnSession", Key);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter("@SessionKey", SqlDbType.VarChar, ParameterDirection.Input,mGuid);
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, mMemberId);
				client.AddParameter("@DomainID", SqlDbType.Int,ParameterDirection.Input,mDomainId);
				client.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);
				client.AddParameter("@Value", SqlDbType.NVarChar, ParameterDirection.Input, buf.ToString());

				client.ExecuteNonQuery("up_Session_Save", CommandType.StoredProcedure);
			}
			catch(MatchnetException ex)
			{
				System.Console.WriteLine(ex.ToString());
			}
		}

		private void SaveToCookie(string domainName)
		{
			System.Web.HttpCookie matchnetCookie = HttpContext.Current.Response.Cookies[mCookieName];
			// clear the cookie first
			matchnetCookie.Values.Clear();

			IDictionaryEnumerator item = mPersistentProperties.GetEnumerator();
			while (item.MoveNext())
			{
				matchnetCookie.Values.Add(item.Key.ToString(), item.Value.ToString());
			}
			matchnetCookie.Expires = System.DateTime.Now.AddYears(1);

			if(domainName != null && domainName.Length > 0)
			{
				matchnetCookie.Domain = "." + domainName;
			}
		}

		public void Save(string domainName, string ipAddress)
		{
			if(mWriteToDatabase) 
			{
				SaveToDatabase(ipAddress);
			}

			if(mWriteToCookie) 
			{
				SaveToCookie(domainName);
			}
			
		}

		// Implementation
		private void Load()
		{
			try
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnSession", Key );
				SQLClient client = new SQLClient(descriptor);
				client.AddParameter("@SessionKey", SqlDbType.VarChar, ParameterDirection.Input,mGuid);

				DataTable table = client.GetDataTable("up_Session_List", CommandType.StoredProcedure);
			
				if (table.Rows.Count > 0)
				{
					mSessionId  = System.Convert.ToInt32(table.Rows[0]["SessionID"].ToString());
					mMemberId	= System.Convert.ToInt32(table.Rows[0]["MemberID"].ToString());
					mDomainId	= System.Convert.ToInt32(table.Rows[0]["DomainID"].ToString());
			
					ParseValues(table.Rows[0]["Value"].ToString());
				}
			}
			catch(MatchnetException ex)
			{
				String err = ex.ToString();
			}
		}

		private void ParseValues(string rawValue)
		{
			String temporaryValues;
			String persistentValues;

			String[] valueComponents = Regex.Split(rawValue,PERSIST_TOKEN.Replace("|",@"\174"));
			temporaryValues = valueComponents[0];
			persistentValues = valueComponents[1];

			LoadItems(temporaryValues,SessionValueType.TemporaryValueType);
			LoadItems(persistentValues,SessionValueType.TemporaryPersistentValueType);
		}

		private void LoadItems(String values, SessionValueType valueType)
		{
			String[] valueList = Regex.Split(values,ITEM_TOKEN.Replace("|",@"\174"));

			foreach (String valuePair in valueList)
			{
				if (valuePair.Length > 0)
				{
					String[] nameValuePairList = Regex.Split(valuePair,VALUE_TOKEN.Replace("|",@"\174"));
					if (nameValuePairList.GetUpperBound(0) == 1)
					{
						AddValue(nameValuePairList[0],nameValuePairList[1], valueType, true);
					}
				}
			}
		}

		public String this[String name]
		{
			get 
			{
				String value=null;
				Object obj = null;

				if ( (obj = mTemporaryProperties[name]) != null ) 
				{
					value = obj.ToString();
				} 
				else if ( (obj = mTemporaryPersistentProperties[name]) !=null) 
				{
					value = obj.ToString();
				}
				else if ( (obj =  mPersistentProperties[name]) != null ) 
				{
					value = obj.ToString();
				} 

				return value;
			}
		}


		public string Guid
		{
			get
			{
				if (mGuid == null)
					return mPersistentProperties[MATCHNET_COOKIE_GUID_NAME].ToString();
				else
					return mGuid;
			}
		}

		public Int32 Key
		{
			get
			{
				if (mGuid == null || mGuid == string.Empty) 
				{
					// h4x0r3d to un-break userplane chat / cleared cookies
					string tempGuid = System.Guid.NewGuid().ToString().ToUpper();
					return Util.Util.CRC32(tempGuid);
				} 
				else 
				{
					return Util.Util.CRC32(mGuid);
				}
			}
		}

		public Int32 SessionId
		{
			get
			{
				return mSessionId;
			}
		}

		public Int32 MemberId
		{
			get
			{
				return mMemberId;
			}
		}

		public Int32 DomainId
		{
			get
			{
				return mDomainId;
			}
		}

		public bool IsLoggedIn
		{
			get
			{
				return mMemberId > 0;
			}
		}

		public bool IsAdmin
		{
			get
			{
				if ( this["IsAdmin"] != null && this["IsAdmin"]=="1" )
					return true;
				else
					return false;
			}
		}

		public void Clear()
		{
			if(mTemporaryProperties.Count > 0) 
			{
				mTemporaryProperties.Clear();
				mWriteToDatabase = true;
			}
			mMemberId = 0;
		}


		public static void SessionKeepAlive(string UID)
		{
			int key = Util.Util.CRC32(UID);
			
			SQLDescriptor descriptor = new SQLDescriptor("mnSession", key);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@SessionKey", SqlDbType.VarChar, ParameterDirection.Input, UID);
			client.ExecuteNonQuery("up_Session_ListIMKeepAlive", CommandType.StoredProcedure);
	   }
	}
}