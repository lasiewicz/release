using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Threading;
using Matchnet.Lib;
using Matchnet.Lib.Util;


namespace Matchnet.Lib.Subscription
{
	/// <summary>
	/// Summary description for Plan.
	/// </summary>
	public class Plan
	{
		public int PlanID;
		public int CurrencyID;
		public int ResourceID;
		public decimal InitialCost;
		public int InitialDurationTypeID;
		public int InitialDuration;
		public decimal RenewCost;
		public int RenewDurationTypeID;
		public int RenewDuration;
		public DateTime UpdateDate;

		public bool IsPopulated = false;
		

		public Plan()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public bool Populate(int intPlanID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnShared");
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, intPlanID);
		
			DataTable dt = client.GetDataTable("up_Plan_List_ByID", CommandType.StoredProcedure);
			DataRow r;

			if (dt.Rows.Count > 0)
			{
				r = dt.Rows[0];
			
				
				PlanID = Util.Util.CInt(r["PlanID"]);
				CurrencyID = Util.Util.CInt(r["CurrencyID"]);
				ResourceID = Util.Util.CInt(r["ResourceID"]);
				InitialCost = Util.Util.CDecimal(r["InitialCost"]);
				InitialDurationTypeID = Util.Util.CInt(r["InitialDurationTypeID"]);
				InitialDuration = Util.Util.CInt(r["InitialDuration"]);
				RenewCost = Util.Util.CDecimal(r["RenewCost"]);
				RenewDurationTypeID = Util.Util.CInt(r["RenewDurationTypeID"]);
				RenewDuration = Util.Util.CInt(r["RenewDuration"]);
				UpdateDate = Util.Util.CDateTime(r["UpdateDate"]);
				IsPopulated = true;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
