using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Caching;

using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.CachingTemp;

namespace Matchnet.Lib.Domain
{
	/// <summary>
	/// Summary description for Domain.
	/// </summary>
	public class Domain
	{
		const string DOMAIN_CACHEKEY_PREFIX = "Domain:";
		const int CACHE_SECONDS = 3600;
	    private Matchnet.CachingTemp.Cache _Cache;

		public Domain()
		{
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
		}

		public DataTable GetDomains()
		{
			string key = DOMAIN_CACHEKEY_PREFIX;
		    DataTable dt = (DataTable) _Cache[key];

			if (dt == null)
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnShared");
				SQLClient client = new SQLClient(descriptor);
				dt = client.GetDataTable("up_Domain_List", CommandType.StoredProcedure);
        // PT:  Removing old cache dependency, in favor of wrapped cache
				// System.Web.HttpRuntime.Cache.Insert(  key, 
        //                                        dt, 
        //                                      null, 
        //                                      DateTime.Now.AddSeconds(CACHE_SECONDS), 
        //                                      TimeSpan.Zero, 
        //                                      CacheItemPriority.High, 
        //                                      null);
        _Cache.Insert( key, 
                       dt, 
                       null,
                       DateTime.Now.AddSeconds(CACHE_SECONDS),
                       TimeSpan.Zero,
                       CacheItemPriority.High,
                       null );
			}
			return dt;
		}
	}
}
