using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Domain
{
	/// <summary>
	/// Summary description for Language.
	/// </summary>
	public class Language
	{
		public Language()
		{
		}

		public DataTable GetLanguages()
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnShared");
			SQLClient client = new SQLClient(descriptor);
			return client.GetDataTable("up_Language_List", CommandType.StoredProcedure);
		}
	}
}
