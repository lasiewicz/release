using System;
using System.Collections;
using System.Xml;

using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Config
{
	/// <summary>
	/// A shared configuration that provides in memory access to 
	/// properties defined in the c:\Matchnet\Matchnet.config file.
	/// </summary>
	public class Configuration
	{
		// TODO: provide a mechanism for specifying app specific overrides
		//       ie: mnMailQueue:InitialConnectionString

		/// <summary>
		/// The location of the configuration file
		/// </summary>
		public static string CONFIG_PATH = @"C:\Matchnet\Matchnet.config";

		private static Configuration m_singleton = LoadInstance();

		private Hashtable m_properties;

		/// <summary>
		/// Gets a property from the configuration if it exists. 
		/// </summary>
		/// <param name="propName">The name of the property</param>
		/// <returns>The property value</returns>
		public string GetProperty(string propName) {
			return (string)m_properties[propName.ToUpper()];
		}

		/// <summary>
		/// Adds a property to the global configuration. Properties
		/// added in this manner will not persist between restarts.
		/// </summary>
		/// <param name="propName">The name of the property</param>
		/// <param name="propValue">The property value</param>
		public void AddProperty(string propName, string propValue) {
			// should this lock? I don't think it matters because the
			// underlying collection is not exposed, so there is no
			// threat of modifying the collection while an enumeration
			// is being done.
			if (m_properties.ContainsKey(propName)) {
				m_properties[propName] = propValue;
			} else {
				m_properties.Add(propName.ToUpper(), propValue);
			}
		}

		/// <summary>
		/// Whether or not the property is defined in the configuration.
		/// </summary>
		/// <param name="propName">The name of the property to check</param>
		/// <returns>Whether or not the property is defined</returns>
		public bool PropertyIsDefined(string propName) {
			return m_properties.ContainsKey(propName.ToUpper());
		}

		private Configuration() {
			m_properties = new Hashtable();
		}

		/// <summary>
		/// The one and only instance of the global configuration.
		/// </summary>
		/// <returns>The one and only instance of the global configuration.</returns>
		public static Configuration GetInstance() {
			return m_singleton;
		}

		private static Configuration LoadInstance() {
			XmlTextReader reader = null;
			Configuration configuration = new Configuration();
			try {
				reader = new XmlTextReader(CONFIG_PATH);
				string propName = "";
				while (reader.Read()) {
					if (reader.Name.ToLower() != "configuration") {
						switch (reader.NodeType) {
							case XmlNodeType.Element:
								propName = reader.Name;
								break;
							case XmlNodeType.Text:
								if (propName != "") {
									configuration.AddProperty(propName, reader.Value);
									propName = "";
									
								}
								break;
						}
					}
				}
				return configuration;
			} catch (Exception ex) {
				MatchnetException exception = new MatchnetException("Unable to load properties", "ConfigLoader.LoadInstance", ex);
				throw exception;
			} finally {
				if (reader != null) {
					reader.Close();
				}
			}
		}
	}
}
