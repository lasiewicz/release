using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Xml;
using System.Collections;
using System.Reflection;

namespace Matchnet.Lib.Config
{
	/// <summary>
	/// Summary description for ListSectionHandler.
	/// </summary>
	public class ListSectionHandler
		: IConfigurationSectionHandler
	{
		public ListSectionHandler()
		{
		}

		public object Create( object parent, object configContext, XmlNode section)
		{
			return new ListSectionData(section);
		}
	}
}
