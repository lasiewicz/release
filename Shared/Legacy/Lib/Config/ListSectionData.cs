using System;
using System.Collections;
using System.Xml;
using System.Configuration;
using System.IO;

namespace Matchnet.Lib.Config
{
	/// <summary>
	/// Summary description for ListSectionData.
	/// </summary>
	public class ListSectionData
	{
		private string _ListName;
		private ArrayList _Data;
		private string _RootName;
		private string _AppName;
		private FileSystemWatcher _Watcher;
	//	private bool _IsDataValid=false;

		public string ListName 
		{
			get 
			{
				return _ListName;
			}
		}
		
		public ArrayList List
		{
			get
			{
				return _Data;
			}
		}


		public ListSectionData(XmlNode section)
		{
			LoadXml(section);
			SetupFileWatcher();
		}

		public void LoadXml(XmlNode section)
		{
			_Data = new ArrayList();
			XmlElement root =(XmlElement)section;
			_ListName = root.GetAttribute("name");

			_RootName = root.Name;
			_AppName = root.GetAttribute("appName");

			foreach( XmlNode node in root.ChildNodes )
			{
				if ( node.NodeType == XmlNodeType.Element )
				{
					string text = node.Attributes["value"].Value;
					_Data.Add(text);
				}
			}
		}

		public void SetupFileWatcher()
		{
			FileInfo info = new FileInfo(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
			string path = info.DirectoryName;
			string file = info.Name;

			try
			{
				_Watcher = new FileSystemWatcher(path);
				this._Watcher.Changed += new System.IO.FileSystemEventHandler(this._Watcher_Changed); // Glue in the FileSystemWatcher
				_Watcher.Filter = file;
				_Watcher.NotifyFilter = NotifyFilters.LastWrite;
				_Watcher.EnableRaisingEvents = true;
				//_IsDataValid = true;
			}
			catch( Exception ex )
			{
				throw new System.Configuration.ConfigurationException("Unable to initialize FileSystemWatcher for configuration file", ex);
			}
		}

		private void _Watcher_Changed(object sender, System.IO.FileSystemEventArgs args)
		{
			//_IsDataValid = false;
		}
	}
}
