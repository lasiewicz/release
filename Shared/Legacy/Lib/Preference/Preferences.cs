// TODO: This file should be removed from the solution, it is no longer necessary, waiting on access to the Matchnet.Lib project
// K.Landrus 12/6/04

//using System;
//using System.Data;
//using System.Data.SqlClient;
//
//using Matchnet.DataTemp;
//using Matchnet.Lib.Util;
//using Matchnet.Lib;
//
//namespace Matchnet.Lib.Preference
//{
//	/// <summary>
//	/// Summary description for Preferences.
//	/// </summary>	
//	public class Preferences
//	{
//		private int _DomainID;
//		private int _TranslationID;
//		private NameObjectCollection _PreferenceList;
//		
//
//		public int DomainID
//		{
//			set { _DomainID = value; }
//		}
//
//		public int TranslationID
//		{
//			set { _TranslationID = value; }
//		}
//		public Preferences()
//		{
//			_PreferenceList = new NameObjectCollection();
//		}
//		/// <summary>
//		/// Add single preference value to the preference list.
//		/// </summary>	
//		public int Add(string name, long val)
//		{
//			//see if the item exists already
//			
//			Preference pref = (Preference) _PreferenceList[name];
//			if (pref == null)
//			{
//				pref = new Preference();
//				_PreferenceList.Add(name.ToUpper(), pref);
//			}
//
//			pref.Name = name;
//			pref.Value = val;
//
//			return ConstantsTemp.ERROR_SUCCESS;
//		}
//
//		/// <summary>
//		/// Add comma delimited perference value to the preference list
//		/// </summary>	
//		public int AddList(string name, string val)
//		{
//			long finalValue = 0;
//			//see if the item exists already
//			Preference pref = (Preference) _PreferenceList[name];
//			if (pref == null)
//			{
//				pref = new Preference();
//				_PreferenceList.Add(name.ToUpper(), pref);
//			}
//
//			//parse the list and combine the elements
//			string [] split = null;
//			char [] delimiter = new char[] {','};
//			split = val.Split(delimiter);
//			for (int i = 0; i < split.Length; i++)
//			{
//				finalValue = finalValue | Util.Util.CLong(split[i], 0);
//			}
//
//			pref.Name = name;
//			pref.Value = finalValue;
//
//			return ConstantsTemp.ERROR_SUCCESS;
//		}
//
//		/// <summary>
//		/// Retrieve preference object from the preference list with name
//		/// </summary>	
//		public Preference this[string name]
//		{
//			get
//			{
//				return (Preference) _PreferenceList[name];
//			}
//		}
//		/// <summary>
//		/// Remove perference object from the preference list with name
//		/// </summary>	
//		public int Remove(string name)
//		{
//			_PreferenceList.Remove(name);
//			return ConstantsTemp.ERROR_SUCCESS;
//		}
//
//		/// <summary>
//		/// Retrieve item count in the preference list
//		/// </summary>	
//		public int Count()
//		{
//			return _PreferenceList.Count;
//		}
//
//		/// <summary>
//		/// Clear the preference list
//		/// </summary>	
//		public void Clear()
//		{
//			_PreferenceList.Clear();
//		}
//
//		/// <summary>
//		/// Retrieve item value in the preference list
//		/// </summary>	
//		public long Value(string name)
//		{
//			return this.Value(name, ConstantsTemp.NULL_LONG);
//		}
//
//		
//		/// <summary>
//		/// Retrieve item value in the preference list
//		/// </summary>	
//		public long Value(string name, long defaultVal)
//		{
//
//			Preference pref = (Preference) _PreferenceList[name];
//			if (pref == null)
//			{
//				return defaultVal;
//			}
//			else
//			{
//				return pref.Value;
//			}
//		}
//
//		/// <summary>
//		/// Retrieve item value in the preference list
//		/// </summary>	
//		public int Value(string name, int defaultVal)
//		{
//			Preference pref = (Preference) _PreferenceList[name];
//			if (pref == null)
//			{
//				return defaultVal;
//			}
//			else
//			{
//				return Util.Util.CInt(pref.Value);
//			}
//		}
//
//		/// <summary>
//		/// Retrieve item value in the preference list
//		/// </summary>	
//		public string Value(string name, string defaultVal)
//		{
//			Preference pref = (Preference) _PreferenceList[name];
//			if (pref == null)
//			{
//				return defaultVal;
//			}
//			else
//			{
//				return Util.Util.CString(pref.Value);
//			}
//		}
//
//		private int PopulateFromSession(string sessionKey)
//		{
//			//Clear the list
//			_PreferenceList.Clear();
//
//			SQLDescriptor descriptor = new SQLDescriptor("mnSession", Util.Util.CRC32(sessionKey));
//			SQLClient client = new SQLClient(descriptor);
//
//			client.AddParameter("@SessionKey", SqlDbType.VarChar, ParameterDirection.Input, sessionKey, ConstantsTemp.GUID_LENGTH);
//			
//			SqlCommand cmd = new SqlCommand("up_SearchPreference_ListSession");
//			cmd.CommandType = CommandType.StoredProcedure;
//			client.PopulateCommand(cmd);
//
//			DataTable table = client.GetDataTable(cmd);
//			DataRow r;
//			Preference pref;
//			int i;
//			string name;
//			long val;
//
//			for (i = 0; i < table.Rows.Count; i++)
//			{
//				r = table.Rows[i];
//				foreach(DataColumn dc in table.Columns)
//				{
//					name = dc.ColumnName;
//					val = Util.Util.CLong(r[dc.ColumnName]);
//					if (val != ConstantsTemp.NULL_LONG)
//					{
//						pref = new Preference();
//						pref.Name = name;
//						pref.Value = val;
//						_PreferenceList.Add(pref.Name, pref);
//					}
//				}
//			}
//			return ConstantsTemp.ERROR_SUCCESS;
//		}
//
//		private int PopulateFromMember(int memberID, int domainID)
//		{
//			//Clear the list
//			_PreferenceList.Clear();
//
//			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
//			SQLClient client = new SQLClient(descriptor);
//
//			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
//			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
//			
//			SqlCommand cmd = new SqlCommand("up_SearchPreference_ListMember");
//			cmd.CommandType = CommandType.StoredProcedure;
//			client.PopulateCommand(cmd);
//
//			DataTable table = client.GetDataTable(cmd);
//			DataRow r;
//			Preference pref;
//			int i;
//			string name;
//			long val;
//
//			for (i = 0; i < table.Rows.Count; i++)
//			{
//				r = table.Rows[i];
//				foreach(DataColumn dc in table.Columns)
//				{
//					name = dc.ColumnName;
//					val = Util.Util.CLong(r[dc.ColumnName]);
//					if (val != ConstantsTemp.NULL_LONG)
//					{
//						pref = new Preference();
//						pref.Name = name;
//						pref.Value = val;
//						_PreferenceList.Add(pref.Name, pref);
//					}
//				}
//			}
//			return ConstantsTemp.ERROR_SUCCESS;
//		}
//
//		/// <summary>
//		/// Populate the preference list
//		/// </summary>	
//		public int Populate(string sessionKey, int memberID, int domainID)
//		{
//			bool Found = false;
//			int ErrorNumber = ConstantsTemp.ERROR_SUCCESS;
//			if (! Util.StringUtil.StringIsEmpty(sessionKey))
//			{
//				ErrorNumber = this.PopulateFromSession(sessionKey);
//				Found = (this._PreferenceList.Count > 0);
//			}
//
//			if (ErrorNumber == ConstantsTemp.ERROR_SUCCESS)
//			{
//				if ((! Found) && (memberID > 0))
//				{
//					ErrorNumber = this.PopulateFromMember(memberID, domainID);
//					if (ErrorNumber == ConstantsTemp.ERROR_SUCCESS)
//					{
//						ErrorNumber = this.Save(sessionKey, memberID, domainID, false);
//					}
//				}
//			}
//
//			return ErrorNumber;
//		}
//
//		private bool ExcludeFromSession(string name)
//		{
//			switch (name.ToLower())
//			{
//				case "memberid": 
//					return true;
//				case "domainid" :
//					return true;
//				case "memberdomainid": 
//					return true;
//				case "sessionkey":
//					return true;
//				default:
//					return false;
//			}
//		}
//
//		/// <summary>
//		/// Save the preference list
//		/// </summary>	
//		public int Save(string sessionKey, int memberID, int domainID, bool saveToMember)
//		{
//			Preference pref;
//			
//			// Always save the session if we can
//			if (! Util.StringUtil.StringIsEmpty(sessionKey))
//			{
//				SQLDescriptor descriptor = new SQLDescriptor("mnSession", Util.Util.CRC32(sessionKey));
//				SQLClient client = new SQLClient(descriptor);
//
//				client.AddParameter("@SessionKey", SqlDbType.VarChar, ParameterDirection.Input, sessionKey, ConstantsTemp.GUID_LENGTH);
//				//Build the parameter list
//				for (int i = 0; i < _PreferenceList.Count; i++)
//				{
//					pref = (Preference) this._PreferenceList[i];
//					if (ExcludeFromSession(pref.Name) == false)
//					{
//						client.AddParameter("@" + pref.Name, SqlDbType.Int, ParameterDirection.Input, pref.Value);
//					}
//				}
//				
//				client.ExecuteNonQuery("up_SearchPreference_SaveSession", CommandType.StoredProcedure);
//			}
//
//			// save to memberX if they are logged in
//			if (memberID > 0 && saveToMember == true)
//			{
//				SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
//				SQLClient client = new SQLClient(descriptor);
//
//				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
//				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
//				//Build the parameter list
//				for (int i = 0; i < _PreferenceList.Count; i++)
//				{
//					pref = (Preference) this._PreferenceList[i];
//					if (pref.Name.ToLower() != "sessionkey") //-- cannot save session key 
//					{
//						client.AddParameter("@" + pref.Name, SqlDbType.Int, ParameterDirection.Input, pref.Value);
//					}
//				}
//
//				client.ExecuteNonQuery("up_SearchPreference_SaveMember", CommandType.StoredProcedure);
//		
//			}
//
//			return ConstantsTemp.ERROR_SUCCESS;
//		}
//		
////		/// <summary>
////		/// Retrieve option list for a given preference
////		/// </summary>	
////		public string Content(string name, string defaultValue)
////		{
////			return "";
////		}
////		/// <summary>
////		/// Retrieve option list for a given preference
////		/// </summary>	
////		public string Content(string name)
////		{
////			return this.Content(name, "");
////		}
//
//	}
//}
