using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace Matchnet.Lib.Encryption
{
	/// <summary>
	/// A wrapper around the System.Security.Cryptography.SymmetricAlgorithm that
	/// provides additional logic for encryption and decryption using DES, RC2, or Rijndael
	/// providers.
	/// </summary>
	public class SymmetricalEncryption
	{
		/// <summary>
		/// The provider to use when encrypting or decrypting.
		/// </summary>
		public enum Provider : int
		{
			/// <summary>
			/// DES encryption
			/// </summary>
			DES, 
			/// <summary>
			/// RC2 encryption
			/// </summary>
			RC2,
			/// <summary>
			/// Rijndael encryption
			/// </summary>
			Rijndael
		}

		private SymmetricAlgorithm cryptoService;

		/// <summary>
		/// Constructor for using an intrinsic .Net SymmetricAlgorithm class.
		/// </summary>
		public SymmetricalEncryption(Provider netSelected)
		{
			switch (netSelected)
			{
				case Provider.DES:
					cryptoService = new DESCryptoServiceProvider();
					break;
				case Provider.RC2:
					cryptoService = new RC2CryptoServiceProvider();
					break;
				case Provider.Rijndael:
					cryptoService = new RijndaelManaged();
					break;
			}
		}

		/// <summary>
		/// Constructor for using a customized SymmetricAlgorithm class.
		/// </summary>
		public SymmetricalEncryption(SymmetricAlgorithm serviceProvider)
		{
			cryptoService = serviceProvider;
		}

		/// <summary>
		/// Depending on the legal key size limitations of a specific CryptoService provider
		/// and length of the private key provided, padding the secret key with space character
		/// to meet the legal size of the algorithm.
		/// </summary>
		private byte[] GetLegalKey(string Key)
		{
			string sTemp;
			if (cryptoService.LegalKeySizes.Length > 0)
			{
				int lessSize = 0, moreSize = cryptoService.LegalKeySizes[0].MinSize;

				while (Key.Length * 8 > moreSize)
				{
					lessSize = moreSize;
					moreSize += cryptoService.LegalKeySizes[0].SkipSize;
				}
				sTemp = Key.PadRight(moreSize / 8, ' ');
			}
			else
			{
				sTemp = Key;
			}

			return ASCIIEncoding.ASCII.GetBytes(sTemp);
		}

		/// <summary>
		/// Encrypts a given string using a given key.
		/// </summary>
		/// <param name="Source">The source string to encrypt</param>
		/// <param name="Key">The key to use when encrypting the string</param>
		/// <returns>An encrypted string</returns>
		public string Encrypt(string Source, string Key)
		{
			byte[] bytIn = System.Text.ASCIIEncoding.ASCII.GetBytes(Source);

			System.IO.MemoryStream ms = new System.IO.MemoryStream();

			byte[] bytKey = GetLegalKey(Key);

			cryptoService.Key = bytKey;
			cryptoService.IV = bytKey;

			ICryptoTransform encrypto = cryptoService.CreateEncryptor();

			CryptoStream cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Write);

			cs.Write(bytIn, 0, bytIn.Length);
			cs.FlushFinalBlock();

			byte[] bytOut = new byte[ms.Position];
			ms.Position = 0;
			ms.Read(bytOut, 0, bytOut.Length) ;
   
			cs.Close();
			ms.Close();
                    
			return System.Convert.ToBase64String(bytOut, 0, bytOut.Length);
		}

		/// <summary>
		/// Decrypts a given string using a given key.
		/// </summary>
		/// <param name="Source">The source string to decrypt</param>
		/// <param name="Key">The key to use when decrypting the string</param>
		/// <returns>A decrypted string</returns>
		public string Decrypt(string Source, string Key)
		{
			byte[] bytIn = System.Convert.FromBase64String(Source);

			System.IO.MemoryStream ms = new System.IO.MemoryStream(bytIn, 0, bytIn.Length);

			byte[] bytKey = GetLegalKey(Key);

			cryptoService.Key = bytKey;
			cryptoService.IV = bytKey;

			ICryptoTransform encrypto = cryptoService.CreateDecryptor();
 
			CryptoStream cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Read);

			System.IO.StreamReader sr = new System.IO.StreamReader( cs );
			string s = sr.ReadToEnd();
			sr.Close();
			return s;
		}
	}
}
