using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Caching;
using System.Text;

using Matchnet.CachingTemp;
using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Util;
using mnTranslator;

namespace Matchnet.Lib.Option
{
	/// <summary>
	/// Summary description for Option.
	/// </summary>
	public class Option
	{
		private const string OPTION_CACHEKEY_PREFIX = "Option:";
		private const string TRANSLATIONINOPTION_CACHEKEY_PREFIX = "TranslationInOption:";
		private const int CACHE_TTL = 3600;
	//	private const int HEBREW_TRANSLATIONID = 20;
		private const int HEBREW_LANGUAGEID = 262144;
	    private Matchnet.CachingTemp.Cache _Cache;

		public Option()
		{
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
		}

		public DataTable GetOptions(string attributeName, int domainID, int translationID)
		{
			string key = OPTION_CACHEKEY_PREFIX + attributeName + ":" + domainID.ToString() + ":" + translationID.ToString();
			DataTable dt = (DataTable) _Cache[ key ];

			if (dt == null)
			{
				try 
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnShared");
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@AttributeName", SqlDbType.VarChar, ParameterDirection.Input, attributeName);
					client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
					client.AddParameter("@TranslationID", SqlDbType.Int, ParameterDirection.Input, translationID);
					dt = client.GetDataTable("up_AttributeOptionEx_SelectList", CommandType.StoredProcedure);
				} 
				catch (Exception ex)
				{
					throw new MatchnetException("Unable to load options for attribute [" + attributeName +"]", "Option.GetOptions", ex);
				}

				if (dt != null) 
				{
					if ( IsHebrewTranslation(translationID) == true)
					{
						UnicodeTransform(dt); // This will be removed when options are ported to the new pure unicode resource model
					}
					_Cache.Insert( key,
									dt,
									null,
									DateTime.Now.AddSeconds( CACHE_TTL ),
									TimeSpan.Zero,
									CacheItemPriority.High,
									null );
				}
			}
			return dt;
		}

		public string FindContent(DataTable dt, string val)
		{
			string content = string.Empty;

			if (dt.Rows.Count > 0 && val.Length > 0)
			{
				DataView dv = new DataView(dt);
				dv.RowFilter = "Value = " + val;

				if (dv.Count > 0)
				{
					content = dv[0]["Content"].ToString();
				}
			}
			return content;
		}

		 public bool IsHebrewTranslation(int translationID)
		{
			string key = TRANSLATIONINOPTION_CACHEKEY_PREFIX;
			DataTable dt = (DataTable) _Cache[ key ];

			if (dt == null)
			{
				try 
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnShared");
					SQLClient client = new SQLClient(descriptor);
					dt = client.GetDataTable("up_Translation_List", CommandType.StoredProcedure);
				} 
				catch (Exception ex)
				{
					throw new MatchnetException("Unable to load translation in option", "Option.IsHebrewTranslation", ex);
				}
				

				if (dt != null) 
				{
					//Insert into cache
					_Cache.Insert( key,
						dt,
						null,
						DateTime.Now.AddSeconds( CACHE_TTL ),
						TimeSpan.Zero,
						CacheItemPriority.High,
						null );
				}
			}
			foreach ( DataRow row in dt.Rows )
			{
				if (Util.Util.CInt(row["LanguageID"]) == HEBREW_LANGUAGEID && 
					Util.Util.CInt(row["TranslationID"]) == translationID)
				{
					return true;
				}
			}
			return false;
		}


		public static string GetContent(int domainID, int translationID, string attributeName, int selectedValue)
		{
			Option option = new Option();
			option.GetOptions(attributeName, domainID, translationID);
			//return GetContent(privateLabelID, languageID, attributeID, selectedValue);
			return "";
		}


		public static string GetMaskContent(int domainID, int translationID, string attributeName, int maskValue)
		{
			Option option = new Option();
			DataTable dt = option.GetOptions(attributeName, domainID, translationID);

			StringBuilder sb = new StringBuilder();

			foreach (DataRow row in dt.Rows)
			{
				if (row["Value"] != DBNull.Value)
				{
					int optionValue = Convert.ToInt32(row["Value"]);
					if ((maskValue & optionValue) == optionValue)
					{
						if (sb.Length > 0)
						{
							sb.Append(", ");
						}
						sb.Append(row["Content"].ToString());
					}
				}
			}

			return sb.ToString();
		}


		public void UnicodeTransform(DataTable dt)
		{
			mnTranslator.IUnicodeEncoder encoder = new mnTranslator.UnicodeEncoderClass();
			foreach ( DataRow row in dt.Rows )
			{
				string rawVal = row["Content"].ToString();
				if ( rawVal.Length > 0 )
				{
					try
					{
						encoder.SetMbText(rawVal,1255);
						string tempval = encoder.GetUnicodeText();
						row["Content"] = tempval;
					}
					catch(Exception ex)
					{
						System.Diagnostics.Trace.WriteLine(ex);
					}
				}
			}
		}
	}
}
