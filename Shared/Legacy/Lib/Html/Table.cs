using System;
using System.Text;

using Matchnet.Lib.Util;

namespace Matchnet.Lib.Html
{

	/// <summary>
	/// An object that helps in creating Html tables. It maintains
	/// state between beginning tags and appropriately returns ending
	/// tags for each element.
	/// </summary>
	public class Table
	{
		private SCellStyle m_cellStylesHead;
		private SCellStyle m_cellStylesDefault;
		private STableStyle m_tableStylesHead;
		private STableStyle m_tableStylesDefault;
		private STableNode m_tablesHead;
		private bool m_enableIndent;
		private string m_indent;

		/// <summary>
		/// Contructs a new instance
		/// </summary>
		public Table() {
			m_enableIndent = true;
			m_cellStylesHead = null;
			m_cellStylesDefault = null;
			m_tableStylesHead = null;
			m_tableStylesDefault = null;
			m_tablesHead = null;
			m_indent = "";
		}

		/// <summary>
		/// The indentation string that is used. This changes 
		/// dynamically based on how the instance is used.
		/// </summary>
		public string Indent {
			get {
				return m_indent;
			}
		}

		/// <summary>
		/// The default cell style name.
		/// </summary>
		public string DefaultCellStyle {
			get {
				return m_cellStylesDefault == null ? null : m_cellStylesDefault.Name;
			}

			set {
				m_cellStylesDefault = FindCellStyleByName(value);
			}
		}

		/// <summary>
		/// The default table style name.
		/// </summary>
		public string DefaultTableStyle {
			get {
				return m_tableStylesDefault == null ? null : m_tableStylesDefault.Name;
			}

			set {
				m_tableStylesDefault = FindTableStyleByName(value);
			}
		}

		/// <summary>
		/// Defines a cell style
		/// </summary>
		/// <param name="cellProperties">the properties for the cell</param>
		/// <param name="cellStart">the first thing rendered after the opening cell tag</param>
		/// <param name="cellEnd">the last thing rendered before the closing cell tag</param>
		public void DefineCellStyle(string cellProperties, string cellStart, string cellEnd) {
			DefineCellStyle(cellProperties, cellStart, cellEnd, "");
		}

		/// <summary>
		/// Defines a cell style
		/// </summary>
		/// <param name="cellProperties">the properties for the cell</param>
		/// <param name="cellStart">the first thing rendered after the opening cell tag</param>
		/// <param name="cellEnd">the last thing rendered before the closing cell tag</param>
		/// /// <param name="styleName">the name of the style</param>
		public void DefineCellStyle(string cellProperties, string cellStart, string cellEnd, string styleName) 
		{
			SCellStyle cellStyle = FindCellStyleByName(styleName);

			if (cellStyle == null) {
				m_cellStylesHead = new SCellStyle(m_cellStylesHead);
				cellStyle = m_cellStylesHead;
			}
			cellStyle.Properties = SplitProperties(cellProperties);
			cellStyle.Start = cellStart;
			cellStyle.End = cellEnd;
			cellStyle.Name = styleName;
			if (cellStyle.Name.Length == 0 && m_cellStylesDefault == null) {
				m_cellStylesDefault = cellStyle;
			}
		}

		/// <summary>
		/// Defines a table style
		/// </summary>
		/// <param name="properties">The properties for the table</param>
		public void DefineTableStyle(string properties) {
			DefineTableStyle(properties, "");
		}

		/// <summary>
		/// Defines a table style
		/// </summary>
		/// <param name="tableProperties">the properties for the table</param>
		/// <param name="styleName">the name of the style</param>
		public void DefineTableStyle(string tableProperties, string styleName) 
		{
			STableStyle tableStyle = FindTableStyleByName(styleName);

			if (tableStyle == null) {
				m_tableStylesHead = new STableStyle(m_tableStylesHead);
				tableStyle = m_tableStylesHead;
			}
			tableStyle.Name = styleName;
			tableStyle.Properties = SplitProperties(tableProperties);
			
			if (m_tableStylesDefault == null && tableStyle.Name.Length == 0) {
				m_tableStylesDefault = tableStyle;
			}
		}


		/// <summary>
		/// A cell definition
		/// </summary>
		/// <param name="args">properties, startCell, endCell, styleName</param>
		/// <returns>String representing defined table cell</returns>
		public string Cell(params string[] args) 
		{
			string properties = ParamsUtil.ArgVal(args, 0, "");
			string startCell = ParamsUtil.ArgVal(args, 1, "*");
			string endCell = ParamsUtil.ArgVal(args, 2, "*");
			string styleName = ParamsUtil.ArgVal(args, 3, "");

			SCellStyle cellStyle = null;

			string start = "";
			string end = "";
			StringBuilder output = new StringBuilder();

			if (m_tablesHead == null || m_tablesHead.EndRow.Length == 0) {
				return output.ToString();
			} else {
				cellStyle = FindCellStyleByName(styleName);
				
				if (startCell.Length == 1 && startCell == "*") {
					if (cellStyle != null) {
						start = cellStyle.Start;
					}
				} else if (startCell.Length > 0) {
					start = startCell;
				}

				if (endCell.Length == 1 && endCell == "*") {
					if (cellStyle != null) {
						end = cellStyle.End;
					}
				} else if (endCell.Length > 0) {
					end = endCell;
				}

				if (!StringUtil.StringIsEmpty(m_tablesHead.EndCell)) {
					output.Append(m_tablesHead.EndCell.ToString());
					m_tablesHead.EndCell = new StringBuilder();
				}

				if (m_enableIndent) {
					output.Append(m_indent);
					output.Append("  ");
					output.Append("<td");
				} else {
					output.Append("<td");
				}

				if (cellStyle != null) {
					AppendMergedProperties(properties, cellStyle.Properties, output);
				} else {
					if (!StringUtil.StringIsEmpty(properties)) {
						output.Append(" ");
						output.Append(properties.Trim());
					}
				}

				output.Append(">");
				output.Append(start);

				if (!StringUtil.StringIsEmpty(end)) {
					if (m_enableIndent) {
						m_tablesHead.EndCell.Append(m_indent);
						m_tablesHead.EndCell.Append("  ");
					}
					m_tablesHead.EndCell.Append(end);
				}
				if (m_enableIndent) {
					if (StringUtil.StringIsEmpty(end)) {
						m_tablesHead.EndCell.Append(m_indent);
						m_tablesHead.EndCell.Append("  ");
					}
					m_tablesHead.EndCell.Append("</td>\r\n");
				} else {
					m_tablesHead.EndCell.Append("</td>\r\n");
				}
			}
			return output.ToString();
		}
		
		/// <summary>
		/// A row definition that includes a cell
		/// </summary>
		/// <param name="args">properties, startCell, endCell, styleName</param>
		/// <returns>String representing defined row cell</returns>
		public string RowCell(params string[] args) 
		{
			string properties = ParamsUtil.ArgVal(args, 0, "");
			string startCell = ParamsUtil.ArgVal(args, 1, "*");
			string endCell = ParamsUtil.ArgVal(args, 2, "*");
			string styleName = ParamsUtil.ArgVal(args, 3, "");
			
			StringBuilder builder = new StringBuilder();
			builder.Append(Row(null));
			builder.Append("\r\n");
			builder.Append(Cell(properties, startCell, endCell, styleName));
			return builder.ToString();
		}

		/// <summary>
		/// A row definition
		/// </summary>
		/// <returns>String representing a row</returns>
		public string Row() {
			return Row("");
		}

		/// <summary>
		/// A row definition
		/// </summary>
		/// <param name="properties">The properties for the row</param>
		/// <returns>String representing a row</returns>
		public string Row(string properties) {
			StringBuilder output = new StringBuilder();

			if (m_tablesHead == null) {
				return output.ToString();
			} else {
				output.Append(m_tablesHead.EndCell.ToString());
				m_tablesHead.EndCell = new StringBuilder();

				if (!StringUtil.StringIsEmpty(m_tablesHead.EndRow)) {
					if (m_enableIndent) {
						output.Append(m_indent);
					}
					output.Append(m_tablesHead.EndRow.ToString());
					m_tablesHead.EndRow = new StringBuilder();
				}

				if (m_enableIndent) {
					output.Append(m_indent);
				}

				if (StringUtil.StringIsEmpty(properties)) {
					output.Append("<tr>");
				} else {
					output.Append("<tr ");
					output.Append(properties.Trim());
					output.Append(">");
				}

				if (m_enableIndent) {
					output.Append(m_indent);
					m_tablesHead.EndRow.Append("</tr>\r\n");
				} else {
					m_tablesHead.EndRow.Append("</tr>");
				}
				return output.ToString();
			}
		}

		/// <summary>
		/// Begins a table definition
		/// </summary>
		/// <param name="args">properties, form, styleName</param>
		/// <returns>String representing defined begin table</returns>
		public string BeginTable(params string[] args) 
		{
			string properties = ParamsUtil.ArgVal(args, 0, "");
			string form = ParamsUtil.ArgVal(args, 1, "");
			string styleName = ParamsUtil.ArgVal(args, 2, "");

			STableStyle tableStyle = FindTableStyleByName(styleName);
			StringBuilder output = new StringBuilder();
			
			if (m_tablesHead != null) {
				m_indent = m_indent + "  ";
			}

			m_tablesHead = new STableNode(m_tablesHead);

			if (m_enableIndent) {
				//output.Append("\r\n");
				output.Append(m_indent);
			}
			output.Append("<table");

			if (tableStyle != null) {
				AppendMergedProperties(properties, tableStyle.Properties, output);
			} else {
				if (!StringUtil.StringIsEmpty(properties)) {
					output.Append(" ");
					output.Append(properties.Trim());
				}
			}
			output.Append(">");

			if (!StringUtil.StringIsEmpty(form)) {
				m_tablesHead.HasForm = true;
				if (m_enableIndent) {
					output.Append(m_indent);
				}

				output.Append(form);
				if (m_enableIndent) {
					output.Append("\r\n");
				}
			}
			if (m_enableIndent) {
				m_indent = m_indent + "  ";
			}
			return output.ToString();
		}

		/// <summary>
		/// Ends a table definition. 
		/// </summary>
		/// <returns>String represnting the ending tag of the table (closes all open tags inside the table)</returns>
		public string EndTable() {
			StringBuilder output = new StringBuilder();

			if (m_tablesHead == null) {
				return output.ToString();
			} else {
				output.Append(m_tablesHead.EndCell.ToString());
				if (m_tablesHead.EndRow.Length > 0) {
					if (m_enableIndent) {
						output.Append(m_indent);
					}
					output.Append(m_tablesHead.EndRow.ToString());
				}

				if (m_indent.Length > 2) {
					m_indent = m_indent.Substring(1, m_indent.Length - 2);
				} else {
					m_indent = "";
				}

				if (m_enableIndent) {
					output.Append(m_indent);
				}

				if (m_tablesHead.HasForm) {
					output.Append("</form>");
				}
				output.Append("</table>");
				m_tablesHead = m_tablesHead.Pop();
				if (m_indent.Length > 2) {
					m_indent = m_indent.Substring(1, m_indent.Length - 2);
				} else {
					m_indent = "";
				}

				return output.ToString();
			}
		}

		private SCellStyle FindCellStyleByName(string name) {
			if (StringUtil.StringIsEmpty(name)) {
				return m_cellStylesDefault;
			} else {
				SCellStyle currentStyle = m_cellStylesHead;
				while (currentStyle != null) {
					if (currentStyle.Name == name) {
						return currentStyle;
					}
					currentStyle = currentStyle.Next;
				}
				return null;
			}
		}

		private STableStyle FindTableStyleByName(string name) {
			if (StringUtil.StringIsEmpty(name)) {
				return m_tableStylesDefault;
			} else {
				STableStyle currentStyle = m_tableStylesHead;

				while (currentStyle != null) {
					if (currentStyle.Name == name) {
						return currentStyle;
					}
					currentStyle = currentStyle.Next;
				}
				return null;
			}
		}

		private void AppendMergedProperties(string properties, SPropertyNode props, StringBuilder output) {
			SPropertyNode headNode = null;
			SPropertyNode currentDefaultNode = null;
			SPropertyNode currentNode = null;
			int propertyLen = 0;

			if (!StringUtil.StringIsEmpty(properties)) {
				propertyLen = properties.Length;
			}

			if (propertyLen == 0 && props == null) {
				return;
			}

			output.Append(" ");
			
			if (props == null) {
				output.Append(properties.Trim());
				return;
			}

			if (propertyLen > 0) {
				headNode = SplitProperties(properties);
			}

			currentNode = props;

			while (currentNode != null) {
				if (!StringUtil.StringIsEmpty(currentNode.Value) && currentNode.Value != "\"\"") {
					output.Append(currentNode.Name);
					output.Append("=\"");
					output.Append(currentNode.Value);
					output.Append("\"");
				}
				output.Append(" ");
				while (currentDefaultNode != null) {
					if (currentDefaultNode.Name == currentNode.Name) {
						currentDefaultNode.Used = true;
					}
					currentDefaultNode = currentDefaultNode.Next;
				}
				currentNode = currentNode.Next;
			}

			currentDefaultNode = headNode;
			while (currentDefaultNode != null) {
				if (!currentDefaultNode.Used) {
					if (!StringUtil.StringIsEmpty(currentDefaultNode.Value) && currentDefaultNode.Value != "\"\"") {
						output.Append(currentDefaultNode.Name);
						output.Append("=\"");
						output.Append(currentDefaultNode.Value);
						output.Append("\"");
					}
					output.Append(" ");
				} else {
					currentDefaultNode.Used = false;
				}
				currentDefaultNode = currentDefaultNode.Next;
			}
			headNode = null;
		}

		private SPropertyNode SplitProperties(string properties) {
			if (StringUtil.StringIsEmpty(properties)) {
				return null;
			} else {
				String[] props = properties.Split(' ');
				SPropertyNode headNode = null;
				SPropertyNode currentNode = null;

				for (int x = 0; x < props.Length; x++) {
					string currentProp = props[x];

					if (!StringUtil.StringIsEmpty(currentProp)) {
						String[] nameValPair = currentProp.Split("=".ToCharArray(),2);
						if (nameValPair.Length != 2) {
							continue;
						} else {
							SPropertyNode childNode = new SPropertyNode(nameValPair[0], nameValPair[1], null);
							if (headNode == null) {
								headNode = childNode;
							}
							if (currentNode != null) {
								currentNode.Next = childNode;
							}
							currentNode = childNode;
						}
					}
				}
				return headNode;
			}
		}

		/// <summary>
		/// Not used. The method declaration is provided for interface
		/// compatability with the COM object that was used as a basis
		/// for this port.
		/// </summary>
		/// <returns>String stating the method is not used.</returns>
		public string Debug() {
			return "--Debug left in for interface compatability.--";
		}
		
		private class SPropertyNode {
			public string Name;
			public string Value;
			public bool Used;
			public SPropertyNode Next;

			public SPropertyNode(string name, string val, SPropertyNode next) {
				this.Name = name;
				this.Value = FormatValue(val);
				this.Next = next;
			}

			private string FormatValue(string val) {
				val = val.Trim();
				val = val.Replace("\"", "");
				return val;
			}
		}

		private class SCellStyle {
			public string Name;
			public SPropertyNode Properties;
			public string Start;
			public string End;
			public SCellStyle Next;

			public SCellStyle(SCellStyle next) {
				this.Properties = null;
				this.Next = next;
			}
		}
		
		private class STableStyle {
			public string Name;
			public SPropertyNode Properties;
			public STableStyle Next;


			public STableStyle(STableStyle next) {
				this.Properties = null;
				this.Next = next;
			}
		}

		private class STableNode {
			public bool HasForm;
			public StringBuilder EndCell;
			public StringBuilder EndRow;

			private STableNode Next;

			public STableNode(STableNode next) {
				this.HasForm = false;
				this.Next = next;
				this.EndCell = new StringBuilder();
				this.EndRow = new StringBuilder();
			}

			public STableNode Pop() {
				STableNode next = this.Next;
				this.Next = null;
				return next;
			}
		}
	}
}
