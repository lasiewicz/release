using System;
using System.Collections;

namespace Matchnet.Lib.Util
{
	/// <summary>
	/// Simple date utility class.
	/// </summary>
	public class DateUtil
	{
		private static Hashtable _Months = initializeMonths();
		
		private DateUtil()
		{
		}

		private static Hashtable initializeMonths()
		{
			Hashtable hash = new Hashtable();
			
			hash.Add(1, "January");
			hash.Add(2, "February");
			hash.Add(3, "March");
			hash.Add(4, "April");
			hash.Add(5, "May");
			hash.Add(6, "June");
			hash.Add(7, "July");
			hash.Add(8, "August");
			hash.Add(9, "September");
			hash.Add(10, "October");
			hash.Add(11, "November");
			hash.Add(12, "December");

			return hash;
		}

		public static string getMonth(int index)
		{
			return _Months[index].ToString();
		}
	}
}
