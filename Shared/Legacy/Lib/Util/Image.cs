using System;
using System.Drawing;
using System.IO;

namespace Matchnet.Lib.Util
{
	/// <summary>
	/// This class will contain utility methods for manipulating images
	/// </summary>
	public class Image {
		/// <summary>
		/// Converts an image to JPEG format
		/// </summary>
		/// <param name="data">Byte array containing the contents of the image to be converted</param>
		/// <returns></returns>
		public byte[] JpegEncode(byte[] data) {
			byte[] output;
			System.Drawing.Image img = null;
			try {
				// Load stream with data from array
				System.IO.MemoryStream stream = new MemoryStream();
				stream.Write(data, 0, data.Length);
				
				// Load image with data from stream
				img = System.Drawing.Image.FromStream(stream);

				// Save image in JPEG format into stream
				stream = new MemoryStream();
				img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
				
				// return array
				output = stream.GetBuffer();

				// return JPEG contents
				return output;
			}
			catch(Exception e) {
				throw e;
			} finally {
				if(img != null) {
					img.Dispose();
				}
			}
		}

		/*
		    ------------ VB.Net snippet for a better way to do this ------------------ 
		 
            imgThumb = Image.FromFile(Filename)

            If imgThumb.Height > imgThumb.Width Then
                ratio = imgThumb.Height / imgThumb.Width
                sizeX = 120 / ratio
                sizeY = 120
            Else
                ratio = imgThumb.Width / imgThumb.Height
                sizeX = 120
                sizeY = 120 / ratio
            End If

            bmThumb = New Bitmap(sizeX, sizeY, PixelFormat.Format24bppRgb)
            bmThumb.SetResolution(imgThumb.HorizontalResolution, imgThumb.VerticalResolution)

            Dim grThumb As Graphics

            grThumb = Graphics.FromImage(bmThumb)
            grThumb.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic

            grThumb.DrawImage(imgThumb, New Rectangle(0, 0, sizeX, sizeY), New Rectangle(0, 0, imgThumb.Width, imgThumb.Height), GraphicsUnit.Pixel)
            grThumb.Dispose()

            'imgThumb.Dispose()
            bmThumb.Save(thumbFile, System.Drawing.Imaging.ImageFormat.Jpeg)
            bmThumb.Dispose()
		
		 */	
	}
}


