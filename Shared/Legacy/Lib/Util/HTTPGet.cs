using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Net;
using System.IO;
using System.Text;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;

namespace Matchnet.Lib.Util {

	/// <summary>
	/// Performs a simple HTTP Get request.
	/// </summary>
	public class HTTPGet 
	{
		/// <summary>
		/// Performs a Get request to a valid URL. The URL can 
		/// use either the Http or Https protocol.
		/// </summary>
		/// <param name="url">The fully qualified URL</param>
		/// <param name="timeout">The max allowable timeout</param>
		/// <returns>The response to the request</returns>
		public string Get(string url, int timeout) {
			if(url.StartsWith("https")) {
				//ignore invalid/expired SSL cert
				ServicePointManager.CertificatePolicy = new OpenCertificatePolicy();
			}

			WebRequest req = WebRequest.Create(url);
			req.Timeout = timeout;
			try {
				WebResponse result = req.GetResponse();
				return result.GetResponseStream().ToString();
			} catch (WebException ex) {
				Matchnet.Lib.Exceptions.MatchnetException mex;
				if (ex.Status == WebExceptionStatus.Timeout) {
					mex = new Matchnet.Lib.Exceptions.MatchnetException("URL Request timed out","Get",ex);
				} else {
					mex = new Matchnet.Lib.Exceptions.MatchnetException("Unexpected error","Get",ex);
				}
				throw mex;
			}

		}

		/// <summary>
		/// An implementation of the System.Net.ICertificatePolicy interface that
		/// ignores Https certificate problem.
		/// </summary>
		public class OpenCertificatePolicy : System.Net.ICertificatePolicy 
		{
			/// <summary>
			/// Returns true regardless of Https messages
			/// </summary>
			/// <param name="srvPoint">The ServicePoint that handles the connection</param>
			/// <param name="certificate">The certificate to validate</param>
			/// <param name="request">The request that received the certificate</param>
			/// <param name="certificateProblem">The problem encountered when using the certificate</param>
			/// <returns>true regardless of certificate problem</returns>
			public bool CheckValidationResult(ServicePoint srvPoint,
				X509Certificate certificate, WebRequest request, int
				certificateProblem) {
				return true;
			}
		}

	}
}
