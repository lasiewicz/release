using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Net;
using System.IO;
using System.Text;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;

using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Util
{
	public class HttpUtil
	{
		/// <summary>
		/// Performs a Get request to a valid URL. The URL can 
		/// use either the Http or Https protocol.
		/// </summary>
		/// <param name="url">The fully qualified URL</param>
		/// <param name="timeout">The max allowable timeout</param>
		/// <returns>The response to the request</returns>
		public static string Get(string url, int timeout) 
		{
			if(url.StartsWith("https")) 
			{
				//ignore invalid/expired SSL cert
				ServicePointManager.CertificatePolicy = new OpenCertificatePolicy();
			}

			WebRequest req = WebRequest.Create(url);
			req.Timeout = timeout;
			try 
			{
				WebResponse result = req.GetResponse();
				return result.GetResponseStream().ToString();
			} 
			catch (WebException ex) 
			{
				Matchnet.Lib.Exceptions.MatchnetException mex;
				if (ex.Status == WebExceptionStatus.Timeout) 
				{
					mex = new Matchnet.Lib.Exceptions.MatchnetException("URL Request timed out","Get",ex);
				} 
				else 
				{
					mex = new Matchnet.Lib.Exceptions.MatchnetException("Unexpected error","Get",ex);
				}
				throw mex;
			}

		}

		public static string PostData(string url, string data, string referer) 
		{
			return PostData(url, data, referer, System.Threading.Timeout.Infinite);
		}

		public static string PostData(string url, string data, string referer, int timeout) 
		{
			try 
			{
				if(url.StartsWith("https")) 
				{
					//ignore invalid/expired SSL cert
					ServicePointManager.CertificatePolicy = new OpenCertificatePolicy();
				}
				
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				//request.UserAgent = "Matchnet.Lib.Util.HttpUtil";
				request.Method = "POST";
				request.Timeout = timeout;

				if (referer != null && referer.Trim().Length > 0) 
				{
					request.Referer = referer;
				}

				request.ContentType = "multipart/form-data";
				//request.ContentType = "application/x-www-form-urlencoded";
				request.ContentLength = data.Length;

				byte[] postData = System.Text.Encoding.UTF8.GetBytes(data);
				Stream requestStream = request.GetRequestStream();

				requestStream.Write(postData, 0, data.Length);
				requestStream.Close();

				WebResponse response = request.GetResponse();
				StreamReader reader = new StreamReader(response.GetResponseStream());
				
				string retVal = reader.ReadToEnd();
				try {
					reader.Close();
					response.Close();
				} catch {} 
				return retVal;
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to post data", "HttpUtil.PostData", ex);
			}
		}

		/// <summary>
		/// An implementation of the System.Net.ICertificatePolicy interface that
		/// ignores Https certificate problem.
		/// </summary>
		public class OpenCertificatePolicy : System.Net.ICertificatePolicy 
		{
			/// <summary>
			/// Returns true regardless of Https messages
			/// </summary>
			/// <param name="srvPoint">The ServicePoint that handles the connection</param>
			/// <param name="certificate">The certificate to validate</param>
			/// <param name="request">The request that received the certificate</param>
			/// <param name="certificateProblem">The problem encountered when using the certificate</param>
			/// <returns>true regardless of certificate problem</returns>
			public bool CheckValidationResult(ServicePoint srvPoint,
				X509Certificate certificate, WebRequest request, int
				certificateProblem) 
			{
				return true;
			}
		}
	}
}
