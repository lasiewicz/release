using System;
using System.Collections.Specialized;

namespace Matchnet.Lib.Util
{
	/// <summary>
	/// Summary description for NameObjectCollection.
	/// </summary>
	public class NameObjectCollection : NameObjectCollectionBase
	{
		public NameObjectCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public Object this[ int index ]  
		{
			get  
			{
				return( this.BaseGet( index ) );
			}
			set  
			{
				this.BaseSet( index, value );
			}

		}


		// Gets or sets the value associated with the specified key.
		public Object this[ String key ]  
		{
			get  
			{
				return( this.BaseGet( key ) );
			}
			set  
			{
				this.BaseSet( key, value );
			}
		}

		// Gets a String array that contains all the keys in the collection.
		public String[] AllKeys  
		{
			get  
			{
				return( this.BaseGetAllKeys() );
			}
		}

		// Gets an Object array that contains all the values in the collection.
		public Array AllValues  
		{
			get  
			{
				return( this.BaseGetAllValues() );
			}
		}

		// Gets a String array that contains all the values in the collection.
		public String[] AllStringValues  
		{
			get  
			{
				return( (String[]) this.BaseGetAllValues( Type.GetType( "System.String" ) ) );
			}
		}

		// Gets a value indicating if the collection contains keys that are not null.
		public Boolean HasKeys  
		{
			get  
			{
				return( this.BaseHasKeys() );
			}
		}

		// Adds an entry to the collection.
		public void Add( String key, Object value )  
		{
			this.BaseAdd( key, value );
		}

		// Removes an entry with the specified key from the collection.
		public void Remove( String key )  
		{
			this.BaseRemove( key );
		}

		// Removes an entry in the specified index from the collection.
		public void Remove( int index )  
		{
			this.BaseRemoveAt( index );
		}

		// Clears all the elements in the collection.
		public void Clear()  
		{
			this.BaseClear();
		}
	}
}
