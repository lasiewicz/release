using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Configuration;

using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Threading;
using Matchnet.Lib.Config;

namespace Matchnet.Lib.Util
{
	/// <summary>
	/// Summary description for SuspectWordFinder.
	/// </summary>
	public class SuspectWordFinder
	{
		public SuspectWordFinder()
		{
		}
		
		private bool _IsInitialized=false;
		private Regex suspectWordsPattern;
		private Regex bannedWordsPattern;
		private Regex cleanWhiteSpacePattern;
        
		public void Initialize(int languageId)
		{
			if ( _IsInitialized )
				return;

			string bannedExpression="";;
			ListSectionData bannedWordsSection = (ListSectionData)ConfigurationSettings.GetConfig("bannedWordsSection");
			
			foreach( string wordValue in bannedWordsSection.List )
			{
				bannedExpression = bannedExpression  + "(" + wordValue +")|";
			}
			bannedWordsPattern = new Regex(bannedExpression.Substring(0,bannedExpression.Length-1),RegexOptions.IgnoreCase);


			string suspectExpression="";
			ListSectionData suspectWordsSection = (ListSectionData)ConfigurationSettings.GetConfig("suspectWordsSection");
			foreach( string wordValue in suspectWordsSection.List )
			{
				suspectExpression = suspectExpression  + "(" + wordValue +")|";
			}
			suspectWordsPattern = new Regex(suspectExpression.Substring(0,suspectExpression.Length-1),RegexOptions.IgnoreCase);

			cleanWhiteSpacePattern = new Regex(@"\s+",RegexOptions.IgnoreCase);

			_IsInitialized = true;
		}


		public string CleanBannedWords(string source, int languageId)
		{
			Initialize(languageId);
		
			string rawCleaned = bannedWordsPattern.Replace(source," ");
			return cleanWhiteSpacePattern.Replace(rawCleaned," ");	
		}

		public bool ContainsBannedWords(string source, int languageID)
		{
			Initialize(languageID);

			MatchCollection matchList = bannedWordsPattern.Matches(source);

			if (matchList.Count > 0)
				return true;
			else
				return false;
		}

		public bool ContainsSuspectWords(string source, int languageId)
		{
			Initialize(languageId);

			MatchCollection matchList = suspectWordsPattern.Matches(source);

			if ( matchList.Count > 0 )
				return true;
			else
				return false;
		}
	}
}
