using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

namespace Matchnet.Lib.Util
{
	/// <summary>
	/// Provides basic String utilities
	/// </summary>
	public class StringUtil
	{

		private const string CHARS_ALPHA = "97,98,99,100,101,102,103,104,105,106,107,108,109,110,112,113,114,115,116,117,118,119,120,121,122"; //a-z execpt for o
		private const string CHARS_NUM = "49,50,51,52,53,54,55,56,57"; //1-9

		/// <summary>
		/// Different types of random strings
		/// </summary>
		public enum RandomStringType
		{
			/// <summary>
			/// Numeric character only string
			/// </summary>
			StringTypeNumeric,
			/// <summary>
			/// Alphanumeric character string
			/// </summary>
			StringTypeAlphaNumeric
		}

		public bool EqualsIgnoreCase(string a, string b) 
		{
			return CaseInsensitiveComparer.Default.Compare(a, b) == 0;
		}
		
		/// <summary>
		/// Generates a random string of the specified length, no more than two alpha characters will appear in a row
		/// </summary>
		/// <param name="length">string length</param>
		/// <param name="stringType">enum - alphanumeric (without O or zero), or numeric (without zero)</param>
		/// <returns>random string</returns>
		/*
		public string GenerateRandomString(int length, RandomStringType stringType)
		{
			StringBuilder sbResult;
			int numChar;
			Random r;

			sbResult = new StringBuilder();
			r = new Random();

			switch (stringType)
			{
				case RandomStringType.StringTypeNumeric:
					for (numChar = 0; numChar < length; numChar++)
					{
						sbResult.Append(r.Next(1,9).ToString());
					}
					break;
				case RandomStringType.StringTypeAlphaNumeric:
					int charCode = 0;
					int countAlpha = 0;
					int countNum = 0;
					Array arrAlpha = CHARS_ALPHA.Split(new char[] {','});
					Array arrNum = CHARS_NUM.Split(new char[] {','});
					int lenAlpha = arrAlpha.Length;
					int lenNum = arrNum.Length;
		
					for (numChar = 0; numChar < length; numChar++)
					{
						if (false)
						{
							if (countNum >= 2 | (countAlpha < 2 && (r.Next(1,10) % 2) == 0))
							{
								charCode = int.Parse(arrAlpha.GetValue(r.Next(0,lenAlpha - 1)).ToString());
								countAlpha++;
								countNum = 0;
							}
							else
							{
								charCode = int.Parse(arrNum.GetValue(r.Next(0,lenNum - 1)).ToString());
								countNum++;
								countAlpha = 0;
							}
						}
						sbResult.Append((char)charCode);
					}
					break;
			}

			r = null;

			return sbResult.ToString();
		}
		*/

		/// <summary>
		/// Determines whether or not the given string is empty
		/// </summary>
		/// <param name="val">The string to evaluate</param>
		/// <returns>Whether or not the given string is empty</returns>
		public static bool StringIsEmpty(string val) 
		{
			if (val == null || val.Trim().Length == 0 || val == String.Empty) 
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Determines whether or not the contents of a StringBuilder is empty
		/// </summary>
		/// <param name="val">The StringBuilder instance to evaluate</param>
		/// <returns>Whether or not the contents of the StringBuilder is empty</returns>
		public static bool StringIsEmpty(StringBuilder val) 
		{
			if (val == null) 
			{
				return true;
			}
			return StringIsEmpty(val.ToString());
		}

		/// <summary>
		/// Returns the byte array representation of a string
		/// </summary>
		/// <param name="text">The string to return a byte array representation for</param>
		/// <returns>The byte array representation of the string</returns>
		public static byte[] GetBytes(string text) 
		{
			char[] chars = text.ToCharArray();
			byte[] bytes = new byte[chars.Length];
			for (int x = 0; x < chars.Length; x++) 
			{
				bytes[x] = (byte)chars[x];
			}
			return bytes;
		}

		public static string DateTimeString(DateTime date)
		{
			return DateTimeString(date, string.Empty);
		}
		
		public static string DateTimeString(DateTime date, string format)
		{
			try
			{
				if (date == DateTime.MinValue)
					return "";
				else
					return date.ToString(format);
			}
			catch(Exception)
			{
				return "";
			}
		}

		public static string NumberString(int number)
		{
			return NumberString(number, string.Empty);
		}

		public static string NumberString(int number, string format)
		{
			try
			{
				if (number == Matchnet.Constants.NULL_INT)
					return "";
				else
					return number.ToString(format);
			}
			catch(Exception)
			{
				return "";
			}
		}

		public static string MaxWordLength(string input, int maxLength)
		{
			Regex regEx = new Regex(@"(\S{" + maxLength.ToString() + @"})(\S{1," + maxLength.ToString() + @"})");
			return regEx.Replace(input, "$1 $2 ");
		}

		public static string HebrewToUTF(string str)
		{
			byte[] bufferHebrew = new byte[str.Length];
			for(int k = 0; k < str.Length; k++)
				bufferHebrew[k] = (byte)str[k];

			System.Text.Encoding encHebrew = System.Text.Encoding.GetEncoding(1255); //hebrew encoding
			System.Text.Encoding encUnicode = System.Text.Encoding.GetEncoding("utf-8");

			byte[] bufferUTF = System.Text.Encoding.Convert(encHebrew, encUnicode, bufferHebrew);
			return encUnicode.GetString(bufferUTF);
		}
	}
}
