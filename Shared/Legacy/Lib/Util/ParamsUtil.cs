using System;

namespace Matchnet.Lib.Util
{
	/// <summary>
	/// Provides utilities for handling optional parameters
	/// </summary>
	public class ParamsUtil
	{
		private static bool EmptyArgs(String[] args) {
			return (args == null || args.Length == 0);
		}

		/// <summary>
		/// Returns the value of the argument at a given position
		/// </summary>
		/// <param name="args">the optional arguments</param>
		/// <param name="ndx">the index of the argument to get a value for</param>
		/// <param name="defaultVal">the default value to use when no value exists in the passed in arguments</param>
		/// <returns>The value of the argument at a given position</returns>
		public static string ArgVal(string[] args, int ndx, string defaultVal) {
			if (EmptyArgs(args)) {
				return defaultVal;
			}
			if (ndx > (args.Length - 1) || ndx < 0) {
				return defaultVal;
			}
			return StringUtil.StringIsEmpty(args[ndx]) ? defaultVal : args[ndx];
		}
	}
}
