using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Collections;
using System.Xml;
using System.IO;

using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Threading;
using Matchnet.Lib.Config;

namespace Matchnet.Lib.Util
{
	public sealed class FreeTextApproval
	{
		#region Variables
		static readonly object _Padlock = new object();
		static FreeTextApproval _Instance = null;
		static Hashtable _List = new Hashtable();
		static Regex cleanWhiteSpacePattern = new Regex(@"\s+",RegexOptions.IgnoreCase);
		static string _FilePath = "";
		static bool _Locked = false;
		#endregion

		public static FreeTextApproval GetInstance()
		{
			lock (_Padlock)
			{
				if (_Instance == null)
					_Instance = new FreeTextApproval();
				return _Instance;
			}
		}

		public void Load()
		{
			Load(_FilePath);
		}
		
		public void Load(string filePath)
		{
			_FilePath = filePath;

			if (!File.Exists(filePath))
				throw new IOException(filePath + " not found.");

			XmlDocument doc = new XmlDocument();
			XmlTextReader reader = new XmlTextReader(_FilePath);
			reader.WhitespaceHandling = WhitespaceHandling.None;
			doc.Load(reader);
		
			XmlElement root = doc.DocumentElement;
			foreach(XmlNode node in root.ChildNodes)
			{
				string sectionName = node.Name;
				XmlNode parentNode;
				try
				{
					XmlNodeList nodeList = root.GetElementsByTagName(sectionName);
					parentNode = nodeList[0];
				}
				catch(Exception ex)
				{
					throw new Exception("Section not found", ex);
				}
				string expression = "";
		
				foreach(XmlNode wordNode in parentNode.ChildNodes)
				{
					string nodeValue = wordNode.Attributes["value"].Value;
					expression += "(" + nodeValue + ")|";
				}
				
				Regex pattern;
				try
				{
					pattern = new Regex(expression.Substring(0, expression.Length - 1),  RegexOptions.IgnoreCase);
				}
				catch(ArgumentOutOfRangeException ex)
				{
					throw new Exception("Empty Section. No patterns found. File Path=" + filePath, ex);
				}
				
				lock (_Padlock)
				{
					_Locked = true;
					if ( _List.ContainsKey(sectionName))
						_List.Remove(sectionName);

					_List.Add(sectionName, pattern);
					_Locked = false;
				}
				
			}
		
			reader.Close();
		}

		/// <summary>
		/// Returns Regex pattern
		/// </summary>
		/// <param name="sectionName"></param>
		/// <returns></returns>
		public Regex GetRegex(string sectionName)
		{
			while (_Locked)
			{
			}
			Regex pattern = (Regex) _List[sectionName];
			return pattern;
		}

		/// <summary>
		/// Replace any patterns that match with blank spaces
		/// </summary>
		/// <param name="sectionName">"bannedWords", "suspectWords"</param>
		/// <param name="text"></param>
		/// <returns>Cleaned text</returns>
		public string Clean(string sectionName, string text)
		{
			while (_Locked)
			{
			}
			Regex pattern = (Regex) _List[sectionName];
			string cleaned = pattern.Replace(text, " ");
			return cleanWhiteSpacePattern.Replace(cleaned," ");	
		}

		/// <summary>
		/// Test to see if given text contains any patterns that match
		/// </summary>
		/// <param name="sectionName">"bannedWords", "suspectWords"</param>
		/// <param name="text"></param>
		/// <returns>True if there are any matching patterns</returns>
		public bool Contains(string sectionName, string text)
		{
			while (_Locked)
			{
			}
			Regex pattern = (Regex) _List[sectionName];
			MatchCollection matchList = pattern.Matches(text);
			if (matchList.Count > 0)
				return true;
			else
				return false;
		}
	}
}
