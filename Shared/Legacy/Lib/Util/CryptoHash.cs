using System;
using System.Text;

namespace Matchnet.Lib.Util
{
	/// <summary>
	/// CryptoHash is a base class for cryptographic hashing functions
	/// </summary>
	public abstract class CryptoHash
	{
		const int CHRSZ = 8;		// 16 for unicode
		const bool LCHEX = true;	// false for uppercase hex chars
		const string B64PAD = "";	// "=" for strict RFC 3548 compliance

		/// <summary>
		/// Add integers, wrapping at 2^32. This uses 16-bit operations internally
		/// to work around bugs in some JS interpreters.
		/// Yes, this is C#, but better safe than sorry!
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		protected Int32 safe_add(Int32 x, Int32 y)
		{
			Int32 lsw = (x & 0xFFFF) + (y & 0xFFFF);
			Int32 msw = (x >> 16) + (y >> 16) + (lsw >> 16);
			return (msw << 16) | (lsw & 0xFFFF);
		}

		/// <summary>
		/// Bitwise rotate a 32-bit number to the left.
		/// </summary>
		/// <param name="num"></param>
		/// <param name="cnt"></param>
		/// <returns></returns>
		protected Int32 rol(Int32 num, Int32 cnt)
		{
			// NOTE: uint cast in following line is important!
			// Unsigned shift required here!
			return (num << cnt) | (Int32)( ((uint)num) >> (32 - cnt));
		}

		/// <summary>
		/// Convert ASCII string to an array of little-endian words
		/// </summary>
		/// <param name="str"></param>
		/// <returns>Array of little-endian 4 byte ints</returns>
		public Int32[] str2binl(string str)
		{
			int dim = (str.Length * CHRSZ) >> 5;
			if (dim < 16) dim = 16;
			Int32[] bin = new Int32[dim];
			Int32 mask = (1 << CHRSZ) - 1;
			for (int i = 0; i < str.Length * CHRSZ; i += CHRSZ) 
			{
				bin[i>>5] |= (str[i/CHRSZ] & mask) << (i%32);
			}
			return bin;
		}

		/// <summary>
		/// Convert array of little-endian words to ASCII encoded string
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public string binl2str(Int32[] data)
		{
			StringBuilder sb = new StringBuilder();
			Int32 mask = (1 << CHRSZ) - 1;
			for (int i = 0; i < data.Length * 32; i += CHRSZ) 
			{
				// NOTE: uint cast in following line is important!
				// Unsigned shift required here!
				byte[] onechar = { (byte)(( ((uint)(data[i>>5])) >> (i%32) ) & mask) };
				sb.Append(Encoding.ASCII.GetChars(onechar));
			}
			return sb.ToString();
		}

		/// <summary>
		/// Convert array of little-endian words to Base 64 encoded string
		/// Uses "url safe" Base 64 alphabet as specified in RFC 3548,
		/// substituting - for + and _ for / at the end of the alphabet.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public string binl2b64(Int32[] data)
		{
			StringBuilder sb = new StringBuilder();
			string tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
			for(int i = 0; i < data.Length * 4; i += 3)
			{
				int triplet = (((data[i   >> 2] >> 8 * ( i   %4)) & 0x0FF) << 16)
							| (((data[i+1 >> 2] >> 8 * ((i+1)%4)) & 0x0FF) << 8 )
							|  ((data[i+2 >> 2] >> 8 * ((i+2)%4)) & 0x0FF);
				for(int j = 0; j < 4; j++)
				{
					if(i * 8 + j * 6 > data.Length * 32) sb.Append(B64PAD);
					else sb.Append(tab[(triplet >> 6*(3-j)) & 0x03F]);
				}
			}
			return sb.ToString();
		}

		/// <summary>
		/// Convert array of little-endian words to hexadecimal encoded string
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public string binl2hex(Int32[] data)
		{
			string hex_tab = LCHEX ? "0123456789abcdef" : "0123456789ABCDEF";
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < data.Length * 4; i++) 
			{
				sb.Append(hex_tab[(data[i>>2] >> ((i%4)*8+4)) & 0x0F]);
				sb.Append(hex_tab[(data[i>>2] >> ((i%4)*8  )) & 0x0F]);
			}
			return sb.ToString();
		}

		/// <summary>
		/// Hash a string to an array of little-endian words
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public Int32[] Hash(string str)
		{
			return Hash(str2binl(str), str.Length * CHRSZ);
		}

		/// <summary>
		/// Hash a string to an ASCII encoded hash string
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public string ASCIIHash(string str)
		{
			return binl2str(Hash(str));
		}


		/// <summary>
		/// Hash a string to a Base 64 encoded hash string
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public string Base64Hash(string str)
		{
			return binl2b64(Hash(str));
		}

		/// <summary>
		/// Hash a string to a hexadecimal encoded hash string
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public string HexHash(string str)
		{
			return binl2hex(Hash(str));
		}

		/// <summary>
		/// Produce a hash value based on an array of little-endian words
		/// </summary>
		/// <param name="data"></param>
		/// <param name="bitlen">length, in bits, of data</param>
		/// <returns>array containing hash</returns>
		public abstract Int32[] Hash(Int32[] data, int bitlen);
	}

	/// <summary>
	/// RSA Data Security, Inc. MD5 Message Digest Algorithm,
	/// as defined in RFC 1321 (http://www.faqs.org/rfcs/rfc1321.html)
	/// </summary>
	public class MD5Hash : CryptoHash
	{
		public static string[] RFC1321_TEST_INPUT = 
		{
			"",
			"a",
			"abc",
			"message digest",
			"abcdefghijklmnopqrstuvwxyz",
			"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
			"12345678901234567890123456789012345678901234567890123456789012345678901234567890"
		};
		public static string[] RFC1321_TEXT_OUTPUT_HEX = 
		{
			"d41d8cd98f00b204e9800998ecf8427e",
			"0cc175b9c0f1b6a831c399e269772661",
			"900150983cd24fb0d6963f7d28e17f72",
			"f96b697d7cb7938d525a2f31aaf161d0",
			"c3fcd3d76192e4007dfb496cca67e13b",
			"d174ab98d277d9f5a5611c2c9f419d9f",
			"57edf4a22be3c955ac49da2e2107b67a"
		};

		Int32 md5_cmn(Int32 q, Int32 a, Int32 b, Int32 x, Int32 s, Int32 t)
		{
			return safe_add(rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
		}
		Int32 md5_ff(Int32 a, Int32 b, Int32 c, Int32 d, Int32 x, Int32 s, Int32 t)
		{
			return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
		}
		Int32 md5_gg(Int32 a, Int32 b, Int32 c, Int32 d, Int32 x, Int32 s, Int32 t)
		{
			return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
		}
		Int32 md5_hh(Int32 a, Int32 b, Int32 c, Int32 d, Int32 x, Int32 s, Int32 t)
		{
			return md5_cmn(b ^ c ^ d, a, b, x, s, t);
		}
		Int32 md5_ii(Int32 a, Int32 b, Int32 c, Int32 d, Int32 x, Int32 s, Int32 t)
		{
			return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
		}

		/// <summary>
		/// Compute 128-bit MD5 hash
		/// </summary>
		/// <param name="x"></param>
		/// <param name="len">length in bits of x</param>
		/// <returns></returns>
		public override Int32[] Hash(Int32[] x, int len)
		{
			// Ensure array is large enough for padding & iterations
			// iLen is offset into data to write length
			int iLen = (int)(( ((uint)(len + 64)) >> 9) << 4) + 14;
			int xLen = iLen;
			int xtra = 16 - (xLen % 16);
			if (xtra != 16) 
			{
				xLen += xtra;
			}
			if (x.Length < xLen)
			{
				Int32[] y = x;
				x = new Int32[xLen];
				y.CopyTo(x, 0);
			}
			/* append padding */
			x[len >> 5] |= 0x80 << ((len) % 32);
			// NOTE: uint cast in following line is important!
			// Unsigned shift required here!
			x[iLen] = len;

			Int32 a =  1732584193;
			Int32 b = -271733879;
			Int32 c = -1732584194;
			Int32 d =  271733878;

			for(int i = 0; i < x.Length; i += 16)
			{
				Int32 olda = a;
				Int32 oldb = b;
				Int32 oldc = c;
				Int32 oldd = d;

				a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
				d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
				c = md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);
				b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
				a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
				d = md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);
				c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
				b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
				a = md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
				d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
				c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
				b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
				a = md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);
				d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
				c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
				b = md5_ff(b, c, d, a, x[i+15], 22,  1236535329);

				a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
				d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
				c = md5_gg(c, d, a, b, x[i+11], 14,  643717713);
				b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
				a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
				d = md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);
				c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
				b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
				a = md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
				d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
				c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
				b = md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);
				a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
				d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
				c = md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);
				b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);

				a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
				d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
				c = md5_hh(c, d, a, b, x[i+11], 16,  1839030562);
				b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
				a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
				d = md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);
				c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
				b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
				a = md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);
				d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
				c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
				b = md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);
				a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
				d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
				c = md5_hh(c, d, a, b, x[i+15], 16,  530742520);
				b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);

				a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
				d = md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);
				c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
				b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
				a = md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);
				d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
				c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
				b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
				a = md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
				d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
				c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
				b = md5_ii(b, c, d, a, x[i+13], 21,  1309151649);
				a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
				d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
				c = md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);
				b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);

				a = safe_add(a, olda);
				b = safe_add(b, oldb);
				c = safe_add(c, oldc);
				d = safe_add(d, oldd);
			}
			return new Int32[] { a, b, c, d };
		}
	}

}
