using System;
using System.Diagnostics;
using Matchnet.Lib;

namespace Matchnet.Lib.Util
{
	/// <summary>
	/// General utilities
	/// </summary>
	public class Util
	{
		public const double ONE_GRAM_IN_POUNDS = 0.00220462262;
		public const double ONE_GRAM_IN_STONE = 0.000157473044;

		/// <summary>
		/// Bit operations used for dynamically determining
		/// how to compare values.
		/// </summary>
		public enum menmOperatorIDs
		{
			/// <summary>
			/// No opereration
			/// </summary>
			opNone = 0,
			/// <summary>
			/// SQL Between
			/// </summary>
			opBetween = 1,
			/// <summary>
			/// SQL In
			/// </summary>
			opIn = 2,
			/// <summary>
			/// SQL Like
			/// </summary>
			opLike = 3,
			/// <summary>
			/// Less than
			/// </summary>
			opLessThan = 4,
			/// <summary>
			/// Greater than
			/// </summary>
			opGreaterThan = 5,
			/// <summary>
			/// Less than or equal to
			/// </summary>
			opLessThanEqual = 6,
			/// <summary>
			/// Greater than or equal to
			/// </summary>
			opGreaterThanEqual = 7,
			/// <summary>
			/// Equal to
			/// </summary>
			opEqual = 8,
			/// <summary>
			/// Not equal to
			/// </summary>
			opNotEqual = 9,
			/// <summary>
			/// Bitwise And
			/// </summary>
			opBitwiseAnd = 100,
			/// <summary>
			/// Bitwise Or
			/// </summary>
			opBitwiseOr = 101,
			/// <summary>
			/// Bitwise 2's complement
			/// </summary>
			opBitwise2sComplement = 102,
			/// <summary>
			/// Radius range (search)
			/// </summary>
			opRadiusRange = 200,
		}

		/// <summary>
		/// Converts a string to an integer
		/// </summary>
		/// <param name="strValue">the string to convert</param>
		/// <param name="iDefaultValue">the default value when the string is not numeric</param>
		/// <returns>the integer representation of the string</returns>
		public static int CInt(string strValue, int iDefaultValue)
		{
			if ( strValue == null )
				return iDefaultValue;

			if (! IsNumeric(strValue))
				return iDefaultValue;
			else
				return System.Convert.ToInt32(strValue);
		}


		public static double CDouble(Object objValue, double DefaultValue)
		{
			if ( objValue == null )
				return DefaultValue;

			try
			{
				return System.Convert.ToDouble(objValue);
			}
			catch (Exception)
			{
				return DefaultValue;
			}
		}

		public static double CDouble(Object objValue)
		{
			return CDouble(objValue, ConstantsTemp.NULL_DOUBLE);
		}


		/// <summary>
		/// Converts a object to an integer
		/// </summary>
		/// <param name="objValue">the object to convert</param>
		/// <returns>the integer representation of the object</returns>
		public static int CInt(Object objValue)
		{
			return CInt(objValue, Matchnet.Constants.NULL_INT);
		}

		/// <summary>
		/// Converts a object to an integer
		/// </summary>
		/// <param name="objValue">the object to convert</param>
		/// <param name="iDefaultValue">the default value when the object is not numeric</param>
		/// <returns>the integer representation of the object</returns>
		public static int CInt(Object objValue, int iDefaultValue)
		{
			if ( objValue == null )
				return iDefaultValue;

			try
			{
				return System.Convert.ToInt32(objValue);
			}
			catch (Exception)
			{
				return iDefaultValue;
			}
		}

		/// <summary>
		/// Converts a object to a string
		/// </summary>
		/// <param name="objValue">the object to convert</param>
		/// <returns>the string representation of the object</returns>
		public static string CString(Object objValue)
		{
			return CString(objValue, Matchnet.Constants.NULL_STRING);
		}

		/// <summary>
		/// Converts a object to a string
		/// </summary>
		/// <param name="objValue">the object to convert</param>
		/// <param name="iDefaultValue">the default value when the object can not coverted to string</param>
		/// <returns>the string representation of the object</returns>
		public static string CString(Object objValue, string strDefaultValue)
		{
			if (objValue == null)
				return strDefaultValue;

			try
			{
				return System.Convert.ToString(objValue);
			}
			catch (Exception)
			{
				return strDefaultValue;
			}

		}

		/// <summary>
		/// Converts a object to a long
		/// </summary>
		/// <param name="objValue">the object to convert</param>
		/// <returns>the long representation of the object</returns>
		public static long CLong(Object objValue)
		{
			return CLong(objValue, ConstantsTemp.NULL_LONG);
		}

		/// <summary>
		/// Converts a object to a long
		/// </summary>
		/// <param name="objValue">the object to convert</param>
		/// <param name="iDefaultValue">the default value when the object can not convert to long</param>
		/// <returns>the long representation of the object</returns>
		public static long CLong(Object objValue, long lngDefaultValue)
		{
			if (objValue == null)
				return lngDefaultValue;

			try
			{
				return System.Convert.ToInt64(objValue);
			}
			catch (Exception)
			{
				return lngDefaultValue;
			}
		}

		public static decimal CDecimal(Object objValue)
		{
			return CDecimal(objValue, ConstantsTemp.NULL_DECIMAL);	
		}

		public static decimal CDecimal(Object objValue, decimal decDefaultValue)
		{
			if (objValue == null)
				return decDefaultValue;

			try
			{
				return System.Convert.ToDecimal(objValue);
			}
			catch (Exception)
			{
				return decDefaultValue;
			}

		}


		public static bool CBool(object objValue, bool blnDefault)
		{
			try
			{
				return System.Convert.ToBoolean(objValue);
			}
			catch (Exception)
			{
				return blnDefault;
			}
		}



		/// <summary>
		/// Converts a object to a long
		/// </summary>
		/// <param name="objValue">the object to convert</param>
		/// <returns>the long representation of the object</returns>
		public static DateTime CDateTime(Object objValue)
		{
			return CDateTime(objValue, DateTime.MinValue);
		}

		/// <summary>
		/// Converts a object to a long
		/// </summary>
		/// <param name="objValue">the object to convert</param>
		/// <param name="iDefaultValue">the default value when the object can not convert to long</param>
		/// <returns>the long representation of the object</returns>
		public static DateTime CDateTime(Object objValue, DateTime dtDefaultValue)
		{
			if (objValue == null)
				return dtDefaultValue;

			try
			{
				return System.Convert.ToDateTime(objValue);
			}
			catch (Exception)
			{
				return dtDefaultValue;
			}
		}



		/// <summary>
		/// Whether or not a string can be converted into an integer
		/// </summary>
		/// <param name="strValue">the string to check</param>
		/// <returns>Whether or not the string can be converted into an integer</returns>
		public static bool IsNumeric(string strValue)
		{
			try 
			{
				int i = System.Convert.ToInt32(strValue.ToString());
				return true; 
			}
			catch (FormatException) {return false;}
		}

		/// <summary>
		/// Converts a string to a bool
		/// </summary>
		/// <param name="strValue">the string to convert</param>
		/// <param name="blnDefault">the default value when the string cannot be converted</param>
		/// <returns>the bool representation of the string</returns>
		public static bool CBool(string strValue, bool blnDefault)
		{
			if (IsBoolean(strValue))
				return System.Convert.ToBoolean(strValue);
			else
				return blnDefault;
		}

		/// <summary>
		/// Whether or not a string can be converted into a bool
		/// </summary>
		/// <param name="strValue">the string to convert</param>
		/// <returns>Whether or not a string can be converted into a bool</returns>
		public static bool IsBoolean(string strValue)
		{
			try 
			{
				bool i = System.Convert.ToBoolean(strValue.ToString());
				return true; 
			}
			catch (FormatException) {return false;}
		}

		/// <summary>
		/// Whether or not a string can be converted into a DateTime
		/// </summary>
		/// <param name="strValue">the string to convert</param>
		/// <returns>Whether or not a string can be converted into a DateTime</returns>
		public static bool IsDate(string strValue)
		{
			try 
			{
				DateTime i = System.Convert.ToDateTime(strValue.ToString());
				return true; 
			}
			catch (FormatException) {return false;}
		}

		/// <summary>
		/// Returns the age, in years, based on the current date
		/// </summary>
		/// <param name="dtBirthDate">The date to compare against the current date</param>
		/// <returns>The amount of whole years between the current date and the given date</returns>
		public static int Age(DateTime dtBirthDate)
		{
			DateTime dtNow = System.DateTime.Now;
			int lngAge=0;
			
			lngAge = (int)dtNow.Year - (int)dtBirthDate.Year;
			if (dtNow.Month == dtBirthDate.Month)
			{
				if (dtNow.Day < dtBirthDate.Day)
					lngAge--;
			}
			else if (dtNow.Month < dtBirthDate.Month) 
			{
				lngAge--;
			}
			if (lngAge>0)
				return lngAge;
			else
				return 0;

		}

		public static long GramsToPounds(long pGrams)
		{
			return CInt(pGrams * ONE_GRAM_IN_POUNDS);
		}

		public static long GramsToStone(long pGrams)
		{
			return CInt(pGrams * ONE_GRAM_IN_STONE);
		}

		/// <summary>
		/// Writes an entry to the application event log.
		/// </summary>
		/// <param name="strSource">The source of the message</param>
		/// <param name="strMessage">The message</param>
		/// <param name="enmEntryType">The type of entry</param>
		public static void WriteToApplicationEventLog(string strSource, string strMessage, EventLogEntryType enmEntryType)
		{
			EventLog Log = new EventLog();
			Log.Source = strSource;
			Log.WriteEntry(strMessage,enmEntryType);
		}

		// Constants
		private const int CRC32BASE = 65521;  // largest prime smaller than 65536

		// Largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1
		private const int NMAX = 5552;
		// What this means is that in the worst case senario, each byte
		// could have a value of 255.  As we are adding numbers together
		// we could go beyond the value of a unsigned long or 2^32.
		// Thats a value of 4294967296.  To be sure we are safe, we take
		// things in handfulls of 5552 bytes at a time.


		/// <summary>
		/// Computes the 32 bit CRC of a given string, used by Session to derive the DAL partition key
		/// </summary>
		/// <param name="source">String for which to compute the 32 bit CRC</param>
		/// <returns>returns the 32 bit CRC of the soruce string</returns>
		public static int CRC32(string source)
		{
			if ( source.Length > 0 )
			{

				byte[] bdata = System.Text.Encoding.Unicode.GetBytes(source);
				return Adler32(1, bdata, bdata.Length);
			}
			else
			{
				return 0;
			}
		}

		private static int Adler32( long adler32, byte[] data, double length)
		{
			int pos=0;
			int arrayPos=0;
			int lengthRemaining=0;
			double low=0;
			double high=0;

			if ( adler32 != 0 )
			{
				low = adler32 & 65535;
				high = (int)((adler32 >> 16) & 65535);
			}

			if ( data.Length == 0 )
			{
				return 1;
			}

			lengthRemaining = (int)length;

			while ( lengthRemaining > 0 )
			{
				if ( lengthRemaining < NMAX )
				{
					pos = lengthRemaining-1;
					lengthRemaining = 0;
				}
				else
				{
					pos = NMAX;
					lengthRemaining = lengthRemaining - (NMAX + 1);
				}
				
				while ( pos >= 0 )
				{
					low = low + data[arrayPos];
					high = high + low;
					
					arrayPos++;
					pos--;
				} 

				//low = low % CRC32BASE;
				//high = high % CRC32BASE;

				low = Modulus(low, CRC32BASE);
				high = Modulus(high, CRC32BASE);
			}
            
			return (int)LShift4Byte(high, 16) | (int)low;
		}

		private static double Modulus(double val, double modValue)
		{
			return val - (modValue * (int)(val / modValue));
		}

		private static double RShiftNoRound( double val, int shift )
		{
			return (int)val >> shift;
		}

		private static double LShift4Byte( double val , int shift )
		{
			return (int)val << shift;
		}

		public static string YesNo(bool blnValue)
		{
			if (blnValue == true) 
			{
				return "Yes";
			}
			else
			{
				return "No";
			}
		}


		public static string GetMaskValue(string strValue)
		{
			return GetMaskValue(strValue, 0);
		}
		public static string GetMaskValue(string strValue, long lngDefaultValue)
		{
			string [] aryTemp;
			int i;
			long lngMaskValue = 0;
			long lngTemp;

			lngMaskValue = lngDefaultValue;

			if (strValue != null && strValue.Length > 0)
			{
				strValue = strValue.Trim();
				// split the multi select list.
				aryTemp = strValue.Split(new char[] {','});
				for (i = 0; i < aryTemp.Length; i++)
				{
					try
					{
						lngTemp = System.Convert.ToInt64(aryTemp[i].Trim());
					}
					catch (Exception)
					{
						lngTemp = 0;
					}
					lngMaskValue = (lngMaskValue | lngTemp);
				}
			}
			return System.Convert.ToString(lngMaskValue);
		}

		public static int GetGenderMaskCompliment(int genderMask)
		{
			int genderMaskComplement = 0;
			if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
			{
				genderMaskComplement = ConstantsTemp.GENDERID_SEEKING_MALE;
			}
			else if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
			{
				genderMaskComplement = ConstantsTemp.GENDERID_SEEKING_FEMALE;
			}
			else if ((genderMask & ConstantsTemp.GENDERID_MTF) == ConstantsTemp.GENDERID_MTF)
			{
				genderMaskComplement = ConstantsTemp.GENDERID_SEEKING_MTF;
			}
			else if ((genderMask & ConstantsTemp.GENDERID_FTM) == ConstantsTemp.GENDERID_FTM)
			{
				genderMaskComplement = ConstantsTemp.GENDERID_SEEKING_FTM;
			}

			if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE)
			{
				genderMaskComplement = (genderMaskComplement | ConstantsTemp.GENDERID_MALE);
			}
			else if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FEMALE) == ConstantsTemp.GENDERID_SEEKING_FEMALE)
			{
				genderMaskComplement = (genderMaskComplement | ConstantsTemp.GENDERID_FEMALE);
			}
			else if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MTF) == ConstantsTemp.GENDERID_SEEKING_MTF)
			{
				genderMaskComplement = (genderMaskComplement | ConstantsTemp.GENDERID_MTF);
			}
			else if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FTM) == ConstantsTemp.GENDERID_SEEKING_FTM)
			{
				genderMaskComplement = (genderMaskComplement | ConstantsTemp.GENDERID_FTM);
			}
			return genderMaskComplement;
		}
	}
}
