using System;
using System.Collections;
using System.Data;
using System.Messaging;
using System.Text;
using System.Web;

using Matchnet.DataTemp;
using Matchnet.CachingTemp;

namespace Matchnet.Lib.Stats
{

	public class Logger
	{
		const string QUEUEPATH_CACHEKEY = "StatsQueuePath";
		const string QUEUEPATH_PROP_OWNER = "Stats";
		const string QUEUEPATH_PROP_NAME = "MessageQueuePath";

		private int _AppID;
		private int _PageID;
		private Guid _SessionKey;
		private int _MemberID;
		private DateTime _Date;
		private string _Parameters = "";

		private Hashtable _ParameterTable;
		private Matchnet.CachingTemp.Cache _Cache;

		#region public properties

		public int AppID
		{
			get
			{
				return _AppID;
			}
			set
			{
				_AppID = value;
			}
		}


		public int PageID
		{
			get
			{
				return _PageID;
			}
			set
			{
				_PageID = value;
			}
		}


		public Guid SessionKey
		{
			get
			{
				return _SessionKey;
			}
			set
			{
				_SessionKey = value;
			}
		}


		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}


		public DateTime Date
		{
			get
			{
				return _Date;
			}
			set
			{
				_Date = value;
			}
		}


		public string Parameters
		{
			get
			{
				return _Parameters;
			}
			set
			{
				_Parameters = value;
			}
		}


		#endregion

		public Logger()
		{
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
		}

		public void AddProperty(string propName, string propVal)
		{
			if (_ParameterTable == null)
			{
				_ParameterTable = new Hashtable();
			}

			if (!_ParameterTable.ContainsKey("propName"))
			{
				_ParameterTable.Add(propName, propVal);
			}
			else
			{
				_ParameterTable[propName] = propVal;
			}
		}


		public void Send()
		{
			_Date = DateTime.Now;
			SerializeParameters();
			MessageQueue mq = new MessageQueue(GetQueuePath());
			Message m = new Message();
			m.Body = this;
			//m.Recoverable = true;
			mq.Send(m);
			mq.Close();
		}


		private void SerializeParameters()
		{
			if (_ParameterTable != null)
			{
				StringBuilder sb = new StringBuilder();

				foreach (string key in _ParameterTable.Keys)
				{
					if (sb.Length > 0)
					{
						sb.Append("&");
					}

					sb.Append(key);
					sb.Append("=");
					sb.Append(HttpUtility.UrlEncode(_ParameterTable[key].ToString()));
				}

				_ParameterTable = null;
				_Parameters = sb.ToString();
			}
		}


		private string GetQueuePath()
		{
      // PT:  Removed old caching dependency
			// string path = System.Convert.ToString(System.Web.HttpRuntime.Cache.Get(QUEUEPATH_CACHEKEY));
      string path = 
        System.Convert.ToString( _Cache.Get(QUEUEPATH_CACHEKEY) );

			if (path == string.Empty)
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnMaster");
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter("@Owner", SqlDbType.VarChar, ParameterDirection.Input, QUEUEPATH_PROP_OWNER);
				client.AddParameter("@Name", SqlDbType.VarChar, ParameterDirection.Input, QUEUEPATH_PROP_NAME);

				DataTable dt = client.GetDataTable("up_Property_List", CommandType.StoredProcedure);
				if (dt.Rows.Count > 0)
				{
					path = dt.Rows[0]["Value"].ToString();
          // PT:  Removed old caching dependency
					// System.Web.HttpRuntime.Cache.Insert(QUEUEPATH_CACHEKEY, path, null, DateTime.Now.AddMinutes(60), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
          _Cache.Insert( QUEUEPATH_CACHEKEY, 
                         path, 
                         null, 
                         DateTime.Now.AddMinutes(60), 
                         TimeSpan.Zero, 
                         System.Web.Caching.CacheItemPriority.High, 
                         null );
				}
				else
				{
					throw new Exception("mnMaster property " + QUEUEPATH_PROP_NAME + " (owner " + QUEUEPATH_PROP_OWNER + ") not found");
				}
			}

			return path;
		}


	}

}
