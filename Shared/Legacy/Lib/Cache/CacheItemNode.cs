using System;

namespace Matchnet.Lib.Cache
{
	/// <summary>
	/// A node that represents a CacheItem that exists
	/// in a List.
	/// </summary>
	public class CacheItemNode
	{
		private CacheItemNode m_previous;
		private CacheItemNode m_next;
		private string m_key;
		private int m_length;
	
		/// <summary>
		/// Constructs a new instance with the key and length
		/// of the cache item.
		/// </summary>
		/// <param name="key">The key of the CacheItem</param>
		/// <param name="length">The length of the CacheItem value</param>
		public CacheItemNode(string key, int length) 
		{
			m_key = key;
			m_length = length;
		}

		/// <summary>
		/// The key of the cache item
		/// </summary>
		public string Key 
		{
			get 
			{
				return m_key;
			}
		}

		/// <summary>
		/// The length of the cache item value
		/// </summary>
		public int Length 
		{
			get 
			{
				return m_length;
			}
		}

		/// <summary>
		/// The node that exists in the list prior to this node
		/// </summary>
		public CacheItemNode Previous 
		{
			get 
			{
				return m_previous;
			}

			set 
			{
				m_previous = value;
			}
		}

		/// <summary>
		/// The node that exists in the list after this node
		/// </summary>
		public CacheItemNode Next 
		{
			get 
			{
				return m_next;
			}

			set 
			{
				m_next = value;
			}
		}
	}
}
