using System;
using System.Collections;
using System.Threading;

namespace Matchnet.Lib.Cache
{
	/// <summary>
	/// A simple Cache that allows for in memory storage of string values. 
	/// </summary>
	public class Cache 
	{
		private static Cache mSingleton;
		private Hashtable mCache;
		private static Thread mThread;
		private bool mblnRunning;
		private static string strLock = "";

		/// <summary>
		/// The one and only instance of the Cache.
		/// </summary>
		/// <returns></returns>
		public static Cache GetInstance() 
		{
			if (mSingleton == null) 
			{
				lock(strLock) 
				{
					if (mSingleton == null)
					{
						mSingleton = new Cache();
						mThread = new Thread(new ThreadStart(mSingleton.ExpireProc));
						mThread.Start();
					}
				
				}
			}
			return mSingleton;
		}


		private Cache() 
		{
			mCache = new Hashtable(5000);
			mblnRunning = true;
		}

		/// <summary>
		/// Stops the thread that expires cache content
		/// </summary>
		public void StopExpirationThread()
		{
			mblnRunning = false;
			mThread.Abort();
		}

		/// <summary>
		/// Inserts an item into the Cache. If the item already exists, its
		/// value will be updated.
		/// </summary>
		/// <param name="strKey">The key of the inserted item</param>
		/// <param name="objValue">The value of the inserted item</param>
		/// <param name="dtExpiration">The date to expire this item</param>
		public void Insert(string strKey, object objValue, DateTime dtExpiration) 
		{
			CacheItem cacheItem;

			lock (strLock) 
			{
				// remove the existing value, client code should already
				// check for the existence, so this is just a safeguard
				if (mCache.ContainsKey(strKey)) 
				{
					cacheItem = (CacheItem)mCache[strKey];
					cacheItem.Initialize(objValue, dtExpiration);
				}
				else
				{
					cacheItem = new CacheItem(objValue, dtExpiration);
					mCache.Add(strKey, cacheItem);
				}
			}
		}

		/// <summary>
		/// Inserts an item into the cache.
		/// </summary>
		/// <param name="strKey">The key to use for retrieving the item</param>
		/// <param name="objValue">The value of the cache item</param>
		/// <param name="intTimeToLive">The amount of seconds that the value is valid for</param>
		public void Insert(string strKey, object objValue, int intTimeToLive)
		{
			DateTime dtExpiration = DateTime.Now.Add(new TimeSpan(0, 0, 0, intTimeToLive, 0));
			Insert(strKey, objValue, dtExpiration);
		}


		/// <summary>
		/// Retrieves a value from the cache. A null value is returned if the cache
		/// does not contain a value based on the given key or if the cached item
		/// should be expired
		/// </summary>
		/// <param name="strKey">The key of the cached value</param>
		/// <returns>The cached value</returns>
		public object Retrieve(string key) 
		{
			// Lock the cache for exclusive access
			lock(strLock)
			{
				if (mCache.ContainsKey(key)) 
				{
					CacheItem cacheItem = (CacheItem)mCache[key];

					if (cacheItem.Expiration.CompareTo(DateTime.Now) < 0) 
					{
						mCache.Remove(key);
						return null;
					}
					else
					{
						return cacheItem.Value;
					}
				}
				else
				{
					return null;
				}
			} 
		}

		/// <summary>
		/// Whether or not the cache contains an item
		/// corresponding to the given key.
		/// </summary>
		/// <param name="key">The key of the CacheItem</param>
		/// <returns>Whether or not the cache contains an item</returns>
		public bool Contains(string key)
		{
			lock(strLock)
			{
				if (mCache.ContainsKey(key)) 
				{
					return true;
				}
				else
				{
					return false;
				}
			} 
		}

		/// <summary>
		/// The amount of items that exist in the cache
		/// </summary>
		/// <returns></returns>
		public int Count()
		{
			return mCache.Count;
		}

		/// <summary>
		/// Returns a list of all cache items.
		/// </summary>
		public ArrayList All 
		{
			get 
			{
				// it would be dangerous to return the underlying
				// collection
				//return mCache;
				// So let's carefully build a new list while in a lock and return the 
				// copy
				ArrayList list;

				// Lock the cache for exclusive access
				lock(strLock)
				{
					ICollection collection = mCache.Keys;
					IEnumerator enumerator = collection.GetEnumerator();
					list = new ArrayList(collection.Count);
					while (enumerator.MoveNext()) 
					{
						string key = (string)enumerator.Current;
						if (mCache.ContainsKey(key)) 
						{
							list.Add((CacheItem)mCache[key]);
						}
					}
				} // Unlock the cache

				return list;  // Return the copy of the cache's contents
			}
		}


		/// <summary>
		/// Deletes an item from the cache
		/// </summary>
		/// <param name="strKey">The key of the cache item to remove</param>
		public void Delete(string strKey) 
		{
			lock (strLock) 
			{
				if (mCache.ContainsKey(strKey)) 
				{
					mCache.Remove(strKey);
				}
			}
		}

		/// <summary>
		/// Clears the entire cache.
		/// </summary>
		public void DeleteAll() 
		{
			lock (strLock) 
			{
				mCache.Clear();
				// ask the framework to clean up
				System.GC.Collect();
			}
		}

		/// <summary>
		/// Removes expired items from the cache.
		/// </summary>
		public void ExpireProc()
		{
			while(mblnRunning)
			{
				System.Threading.Thread.Sleep(60000);
				if (mblnRunning)
				{
					DateTime dtNow = DateTime.Now;
					ArrayList aryKeyList = new ArrayList(200);

					lock(strLock)
					{
						IDictionaryEnumerator iterator = mCache.GetEnumerator();
						CacheItem cacheItem;

						while(iterator.MoveNext())
						{
							cacheItem = (CacheItem)iterator.Value;
							if (cacheItem.Expiration.CompareTo(dtNow) < 0) 
							{
								aryKeyList.Add(iterator.Key);
							}
						}

						foreach(string strKey in aryKeyList)
						{
							mCache.Remove(strKey);
						}
					}
				}
			}
		}
	}
}
