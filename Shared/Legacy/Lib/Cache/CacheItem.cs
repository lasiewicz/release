using System;

namespace Matchnet.Lib.Cache
{
	/// <summary>
	/// An item that exists in the Cache
	/// </summary>
	public class CacheItem
	{
		private DateTime mdtExpiration;
		private object mobjValue;

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		/// <param name="objValue">The value of the cache item</param>
		/// <param name="dtExpiration">When the cache item should be expired</param>
		public CacheItem(object objValue, DateTime dtExpiration) 
		{
			mobjValue = objValue;
			mdtExpiration = dtExpiration;
		}

		/// <summary>
		/// The expiration date of the cache item
		/// </summary>
		public DateTime Expiration 
		{
			get 
			{
				return mdtExpiration;
			}
		}

		/// <summary>
		/// The value of the cache item
		/// </summary>
		public object Value 
		{
			get 
			{
				return mobjValue;
			}
		}

		/// <summary>
		/// Resets the value and expiration of the cache item
		/// </summary>
		/// <param name="objValue">The value of the cache item</param>
		/// <param name="dtExpiration">When the cache item should be expired</param>
		public void Initialize(object objValue, DateTime dtExpiration) 
		{
			mobjValue = objValue;
			mdtExpiration = dtExpiration;
		}
	}
}
