using System;

namespace Matchnet.Lib.Data
{
	/// <summary>
	/// Holds properties used for obtaining a connection string
	/// </summary>
	[Serializable]
	public class SQLDescriptor
	{
		private string m_logicalDatabase;
		private int m_key;
		private bool m_withLoad;

		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="logicalDatabase">The logical database name</param>
		public SQLDescriptor(string logicalDatabase) {
			m_logicalDatabase = logicalDatabase;
			m_key = 0;
			m_withLoad = false;
		}


		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="logicalDatabase">The logical database name</param>
		/// <param name="key">The key of the item to provide a connection string for</param>
		public SQLDescriptor(string logicalDatabase, int key) {
			m_logicalDatabase = logicalDatabase;
			m_key = key;
			m_withLoad = false;
		}

		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="logicalDatabase">The logical database name</param>
		/// <param name="key">The key of the item to provide a connection string for</param>
		/// <param name="withLoad">Whether or not to load a fresh copy of existing databases when getting a connection string</param>
		public SQLDescriptor(string logicalDatabase, int key, bool withLoad) {
			m_logicalDatabase = logicalDatabase;
			m_key = key;
			m_withLoad = withLoad;
		}

		/// <summary>
		/// The logical database name
		/// </summary>
		public string LogicalDatabase {
			get {
				return m_logicalDatabase;
			}
		}

		/// <summary>
		/// The key of the item to provide a connection string for
		/// </summary>
		public int Key {
			get {
				return m_key;
			}
		}

		/// <summary>
		/// Whether or not to load a fresh copy of existing databases when getting a connection string
		/// </summary>
		public bool WithLoad {
			get {
				return m_withLoad;
			}
		}
	}
}
