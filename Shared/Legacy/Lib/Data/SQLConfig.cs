using System;
using System.Collections;
using System.Xml;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Config;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Util;

namespace Matchnet.Lib.Data
{
	/// <summary>
	/// A SQLConfig is a Singleton that provides connection strings to
	/// databases based on a logical database name and key.
	/// </summary>
	public sealed class SQLConfig
	{
		private static SQLConfig m_singleton = new SQLConfig();
		private const int DEFAULT_REFRESH = 30;
		private static string m_LockHandle = "";

		private string m_Version = Guid.NewGuid().ToString();
		private DateTime m_TimeCheck = DateTime.MinValue;
		private Hashtable m_Cache = new Hashtable();
		private bool m_isCurrent = false;
		
		private string m_initialConnectionString;
		private int m_refresh;

		/// <summary>
		/// Returns the one and only instance of the SQLConfig
		/// </summary>
		/// <returns>The one and only instance of the SQLConfig</returns>
		public static SQLConfig GetInstance() {
			return m_singleton;
		}

		private SQLConfig() {
			try {
				Matchnet.Lib.Config.Configuration config = Matchnet.Lib.Config.Configuration.GetInstance();
				if (config.PropertyIsDefined("SQLConfigRefresh")) {
					m_refresh = Util.Util.CInt(config.GetProperty("SQLConfigRefresh"));
				} else {
					m_refresh = DEFAULT_REFRESH;
				}
			} catch {
				m_refresh = DEFAULT_REFRESH;
			}
		}

		/// <summary>
		/// Returns a database connection string based on a SQLDescriptor
		/// </summary>
		/// <param name="descriptor">The SQLDescriptor to use when obtaining a connection string</param>
		/// <returns>A connection string to a database</returns>
		public string GetConnectionString(SQLDescriptor descriptor) {
			return GetConnectionString(descriptor.LogicalDatabase, descriptor.Key, descriptor.WithLoad);
		}

		/// <summary>
		/// Returns a database connection string.
		/// </summary>
		/// <param name="logicalDatabase">The logical name of the database</param>
		/// <param name="key">The key of the item to provide a connection string for</param>
		/// <param name="forceLoad">Whether or not to force a fresh loading of logical databases</param>
		/// <returns>A connection string to a database</returns>
		public string GetConnectionString(string logicalDatabase, int key, bool forceLoad) {
			// will load if forceLoad is true or existing version has expired
			Load(forceLoad);

			if (!m_Cache.ContainsKey(logicalDatabase)) {
				MatchnetException exception = new MatchnetException("Unable to load database [" + logicalDatabase + "]", "SQLConfig.GetConnectionString");
				throw exception;
			}

			ArrayList list = (ArrayList)m_Cache[logicalDatabase];

			Hashtable validKeys = new Hashtable();
			IEnumerator enumerator = list.GetEnumerator();
			int counter = 0;
			while (enumerator.MoveNext()) {
				LogicalDatabase val = (LogicalDatabase)enumerator.Current;
				if (val.Name == logicalDatabase) {
					if ((key % list.Count) == val.AccessFlag) {
						if (val.ActiveDateTime <= DateTime.Now) {
							validKeys.Add(counter, val.ConnectionString);
							counter++;
						}								
					}							
				}
			}

			if (counter == 0) {
				MatchnetException exception = new MatchnetException("Database [" + logicalDatabase + "] is currently unavailable", "SQLConfig.GetConnectionString");
				throw exception;
			}

			//DO NOT use Random.Next(MinValue, MaxValue). Does not balance.
			counter = (int) (Math.Floor(counter * (new Random((int)DateTime.Now.Ticks).NextDouble())));
			return validKeys[counter].ToString();

			//TODO: leveling logic
		}

		private string GetInitialConnectionString() {
			Matchnet.Lib.Config.Configuration config = Matchnet.Lib.Config.Configuration.GetInstance();
			if (!config.PropertyIsDefined("InitialConnectionString")) {
				throw new Exception("InitialConnectionString not defined");
			}
			return config.GetProperty("InitialConnectionString");
		}

		private void Load(bool forceLoad) {	
			lock (this) 
			{
				string newVersion;
				DataTable dataTable;
				LogicalDatabase logicalDatabase;

				// TODO: this should be smarter by using any mnMaster database (load balancing) and refreshing
				//       if the current database version is different than our version
				if (m_initialConnectionString == null || m_initialConnectionString.Length == 0) 
				{
					m_initialConnectionString = GetInitialConnectionString();
				}

				try 
				{				
					TimeSpan ts = DateTime.Now - m_TimeCheck;
					if (ts.TotalSeconds <= m_refresh) 
					{
						m_isCurrent = true;	
					}
					else 
					{
						m_isCurrent = false;
						if (m_Cache.Count > 0) 
						{
							dataTable = GetDataTable(m_initialConnectionString, "up_LogicalDatabaseVersion_List");
							if (dataTable.Rows.Count == 0) 
							{
								throw (new Exception("Logical Database Version is not configured properly."));
							}

							newVersion = dataTable.Rows[0][0].ToString();
							if (newVersion == m_Version) 
							{
								lock(m_LockHandle) 
								{
									m_TimeCheck = DateTime.Now;
									m_isCurrent = true;
								}
							}
						}
					}
				
					if (!m_isCurrent || forceLoad) 
					{
						//instruct all other threads to use the current list
						m_TimeCheck = DateTime.Now;
					
						try 
						{
							dataTable = GetDataTable(m_initialConnectionString, "up_LogicalDatabases_List");
						} 
						catch (Exception ex) 
						{
							throw ex;
						}

						if (dataTable.Rows.Count == 0) 
						{
							throw (new Exception("Logical Databases are not configured properly."));
						}
						else 
						{
							newVersion = dataTable.Rows[0]["Version"].ToString();
						
							m_Version = newVersion;
							m_TimeCheck = DateTime.Now;

							ArrayList arr;

							m_Cache.Clear();
							foreach (DataRow dataRow in dataTable.Rows) 
							{
								logicalDatabase = new LogicalDatabase();
								logicalDatabase.Name = dataRow["LogicalDatabase"].ToString();
								logicalDatabase.AccessFlag = (int) dataRow["AccessFlag"];
								logicalDatabase.ActiveDateTime = (DateTime) dataRow["ActiveDateTime"];
								logicalDatabase.ConnectionString = dataRow["ConnectionString"].ToString();

								arr = (ArrayList) m_Cache[logicalDatabase.Name];
								if(arr == null) 
								{
									arr = new ArrayList();
									m_Cache.Add(logicalDatabase.Name, arr);
								}

								arr.Add(logicalDatabase);
							}
						}
					}
				}
				catch 
				{
					// force a reload on next attempt
					m_Cache.Clear();
					m_Version = "";
					m_TimeCheck = DateTime.MinValue;
					throw;
				}
			}
		}

		/// <summary>
		/// Obtains a connection string based on the physical database id
		/// </summary>
		/// <param name="physicalDatabaseID">The physical database id</param>
		/// <returns>A database connection string</returns>
		public string PhysicalConnectionString(int physicalDatabaseID) {
			SqlConnection connection = new SqlConnection(GetInitialConnectionString());
			SqlCommand command = new SqlCommand("up_PhysicalDatabase_List");

			command.CommandType = CommandType.StoredProcedure;
			command.Connection = connection;

			SqlParameter param = command.CreateParameter();
			param.SqlDbType = SqlDbType.Int;
			param.Direction = ParameterDirection.Input;
			param.ParameterName = "@PhysicalDatabaseID";
			param.Value = physicalDatabaseID;
			command.Parameters.Add(param);

			SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

			DataTable dataTable = new DataTable();
			string connectionString = null;

			try {
				connection.Open();
				dataAdapter.Fill(dataTable);
				connectionString = dataTable.Rows[0]["ConnectionString"].ToString();
			}
			finally {
				if (connection.State != ConnectionState.Closed) {
					connection.Close();
				}
				command.Dispose();
				connection.Dispose();
			}
			return connectionString;
		}

		// used to populate logical databases
		private DataTable GetDataTable(string connectionString, string commandText) {
			SqlConnection connection = new SqlConnection(connectionString);
			SqlCommand command = new SqlCommand(commandText);
			command.CommandType = CommandType.StoredProcedure;
			command.Connection = connection;
			SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
			
			DataTable dataTable = new DataTable();

			try {
				connection.Open();
				dataAdapter.Fill(dataTable);
				
				return dataTable;				
			}
			finally {
				if (connection.State != ConnectionState.Closed) {
					connection.Close();
				}
				command.Dispose();
				connection.Dispose();
			}			
		}

		private class LogicalDatabase {
			public string Name;
			public int AccessFlag;
			public DateTime ActiveDateTime;
			public string ConnectionString;
		}
	}
}
