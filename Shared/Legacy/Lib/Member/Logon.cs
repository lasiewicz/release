using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Threading;

namespace Matchnet.Lib.Member
{
	/// <summary>
	/// Summary description for Logon.
	/// </summary>
	public class Logon
	{

#region Property
		private long mlngMemberID;
		private string mstrEmailAddress;
		private string mstrUserName;
		private string mstrPassword;
		private int mintCryptKeyID;
		private int mintLogonCount;
		private DateTime mdtInsertDate;
		private DateTime mdtLastLogonDate;

		public long MemberID
		{
			get { return mlngMemberID;}
			set {mlngMemberID = value;}
		}

		public string EmailAddress
		{
			get { return mstrEmailAddress;}
			set {mstrEmailAddress = value;}
		}

		public string UserName
		{
			get { return mstrUserName;}
			set {mstrUserName = value;}
		}

		public string Password
		{
			get { return mstrPassword;}
			set {mstrPassword = value;}
		}

		public int CryptKeyID
		{
			get { return mintCryptKeyID;}
			set {mintCryptKeyID = value;}
		}

		public int LogonCount
		{
			get { return mintLogonCount;}
			set {mintLogonCount = value;}
		}
		public DateTime InsertDate
		{
			get { return mdtInsertDate;}
			set {mdtInsertDate = value;}
		}
		public DateTime LastLogonDate
		{
			get { return mdtLastLogonDate;}
			set {mdtLastLogonDate = value;}
		}

#endregion 



		public Logon()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static int Update(long lngMemberID, string strEmailAddress,
			string strUserName, long lngCharSet, string strPassword)
		{
			return Update(lngMemberID, strEmailAddress, strUserName, lngCharSet, strPassword, false);
		}

		public static int Update(long lngMemberID, string strEmailAddress,
			string strUserName, long lngCharSet, string strPassword, bool usernameChange)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnLogon",0);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, lngMemberID);
			client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

			if (strEmailAddress != Matchnet.Constants.NULL_STRING)
			{
				client.AddParameter("@EmailAddress", SqlDbType.NVarChar , ParameterDirection.Input, strEmailAddress, 255);
			}
			if (strUserName != Matchnet.Constants.NULL_STRING)
			{
				client.AddParameter("@UserName", SqlDbType.NVarChar , ParameterDirection.Input, strUserName, 50);
			}
			if (strPassword != Matchnet.Constants.NULL_STRING)
			{
				client.AddParameter("@Password", SqlDbType.NVarChar , ParameterDirection.Input, strPassword, 50);
			}

			SqlCommand cmd = new SqlCommand("up_Member_Update");
			cmd.CommandType = CommandType.StoredProcedure;
			client.PopulateCommand(cmd);
			client.ExecuteNonQuery(cmd);
			if (usernameChange)
			{
				DataTable dt=client.GetDataTable(cmd);
				if (dt.Rows.Count != 0)
				{
					string CurrentUserName=dt.Rows[0]["UserName"].ToString();

					if (CurrentUserName != strUserName)
					{
						throw new Exception("user name exists");
					}
				}
			}
			return  System.Convert.ToInt32(cmd.Parameters["@ReturnValue"].Value);	
		}


		public int PopulateByMemberID(long lngMemberID)
		{
			return Populate(lngMemberID, Matchnet.Constants.NULL_STRING, Matchnet.Constants.NULL_STRING);
		}

		public int PopulateByEmailAddress(string strEmailAddress)
		{
			return Populate(Matchnet.Constants.NULL_INT, strEmailAddress, Matchnet.Constants.NULL_STRING);
		}

		public int PopulateByUserName(string strUserName)
		{
			return Populate(Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_STRING, strUserName);
		}

		public int Populate(long lngMemberID, string strEmailAddress, string strUserName)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnLogon",0);
			SQLClient client = new SQLClient(descriptor);
			if (lngMemberID != Matchnet.Constants.NULL_INT)
			{
				client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, lngMemberID);
			}
			if (strEmailAddress != Matchnet.Constants.NULL_STRING) 
			{
				client.AddParameter("@EmailAddress", SqlDbType.NVarChar, ParameterDirection.Input, strEmailAddress, 255);
			}
			if (strUserName != Matchnet.Constants.NULL_STRING)
			{
				client.AddParameter("@UserName", SqlDbType.NVarChar, ParameterDirection.Input, strUserName, 50);
			}
		
			DataTable dt = client.GetDataTable("up_Member_List", CommandType.StoredProcedure);
			DataRow r;
			if (dt.Rows.Count > 0)
			{
				r = dt.Rows[0];
				this.MemberID = System.Convert.ToInt32(r["MemberID"]);
				this.EmailAddress = System.Convert.ToString(r["EmailAddress"]);
				this.UserName = System.Convert.ToString(r["UserName"]);
				this.Password = System.Convert.ToString(r["Password"]);
				this.CryptKeyID = System.Convert.ToInt32(r["CryptKeyID"]);
				this.LogonCount = System.Convert.ToInt32(r["LogonCount"]);
				this.InsertDate = System.Convert.ToDateTime(r["InsertDate"]);
				this.LastLogonDate = System.Convert.ToDateTime(r["LastLogonDate"]);
				return ConstantsTemp.ERROR_SUCCESS;
			}
			else
			{
				//member not found
				return Matchnet.Constants.NULL_INT;
			}
	
		}

	

		
	}
}
