using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Threading;
using Matchnet.Lib.Util;

namespace Matchnet.Lib.Member
{
	/// <summary>
	/// Summary description for MemberProfile.
	/// </summary>
	public class MemberProfile
	{
		public MemberProfile()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string this[string name]
		{
			get
			{
				if ( mAttributes.Rows.Count > 0 )
				{
					DataRow[] attributeRows = mAttributes.Select("AttributeName='"+name+"'");
					
					if ( attributeRows.Length > 0 )
					{
						return attributeRows[0]["Value"].ToString();
					}
				}
				return null;
			}	
		}

		public int DomainID;
		public int TranslationID;

		public DataTable mAttributes;

		public bool PopulateAttributeGroup(int memberID, int domainID, int privateLabelID, int languageID, int attributeGroupID, bool isAdminApp)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
		
			DomainID = domainID;

			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input,privateLabelID);
			client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input,languageID);
			client.AddParameter("@AttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroupID);

			string storedProc = (isAdminApp) ? "up_MemberAttribute_List_Admin" : "up_MemberAttribute_List";
			mAttributes = client.GetDataTable(storedProc, CommandType.StoredProcedure);

			return true;
		}

		public bool PopulateAttributeGroup(int memberID, int domainID, int privateLabelID, int languageID, int attributeGroupID)
		{
			return PopulateAttributeGroup(memberID, domainID, privateLabelID, languageID, attributeGroupID, false);
		}


		public bool Populate(int memberID, int domainID, int privateLabelID, int languageID)
		{
			return Populate(memberID, domainID, privateLabelID, languageID,
						Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT, false);
		}

		public bool Populate(int memberID, int domainID, int privateLabelID, int languageID, 
						int visitorMemberID, int attributeGroupID, int numPhotos, bool hideMask)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
		
			DomainID = domainID;

			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			if (privateLabelID != Matchnet.Constants.NULL_INT)
				client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input,privateLabelID);
			if (languageID != Matchnet.Constants.NULL_INT)
				client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input,languageID);

			if ( visitorMemberID != Matchnet.Constants.NULL_INT )
				client.AddParameter("@VisitorMemberID", SqlDbType.Int, ParameterDirection.Input, visitorMemberID);
			if ( attributeGroupID != Matchnet.Constants.NULL_INT )
				client.AddParameter("@AttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroupID);
			if ( numPhotos != Matchnet.Constants.NULL_INT )
				client.AddParameter("@GetMaxPhotos", SqlDbType.Int, ParameterDirection.Input, numPhotos);
			
			mAttributes = client.GetDataTable("up_MemberAttribute_ListViewProfile", CommandType.StoredProcedure);

			return true;
		}


		public bool SaveAttributeText(int memberID, int domainID, int privateLabelID, int languageID, string attributeName,
							string attributeValue, int operationTypeID, bool approvedFlag, int actionMask)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
		
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
			client.AddParameter("@AttributeName",SqlDbType.VarChar, ParameterDirection.Input, attributeName);
			client.AddParameter("@Value",SqlDbType.NVarChar, ParameterDirection.Input, attributeValue);
			client.AddParameter("@OperationTypeId", SqlDbType.Int, ParameterDirection.Input, operationTypeID);
			client.AddParameter("@ApprovedFlag", SqlDbType.Bit, ParameterDirection.Input, approvedFlag);
			client.AddParameter("@ActionMask", SqlDbType.Int, ParameterDirection.Input, actionMask);
            
			client.ExecuteNonQuery("up_MemberAttribute_Save",CommandType.StoredProcedure);

			return true;
		}

		public bool SaveAttributeInt(int memberID, int domainID, int privateLabelID, string attributeName, int attributeValue, int actionMask)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
		
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter("@AttributeName",SqlDbType.VarChar, ParameterDirection.Input, attributeName);
			client.AddParameter("@Value",SqlDbType.NVarChar, ParameterDirection.Input, attributeValue.ToString());            
			client.AddParameter("@ActionMask", SqlDbType.Int, ParameterDirection.Input, actionMask);
			
			client.ExecuteNonQuery("up_MemberAttribute_Save",CommandType.StoredProcedure);

			return true;
		}

		public bool SaveAttributeInt(int memberID, int domainID, int privateLabelID, string attributeName, int attributeValue)
		{
			return SaveAttributeInt(memberID, domainID, privateLabelID, attributeName, attributeValue, 0);
		}

		public bool SaveAttributeMultiSelect(int memberID, int domainID, int privateLabelID, int languageID, string attributeName,
			string attributeValue, int operationTypeID, int actionMask)
		{

			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);

			attributeValue = Util.Util.GetMaskValue(attributeValue);
		
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
			client.AddParameter("@AttributeName",SqlDbType.VarChar, ParameterDirection.Input, attributeName);
			client.AddParameter("@Value",SqlDbType.NVarChar, ParameterDirection.Input, attributeValue);
			client.AddParameter("@OperationTypeId", SqlDbType.Int, ParameterDirection.Input, operationTypeID);
			client.AddParameter("@ActionMask", SqlDbType.Int, ParameterDirection.Input, actionMask);
            
			client.ExecuteNonQuery("up_MemberAttribute_Save",CommandType.StoredProcedure);

			return true;
		}

		public string GetMemberAttributeTextValue(int memberID, string attributeName, int domainID, int languageID)
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnMember", memberID));
			string sql = "select dbo.fn_MemberAttribute_GetValueEx(" + memberID + ",'" + attributeName + "'," + domainID + "," + languageID + ") as Value";
			DataTable table = client.GetDataTable(sql, CommandType.Text);

			if (table != null && table.Rows.Count > 0) 

			{
				return System.Convert.ToString(table.Rows[0]["Value"]);
			} 

			return string.Empty;
		}
		
		public int GetMemberAttributeIntValue(int memberID, string attributeName, int domainID, int languageID)
		{
			string valueString = GetMemberAttributeTextValue(memberID, attributeName, domainID, languageID);
			double valueDbl;

			if (!Double.TryParse(valueString, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out valueDbl))
			{
				valueDbl = Matchnet.Constants.NULL_INT;
			}

			return (int)valueDbl;
		}

		public int GetMemberAttributeIntValue(int memberID, string attributeName, int domainID, int languageID, int defaultValue)
		{
			int result = GetMemberAttributeIntValue(memberID, attributeName, domainID, languageID);

			if (result == Matchnet.Constants.NULL_INT)
			{
				result = defaultValue;
			}

			return result;
		}

	}
}
