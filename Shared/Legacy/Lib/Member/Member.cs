using System;
using System.Data;
using System.Data.SqlClient;

//using Matchnet.Content;
using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Threading;
using Matchnet.Lib.Util;

namespace Matchnet.Lib.Member
{
	/// <summary>
	/// Summary description for Member.
	/// </summary>
	public class Member
	{
		private int _MemberID;
		private int _DomainID;
		private bool _SelfSuspendedFlag;
		private bool _AdminSuspendedFlag;
		private int _GlobalStatusMask;
		private int _NextRegistrationActionPageID;
		private int _GenderMask;
		private DateTime _Birthdate;
		private int _HideMask;
		private int _PromotionID;
		private int _RegionID;
		private bool _HasPhotoFlag;
		private string _Username;
		private bool _IsPayingMemberFlag;
		private string _HtmlUsername;
		private bool _IsAdmin;
		//private bool _IsLoggedIn;

		public void Populate(int memberID, int domainID, int privateLabelID, string emailAddress, string username)
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnMember", memberID));
		
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input,memberID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input,domainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
			client.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailAddress, 255);
			client.AddParameter("@UserName", SqlDbType.NVarChar, ParameterDirection.Input, username, 50 );

			DataTable table = client.GetDataTable("up_Member_Logon", CommandType.StoredProcedure);
			
			if ( table.Rows.Count > 0 )
			{
				DataRow row = table.Rows[0];

				_MemberID = memberID;
				_DomainID = domainID;

				_SelfSuspendedFlag = System.Convert.ToBoolean(row["SelfSuspendedFlag"]);
				//_AdminSuspendedFlag = System.Convert.ToBoolean(row["AdminSuspendedFlag"]);
				_GlobalStatusMask = System.Convert.ToInt32(row["GlobalStatusMask"]);
				_AdminSuspendedFlag = ((_GlobalStatusMask & System.Convert.ToInt32(1)) == System.Convert.ToInt32(1));
				if (row["NextRegistrationActionPageID"] != DBNull.Value)
				{
					_NextRegistrationActionPageID = System.Convert.ToInt32(row["NextRegistrationActionPageID"]);
				}
				if (row["GenderMask"] != DBNull.Value)
				{
					_GenderMask = System.Convert.ToInt32(row["GenderMask"]);
				}
				if (row["BirthDate"] != DBNull.Value)
				{
					_Birthdate = System.Convert.ToDateTime(row["Birthdate"]);
				}
				_HideMask = System.Convert.ToInt32(row["HideMask"]);
				//_PromotionID = System.Convert.ToInt32(row["PromotionID"]);
				_PromotionID = Util.Util.CInt(row["PromotionID"], 0);
				if (row["RegionID"] != DBNull.Value)
				{
					_RegionID = System.Convert.ToInt32(row["RegionID"]);
				}
				_HasPhotoFlag = System.Convert.ToBoolean(row["HasPhotoFlag"]);
				_Username = row["UserName"].ToString();
				_IsPayingMemberFlag = System.Convert.ToBoolean(row["IsPayingMemberFlag"]);
				_HtmlUsername = row["UserName"].ToString();
				_IsAdmin = System.Convert.ToBoolean(row["IsAdmin"]);
			}
		}
								
		public static int Update(int memberId, string emailAddress, string userName, string password)
		{
			SQLClient client = new SQLClient(new SQLDescriptor("mnLogon"));

			client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);

			if (emailAddress != null)
				client.AddParameter("@EmailAddress", SqlDbType.NVarChar, ParameterDirection.Input, emailAddress, 255);

			if (userName != null)
				client.AddParameter("@UserName", SqlDbType.NVarChar, ParameterDirection.Input, userName, 50);

			if (password != null)
				client.AddParameter("@Password", SqlDbType.NVarChar, ParameterDirection.Input, password, 50);

			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "up_Member_Update";
			client.PopulateCommand(cmd);

			DataTable dt = client.GetDataTable(cmd);
		
			// int resource = Content.Translator.ConvertToResource(cmd.Parameters["@ReturnValue"].Value);
		    int resource = Convert.ToInt32( cmd.Parameters["@ReturnValue"].Value );

			return resource;
		}

		public DataTable GetMemberAttributeSet(int attributeGroupID, int privateLabelID, int languageID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", _MemberID);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@MemberID", SqlDbType.VarChar, ParameterDirection.Input, _MemberID);
			client.AddParameter("@DomainID", SqlDbType.VarChar, ParameterDirection.Input, _DomainID);
			client.AddParameter("@PrivateLabelID", SqlDbType.VarChar, ParameterDirection.Input, privateLabelID);
			client.AddParameter("@AttributeGroupID", SqlDbType.VarChar, ParameterDirection.Input, attributeGroupID);
			client.AddParameter("@LanguageID", SqlDbType.VarChar, ParameterDirection.Input, languageID);
		
			return client.GetDataTable("up_MemberAttribute_List", CommandType.StoredProcedure);
		}

		public static bool HasPrivilege(int memberID, int privilegeID, bool isAdmin)
		{
			bool blnHasPrivilege = false;
			int intPrivilegeCount = 0;

			if (memberID != 0) //check user login
			{
				if (isAdmin) 
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
					SQLClient client = new SQLClient(descriptor);

					client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
					client.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);

					DataTable table = client.GetDataTable("up_MemberPrivilege_HasPrivilege", CommandType.StoredProcedure);
					
					if (table.Rows.Count > 0)
					{
						intPrivilegeCount = System.Convert.ToInt32(table.Rows[0][0]);
						if (intPrivilegeCount > 0)
						{
							blnHasPrivilege = true;
						}
					}
				}
			}
			return blnHasPrivilege;
		}

		public static bool IsPayingMember(int memberID, int privateLabelID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
			string sql = "select dbo.fn_IsPayingMemberEx(" + memberID + "," + privateLabelID + ")";
			DataTable dt = client.GetDataTable(sql, CommandType.Text);
		
			if (dt != null)
			{
				if (dt.Rows.Count > 0)
				{
					return Util.Util.CBool(dt.Rows[0][0], false);
				}
			}
			return false;
		}

		public static bool IsAdminMember(int memberID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", memberID);
			SQLClient client = new SQLClient(descriptor);
			string sql = "select dbo.fn_IsAdmin(" +	memberID + ")";
			DataTable dt = client.GetDataTable(sql, CommandType.Text);
		
			if (dt != null)
			{
				if (dt.Rows.Count > 0)
				{
					return Util.Util.CBool(dt.Rows[0][0], false);
				}
			}
			return false;
		}

		public static DataTable GetCurrentSubscription(int intMemberID, int intPrivateLabelID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnMember", intMemberID);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, intPrivateLabelID);
			client.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, intMemberID);

			return client.GetDataTable("up_MemberSub_List", CommandType.StoredProcedure);
		}

		public static bool IsSubscriptionTerminated(int memberID, int privateLabelID)
		{
			DataTable table = GetCurrentSubscription(memberID, privateLabelID);
			if(table != null)
			{
				if(table.Rows.Count > 0)
				{
					if (table.Rows[0]["EndDate"] != System.DBNull.Value)
					{
						//End date is not null
						return true;
					}
				}
			}
			return false;
		}

		// WFN - removed all the setters because we want to be
		//       sure of a consistent state between this object
		//       and session / member persisted data
		public int MemberID
		{
			get{return _MemberID;}
		}

		public int DomainID
		{
			get{return _DomainID;}
		}

		public bool SelfSuspendedFlag
		{
			get{return _SelfSuspendedFlag;}
		}

		public bool AdminSuspendedFlag
		{
			get{return _AdminSuspendedFlag;}
		}
		
		public int GlobalStatusMask
		{
			get{return _GlobalStatusMask;}
		}

		public int NextRegistrationActionPageID
		{
			get{return _NextRegistrationActionPageID;}
		}

		public int GenderMask
		{
			get{return _GenderMask;}
		}

		public DateTime Birthdate
		{
			get{return _Birthdate;}
		}

		public int HideMask
		{
			get{return _HideMask;}
		}

		public int PromotionID
		{
			get{return _PromotionID;}
		}

		public int RegionID
		{
			get{return _RegionID;}
		}

		public bool HasPhotoFlag
		{
			get{return _HasPhotoFlag;}
		}

		public string Username
		{
			get{return _Username;}
		}

		// mutable for subscriptions. we need a better
		// way to enforce session / member consistency
		public bool IsPayingMemberFlag
		{
			get{return _IsPayingMemberFlag;}
			set{_IsPayingMemberFlag = value;}
		}

		public string HtmlUsername
		{
			get{return _HtmlUsername;}
		}

		public bool IsAdmin
		{
			get{return _IsAdmin;}
		}
/*
		public bool IsLoggedIn
		{
			get{return _IsLoggedIn;}
		}
		*/
	}
}