using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
// PT:  Removed.  Matchnet.Lib cache wrapper is used instead
// using System.Web.Caching;
using Matchnet.Lib.Caching;

using Matchnet.Lib.Data;
using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Content
{
  public class Translator
  {
    private const int CacheTimeout = 60;
    const string KEY_DEVELOPMENTMODE = "DevelopmentMode";

    // PT:  Removed, so cache wrapper could be dropped in
    // private System.Web.Caching.Cache _Cache;
    private static Matchnet.Lib.Caching.Cache _Cache;

    private Hashtable _Tokens = new Hashtable();

    private bool _DevelopmentMode = false;
    private bool _ResourceHints = false;

    /* PT:  Constructor removed.  Modified to use the new cache wrapper.
     *
     * public Translator(System.Web.Caching.Cache cache)
     * {
     *	_Cache = cache;
     * }
     */

    public Translator( Matchnet.Lib.Caching.Cache cache )
    {
      _Cache = cache;
      _DevelopmentMode = Convert.ToBoolean(ConfigurationSettings.AppSettings.Get(KEY_DEVELOPMENTMODE));
    }

    public string Value(int resourceID, int languageID, int privateLabelID)
    {
      try 
      {
        // First off, if we are in development mode and ResourceHints are on, don't even get from the DB
        if ( _DevelopmentMode && _ResourceHints )
        {
          // Never cache hinted resources...
          return String.Format("[{0}]{1}",
            resourceID,
            LoadFromDB(resourceID, languageID, privateLabelID));

        }

        string key = "RSRC:" + resourceID + ":" + languageID + ":" + privateLabelID;
        string content = (string)_Cache.Get(key);

        if (content == null) 
        {
          content = LoadFromDB(resourceID, languageID, privateLabelID);
          if (content != null && content != string.Empty) 
          {
            _Cache.Insert(key, content, null, DateTime.Now.AddMinutes(CacheTimeout), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
          }
        }
        return ExpandTokens(content);

      } 
      catch (Exception ex)
      {
        MatchnetException exception = new MatchnetException("Unable to load resource", "Translator.Value", ex);
        throw exception;
      }
    }

    public string Value(string resourceConstant, int languageID, int privateLabelID)
    {
      try 
      {
        // First off, if we are in development mode and ResourceHints are on, don't even get from the DB
        if ( _DevelopmentMode && _ResourceHints )
        {
          // Never cache hinted resources...
          return String.Format("[{0}]{1}",
            resourceConstant,
            LoadFromDB(resourceConstant, languageID, privateLabelID));

        }

        string key = "RSRC:" + resourceConstant + ":" + languageID + ":" + privateLabelID;
        string content = (string)_Cache.Get(key);

        if (content == null) 
        {
          content = LoadFromDB(resourceConstant, languageID, privateLabelID);
          if (content != null && content != string.Empty) 
          {
            _Cache.Insert(key, content, null, DateTime.Now.AddMinutes(CacheTimeout), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
          }
        }
        return ExpandTokens(content);

      } 
      catch (Exception ex)
      {
        MatchnetException exception = new MatchnetException("Unable to load resource", "Translator.Value", ex);
        throw exception;
      }
    }

    /*
                public string Value(int resourceID, int languageID, int privateLabelID,string[] args)
                {
                    try 
                    {
                        string content = Value(resourceID, languageID, privateLabelID);

                        if (content != null && content != string.Empty) 
                        {
                            if (args != null && args.Length > 0) 
                            {
                                content = Replacements(content, args);
                            }
                        }
                        return ExpandTokens(content);
                    }
                    catch (Exception ex)
                    {
                        MatchnetException exception = new MatchnetException("Unable to load resource", "Translator.Value", ex);
                        throw exception;
                    }

                }
        */

    public string Value(string resourceConstant, int languageID, int privateLabelID,string[] args)
    {
      try 
      {
        string content = Value(resourceConstant, languageID, privateLabelID);

        if (content != null && content != string.Empty) 
        {
          if (args != null && args.Length > 0) 
          {
            content = Replacements(content, args);
          }
        }
        return ExpandTokens(content);
      }
      catch (Exception ex)
      {
        MatchnetException exception = new MatchnetException("Unable to load resource", "Translator.Value", ex);
        throw exception;
      }


    }

    /*
                public string Value(int resourceID, string[] args, PrivateLabel.PrivateLabel privateLabel)
                {
                    try 
                    {
                        string content = Value(resourceID, privateLabel.LanguageID, privateLabel.PrivateLabelID);

                        if (content != null && content != string.Empty) 
                        {
                            if (args != null && args.Length > 0) 
                            {
                                content = Replacements(content, args);
                            }
                        }
                        content = ExpandTokens(content);
                        return ExpandImageTokens(content, privateLabel);
                    }
                    catch (Exception ex)
                    {
                        MatchnetException exception = new MatchnetException("Unable to load resource", "Translator.Value", ex);
                        throw exception;
                    }

                }
        */

    public string Value(string resourceConstant, string[] args, PrivateLabel.PrivateLabel privateLabel)
    {
      try 
      {
        string content = Value(resourceConstant, privateLabel.LanguageID, privateLabel.PrivateLabelID);

        if (content != null && content != string.Empty) 
        {
          if (args != null && args.Length > 0) 
          {
            content = Replacements(content, args);
          }
        }
        content = ExpandTokens(content);
        return ExpandImageTokens(content, privateLabel);
      }
      catch (Exception ex)
      {
        MatchnetException exception = new MatchnetException("Unable to load resource", "Translator.Value", ex);
        throw exception;
      } 

    }

    /*
                public string Value(int resource, int languageID, int privateLabelID)
                {
                    try 
                    {
                        // First off, if we are in development mode and ResourceHints are on, don't even get from the DB
                        if ( _DevelopmentMode && _ResourceHints )
                        {
                            // Never cache hinted resources...
                            return String.Format("[{0}]{1}",
                                                    (int)resource,
                                                    LoadFromDB(resource, languageID, privateLabelID));

                        }

                        string key = "RSRC:" + (int)resource + ":" + languageID + ":" + privateLabelID;
                        string content = (string)_Cache.Get(key);

                        if (content == null) 
                        {
                            content = LoadFromDB(resource, languageID, privateLabelID);
                            if (content != null && content != string.Empty) 
                            {
                                _Cache.Insert(key, content, null, DateTime.Now.AddMinutes(CacheTimeout), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
                            }
                        }
                        return ExpandTokens(content);

                    } 
                    catch (Exception ex)
                    {
                        MatchnetException exception = new MatchnetException("Unable to load resource", "Translator.Value", ex);
                        throw exception;
                    }
                }
        */

    public string Value(int resource, int languageID, int privateLabelID,  string[] args) 
    {
      try 
      {
        string content = Value(resource, languageID, privateLabelID);

        if (content != null && content != string.Empty) 
        {
          if (args != null && args.Length > 0) 
          {
            content = Replacements(content, args);
          }
        }
        return ExpandTokens(content);
      }
      catch (Exception ex)
      {
        MatchnetException exception = new MatchnetException("Unable to load resource", "Translator.Value", ex);
        throw exception;
      }
    }

    public string Value(int resource, string[] args, PrivateLabel.PrivateLabel PrivateLabel) 
    {
      try 
      {
        string content = Value(resource, PrivateLabel.LanguageID, PrivateLabel.PrivateLabelID);

        if (content != null && content != string.Empty) 
        {
          if (args != null && args.Length > 0) 
          {
            content = Replacements(content, args);
          }
        }
        content = ExpandTokens(content);
        return ExpandImageTokens(content, PrivateLabel);
      }
      catch (Exception ex)
      {
        MatchnetException exception = new MatchnetException("Unable to load resource", "Translator.Value", ex);
        throw exception;
      }
    }

    public void TranslateDataTable(DataTable dt, int languageID, int privateLabelID)
    {
      dt.Columns.Add("Description", typeof(string));

      foreach (DataRow row in dt.Rows)
      {
        row["Description"] = this.Value(Translator.ConvertToResource(System.Convert.ToInt32(row["ResourceID"])), 
          languageID,
          privateLabelID);
      }
    }
		

    private string LoadFromDB(int resource, int translationID) 
    {
      SQLDescriptor descriptor = new SQLDescriptor("mnShared", 0);
      SQLClient client = new SQLClient(descriptor);

      client.AddParameter("@ResourceExID", SqlDbType.Int, ParameterDirection.Input, (int)resource);
      client.AddParameter("@TranslationID", SqlDbType.Int, ParameterDirection.Input, translationID);

      DataTable table = client.GetDataTable("up_ResourceData_Select", CommandType.StoredProcedure);

      if (table.Rows.Count > 0) 
      {
        return table.Rows[0]["Content"].ToString();	
      } 
      else 
      {
        return string.Empty;
      }
    }
        
    // Translator.Resources.RESOURCE_CONSTANT
    // replace with
    // Translator.GetResourceID("RESOURCE_CONSTANT");
    public static int GetResourceID(string resourceConstant) 
    {
      int retval = 700; // RESOURCE_NOT_FOUND
      string key = "RID:" + resourceConstant;

      object cacheObject = _Cache[ key ];
      if( cacheObject == null )
      {
        // Cache miss?  Retrieve from the DB.
        SQLDescriptor descriptor = new SQLDescriptor("mnShared", 0);
        SQLClient client = new SQLClient(descriptor);

        DataTable table =
          client.GetDataTable( String.Format( "select dbo.fn_GetResourceID('{0}') as ResourceID",
                               resourceConstant ), 
                               CommandType.Text );
        if (table.Rows.Count > 0) 
        {
          retval = Convert.ToInt32(table.Rows[0]["ResourceID"]);	
          _Cache.Insert( key, retval );
        } 
      }
      else
      {
        retval = (int) cacheObject;
      }

      return retval;
    }

    private string LoadFromDB(int resourceID, int languageID, int privateLabelID) 
    {
      /*
            create procedure up_Resource_List
            (
            @ResourceID int,
            @PrivateLabelID int = null,
            @LanguageID int
            )
            */

      string retval = string.Empty;
      SQLDescriptor descriptor = new SQLDescriptor("mnShared", 0);
      SQLClient client = new SQLClient(descriptor);

      client.AddParameter("@ResourceID", SqlDbType.Int, ParameterDirection.Input, resourceID);
      client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
      client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);

      DataTable table = client.GetDataTable("up_Resource_List", CommandType.StoredProcedure);

      if (table.Rows.Count > 0) 
      {
        retval = table.Rows[0]["Content"].ToString();	
      } 
      else 
      {
        if (_DevelopmentMode)
        {
          retval = "Missing Resource rsrc[" + resourceID.ToString() + "] ";
          retval += "lang[" + languageID.ToString() + "] ";
          retval += "plid[" + privateLabelID.ToString() + "] ";
        }
        else
        {
          retval = string.Empty;
        }
      }
      return retval;
    }

    private string LoadFromDB(string resourceConstant, int languageID, int privateLabelID) 
    {
      /*
            create procedure up_Resource_List
            (
            @ResourceID int,
            @PrivateLabelID int = null,
            @LanguageID int
            )
            */

      string retval = string.Empty;
      SQLDescriptor descriptor = new SQLDescriptor("mnShared", 0);
      SQLClient client = new SQLClient(descriptor);

      client.AddParameter("@ResourceConstant", SqlDbType.VarChar, ParameterDirection.Input, resourceConstant);
      client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
      client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);

      DataTable table = client.GetDataTable("up_Resource_List_ByConstant", CommandType.StoredProcedure);

      if (table.Rows.Count > 0) 
      {
        retval = table.Rows[0]["Content"].ToString();	
      } 
      else 
      {
        if (_DevelopmentMode)
        {
          retval = "Missing Resource rsrc[" + resourceConstant.ToString() + "] ";
          retval += "lang[" + languageID.ToString() + "] ";
          retval += "plid[" + privateLabelID.ToString() + "] ";
        }
        else
        {
          retval = string.Empty;
        }
      }

      return retval;
    }


#if false 
		private string LoadFromDB(int resource, int languageID, int privateLabelID) 
		{
			/*
			create procedure up_Resource_List
			(
			@ResourceID int,
			@PrivateLabelID int = null,
			@LanguageID int
			)
			*/

			string retval = string.Empty;
			SQLDescriptor descriptor = new SQLDescriptor("mnShared", 0);
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@ResourceID", SqlDbType.Int, ParameterDirection.Input, (int)resource);
			client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);

			DataTable table = client.GetDataTable("up_Resource_List", CommandType.StoredProcedure);

			if (table.Rows.Count > 0) 
			{
				retval = table.Rows[0]["Content"].ToString();	
			} 
			else 
			{
				if (_DevelopmentMode)
				{
					retval = "Missing Resource rsrc[" + resource.ToString() + "] ";
					retval += "lang[" + languageID.ToString() + "] ";
					retval += "plid[" + privateLabelID.ToString() + "] ";
				}
				else
				{
					retval = string.Empty;
				}
			}
			return retval;
		}
#endif
		
    public void AddToken(string name, string val)
    {
      if ( name == "RESOURCEHINTS" )
      {
        _ResourceHints = true;
        return;
      }

      _Tokens.Add("(("+name+"))",val);
    }


    public string ExpandTokens(string text)
    {
      IDictionaryEnumerator item = _Tokens.GetEnumerator();
      while (item.MoveNext())
      {
        if ( item.Key != null && item.Value != null )
          text = text.Replace(item.Key.ToString(), item.Value.ToString());
      }
			
      return text;
    }

		
    private string Replacements(string text, string[] args)
    {
      int pos = text.IndexOf("%s");
      int lastPos = 0;
      int counter = 0;

      if (pos < 0) 
      {
        return text;
      }

      if (args == null || args.Length < 1) 
      {
        return text;
      }
			
      StringBuilder builder = new StringBuilder();
	
      while (pos > -1) 
      {
        builder.Append(text.Substring(lastPos, pos - lastPos));
        if (counter < args.Length) 
        {
          builder.Append(args[counter]);
          counter++;
        } 
        else 
        {
          break;
        }
        lastPos = pos + 2;
        if (lastPos > text.Length) 
        {
          break;
        }
        pos = text.IndexOf("%s", lastPos);
      }
      if (lastPos < text.Length) 
      {
        builder.Append(text.Substring(lastPos));
      }

      return builder.ToString();
    }
		
    private string ExpandImageTokens(string text, PrivateLabel.PrivateLabel PrivateLabel)
    {
      int pos = text.IndexOf("((image:");
      int lastPos = 0;
      int endPos = 0;
      int appId = 0;
      string fileName = string.Empty;
      string token = string.Empty;
			
      if (pos < 0) 
      {
        return text;
      }

      StringBuilder builder = new StringBuilder();
	
      while (pos > -1) 
      {
				
        builder.Append(text.Substring(lastPos, pos - lastPos));
				
        endPos = text.IndexOf("))", pos);
				
        if (endPos > -1)
        {
          token = text.Substring(pos + 2, endPos - pos - 2);

          int colon1 = token.IndexOf(":");
          int colon2 = token.LastIndexOf(":");
					
          if (colon1 != -1 && colon2 != -1)
          {
            appId = System.Convert.ToInt32(token.Substring(colon1 + 1, colon2 - colon1 - 1));
            fileName = token.Substring(colon2 + 1, token.Length - colon2 - 1);
            builder.Append(PrivateLabel.WebSrc(fileName, appId, false));
          }

          lastPos = endPos + 2;
          if (lastPos > text.Length) 
          {
            break;
          }
          pos = text.IndexOf("((image:", lastPos);
        }
        else
          break;

      }
      if (lastPos < text.Length) 
      {
        builder.Append(text.Substring(lastPos));
      }

      return builder.ToString();
    }



    public static int ConvertToResource( object r )
    {
      // Resources resourceDefault = Resources.RESOURCE_NOT_FOUND;
      int resourceDefault = Translator.GetResourceID( "RESOURCE_NOT_FOUND" );

      try
      {
        resourceDefault = System.Convert.ToInt32(r);
        return resourceDefault;
	
        // FIXME: No more safety check here. 	This was required because of Randy's changes requiring
        // removal of the Resources data type
        //				if (Enum.GetName(typeof(Resources), resourceDefault) != null)
        //					return resourceDefault;
        //				else
        //          return 700;
        //					return Resources.RESOURCE_NOT_FOUND;
      }
      catch(Exception ex)
      {
        System.Diagnostics.Trace.WriteLine(ex);
        return resourceDefault;
      }
    }


    /*
                public enum Resources : int
                {
                    ERROR_SUCCESS = 0,
                    TEASE_CATEGORY_SINCERE = 18,
                    TEASE_CATEGORY_ROMANTIC = 20,
                    TEASE_CATEGORY_HUMOROUS = 22,
                    TEASE_CATEGORY_FLIRTATIOUS = 24,
                    TEASE_CATEGORY_POEMS = 26,
                    TEASE_CATEGORY_MOVIE_LINES = 28,
                    TEASE_CATEGORY_HOLIDAY_SEASONAL = 30,
                    TEASE_CATEGORY_SONGS = 32,
                    TEASE_SINCERE_COMMON = 34,
                    TEASE_SINCERE_SERIOUS = 36,
                    TEASE_SINCERE_CONVERSATION = 38,
                    TEASE_SINCERE_CLICKED = 40,
                    TEASE_SINCERE_PERFECT = 42,
                    TEASE_SINCERE_INTRIGUED = 44,
                    TEASE_ROMANTIC_FAR_APART = 46,
                    TEASE_ROMANTIC_RESPONSE = 48,
                    TEASE_ROMANTIC_BOOK_BY_COVER = 50,
                    TEASE_ROMANTIC_STRANDED = 52,
                    TEASE_ROMANTIC_MILLION_MILES = 54,
                    TEASE_ROMANTIC_BAGELS_AND_PAPER = 56,
                    TEASE_HUMOROUS_REALITY = 58,
                    TEASE_HUMOROUS_ED_MCMAHON = 60,
                    TEASE_HUMOROUS_SCRABBLE = 62,
                    TEASE_HUMOROUS_PSYCHIC = 64,
                    TEASE_HUMOROUS_CRAZY = 66,
                    TEASE_HUMOROUS_DUSTPAN = 68,
                    TEASE_FLIRTATIOUS_SMILE = 70,
                    TEASE_FLIRTATIOUS_OPPOSITES = 72,
                    TEASE_FLIRTATIOUS_ATTRACTION_TEST = 74,
                    TEASE_FLIRTATIOUS_BURNING_MONITOR = 76,
                    TEASE_FLIRTATIOUS_HIGH_SPEED = 78,
                    TEASE_FLIRTATIOUS_BOWLING = 80,
                    TEASE_FLIRTATIOUS_RESUME = 82,
                    TEASE_POEMS_FACE = 84,
                    TEASE_POEMS_YOU_ME_US = 86,
                    TEASE_POEMS_SHAKESPEARE = 88,
                    TEASE_POEMS_MOORE = 90,
                    TEASE_POEMS_TAYLOR = 92,
                    TEASE_POEMS_ANONYMOUS = 94,
                    TEASE_MOVIES_CASABLANCA = 96,
                    TEASE_MOVIES_HAVE_AND_HAVE_NOT = 98,
                    TEASE_MOVIES_MAGUIRE = 100,
                    TEASE_MOVIES_GONE_WITH_THE_WIND = 102,
                    TEASE_MOVIES_AS_GOOD_AS_IT_GETS = 104,
                    TEASE_MOVIES_ACROSS_PACIFIC = 106,
                    TEASE_MOVIES_PUBLIC_ENEMY = 108,
                    TEASE_HOLIDAY_INDEPENDENCE = 110,
                    TEASE_HOLIDAY_SMILE = 112,
                    TEASE_HOLIDAY_BOO = 114,
                    TEASE_HOLIDAY_WROTE_BACK = 116,
                    TEASE_HOLIDAY_MENORAH = 118,
                    TEASE_HOLIDAY_HAPPY_HOLIDAYS = 120,
                    TEASE_HOLIDAY_NEW_YEAR = 122,
                    TEASE_HOLIDAY_KISSING = 124,
                    TEASE_SONGS_ONE_THAT_I_WANT = 126,
                    TEASE_SONGS_SOMETHING_GOOD = 128,
                    TEASE_SONGS_TOO_GOOD = 130,
                    TEASE_SONGS_BIRDS_APPEAR = 132,
                    TEASE_SONGS_TEDDY_BEAR = 134,
                    TEASE_SONGS_INTO_MY_LIFE = 136,
                    TEASE_SONGS_HAVE_SOME_FUN = 138,
                    TEASE_POEMS_SHAKESPEAR_2 = 140,
                    DUPLICATE_CONSTANT = 216,
                    TRAN_ADMIN_SUSPENDED = 218,
                    TRAN_SELF_SUSPENDED = 220,
                    TRAN_CHECK_PERIOD_ENDED = 222,
                    TRAN_UNABLE_TO_RENEW_PAST_GRACE_PERIOD = 224,
                    TEST_RESOURCE = 228,
                    RESOURCE_NOT_FOUND = 700,
                    EMAIL_ADDRESS = 2001,
                    PASSWORD = 2002,
                    RETRIEVE_PASSWORD = 2008,
                    MEMBER_PASSWORD_INFORMATION = 2502,
                    MNCHARGE___PROVIDER_STATUSID_1001_13004 = 13004,
                    MNCHARGE___PROVIDER_STATUSID_1003_13005 = 13005,
                    MNCHARGE___PROVIDER_STATUSID_1004_13006 = 13006,
                    MNCHARGE___PROVIDER_STATUSID_1006_13008 = 13008,
                    MNCHARGE___PROVIDER_STATUSID_1007_13009 = 13009,
                    MNCHARGE___PROVIDER_STATUSID_1009_13011 = 13011,
                    MNCHARGE___PROVIDER_STATUSID_1010_13012 = 13012,
                    MNCHARGE___PROVIDER_STATUSID_2002_13013 = 13013,
                    MNCHARGE___PROVIDER_STATUSID_2003_13014 = 13014,
                    MNCHARGE___PROVIDER_STATUSID_2004_13015 = 13015,
                    MNCHARGE___PROVIDER_STATUSID_2005_13016 = 13016,
                    MNCHARGE___PROVIDER_STATUSID_2006_13017 = 13017,
                    MNCHARGE___PROVIDER_STATUSID_2007_13018 = 13018,
                    MNCHARGE___PROVIDER_STATUSID_2008_13019 = 13019,
                    MNCHARGE___PROVIDER_STATUSID_2009_13020 = 13020,
                    MNCHARGE___PROVIDER_STATUSID_2010_13021 = 13021,
                    MNCHARGE___PROVIDER_STATUSID_2011_13022 = 13022,
                    MNCHARGE___PROVIDER_STATUSID_2012_13023 = 13023,
                    MNCHARGE___PROVIDER_STATUSID_2013_13024 = 13024,
                    MNCHARGE___PROVIDER_STATUSID_2014_13025 = 13025,
                    MNCHARGE___PROVIDER_STATUSID_2015_13026 = 13026,
                    MNCHARGE___PROVIDER_STATUSID_2016_13027 = 13027,
                    MNCHARGE___PROVIDER_STATUSID_2017_13028 = 13028,
                    MNCHARGE___PROVIDER_STATUSID_2018_13029 = 13029,
                    MNCHARGE___PROVIDER_STATUSID_2019_13030 = 13030,
                    MNCHARGE___PROVIDER_STATUSID_2023_13034 = 13034,
                    MNCHARGE___PROVIDER_STATUSID_2024_13035 = 13035,
                    MNCHARGE___PROVIDER_STATUSID_2025_13036 = 13036,
                    MNCHARGE___PROVIDER_STATUSID_2026_13037 = 13037,
                    MNCHARGE___PROVIDER_STATUSID_2027_13038 = 13038,
                    MNCHARGE___PROVIDER_STATUSID_2028_13039 = 13039,
                    MNCHARGE___PROVIDER_STATUSID_2029_13040 = 13040,
                    MNCHARGE___PROVIDER_STATUSID_2030_13041 = 13041,
                    MNCHARGE___PROVIDER_STATUSID_2031_13042 = 13042,
                    MNCHARGE___PROVIDER_STATUSID_2032_13043 = 13043,
                    MNCHARGE___PROVIDER_STATUSID_2033_13044 = 13044,
                    MNCHARGE___PROVIDER_STATUSID_2034_13045 = 13045,
                    MNCHARGE___PROVIDER_STATUSID_2035_13046 = 13046,
                    MNCHARGE___PROVIDER_STATUSID_2036_13047 = 13047,
                    MNCHARGE___PROVIDER_STATUSID_2038_13049 = 13049,
                    MNCHARGE___PROVIDER_STATUSID_2039_13050 = 13050,
                    MNCHARGE___PROVIDER_STATUSID_2040_13051 = 13051,
                    MNCHARGE___PROVIDER_STATUSID_2041_13052 = 13052,
                    MNCHARGE___PROVIDER_STATUSID_2044_13053 = 13053,
                    MNCHARGE___PROVIDER_STATUSID_3002_13054 = 13054,
                    MNCHARGE___PROVIDER_STATUSID_3003_13055 = 13055,
                    MNCHARGE___PROVIDER_STATUSID_3004_13056 = 13056,
                    MNCHARGE___PROVIDER_STATUSID_3006_13057 = 13057,
                    MNCHARGE___PROVIDER_STATUSID_3007_13058 = 13058,
                    MNCHARGE___PROVIDER_STATUSID_3008_13059 = 13059,
                    MNCHARGE___PROVIDER_STATUSID_3009_13060 = 13060,
                    MNCHARGE___PROVIDER_STATUSID_3010_13061 = 13061,
                    MNCHARGE___PROVIDER_STATUSID_3011_13062 = 13062,
                    MNCHARGE___PROVIDER_STATUSID_3012_13063 = 13063,
                    MNCHARGE___PROVIDER_STATUSID_3013_13064 = 13064,
                    MNCHARGE___PROVIDER_STATUSID_3014_13065 = 13065,
                    MNCHARGE___PROVIDER_STATUSID_3015_13066 = 13066,
                    MNCHARGE___PROVIDER_STATUSID_3016_13067 = 13067,
                    MNCHARGE___PROVIDER_STATUSID_3017_13068 = 13068,
                    MNCHARGE___PROVIDER_STATUSID_3018_13069 = 13069,
                    MNCHARGE___PROVIDER_STATUSID_3019_13070 = 13070,
                    MNCHARGE___PROVIDER_STATUSID_3020_13071 = 13071,
                    MNCHARGE___PROVIDER_STATUSID_3021_13072 = 13072,
                    MNCHARGE___PROVIDER_STATUSID_3022_13073 = 13073,
                    MNCHARGE___PROVIDER_STATUSID_3023_13074 = 13074,
                    MNCHARGE___PROVIDER_STATUSID_3024_13075 = 13075,
                    MNCHARGE___GENERAL_DATABASE_FAILURE_13076 = 13076,
                    MNCHARGE___CHARGE_RETRIES_EXPIRED_13077 = 13077,
                    MNCHARGE___CREDIT_AMOUNT_EXCEEDS_MAX_CREDIT_AMOUNT_13078 = 13078,
                    MNCHARGE___PROVIDER_STATUSCODE_NOT_FOUND_13079 = 13079,
                    PROVIDER_OR_MERCHANT_IS_NOT_CONFIGURED_PROPERLY = 13080,
                    NO_PENDING_CHARGE = 13081,
                    MNCHARGE___DATABASEERROR_13082 = 13082,
                    CHECK_TERMS_AND_CONFITIONS = 13083,
                    CREDIT_CARD_TERMS = 13088,
                    CHARGE_APPROVED_CREDIT_CARD = 13089,
                    CHARGE_IN_PROGRESS = 13090,
                    RENEWAL_RATE = 13092,
                    CHECKS_ADDITIONAL_TERMS_AND_CONDITIONS = 13093,
                    CHARGEAPPROVED2__FOR_JDATE_CO_IL__13094 = 13094,
                    PACKAGEID2 = 13095,
                    PACKAGEID4 = 13096,
                    PACKAGEID6 = 13097,
                    PACKAGEID8 = 13098,
                    PACKAGEID10 = 13099,
                    PACKAGEID12 = 13101,
                    PACKAGEID14 = 13102,
                    PACKAGEID16 = 13103,
                    AMERICAN_EXPRESS_CREDIT_CARD_TYPE = 13104,
                    CARTE_BLANCHE_CREDIT_CARD_TYPE = 13105,
                    DINERS_CREDIT_CARD_TYPE = 13106,
                    DISCOVER_CREDIT_CARD_TYPE = 13107,
                    JCB_CREDIT_CARD_TYPE = 13108,
                    MASTERCARD_CREDIT_CARD_TYPE = 13109,
                    VISA_CARD_CREDIT_CARD_TYPE = 13110,
                    CREDIT_CARD_POPUP_INTRODUCTION = 13111,
                    CREDIT_CARD_INTRODUCTION = 13112,
                    PACKAGEID18 = 13113,
                    CHARGE_PENDING = 13201,
                    OPEN_SUBSCRIPTION = 13202,
                    CLOSED_PLAN = 13203,
                    MNCHARGE___PROVIDER_STATUSID_1011_13204 = 13204,
                    MNCHARGE___PROVIDER_STATUSID_1012_13205 = 13205,
                    MNCHARGE___PROVIDER_STATUSID_1013_13206 = 13206,
                    MNCHARGE___PROVIDER_STATUSID_1014_13207 = 13207,
                    MNCHARGE___PROVIDER_STATUSID_1015_13208 = 13208,
                    MNCHARGE___PROVIDER_STATUSID_1016_13209 = 13209,
                    MNCHARGE___PROVIDER_STATUSID_1017_13210 = 13210,
                    MNCHARGE___PROVIDER_STATUSID_1018_13211 = 13211,
                    MALE = 69000,
                    FEMALE = 69001,
                    COUNTRY_X = 69026,
                    MAXIMUM_PHOTO_COUNT = 69039,
                    EMAIL_ALREADY_IN_USE = 400000,
                    OTHER = 517238,
                    SEEKING_MALE = 518182,
                    SEEKING_FEMALE = 518183,
                    SEEKING_A__518254 = 518254,
                    COUNTRY__518256 = 518256,
                    STATE__518257 = 518257,
                    TO_ = 518258,
                    CITY__518273 = 518273,
                    AGE_ = 518540,
                    CONTACTUS = 518551,
                    FIRSTNAME = 518560,
                    AGE__518666 = 518666,
                    SPAMWARNING = 518683,
                    HOME = 518714,
                    TAKING_A_BREAK___VACATION_518751 = 518751,
                    SEEKING = 518765,
                    PLEASE_ENTER_THE_USERNAME_OR_MEMBER_NUMBER_OF_THE_PERSON_YOU_WANT_TO_LOOK_UP__518766 = 518766,
                    CHARGEPENDINGINSTRUCTIONS = 518780,
                    SUBSCRIBE = 518783,
                    CREDIT_CARD_PURCHASE = 519027,
                    SUBSCRIPTION_END_INTRO = 519032,
                    NO_CURRENT_SUBSCRIPTION = 519035,
                    SUBSCRIPTION_ALREADY_TERMINATED = 519036,
                    ADMIN_SUSPENDED_ACTION = 519041,
                    CHOOSE_AN_INITIAL_TERM___MONTHLY_SUBSCRIPTION_RATE__519064 = 519064,
                    ENTER_YOUR_CREDIT_CARD_INFORMATION__519066 = 519066,
                    PLEASE_MAKE_SURE_THAT_YOUR_NAME_AND_ADDRESS_ARE_THE_SAME_AS_ON_YOUR_CREDIT_CARD_BILLING_STATEMENT = 519067,
                    OTHER_PAYMENT_METHODS = 519068,
                    PAY_BY_CHECK = 519069,
                    PAYMENT_ALTERNATIVES = 519070,
                    YOUR_PASSWORD_WILL_BE_SENT_TO_YOU_IMMEDIATELY = 519071,
                    SUBMIT = 519072,
                    SUBSCRIPTION_TERMS = 519073,
                    TERMS_AND_CONDITIONS_OF_PURCHASE = 519074,
                    FOUND_MY_SOUL_MATE_ON_THE_SITE = 519075,
                    FOUND_MY_SOUL_MATE_ON_MY_OWN = 519076,
                    TOO_MANY_PEOPLE_ARE_CONTACTING_ME = 519077,
                    GIVING_UP___COULDN_T_FIND_A_SOUL_MATE_519078 = 519078,
                    NOBODY_CONTACTS_ME = 519079,
                    NOBODY_REPLIES_TO_ME = 519080,
                    I_DIDN_T_LIKE_THE_MATCHES_I_GOT_519081 = 519081,
                    TOO_EXPENSIVE = 519082,
                    JUST_TESTING_THE_SERVICE = 519083,
                    DON_T_HAVE_TIME_TO_DATE_RIGHT_NOW_519084 = 519084,
                    UNABLE_TO_UPLOAD_A_PHOTO = 519085,
                    DIFFICULTY_USING_THE_SITE = 519086,
                    TOO_FEW_MATCHES_IN_MY_AREA = 519087,
                    CUSTOMER_SERVICE_ISSUES = 519088,
                    GOING_TO_TRY_ANOTHER_SERVICE = 519089,
                    TERMINATE_SUBSCRIPTION_1 = 519090,
                    TERMINATE_SUBSCRIPTION_2 = 519091,
                    SUBMIT_REQUEST = 519092,
                    YOUR_REQUEST = 519093,
                    TERMINATE_SUBSCRIPTION_RESPONSE_1 = 519094,
                    TERMINATE_SUBSCRIPTION_RESPONSE_2 = 519095,
                    TERMINATE_SUBSCRIPTION_RESPONSE_3 = 519096,
                    REFUND_FOR_BOUNCED_EMAIL = 519097,
                    ADMIN_MONTHLY_CHANGE = 519098,
                    ADMIN_UNITS_CHANGE = 519099,
                    COMPLIMENTARY = 519100,
                    TRANSACTION_RECORD = 519126,
                    YOU_HAVE_SUBSCRIPTION_PRIVILEGES_FROM__519127 = 519127,
                    AUTOMATIC_RENEWAL_OF_YOUR_ACCOUNT_WILL_END_ON__519128 = 519128,
                    INVALID_PACKAGE = 519137,
                    DEFAULTAWAYMESSAGE = 519138,
                    EMAIL_ADDRESS_NOT_ALLOWED = 519238,
                    PACKAGEID20 = 519292,
                    PACKAGEID22 = 519293,
                    PACKAGEID24 = 519294,
                    PACKAGEID26 = 519295,
                    PACKAGEID28 = 519296,
                    PACKAGEID30 = 519297,
                    PACKAGEID32 = 519298,
                    PACKAGEID34 = 519299,
                    PACKAGEID36 = 519300,
                    PACKAGEID38 = 519301,
                    PACKAGEID40 = 519302,
                    PACKAGEID42 = 519303,
                    PACKAGEID44 = 519304,
                    PACKAGEID46 = 519305,
                    PACKAGEID48 = 519306,
                    PACKAGEID50 = 519307,
                    PACKAGEID52 = 519308,
                    BOUNCED_EMAIL = 519354,
                    VERIFY_EMAIL_LETTER_BODY = 519361,
                    SUBSCRIBE_NOW_TO_CONNECT_WITH_ANY_MEMBER_YOU_CHOOSE__519366 = 519366,
                    INVALIDEMAIL = 519385,
                    MEMBERS_YOU_VE_TEASED_519402 = 519402,
                    WITHIN = 519408,
                    CITY = 519410,
                    STATE = 519412,
                    ZIP_CODE = 519423,
                    MEMBERS_YOU_VE_VIEWED_519430 = 519430,
                    MNCHARGE___PROVIDER_STATUSID_4002_519432 = 519432,
                    MNCHARGE___PROVIDER_STATUSID_4005_519433 = 519433,
                    MNCHARGE___PROVIDER_STATUSID_4020_519434 = 519434,
                    MNCHARGE___PROVIDER_STATUSID_4051_519435 = 519435,
                    PACKAGEID58 = 519438,
                    PACKAGEID60 = 519439,
                    MNCHARGE___PROVIDER_STATUSID_4003_519454 = 519454,
                    MNCHARGE___PROVIDER_STATUSID_4004_519455 = 519455,
                    MNCHARGE___PROVIDER_STATUSID_4006_519456 = 519456,
                    MNCHARGE___PROVIDER_STATUSID_4007_519457 = 519457,
                    MNCHARGE___PROVIDER_STATUSID_4008_519458 = 519458,
                    MNCHARGE___PROVIDER_STATUSID_4009_519459 = 519459,
                    MNCHARGE___PROVIDER_STATUSID_4010_519460 = 519460,
                    MNCHARGE___PROVIDER_STATUSID_4011_519461 = 519461,
                    MNCHARGE___PROVIDER_STATUSID_4012_519462 = 519462,
                    MNCHARGE___PROVIDER_STATUSID_4013_519463 = 519463,
                    MNCHARGE___PROVIDER_STATUSID_4014_519464 = 519464,
                    MNCHARGE___PROVIDER_STATUSID_4015_519465 = 519465,
                    MNCHARGE___PROVIDER_STATUSID_4016_519466 = 519466,
                    MNCHARGE___PROVIDER_STATUSID_4017_519467 = 519467,
                    MNCHARGE___PROVIDER_STATUSID_4018_519468 = 519468,
                    MNCHARGE___PROVIDER_STATUSID_4019_519469 = 519469,
                    MNCHARGE___PROVIDER_STATUSID_4021_519470 = 519470,
                    MNCHARGE___PROVIDER_STATUSID_4023_519471 = 519471,
                    MNCHARGE___PROVIDER_STATUSID_4024_519472 = 519472,
                    MNCHARGE___PROVIDER_STATUSID_4025_519473 = 519473,
                    MNCHARGE___PROVIDER_STATUSID_4026_519474 = 519474,
                    MNCHARGE___PROVIDER_STATUSID_4027_519475 = 519475,
                    MNCHARGE___PROVIDER_STATUSID_4028_519476 = 519476,
                    MNCHARGE___PROVIDER_STATUSID_4029_519477 = 519477,
                    MNCHARGE___PROVIDER_STATUSID_4030_519478 = 519478,
                    MNCHARGE___PROVIDER_STATUSID_4031_519479 = 519479,
                    MNCHARGE___PROVIDER_STATUSID_4032_519480 = 519480,
                    MNCHARGE___PROVIDER_STATUSID_4033_519481 = 519481,
                    MNCHARGE___PROVIDER_STATUSID_4034_519482 = 519482,
                    MNCHARGE___PROVIDER_STATUSID_4035_519483 = 519483,
                    MNCHARGE___PROVIDER_STATUSID_4036_519484 = 519484,
                    MNCHARGE___PROVIDER_STATUSID_4037_519485 = 519485,
                    MNCHARGE___PROVIDER_STATUSID_4038_519486 = 519486,
                    MNCHARGE___PROVIDER_STATUSID_4039_519487 = 519487,
                    MNCHARGE___PROVIDER_STATUSID_4040_519488 = 519488,
                    MNCHARGE___PROVIDER_STATUSID_4041_519489 = 519489,
                    MNCHARGE___PROVIDER_STATUSID_4042_519490 = 519490,
                    MNCHARGE___PROVIDER_STATUSID_4043_519491 = 519491,
                    MNCHARGE___PROVIDER_STATUSID_4044_519492 = 519492,
                    MNCHARGE___PROVIDER_STATUSID_4045_519493 = 519493,
                    MNCHARGE___PROVIDER_STATUSID_4046_519494 = 519494,
                    MNCHARGE___PROVIDER_STATUSID_4047_519495 = 519495,
                    MNCHARGE___PROVIDER_STATUSID_4048_519496 = 519496,
                    MNCHARGE___PROVIDER_STATUSID_4049_519497 = 519497,
                    MNCHARGE___PROVIDER_STATUSID_4050_519498 = 519498,
                    MNCHARGE___PROVIDER_STATUSID_4052_519499 = 519499,
                    MNCHARGE___PROVIDER_STATUSID_4053_519500 = 519500,
                    MNCHARGE___PROVIDER_STATUSID_4054_519501 = 519501,
                    MNCHARGE___PROVIDER_STATUSID_4055_519502 = 519502,
                    MNCHARGE___PROVIDER_STATUSID_4056_519503 = 519503,
                    MNCHARGE___PROVIDER_STATUSID_4057_519504 = 519504,
                    MNCHARGE___PROVIDER_STATUSID_4058_519505 = 519505,
                    MNCHARGE___PROVIDER_STATUSID_4059_519506 = 519506,
                    MNCHARGE___PROVIDER_STATUSID_4060_519507 = 519507,
                    MNCHARGE___PROVIDER_STATUSID_4061_519508 = 519508,
                    MNCHARGE___PROVIDER_STATUSID_4062_519509 = 519509,
                    MNCHARGE___PROVIDER_STATUSID_4063_519510 = 519510,
                    MNCHARGE___PROVIDER_STATUSID_4064_519511 = 519511,
                    MNCHARGE___PROVIDER_STATUSID_4065_519512 = 519512,
                    MNCHARGE___PROVIDER_STATUSID_4066_519513 = 519513,
                    MNCHARGE___PROVIDER_STATUSID_4067_519514 = 519514,
                    MNCHARGE___PROVIDER_STATUSID_4068_519515 = 519515,
                    MNCHARGE___PROVIDER_STATUSID_4069_519516 = 519516,
                    MNCHARGE___PROVIDER_STATUSID_4070_519517 = 519517,
                    MNCHARGE___PROVIDER_STATUSID_4071_519518 = 519518,
                    MNCHARGE___PROVIDER_STATUSID_4072_519519 = 519519,
                    MNCHARGE___PROVIDER_STATUSID_4073_519520 = 519520,
                    MNCHARGE___PROVIDER_STATUSID_4074_519521 = 519521,
                    MNCHARGE___PROVIDER_STATUSID_4075_519522 = 519522,
                    MNCHARGE___PROVIDER_STATUSID_4076_519523 = 519523,
                    MNCHARGE___PROVIDER_STATUSID_4077_519524 = 519524,
                    MNCHARGE___PROVIDER_STATUSID_4078_519525 = 519525,
                    NO_SUSCRIPTION = 519529,
                    ACH_TERMS = 519530,
                    INTERNET_CHECK_FAQ = 519534,
                    TELECHECK_PRIVACY_POLICY = 519535,
                    MICR_INSTRUCTIONS = 519537,
                    TELECHECK_DECLINE_000804 = 519539,
                    TELECHECK_DECLINE_000805 = 519541,
                    TELECHECK_DECLINE_000807 = 519542,
                    TELECHECK_CONFIRMATION_LETTER = 519543,
                    PACKAGEID70 = 519641,
                    LASTNAME = 519649,
                    ADDRESS_LINE_1 = 519650,
                    ADDRESS_LINE_2 = 519651,
                    TELEPHONE = 519652,
                    CHECK_NUMBER = 519653,
                    DRIVERS_LIC____519654 = 519654,
                    STATE_OF_ISSUE = 519655,
                    I_AUTHORIZE_THIS_TRANSACTION = 519656,
                    PHONE_NUMBER = 519657,
                    ADDRESS = 519658,
                    CREDIT_CARD_TYPE = 519659,
                    CREDIT_CARD_NUMBER = 519660,
                    EXPIRATION_DATE = 519661,
                    INITIAL_SUBSCRIPTION_TERM = 519662,
                    INITIAL_COST = 519663,
                    GUARANTEED_MONTHLY_RENEWAL_RATE__519664 = 519664,
                    CLICK_BELOW_TO_RE_SUBSCRIBE_USING_BILLING_METHOD_ON_FILE__519665 = 519665,
                    I_AGREE__PLEASE_PROCESS_MY_PURCHASE_519666 = 519666,
                    BUY_SUBSCRIPTION_WITH_ANOTHER_METHOD_OF_PAYMENT = 519667,
                    CHANGE_EXTEND_YOUR_SUBSCRIPTION_PLAN_519668 = 519668,
                    TERMINATE_YOUR_SUBSCRIPTION = 519669,
                    CHECK = 519670,
                    HELP___ADVICE_519695 = 519695,
                    YOUR_ONLINE_CHECK = 519746,
                    CLICK_HERE_TO_CONTINUE = 519760,
                    COSTS = 519795,
                    MEMBER_SERVICES = 519796,
                    PRIVACY = 519797,
                    DATING_SAFETY = 519799,
                    LOGOUT = 519800,
                    CLICK_HERE_TO_PAY_BY_CREDIT_CARD__519811 = 519811,
                    RETURN_CHECK_FEE_BY_STATE__519812 = 519812,
                    X__FEE_SHOWN_OR_THE_MAXIMUM_AMOUNT_PERMITTED_BY_LAW__519813 = 519813,
                    X___CHECK_WRITER_ALSO_RESPONSIBLE_FOR_ALL_OTHER_COSTS_OF_COLLECTION__519814 = 519814,
                    TRANSACTION_AUTHORIZATION__519815 = 519815,
                    TELECHECK_HEADER = 519816,
                    CHECK_RETURN_FEES = 519817,
                    MONTHABBR = 519818,
                    CHAT_ROOMS = 519846,
                    NAV_WHOSONLINE = 519847,
                    LOOK_UP_PROFILE = 519848,
                    PREFERENCES = 519849,
                    MATCHES = 519850,
                    PROFILE = 519852,
                    PHOTOS = 519853,
                    YOUR = 519855,
                    WELCOME = 519856,
                    QUICK_SEARCH = 519876,
                    SHOW_ME = 519888,
                    PASSWORD__519931 = 519931,
                    I_M_AWAY_519956 = 519956,
                    CURRENT_STATUS = 519957,
                    PRESET_MESSAGE = 519958,
                    CUSTOM_MESSAGE = 519959,
                    UPDATE_AWAY_STATUS = 519960,
                    NAVHOME = 519992,
                    PR___MEDIA_RELATIONS_520027 = 520027,
                    INVESTOR_RELATIONS = 520028,
                    AFFILIATE_PROGRAM = 520029,
                    SHOW_ME1 = 520049,
                    SHOW_ME2 = 520050,
                    NAVLOGIN = 520051,
                    EMAILADDRESS__520063 = 520063,
                    EMAIL_US = 520064,
                    TERMS_OF_SERVICE = 520065,
                    ACTIVATE_MEMBERSHIP = 520066,
                    IN_THE_NEWS = 520067,
                    ABOUT = 520069,
                    FRMVALIDATION1 = 520082,
                    FRMVALIDATION2 = 520083,
                    FRMVALIDATION3 = 520084,
                    FRMVALIDATION4 = 520085,
                    FRMVALIDATION5 = 520086,
                    FRMVALIDATION6 = 520087,
                    FRMVALIDATION7 = 520088,
                    FRMVALIDATION8 = 520089,
                    FRMVALIDATION9 = 520090,
                    FRMVALIDATION10 = 520091,
                    FRMVALIDATION11 = 520092,
                    FRMVALIDATION12 = 520093,
                    FRMVALIDATION13 = 520094,
                    FRMVALIDATION14 = 520095,
                    FRMVALIDATION15 = 520096,
                    FRMVALIDATION16 = 520097,
                    FRMVALIDATION17 = 520098,
                    FRMVALIDATION18 = 520099,
                    YEAR = 520105,
                    DAY = 520107,
                    HOUR = 520108,
                    MINUTE = 520109,
                    JOBS = 520130,
                    LOOKUP_MEMBER___NEED_HELP_TEXT_520189 = 520189,
                    LOOK_UP_BY_MEMBER_NUMBER = 520192,
                    PLEASE_ENTER_THE_MEMBER_NUMBER_OF_THE_PERSON_YOU_WANT_TO_LOOK_UP__520193 = 520193,
                    LOOK_UP_BY_USERNAME = 520194,
                    PLEASE_ENTER_THE_USERNAME_OF_THE_PERSON_YOU_WANT_TO_LOOK_UP__520195 = 520195,
                    NEED_HELP_FINDING_A_MEMBER__520198 = 520198,
                    MEMBERS_YOU_VE_HOT_LISTED_520199 = 520199,
                    MEMBERS_YOU_VE_EMAILED_520200 = 520200,
                    MATCHES_SENT_TO_YOU_VIA_EMAIL = 520201,
                    NAVEMAIL = 520250,
                    NAVHOTLISTS = 520259,
                    NAVSEARCH = 520275,
                    PACKAGEID110 = 520382,
                    PACKAGEID112 = 520383,
                    SEARCH_LOCATION__520449 = 520449,
                    NAV_CONTACTUS = 520702,
                    PACKAGEID114 = 520716,
                    INVALID_LOGIN_CREDENTIALS = 520785,
                    PACKAGEID115 = 520791,
                    ACTIVATE_YOUR_MEMBERSHIP = 520804,
                    OUR_MISSION = 520818,
                    EVENTS___TRAVEL = 520820,
                    FROM__1 = 520990,
                    YOU_ARE_NOT_AUTHORIZED_TO_SEE_THIS_PAGE = 521160,
                    REASON_IS_REQUIRED = 521176,
                    WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X = 521328,
                    NOW_WORKING_ON_MEMBERID = 521342,
                    PASSWORD_SENT_TO = 521356,
                    EMAIL_VERIFICATION_LETTER_SENT_TO__S = 521370,
                    BANK_ROUTING_NUMBER = 521442,
                    MTF = 521452,
                    FTM = 521454,
                    SEEKING_MTF = 521460,
                    SEEKING_FTM = 521462,
                    YOU_HAVE_EXCEEDED_THE_MAXIMUM_NUMBER_OF_CHARGES_IN_A_24_HOUR_PERIOD = 521538,
                    PL_CHECK_WARNING = 521636,
                    MNCHARGE_LIMIT_EXCEEDED = 521642,
                    FREETEXT_NOT_APPROVED = 521702,
                    ALREADY_A_MEMBER_ = 521710,
                    CLICK_HERE_TO_LOGIN = 521756,
                    SITE_FOOTER = 521810,
                    ACCOUNT_NUMBER = 521814,
                    BANK_NAME = 521816,
                    PACKAGEID116 = 521824,
                    UNABLE_TO_PROCESS__YOUR_CARD_APPEARS_TO_BE_RESTRICTED___PLEASE_TRY_A_DIFFERENT_CARD_OR_CALL_OUR_CUST = 521860,
                    MEMBER_SERVICES_TIP = 521868,
                    INVALID_CREDIT_CARD_NUMBER = 521882,
                    SITE_FOOTER2 = 521884,
                    ADMIN = 521886,
                    INITIAL_BUY = 521892,
                    RENEWAL = 521894,
                    TERMINATION = 521896,
                    ADMINISTRATIVE_ADJUSTMENT = 521898,
                    RECEIVED_CHECK = 521900,
                    AUTHORIZATION_ONLY = 521902,
                    WEEK = 521904,
                    YOU_HAVE_SUBSCRIPTION_PRIVILEGES_UNTIL = 600002,
                    OTHER_WAYS_TO_PAY = 600004,
                    ADMIN_ADJUSTMENT = 600006,
                    FRMVALIDATION19 = 600008,
                    FRMVALIDATION20 = 600010,
                    SELECT_TERMINATION_REASON = 600012,
                    MET_SOMEONE_ON_THE_SITE = 600014,
                    MET_SOMEONE_SOMEWHERE_ELSE = 600016,
                    BAD_EXPERIANCE_ON_THE_SITE = 600018,
                    NOT_ENOUGH_CONTACTS_REPLIES = 600020,
                    NO_MEMBERS_INTEREST_ME = 600022,
                    ONLINE_DATING_ISNT_FOR_ME = 600024,
                    TAKING_A_BREAK = 600026,
                    ONLINE_DATING_IS_TOO_EXPENSIVE = 600028,
                    ONLINE_DATING_IS_TOO_TIME_CONSUMING = 600030,
                    OTHER_SITES_ARE_CHEAPER = 600032,
                    PREFER_ANOTHER_SITE = 600034,
                    POOR_CUSTOMER_CARE = 600036,
                    REASON_NOT_MENTIONED = 600038,
                    CANCEL_UNDERSTAND = 600040,
                    CONTINUE_WITH_CANCELLATION = 600042,
                    TERMINATION_REASON_REQUIRED = 600044,
                    ONE_MORE_STEP = 600046,
                    I_WANT_TO = 600048,
                    KEEP_MY_PROFILE = 600050,
                    REMOVE_MY_PROFILE = 600052,
                    CANCEL_NOW = 600054,
                    CHOICE_IS_REQUIRED = 600056,
                    TERMINATE_FINAL_1 = 600058,
                    TERMINATE_FINAL_2 = 600060,
                    SAVE_FINAL_1 = 600062,
                    SAVE_FINAL_2 = 600064,
                    AN_UNRECOVERABLE_ERROR_HAS_OCCURED_PLEASE_RETURN = 600066,
                    OTHER_WAYS_TO_PAY_NO_CHECK = 600068,
                    NO_DATA_FOUND = 600070,
                    OK = 600072,
                    INVALID_MEMBER_ID = 600074,
                    MEMBER_EMAIL_CHANGED_SUCCESSFULLY = 600076,
                    UNABLE_TO_UPDATE_GLOBALSTATUSMASK = 600078,
                    UNABLE_TO_UPDATE_EMAIL_ADDRESS = 600080,
                    SAME_EMAIL_ADDRESS_ENTERED = 600082,
                    INVALID_MEMBER_GLOBALSTATUSMASK = 600084,
                    GLOBALSTATUSMASK_UPDATED_SUCCESSFULLY = 600086,
                    INVALID_FORM_DATA = 600088,
                    ACCESS_DENIED = 600090,
                    MEMBER_REQUEUE_SUCCESSFULLY = 600092,
                    PLEASE_ENTER_VALID_MEMBER_INFO = 600094,
                    ADMIN_NOTE_UPDATED_SUCCESSFULLY = 600096,
                    UNABLE_TO_UPDATE_ADMIN_NOTE = 600098,
                    ADMIN_NOTE_MESSAGE_CAN_NOT_EXCEED_3900_CHARACTERS = 600100,
                    MEMBER_DOES_NOT_HAVE_SUBSCRIPTION = 600102,
                    PLEASE_ENTER_VALID_DURATION = 600104,
                    PLEASE_SELECT_A_VALID_DURATION_TYPE = 600106,
                    PLEASE_SELECT_A_VALID_TRANSACTION_REASON = 600108,
                    SUBSCRIPTION_UPDATED_SUCCESSFULLY = 600110,
                    UNABLE_TO_UPDATE_SUBSCRIPTION = 600112,
                    SAME_PLAN_IS_CHOSEN_PLEASE_SELECT_AN_DIFFERENT_PL = 600114,
                    PLAN_UPDATED_SUCCESSFULLY = 600116,
                    FAILURE = 600118,
                    TERMS_AND_CONDITION_5DAY_TRIAL = 600120,
                    PHOTO_UPLOAD_8_TIMES_MORE_RESPONSES = 600122,
                    SUBMIT_PHOTO_CONTINUE_FREE_TRIAL = 600124,
                    FAIL_TO_SEND_VERIFICATION_EMAIL = 600126,
                    FAIL_TO_SEND_MEMBER_PASSWORD = 600128,
                    MEMBER_DOES_NOT_HAVE_A_PAID_SUBSCRIPTION = 600130,
                    YES_ACCEPT_OFFER = 600134,
                    NO_THANKS__WANT_TO_CANCEL = 600136,
                    PHOTO = 600142,
                    ADMIN_INVALID_CREDIT_CARD_NUMBER = 600144,
                    INVALID_PHOTO_TYPE = 600152,
                    SIGN_UP_NOW_AND_ENJOY_5_FREE_DAYS_OF_AMERICANSINGL = 600154,
                    WHO_KNOWS_YOU_MIGHT_JUST_FIND_THAT_SPECIAL_SOMEON = 600156,
                    SAVEOFFER_25PCT_3MOS = 600160,
                    SAVEOFFER_20PCT_3MOS = 600162,
                    SAVEOFFER_50PCT_3MOS = 600164,
                    SAVEOFFER_10PCT_3MOS = 600166,
                    SAVEOFFER_1495MO_3MOS = 600168,
                    SAVEOFFER_30DAYS_FREE = 600170,
                    FIVE_DAY_FREE_TRIAL_2495_MONTHLY_THEREAFTER = 600172,
                    MAXIMUM_FILE_SIZE_EXCEEDED = 600174,
                    CHARGE_APPROVED_CHECK_TELECHECK = 600182,
                    COMPLIMENTARY_LIMIT_TO_ONE_YEAR = 600188,
                    INTERNET_CHECKS_FREQUENTLY_ASKED_QUESTIONS = 600198,
                    TELECHECK_PRIVACY_POLICY_DETAILS = 600200,
                    REOPEN_SUBSCRIPTION = 600202,
                    RENEW_DATE = 600204,
                    END_DATE = 600206,
                    MANUAL_ADJUSTMENT = 600208,
                    TELECHECK_TERMS = 600210,
                    APPLICATIONS_SUBSCRIPTION_TELECHECK_ASPX = 600212,
                    FREE_TRIAL_SUBSCRIPTION_WELCOME = 600214,
                    SUBSCRIPTION_WELCOME = 600216,
                    FREE_TRIAL_OFFER = 600218,
                    VISA = 600220,
                    CHARGE_APPROVED_CHECK = 600222,
                    TELECHECK_CONFIRMATION = 600224,
                    DISCOUNT_NOT_AVAILABLE = 600226,
                    PAPER_CHECK = 600228,
                    COMPLIMENTARY_LIMIT = 600230,
                    PREMIUM_SUBSCRIBER_WELCOME_SUBJECT = 600232,
                    FREE_TRIAL_WELCOME_SUBJECT = 600234,
                    FREE_TRIAL_OFFER_SUBJECT = 600236,
                    TRANSACTION_REASON = 600238,
                    PLAN = 600240,
                    ADJUST_DURATION = 600242,
                    INVALID_DURATION = 600244,
                    MEMBER_PAYMENT_INFO_REMOVED_SUCCESSFULLY = 600246,
                    FAIL_TO_REMOVE_MEMBER_PAYMENT_INFO = 600248,
                    MEMBER_DOES_NOT_HAVE_PAYMENT_INFO = 600250,
                    FIVE_DAY_TRIAL__FREE_2450_MONTHLY_THEREAFTER = 600252,
                    TERMS_AND_CONDITIONSTHIS_FREE_5DAY_TRIAL_PERI = 600254,
                    PREFER_TO_SIGN_UP_VIA_PHONEBRCALL_US_BTOL  = 600256,
                    BY_CLICKING_THE_BUTTON_BELOW_YOU_AGREE_THAT_YOU_H = 600258,
                    FREE_TRIAL_SUBSCRIPTION_CONFIRMATION = 600260,
                    MANUAL_ADJUSTMENT_600208 = 600262,
                    FIVE_DAY_TRIAL__FREE_2450_MONTHLY_THEREAFTER_600252 = 600332,
                    MANUAL_ADJUSTMENT_600208_600262 = 600334,
                    FIVE_DAY_TRIAL__FREE_2450_MONTHLY_THEREAFTER_600252_600332 = 600404,
                    MANUAL_ADJUSTMENT_600208_600262_600334 = 600406,
                    FIVE_DAY_TRIAL__FREE_2450_MONTHLY_THEREAFTER_600252_600332_600404 = 600476,
                    MANUAL_ADJUSTMENT_600208_600262_600334_600406 = 600478,
                    FIVE_DAY_TRIAL__FREE_2450_MONTHLY_THEREAFTER_600252_600332_600404_600476 = 600548,
                    PACKAGE_CHECK_600550 = 600550,
                    PACKAGE_CHECK_600552 = 600552,
                    PACKAGE_CHECK_600554 = 600554,
                    PACKAGE_CHECK_600556 = 600556,
                    PACKAGE_CHECK_600558 = 600558,
                    PACKAGE_CHECK_600560 = 600560,
                    PACKAGE_CHECK_600562 = 600562,
                    PACKAGE_CHECK_600564 = 600564,
                    PACKAGE_CHECK_600566 = 600566,
                    PACKAGE_CHECK_600568 = 600568,
                    PACKAGE_CHECK_600570 = 600570,
                    PACKAGE_CHECK_600574 = 600574,
                    PACKAGE_CHECK_600578 = 600578,
                    PACKAGE_CHECK_600582 = 600582,
                    PACKAGE_CHECK_600586 = 600586,
                    PACKAGE_CHECK_600590 = 600590,
                    PACKAGE_CHECK_600594 = 600594,
                    PACKAGE_CHECK_600598 = 600598,
                    PACKAGE_CHECK_600602 = 600602,
                    PACKAGE_CHECK_600606 = 600606,
                    PACKAGE_CHECK_600644 = 600644,
                    PACKAGE_CHECK_600648 = 600648,
                    PACKAGE_CHECK_600652 = 600652,
                    PACKAGE_CHECK_600656 = 600656,
                    PACKAGE_CHECK_600680 = 600680,
                    PACKAGE_CHECK_600684 = 600684,
                    PACKAGE_CHECK_600714 = 600714,
                    PACKAGE_CHECK_600718 = 600718,
                    PACKAGE_CHECK_600726 = 600726,
                    PACKAGE_CHECK_600730 = 600730,
                    PACKAGE_CHECK_600734 = 600734,
                    PACKAGE_CHECK_600738 = 600738,
                    PACKAGE_CHECK_600742 = 600742,
                    PACKAGE_CHECK_600746 = 600746,
                    PACKAGE_CHECK_600750 = 600750,
                    PACKAGE_CHECK_600754 = 600754,
                    PACKAGE_CHECK_600758 = 600758,
                    PACKAGE_CHECK_600796 = 600796,
                    PACKAGE_CHECK_600800 = 600800,
                    PACKAGE_CHECK_600804 = 600804,
                    PACKAGE_CHECK_600808 = 600808,
                    PACKAGE_CHECK_600832 = 600832,
                    PACKAGE_CHECK_600836 = 600836,
                    PACKAGE_CHECK_600866 = 600866,
                    PACKAGE_CHECK_600870 = 600870,
                    PACKAGE_CHECK_4600876 = 600876,
                    PACKAGE_CHECK_6600880 = 600880,
                    PACKAGE_CHECK_8600884 = 600884,
                    PACKAGE_CHECK_10600888 = 600888,
                    PACKAGE_CHECK_12600892 = 600892,
                    PACKAGE_CHECK_14600896 = 600896,
                    PACKAGE_CHECK_16600900 = 600900,
                    PACKAGE_CHECK_18600904 = 600904,
                    PACKAGE_CHECK_20600908 = 600908,
                    PACKAGE_CHECK_56600946 = 600946,
                    PACKAGE_CHECK_58600950 = 600950,
                    PACKAGE_CHECK_60600954 = 600954,
                    PACKAGE_CHECK_62600958 = 600958,
                    PACKAGE_CHECK_84600982 = 600982,
                    PACKAGE_CHECK_86600986 = 600986,
                    PACKAGE_CHECK_114601016 = 601016,
                    PACKAGE_CHECK_116601020 = 601020,
                    PACKAGE_CHECK_2 = 601498,
                    PACKAGE_CHECK_4 = 601500,
                    PACKAGE_CHECK_6 = 601502,
                    PACKAGE_CHECK_8 = 601504,
                    PACKAGE_CHECK_10 = 601506,
                    PACKAGE_CHECK_12 = 601508,
                    PACKAGE_CHECK_14 = 601510,
                    PACKAGE_CHECK_16 = 601512,
                    PACKAGE_CHECK_18 = 601514,
                    PACKAGE_CHECK_20 = 601516,
                    PACKAGE_CHECK_22 = 601518,
                    PACKAGE_CHECK_24 = 601520,
                    PACKAGE_CHECK_26 = 601522,
                    PACKAGE_CHECK_28 = 601524,
                    PACKAGE_CHECK_30 = 601526,
                    PACKAGE_CHECK_32 = 601528,
                    PACKAGE_CHECK_34 = 601530,
                    PACKAGE_CHECK_36 = 601532,
                    PACKAGE_CHECK_38 = 601534,
                    PACKAGE_CHECK_40 = 601536,
                    PACKAGE_CHECK_42 = 601538,
                    PACKAGE_CHECK_44 = 601540,
                    PACKAGE_CHECK_46 = 601542,
                    PACKAGE_CHECK_48 = 601544,
                    PACKAGE_CHECK_50 = 601546,
                    PACKAGE_CHECK_52 = 601548,
                    PACKAGE_CHECK_54 = 601550,
                    PACKAGE_CHECK_56 = 601552,
                    PACKAGE_CHECK_58 = 601554,
                    PACKAGE_CHECK_60 = 601556,
                    PACKAGE_CHECK_62 = 601558,
                    PACKAGE_CHECK_64 = 601560,
                    PACKAGE_CHECK_66 = 601562,
                    PACKAGE_CHECK_68 = 601564,
                    PACKAGE_CHECK_70 = 601566,
                    PACKAGE_CHECK_72 = 601568,
                    PACKAGE_CHECK_74 = 601570,
                    PACKAGE_CHECK_76 = 601572,
                    PACKAGE_CHECK_78 = 601574,
                    PACKAGE_CHECK_80 = 601576,
                    PACKAGE_CHECK_82 = 601578,
                    PACKAGE_CHECK_84 = 601580,
                    PACKAGE_CHECK_86 = 601582,
                    PACKAGE_CHECK_88 = 601584,
                    PACKAGE_CHECK_90 = 601586,
                    PACKAGE_CHECK_92 = 601588,
                    PACKAGE_CHECK_94 = 601590,
                    PACKAGE_CHECK_96 = 601592,
                    PACKAGE_CHECK_98 = 601594,
                    PACKAGE_CHECK_100 = 601596,
                    PACKAGE_CHECK_102 = 601598,
                    PACKAGE_CHECK_104 = 601600,
                    PACKAGE_CHECK_106 = 601602,
                    PACKAGE_CHECK_108 = 601604,
                    PACKAGE_CHECK_110 = 601606,
                    PACKAGE_CHECK_112 = 601608,
                    PACKAGE_CHECK_114 = 601610,
                    PACKAGE_CHECK_116 = 601612,
                    PACKAGE_CHECK_118 = 601614,
                    DISCOUNT_002 = 601616,
                    DISCOUNT_004 = 601618,
                    DISCOUNT_006 = 601620,
                    DISCOUNT_010 = 601622,
                    DISCOUNT_012 = 601624,
                    DISCOUNT_014 = 601626,
                    DISCOUNT_000 = 601628,
                    CURRENT_PLAN = 601630,
                    CURRENT_DISCOUNT = 601632,
                    INVALID_PLANDISCOUNT = 601634,
                    DISCOUNT = 601636,
                    AMERICAN_EXPRESS = 601638,
                    SWITCH = 601640,
                    SOLO = 601642,
                    CARTE_BLANCHE = 601644,
                    DINERS = 601646,
                    DISCOVER = 601648,
                    JCB = 601650,
                    MASTERCARD = 601652,
                    CHOOSE_A_CITY_FROM_THE_LIST_BELOW = 601660,
                    DELTA = 601662,
                    ELECTRON = 601664,
                    LOOK_UP_CITY = 601666,
                    YOU_HAVE_EXCEEDED_THE_MAXIMUM_NUMBER_OF_FAILURES_IN_A_24_HOUR_PERIOD = 601668,
                    PHOTO_REQUIRED_FOR_FT = 601670,
                    TERMS_AND_CONDITIONSTHIS_FREE_5DAY_TRIAL_PERI_GRP1 = 601672,
                    DATE = 601674,
                    TYPE = 601676,
                    DURATION = 601678,
                    ACCOUNT = 601680,
                    CONFIRMATION = 601682,
                    STATUS = 601684,
                    AMOUNT = 601686,
                    VIEW = 601688,
                    SUBSCRIPTION_PLANS = 601690,
                    PLANID136 = 601692,
                    PLANID128 = 601694,
                    PLANID130 = 601696,
                    PLANID138 = 601698,
                    PLANID140 = 601700,
                    PLANID142 = 601702,
                    PLANID144 = 601704,
                    PLANID146 = 601706,
                    PLANID148 = 601708,
                    PLANID150 = 601710,
                    PLANID152 = 601712,
                    PLANID154 = 601714,
                    PLANID156 = 601716,
                    PLANID158 = 601718,
                    PLANID160 = 601720,
                    FREE_TRIAL_LIMITATIONS = 601722,
                    NO_THANKS = 601724,
                    PLEASE_PROCESS = 601726,
                    FREE_TRIAL = 601730,
                    USER_NAME_HEBREW = 601734,
                    USERNAME_CUPID = 601736,
                    PASSWORD_CUPID = 601738,
                    FORGOT_YOUR_PASS_CUPID = 601740,
                    VERIFY_EMAIL_CUPID = 601742,
                    REAL_EMAIL_CUPID = 601744,
                    SCREEN_NAME_CUPID = 601746,
                    PLANID162 = 601748,
                    PLANID164 = 601750,
                    READ_PHOTO_TIP = 601752,
                    INSTRUCTIONS_STEP3 = 601754,
                    SYSTEM_STATUS = 601756,
                    AWAITING_APPROVAL = 601758,
                    APPROVED = 601760,
                    DELETE_PHOTO = 601762,
                    REQUEUE = 601764,
                    PHOTO_UPLOAD = 601766,
                    DISPLAY_MY_PHOTOS_TO_GUEST = 601768,
                    X1ST = 601770,
                    X2ND = 601772,
                    X3RD = 601774,
                    X4TH = 601776,
                    X5TH = 601778,
                    JCUPID_IMPORT = 601780,
                    CUPIDON_IMPORT = 601782,
                    PARTY_LINE = 601784,
                    KLICKEN_SIE_HIER_FR_ANDERE_ZAHLUNGSARTEN_ = 601786,
                    PAY_BY_ELV = 601788,
                    CLICK_TO_SUBMIT = 601790,
                    BANK_CODE = 601792,
                    YES = 601794,
                    NO = 601796,
                    PLANID166 = 601798,
                    PLANID168 = 601800,
                    DIRECT_DEBIT_SUCCESS = 601802,
                    ELV_TITLE = 601804,
                    ELV_INFO_1 = 601806,
                    ELV_INFO_2 = 601808,
                    ELV_TERM_AND_CONDITION = 601810,
                    THIS_IS_A_PRIVATE_PHOTO = 601812,
                    ARE_YOU_SURE_YOU_WANT_TO_DELETE_THIS_PHOTO = 601816,
                    USERNAME_CUPID2 = 601818,
                    OPT_OUT_EMAIL = 601822,
                    PLEASE_SELECT_A_PLAN = 601824,
                    USERNAMECUPID = 601826,
                }
        */
  }
}
