using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Data;
using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Content
{
	public class Resource
	{
		private const string DB_SHAREDADMIN = "mnSharedAdmin";
		private int mResourceID = Constants.NULL_NUMBER;
		private int mResourceTypeID;
		private int mAppID;
		private string mConstant;
		private int mPrivateLabelID = Constants.NULL_NUMBER;
		private int mLanguageID;
		private string mContent;
		private DateTime mUpdateDate;

		#region Property get/set
		public int ResourceID 
		{
			get 
			{
				return mResourceID;
			}
			set 
			{
				mResourceID = value;
			}
		}

		public int ResourceTypeID 
		{
			get 
			{
				return mResourceTypeID;
			}
			set 
			{
				mResourceTypeID = value;
			}
		}

		public int AppID 
		{
			get 
			{
				return mAppID;
			}
			set 
			{
				mAppID = value;
			}
		}

		public string Constant 
		{
			get 
			{
				return mConstant;
			}
			set 
			{
				mConstant = value;
			}
		}

		public int PrivateLabelID 
		{
			get 
			{
				return mPrivateLabelID;
			}	
			set 
			{
				mPrivateLabelID = value;
			}
		}

		public int LanguageID 
		{
			get 
			{
				return mLanguageID;
			}
			set 
			{
				mLanguageID = value;
			}
		}

		public string Content 
		{
			get 
			{
				return mContent;
			}
			set 
			{
				mContent = value;
			}
		}

		public DateTime UpdateDate 
		{
			get 
			{
				return mUpdateDate;
			}
			set 
			{
				mUpdateDate = value;
			}
		}
		#endregion

		public void Populate(int resourceID, int languageID, int privateLabelID) 
		{
			//bool resourceNotFound = false;
			try 
			{
				/*
				procedure up_Resource_List
				(
					@ResourceID int,
					@PrivateLabelID int = null,
					@LanguageID int
				)
				*/
				SQLClient client = new SQLClient(new SQLDescriptor(DB_SHAREDADMIN));
				client.AddParameter("@ResourceID", SqlDbType.Int, ParameterDirection.Input, resourceID);
				client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
				if (privateLabelID != Constants.NULL_NUMBER) 
				{
					client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, privateLabelID);
				} 
				else 
				{
					client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, DBNull.Value);
				}
				DataTable table = client.GetDataTable("up_Resource_List", CommandType.StoredProcedure);
				if (table != null && table.Rows.Count > 0) 
				{
					DataRow row = table.Rows[0];
					mResourceID = resourceID;
					mLanguageID = languageID;
					mPrivateLabelID = privateLabelID;
					mResourceTypeID = System.Convert.ToInt32(row["ResourceTypeID"]);
					mAppID = System.Convert.ToInt32(row["AppID"]);
					mConstant = System.Convert.ToString(row["Constant"]);
					mUpdateDate = System.Convert.ToDateTime(row["UpdateDate"]);
					mContent = System.Convert.ToString(row["Content"]);
				} 
		//		else 
		//		{
		//			resourceNotFound = true;
		//		}
			}
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to populate Resource", "Populate", ex);
			}
			/*
			if (resourceNotFound) 
			{
				throw new ResourceNotFoundException(resourceID, languageID, privateLabelID);
			}
			*/
		}

		public void Save() 
		{
			try 
			{
				/*
				procedure up_Resource_Save
				(
					@ResourceID int output,
					@ResourceTypeID int,
					@AppID int,
					@Constant varchar (200),
					@Content ntext,
					@PrivateLabelID int = null,
					@LanguageID int
				)
				*/
				SQLClient client = new SQLClient(new SQLDescriptor(DB_SHAREDADMIN));
				SqlCommand command = new SqlCommand();

				if (mResourceID != Constants.NULL_NUMBER) 
				{
					client.AddParameter("@ResourceID", SqlDbType.Int, ParameterDirection.InputOutput, mResourceID);
				}
				else 
				{
					client.AddParameter("@ResourceID", SqlDbType.Int, ParameterDirection.InputOutput, DBNull.Value);
				}
				client.AddParameter("@ResourceTypeID", SqlDbType.Int, ParameterDirection.Input, mResourceTypeID);
				client.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, mAppID);
				client.AddParameter("@Constant", SqlDbType.VarChar, ParameterDirection.Input, mConstant, 200);
				client.AddParameter("@Content", SqlDbType.NText, ParameterDirection.Input, mContent);
				if (mPrivateLabelID != Constants.NULL_NUMBER) 
				{
					client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, mPrivateLabelID);
				}
				else 
				{
					client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, DBNull.Value);
				}
				client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, mLanguageID);
				client.PopulateCommand(command);
				command.CommandText = "up_Resource_Save";
				command.CommandType = CommandType.StoredProcedure;
				client.ExecuteNonQuery(command);

				mResourceID = System.Convert.ToInt32(command.Parameters["@ResourceID"].Value);
			} 
			catch (Exception ex) 
			{
				throw new MatchnetException("Unable to save Resource", "Save", ex);
			}
		}
	}
}
