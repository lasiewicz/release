using System;

namespace Matchnet.Lib.Content
{
	public class ResourceNotFoundException : Exception
	{
		private int mResourceID;
		private int mLanguageID;
		private int mPrivateLabelID;

		public ResourceNotFoundException(int resourceID, int languageID, int privateLabelID)
			: base("Resource Not Found: ResourceID(" + resourceID + ") LanguageID(" + languageID + ") PrivateLabelID(" + privateLabelID + ")")
		{
			mResourceID = resourceID;
			mLanguageID = languageID;
			mPrivateLabelID = privateLabelID;
		}

		public int ResourceID 
		{
			get 
			{
				return mResourceID;
			}
		}

		public int LanguageID 
		{
			get 
			{
				return mLanguageID;
			}
		}

		public int PrivateLabelID 
		{
			get 
			{
				return mPrivateLabelID;
			}
		}

		public override string ToString()
		{
			return "Resource Not Found: ResourceID(" + mResourceID + ") LanguageID(" + mLanguageID + ") PrivateLabelID(" + mPrivateLabelID + ")";
		}
	}
}
