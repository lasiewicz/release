using System;
using System.Web;
using System.Web.Caching;


namespace Matchnet.Lib.Caching
{
	/// <summary>
	/// Summary description for Cache.
	/// </summary>

	public class Cache : System.Collections.IEnumerable
	{
		// PT:  This variable is what Cache wraps.  In certain situations, the cache
		// will function differently.  For example, you need different functionality when
		// design time is used.
		private System.Web.Caching.Cache _InternalCache;
		private bool _UseMSCache;
		private static Matchnet.Lib.Caching.Cache _Instance;
		private static string _Lock = "";

		public static Matchnet.Lib.Caching.Cache GetInstance()
		{
			if( _Instance == null )
			{
				lock( _Lock )
				{
					if( _Instance == null )
					{
						_Instance = new Matchnet.Lib.Caching.Cache();
					}
				}
			}
			return _Instance;
		}

		// PT: This class attempts to autodetect the current running situation and decides
		// whether to use the internal Microsoft caching objects or not.  If it can't
		// figure out the runtime situation, then it'll default to not using the internal
		// caching structure, it will default to an internal implementation of sorts. 
		//
		// Currently, there is no internal implementation.  This is just a wrapper for
		// the Microsoft class.  We intend to create functionality within this class so
		// that we can support design time operations.

		private Cache()
		{
			_UseMSCache = true;
			try
			{
				_InternalCache = System.Web.HttpRuntime.Cache;
			}
			catch(Exception ex)
			{

				System.Diagnostics.Trace.WriteLine( "System.Web.HttpRuntime==null or " + 
					"System.Web.HttpRuntime.Cache==null." );
				System.Diagnostics.Trace.WriteLine(ex);
				_UseMSCache = false;
			}
		}

		// PT: [Deprecated] Secondary Constructor.  Force utilization of a different internal
		// caching structure
		private Cache( bool usemscache )
		{
			_UseMSCache = usemscache;
			if( _UseMSCache == true )
			{
				_InternalCache = System.Web.HttpRuntime.Cache;
			}
			else
			{
				// PT: Implement this later to satisfy the DesignTime scenario.  For
				// now, just throw the exception.
				// throw new System.Exception( "Unsupported operation. (_UseMSCache is false)\n" );
			}
		}

		// Properties
		public int Count
		{
			get
			{
				if( _UseMSCache == true )
					return _InternalCache.Count;
				else
				{
					// PT: Implement this later to satisfy the DesignTime scenario
					// For the time being, just throw an exception.
					// throw new System.Exception( "Unsupported operation. (_UseMSCache ==false)\n" );
					return 0;
				}
			}
		}

		// PT: We're aliasing the read only static field with a property.
		public DateTime NoAbsoluteExpiration
		{
			get
			{
				if( _UseMSCache == true )
					return System.Web.Caching.Cache.NoAbsoluteExpiration;
				else
				{
					throw new 
						System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				}
			}
		}

		// PT: We're aliasing the read only static field with a property.
		public TimeSpan NoSlidingExpiration
		{
			get
			{
				if( _UseMSCache == true )
					return System.Web.Caching.Cache.NoSlidingExpiration;
				else
				{
					throw new System.Exception();
				}
			}
		}

		// Item Gets or sets the cache item at the specified key. 
		public object this[ string key ]
		{
			get
			{ 
				if ( _UseMSCache == true )
					return _InternalCache[ key ];
				else
				{
					return null;
					// throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				}
			}
			set
			{
				if ( _UseMSCache == true )
					_InternalCache[key] = value;
				else
				{
					//throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				}
			}
		}

		// Methods
		public object Add( string key, 
			object value, 
			System.Web.Caching.CacheDependency dependencies, 
			DateTime absoluteExpiration, 
			TimeSpan slidingExpiration, 
			System.Web.Caching.CacheItemPriority priority, 
			System.Web.Caching.CacheItemRemovedCallback onRemoveCallback )
		{
			if( _UseMSCache == false )
			{
				// throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				return null;
			}

			return _InternalCache.Add( key, 
				value, 
				dependencies, 
				absoluteExpiration, 
				slidingExpiration, 
				priority,
				onRemoveCallback );
		}

		public object Get(string key)
		{
			if( _UseMSCache == false )
			{
				// PT: Implement this later to satisfy the DesignTime scenario
				// For the time being, just throw an exception.
				// throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				return null;
			}
			return _InternalCache.Get( key );
		}

		public System.Collections.IEnumerator GetEnumerator()
		{
			if( _UseMSCache == false )
			{
				// PT: Implement this later to satisfy the DesignTime scenario
				// For the time being, just throw an exception.
				// throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				return null;
			}
			return _InternalCache.GetEnumerator();
		}

		public void Insert(string key, object value)
		{
			if( _UseMSCache == false )
			{
				// PT: Implement this later to satisfy the DesignTime scenario
				// For the time being, just throw an exception.
				// throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				return;
			}
			_InternalCache.Insert( key, value );
		}

		public void Insert( string key, 
			object value, 
			System.Web.Caching.CacheDependency dependencies )
		{
			if( _UseMSCache == false )
			{
				// PT: Implement this later to satisfy the DesignTime scenario
				// For the time being, just throw an exception.
				// throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				return;
			}
			_InternalCache.Insert( key, value, dependencies);
		}

		public void Insert( string key, 
			object value, 
			System.Web.Caching.CacheDependency dependencies, 
			DateTime absoluteExpiration, 
			TimeSpan slidingExpiration)
		{
			if( _UseMSCache == false )
			{
				// PT: Implement this later to satisfy the DesignTime scenario
				// For the time being, just throw an exception.
				// throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				return;
			}
			_InternalCache.Insert( key, 
				value,
				dependencies,
				absoluteExpiration,
				slidingExpiration );
		}

		public void Insert( string key, 
			object value, 
			System.Web.Caching.CacheDependency dependencies, 
			DateTime absoluteExpiration, 
			TimeSpan slidingExpiration, 
			System.Web.Caching.CacheItemPriority priority, 
			System.Web.Caching.CacheItemRemovedCallback onRemoveCallback )
		{
			if( _UseMSCache == false )
			{
				// PT: Implement this later to satisfy the DesignTime scenario
				// For the time being, just throw an exception.
				// throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				return;
			}
			_InternalCache.Insert( key,
				value,
				dependencies,
				absoluteExpiration,
				slidingExpiration,
				priority,
				onRemoveCallback );
		}

		public object Remove(string key)
		{
			if( _UseMSCache == false )
			{
				// PT: Implement this later to satisfy the DesignTime scenario
				// For the time being, just throw an exception.
				// throw new System.Exception( "Unsupported operation. (_UseMSCache==false)\n" );
				return null;
			}
			return _InternalCache.Remove( key );
		}

	};

}
