using System;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Xsl
{
	/// <summary>
	/// A DataTransformer transforms data into
	/// a format specified an Xsl document.
	/// </summary>
	public class DataTransformer 
	{
		/// <summary>
		/// Transforms a DataSet
		/// </summary>
		/// <param name="dataSet">The DataSet to Transform</param>
		/// <param name="xslDocument">The XSL Transformation</param>
		/// <returns></returns>
		public static string Transform(DataSet dataSet, string xslDocument) 
		{
			return Transform(dataSet.GetXml(), xslDocument, null);
		}

		/// <summary>
		/// Transforms an XML Document
		/// </summary>
		/// <param name="xmlDocument">The XML Document to Transform</param>
		/// <param name="xslDocument">The XSL Transformation</param>
		/// <returns></returns>
		public static string Transform(string xmlDocument, string xslDocument) 
		{
			return Transform(xmlDocument, xslDocument, null);
		}

		/// <summary>
		/// Transforms a DataSet
		/// </summary>
		/// <param name="dataSet">The DataSet to Transform</param>
		/// <param name="xslDocument">The XSL Transformation</param>
		/// <param name="argList">An argument list for the Transformation</param>
		/// <returns></returns>
		public static string Transform(DataSet dataSet, string xslDocument, XsltArgumentList argList) 
		{
			return Transform(dataSet.GetXml(), xslDocument, argList);
		}

		/// <summary>
		/// Transforms an XML Document
		/// </summary>
		/// <param name="xmlDocument">The XML Document to Transform</param>
		/// <param name="xslDocument">The XSL Transformation</param>
		/// <param name="argList">An argument list for the Transformation</param>
		/// <returns></returns>
		public static string Transform(string xmlDocument, string xslDocument, XsltArgumentList argList) 
		{			
			XmlDocument xmlDoc = new XmlDocument();
			XmlDocument xslDoc = new XmlDocument();

			try 
			{

				xmlDoc.LoadXml(xmlDocument);
				xslDoc.LoadXml(xslDocument);
			} 
			catch (Exception ex) 
			{
				MatchnetException exception  = new MatchnetException("Unable to transform document", "DataTransformer.Transform", ex);
				throw exception;
			}

			return Transform(xmlDoc, xslDoc, argList);
		}

		/// <summary>
		/// Transforms an XML Document
		/// </summary>
		/// <param name="xmlDocument">The XML Document to Transform</param>
		/// <param name="xslDocument">The XSL Transformation</param>
		/// <param name="argList">An argument list for the Transformation</param>
		/// <returns></returns>
		public static string Transform(XmlDocument xmlDoc, XmlDocument xslDoc, XsltArgumentList argList) 
		{
			StringBuilder builder = new StringBuilder();
			try 
			{
				XslTransform xslTrans = new XslTransform();
				StringWriter writer = new StringWriter(builder);
                System.Security.Policy.Evidence evidence = new System.Security.Policy.Evidence(null);
                System.Xml.XmlUrlResolver resolver = new System.Xml.XmlUrlResolver();
				xslTrans.Load(xslDoc,resolver,evidence);
				xslTrans.Transform(xmlDoc.CreateNavigator(), argList, writer, resolver);
			} 
			catch (Exception ex) 
			{
				MatchnetException exception  = new MatchnetException("Unable to transform document", "DataTransformer.Transform", ex);
				throw exception;
			}

			return builder.ToString();
		}
	
	}
}
