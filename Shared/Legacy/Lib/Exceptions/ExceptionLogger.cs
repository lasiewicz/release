using System;
using System.Diagnostics;

namespace Matchnet.Lib.Exceptions
{
	/// <summary>
	/// Basic Exception Logging facilities
	/// </summary>
	public class ExceptionLogger
	{
		// TODO: this can be expanded on greatly by providing tracing capabilities

		/// <summary>
		/// Logs an exception to standard output
		/// </summary>
		/// <param name="exception">The exception to log</param>
		public static void LogToConsole(MatchnetException exception) {
			Trace.WriteLine(exception.ToString());
			System.Console.WriteLine(exception.ToString());
		}

		/// <summary>
		/// Logs an exception to the NT Event Log
		/// </summary>
		/// <param name="exception">The exception to log</param>
		public static void LogToEventLog(MatchnetException exception) {
			LogToEventLog(exception, "Matchnet", EventLogEntryType.Error);
		}

		/// <summary>
		/// Logs an exception to the NT Event Log
		/// </summary>
		/// <param name="exception">The exception to log</param>
		/// <param name="source">The source of the exception, as seen in the event log</param>
		public static void LogToEventLog(MatchnetException exception, string source) {
			LogToEventLog(exception, source, EventLogEntryType.Error);
		}

		/// <summary>
		/// Logs an exception to the NT Event Log
		/// </summary>
		/// <param name="exception">The exception to log</param>
		/// <param name="source">The source of the exception, as seen in the event log</param>
		/// <param name="type">The EventLogEntryType of the exception to log</param>
		public static void LogToEventLog(MatchnetException exception, string source, EventLogEntryType type) {
			Trace.WriteLine("\r\n" + exception.ToString());
			try 
			{
				EventLog.WriteEntry(source, exception.ToString(), type);
			} 
			catch {}
		}
	}
}
