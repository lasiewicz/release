using System;
using System.Diagnostics;
using System.Text;

using Matchnet.Lib.Util;

namespace Matchnet.Lib.Exceptions
{
	/// <summary>
	/// Represents an exception that occured in a Matchnet application.
	/// </summary>
	public class MatchnetException : Exception 
	{
		private string m_method;
		private bool m_fatal;

		public MatchnetException(Exception rootCause) 
			: base (rootCause.Message, rootCause)
		{
			PopulateByRootCause(rootCause);
		}

		private void PopulateByRootCause(Exception root) 
		{
		}


		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="message">The message of the exception</param>
		public MatchnetException(string message) 
			: base(message) {
			m_method = null;
		}
		
		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="message">The message of the exception</param>
		/// <param name="fatal">Whether or not the exception is fatal</param>
		public MatchnetException(string message, bool fatal) 
			: base(message) 
		{
			m_method = null;
			m_fatal = fatal;
		}

		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="message">The message of the exception</param>
		/// <param name="method">The calling method that raised the exception</param>
		public MatchnetException(string message, string method)
			:base(message) {
			m_method = method;
		}

		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="message">The message of the exception</param>
		/// <param name="method">The calling method that raised the exception</param>
		/// <param name="fatal">Whether or not the exception is fatal</param>
		public MatchnetException(string message, string method, bool fatal)
			:base(message) 
		{
			m_method = method;
			m_fatal = fatal;
		}
		
		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="message">The message of the exception</param>
		/// <param name="method">The calling method that raised the exception</param>
		/// <param name="rootCause">The root cause of the exception</param>
		public MatchnetException(string message, string method, System.Exception rootCause)
			:base(message, rootCause) {
			m_method = method;
			PopulateByRootCause(rootCause);
		}

		/// <summary>
		/// Constructs a new instance
		/// </summary>
		/// <param name="message">The message of the exception</param>
		/// <param name="method">The calling method that raised the exception</param>
		/// <param name="rootCause">The root cause of the exception</param>
		/// <param name="fatal">Whether or not the exception is fatal</param>
		public MatchnetException(string message, string method, System.Exception rootCause, bool fatal)
			:base(message, rootCause) 
		{
			m_method = method;
			m_fatal = fatal;
			PopulateByRootCause(rootCause);
		}

		/// <summary>
		/// The calling method that raised the exception
		/// </summary>
		public string Method 
		{
			get {
				return m_method;
			}
		}

		/// <summary>
		/// Whether or not the exception was fatal
		/// </summary>
		public bool Fatal 
		{
			get {
				return m_fatal;
			}
		}


		/// <summary>
		/// A string representation of the exception
		/// </summary>
		/// <returns>A string representation of the exception</returns>
		public override string ToString() {
			StringBuilder builder = new StringBuilder();
	
			builder.Append("[MatchnetException] " + this.Message);
			if (!StringUtil.StringIsEmpty(m_method)) {
				builder.Append("\r\n\t[Method] " + m_method);
			}
			Exception rootCause = InnerException;

			while (rootCause != null) 
			{
				builder.Append("\r\n\t[Root Cause] " + rootCause.ToString());
				rootCause = rootCause.InnerException;
			}

			return builder.ToString();
		}

		public override string StackTrace
		{
			get
			{
				StringBuilder builder = new StringBuilder();
				builder.Append(this.GetType() + " " + base.StackTrace);
				builder.Append("\r\n\t" + ToString());
				Exception rootCause = InnerException;

				while (rootCause != null) 
				{
					builder.Append("\r\n" + rootCause.GetType() + " " + rootCause.StackTrace);
					builder.Append("\r\n\t" + rootCause.ToString());
					rootCause = rootCause.InnerException;
				}
				return builder.ToString();
			}
		}
	}
}
