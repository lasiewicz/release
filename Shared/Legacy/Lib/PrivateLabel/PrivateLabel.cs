using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Caching;
using System.Web;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Threading;

using Matchnet.Lib.Data;
using Matchnet.Lib.Exceptions;
using Matchnet.Lib.Caching;


namespace Matchnet.Lib.PrivateLabel 
{
	public class PrivateLabel 
	{
		#region Constructors
		/// <summary>
		/// Default Constructor
		/// </summary>
		public PrivateLabel() 
		{
			_InternalCache = Matchnet.Lib.Caching.Cache.GetInstance();
			if( _InternalCache == null )
				throw new System.Exception( "Unable to allocate " + 
					"Matchnet.Lib.Caching.Cache object." );
		}
		#endregion
	
		#region Public Properties

		/// <summary>
		/// Private Label ID
		/// </summary>
		public int PrivateLabelID 
		{
			get 
			{
				return _PrivateLabelID;
			}
		}


		/// <summary>
		/// Domain ID
		/// </summary>
		public int DomainID 
		{
			get 
			{
				return _DomainID;
			}
		}

		/// <summary>
		/// Default Current ID
		/// </summary>
		public int DefaultCurrencyID
		{
			get
			{
				return _DefaultCurrencyID;
			}
		}

		/// <summary>
		/// Language ID
		/// </summary>
		public int LanguageID 
		{
			get 
			{
				return _LanguageID;
			}
		}


		/// <summary>
		/// Translation ID
		/// </summary>
		public int TranslationID 
		{
			get 
			{
				return _TranslationID;
			}
		}


		/// <summary>
		/// Description
		/// </summary>
		public string Description 
		{
			get 
			{
				return _Description;
			}
		}


		/// <summary>
		/// Base HTTP URL
		/// </summary>
		public string BaseHttpUrl 
		{
			get 
			{
				return "http://" + _DefaultHost + "." + _URI;
			}
		}


		/// <summary>
		/// URI
		/// </summary>
		public string URI 
		{
			get 
			{
				return _URI;
			}
		}


		/// <summary>
		/// Color Dark
		/// </summary>
		public string ColorDark 
		{
			get
			{
				return _ColorDark;
			}
		}


		/// <summary>
		/// Color Darker
		/// </summary>
		public string ColorDarker 
		{
			get 
			{
				return _ColorDarker;
			}
		}


		/// <summary>
		/// Color Darkest
		/// </summary>
		public string ColorDarkest 
		{
			get
			{
				return _ColorDarkest;
			}
		}


		/// <summary>
		/// Color Light
		/// </summary>
		public string ColorLight 
		{
			get 
			{
				return _ColorLight;
			}
		}


		/// <summary>
		/// Color Lighter
		/// </summary>
		public string ColorLighter 
		{
			get
			{
				return _ColorLighter;
			}
		}


		/// <summary>
		/// Color Lightest
		/// </summary>
		public string ColorLightest 
		{
			get 
			{
				return _ColorLightest;
			}
		}


		/// <summary>
		/// Color Medium
		/// </summary>
		public string ColorMedium 
		{
			get 
			{
				return _ColorMedium;
			}
		}	


		/// <summary>
		/// Left Align
		/// </summary>
		public string LAlign 
		{
			get 
			{
				return _LAlign;
			}

		}
		
		
		/// <summary>
		/// Right Align
		/// </summary>
		public string RAlign 
		{
			get 
			{
				return _RAlign;
			}

		}


		/// <summary>
		/// Character Set
		/// </summary>
		public string CharSet 
		{
			get 
			{
				return _CharSet;
			}

		}
		

		/// <summary>
		/// Text Direction
		/// </summary>
		public string Direction 
		{
			get 
			{
				return _Direction;
			}
		}


		/// <summary>
		/// Image URL
		/// </summary>
		public string ImgURL 
		{
			get 
			{
				return _ImgURL;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string ImageRoot
		{
			get 
			{
				return _ImageRoot;
			}
		}

		/// <summary>
		/// Locale ID
		/// </summary>
		public int LCID 
		{
			get 
			{
				return _LCID;
			}
		}


		/// <summary>
		/// GMT Offset
		/// </summary>
		public double GMTOffset 
		{
			get 
			{
				return _GMTOffset;
			}
		}
		

		/// <summary>
		/// Search Type Mask
		/// </summary>
		public int SearchTypeMask
		{
			get
			{
				return _SearchTypeMask;
			}
		}


		/// <summary>
		/// Base Private Label ID
		/// </summary>
		public int BasePrivateLabelID
		{
			get
			{
				return _BasePrivateLabelID;
			}
		}


		/// <summary>
		/// Redirect to Base
		/// </summary>
		public bool RedirectToBase
		{
			get
			{
				return _RedirectToBase;
			}
		}


		/// <summary>
		/// Status Mask
		/// </summary>
		public int StatusMask
		{
			get
			{
				return _StatusMask;
			}
		}


		/// <summary>
		/// Default Host
		/// </summary>
		public string DefaultHost
		{
			get
			{
				return _DefaultHost;
			}
		}


		/// <summary>
		/// Default Region ID
		/// </summary>
		public int DefaultRegionID
		{
			get
			{
				return _DefaultRegionID;
			}
		}


		
		/// <summary>
		/// Registration Step Count
		/// </summary>
		public int RegistrationStepCount
		{
			get
			{
				return _RegistrationStepCount;
			}
		}

		/// <summary>
		/// Default Min Age
		/// </summary>
		public int DefaultAgeMin
		{
			get
			{
				return _DefaultAgeMin;
			}
		}

		/// <summary>
		/// Default Max Age
		/// </summary>
		public int DefaultAgeMax
		{
			get
			{
				return _DefaultAgeMax;
			}
		}

		/// <summary>
		/// Default Search Radius
		/// </summary>
		public int DefaultSearchRadius
		{
			get
			{
				return _DefaultSearchRadius;
			}
		}

		/// <summary>
		/// PaymentTypeMask
		/// </summary>
		public int PaymentTypeMask
		{
			get
			{
				return _PaymentTypeMask;
			}
		}


		/// <summary>
		/// Default Search Type
		/// </summary>
		public int DefaultSearchTypeID
		{
			get
			{
				return _DefaultSearchTypeID;
			}
		}


		/// < summary>
		/// SSL URL
		/// </summary>
		public string SSLURL
		{
			get
			{
				return _SSLURL;
			}
		}


		/// <summary>
		/// Development Mode
		/// </summary>
		public bool ConfigLoaded
		{
			get
			{
				return _ConfigLoaded;
			}
		}


		/// <summary>
		/// Development Mode
		/// </summary>
		public bool DevelopmentMode
		{
			get
			{
				LoadConfig();

				return _DevelopmentMode;
			}
		}


		/// <summary>
		/// Secure Mode
		/// </summary>
		public SecureModes SecureMode
		{
			get
			{
				LoadConfig();

				return _SecureMode;
			}
		}

		/// <summary>
		/// Server Name
		/// </summary>
		public string ServerName
		{
			get
			{
				if (_ServerName == String.Empty)
				{
					_ServerName = SystemInformation.ComputerName;
				}
				return _ServerName;
			}
		}


		/// <summary>
		/// Host
		/// </summary>
		public string Host
		{
			get
			{
				if (_Host == null || _Host == "")
				{ //return the default host
					return _DefaultHost;
				}
				else
				{
					return _Host;
				}
			}
		}


		/// <summary>
		/// SSL Flag
		/// </summary>
		public bool SSLFlag
		{
			get
			{
				return _SSLFlag;
			}
		}


		/// <summary>
		/// Site Description
		/// </summary>
		public string SiteDescription 
		{
			get
			{
				return _SiteDescription;
			}
		}

		public string BaseHttpsURL
		{
			get
			{
				if ( _BaseHttpsURL == null || _BaseHttpsURL.Length == 0)
				{
					if (SSLURL.Length > 0)
					{
						_BaseHttpsURL = "https://" + SSLURL;
					}
					else
					{
						_BaseHttpsURL = "https://" + HostPrefix() + _URI;
					}
				}
    
				return _BaseHttpsURL;
			}
		}

		public string CSSPath
		{
			get
			{
				return _CSSPath;
			}
		}

		public Matchnet.Lib.Caching.Cache Cache
		{
			get
			{
				return _InternalCache;
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Retreives a datatable of PrivateLabels for a given Domain ID
		/// </summary>
		/// <param name="domainID">Domain ID</param>
		/// <returns>datatable</returns>
		public DataTable GetPrivateLabels(int domainID)
		{
			string key = PRIVATELABEL_CACHEKEY_PREFIX + domainID.ToString();
			DataTable dt = (DataTable)Cache[key];

			if (dt == null)
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnShared");
				SQLClient client = new SQLClient(descriptor);
				client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, domainID);
				dt = client.GetDataTable("up_PrivateLabel_ListByDomainID", CommandType.StoredProcedure);
				
				if ( dt.Rows.Count > 0 )
				{
					// Only if we have values, save it...
					Cache.Insert( key, dt, null, DateTime.Now.AddSeconds(CACHE_SECONDS), 
						TimeSpan.Zero, CacheItemPriority.High, null);
				}
			}

			return dt;
		}

		/// <summary>
		/// Populates object via URL
		/// </summary>
		/// <param name="url">current URL</param>
		public void Load(string url)

		{
			string fullURI;
			string uri;
			string key;
    
			_URI = "";
			_ServerName = "";
			_Host = "";
    
			//extract the uri
			fullURI = GetFullURI(url);

			key = "PL:" + fullURI;

			if (CacheLoad(key) == false)
			{
				uri = fullURI;
				while (uri.Length > 0)
				{
					if (LoadByURIFromDb(uri))
					{
						break;
					}
					else
					{
						uri = TrimURI(uri);
					}
				}
				// If the private label is still undefined, load plid 1
				if (_PrivateLabelID == 0)
				{
					Load(DEFAULT_PRIVATELABEL);				
				}
				CacheSave(key);
			}
		}

		/// <summary>
		/// Populates object via Private Label ID
		/// </summary>
		/// <param name="privateLabelID">Private Label ID</param>
		public void Load(int privateLabelID) 
		{
			string key = "PLID:" + privateLabelID.ToString();

			if(CacheLoad(key) == false) 
			{
				LoadByIdFromDb(privateLabelID);
				CacheSave(key);
			}
			SetUNCRoot();

		}

		public void Load(int privateLabelID, bool sslFlag, string host, string uri)
		{
			string key = "SPLID:" + privateLabelID.ToString();

			if (CacheLoad(key) == false)
			{
				LoadByIdFromDb(privateLabelID);
				_SSLFlag = sslFlag;
				if (host != null && uri != null)
				{
					_DefaultHost = host;
					_URI = uri;
				}
				CacheSave(key);
			}
		}
		

		/// <summary>
		/// retreives file web path
		/// </summary>
		/// <param name="filename">file name</param>
		/// <param name="applicationID">application ID</param>
		/// <param name="fullyQualified">relative or fully qualified path</param>
		/// <returns>file web path</returns>
		public string WebSrc(string filename, int applicationID, bool fullyQualified) 
		{

			string key = "FILESRC:" + filename + ":" + applicationID + ":" + _PrivateLabelID + ":" + fullyQualified;

			string path = string.Empty;

			path = System.Convert.ToString(Cache[key]);

			if (path == null || path == string.Empty)
			{
				path = FileSrc(filename, applicationID);
				if(path.Length > 0) 
				{
					path = path.Replace(@"\", "/");
					if(fullyQualified) 
					{
						if(_ImgURL.IndexOf("http://") == -1) 
						{
							path = BaseHttpUrl + path;
						} 
					}
				}
				if (Cache[key] != null) 
				{
					Cache.Remove(key);
				}
				Cache.Insert(key, path, null, DateTime.Now.AddSeconds(CACHE_SECONDS), 
					TimeSpan.Zero,	
					CacheItemPriority.High, null);
			}
			return path;
		}


		public string GetDomainAbbreviation(int domainID)
		{
			string domainAbbreviation = "";

			switch (domainID)
			{
				case (int)Domains.DOMAIN_MND:
					domainAbbreviation = "as";
					break;
				case (int)Domains.DOMAIN_SS:
					domainAbbreviation = "gy";
					break;
				case (int)Domains.DOMAIN_JEW:
					domainAbbreviation = "jd";
					break;
				case (int)Domains.DOMAIN_MNC:
					domainAbbreviation = "mn";
					break;
				case (int)Domains.DOMAIN_PA:
					domainAbbreviation = "fl";
					break;
				case (int)Domains.DOMAIN_COL:
					domainAbbreviation = "cl";
					break;
			}

			return domainAbbreviation;
		}


		/// <summary>
		/// retreives descriptive region string
		/// </summary>
		/// <param name="regionID">region ID</param>
		/// <returns>region string</returns>
		public string GetRegionString(int regionID) 
		{
			string cacheKey = "PLAR:" + _LanguageID + ":" + regionID;
			string region = string.Empty;

			if ( Cache[cacheKey] != null ) 
			{
				region = System.Convert.ToString(Cache[cacheKey]);
			}

			if (region == null || region.Equals(string.Empty)) 
			{
				region = PopulateRegionString(regionID, _LanguageID);

				if (!region.Equals("n/a")) 
				{
					if (Cache[cacheKey] != null) 
					{
						Cache.Remove(cacheKey);
					}
					Cache.Insert(cacheKey, region, null, DateTime.Now.AddSeconds(CACHE_SECONDS), 
						TimeSpan.Zero,	
						CacheItemPriority.High, null);
				}
			}
			return region;
		}


		#endregion

		#region Enums and Constants
		public enum Domains : int
		{
			DOMAIN_MND = 1,
			DOMAIN_SS = 2,
			DOMAIN_JEW = 3,
			DOMAIN_MNC = 8,
			DOMAIN_PA = 9,
			DOMAIN_COL = 12
		};

		public enum SecureModes
		{
			Separated = 0,
			Local = 1
		}

		const string CACHE_DELIMITER = "<=>"; // needs to be acceptible by regexp
		const int CACHE_SECONDS = 3600; // cache for 1 hour
		const int PROPERTY_OWNER = 50;
		const int PROPERTY_NAME = 50;
		const string REGISTRY_KEY = @"SOFTWARE\Matchnet";
		const string KEY_DEVELOPMENTMODE = "DevelopmentMode";
		const string KEY_SECUREMODE = "SecureMode";
		const string DEVELOPMENT_MODE_ON = "1";
		const string HTTP_PREFIX = "http://";
		const string HTTPS_PREFIX = "https://";
		const int DEFAULT_PRIVATELABEL = 1;
		const string PRIVATELABEL_CACHEKEY_PREFIX = "PrivateLabel:";


		#endregion

		#region Member Variables

		private Matchnet.Lib.Caching.Cache _InternalCache;
		private int _PrivateLabelID;
		private int _DomainID;
		private int _DefaultCurrencyID;
		private string _URI;
		private string _Description;
		private int _LanguageID;
		private int _StatusMask;
		private int _LCID;
		private int _DefaultRegionID;
		private bool _Disabled;
		private string _Host;
		private string _CSSPath;
		private string _CharSet;
		private string _RAlign;
		private string _LAlign;
		private string _Direction;
		private string _ColorLightest;
		private string _ColorLighter;
		private string _ColorLight;
		private string _ColorMedium;
		private string _ColorDark;
		private string _ColorDarker;
		private string _ColorDarkest;
		private string _BodyProperties;
		private string _ImgURL;
		private int _RegistrationStepCount;
		private int _DefaultAgeMin;
		private int _DefaultAgeMax;
		private int _DefaultSearchRadius;
		private int _PaymentTypeMask;
		private double _GMTOffset;
		private int _TranslationID;
		private int _BasePrivateLabelID;
		private bool _RedirectToBase;
		private string _DefaultHost;
		private string _SSLURL;
		private int _DefaultSearchTypeID;
		private string _ChatDNS;
		private string _ChatServer;
		private int _ChatPort;
		private int _ChatAppletVersion;
		private int _CheckIMVersion;
		private int _SearchTypeMask;
		private string _ServerName = String.Empty;
		private bool _SSLFlag;
		private string _ImageRoot;
		private string _SiteDescription;
		private string _BaseHttpsURL;
		private bool _ConfigLoaded = false;
		private SecureModes _SecureMode;
		private bool _DevelopmentMode;
		static private ReaderWriterLock _CacheLock = new ReaderWriterLock();

		#endregion

		#region Private Methods

		private void SetProperties(DataTable table)
		{
			_PrivateLabelID = System.Convert.ToInt32(table.Rows[0]["PrivateLabelID"]);
			_DomainID = System.Convert.ToInt32(table.Rows[0]["DomainID"]);
			_DefaultCurrencyID = System.Convert.ToInt32(table.Rows[0]["DefaultCurrencyID"]);
			_URI = table.Rows[0]["URI"].ToString();
			_SiteDescription = table.Rows[0]["Description"].ToString();
			_LanguageID = System.Convert.ToInt32(table.Rows[0]["LanguageID"]);
			_StatusMask = System.Convert.ToInt32(table.Rows[0]["StatusMask"]); 
			_LCID = System.Convert.ToInt32(table.Rows[0]["LCID"]);
			_DefaultRegionID = System.Convert.ToInt32(table.Rows[0]["DefaultRegionID"]);
			_Disabled = System.Convert.ToBoolean(table.Rows[0]["DisableFlag"]);
			_CSSPath = table.Rows[0]["CSSPath"].ToString();
			_CharSet = table.Rows[0]["CharSet"].ToString();	 
			_RAlign = table.Rows[0]["RAlign"].ToString();
			_LAlign = table.Rows[0]["LAlign"].ToString();
			_Direction = table.Rows[0]["Direction"].ToString();	 
			_ColorLightest = table.Rows[0]["ColorLightest"].ToString();
			_ColorLighter = table.Rows[0]["ColorLighter"].ToString();
			_ColorLight = table.Rows[0]["ColorLight"].ToString();
			_ColorMedium = table.Rows[0]["ColorMedium"].ToString();
			_ColorDark = table.Rows[0]["ColorDark"].ToString();
			_ColorDarker = table.Rows[0]["ColorDarker"].ToString();
			_ColorDarkest = table.Rows[0]["ColorDarkest"].ToString();
			_BodyProperties = table.Rows[0]["BodyProperties"].ToString();
			_RegistrationStepCount = System.Convert.ToInt32(table.Rows[0]["RegistrationStepCount"]);
			_DefaultAgeMin = System.Convert.ToInt32(table.Rows[0]["DefaultAgeMin"]);
			_DefaultAgeMax = System.Convert.ToInt32(table.Rows[0]["DefaultAgeMax"]);
			_DefaultSearchRadius = System.Convert.ToInt32(table.Rows[0]["DefaultSearchRadius"]);
			_PaymentTypeMask = System.Convert.ToInt32(table.Rows[0]["PaymentTypeMask"]);
			_ChatDNS = table.Rows[0]["ChatDNS"].ToString();
			_ChatServer = table.Rows[0]["ChatServer"].ToString();
			_ChatPort = System.Convert.ToInt32(table.Rows[0]["ChatPort"]);
			_ChatAppletVersion = System.Convert.ToInt32(table.Rows[0]["ChatAppletVersion"]);
			_CheckIMVersion = System.Convert.ToInt32(table.Rows[0]["CheckIMVersion"]);
			_GMTOffset = System.Convert.ToDouble(table.Rows[0]["GMTOffset"]);
			_TranslationID = System.Convert.ToInt32(table.Rows[0]["TranslationID"]);
			if (table.Rows[0]["BasePrivateLabelID"] != DBNull.Value)
			{
				_BasePrivateLabelID = System.Convert.ToInt32(table.Rows[0]["BasePrivateLabelID"]);
			}
			else
			{
				_BasePrivateLabelID = Constants.NULL_NUMBER;
			}
			_RedirectToBase = System.Convert.ToBoolean(table.Rows[0]["RedirectToBase"]);
			_DefaultHost = table.Rows[0]["DefaultHost"].ToString();	
			_SSLURL = table.Rows[0]["SSLURL"].ToString();
			
			// This will conditionally modify urls for local secure mode
			LocalModeModifyURLS();
			
			_DefaultSearchTypeID = System.Convert.ToInt32(table.Rows[0]["DefaultSearchTypeID"]);
			_SearchTypeMask = System.Convert.ToInt32(table.Rows[0]["SearchTypeMask"]);

			_ImgURL = table.Rows[0]["ImgURL"].ToString();
			if(_ImgURL.EndsWith("/")) 
			{
				_ImgURL = _ImgURL.Substring(0, _ImgURL.Length - 1);
			}

			if(_ImgURL.StartsWith("/") == false) 
			{
				_ImgURL = "/" + _ImgURL;
			}
		}


		private void LocalModeModifyURLS()
		{
			if ( SecureMode != SecureModes.Local )
				return;

			string localHost=null;

			if ( HttpContext.Current != null )
			{
				int dotpos = HttpContext.Current.Request.Url.Host.IndexOf(".");

				if ( dotpos != -1 )
				{
					localHost = HttpContext.Current.Request.Url.Host.Substring(0,dotpos);

					if ( HttpContext.Current.Request.Url.Host != "localhost" )
					{
						_SSLURL = HttpContext.Current.Request.Url.Host;
						_DefaultHost = localHost;
					}
				}
			}
		}


		private void SetUNCRoot()
		{
			SQLDescriptor conf = new SQLDescriptor("mnMaster", 0);
			SQLClient client = new SQLClient(conf);
			client.AddParameter("@Owner", SqlDbType.VarChar, ParameterDirection.Input, "mnMaster", PROPERTY_OWNER);
			client.AddParameter("@Name", SqlDbType.VarChar, ParameterDirection.Input, "ImageRoot", PROPERTY_NAME);
			DataTable table = client.GetDataTable("up_Property_List", CommandType.StoredProcedure);
			if(table.Rows.Count > 0) 
			{
				_ImageRoot= table.Rows[0]["Value"].ToString();
				if(_ImageRoot.EndsWith(@"\")) 
				{
					_ImageRoot = _ImageRoot.Substring(0, _ImageRoot.Length - 1);
				}
			} 
			else 
			{
				throw new MatchnetException("Failed to load UNC Image Root");
			}
		}


		private string TrimURI(string uri)
		{
			string result = "";
			int dotPos;

			dotPos = uri.IndexOf(".");

			if (dotPos > 0 && dotPos < uri.Length)
			{
				if (_Host.Length > 0)
				{
					_Host = _Host + ".";
				}

				if (dotPos > 1)
				{
					_Host = _Host + uri.Substring(0, dotPos);
				}

				result = uri.Substring(dotPos + 1);
			}
			else
			{
				// If no dot, the whole this is the host
				_Host = uri;
			}

			return result;
		}


		private void CacheSave(string key) 
		{
			try
			{
				_CacheLock.AcquireWriterLock(1000);

				PrivateLabel privateLabel = (PrivateLabel)this.MemberwiseClone();

				Cache.Insert(key, privateLabel, null, DateTime.Now.AddSeconds(CACHE_SECONDS), 
					TimeSpan.Zero,	
					CacheItemPriority.High, null);
			}
			finally
			{
				_CacheLock.ReleaseWriterLock();
			}
		}


		private bool CacheLoad(string key) 
		{
			bool success = false;

			PrivateLabel val;

			try
			{
				_CacheLock.AcquireReaderLock(1000);

				val = (PrivateLabel)Cache[key];

				if(val != null) 
				{
					_BasePrivateLabelID = val.BasePrivateLabelID;
					_DomainID = val.DomainID;
					_DefaultCurrencyID = val.DefaultCurrencyID;
					_URI = val.URI;
					_SiteDescription = val.SiteDescription;
					_LanguageID = val.LanguageID;
					_StatusMask = val.StatusMask;
					_LCID = val.LCID;
					_DefaultRegionID = val.DefaultRegionID;
					//_Disabled = val._Disabled;
					_CSSPath = val.CSSPath;
					_CharSet = val.CharSet;
					_RAlign = val.RAlign;
					_LAlign = val.LAlign;
					_Direction = val.Direction;
					_ColorLightest = val.ColorLightest;
					_ColorLighter = val.ColorLighter;
					_ColorLight = val.ColorLight;
					_ColorMedium = val.ColorMedium;
					_ColorDark = val.ColorDark;
					_ColorDarker = val.ColorDarker;
					_ColorDarkest = val.ColorDarkest;
					//_BodyProperties = val._BodyProperties;
					_RegistrationStepCount = val.RegistrationStepCount;
					_DefaultAgeMin = val.DefaultAgeMin;
					_DefaultAgeMax = val.DefaultAgeMax;
					_DefaultSearchRadius = val.DefaultSearchRadius;
					_PaymentTypeMask = val.PaymentTypeMask;
					//_ChatDNS = val._ChatDNS;
					//_ChatServer = val._ChatServer;
					//_ChatPort = val._ChatPort;
					//_ChatAppletVersion = val._ChatAppletVersion;
					//_CheckIMVersion = val._CheckIMVersion;
					_GMTOffset = val.GMTOffset;
					_TranslationID = val.TranslationID;
					_RedirectToBase = val.RedirectToBase;
					_DefaultHost = val.DefaultHost;
					_SSLURL = val.SSLURL;
					_Description = val.Description;
					_DevelopmentMode = val.DevelopmentMode;
					_SecureMode = val.SecureMode;
					_ConfigLoaded = val.ConfigLoaded;
					_Host = val.Host;
					_ImgURL = val.ImgURL;
					_PrivateLabelID = val.PrivateLabelID;
					_SearchTypeMask = val.SearchTypeMask;
					_ServerName = val.ServerName;
					_SSLFlag = val.SSLFlag;
					_DefaultSearchTypeID = val.DefaultSearchTypeID;
					_ImageRoot = val.ImageRoot;

					success = true;
				}
			}
			finally
			{
				_CacheLock.ReleaseReaderLock();
			}

			return success;
		}


		private string HostPrefix()
		{
			string result = "";

			if (_Host.Length > 0)
			{
				result = _Host + ".";
			}
			else if (_DefaultHost.Length > 0)
			{
				result = _DefaultHost + ".";
			}

			return result;
		}
			

		private string GetFullURI(string url)
		{
			Uri uri = new Uri(url);

			return uri.Host;
		}


		private string FileSrc(string filename) 
		{
			return FileSrc(filename, Constants.NULL_NUMBER);
		}


		private string FileSrc(string filename, int applicationID) 
		{
			string path = "";
			bool found = false;

			SQLDescriptor descriptor = new SQLDescriptor("mnShared");
			SQLClient client = new SQLClient(descriptor);

			client.AddParameter("@Name", SqlDbType.VarChar, ParameterDirection.Input, filename);
			client.AddParameter("@ApplicationID", SqlDbType.Int, ParameterDirection.Input, applicationID);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, _PrivateLabelID);
			client.AddParameter("@BasePrivateLabelID", SqlDbType.Int, ParameterDirection.Input, (_BasePrivateLabelID == Constants.NULL_NUMBER) ? _BasePrivateLabelID : _BasePrivateLabelID);
			client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, _LanguageID);
			client.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, _DomainID);

			DataTable table = client.GetDataTable("up_Image_ListEx", CommandType.StoredProcedure);
			if (table.Rows.Count > 0)
			{
				int intPrivateLabelID = 0;
				int intLanguageID = 0;
				int intDomainID = 0;
				int intApplicationID = applicationID;
				string strName = filename;
				foreach (DataRow row in table.Rows) 
				{
					intPrivateLabelID = System.Convert.ToInt32(row["PrivateLabelID"].ToString());
					intLanguageID = System.Convert.ToInt32(row["LanguageID"].ToString());
					intDomainID = System.Convert.ToInt32(row["DomainID"].ToString());
					intApplicationID = System.Convert.ToInt32(row["ApplicationID"].ToString());
					strName = row["Name"].ToString();
					break; // take 1st
				}

				// get the base path for a brand, locale, or domain
				if (intPrivateLabelID > 0) 
				{
					path = @"/p/" + intPrivateLabelID;
				} 
				else if (intLanguageID > 0) 
				{
					path = @"/l/" + intLanguageID;
				}
				else if (intDomainID > 0) 
				{
					path = @"/d/" + intDomainID;
				}
                       
				// Some images don't have application IDs
				if (intApplicationID > 0) 
				{
					path = path + @"/" + intApplicationID;
				}

				// Append the File Name
				path = _ImgURL + path + @"/" + strName;

			}
			else // scan the drive, but _ImageRoot is \\dv-web1 when it should be local
			{
				string[] delims = new string[] {"p", "d", "l"};
				string[] vals = new string[] { _PrivateLabelID.ToString(), _DomainID.ToString(), _LanguageID.ToString() };
				for(int i = 0; i < delims.Length; i++) 
				{
					path = _ImageRoot + @"\" + delims[i] + @"\" + vals[i] + @"\" + filename;
					if(System.IO.File.Exists(path)) 
					{
						found = true;
						break;
					}

					if(applicationID != Constants.NULL_NUMBER) 
					{
						path = _ImageRoot + @"\" + delims[i] + @"\" + vals[i] + @"\" + applicationID.ToString() + @"\" + filename;
						if(System.IO.File.Exists(path)) 
						{
							found = true;
							break;
						}
					}
				}

				if(found == false) 
				{
					if(applicationID != Constants.NULL_NUMBER) 
					{
						path = @"\" + applicationID.ToString() + @"\" + filename;
						found = System.IO.File.Exists(_ImageRoot + path);
					}
				
					if(found == false) 
					{
						path = @"\" + filename;
						found = System.IO.File.Exists(_ImageRoot + path);
					}
				}
				if(found == false) 
				{
					path = "";
				}
				else
				{
					path = path.Replace(_ImageRoot,"");
					path = path.Replace(@"\",@"/");
					path = _ImgURL + path;
				}
			}
			return path;
			
		}


		private void LoadByIdFromDb(int privateLabelID) 
		{
			SQLDescriptor conf = new SQLDescriptor("mnShared", 0);
			SQLClient client = new SQLClient(conf);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int,ParameterDirection.Input,privateLabelID);
			DataTable table = client.GetDataTable("up_PrivateLabel_SelectByID", CommandType.StoredProcedure);
			if (table.Rows.Count > 0) 
			{
				SetProperties(table);
				SetUNCRoot();
			}
			SetUNCRoot();
		}


		private bool LoadByURIFromDb(string uri)
		{
			SQLDescriptor conf = new SQLDescriptor("mnShared", 0);
			SQLClient client = new SQLClient(conf);
			client.AddParameter("@URI", SqlDbType.VarChar, ParameterDirection.Input, uri);
			DataTable table = client.GetDataTable("up_PrivateLabel_Select", CommandType.StoredProcedure);
			if (table.Rows.Count > 0) 
			{
				SetProperties(table);
				SetUNCRoot();
				return true;
			}

			return false;
		}


		private string PopulateRegionString(int regionID, int languageID) 
		{
			string retVal = "n/a";
			try 
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnRegion", 0);
				SQLClient client = new SQLClient(descriptor);

				client.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
				client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);

				DataTable table = client.GetDataTable("up_Region_ListHierarchy", CommandType.StoredProcedure);

				if (table.Rows.Count > 0) 
				{
					string city = "";
					string state = "";
					string country = "";

					foreach (DataRow row in table.Rows) 
					{
						int depth = System.Convert.ToInt32(row["Depth"]);

						switch (depth) 
						{
							case 1:
								country = System.Convert.ToString(row["Description"]);
								break;
							case 2:
								state = System.Convert.ToString(row["Description"]);
								break;
							case 3:
								city = System.Convert.ToString(row["Description"]);
								break;
						}
					}

					retVal = city + ", " + state;
				}
			} 
			catch (Exception ex) 
			{
				MatchnetException exception = new MatchnetException("Unable to load region", "PrivateLabel.PopulateRegionString", ex);
				throw exception;
			}
			return retVal;
		}

		private void LoadConfig()
		{
			if ( _ConfigLoaded )
				return;

			try
			{
				if ( ConfigurationSettings.AppSettings.Get(KEY_DEVELOPMENTMODE) != null )
				{
					_DevelopmentMode = 
						Convert.ToBoolean(ConfigurationSettings.AppSettings.Get(KEY_DEVELOPMENTMODE));
				}

				string secureModeText = ConfigurationSettings.AppSettings.Get(KEY_SECUREMODE);
				if ( secureModeText != null )
				{
					if ( secureModeText.ToLower() == "local" )
					{
						_SecureMode = SecureModes.Local;
					}
					else
					{
						_SecureMode = SecureModes.Separated;
					}
				}
				
				_ConfigLoaded = true;
			}
			catch(Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex);
			}
		}


		#endregion
	}
}
