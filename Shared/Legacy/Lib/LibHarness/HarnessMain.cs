using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Data;

namespace LibHarness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class HarnessMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TabControl tcHarness;
		private System.Windows.Forms.TabPage tpData;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RichTextBox txtDataOutput;
		private System.Windows.Forms.Label lblLogicalDatabase;
		private System.Windows.Forms.TextBox txtLogicalDatabase;
		private System.Windows.Forms.Label lblKey;
		private System.Windows.Forms.TextBox txtKey;
		private System.Windows.Forms.Label lblCommandText;
		private System.Windows.Forms.TextBox txtCommandText;
		private System.Windows.Forms.Label lblParameters;
		private System.Windows.Forms.TextBox txtParameters;
		private System.Windows.Forms.Label lblExampleParameters;
		private System.Windows.Forms.Button btnExecute;
		private System.Windows.Forms.Button btnDataTable;
		private System.Windows.Forms.Button btnDataSet;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public HarnessMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tcHarness = new System.Windows.Forms.TabControl();
			this.tpData = new System.Windows.Forms.TabPage();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.txtDataOutput = new System.Windows.Forms.RichTextBox();
			this.lblLogicalDatabase = new System.Windows.Forms.Label();
			this.txtLogicalDatabase = new System.Windows.Forms.TextBox();
			this.lblKey = new System.Windows.Forms.Label();
			this.txtKey = new System.Windows.Forms.TextBox();
			this.lblCommandText = new System.Windows.Forms.Label();
			this.txtCommandText = new System.Windows.Forms.TextBox();
			this.lblParameters = new System.Windows.Forms.Label();
			this.txtParameters = new System.Windows.Forms.TextBox();
			this.lblExampleParameters = new System.Windows.Forms.Label();
			this.btnExecute = new System.Windows.Forms.Button();
			this.btnDataTable = new System.Windows.Forms.Button();
			this.btnDataSet = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.tcHarness.SuspendLayout();
			this.tpData.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tcHarness);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(600, 560);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// tcHarness
			// 
			this.tcHarness.Controls.Add(this.tpData);
			this.tcHarness.Location = new System.Drawing.Point(8, 16);
			this.tcHarness.Name = "tcHarness";
			this.tcHarness.SelectedIndex = 0;
			this.tcHarness.Size = new System.Drawing.Size(584, 536);
			this.tcHarness.TabIndex = 0;
			// 
			// tpData
			// 
			this.tpData.Controls.Add(this.txtDataOutput);
			this.tpData.Controls.Add(this.groupBox2);
			this.tpData.Location = new System.Drawing.Point(4, 22);
			this.tpData.Name = "tpData";
			this.tpData.Size = new System.Drawing.Size(576, 510);
			this.tpData.TabIndex = 0;
			this.tpData.Text = "Data";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.btnDataSet);
			this.groupBox2.Controls.Add(this.btnDataTable);
			this.groupBox2.Controls.Add(this.btnExecute);
			this.groupBox2.Controls.Add(this.lblExampleParameters);
			this.groupBox2.Controls.Add(this.txtParameters);
			this.groupBox2.Controls.Add(this.lblParameters);
			this.groupBox2.Controls.Add(this.txtCommandText);
			this.groupBox2.Controls.Add(this.lblCommandText);
			this.groupBox2.Controls.Add(this.txtKey);
			this.groupBox2.Controls.Add(this.lblKey);
			this.groupBox2.Controls.Add(this.txtLogicalDatabase);
			this.groupBox2.Controls.Add(this.lblLogicalDatabase);
			this.groupBox2.Location = new System.Drawing.Point(8, 8);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(560, 168);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			// 
			// txtDataOutput
			// 
			this.txtDataOutput.Location = new System.Drawing.Point(8, 184);
			this.txtDataOutput.Name = "txtDataOutput";
			this.txtDataOutput.Size = new System.Drawing.Size(560, 320);
			this.txtDataOutput.TabIndex = 1;
			this.txtDataOutput.Text = "";
			// 
			// lblLogicalDatabase
			// 
			this.lblLogicalDatabase.Location = new System.Drawing.Point(8, 16);
			this.lblLogicalDatabase.Name = "lblLogicalDatabase";
			this.lblLogicalDatabase.Size = new System.Drawing.Size(96, 16);
			this.lblLogicalDatabase.TabIndex = 0;
			this.lblLogicalDatabase.Text = "Logical Database";
			this.lblLogicalDatabase.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtLogicalDatabase
			// 
			this.txtLogicalDatabase.Location = new System.Drawing.Point(104, 16);
			this.txtLogicalDatabase.Name = "txtLogicalDatabase";
			this.txtLogicalDatabase.Size = new System.Drawing.Size(120, 20);
			this.txtLogicalDatabase.TabIndex = 1;
			this.txtLogicalDatabase.Text = "";
			// 
			// lblKey
			// 
			this.lblKey.Location = new System.Drawing.Point(8, 40);
			this.lblKey.Name = "lblKey";
			this.lblKey.Size = new System.Drawing.Size(88, 16);
			this.lblKey.TabIndex = 2;
			this.lblKey.Text = "Key";
			this.lblKey.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtKey
			// 
			this.txtKey.Location = new System.Drawing.Point(104, 40);
			this.txtKey.Name = "txtKey";
			this.txtKey.Size = new System.Drawing.Size(64, 20);
			this.txtKey.TabIndex = 3;
			this.txtKey.Text = "";
			// 
			// lblCommandText
			// 
			this.lblCommandText.Location = new System.Drawing.Point(8, 64);
			this.lblCommandText.Name = "lblCommandText";
			this.lblCommandText.Size = new System.Drawing.Size(96, 16);
			this.lblCommandText.TabIndex = 4;
			this.lblCommandText.Text = "Command Text";
			this.lblCommandText.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtCommandText
			// 
			this.txtCommandText.Location = new System.Drawing.Point(104, 64);
			this.txtCommandText.Name = "txtCommandText";
			this.txtCommandText.Size = new System.Drawing.Size(168, 20);
			this.txtCommandText.TabIndex = 5;
			this.txtCommandText.Text = "";
			// 
			// lblParameters
			// 
			this.lblParameters.Location = new System.Drawing.Point(8, 88);
			this.lblParameters.Name = "lblParameters";
			this.lblParameters.Size = new System.Drawing.Size(96, 16);
			this.lblParameters.TabIndex = 6;
			this.lblParameters.Text = "Parameters";
			this.lblParameters.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtParameters
			// 
			this.txtParameters.Location = new System.Drawing.Point(104, 88);
			this.txtParameters.Name = "txtParameters";
			this.txtParameters.Size = new System.Drawing.Size(448, 20);
			this.txtParameters.TabIndex = 7;
			this.txtParameters.Text = "";
			// 
			// lblExampleParameters
			// 
			this.lblExampleParameters.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblExampleParameters.Location = new System.Drawing.Point(104, 112);
			this.lblExampleParameters.Name = "lblExampleParameters";
			this.lblExampleParameters.Size = new System.Drawing.Size(448, 16);
			this.lblExampleParameters.TabIndex = 8;
			this.lblExampleParameters.Text = "(@MemberID=1:int;@EmailAddress=bnickel@matchnet.com:string)";
			this.lblExampleParameters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnExecute
			// 
			this.btnExecute.Location = new System.Drawing.Point(104, 136);
			this.btnExecute.Name = "btnExecute";
			this.btnExecute.Size = new System.Drawing.Size(72, 24);
			this.btnExecute.TabIndex = 9;
			this.btnExecute.Text = "Execute";
			this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
			// 
			// btnDataTable
			// 
			this.btnDataTable.Location = new System.Drawing.Point(184, 136);
			this.btnDataTable.Name = "btnDataTable";
			this.btnDataTable.Size = new System.Drawing.Size(72, 24);
			this.btnDataTable.TabIndex = 10;
			this.btnDataTable.Text = "DataTable";
			this.btnDataTable.Click += new System.EventHandler(this.btnDataTable_Click);
			// 
			// btnDataSet
			// 
			this.btnDataSet.Location = new System.Drawing.Point(264, 136);
			this.btnDataSet.Name = "btnDataSet";
			this.btnDataSet.Size = new System.Drawing.Size(72, 24);
			this.btnDataSet.TabIndex = 11;
			this.btnDataSet.Text = "DataSet";
			this.btnDataSet.Click += new System.EventHandler(this.btnDataSet_Click);
			// 
			// HarnessMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 573);
			this.Controls.Add(this.groupBox1);
			this.Name = "HarnessMain";
			this.Text = "Matchnet.Lib Harness";
			this.groupBox1.ResumeLayout(false);
			this.tcHarness.ResumeLayout(false);
			this.tpData.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new HarnessMain());
		}

		//==============================================================
		// Data

		private void btnExecute_Click(object sender, System.EventArgs e)
		{
			try 
			{
				SQLClient client = new SQLClient(GetDescriptor());
				AddParameters(client);
				client.ExecuteNonQuery(txtCommandText.Text, CommandType.StoredProcedure);
				AppendDataOutput("Executed Non Query");
			} 
			catch (Exception ex) 
			{
				AppendDataError(ex);
			}
		}

		private void btnDataTable_Click(object sender, System.EventArgs e)
		{
			try 
			{
				SQLClient client = new SQLClient(GetDescriptor());
				AddParameters(client);
				AppendXMLOutput(client.GetDataTable(txtCommandText.Text, CommandType.StoredProcedure));
			} 
			catch (Exception ex) 
			{
				AppendDataError(ex);
			}
		}

		private void btnDataSet_Click(object sender, System.EventArgs e)
		{
			try 
			{
				ExtendedSQLClient client = new ExtendedSQLClient(GetDescriptor());
				AddParameters(client);
				AppendXMLOutput(client.GetDataSet(txtCommandText.Text, CommandType.StoredProcedure));
			} 
			catch (Exception ex) 
			{
				AppendDataError(ex);
			}
		}

		private SQLDescriptor GetDescriptor() 
		{
			return new SQLDescriptor(txtLogicalDatabase.Text, System.Convert.ToInt32(txtKey.Text));					
		}

		private void AddParameters(SQLClient client) 
		{
			if (txtParameters.Text.Trim().Length > 0) 
			{
				string[] parameters = txtParameters.Text.Trim().Split(',');

				for (int x = 0; x < parameters.Length; x++) 
				{
					string param = parameters[x];
					int posA = param.IndexOf("=");
					string name = param.Substring(0, posA);
					int posB = param.IndexOf(":");
					string val = param.Substring(posA + 1, posB - (posA + 1));
					string type = param.Substring(posB + 1);

					// only supports these two types
					SqlDbType dbType = type.ToUpper().Equals("INT") ? SqlDbType.Int : SqlDbType.VarChar;

					if (dbType == SqlDbType.VarChar) 
					{
						client.AddParameter(name, dbType, ParameterDirection.Input, val, 255);
					} 
					else 
					{
						client.AddParameter(name, dbType, ParameterDirection.Input, System.Convert.ToInt32(val));
					}
				}
			}
		}

		private void AppendXMLOutput(DataTable dataTable) 
		{
			DataSet dataSet = new DataSet("CreatedForXMLOutput");
			dataSet.Tables.Add(dataTable);
			AppendXMLOutput(dataSet);
		}

		private void AppendXMLOutput(DataSet dataSet) 
		{
			ClearDataOutput();
			AppendDataOutput(dataSet.GetXml());
		}

		private void AppendDataError(Exception ex) 
		{
			ClearDataOutput();
			AppendDataOutput(ex.ToString());
		}

		private void AppendDataOutput(string message) 
		{
			txtDataOutput.AppendText(message + "\r\n");
		}

		private void ClearDataOutput() 
		{
			txtDataOutput.Text = "";
		}
	}
}
