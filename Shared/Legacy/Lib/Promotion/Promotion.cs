using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Caching;

using Matchnet.CachingTemp;
using Matchnet.DataTemp;

namespace Matchnet.Lib.Promotion
{
	/// <summary>
	/// Summary description for Promoter.
	/// </summary>
	public class Promotion
	{
		private const string PROMOTION_CACHEKEY_PREFIX = "Promotion:";
		private const int CACHE_TTL = 3600;
	    private Matchnet.CachingTemp.Cache _Cache;
	
		public Promotion()
		{
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
		}

		public DataTable GetPromotions(int promoterID)
		{
			string key = PROMOTION_CACHEKEY_PREFIX + promoterID.ToString();
			DataTable dt = (DataTable) _Cache[key];

			if (dt == null)
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnShared");
				SQLClient client = new SQLClient(descriptor);
				client.AddParameter("@PromoterID", SqlDbType.VarChar, ParameterDirection.Input, promoterID);
				dt = client.GetDataTable("up_Promotion_List", CommandType.StoredProcedure);
        // PT:  Removed old caching system dependency
				// System.Web.HttpRuntime.Cache.Insert( key, 
        //                                      dt, 
        //                                      null, 
        //                                      DateTime.Now.AddSeconds(CACHE_TTL), 
        //                                      TimeSpan.Zero, 
        //                                      CacheItemPriority.High, null);
        _Cache.Insert( key,
                       dt,
                       null,
                       DateTime.Now.AddSeconds( CACHE_TTL ),
                       TimeSpan.Zero,
                       CacheItemPriority.High,
                       null );
			}

			return dt;
		}
	}
}
