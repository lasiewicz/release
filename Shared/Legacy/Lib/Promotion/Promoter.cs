using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Caching;
using Matchnet.CachingTemp;

using Matchnet.DataTemp;

namespace Matchnet.Lib.Promotion
{
	/// <summary>
	/// Summary description for Promoter.
	/// </summary>
	public class Promoter
	{
		private const string PROMOTER_CACHEKEY_PREFIX = "Promoter:";
		private const int CACHE_TTL = 3600;
		private Matchnet.CachingTemp.Cache _Cache;
	
		public Promoter()
		{
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
		}

		public DataTable GetPromoters()
		{
			string key = PROMOTER_CACHEKEY_PREFIX;
			DataTable dt = (DataTable) _Cache[key];

			if (dt == null)
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnShared");
				SQLClient client = new SQLClient(descriptor);
				dt = client.GetDataTable("up_Promoter_List", CommandType.StoredProcedure);
        // PT: Removed old caching depedency
				// System.Web.HttpRuntime.Cache.Insert( key, 
        //                                      dt, 
        //                                      null, 
        //                                      DateTime.Now.AddSeconds(CACHE_TTL), 
        //                                      TimeSpan.Zero, 
        //                                      CacheItemPriority.High, 
        //                                      null );
        _Cache.Insert( key,
                       dt,
                       null,
                       DateTime.Now.AddSeconds( CACHE_TTL ),
                       TimeSpan.Zero,
                       CacheItemPriority.High,
                       null );
			}

			return dt;
		}
	}
}
