using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Web.Caching;

using Matchnet.CachingTemp;
using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Region
{
	/// <summary>
	/// Summary description for RegionLanguage.
	/// </summary>
	public class RegionLanguage
	{
		private const string REGION_CACHEKEY_PREFIX = "Region:";
		private const int CACHE_TTL = 300;

		private string _PostalCode;
		private int _StateRegionID;
		private string _StateAbbreviation;
		private string _StateDescription;
		private int _CityRegionID;
		private string _CityName;
		private int _PostalCodeRegionID;
		private int _CountryRegionID;
		private string _CountryAbbreviation;
		private string _CountryName;
		private int _HighestDepthRegionTypeID;
		private decimal _Longitude;
		private decimal _Latitude;
		private int _Depth1RegionID;

		public RegionLanguage()
		{
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
		}

		private Matchnet.CachingTemp.Cache _Cache;

		public int PopulateByPostalCode(int countryRegionID, string postalCode)
		{
			int regionID = Matchnet.Constants.NULL_INT;
			string key = REGION_CACHEKEY_PREFIX + "PopulateByPostalCode:" + countryRegionID.ToString() + 
				":" + postalCode;
			object o = _Cache[key];

			if (o != null)
			{
				regionID = (int)o;
			}
			else
			{
				try
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@CountryRegionID", SqlDbType.Int, ParameterDirection.Input, countryRegionID);
					client.AddParameter("@PostalCode", SqlDbType.NVarChar , ParameterDirection.Input, postalCode);
					client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

					SqlCommand command = new SqlCommand();
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "up_Region_ListPostalCodeRegionID";
					client.PopulateCommand(command);
					DataTable dt = client.GetDataTable(command);
					if (dt.Rows.Count > 0)
					{
						regionID = Convert.ToInt32(dt.Rows[0]["RegionID"]);
						_Cache.Insert(key, regionID, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(CACHE_TTL), CacheItemPriority.Normal, null);
					}
					else
					{
						// Invalid zip code
						regionID = Matchnet.Constants.NULL_INT;
					}

				}
				catch(Exception ex)
				{
					throw new MatchnetException("Unable to load regiond ID. [ postalCode: " + postalCode + " countryRegionID: " + countryRegionID.ToString() + "]","RegionLangauge.PopulateByCity" ,ex);
				}
			}

			return regionID;
		}

		public int PopulateByCity(int parentRegionID, string description, int languageID)
		{
			int regionID = Matchnet.Constants.NULL_INT;
			string key = REGION_CACHEKEY_PREFIX + "PopulateByCity:" + parentRegionID.ToString() + ":" + languageID.ToString() + ":" + description;
			object o = _Cache[key];

			if (o != null)
			{
				regionID = (int)o;
			}
			else
			{
				try
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@ParentRegionID", SqlDbType.Int, ParameterDirection.Input, parentRegionID);
					client.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, description);
					client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
					client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

					SqlCommand command = new SqlCommand();
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "up_Region_ListCityRegionID";
					client.PopulateCommand(command);
					DataTable dt = client.GetDataTable(command);
					regionID = Convert.ToInt32(dt.Rows[0]["RegionID"]);

					_Cache.Insert(key, regionID, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(CACHE_TTL), CacheItemPriority.Normal, null);
				}
				catch( Exception ex)
				{
					throw new MatchnetException("Unable to load regiond ID. [ description: " + description + " parentRegionID: " + parentRegionID.ToString() + "]","RegionLangauge.PopulateByCity" ,ex);
				}
			}

			return regionID;
		}


		public DataTable Populate(int regionID, int languageID)
		{
			string key = REGION_CACHEKEY_PREFIX + ":Region: " + regionID + ":" + languageID.ToString();
			DataTable dt = (DataTable)_Cache[key];

			if (dt == null)
			{
				try
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
					client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
					dt = client.GetDataTable("up_Region_List", CommandType.StoredProcedure);
				} 
				catch (Exception ex) 
				{
					throw new MatchnetException("Unable to load region inforation. [ RegionID : " + regionID + ", languageID : " + languageID + "]", "RegionLanguage.Populate", ex);
				}
			}
			if (dt != null) 
			{
				_Cache.Insert(key, dt, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(CACHE_TTL), CacheItemPriority.Normal, null );
			}

			return dt;
		}

		// temporary function utilized by SearchByLocationData.ascx.cs for forcing a fresh load from
		// the database due to translation issues encountered when retrieving previously translated
		// content from the cache.
		public DataTable GetChildRegions(int parentRegionID, int languageID, int translationID, bool forceLoad) 
		{
			string key = null;
			if(forceLoad) 
			{
				key = REGION_CACHEKEY_PREFIX + ":Children:" + parentRegionID + ":" + languageID.ToString();
				if(_Cache[key] != null) 
				{
					_Cache.Remove(key);
				}
			}
			return GetChildRegions(parentRegionID, languageID, translationID);
		}

		public DataTable GetChildRegions(int parentRegionID, int languageID)
		{
			return GetChildRegions(parentRegionID, languageID, Matchnet.Constants.NULL_STRING);
		}

		public DataTable GetChildRegions(int parentRegionID, int languageID, string description)
		{
			return GetChildRegions(parentRegionID, languageID, description, Matchnet.Constants.NULL_INT);
		}

		public DataTable GetChildRegions(int parentRegionID, int languageID, int translationID)
		{
			return GetChildRegions(parentRegionID, languageID, Matchnet.Constants.NULL_STRING, translationID);
		}

		public DataTable GetChildRegions(int parentRegionID, int languageID, string description, int translationID)
		{
			string key = REGION_CACHEKEY_PREFIX + ":Children:" + parentRegionID + ":" + languageID.ToString();
			DataTable dt = (DataTable) _Cache[ key ];

			if (dt == null) 
			{
				try
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@ParentRegionID", SqlDbType.Int, ParameterDirection.Input, parentRegionID);
					client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
					if (description != Matchnet.Constants.NULL_STRING)
					{
						key += ":" + description;
						client.AddParameter("@Description", SqlDbType.VarChar, ParameterDirection.Input, description);
					}
					dt = client.GetDataTable("up_Region_ListChildren", CommandType.StoredProcedure);
					
					if (translationID != Matchnet.Constants.NULL_INT)
					{
						Matchnet.Lib.Option.Option option = new Matchnet.Lib.Option.Option();
						if (option.IsHebrewTranslation(translationID))
						{
							dt = this.UnicodeTransform(dt);
						}
					}
				}
				catch (Exception ex) 
				{
					throw new MatchnetException("Unable to load child regions for region. [ parentRegionID : " + parentRegionID + ", languageID : " + languageID + "]", "RegionLanguage.GetChildRegions", ex);
				}
			}

			if (dt != null) 
			{
				_Cache.Insert(key, dt, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(CACHE_TTL), CacheItemPriority.Normal, null);
			}

			return dt;
		}

		/// <summary>
		/// this is found in Option class too.
		/// need to consolidate into transltor lib
		/// </summary>
		/// <param name="translationID"></param>
		/// <returns></returns>
		public DataTable UnicodeTransform(DataTable dt)
		{
			mnTranslator.IUnicodeEncoder encoder = new mnTranslator.UnicodeEncoderClass();
			foreach ( DataRow row in dt.Rows )
			{
				string rawVal = row["Description"].ToString();
				if ( rawVal.Length > 0 )
				{
					try
					{
						encoder.SetMbText(rawVal,1255);
						row["Description"] = encoder.GetUnicodeText();
					}
					catch(Exception ex)
					{
						System.Diagnostics.Trace.WriteLine(ex);
					}
				}
			}
			return dt;
		}


		public DataTable GetCountries()
		{
			return GetCountries(2); // english
		}

		public DataTable GetCountries(int languageId) 
		{
			string key = REGION_CACHEKEY_PREFIX + ":Countries:" + languageId.ToString();
			DataTable dt = (DataTable) _Cache[key];

			if (dt == null) 
			{
				try 
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageId);
					dt = client.GetDataTable("up_Region_ListTop", CommandType.StoredProcedure);
				
					if (dt.Rows.Count > 0)
					{
						AsciiToUnicode(dt, languageId, "Description");
						_Cache.Insert(key, dt, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(CACHE_TTL), CacheItemPriority.Normal, null);
					}
				} 
				catch (Exception ex) 
				{
					throw new MatchnetException("Unable to load countries. [ languageId : " + languageId + "]", "RegionLanguage.GetCountries", ex);
				}
			}
			
			return dt;
		}

		public void AsciiToUnicode(DataTable dt, int languageId, string field)
		{
			if (dt != null && languageId == ConstantsTemp.LANG_HEBREW)
			{
				
				// The translation wont work if you don't set the current culture to english
				System.Globalization.CultureInfo culture = Thread.CurrentThread.CurrentCulture;
				Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);
				
				mnTranslator.IUnicodeEncoder encoder = new mnTranslator.UnicodeEncoderClass();

				foreach( DataRow row in dt.Rows )
				{
					string raw = row[field].ToString();
					encoder.SetMbText(raw, 1255);
					row[field] = encoder.GetUnicodeText();
				}

				Thread.CurrentThread.CurrentCulture = culture;
			}
		}

		public void PopulateHierarchy(int regionID, int languageID)
		{
			PopulateHierarchy(regionID, languageID, Matchnet.Constants.NULL_INT);
		}

		public void PopulateHierarchy(int regionId, int languageId, int maxDepth)
		{
			string key = REGION_CACHEKEY_PREFIX + ":Heirarchy:" + regionId.ToString() + ":" + languageId.ToString() + ":" + maxDepth.ToString();

			object o = _Cache[key];
			CachedRegion cachedRegion = null;

			if (o != null)
			{
				cachedRegion = (CachedRegion)o;
			}
			else
			{
				try 
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionId);
					if (maxDepth != Matchnet.Constants.NULL_INT)
					{
						client.AddParameter("@MaxDepth", SqlDbType.Int, ParameterDirection.Input, maxDepth);
					}
					client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageId);

					DataTable dt = client.GetDataTable("up_Region_ListHierarchy", CommandType.StoredProcedure);

					if (dt.Rows.Count > 0)
					{
						AsciiToUnicode(dt, languageId, "Description");

						foreach (DataRow row in dt.Rows)
						{
							CachedRegion currentRegion = new CachedRegion();
							currentRegion.RegionID = Convert.ToInt32(row["RegionID"]);
							currentRegion.Depth = Convert.ToInt32(row["Depth"]);
							currentRegion.Latitude = Convert.ToDecimal(row["Latitude"]);
							currentRegion.Longitude = Convert.ToDecimal(row["Longitude"]);
							currentRegion.Description = row["Description"].ToString();
							currentRegion.Abbreviation = row["Abbreviation"].ToString();
							currentRegion.Mask = Convert.ToInt32(row["Mask"]);
							if (row["ChildrenDepth"] != DBNull.Value)
							{
								currentRegion.ChildrenDepth = Convert.ToInt32(row["ChildrenDepth"]);
							}
							else
							{
								currentRegion.ChildrenDepth = Matchnet.Constants.NULL_INT;
							}

							if (cachedRegion == null)
							{
								cachedRegion = currentRegion;
							}
							else
							{
								currentRegion.ParentRegion = cachedRegion;
								cachedRegion = currentRegion;
							}
						}
						_Cache.Insert(key, cachedRegion, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(CACHE_TTL), CacheItemPriority.Normal, null);
					}
				} 
				catch (Exception ex) 
				{
					throw new MatchnetException("Unable to populate region hierarchy [ regionID : " + 
						regionId + ", languageID : " + languageId + ", maxDepth : " + maxDepth + "]", "RegionLanguage.PopulateHierarchy", ex);
				}
			}

			if (cachedRegion != null)
			{
				do
				{
					switch (cachedRegion.Depth)
					{
						case 4:
							_PostalCodeRegionID = cachedRegion.RegionID;
							_PostalCode = cachedRegion.Description;
							_Longitude = cachedRegion.Longitude;
							_Latitude = cachedRegion.Latitude;
							break;
						case 3:
							_CityRegionID = cachedRegion.RegionID;
							_CityName = cachedRegion.Description;
							_Longitude = cachedRegion.Longitude;
							_Latitude = cachedRegion.Latitude;
							break;
						case 2:
							_StateRegionID = cachedRegion.RegionID;
							_StateAbbreviation = cachedRegion.Abbreviation;
							_StateDescription = cachedRegion.Description;
							break;
						case 1:
							_CountryRegionID = cachedRegion.RegionID;
							_CountryAbbreviation = cachedRegion.Abbreviation;
							_CountryName = cachedRegion.Description;
							_Depth1RegionID = cachedRegion.RegionID;
							break;
					}

					if (cachedRegion.Depth != Matchnet.Constants.NULL_INT)
					{
						int depth = cachedRegion.Depth;
						if (depth > _HighestDepthRegionTypeID)
						{
							_HighestDepthRegionTypeID = depth;
						}
					}

					cachedRegion = cachedRegion.ParentRegion;
				}
				while (cachedRegion != null);
			}
		}

		public static string GetSchoolName(int schoolRegionID)
		{
			string key = "SchoolRegion:" + schoolRegionID.ToString();
			Matchnet.CachingTemp.Cache cache = Matchnet.CachingTemp.Cache.GetInstance();
			string schoolName = "";

			object o = cache.Get(key);

			if (o != null)
			{
				schoolName = o.ToString();
			}
			else
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
				SQLClient client = new SQLClient(descriptor);
				client.AddParameter("@SchoolRegionID", SqlDbType.Int, ParameterDirection.Input, schoolRegionID);
				DataTable dt = client.GetDataTable("up_SchoolRegion_List", CommandType.StoredProcedure);

				if (dt.Rows.Count == 1)
				{
					schoolName = dt.Rows[0]["Name"].ToString();
				}
				else
				{
					schoolName = "";
				}

				cache.Insert(key, schoolName, null, DateTime.Now.AddDays(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
			}

			return schoolName;
		}

		public static DataTable GetSchoolParents(int schoolRegionID)
		{
			string key = "SchoolRegionParents:" + schoolRegionID.ToString();
			Matchnet.CachingTemp.Cache cache = Matchnet.CachingTemp.Cache.GetInstance();
			DataTable dt = null;

			object o = cache.Get(key);

			if (o != null)
			{
				dt = (DataTable) o;
			}
			else
			{
				SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
				SQLClient client = new SQLClient(descriptor);
				client.AddParameter("@SchoolRegionID", SqlDbType.Int, ParameterDirection.Input, schoolRegionID);
				dt = client.GetDataTable("up_SchoolRegion_List", CommandType.StoredProcedure);

				cache.Insert(key, dt, null, DateTime.Now.AddDays(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
			}

			return dt;
		}

		public static DataTable GetSchoolListByStateRegion(int stateRegionID)
		{

			string key = "SchoolListByStateRegion:" + stateRegionID.ToString();
			Matchnet.CachingTemp.Cache cache = Matchnet.CachingTemp.Cache.GetInstance();
			DataTable dt = (DataTable) cache.Get(key);

			if (dt == null) 
			{
				try 
				{
					SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
					SQLClient client = new SQLClient(descriptor);
					client.AddParameter("@StateRegionID", SqlDbType.Int, ParameterDirection.Input, stateRegionID);
					dt = client.GetDataTable("up_SchoolRegion_List", CommandType.StoredProcedure);
				
					if (dt.Rows.Count > 0)
					{
						//AsciiToUnicode(dt, languageId, "Description");
						cache.Insert(key, dt, null, DateTime.Now.AddDays(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
					}
				} 
				catch (Exception ex) 
				{
					throw new MatchnetException("Unable to load school list. [ stateRegionID : " + stateRegionID + "]", "RegionLanguage.GetSchoolListByStateRegion", ex);
				}
			}
			
			return dt;
		}

		public static string popCityList(int PrivateLabelID, string RegionControlName, string cityControlName)
		{
			System.Text.StringBuilder script = new System.Text.StringBuilder();
			script.Append("<script language='javascript'>\n");
			script.Append("function popCityList(){\n");
			script.Append(@"var parentRegionID = document.Main.elements['" + RegionControlName + "'].value;");
			script.Append(@"window.open('/Applications/MemberProfile/CityList.aspx?" +
				"plid=" + PrivateLabelID +
				"&CityControlName=" + cityControlName + "&ParentRegionID=' + parentRegionID, '','height=415px,width=500px,scrollbars=yes');\n");

			script.Append("}\n</script>\n");
			return script.ToString();
		}

		#region public properties
		public string PostalCode
		{
			get
			{
				return _PostalCode;
			}
		}

		public int StateRegionID
		{
			get
			{
				return _StateRegionID;
			}
		}

		public string StateAbbreviation
		{
			get
			{
				return _StateAbbreviation;
			}
		}

		public string StateDescription
		{
			get { return _StateDescription; }
		}

		public int PostalCodeRegionID
		{
			get
			{
				return _PostalCodeRegionID;
			}
		}
		public int CityRegionID
		{
			get
			{
				return _CityRegionID;
			}
		}

		public string CityName
		{
			get
			{
				return _CityName;
			}
		}

		public int CountryRegionID
		{
			get
			{
				return _CountryRegionID;
			}
		}

		public string CountryAbbreviation
		{
			get
			{
				return _CountryAbbreviation;
			}
		}

		public string CountryName
		{
			get
			{
				return _CountryName;
			}
		}

		public int HighestDepthRegionTypeID
		{
			get
			{
				return _HighestDepthRegionTypeID;
			}
		}

		public decimal Longitude
		{
			get
			{
				return _Longitude;
			}
		}

		public decimal Latitude
		{
			get
			{
				return _Latitude;
			}
		}

		public int Depth1RegionID
		{
			get
			{
				return _Depth1RegionID;
			}
		}

		#endregion

	}
}
