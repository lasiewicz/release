using System;
using System.Web.Caching;
using System.Data;

using Matchnet.CachingTemp;
using Matchnet.DataTemp;
using Matchnet.Lib.Exceptions;

namespace Matchnet.Lib.Region {
	/// <summary>
	/// Summary description for ISOCode.
	/// </summary>
	public class ISOCode {
		private const int REGION_USA = 223;
		private const string CACHEKEY_PREFIX = "RegionISO:";
		private const int CACHE_TTL = 3600;
		// PT:  Added new caching dependency
		private Matchnet.CachingTemp.Cache _Cache;

		private int mRegionID = Matchnet.Constants.NULL_INT;
		public int RegionID {
			set { mRegionID = value; }
		}

		private string mISO2 = string.Empty;
		public string ISO2 {
			get { 
				if (mISO2 == string.Empty) GetISOCountryCodes(); 
				return mISO2;
			}
		}

		private string mISO3 = string.Empty;
		public string ISO3 {
			get { 
				if (mISO3 == string.Empty) GetISOCountryCodes(); 
				return mISO3;
			}
		}

		public ISOCode() { 
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
		}

		public ISOCode(int RegionID) {
			_Cache = Matchnet.CachingTemp.Cache.GetInstance();
			mRegionID = RegionID;
		}

		private void GetISOCountryCodes() {
			try {
				if (mRegionID == Matchnet.Constants.NULL_INT)
					mRegionID = REGION_USA;

				string key = CACHEKEY_PREFIX + mRegionID.ToString();
				ISOCode CachedISO = (ISOCode)_Cache[key];

				if (CachedISO == null) {
				
					SQLClient client = new SQLClient(new SQLDescriptor("mnRegion"));
					client.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, mRegionID);

					DataTable table = client.GetDataTable("up_Country_List", CommandType.StoredProcedure);

					if (table != null && table.Rows.Count > 0) {
						mISO2 = System.Convert.ToString(table.Rows[0]["ISOCountryCode2"]);
						mISO3 = System.Convert.ToString(table.Rows[0]["ISOCountryCode3"]);
					}
					else {
						throw new MatchnetException("Unable to get ISOCountryCodes for " + mRegionID);
					}

					// PT:  Removed old caching dependency
					// System.Web.HttpRuntime.Cache.Insert(key, this.MemberwiseClone(), null, 
					//	DateTime.Now.AddSeconds(CACHE_TTL), TimeSpan.Zero, CacheItemPriority.Low, null);
					_Cache.Insert( key, 
						this.MemberwiseClone(),
						null,
						DateTime.Now.AddSeconds( CACHE_TTL ),
						TimeSpan.Zero,
						CacheItemPriority.Low,
						null );
				}
				else {
					mISO2 = CachedISO.ISO2;
					mISO3 = CachedISO.ISO3;
				}
			} 
			catch (Exception ex) {
				throw new MatchnetException("Error Getting ISOCountryCodes for " + mRegionID, "GetISOCountryCodes", ex);
			}
		}


	}
}
