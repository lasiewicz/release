using System;

using Matchnet.Lib;

namespace Matchnet.Lib.Region
{
	public class CachedRegion
	{
		private int _RegionID;
		private int _Depth;
		private decimal _Latitude;
		private decimal _Longitude;
		private string _Description;
		private string _Abbreviation;
		private int _Mask;
		private int _ChildrenDepth ;
		private CachedRegion _ParentRegion = null;

		public int RegionID
		{
			get
			{
				return _RegionID;
			}
			set
			{
				_RegionID = value;
			}
		}


		public int ParentRegionID
		{
			get
			{
				if (_ParentRegion != null)
				{
					return _ParentRegion.RegionID;
				}
				else
				{
					return Matchnet.Constants.NULL_INT;
				}
			}
		}

		public int Depth
		{
			get
			{
				return _Depth;
			}
			set
			{
				_Depth = value;
			}
		}


		public decimal Latitude
		{
			get
			{
				return _Latitude;
			}
			set
			{
				_Latitude = value;
			}
		}


		public decimal Longitude
		{
			get
			{
				return _Longitude;
			}
			set
			{
				_Longitude = value;
			}
		}


		public string Description
		{
			get
			{
				return _Description;
			}
			set
			{
				_Description = value;
			}
		}


		public string Abbreviation
		{
			get
			{
				return _Abbreviation;
			}
			set
			{
				_Abbreviation = value;
			}
		}


		public int Mask
		{
			get
			{
				return _Mask;
			}
			set
			{
				_Mask = value;
			}
		}


		public int ChildrenDepth
		{
			get
			{
				return _ChildrenDepth;
			}
			set
			{
				_ChildrenDepth = value;
			}
		}


		public CachedRegion ParentRegion
		{
			get
			{
				return _ParentRegion;
			}
			set
			{
				_ParentRegion = value;
			}
		}



	}
}
