using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

using Matchnet.Lib;
using Matchnet.Lib.Content;
using Matchnet.Lib.Data;
using Matchnet.Lib.Util;

namespace Matchnet.Lib.Region
{
	public class Regions : ArrayList
	{
		#region private variables
		private string _PostalCode = "";
		private int _StateRegionID;
		private string _StateAbbreviation = "";
		private int _CityRegionID;
		private string _CityName = "";
		private int _CountryRegionID;
		private string _CountryAbbreviation = "";
		private int _HighestDepthRegionTypeID;
		#endregion

		#region public properties
		public string PostalCode
		{
			get
			{
				return _PostalCode;
			}
		}
		public int StateRegionID
		{
			get
			{
				return _StateRegionID;
			}
		}
		public string StateAbbreviation
		{
			get
			{
				return _StateAbbreviation;
			}
		}
		public int CityRegionID
		{
			get
			{
				return _CityRegionID;
			}
		}
		public string CityName
		{
			get
			{
				return _CityName;
			}
		}
		public int CountryRegionID
		{
			get
			{
				return _CountryRegionID;
			}
		}

		#endregion

		#region constructor
		public Regions()
		{
			_HighestDepthRegionTypeID= Constants.NULL_NUMBER;
			_StateRegionID = Constants.NULL_NUMBER;
			_CityRegionID = Constants.NULL_NUMBER;
			_CountryRegionID = Constants.NULL_NUMBER;
			_HighestDepthRegionTypeID = 1;
		}
		#endregion

		#region methods
		public void Populate(int parentRegionID)
		{
			Populate(parentRegionID, "");
		}
		public void Populate(int parentRegionID, string description)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@ParentRegionID", SqlDbType.Int, ParameterDirection.Input, parentRegionID);
			
			if (description != "")
			{
				client.AddParameter("@Description", SqlDbType.NText, ParameterDirection.Input, description);
			}
			DataTable dt = client.GetDataTable("up_Region_ListChildren", CommandType.StoredProcedure);
			
			foreach(DataRow row in dt.Rows)
			{
				Region region = new Region();
				region.ParentRegionID = Util.Util.CInt(row["ParentRegionID"]);
				region.Latitude = Util.Util.CDouble(row["Latitude"]);
				region.Longitude = Util.Util.CDouble(row["Longitude"]);
				region.Description = Util.Util.CString(row["Description"]);
				region.Abbreviation = Util.Util.CString(row["Abbreviation"]);
				region.Depth = Util.Util.CInt(row["Depth"]);
				region.ChildrenDepth = Util.Util.CInt(row["ChildrenDepth"]);
				this.Add(region);
			}
		}
		public void PopulateHierarchy(int regionID)
		{
			PopulateHierarchy(regionID, Constants.NULL_NUMBER);
		}

		public void PopulateHierarchy(int regionID, int maxDepth)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
			client.AddParameter("@MaxDepth", SqlDbType.Int, ParameterDirection.Input, maxDepth);
			DataTable dt = client.GetDataTable("up_Region_ListHierarchy", CommandType.StoredProcedure);

			foreach(DataRow row in dt.Rows)
			{
				Region region = new Region();
				region.RegionID = Util.Util.CInt(row["RegionID"]);
				region.ParentRegionID = Util.Util.CInt(row["ParentRegionID"]);
				region.Depth = Util.Util.CInt(row["Depth"]);
				region.Latitude = Util.Util.CDouble(row["Latitude"]);
				region.Longitude = Util.Util.CDouble(row["Longitude"]);
				region.Description = Util.Util.CString(row["Description"]);
				region.Abbreviation = Util.Util.CString(row["Abbreviation"]);
				region.RegionTypeID = Util.Util.CInt(row["Mask"]);
				region.ChildrenDepth = Util.Util.CInt(row["ChildrenDepth"]);
				this.Add(region);
			}
		}
		#endregion
	}
}


