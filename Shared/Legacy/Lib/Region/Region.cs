using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Lib.Data;
using Matchnet.Lib;
using Matchnet.Lib.Content;

namespace Matchnet.Lib.Region
{
	/// <summary>
	/// mnRegion
	/// </summary>
	public class Region
	{
		#region private variables
		private int _RegionID = 0;
		private int _ParentRegionID = 0;
		private int _RegionTypeID = 0;
		private double _Latitude = 0;
		private double _Longitude = 0;
		private string _Description = "";
		private string _Abbreviation = "";
		private int _Depth = 0;
		private bool _HasChildren = false;
		private int _ChildrenDepth = 0;
		#endregion

		#region public properties
		public int RegionID
		{
			get
			{
				return _RegionID;
			}
			set
			{
				_RegionID = value;
			}
		}
		public int ParentRegionID
		{
			get
			{
				return _ParentRegionID;
			}
			set
			{
				_ParentRegionID = value;
			}
		}
		public int RegionTypeID
		{
			get
			{
				return _RegionTypeID;
			}
			set
			{
				_RegionTypeID = value;
			}
		}

		public double Latitude
		{
			get
			{
				return _Latitude;
			}
			set
			{
				_Latitude = value;
			}
		}
		public double Longitude
		{
			get
			{
				return _Longitude;
			}
			set
			{
				_Longitude = value;
			}
		}
		public string Description
		{
			get
			{
				return _Description;
			}
			set
			{
				_Description = value;
			}
		}
		public string Abbreviation
		{
			get
			{
				return _Abbreviation;
			}
			set
			{
				_Abbreviation = value;
			}
		}
		public int Depth
		{
			get
			{
				return _Depth;
			}
			set
			{
				_Depth= value;
			}
		}
		public bool HasChildren
		{
			get
			{
				return _HasChildren;
			}
			set
			{
				_HasChildren = value;
			}
		}
		public int ChildrenDepth
		{
			get
			{
				return _ChildrenDepth;
			}
			set
			{
				_ChildrenDepth = value;
			}
		}
		#endregion

		#region constructor
		public Region()
		{
		}
		#endregion

		#region methods
		public int PopulateByPostalCode(int countryRegionID, string postalCode)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@CountryRegionID", SqlDbType.Int, ParameterDirection.Input, countryRegionID);
			client.AddParameter("@PostalCode", SqlDbType.NVarChar , ParameterDirection.Input, postalCode);
			client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_Region_ListPostalCodeRegionID";
			client.PopulateCommand(command);
			DataTable dt = client.GetDataTable(command);
			this._RegionID = (int) dt.Rows[0]["RegionID"];
			return Util.Util.CInt(command.Parameters["@ReturnValue"].Value);
		}
		
		public int PopulateByCity(int parentRegionID, string description, int languageID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@ParentRegionID", SqlDbType.Int, ParameterDirection.Input, parentRegionID);
			client.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, description);
			client.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
			client.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "up_Region_ListCityRegionID";
			client.PopulateCommand(command);
			DataTable dt = client.GetDataTable(command);
			this._RegionID = (int) dt.Rows[0]["RegionID"];
			return Util.Util.CInt(command.Parameters["@ReturnValue"].Value);
		}

		public void Populate()
		{
			Populate(this._RegionID);
		}
		public void Populate(int regionID)
		{
			SQLDescriptor descriptor = new SQLDescriptor("mnRegion");
			SQLClient client = new SQLClient(descriptor);
			client.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
			DataTable dt = client.GetDataTable("up_Region_List",CommandType.StoredProcedure);
			DataRow row = dt.Rows[0];
			this._ParentRegionID = Util.Util.CInt(row["ParentRegionID"]);
			this._Latitude = Util.Util.CDouble(row["Latitude"]);
			this._Longitude = Util.Util.CDouble(row["Longitude"]);
			this._Description = Util.Util.CString(row["Description"]);
			this._Abbreviation = Util.Util.CString(row["Abbreviation"]);
			this._Depth = Util.Util.CInt(row["Depth"]);
			this._ChildrenDepth = Util.Util.CInt(row["ChildrenDepth"]);
		}
		#endregion
	}
}
