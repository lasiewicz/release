#region System References
using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Windows.Forms;
#endregion

#region Matchnet Service References
using Matchnet.Content.ServiceAdapters;
#endregion

#region Matchnet Web App References
using Cache = Matchnet.CachingTemp.Cache;
#endregion

namespace Matchnet.Labels
{
	public class PrivateLabel 
	{
		#region Constructors
		/// <summary>
		/// Default Constructor
		/// </summary>
		public PrivateLabel() 
		{
			_InternalCache = Cache.GetInstance();
			if( _InternalCache == null )
				throw new Exception( "Unable to allocate " + 
					"Matchnet.CachingTemp.Cache object." );
		}
		#endregion
	
		#region Public Properties

		/// <summary>
		/// Private Label ID
		/// </summary>
		public int PrivateLabelID 
		{
			get 
			{
				return _PrivateLabelID;
			}
		}


		/// <summary>
		/// Domain ID
		/// </summary>
		public int DomainID 
		{
			get 
			{
				return _DomainID;
			}
		}

		/// <summary>
		/// Default Current ID
		/// </summary>
		public int DefaultCurrencyID
		{
			get
			{
				return _DefaultCurrencyID;
			}
		}

		/// <summary>
		/// Language ID
		/// </summary>
		public int LanguageID 
		{
			get 
			{
				return _LanguageID;
			}
		}


		/// <summary>
		/// Translation ID
		/// </summary>
		public int TranslationID 
		{
			get 
			{
				return _TranslationID;
			}
		}

		/// <summary>
		/// Base HTTP URL
		/// </summary>
		public string BaseHttpUrl 
		{
			get 
			{
				return "http://" + _DefaultHost + "." + _URI;
			}
		}


		/// <summary>
		/// URI
		/// </summary>
		public string URI 
		{
			get 
			{
				return _URI;
			}
		}


		/// <summary>
		/// Color Dark
		/// </summary>
		public string ColorDark 
		{
			get
			{
				return _ColorDark;
			}
		}


		/// <summary>
		/// Color Darker
		/// </summary>
		public string ColorDarker 
		{
			get 
			{
				return _ColorDarker;
			}
		}


		/// <summary>
		/// Color Darkest
		/// </summary>
		public string ColorDarkest 
		{
			get
			{
				return _ColorDarkest;
			}
		}


		/// <summary>
		/// Color Light
		/// </summary>
		public string ColorLight 
		{
			get 
			{
				return _ColorLight;
			}
		}


		/// <summary>
		/// Color Lighter
		/// </summary>
		public string ColorLighter 
		{
			get
			{
				return _ColorLighter;
			}
		}


		/// <summary>
		/// Color Lightest
		/// </summary>
		public string ColorLightest 
		{
			get 
			{
				return _ColorLightest;
			}
		}


		/// <summary>
		/// Color Medium
		/// </summary>
		public string ColorMedium 
		{
			get 
			{
				return _ColorMedium;
			}
		}	


		/// <summary>
		/// Left Align
		/// </summary>
		public string LAlign 
		{
			get 
			{
				return _LAlign;
			}

		}
		
		
		/// <summary>
		/// Right Align
		/// </summary>
		public string RAlign 
		{
			get 
			{
				return _RAlign;
			}

		}


		/// <summary>
		/// Character Set
		/// </summary>
		public string CharSet 
		{
			get 
			{
				return _CharSet;
			}

		}
		

		/// <summary>
		/// Text Direction
		/// </summary>
		public string Direction 
		{
			get 
			{
				return _Direction;
			}
		}


		/// <summary>
		/// Image URL
		/// </summary>
		public string ImgURL 
		{
			get 
			{
				return _ImgURL;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string ImageRoot
		{
			get 
			{
				return _ImageRoot;
			}
		}

		/// <summary>
		/// Locale ID
		/// </summary>
		public int LCID 
		{
			get 
			{
				return _LCID;
			}
		}


		/// <summary>
		/// GMT Offset
		/// </summary>
		public double GMTOffset 
		{
			get 
			{
				return _GMTOffset;
			}
		}
		

		/// <summary>
		/// Search Type Mask
		/// </summary>
		public int SearchTypeMask
		{
			get
			{
				return _SearchTypeMask;
			}
		}


		/// <summary>
		/// Base Private Label ID
		/// </summary>
		public int BasePrivateLabelID
		{
			get
			{
				return _BasePrivateLabelID;
			}
		}


		/// <summary>
		/// Redirect to Base
		/// </summary>
		public bool RedirectToBase
		{
			get
			{
				return _RedirectToBase;
			}
		}


		/// <summary>
		/// Status Mask
		/// </summary>
		public int StatusMask
		{
			get
			{
				return _StatusMask;
			}
		}


		/// <summary>
		/// Default Host
		/// </summary>
		public string DefaultHost
		{
			get
			{
				return _DefaultHost;
			}
		}


		/// <summary>
		/// Default Region ID
		/// </summary>
		public int DefaultRegionID
		{
			get
			{
				return _DefaultRegionID;
			}
		}


		
		/// <summary>
		/// Registration Step Count
		/// </summary>
		public int RegistrationStepCount
		{
			get
			{
				return _RegistrationStepCount;
			}
		}

		/// <summary>
		/// Default Min Age
		/// </summary>
		public int DefaultAgeMin
		{
			get
			{
				return _DefaultAgeMin;
			}
		}

		/// <summary>
		/// Default Max Age
		/// </summary>
		public int DefaultAgeMax
		{
			get
			{
				return _DefaultAgeMax;
			}
		}

		/// <summary>
		/// Default Search Radius
		/// </summary>
		public int DefaultSearchRadius
		{
			get
			{
				return _DefaultSearchRadius;
			}
		}

		/// <summary>
		/// PaymentTypeMask
		/// </summary>
		public int PaymentTypeMask
		{
			get
			{
				return _PaymentTypeMask;
			}
		}


		/// <summary>
		/// Default Search Type
		/// </summary>
		public int DefaultSearchTypeID
		{
			get
			{
				return _DefaultSearchTypeID;
			}
		}


		/// < summary>
		/// SSL URL
		/// </summary>
		public string SSLURL
		{
			get
			{
				return _SSLURL;
			}
		}


		/// <summary>
		/// Development Mode
		/// </summary>
		public bool ConfigLoaded
		{
			get
			{
				return _ConfigLoaded;
			}
		}


		/// <summary>
		/// Development Mode
		/// </summary>
		public bool DevelopmentMode
		{
			get
			{
				LoadConfig();

				return _DevelopmentMode;
			}
		}


		/// <summary>
		/// Secure Mode
		/// </summary>
		public SecureModes SecureMode
		{
			get
			{
				LoadConfig();

				return _SecureMode;
			}
		}

		/// <summary>
		/// Server Name
		/// </summary>
		public string ServerName
		{
			get
			{
				if (_ServerName == String.Empty)
				{
					_ServerName = SystemInformation.ComputerName;
				}
				return _ServerName;
			}
		}


		/// <summary>
		/// Host
		/// </summary>
		public string Host
		{
			get
			{
				if (_Host == null || _Host == "")
				{ //return the default host
					return _DefaultHost;
				}
				else
				{
					return _Host;
				}
			}
		}


		/// <summary>
		/// SSL Flag
		/// </summary>
		public bool SSLFlag
		{
			get
			{
				return _SSLFlag;
			}
			set
			{
				_SSLFlag = value;
			}
		}

		/// <summary>
		/// Site Description
		/// </summary>
		public string SiteDescription 
		{
			get
			{
				return _SiteDescription;
			}
		}


		public string BaseHttpsURL
		{
			get
			{
				if ( _BaseHttpsURL == null || _BaseHttpsURL.Length == 0)
				{
					if (SSLURL.Length > 0)
					{
						_BaseHttpsURL = "https://" + SSLURL;
					}
					else
					{
						HttpContext.Current.Response.Write("HostPrefix = " + HostPrefix() + "<BR>");
						_BaseHttpsURL = "https://" + HostPrefix() + _URI;
					}
				}
    
				return _BaseHttpsURL;
			}
		}


		public string CSSPath
		{
			get
			{
				return _CSSPath;
			}
		}


		public string PhoneNumber
		{
			get { return _PhoneNumber; }
		}


		public Cache Cache
		{
			get
			{
				return _InternalCache;
			}
		}
		

		public int ChatAppletVersion
		{
			get
			{
				return _ChatAppletVersion;
			}
		}


		public int ChatPort
		{
			get
			{
				return _ChatPort;
			}
		}


		public string ChatDNS
		{
			get
			{
				return _ChatDNS;
			}
		}


		public string ChatServer
		{
			get
			{
				return _ChatServer;
			}
		}


		public bool IsPaySite
		{
			get
			{
				return (((StatusType)_StatusMask & StatusType.PaySite) == StatusType.PaySite);
			}
		}

		
		public bool IsPayToReplySite
		{
			get
			{
				return (((StatusType)_StatusMask & StatusType.PayToReplySite) == StatusType.PayToReplySite);
			}
		}
		#endregion

		#region Public Methods

		/// <summary>
		/// Populates object via URL
		/// </summary>
		/// <param name="url">current URL</param>
		public void Load(string url)

		{
			string fullURI;
			string uri;
			string key;
    
			_URI = "";
			_ServerName = "";
			_Host = "";
    
			//extract the uri
			fullURI = GetFullURI(url);

			key = "PL:" + fullURI;

			if (CacheLoad(key) == false)
			{
				uri = fullURI;
				while (uri.Length > 0)
				{
					if (LoadByURIFromDb(uri))
					{
						break;
					}
					else
					{
						uri = TrimURI(uri);
					}
				}
				// If the private label is still undefined, load plid 1
				if (_PrivateLabelID == 0)
				{
					Load(DEFAULT_PRIVATELABEL);				
				}
				CacheSave(key);
			}
		}

		/// <summary>
		/// Populates object via Private Label ID
		/// </summary>
		/// <param name="privateLabelID">Private Label ID</param>
		public void Load(int privateLabelID) 
		{
			string key = "PLID:" + privateLabelID.ToString();

			if(CacheLoad(key) == false) 
			{
				LoadByIdFromDb(privateLabelID);
				SetUNCRoot();
				CacheSave(key);
			}
		}

		public void Load(int privateLabelID, bool sslFlag, string host, string uri)
		{
			string key = "SPLID:" + privateLabelID.ToString();

			_SSLFlag = sslFlag;
			
			if (host != null && uri != null)
			{
				_DefaultHost = host;
				_URI = uri;
			}

			if (CacheLoad(key) == false)
			{
				LoadByIdFromDb(privateLabelID);
				CacheSave(key);
			}
		}		

		public static int GetBasePrivateLabelID(int privateLabelID)
		{
			PrivateLabel privateLabel = new PrivateLabel();
			privateLabel.Load(privateLabelID);
			if (privateLabel.BasePrivateLabelID != Constants.NULL_INT)
			{
				return privateLabel.BasePrivateLabelID;
			}
			else
			{
				return privateLabelID;
			}
		}

		public void SetUrlInfo(string host, string uri)
		{
			_Host = host;
			_URI = uri;
		}

		#endregion

		#region Enums and Constants

		public enum SecureModes
		{
			Separated = 0,
			Local = 1
		}

		const string CACHE_DELIMITER = "<=>"; // needs to be acceptible by regexp
		const int CACHE_SECONDS = 3600; // cache for 1 hour
		const int PROPERTY_OWNER = 50;
		const int PROPERTY_NAME = 50;
		const string REGISTRY_KEY = @"SOFTWARE\Matchnet";
		const string KEY_DEVELOPMENTMODE = "DevelopmentMode";
		const string KEY_SECUREMODE = "SecureMode";
		const string DEVELOPMENT_MODE_ON = "1";
		const string HTTP_PREFIX = "http://";
		const string HTTPS_PREFIX = "https://";
		const int DEFAULT_PRIVATELABEL = 1;
		const string PRIVATELABEL_CACHEKEY_PREFIX = "PrivateLabel:";

		[Flags]
		public enum StatusType
		{
			PaySite = 1,
			PayToReplySite = 8
	}


		#endregion

		#region Member Variables

		private Cache _InternalCache;
		private int _PrivateLabelID;
		private int _DomainID;
		private int _DefaultCurrencyID;
		private string _URI;
		private int _LanguageID;
		private int _StatusMask;
		private int _LCID;
		private int _DefaultRegionID;
		private bool _Disabled;
		private string _Host;
		private string _CSSPath;
		private string _CharSet;
		private string _RAlign;
		private string _LAlign;
		private string _Direction;
		private string _ColorLightest;
		private string _ColorLighter;
		private string _ColorLight;
		private string _ColorMedium;
		private string _ColorDark;
		private string _ColorDarker;
		private string _ColorDarkest;
		private string _BodyProperties;
		private string _ImgURL;
		private int _RegistrationStepCount;
		private int _DefaultAgeMin;
		private int _DefaultAgeMax;
		private int _DefaultSearchRadius;
		private int _PaymentTypeMask;
		private double _GMTOffset;
		private int _TranslationID;
		private int _BasePrivateLabelID;
		private bool _RedirectToBase;
		private string _DefaultHost;
		private string _SSLURL;
		private int _DefaultSearchTypeID;
		private string _ChatDNS;
		private string _ChatServer;
		private int _ChatPort;
		private int _ChatAppletVersion;
		private int _CheckIMVersion;
		private int _SearchTypeMask;
		private string _ServerName = String.Empty;
		private bool _SSLFlag;
		private string _ImageRoot;
		private string _SiteDescription;
		private string _BaseHttpsURL;
		private bool _ConfigLoaded = false;
		private SecureModes _SecureMode;
		private bool _DevelopmentMode;
		private string _PhoneNumber;
		static private ReaderWriterLock _CacheLock = new ReaderWriterLock();

		#endregion

		#region Private Methods

		private void SetProperties(DataTable table)
		{
			_PrivateLabelID = Convert.ToInt32(table.Rows[0]["PrivateLabelID"]);
			_DomainID = Convert.ToInt32(table.Rows[0]["DomainID"]);
			_DefaultCurrencyID = Convert.ToInt32(table.Rows[0]["DefaultCurrencyID"]);
			_URI = table.Rows[0]["URI"].ToString();
			_SiteDescription = table.Rows[0]["Description"].ToString();
			_LanguageID = Convert.ToInt32(table.Rows[0]["LanguageID"]);
			_StatusMask = Convert.ToInt32(table.Rows[0]["StatusMask"]); 
			_LCID = Convert.ToInt32(table.Rows[0]["LCID"]);
			_DefaultRegionID = Convert.ToInt32(table.Rows[0]["DefaultRegionID"]);
			_Disabled = Convert.ToBoolean(table.Rows[0]["DisableFlag"]);
			_CSSPath = table.Rows[0]["CSSPath"].ToString();
			_CharSet = table.Rows[0]["CharSet"].ToString();	 
			_RAlign = table.Rows[0]["RAlign"].ToString();
			_LAlign = table.Rows[0]["LAlign"].ToString();
			_Direction = table.Rows[0]["Direction"].ToString();	 
			_ColorLightest = table.Rows[0]["ColorLightest"].ToString();
			_ColorLighter = table.Rows[0]["ColorLighter"].ToString();
			_ColorLight = table.Rows[0]["ColorLight"].ToString();
			_ColorMedium = table.Rows[0]["ColorMedium"].ToString();
			_ColorDark = table.Rows[0]["ColorDark"].ToString();
			_ColorDarker = table.Rows[0]["ColorDarker"].ToString();
			_ColorDarkest = table.Rows[0]["ColorDarkest"].ToString();
			_BodyProperties = table.Rows[0]["BodyProperties"].ToString();
			_RegistrationStepCount = Convert.ToInt32(table.Rows[0]["RegistrationStepCount"]);
			_DefaultAgeMin = Convert.ToInt32(table.Rows[0]["DefaultAgeMin"]);
			_DefaultAgeMax = Convert.ToInt32(table.Rows[0]["DefaultAgeMax"]);
			_DefaultSearchRadius = Convert.ToInt32(table.Rows[0]["DefaultSearchRadius"]);
			_PaymentTypeMask = Convert.ToInt32(table.Rows[0]["PaymentTypeMask"]);
			_ChatDNS = table.Rows[0]["ChatDNS"].ToString();
			_ChatServer = table.Rows[0]["ChatServer"].ToString();
			_ChatPort = Convert.ToInt32(table.Rows[0]["ChatPort"]);
			_ChatAppletVersion = Convert.ToInt32(table.Rows[0]["ChatAppletVersion"]);
			_CheckIMVersion = Convert.ToInt32(table.Rows[0]["CheckIMVersion"]);
			_GMTOffset = Convert.ToDouble(table.Rows[0]["GMTOffset"]);
			_TranslationID = Convert.ToInt32(table.Rows[0]["TranslationID"]);
			_PhoneNumber = table.Rows[0]["PhoneNumber"].ToString();
			if (table.Rows[0]["BasePrivateLabelID"] != DBNull.Value)
			{
				_BasePrivateLabelID = Convert.ToInt32(table.Rows[0]["BasePrivateLabelID"]);
			}
			else
			{
				_BasePrivateLabelID = Constants.NULL_INT;
			}
			_RedirectToBase = Convert.ToBoolean(table.Rows[0]["RedirectToBase"]);
			_DefaultHost = table.Rows[0]["DefaultHost"].ToString();	
			_SSLURL = table.Rows[0]["SSLURL"].ToString();
			
			// This will conditionally modify urls for local secure mode
			LocalModeModifyURLS();
			
			_DefaultSearchTypeID = Convert.ToInt32(table.Rows[0]["DefaultSearchTypeID"]);
			_SearchTypeMask = Convert.ToInt32(table.Rows[0]["SearchTypeMask"]);

			_ImgURL = table.Rows[0]["ImgURL"].ToString();
			if(_ImgURL.EndsWith("/")) 
			{
				_ImgURL = _ImgURL.Substring(0, _ImgURL.Length - 1);
			}

			if(_ImgURL.StartsWith("/") == false) 
			{
				_ImgURL = "/" + _ImgURL;
			}
		}


		private void LocalModeModifyURLS()
		{
			if ( SecureMode != SecureModes.Local )
				return;

			if ( HttpContext.Current != null )
			{
				string host = HttpContext.Current.Request.Url.Host;

				// this only affects local mode ssl - admin, staging
				// is it an ip address? then convert to hostname
				Regex reg = new Regex(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
				MatchCollection match = reg.Matches(host);
				if (match.Count > 0)
				{
					IPHostEntry entry = Dns.GetHostByAddress(host);
					string [] aliases = entry.Aliases;
					if (aliases.Length > 0)
					{
						// take the first alias
						host = aliases[0].ToString();
					}
					else
					{
						// no alias defined, take the default hostname
						host = entry.HostName;
					}

				}

				int dotpos = host.IndexOf(".");
				if ( dotpos != -1 )
				{
					_SSLURL = host;
					_DefaultHost = host.Substring(0, dotpos);

					// quick hack workaround
					// server keeps resolving its netbios name (WEBADMIN01.matchnet.com)
					// when it's hit by health check
					if (host.ToLower().IndexOf("admin") != -1)
					{
						_SSLURL = "admin.americansingles.com";
						_DefaultHost = "admin";
					}
				}
			}
		}

		private void SetUNCRoot()
		{
			// ToDo: Brent Wood - GetImageUNCRoot returns an ImageRoot object
			//_ImageRoot = Matchnet.Content.ServiceAdapters.ImageSA.Instance.GetImageUNCRoot();
			if(_ImageRoot.EndsWith(@"\")) 
			{
				_ImageRoot = _ImageRoot.Substring(0, _ImageRoot.Length - 1);
			}
		}

		private string TrimURI(string uri)
		{
			string result = "";
			int dotPos;

			dotPos = uri.IndexOf(".");

			if (dotPos > 0 && dotPos < uri.Length)
			{
				if (_Host.Length > 0)
				{
					_Host = _Host + ".";
				}

				if (dotPos > 1)
				{
					_Host = _Host + uri.Substring(0, dotPos);
				}

				result = uri.Substring(dotPos + 1);
			}
			else
			{
				// If no dot, the whole this is the host
				_Host = uri;
			}

			return result;
		}


		private void CacheSave(string key) 
		{
			try
			{
				_CacheLock.AcquireWriterLock(1000);

				PrivateLabel privateLabel = (PrivateLabel)this.MemberwiseClone();

				Cache.Insert(key, privateLabel, null, DateTime.Now.AddSeconds(CACHE_SECONDS), 
					TimeSpan.Zero,	
					CacheItemPriority.High, null);
			}
			finally
			{
				_CacheLock.ReleaseWriterLock();
			}
		}


		private bool CacheLoad(string key) 
		{
			bool success = false;

			PrivateLabel val;

			try
			{
				_CacheLock.AcquireReaderLock(1000);

				val = (PrivateLabel)Cache[key];

				if(val != null) 
				{
					_BasePrivateLabelID = val.BasePrivateLabelID;
					_DomainID = val.DomainID;
					_DefaultCurrencyID = val.DefaultCurrencyID;
					_URI = val.URI;
					_SiteDescription = val.SiteDescription;
					_LanguageID = val.LanguageID;
					_StatusMask = val.StatusMask;
					_LCID = val.LCID;
					_DefaultRegionID = val.DefaultRegionID;
					//_Disabled = val._Disabled;
					_CSSPath = val.CSSPath;
					_CharSet = val.CharSet;
					_RAlign = val.RAlign;
					_LAlign = val.LAlign;
					_Direction = val.Direction;
					_ColorLightest = val.ColorLightest;
					_ColorLighter = val.ColorLighter;
					_ColorLight = val.ColorLight;
					_ColorMedium = val.ColorMedium;
					_ColorDark = val.ColorDark;
					_ColorDarker = val.ColorDarker;
					_ColorDarkest = val.ColorDarkest;
					//_BodyProperties = val._BodyProperties;
					_RegistrationStepCount = val.RegistrationStepCount;
					_DefaultAgeMin = val.DefaultAgeMin;
					_DefaultAgeMax = val.DefaultAgeMax;
					_DefaultSearchRadius = val.DefaultSearchRadius;
					_PaymentTypeMask = val.PaymentTypeMask;
					_ChatDNS = val._ChatDNS;
					_ChatServer = val._ChatServer;
					_ChatPort = val._ChatPort;
					_ChatAppletVersion = val._ChatAppletVersion;
					_CheckIMVersion = val._CheckIMVersion;
					_GMTOffset = val.GMTOffset;
					_TranslationID = val.TranslationID;
					_RedirectToBase = val.RedirectToBase;
					_DefaultHost = val.DefaultHost;
					_SSLURL = val.SSLURL;
					_DevelopmentMode = val.DevelopmentMode;
					_SecureMode = val.SecureMode;
					_ConfigLoaded = val.ConfigLoaded;
					_Host = val.Host;
					_ImgURL = val.ImgURL;
					_PrivateLabelID = val.PrivateLabelID;
					_SearchTypeMask = val.SearchTypeMask;
					_ServerName = val.ServerName;
					_DefaultSearchTypeID = val.DefaultSearchTypeID;
					_ImageRoot = val.ImageRoot;
					_PhoneNumber = val.PhoneNumber;

					success = true;
				}
			}
			finally
			{
				_CacheLock.ReleaseReaderLock();
			}

			return success;
		}


		private string HostPrefix()
		{
			string result = "";

			if (_Host.Length > 0)
			{
				result = _Host + ".";
			}
			else if (_DefaultHost.Length > 0)
			{
				result = _DefaultHost + ".";
			}

			return result;
		}
			
		private string GetFullURI(string url)
		{
			Uri uri = new Uri(url);

			return uri.Host;
		}

		//	TODO - Note that this won't go away until we just remove PrivateLabel
		//	entirely.  This function hudrates the PrivateLabel object so it doesn't
		//	make sense to port if it is just about to go away - dcornell
		private void LoadByIdFromDb(int privateLabelID) 
		{
			//	TODO: Remove SQL 
			Matchnet.DataTemp.SQLDescriptor conf = new Matchnet.DataTemp.SQLDescriptor("mnShared", 0);
			Matchnet.DataTemp.SQLClient client = new Matchnet.DataTemp.SQLClient(conf);
			client.AddParameter("@PrivateLabelID", SqlDbType.Int,ParameterDirection.Input,privateLabelID);
			DataTable table = client.GetDataTable("up_PrivateLabel_SelectByID", CommandType.StoredProcedure);
			if (table.Rows.Count > 0) 
			{
				SetProperties(table);
				SetUNCRoot();
			}
			SetUNCRoot();
		}


		//	TODO - Note that this won't go away until we just remove PrivateLabel
		//	entirely.  This function hudrates the PrivateLabel object so it doesn't
		//	make sense to port if it is just about to go away - dcornell
		private bool LoadByURIFromDb(string uri)
		{
			Matchnet.DataTemp.SQLDescriptor conf = new Matchnet.DataTemp.SQLDescriptor("mnShared", 0);
			Matchnet.DataTemp.SQLClient client = new Matchnet.DataTemp.SQLClient(conf);
			//TODO: Remove SQL 
			client.AddParameter("@URI", SqlDbType.VarChar, ParameterDirection.Input, uri);
			DataTable table = client.GetDataTable("up_PrivateLabel_Select", CommandType.StoredProcedure);
			if (table.Rows.Count > 0) 
			{
				SetProperties(table);
				SetUNCRoot();
				return true;
			}

			return false;
		}

		private void LoadConfig()
		{
			if ( _ConfigLoaded )
				return;

			try
			{
				if ( ConfigurationSettings.AppSettings.Get(KEY_DEVELOPMENTMODE) != null )
				{
					_DevelopmentMode = 
						Convert.ToBoolean(ConfigurationSettings.AppSettings.Get(KEY_DEVELOPMENTMODE));
				}

				string secureModeText = ConfigurationSettings.AppSettings.Get(KEY_SECUREMODE);
				if ( secureModeText != null )
				{
					if ( secureModeText.ToLower() == "local" )
					{
						_SecureMode = SecureModes.Local;
					}
					else
					{
						_SecureMode = SecureModes.Separated;
					}
				}
				
				_ConfigLoaded = true;
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex);
			}
		}


		#endregion
	}
}
