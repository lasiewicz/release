using System;
using System.Data;
using System.IO;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;

namespace Matchnet.Labels
{
	/// <summary>
	/// Summary description for PrivateLabelUtils.
	/// </summary>
	public class PrivateLabelUtils
	{
		private PrivateLabelUtils()
		{

		}


		/// <summary>
		/// retreives file web path
		/// </summary>
		/// <param name="filename">file name</param>
		/// <param name="applicationID">application ID</param>
		/// <param name="fullyQualified">relative or fully qualified path</param>
		/// <returns>file web path</returns>
		public static string WebSrc(string filename, int applicationID, bool fullyQualified, Brand brand) 
		{
			string path = string.Empty;
			string imageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL");

			path = FileSrc(filename, applicationID, brand);
			if(path.Length > 0) 
			{
				path = path.Replace(@"\", "/");
				if(fullyQualified) 
				{
					// BaseHttpUrl = Brand.Site.DefaultHost + Brand.URI
					if(imageUrl.IndexOf("http://") == -1) 
					{
						// path = BaseHttpUrl + path;
						path = brand.Site.DefaultHost + brand.Uri;
					} 
				}
			}
				
			return(path);
		}

		public static string WebSrc(string filename, int applicationID, bool fullyQualified, int brandID)
		{
			Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
			return(WebSrc(filename, applicationID, fullyQualified, brand));
		}

		public static string RetrieveRegionString(int regionID, int languageID)
		{
			string retVal = "N/A";

			RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);
			retVal = regionLanguage.CityName + ", " + regionLanguage.StateDescription;

			return(retVal);
		}

		private static string FileSrc(string filename, int applicationID, Brand brand) 
		{
			string imageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL");
			string imageRoot = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_ROOT");

			string path = "";
			bool found = false;

			DataTable table = ImageSA.Instance.RetrieveImageDataTable(filename, applicationID, brand.BrandID, brand.Site.SiteID, brand.Site.LanguageID, brand.Site.Community.CommunityID);

			if (table.Rows.Count > 0)
			{
				int intPrivateLabelID = 0;
				int intLanguageID = 0;
				int intDomainID = 0;
				int intApplicationID = applicationID;
				string strName = filename;

				intPrivateLabelID = Convert.ToInt32(table.Rows[0]["PrivateLabelID"].ToString());
				intLanguageID = Convert.ToInt32(table.Rows[0]["LanguageID"].ToString());
				intDomainID = Convert.ToInt32(table.Rows[0]["DomainID"].ToString());
				intApplicationID = Convert.ToInt32(table.Rows[0]["ApplicationID"].ToString());
				strName = table.Rows[0]["Name"].ToString();


				// get the base path for a brand, locale, or domain
				if (intPrivateLabelID > 0) 
				{
					path = @"/p/" + intPrivateLabelID;
				} 
				else if (intLanguageID > 0) 
				{
					path = @"/l/" + intLanguageID;
				}
				else if (intDomainID > 0) 
				{
					path = @"/d/" + intDomainID;
				}
                       
				// Some images don't have application IDs
				if (intApplicationID > 0) 
				{
					path = path + @"/" + intApplicationID;
				}

				// Append the File Name
				path = imageUrl + path + @"/" + strName;

			}
			else // scan the drive, but _ImageRoot is \\dv-web1 when it should be local
			{
				string[] delims = new string[] {"p", "d", "l"};
				string[] vals = new string[] { brand.BrandID.ToString(), brand.Site.Community.CommunityID.ToString(), brand.Site.LanguageID.ToString() };
				for(int i = 0; i < delims.Length; i++) 
				{
					path = imageRoot + @"\" + delims[i] + @"\" + vals[i] + @"\" + filename;
					if(File.Exists(path)) 
					{
						found = true;
						break;
					}

					if(applicationID != Constants.NULL_INT) 
					{
						path = imageRoot + @"\" + delims[i] + @"\" + vals[i] + @"\" + applicationID.ToString() + @"\" + filename;
						if(File.Exists(path)) 
						{
							found = true;
							break;
						}
					}
				}

				if(found == false) 
				{
					if(applicationID != Constants.NULL_INT) 
					{
						path = @"\" + applicationID.ToString() + @"\" + filename;
						found = File.Exists(imageRoot + path);
					}
				
					if(found == false) 
					{
						path = @"\" + filename;
						found = File.Exists(imageRoot + path);
					}
				}
				if(found == false) 
				{
					path = "";
				}
				else
				{
					path = path.Replace(imageRoot,"");
					path = path.Replace(@"\",@"/");
					path = imageUrl + path;
				}
			}
			return path;
			
		}
		
		public static string GetDomainAbbreviation(int domainID)
		{
			string domainAbbreviation = "";

			switch (domainID)
			{
				case (int)Domains.DOMAIN_MND:
					domainAbbreviation = "as";
					break;
				case (int)Domains.DOMAIN_SS:
					domainAbbreviation = "gy";
					break;
				case (int)Domains.DOMAIN_JEW:
					domainAbbreviation = "jd";
					break;
				case (int)Domains.DOMAIN_MNC:
					domainAbbreviation = "mn";
					break;
				case (int)Domains.DOMAIN_PA:
					domainAbbreviation = "fl";
					break;
				case (int)Domains.DOMAIN_COL:
					domainAbbreviation = "cl";
					break;
				case (int)Domains.DOMAIN_CI:
					domainAbbreviation = "ci";
					break;
			}

			return domainAbbreviation;
		}

		public enum Domains : int
		{
			DOMAIN_MND = 1,
			DOMAIN_SS = 2,
			DOMAIN_JEW = 3,
			DOMAIN_MNC = 8,
			DOMAIN_PA = 9,
			DOMAIN_CI = 10,
			DOMAIN_COL = 12
		};

	}
}
