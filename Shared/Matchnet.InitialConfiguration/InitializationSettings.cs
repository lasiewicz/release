using System;
using System.Xml;
using System.Collections.Specialized;

using Matchnet;


namespace Matchnet.InitialConfiguration
{
	/// <summary>
	/// Provides access to configuration settings. This class cannot be inherited.  
	/// </summary>
	public sealed class InitializationSettings
	{
		private static System.DateTime _lastLoaded = System.DateTime.Now.Subtract(new TimeSpan(1, 0, 0));
		private static NameValueCollection _cachedNameValueCollection = new NameValueCollection();
		private static System.TimeSpan _ttl = new TimeSpan(0, 1, 0); // 1-minute timeout

		private const string NS = "/";
		private const string SE = "";
		private const string ATT_KEY = "key";
		private const string ATT_VAL = "value";

		private InitializationSettings()
		{
			// Hide the default constructor
		}

		public static string MachineName
		{
			get
			{
				string machineName = System.Environment.MachineName;

				string overrideMachineName = Get("configuration/settings/MachineNameOverride");

				if(overrideMachineName != null)
				{
					machineName = overrideMachineName;
				}

				return machineName;
			}
		}

		/// <summary>
		/// Gets the values associated with the specified key from the System.Collections.Specialized.NameValueCollection combined into one comma-separated list.
		/// </summary>
		/// <param name="name">The System.String key of the entry that contains the values to get. The key can be null.  </param>
		/// <returns>A System.String that contains a comma-separated list of the values associated with the specified key from the System.Collections.Specialized.NameValueCollection, if found; otherwise, null.  </returns>
		public static string Get(string name)
		{
			return GetAll().Get(name);
		}

		/// <summary>
		/// Gets the System.Collections.Specialized.NameValueCollection representing the system's configuration name/value pairs.
		/// </summary>
		/// <returns>A System.Collections.Specialized.NameValueCollection that contains the configuration name/values</returns>
		public static NameValueCollection GetAll() 
		{
			

			if(System.DateTime.Now.Subtract(_ttl) < _lastLoaded)
			{
				return _cachedNameValueCollection;
			}


			string configurationUri = @"C:\MatchNet\System.config";


			int level = -1;

			string name = SE;
			
			NameValueCollection nameValueCollection = new NameValueCollection();
			
			// Using XmlTextReader because it provides very fast, forward only, non-cached access
			XmlTextReader xmlTextReader = null;

			try
			{
				xmlTextReader = new XmlTextReader(configurationUri);

				while(xmlTextReader.Read()) 
				{
					if(xmlTextReader.NodeType == XmlNodeType.Element)
					{
						if(xmlTextReader.IsEmptyElement)
						{
							if(level > xmlTextReader.Depth)
							{
								name = name.Substring(0, name.LastIndexOf(NS));
								level = xmlTextReader.Depth;
							}
							else
								if(level == xmlTextReader.Depth)
							{
								name = name.Substring(0, name.LastIndexOf(NS));
								level = xmlTextReader.Depth-1;
							}
						}
						else
						{
							if(level < xmlTextReader.Depth)
							{
								name += (name.Length>0?NS:SE) + xmlTextReader.Name;
								level = xmlTextReader.Depth;
							}
							else
								if(level > xmlTextReader.Depth)
							{
								name = name.Substring(0, name.LastIndexOf(NS));
								level = xmlTextReader.Depth;
							}
							else
							{
								name=name.Substring(0, name.LastIndexOf(NS)+1) + xmlTextReader.Name;
							}
						}
						if(xmlTextReader.HasAttributes)
						{
							string key=xmlTextReader.GetAttribute(ATT_KEY);
							string val=xmlTextReader.GetAttribute(ATT_VAL);
						
							if(key != null && val != null)
							{
								nameValueCollection[name + NS + key] = val;
							}
						}
					}
					else

						if(xmlTextReader.NodeType == XmlNodeType.EndElement)
					{
						if(level > xmlTextReader.Depth)
						{
							name = name.Substring(0, name.LastIndexOf(NS));
							level = xmlTextReader.Depth;
						}
					}
				}
				
				// Only update the cached NameValueCollection if the new collection has at least one item in it
				if(nameValueCollection.Count > 0)
				{
					// Update the last loaded time-stamp
					_lastLoaded = System.DateTime.Now;

					// Update the cached item
					_cachedNameValueCollection = nameValueCollection;
				}
			}
			catch(Exception ex)
			{
				throw(new Exception("Error occurred when attempting to retrieve Initialization Settings values", ex));
			}
			finally
			{
				// Finally, make sure that the file is closed
				if(xmlTextReader != null)
				{
					xmlTextReader.Close();
				}
			}

			return nameValueCollection;
		}

	}
}
