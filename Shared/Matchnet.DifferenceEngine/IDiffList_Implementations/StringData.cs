using System;
using System.Collections;

namespace Matchnet.DifferenceEngine
{
	/// <summary>
	/// Summary description for StringData.
	/// </summary>
	public class DiffList_StringData : IDiffList
	{
		private ArrayList _words;

		public DiffList_StringData(string entireString)
		{
            _words = new ArrayList();
			
			// Split by space and plug the space back in            
			string[] wordsNoSpace = entireString.Split(' ');
			for(int i=0; i < wordsNoSpace.Length; i++)
			{
				_words.Add(wordsNoSpace[i] + " ");
			}
		}

		#region IDiffList Members
		public int Count()
		{
			return _words.Count;
		}

		public IComparable GetByIndex(int index)
		{
			return (string)_words[index];
		}
		#endregion
	}
}
