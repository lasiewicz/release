﻿using System.Collections.Generic;
using Hammock.Web;
using Spark.Common.RestConsumer.Models.Session;

namespace Spark.Common.RestConsumer.Consumers
{
    public class SessionConsumer : RestConsumerBase
    {
        public static readonly SessionConsumer Instance = new SessionConsumer();

        private SessionConsumer()
        {
			var methodResourceDict = new Dictionary<WebMethod, string>();
			
			var resource = "/{__GROUP}/{key}";
			methodResourceDict.Add(WebMethod.Get, resource );

			resource = "/{__GROUP}/save";
			methodResourceDict.Add(WebMethod.Post, resource);
			RequestMap.Add(typeof(Session), methodResourceDict);

			methodResourceDict = new Dictionary<WebMethod, string>();
			
			resource = "/{__GROUP}/create";
			methodResourceDict.Add(WebMethod.Post, resource);
			RequestMap.Add(typeof(Create), methodResourceDict);
        }
        	
        protected override string ResourceGroup { get { return "session"; }} 

    }
}
