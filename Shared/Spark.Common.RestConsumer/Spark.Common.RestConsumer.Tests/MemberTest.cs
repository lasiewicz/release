﻿using System;
using System.Configuration;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.Member;


namespace Spark.Common.RestConsumer.Tests
{
    
    
    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MemberTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private static OAuthConsumer OAuthConsumer;
        const int TestUserId1 = 16000206; // Won
        const int TestUserId2 = 100068787;
        const int AppicationId = 1054;

        const string User1Email = "wlee@spark.net";
        private const string User1Password = "1234";

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            var tokens = OAuthHelper.GetTokensUsingPassword(AppicationId, User1Email, User1Password);
            OAuthConsumer = new OAuthConsumer(tokens.MemberId, tokens.AccessToken, DateTime.Now.AddSeconds(tokens.ExpiresIn));
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion
        [TestMethod]
        public void VerifyAppDomainHasConfigurationSettings()
        {
            string value = ConfigurationManager.AppSettings["RESTAuthority"];
            Assert.IsFalse(String.IsNullOrEmpty(value), "No App.Config found.");
        }

        [TestMethod()]
        public void Register()
        {
            var random = new Random();

            var register = new Register
                             {
                                 //BrandUri = "jdate.com",
                                 EmailAddress = "mtest" + random.Next() + @"@spark.net",
                                 IpAddress = "127.0.0.1",
                                 Password = "1234",
                                 UserName = "mobiletest"
                             };

            var registerResult = RestConsumer.Instance.Post<RegisterResult, Register>(null, register); // needs urlparams?

            Assert.IsNotNull(registerResult.MemberId);
        }
	
        [TestMethod()]
        public void CompleteRegistrationWithIovation()
        {
            var urlParams = new Dictionary<string, string> { { "memberId", TestUserId1.ToString()} };
            var request = new CompleteRegistrationWithIovationRequest
                              {
                                  IovationBlackBox =
                                      "0400AY1YRP7DGIANf94lis1ztr/gbhPG7zZysBN4Jl1FfOP86ff28/AqMEd5g7N7rJgakDfkJ/qq0kOGXPnCVEXBYmLQVG6Wu5dBAYJBomRdTsao/UtZgaQkYhcQLmVWBXxAh0ylJGfM1m6rVjpSL/tvtXAEKkoBeycPgriaMoAypQuyPr0t8ztVFo5lNNXikUMEH7dgb4Vwqm+X77WlQ9Tc8C/Scu/+OwM7mBZI7b40DhlWwwMxWYcdNXydOz/d7dMjcD6T1dGUjL+J2wJLpMHwVO0As1d5i7mXGtWfmRfeD5Gnxp7ErVwiPxXCc5mKLfi7X9fToP5ZBNjED3HepEpuM0dnBscrd49O3eln9a65pRDCPw/wpHHNoUtezWwdrLOsKoxk0tBb1QV8qjUOoCPake9RJsn4u3PdOCpHq9Ol2DGuNa8QpcydozSIZtzxuwNU1eHfkZ0HoJzSt0oGtrebhlfmg5DSNu61cw8O374N/SeE71NkudcBsrXbUlKdexkPRh35hSsrB0AKSs4KVYKB4pz4N6xZOeqI+snP9+SWhwzDNd2U7bm7tY5lNi+covQlmyte4ph3LRrEqYqLQTmZsqyPcHPZW84H1ciS6DmKrDTrIWh2D/DqupPnFVyYldsigJnjRx+vsOqdM4U/jELhhRRmXbVxMvj5rK9FtM2mNTBqJT8q9BHcLOQMSEmnBh/gg5uHDmIFV+ZvLx/VuIg7lqwzUcHMyAa2v3uvdY9JaQlauODchk2WrH2LOmVQKLUdsrEanxGrWLvIbKQO2dCss9DXR9eqakvDIi7SoUevORpqCVeow06JdhoVXmmGKWcVWn+dCWj3wlGikkRFw8QaAC8f4xvXaasSJLaRDKcyskJ78qr5WU7pB8ZxTmF+/qbk"
                              };

            var result =
                OAuthConsumer.Post<CompleteRegistrationWithIovationResult, CompleteRegistrationWithIovationRequest>(
                    urlParams, request);

            Debug.WriteLine(String.Format("MemberId:{0} Status:{1}", result.MemberId, result.Status));

            Assert.IsNotNull(result);
        }
    }
}
