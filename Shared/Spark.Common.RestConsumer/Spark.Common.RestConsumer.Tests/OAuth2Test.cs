﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.Common.RestConsumer.Models.OAuth2;
using Spark.Common.RestConsumer.Models.Profile;

namespace Spark.Common.RestConsumer.Tests
{

    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class OAuth2Test
    {

        private const int devAppId = 1054;
        const string devAppSecret = "nZGVVfj8dfaBPKsx_dmcRXQml8o5N-iivf5lBkrAmLQ1";


        private static string refreshToken = null;
        private static string accessToken = null;

        // mobiletest101 100170050
        // mobiletest102 111644211
        // mobiletest103 100170023
        // mobiletest104 111644214
        // mobiletest105 108009966

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

		private static void AssertGoodTokens(int memberId, OAuthTokens tokens)
		{
			Assert.IsNotNull(tokens, "REST returned null token object");
			Assert.IsTrue(String.IsNullOrEmpty(tokens.Error), "error returned from REST: " + tokens.Error);
			Assert.IsFalse(String.IsNullOrEmpty(tokens.AccessToken), "null access token");
			accessToken = tokens.AccessToken;
			Assert.IsFalse(String.IsNullOrEmpty(tokens.RefreshToken), "null refresh token");
			Assert.IsTrue(tokens.ExpiresIn > 30, "tokens expiring too soon");
			Assert.AreEqual(memberId, tokens.MemberId, "member id mismatch");
			Assert.AreNotEqual(tokens.RefreshTokenExpiresTime, new DateTime(), "missing refresh token expiration time");
			Assert.IsTrue(tokens.RefreshTokenExpiresTime > DateTime.Now, "refresh token is expired");
			var sixtyFiveDAysAway = DateTime.Now + new TimeSpan(65, 0, 0, 0);
			Assert.IsTrue(tokens.RefreshTokenExpiresTime < sixtyFiveDAysAway, "refresh token expiration is too far out (over 65 days)");
		}

        [TestMethod]
        public void GetTokenUsingPasswordTest()
        {
            //const ushort applicationId = 10000; // this app must be created first, use applicationTest if needed
            const string email = "wlee@spark.net";
            const string password = "1234";
            const int memberId = 16000206; // Won
            //const int memberId = 27029711; // won prod
            //var tokens = OAuthHelper.GetAccessTokenPassword(applicationId, email, password);
            var urlParams = new Dictionary<string, string> {{"applicationId", devAppId.ToString()}};
            var qsPArarms = new Dictionary<string, string> { { "client_secret", devAppSecret } };
            var tokens = RestConsumer.Instance.Post<OAuthTokens, TokenRequestPassword>(urlParams, new TokenRequestPassword { Email = email, Password = password }, qsPArarms);
			AssertGoodTokens(memberId, tokens);
			refreshToken = tokens.RefreshToken;
        }

        /// <summary>
        /// depends on previous test for refresh token
        /// </summary>
        [TestMethod]
        public void GetTokenUsingRefreshTest()
        {
            Assert.IsNotNull(refreshToken, "run previous test to create refresh token");
            const ushort applicationId = devAppId; 
            const int memberId = 16000206; // Won dev
            //const int memberId = 27029711; // Won prod

            var urlParams = new Dictionary<string, string> { { "applicationId", applicationId.ToString() } };
            var qsPArarms = new Dictionary<string, string> { { "client_secret", devAppSecret } };
            var tokens = RestConsumer.Instance.Post<OAuthTokens, TokenRefreshRequest>(urlParams, new TokenRefreshRequest { MemberId = memberId, RefreshToken = refreshToken }, qsPArarms);
			AssertGoodTokens(memberId, tokens);
		}

        [TestMethod]
        public void TryGetTokenUsingPasswordAndBadSecretTest()
        {
            //const ushort applicationId = 10000; // this app must be created first, use applicationTest if needed
            const string email = "wlee@spark.net";
            const string password = "1234";
            const int memberId = 16000206; // Won
            //const int memberId = 27029711; // won prod
            //var tokens = OAuthHelper.GetAccessTokenPassword(applicationId, email, password);
            var urlParams = new Dictionary<string, string> { { "applicationId", devAppId.ToString(CultureInfo.InvariantCulture) } };
            var qsPArarms = new Dictionary<string, string> { { "client_secret", devAppSecret + "THIS_SECRET_IS_BAD"} };
            var tokens = RestConsumer.Instance.Post<OAuthTokens, TokenRequestPassword>(urlParams, new TokenRequestPassword { Email = email, Password = password }, qsPArarms);
            Assert.IsNotNull(tokens, "REST returned null token object");
            Assert.IsFalse(String.IsNullOrEmpty(tokens.Error), "REST should have returned an error");
            Assert.IsFalse(String.IsNullOrEmpty(tokens.AccessToken), "null access token");
            accessToken = tokens.AccessToken;
            Assert.IsFalse(String.IsNullOrEmpty(tokens.RefreshToken), "null refresh token");
            Assert.IsTrue(tokens.ExpiresIn > 30, "tokens expiring too soon");
            Assert.AreEqual(memberId, tokens.MemberId, "member id mismatch");
            refreshToken = tokens.RefreshToken;
        }

    }
}
