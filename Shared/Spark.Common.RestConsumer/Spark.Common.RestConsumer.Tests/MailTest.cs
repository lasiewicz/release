﻿using System;
using System.Configuration;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.Mail;

namespace Spark.Common.RestConsumer.Tests
{
    
    
    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MailTest
    {


		private const string brandHost = "jdate.com";
		//const int testUserId1 = 100170050;
		const int testUserId1 = 16000206; // Won
		private const int testUserId2 = 100068787;
		private const string testUserEmail = "mmishkoff@spark.net";
		private const string testUserPassword = "1234";
		private const int appicationId = 1054;


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }


        private static OAuthConsumer OAuthConsumer;
        const int TestUserId1 = 16000206; // Won
        const int TestUserId2 = 100068787;
        const int AppicationId = 1054;

        const string User1Email = "wlee@spark.net";
        private const string User1Password = "1234";

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            var tokens = OAuthHelper.GetTokensUsingPassword(AppicationId, User1Email, User1Password);
            OAuthConsumer = new OAuthConsumer(tokens.MemberId, tokens.AccessToken, DateTime.Now.AddSeconds(tokens.ExpiresIn));
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for Get Inbox
        ///</summary>
        [TestMethod()]
        public void GetMailInboxFolderTest()
        {
			const int expectedCount = 7;
			var urlParams = new Dictionary<string, string> { { "folderId", "1" }, { "pagesize", "7" }, { "page", "1" } };
            var inbox = OAuthConsumer.Get<MailFolder>(urlParams);

            var actualCount = inbox.MessageList.Count;
            Assert.AreEqual(expectedCount, actualCount);
        }

		/// <summary>
		///A test for Get Inbox
		///</summary>
		[TestMethod]
		public void GetMailMessageTest()
		{
            const int messageId = 608481687;
			const string expectedBody = "now vip message to won";
			var urlParams = new Dictionary<string, string> { { "messageId", messageId.ToString() } };

			var actual = OAuthConsumer.Get<MailMessage>(urlParams);
			var actualBody = actual.Body;
			Assert.AreEqual(messageId, actual.Id, "message id");
			Assert.AreEqual(expectedBody, actualBody, "message bodies didn't match");
		}

		[TestMethod]
		public void SendMailMessage()
		{
			const int recipientId = testUserId2;

			var newMessage = new NewMessage
			                 	{
			                 		Subject = "Unit Test Message",
			                 		Body = "hello this is a unit test message",
			                 		MailType = "Email",
			                 		OriginalMessageRepliedToId = 0,
			                 		RecipientMemberId = recipientId
			                 	};
			
			var result = OAuthConsumer.Post<PostMessageResult, NewMessage>(null, newMessage);
			Assert.IsNotNull(result, "null result returned");
			Assert.IsTrue(result.Success, "success was false");
		}

		[TestMethod]
		public void SendAnFetchMailMessage()
		{
			const int recipientId = testUserId2;

			var newMessage = new NewMessage
			                 	{
			                 		Subject = "Unit Test Message",
			                 		Body = "hello this is a unit test message",
			                 		MailType = "Email",
			                 		OriginalMessageRepliedToId = 0,
			                 		RecipientMemberId = recipientId
			                 	};

			var result = OAuthConsumer.Post<PostMessageResult, NewMessage>(null, newMessage);
			Assert.IsNotNull(result, "null result returned");
			Assert.IsTrue(result.Success, "success was false");
			var messageId = result.MessageId;

			var urlParams = new Dictionary<string, string> {{"messageId", messageId.ToString()}};

			var actual = OAuthConsumer.Get<MailMessage>(urlParams);
			var actualBody = actual.Body;
			Assert.AreEqual(messageId, actual.Id, "message id");
			Assert.AreEqual(newMessage.Body, actualBody, "message bodies didn't match");
		}


		[TestMethod]
		public void SendAndDeleteMailMessage()
		{
			const int recipientId = testUserId2;

			var newMessage = new NewMessage
			                 	{
			                 		Subject = "Unit Test Message",
			                 		Body = "hello this is a unit test message",
			                 		MailType = "Email",
			                 		OriginalMessageRepliedToId = 0,
			                 		RecipientMemberId = recipientId
			                 	};

			var result = OAuthConsumer.Post<PostMessageResult, NewMessage>(null, newMessage);
			Assert.IsNotNull(result, "null result returned");
			Assert.IsTrue(result.Success, "success was false");
			var messageId = result.MessageId;

			var urlParams = new Dictionary<string, string> {{"messageId", messageId.ToString()}};

			var actual = OAuthConsumer.Get<MailMessage>(urlParams);
			var actualBody = actual.Body;
			Assert.AreEqual(messageId, actual.Id, "message id");
			Assert.AreEqual(newMessage.Body, actualBody, "message bodies didn't match");

		
			//var deleteMessage = new DeleteMessage
			//{
			//    MessageId = messageId,
			//    CurrentFolderId = 1,//Inbox
			//    CommunityId = 3
			//};

			urlParams.Add("currentFolderId", "1");
			var restResult = OAuthConsumer.Delete<MailMessage>(urlParams);
			Assert.IsNotNull(restResult, "rest result of deleting message is null");
			Assert.IsTrue(restResult.StatusCode == HttpStatusCode.OK, "rest result deleting message was not 'OK'");

			actual = OAuthConsumer.Get<MailMessage>(urlParams);
			Assert.IsNotNull(actual, "null result getting deleted message");
			Assert.AreEqual(4, actual.MemberFolderId, "message not in deleted folder");
			//Assert.AreEqual(newMessage.Body, actualBody, "message bodies didn't match");
		}

    }
}
