﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.Content.AttributeData;
using Spark.Common.RestConsumer.Models.Content.Promotion;
using Spark.Common.RestConsumer.Models.Content.Region;
using Spark.Common.RestConsumer.Models.Mail;

namespace Spark.Common.RestConsumer.Tests
{
    
    
    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass]
    public class ContentTest
    {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }


        private static OAuthConsumer OAuthConsumer;
        const int TestUserId1 = 16000206; // Won
        const int TestUserId2 = 100068787;
        const int AppicationId = 1054;

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            //var tokens = OAuthHelper.GetOAuthTokensTrusted(AppicationId, TestUserId1);
           // OAuthConsumer = new OAuthConsumer(tokens.MemberId, tokens.AccessToken, DateTime.Now.AddSeconds(tokens.ExpiresIn));
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void GetTeaseCategoriessTest()
        {
            //var result = RestConsumer.Instance.Get<List<TeaseCategory>>(null);
            var result = RestConsumer.GetInstance(9051).Get<List<TeaseCategory>>(null);

            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void GetTeasesTest()
        {
            const int teaseCategoryId = 26; //romantic

            var urlParams = new Dictionary<string, string>
                                {
                                    {"teaseCategoryId", teaseCategoryId.ToString()}

                                };

            var result = RestConsumer.GetInstance(9051).Get<List<Tease>>(urlParams);

            Assert.IsTrue(result.Count > 0);
        }

		[TestMethod]
		public void GetAllCategories()
		{
            var result = RestConsumer.Instance.Get<List<TeaseCategory>>(null);
			Assert.IsTrue(result.Count > 0);
		}

		[TestMethod]
        public void GetAttributeOptionsTest()
        {
            var urlParams = new Dictionary<string, string>
				                	{
				                		{"attributeName", "JDateReligion"},
				                	};
            var results = RestConsumer.Instance.Get<List<AttributeOption>>(urlParams);
            Assert.IsNotNull(results, "Unable to get attribute options results.");
        }

        [TestMethod]
        public void GetCitiesByZipCodeTest()
        {
            var urlParams = new Dictionary<string, string>
				                	{
				                		{"zipCode", "95014"},
				                	};
            var results = RestConsumer.Instance.Get<List<Region>>(urlParams);
            Assert.IsNotNull(results, "Unable to get cities results.");
        }


        [TestMethod]
        public void GetAffiliateTracking()
        {
            var get = new AffiliateTrackingRequest { UrlReferrer = @"http://www.google.com/?somerandomqsvar=randomvalue"};
            var result = RestConsumer.Instance.Post<AffiliateTracking, AffiliateTrackingRequest>(null, get); // needs urlparams?
            Assert.IsNotNull(result.PromotionId);

        }

        [TestMethod]
        public void GetCountries()
        {
            var urlParams = new Dictionary<string, string>
                                {
                                    {"languageId", "262144"}
                                };

            var actual = RestConsumer.Instance.Get<List<Country>>(urlParams);
            Assert.IsNotNull(actual, "Unable to get countries.");
        }

        [TestMethod]
        public void GetStates()
        {
            var urlParams = new Dictionary<string, string>
                                {
                                    {"languageId", "2"},
                                    {"countryRegionId", "223"}
                                };

            var actual = RestConsumer.Instance.Get<List<State>>(urlParams);
            Assert.IsNotNull(actual, "Unable to get states.");
        }

        [TestMethod]
        public void GetCities()
        {
            var urlParams = new Dictionary<string, string>
                                {
                                    {"languageId", "262144"},
                                    {"countryRegionId", "223"},
                                    {"stateRegionId", "1538"}
                                };

            var actual = RestConsumer.Instance.Get<List<City>>(urlParams);
            Assert.IsNotNull(actual, "Unable to get cities.");
        }

        [TestMethod]
        public void GetChildRegions()
        {
            var urlParams = new Dictionary<string, string>
                                {
                                    {"languageId", "2"},
                                    {"parentRegionId", "1538"}
                                };

            var actual = RestConsumer.Instance.Get<List<ChildRegion>>(urlParams);
            Assert.IsNotNull(actual, "Unable to get child regions.");
        }
    }
}
