﻿using System;
using System.Configuration;
using System.Globalization;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.Photos;
using Spark.Common.RestConsumer.Models.Profile;

namespace Spark.Common.RestConsumer.Tests
{

    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ProfileTest
    {
        // mobiletest101 100170050
        // mobiletest102 111644211
        // mobiletest103 100170023
        // mobiletest104 111644214
        // mobiletest105 108009966

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        private static OAuthConsumer OAuthConsumer;
        const int TestUserId1 = 16000206; // Won dev
//        const int TestUserId1 = 27029711; // Won prod
        const int TestUserId2 = 100068787;
        const int ApplicationId = 1054;
        const string devAppSecret = "nZGVVfj8dfaBPKsx_dmcRXQml8o5N-iivf5lBkrAmLQ1";


        const string User1Email = "wlee@spark.net";
        private const string User1Password = "1234";

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            var tokens = OAuthHelper.GetTokensUsingPassword(ApplicationId, User1Email, User1Password);
            OAuthConsumer = new OAuthConsumer(tokens.MemberId, tokens.AccessToken, DateTime.Now.AddSeconds(tokens.ExpiresIn));
        }


        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void GetMiniProfile()
        {
            const int memberId = TestUserId1;
            var dict = new Dictionary<string, string> { { "memberId", memberId.ToString() }, { "targetMemberId", memberId.ToString() } };

            var miniProfile = OAuthConsumer.Get<MiniProfile>(dict);
            Assert.IsNotNull(miniProfile, "mini profile is null");
            const int expected = 22;
            var actual = miniProfile.Age;

            Assert.AreEqual(expected, actual);
        }


		/// <summary>
        ///A test for Get FullProfile
        ///</summary>
        [TestMethod]
        public void GetFullProfileTest()
        {
            const int memberId = TestUserId1;
            var urlParams = new Dictionary<string, string> { { "memberId", memberId.ToString() }, { "targetMemberId", memberId.ToString() } };

			var profile = OAuthConsumer.Get<FullProfile>(urlParams);
            Assert.IsNotNull(profile, "Profile is null");
            Assert.AreEqual(memberId.ToString(), profile.Username);
        }

        [TestMethod]
        public void GetMemberStatusTest()
        {
            const int memberId = TestUserId1;
            var urlParams = new Dictionary<string, string> { { "applicationId",  ApplicationId.ToString()}, {"memberId", memberId.ToString() }, { "targetMemberId", memberId.ToString() } };
            var qsPArarms = new Dictionary<string, string> { { "client_secret", devAppSecret } };
            var status = RestConsumer.Instance.Get<MemberStatus>(urlParams, qsPArarms);
            Assert.IsNotNull(status, "MemberStatus is null");
            Assert.IsNotNull(status.SubscriptionStatus, "SubscriptionStatus is null");
            Assert.AreEqual(status.SubscriptionStatus, "Member", "member was not of expected subscription status");
        }

        [TestMethod]
        public void GetInvalidmberStatusTest()
        {
            const int memberId = 123;
            var urlParams = new Dictionary<string, string> { { "applicationId", ApplicationId.ToString(CultureInfo.InvariantCulture) }, { "memberId", memberId.ToString(CultureInfo.InvariantCulture) }, { "targetMemberId", memberId.ToString(CultureInfo.InvariantCulture) } };
            var qsPArarms = new Dictionary<string, string> { { "client_secret", devAppSecret } };
            var status = RestConsumer.Instance.Get<MemberStatus>(urlParams, qsPArarms);
            Assert.IsNotNull(status, "MemberStatus is null");
            Assert.IsNotNull(status.SubscriptionStatus, "SubscriptionStatus is null");
            Assert.AreEqual(status.SubscriptionStatus, "InvalidMember", "a bad memberID is resturned as a valid member");
        }

        [TestMethod]
		public void GetPhotosTest()
		{
			const int memberId = TestUserId1;
            var dict = new Dictionary<string, string> { { "memberId", memberId.ToString() }, { "targetMemberId", memberId.ToString() } };

			var photos = OAuthConsumer.Get<List<Photo>>(dict);
			Assert.IsNotNull(photos, "photos object is null");
		}


		/// <summary>
		///A test for Put attributes
		///</summary>
        [TestMethod()]
        public void UpdateHeightTest()
		{
            const int memberId = TestUserId1;

            var random = new Random();
            var height = random.Next(137, 244); // 137-244cm
            var singleAttrbutes = new Dictionary<string, object> { { "HeightCm", height }};

		    var attributeRequest = new AttributeRequest
		                               {
		                                   //BrandUri = brandHost,
		                                   Attributes = singleAttrbutes,
		                                   AttributesMultivalue = null
		                               };
            var urlParams = new Dictionary<string, string> { { "memberId", memberId.ToString() }, { "targetMemberId", memberId.ToString() } };
			var response = OAuthConsumer.Put(attributeRequest, urlParams);
            Assert.IsNotNull(response, "response");
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "failed to update attributes");
		}

        /// <summary>
        ///A test for Put attributes
        ///</summary>
        [TestMethod]
        public void UpdateAndVerifyHeightTest()
        {
            const int memberId = TestUserId1;

            var random = new Random();
            var height = random.Next(137, 244); // 137-244cm
            var singleAttrbutes = new Dictionary<string, object> { { "HeightCm", height } };

            var attributeRequest = new AttributeRequest
            {
                //BrandUri = brandHost,
                Attributes = singleAttrbutes,
                AttributesMultivalue = null
            };

            var urlParams = new Dictionary<string, string> { { "memberId", memberId.ToString() }, { "targetMemberId", memberId.ToString() } };
			var response = OAuthConsumer.Put(urlParams, attributeRequest);
            Assert.IsNotNull(response, "response");
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "failed to update attributes");


            var profile = OAuthConsumer.Get<FullProfile>(urlParams);
            Assert.AreEqual(height, profile.HeightCm, "height did not match the set height");
        }
        /// <summary>
		///A test for Put attributes
		///</summary>
		[TestMethod()]
		public void UpdateAttributesTest()
		{
			const int memberId = TestUserId1;
		    var proofGuid = Guid.NewGuid();

			const int height = 193;
			var grewUpInText = "The place where I grew up LA" + proofGuid;
			var desiredJDateReligionList = new List<int> {256, 512};
			var musicList = new List<int> { 32, 1073741824 };
			var birthday = new DateTime(1990, 4, 1);

			var singleAttributes = new Dictionary<string, object> {{"HeightCm", height}, {"GrewUpIn", grewUpInText}, {"Birthdate", birthday}};
			var multiAttributes = new Dictionary<string, List<int>> { { "MatchJDateReligion", desiredJDateReligionList }, { "FavoriteMusic", musicList} };

			var attributeRequest = new AttributeRequest {Attributes = singleAttributes, AttributesMultivalue = multiAttributes};
            var urlParams = new Dictionary<string, string> { { "memberId", memberId.ToString() }, { "targetMemberId", memberId.ToString() } };

			var response = OAuthConsumer.Put(urlParams, attributeRequest);
			Assert.IsNotNull(response);
			//Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);


			var profile = OAuthConsumer.Get<FullProfile>(urlParams);

			Assert.AreEqual(height, profile.HeightCm, "height");
			Assert.AreEqual(grewUpInText, profile.GrewUpIn, "grewUpIn");
			var expectedAge = DateTime.Now - birthday;
			Assert.AreEqual((int)(expectedAge.TotalDays /365.25), profile.Age, "age");
			Assert.IsTrue(profile.FavoriteMusic.Contains("Easy Listening"), "missing easy listening");
			Assert.IsTrue(profile.FavoriteMusic.Contains("Folk"), "missing folk");

			Assert.IsTrue(profile.MatchReligiousBackground.Contains("Reform"), "missing reform");
			Assert.IsTrue(profile.MatchReligiousBackground.Contains("Culturally Jewish but not practicing"), "missing culturally Jewish");
		}


		/// <summary>
		///A test for Put attributes
		///</summary>
		[TestMethod()]
		public void UpdateGenderTest()
		{
			//const int memberId = 100068787;
			const int memberId = TestUserId1;
			const string gender = "Female";
			const string seekingGender = "Male";
			var singleAttrbutes = new Dictionary<string, object> { { "Gender", gender }, { "SeekingGender", seekingGender}};

			var attributeRequest = new AttributeRequest { Attributes = singleAttrbutes };
            var urlParams = new Dictionary<string, string> { { "memberId", memberId.ToString() }, { "targetMemberId", memberId.ToString() } };

			var response = OAuthConsumer.Put(attributeRequest, urlParams);
			Assert.IsNotNull(response);
			Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

			var profile = OAuthConsumer.Get<FullProfile>(urlParams);

			Assert.AreEqual(gender, profile.Gender, "gender");
			Assert.AreEqual(seekingGender, profile.SeekingGender, "seekinggender");
		}
	}
}
