﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.Search;
using Spark.Common.RestConsumer.Models.Subscription;

namespace Spark.Common.RestConsumer.Tests
{
    
    
    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SubscriptionTest
    {

        private static OAuthConsumer OAuthConsumer;
        const int TestUserId1 = 16000206; // Won
        const int TestUserId2 = 100068787;
        const int AppicationId = 1054;

        const string User1Email = "wlee@spark.net";
        private const string User1Password = "1234";

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            var tokens = OAuthHelper.GetTokensUsingPassword(AppicationId, User1Email, User1Password);
            OAuthConsumer = new OAuthConsumer(tokens.MemberId, tokens.AccessToken, DateTime.Now.AddSeconds(tokens.ExpiresIn));
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

		
		/// <summary>
        ///A test for Get Inbox
        ///</summary>
        [TestMethod()]
        public void GetPaymentUiPostData()
        {

            var request = new PaymentUiPostDataRequest()
                             {
//                                 BrandUri = "jdate.com",
                                 CancelUrl = "m.jdate.com/subscription/cancel",
                                 ConfirmationUrl = "m.jdate.com/subscription/confirmation",
                                 DestinationUrl = "m.jdate.com/subscription/destination",
                                 ReturnUrl = "m.jdate.com/subscription/return",
                                 ClientIp = "127.0.0.1",
                                 PrtId = "",
                                 PrtTitle = "",
                                 SrId = ""
                             };

            var response = OAuthConsumer.Post<PaymentUiPostData, PaymentUiPostDataRequest>(null, request); // needs urlparams?

            Assert.IsNotNull(response, "Unable to get PUI data.");
        }



    }
}
