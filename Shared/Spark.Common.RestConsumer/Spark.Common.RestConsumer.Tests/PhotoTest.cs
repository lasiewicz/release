﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.Photos;
using Spark.Common.RestConsumer.Models.Profile;

namespace Spark.Common.RestConsumer.Tests
{

    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PhotoTest
    {
        // mobiletest101 100170050
        // mobiletest102 111644211
        // mobiletest103 100170023
        // mobiletest104 111644214
        // mobiletest105 108009966

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        private static OAuthConsumer OAuthConsumer;
		private const int BgreenDevId = 101801668;
		private const int WLeeDev = 16000206; 
    	private const int TestUserId1 = BgreenDevId; 
//        const int TestUserId1 = 27029711; // Won prod
        const int TestUserId2 = 100068787;
        const int AppicationId = 1054;

        const string User1Email = "wlee@spark.net";
        private const string User1Password = "1234";

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            var tokens = OAuthHelper.GetTokensUsingPassword(AppicationId, User1Email, User1Password);
            OAuthConsumer = new OAuthConsumer(tokens.MemberId, tokens.AccessToken, DateTime.Now.AddSeconds(tokens.ExpiresIn));
        }


        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


		[TestMethod]
		public void GetPhotosTest()
		{
			const int memberId = TestUserId1;
            var dict = new Dictionary<string, string> { { "memberId", memberId.ToString(CultureInfo.InvariantCulture) }, { "targetMemberId", memberId.ToString(CultureInfo.InvariantCulture) } };

			var photos = OAuthConsumer.Get<List<Photo>>(dict);
			Assert.IsNotNull(photos, "photos object is null");
		}


		[TestMethod]
		public void UpdatePhotoCaption()
		{

			const int memberId = TestUserId1;
			var dict = new Dictionary<string, string> { { "memberId", memberId.ToString(CultureInfo.InvariantCulture) }, { "targetMemberId", memberId.ToString(CultureInfo.InvariantCulture) } };
			var photos = OAuthConsumer.Get<List<Photo>>(dict);
			Assert.IsNotNull(photos, "photos object is null");
			Assert.IsTrue(photos.Count > 0, "member has no photos");
			var photo = photos[0];
			var urlParams = new Dictionary<string, string> { { "memberId", memberId.ToString(CultureInfo.InvariantCulture) }, { "memberPhotoId", photo.MemberPhotoId.ToString(CultureInfo.InvariantCulture) } };
			var newCaption = String.Format("Unit test Caption {0}", Guid.NewGuid());
			var update = new PhotoCaptionUpdate {Caption = newCaption, MemberId = memberId, MemeberPhotoId = photo.MemberPhotoId};
			var result = OAuthConsumer.Put<PhotoUploadResult, PhotoCaptionUpdate>(urlParams, update);
			Assert.IsNotNull(result, "null result returned from update to caption");
			Assert.IsTrue(result.Success, String.Format("result returned failure: {0}", result.FailureReason));
			photos = OAuthConsumer.Get<List<Photo>>(dict);
			Assert.IsNotNull(photos, "couldn't load photos after caption update");
			var changedPhoto = photos.FirstOrDefault(tempPhoto => tempPhoto.MemberPhotoId == photo.MemberPhotoId);
			Assert.IsNotNull(changedPhoto, "couldn't find changed photo in photo list");
			Assert.AreEqual(changedPhoto.Caption, newCaption, String.Format("caption did not match updated caption"));
		}
	}
}
