﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Spark.Common.RestConsumer.Models.OAuth2;

namespace Spark.Common.RestConsumer.Tests
{
    public class OAuthHelper
    {
	    private static readonly ushort ApplicationId;
        private static readonly string ClientSecret;

        static OAuthHelper()
        {
            if (ConfigurationManager.AppSettings["ApplicationId"] != null)
            {
                UInt16.TryParse(ConfigurationManager.AppSettings["ApplicationId"], out ApplicationId);
            }

            if (ConfigurationManager.AppSettings["ClientSecret"] != null)
            {
                ClientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            }
        }

        public static OAuthTokens GetTokensUsingPassword(ushort applicationId, string email, string password)
        {
            var tokenRequestPassword = new TokenRequestPassword { Email = email, Password = password };
            var urlParams = new Dictionary<string, string> { { "applicationId", applicationId.ToString() } };
            var qsPArams = new Dictionary<string, string> { { "client_secret", ClientSecret } };
            var tokens = RestConsumer.Instance.Post<OAuthTokens, TokenRequestPassword>(urlParams, tokenRequestPassword, qsPArams);
            if (tokens == null || tokens.AccessToken == null)
            {
                throw new Exception("failed to get access token");
            }
            return tokens;
        }


        public static string GetAccessTokenPassword(ushort applicationId, string email, string password)
        {
            var tokens = GetTokensUsingPassword(applicationId, email, password);
            return tokens.AccessToken;
        }
    }
}
