﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.HotList;

namespace Spark.Common.RestConsumer.Tests
{

    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class HotListTest
    {

		//private const string testUserEmail = "mmishkoff@spark.net";
		//private const string testUserPassword = "1234";
        //const int testUserId1 = 100170050;

        private static OAuthConsumer _oAuthConsumer;
		const int TestUserId1 = 16000206; // Won
        const int TestUserId2 = 100068787;
        const int AppicationId = 1054;

        const string User1Email = "wlee@spark.net";
        private const string User1Password = "1234";

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            var tokens = OAuthHelper.GetTokensUsingPassword(AppicationId, User1Email, User1Password);
            _oAuthConsumer = new OAuthConsumer(tokens.MemberId, tokens.AccessToken, DateTime.Now.AddSeconds(tokens.ExpiresIn));
		}

        // mobiletest101 100170050
        // mobiletest102 111644211
        // mobiletest103 100170023
        // mobiletest104 111644214
        // mobiletest105 108009966

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void GetHotListCounts()
        {
			var hotListCounts = _oAuthConsumer.Get<HotListCounts>(null);
            Assert.IsNotNull(hotListCounts, "hotlist counts object is null");
        }

		[TestMethod]
		public void AddFavorite()
		{
			var fave = new Favorite {FavoriteMemberId = TestUserId2};
			var hotListCounts = _oAuthConsumer.Post<Favorite>(fave, null);
			Assert.IsNotNull(hotListCounts, "hotlist counts object is null");
		}

		[TestMethod]
		public void RemoveFavorite()
		{
			var urlParams = new Dictionary<string, string> { { "favoriteMemberId", TestUserId2.ToString() } };
			_oAuthConsumer.Delete<Favorite>(urlParams);
			//Assert.IsNotNull(hotListCounts, "hotlist counts object is null");
		}

		[TestMethod]
		public void GetHotListEntries()
		{
			//var cats = new List<string> {"WhoViewedYourProfile", "WhoAddedYouToTheirFavorites", "Default"};
			const string category = "WhoAddedYouToTheirFavorites";
			var urlParams = new Dictionary<string, string> { { "category", category }, {"pagesize", "5"}, {"page", "1"} };
			var hotListEntries = _oAuthConsumer.Get<List<HotListEntry>>(urlParams);
			Assert.IsNotNull(hotListEntries, "hotListEntries is null");
			Assert.IsTrue(hotListEntries.Count > 0, "hotListEntries is empty");
		}

	}
}
