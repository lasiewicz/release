﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.Applications;

namespace Spark.Common.RestConsumer.Tests
{

	/// <summary>
	///This is a test class for ProfileTest and is intended
	///to contain all ProfileTest Unit Tests
	///</summary>
	[TestClass()]
	public class ApplicationTest
	{
		private const string brandHost = "jdate.com";
		private static Uri appUri = null;
	    private static int appId;
	    private static string appSecret;
		const int testUserId1 = 100170050;


	    private const int devAppId = 1054;
        const string devAppSecret = "nZGVVfj8dfaBPKsx_dmcRXQml8o5N-iivf5lBkrAmLQ1";

		// mobiletest101 100170050
		// mobiletest102 111644211
		// mobiletest103 100170023
		// mobiletest104 111644214
		// mobiletest105 108009966

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

        // 1. Make sure to change settings in App.config accordingly to point to the right environment.
        //      1.1 refer to this project's app.config
        // 2. on the rest server you're hitting, modify web.config -> oauthrequired -> false and change back. todo:it should require oauth..
        
		[TestMethod]
		public void CreateAppTest()
		{
			var appResource = RestConsumer.Instance.Post<ResourceResult, Application>(null, 
                CreateAppObject()); 

			Assert.IsNotNull(appResource, "app resource");
			Assert.IsNotNull(appResource.Resource, "app resource.resource");
			Assert.IsTrue(Uri.IsWellFormedUriString(appResource.Resource, UriKind.Absolute), "invalid uri returned for app resource");
		    Assert.IsTrue(Uri.TryCreate(appResource.Resource, UriKind.Absolute, out appUri));

		    var path = appUri.AbsolutePath;
		    var appIdStr = path.Substring(path.LastIndexOf('/') + 1);
            Assert.IsTrue(Int32.TryParse(appIdStr, out appId));
            Assert.IsTrue(appId > 0, "app id was 0");
            var qs = appUri.Query;
            appSecret = qs.Substring(qs.LastIndexOf('=') + 1); // assumes secret is last qs param
            // http://rest.local.jdate.com/apps/application/1025?client_id=1025&client_secret=kcVQr69yB7NqEtMHPdVPqYntGR-Xenh1JYyvZ1G8elk1
		}

        [TestMethod]
	    public Application CreateAppObject()
	    {
	        var app =  new Application
	        {
	            BrandId = 90510,// Change here
	            OwnerMemberId = 27029711,// Change here
	            ClientSecret = Security.Encryption.EncryptBytes(Security.Encryption.GetCryptoBytes(15)),
	            Title = "BS IM", // Change here
	            Description = "BS app for JD",// Change here
	            RedirectUrl = "http://www.blacksingles.com" //Change here
	        };

            Assert.IsNotNull(app);

            return app;
	    }

        [TestMethod]
	    public void TestCreateAppObject()
	    {
	        Application appObject = CreateAppObject();
            Console.WriteLine(string.Format("BrandId:{0}, OwnerId:{1}, ClientSecret='{2}', Title:'{3}', Description='{4}'",appObject.BrandId, appObject.OwnerMemberId,appObject.ClientSecret, appObject.Title, appObject.Description));
	    }

	    [TestMethod] // must run a create app test first to generate app id
		public void GetNewlyCreatedAppTest()
		{
			var urlParams = new Dictionary<string, string> { { "applicationId", appId.ToString() } }; 
			var queryStringParams = new Dictionary<string, string> { { "client_secret", appSecret } };
			var app = RestConsumer.Instance.Get<Application>(urlParams, queryStringParams);
			Assert.IsNotNull(app, "app");
		}

        [TestMethod] // must run a create app test first to generate app id
        public void GetAppTest()
        {
            var urlParams = new Dictionary<string, string> { { "applicationId", "1039" } };
            var queryStringParams = new Dictionary<string, string> { { "client_secret", "mynewsecret" } };
            var app = RestConsumer.Instance.Get<Application>(urlParams, queryStringParams);
            Assert.IsNotNull(app, "app");
        }

        [TestMethod] // must run a create app test first to generate app id
        public void UpdateAppTest()
        {
            var urlParams = new Dictionary<string, string> { { "applicationId", "1039" } };
            var queryStringParams = new Dictionary<string, string> { { "client_secret", "mynewsecret" } };
            var app = RestConsumer.Instance.Get<Application>(urlParams, queryStringParams);
            Assert.IsNotNull(app, "null app");

            app.Title = "unit test app - updated";
            app.Description = "app updated by a unit test";

            RestConsumer.Instance.Put<Application>(urlParams, app, queryStringParams);
            Assert.IsNotNull(app, "app");
        }

        [TestMethod] // must run a create app test first to generate app id
        public void DeleteAppTest()
        {
            var urlParams = new Dictionary<string, string> { { "applicationId", appId.ToString() } };
            var queryStringParams = new Dictionary<string, string> { { "client_secret", appSecret} };
            var app = RestConsumer.Instance.Get<Application>(urlParams, queryStringParams);
            Assert.IsNotNull(app, "app was null before deletion");
            Assert.AreNotEqual(0, app.ApplicationId, "app was mssing before deletion");

            RestConsumer.Instance.Delete<Application>(urlParams, queryStringParams);

            try
            {
                app = RestConsumer.Instance.Get<Application>(urlParams, queryStringParams);
                Assert.IsNotNull(app, "null is returned for deleted app");
                Assert.AreEqual(0, app.ApplicationId, "app exists after deletion");
            }
            catch
            {
                // an exception here means we failed to get the app after deleting, which is probably fine
            }
        }
    
    }
}
