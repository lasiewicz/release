﻿using System;
using System.Net;
using Spark.Common.RestConsumer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Configuration;
using Spark.Common.RestConsumer.Models.Profile;

namespace Spark.Common.RestConsumer.Tests
{

    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ResourceConfigTest
    {
        private const string brandHost = "jdate.com";
        private static string accessToken = null;
        const int testUserId1 = 100170050;
        private const int testUserId2 = 100068787;
        private const string testUserEmail = "mmishkoff@spark.net";
        private const string testUserPassword = "1234";
        private const int appicationId = 10000;


        // mobiletest101 100170050
        // mobiletest102 111644211
        // mobiletest103 100170023
        // mobiletest104 111644214
        // mobiletest105 108009966

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            //accessToken = OAuthHelper.GetAccessTokenPassword(appicationId, testUserEmail, testUserPassword);
            //accessToken = OAuthHelper.GetAccessTokenTrusted(appicationId, testUserId1);
        }


        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod()]
        public void GetResourceConfig()
        {
            var rez = ResourceConfigReader.Instance.ResourceDictionary;
            Assert.IsNotNull(rez);
			Assert.IsTrue(rez.Count > 0);
        }
	}
}
