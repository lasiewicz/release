﻿using System.Collections.Generic;
using Spark.Common.RestConsumer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Spark.Common.RestConsumer.Models.Member;

namespace Spark.Common.RestConsumer.Tests
{
    
    
    /// <summary>
    ///This is a test class for AuthenticateConsumerTest and is intended
    ///to contain all AuthenticateConsumerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AuthenticateConsumerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        ///A test for authenticate
        ///</summary>
        [TestMethod()]
        public void Authenticate()
        {
            const string emailaddress = @"wlee@spark.net";
            const string password = "1234";

			var urlParams = new Dictionary<string, string> { { "email", emailaddress}, { "password", password }};

            var response = RestConsumer.Instance.Get<Authenticate>(urlParams);

            Assert.AreEqual("Authenticated", response.StatusMessage);
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

    }
}
