﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.Session;

namespace Spark.Common.RestConsumer.Tests
{
    
    
    /// <summary>
    ///This is a test class for ProfileTest and is intended
    ///to contain all ProfileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SessionTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion
        [TestMethod]
        public void VerifyAppDomainHasConfigurationSettings()
        {
            string value = ConfigurationManager.AppSettings["RESTAuthority"];
            Assert.IsFalse(String.IsNullOrEmpty(value), "No App.Config found.");
        }

        // Session APIs deprecated
        //[TestMethod()]
        //public void CreateSessionTest()
        //{
        //    var create = new Create { BrandId = 1003 };

        //    var session = RestPortal.Instance.SessionConsumer.Post<Session, Create>(null, create); // needs urlparams?

        //    Assert.IsNotNull(session.Key);

        //}
		
        ///// <summary>
        /////A test for Get Inbox
        /////</summary>
        //[TestMethod()]
        //public void GetSessionTest()
        //{

        //    var create = new Create { BrandId = 1003 };

        //    var session = RestPortal.Instance.SessionConsumer.Post<Session, Create>(null, create); // needs urlparams?
        //    System.Threading.Thread.Sleep(500);
        //    var urlParams = new Dictionary<string, string> { { "key", session.Key } };
        //    var getsession = RestPortal.Instance.SessionConsumer.Get<Session>(urlParams);

        //    Assert.IsNotNull(getsession, "Unable to get session using key.");
        //}



        ///// <summary>
        /////A test for Put Session
        /////</summary>
        //[TestMethod()]
        //public void PutSessionTest()
        //{
        //    var create = new Create { BrandId = 1003 };

        //    var session = RestPortal.Instance.SessionConsumer.Post<Session, Create>(null, create); // needs urlparams?
        //    System.Threading.Thread.Sleep(500);
        //    var urlParams = new Dictionary<string, string> { { "key", session.Key } };
        //    var getsession = RestPortal.Instance.SessionConsumer.Get<Session>(urlParams);
        //    getsession.Properties.Add("UnitTest", "Booya");
        //    RestPortal.Instance.SessionConsumer.Post<Session>(getsession);
        //    var updatedsession = RestPortal.Instance.SessionConsumer.Get<Session>(urlParams);

        //    Assert.AreEqual("Booya", updatedsession.Properties["UnitTest"]);
        //}
		

    }
}
