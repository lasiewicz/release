﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Hammock;
using Hammock.Retries;
using Hammock.Serialization;
using Hammock.Web;
using Newtonsoft.Json.Linq;
using Spark.Common.RestConsumer.Configuration;
using log4net;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer
{
    public class RestConsumer : IRestConsumer
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RestConsumer));
        // Not a static variable to support multiton
        private string _brandId;
        const string BrandIdKey = "brandId";

        // maps Types (i.e. ViewProfile) and WebMethods (GET, POST, etc.) to URLs on the REST server user to access the given resources
    	protected Dictionary<Type, Dictionary<WebMethod, string>> RequestMap = new Dictionary<Type, Dictionary<WebMethod, string>>();
		protected static Dictionary<string, Dictionary<WebMethod, string>> ResourceMap;
        // Need to deprecate once multiton is fully implemented.
        public static readonly RestConsumer Instance = new RestConsumer();
        // Multiton pattern
        private static readonly Dictionary<int, RestConsumer> Instances = new Dictionary<int, RestConsumer>();

        private RestConsumer()
        {
            try
            {
                ResourceMap = ResourceConfigReader.Instance.ResourceDictionary;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("failed to read rest consumer configuration\n {0}", ex);
                throw;
            }
            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["BrandId"]))
            {
                _brandId = ConfigurationManager.AppSettings["BrandId"];
                Log.DebugFormat("Created a new Rest Consumer instance for brand :{0}", _brandId);
            }
        }

        /// <summary>
        /// Multiton pattern to support unique instances of RestConsumer
        /// </summary>
        /// <param name="brandId"></param>
        /// <returns></returns>
        public static RestConsumer GetInstance(int brandId)
        {
            lock (Instances)
            {
                RestConsumer instance;
                if (!Instances.TryGetValue(brandId, out instance))
                {
                    instance = new RestConsumer {_brandId = brandId.ToString()};
                    Instances.Add(brandId, instance);
                    Log.DebugFormat("Created a new Rest Consumer instance for brand :{0}", brandId);
                }
                return instance;
            }
        }

        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
                                                               {
                                                                   MissingMemberHandling = MissingMemberHandling.Ignore,
                                                                   NullValueHandling = NullValueHandling.Include,
                                                                   DefaultValueHandling = DefaultValueHandling.Include,
                                                               };

		/// <summary>
		/// Given a type (ViewProfile) and WebMEthod (GET, POST, etc.), and url Params, returns a corresponding REST URL used to access the recources,
		/// with the URL params mapped into the URL.  
		/// </summary>
		/// <param name="dataRequestType">Type of object that needs to be fetched, updated, created</param>
		/// <param name="webMethod">operation to be done on the object</param>
		/// <param name="urlParamDictionary">params such as IDs, needed for the REST resource URL</param>
		/// <returns>a URL to the REST resource</returns>
        protected virtual string GetResourcePath(Type dataRequestType, WebMethod webMethod, Dictionary<string, string> urlParamDictionary)
        {
            // get the resources for this Type (i.e. MiniProfile)
            Dictionary<WebMethod, string> methodResourceDict;

            var className = dataRequestType.ToString();
//            if (!ResourceMap.TryGetValue(dataRequestType, out methodResourceDict) || methodResourceDict == null || methodResourceDict.Count == 0)
			if (!ResourceMap.TryGetValue(className, out methodResourceDict) || methodResourceDict == null || methodResourceDict.Count == 0)
            {
                throw new Exception(String.Format("No resources found for type {0}", className));
            }

            // get the list resources for the webmethod (GET, POST, etc.) for this Type
            string resource;


            if (!methodResourceDict.TryGetValue(webMethod, out resource))
            {
                throw new Exception(String.Format("No resource found for web method {0} for type {1}", webMethod, className));
            }


			var path = resource;
			if (urlParamDictionary == null)
			{
				urlParamDictionary = new Dictionary<string, string>();
			}

            if (!urlParamDictionary.ContainsKey(BrandIdKey))
            {
                urlParamDictionary.Add(BrandIdKey, _brandId);
            }

			foreach (var pair in urlParamDictionary)
			{
				var key = '{' + pair.Key + '}';
				path = path.Replace(key, pair.Value);
			}
			// todo: throw exception if there were expected params that were not given values in the urlParamDictionary
			// also, throw exception if there were extra params that weren't used in the URL
			return path;
        }


		private static RestClient CreateJsonRestClient()
		{
			var client = new RestClient
			             	{
			             		Authority = ConfigurationManager.AppSettings["RESTAuthority"],
                                Deserializer = new SparkSerializer(Settings),
			             		Serializer = new SparkSerializer(Settings),
			             	};

			client.AddHeader("Accept", "application/json");
			client.AddHeader("Content-Type", "application/json; charset=utf-8");
			client.AddHeader("User-Agent", "Hammock");

            client.Timeout = new TimeSpan(0, 0, Convert.ToInt16(ConfigurationManager.AppSettings["RestClientTimeOutSecs"]));
		    client.RetryPolicy = new RetryPolicy{RetryCount = 0};

			    return client;
		}

        /// <summary>
        /// For GET/DELETE
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="urlParamDict"></param>
        /// <param name="webMethod"></param>
        /// <param name="queryStringParams"></param>
        /// <returns></returns>
        private TResponse MakeGetOrDeleteRequest<TResponse>(Dictionary<string, string> urlParamDict, WebMethod webMethod, Dictionary<string, string> queryStringParams) where TResponse : class 
        {
            var result = MakeRequest<TResponse, RestResponse>(urlParamDict, webMethod, null, queryStringParams);
            return result;
        }

		private RestResponse MakePutOrPostRequestNoReturn<TRequest>(Dictionary<string, string> urlParamDict, WebMethod webMethod, TRequest entity, Dictionary<string, string> queryStringParams)
        {
            RestResponse restResponse = MakeRequest<RestResponse, TRequest>(urlParamDict, webMethod, entity, queryStringParams);
			return restResponse;
        }

        /// <summary>
        /// POST or PUT entity object, return object of type T
        /// </summary>
        /// <typeparam name="TResponse">Type of return object to expect</typeparam>
        /// <typeparam name="TRequest">Type of entity to be serialized into post/put content body</typeparam>
        /// <param name="urlParamDict">URL varaibles will be matched with keys and replaced with corresponding values.</param>
        /// <param name="webMethod">POST, PUT, GET, or DELETE</param>
        /// <param name="entity">entity to be serialized into post/put content body</param>
        /// <param name="queryStringParams"></param>
        /// <returns></returns>
        private TResponse MakeRequest<TResponse, TRequest>(Dictionary<string, string> urlParamDict, WebMethod webMethod, TRequest entity, Dictionary<string, string> queryStringParams) where TResponse : class
    	{
    		var client = CreateJsonRestClient();
            if (client == null)
            {
                throw new Exception("Could not create a Json Rest Client");
            }
            var request = new RestRequest
            {
                Method = webMethod,
                Path = String.Empty
            };
            switch (webMethod)
            {
                case WebMethod.Post:
                case WebMethod.Put:
                    request.Entity = entity;
                    request.Path = GetResourcePath(typeof(TRequest), webMethod, urlParamDict);
                        // if POST (or PUT), the resource whose path we want is the object being POSTed, not the return object
                    // i.e. if POSTing a Mail message, lookup up the URL for a Mail message
                    break;
                case WebMethod.Delete:
                    request.Entity = entity;
                    request.Path = GetResourcePath(typeof(TRequest), webMethod, urlParamDict);
                        // if POST (or PUT), the resource whose path we want is the object being POSTed, not the return object
                    // i.e. if POSTing a Mail message, lookup up the URL for a Mail message
                    break;
                case WebMethod.Get:
                    request.Path = GetResourcePath(typeof(TResponse), webMethod, urlParamDict);
                        // if GET, look up the resource for the type of object being *returned*
                    break;
            }
            if (queryStringParams == null)
            {
                queryStringParams = new Dictionary<string, string>();
            }
            if (queryStringParams.Count > 0)
            {
                var builder = new StringBuilder(request.Path.Contains("?") ? "&" : "?");
                foreach (var qs in queryStringParams)
                {
                    builder.AppendFormat("{0}={1}&", qs.Key, qs.Value);
                }
                builder.Remove(builder.Length -1, 1);
                request.Path += builder.ToString();
            }
            var fullRequestURL = client.Authority + request.Path;
            RestResponse<TResponse> restResponseT = null;
            RestResponse restResponse = null;
			if (typeof(TResponse) == (typeof(RestResponse)))
			{
				try
				{
                    Log.InfoFormat("Making rest request.  Method: {0} Path: {1} Entity: {2}", webMethod, fullRequestURL, entity);
					restResponse = client.Request(request);
				}
				catch (Exception ex)
				{
					string errorDetail;
					if (restResponse == null)
					{
                        errorDetail = String.Format("Error making a REST call.\n Method: {0} \nPath: {1} \nEntity: {2}", webMethod, fullRequestURL, entity);
					}
					else
					{
                        errorDetail = String.Format("Error making a REST call.\n Method: {0} \nPath: {1} \nEntity: {2}\n Response code: {3}\n desc: {4}\n Content: {5}", webMethod, fullRequestURL, entity, restResponse.StatusCode, restResponse.StatusDescription, restResponse.Content);
					}
                    Log.ErrorFormat("Failed to make REST call.  Method: {0} Path: {1} Entity: {2} Exception:\n{3}", webMethod, fullRequestURL, entity, ex);

					throw new Exception(errorDetail, ex);
				}
				return restResponse as TResponse;
			}

            try
            {
                Log.InfoFormat("Making rest request.  Method: {0} Path: {1} Entity: {2}", webMethod, fullRequestURL, entity);
                restResponseT = client.Request<TResponse>(request);
            }
            catch (Exception ex)
            {
                string errorDetail;
                if (restResponseT == null)
                {
                    errorDetail = String.Format("Error making a REST call.\n Method: {0} \nPath: {1} \nEntity: {2}", fullRequestURL, webMethod, entity);
                }
                else
                {
                    errorDetail = String.Format("Error making a REST call.\n Method: {0} \nPath: {1} \nEntity: {2}\n Response code: {3}\n desc: {4}\n Content: {5}", fullRequestURL, webMethod, entity, restResponseT.StatusCode, restResponseT.StatusDescription, restResponseT.Content);
                }
                Log.ErrorFormat("Failed to make REST call.  Method: {0} Path: {1} Entity: {2} Exception:\n{3}", webMethod, fullRequestURL, entity, ex);
                throw new Exception(errorDetail, ex);
            }

            if (restResponseT.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Log.WarnFormat("Failure in server request, code: {0} description: {1}", restResponseT.StatusCode, restResponseT.StatusDescription);

                if (restResponseT.Content != null && restResponseT.Content.IndexOf("UniqueId") != -1)
                {
                    var token = JObject.Parse(restResponseT.Content);
                    Log.ErrorFormat("UniqueId: {0} Message: {1}", token.SelectToken("UniqueId"),
                                    token.SelectToken("Message"));
                }

                if (restResponseT.StatusCode == 0)
                {
                    Log.WarnFormat("restResponseT innerException: {0} Timed out? : {1} times tried: {2} rate limited? : {3} kept alive: {4}", 
                        restResponseT.InnerException, restResponseT.TimedOut, restResponseT.TimesTried, restResponseT.SkippedDueToRateLimitingRule,
                        restResponseT.RequestKeptAlive);
                }
            }
            if (restResponseT.StatusCode == System.Net.HttpStatusCode.OK)
            {
				Log.DebugFormat("returning object {0}", restResponseT.ContentEntity);
                return restResponseT.ContentEntity;
            }

            return default(TResponse);
        }

        /// <summary>
        /// Get an object by its resource ID
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <returns>an object of type T</returns>
        public virtual TResponse Get<TResponse>(Dictionary<string, string> urlParamDict, Dictionary<string, string> queryStringParams = null) where TResponse : class
        {
            return MakeGetOrDeleteRequest<TResponse>(urlParamDict, WebMethod.Get, queryStringParams);
        }

		/// <summary>
		/// Delete an object by its resource ID
		/// </summary>
		/// <typeparam name="TRequest">Type of object to delete</typeparam>
		/// <returns></returns>
		public virtual RestResponse Delete<TRequest>(Dictionary<string, string> urlParamDict, Dictionary<string, string> queryStringParams = null) where TRequest : class
		{
			return MakeRequest<RestResponse, TRequest>(urlParamDict, WebMethod.Delete, null, queryStringParams);
		}

    	/// <summary>
    	/// POST (create) an entity object of type E
    	/// </summary>
    	/// <typeparam name="TRequest">type of the object</typeparam>
    	/// <param name="entity"></param>
    	/// <param name="urlParamDict"></param>
    	/// <param name="queryStringParams"></param>
    	public virtual RestResponse Post<TRequest>(TRequest entity, Dictionary<string, string> urlParamDict, Dictionary<string, string> queryStringParams = null)
        {
            return MakePutOrPostRequestNoReturn(urlParamDict, WebMethod.Post, entity, queryStringParams);
        }

        /// <summary>
        /// update object at given resource ID
        /// </summary>
        /// <typeparam name="E">type of the object to udpate</typeparam>
        /// <param name="urlParamDict">URL varaibles will be matched with keys and replaced with corresponding values.</param>
        /// <param name="entity">the new object, which will replace the old object</param>
        /// <param name="queryStringParams"></param>
        public virtual RestResponse Put<E>(Dictionary<string, string> urlParamDict, E entity, Dictionary<string, string> queryStringParams = null)
        {
            return MakePutOrPostRequestNoReturn(urlParamDict, WebMethod.Put, entity, queryStringParams);
        }

        /// <summary>
        /// Create object of type E.  Also returns an object of type T
        /// </summary>
        /// <typeparam name="TResponse">Type of object to create</typeparam>
        /// <typeparam name="TRequest">Type of object to be returned</typeparam>
        /// <param name="urlParamDict"></param>
        /// <param name="entity">object to be created</param>
        /// <param name="queryStringParams"></param>
        /// <returns>object of type T</returns>
        public virtual TResponse Post<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity, Dictionary<string, string> queryStringParams = null) where TResponse : class
        {
            return MakeRequest<TResponse, TRequest>(urlParamDict, WebMethod.Post, entity, queryStringParams);
        }

        public virtual TResponse Delete<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity, Dictionary<string, string> queryStringParams = null) where TResponse : class
        {
            return MakeRequest<TResponse, TRequest>(urlParamDict, WebMethod.Delete, entity, queryStringParams);
        }

        /// <summary>
        /// Update object of type E.  Also returns an object of type T
        /// </summary>
        /// <typeparam name="TResponse">Type of object to update</typeparam>
        /// <typeparam name="TRequest">Type of object to be returned</typeparam>
        /// <param name="urlParamDict">URL varaibles will be matched with keys and replaced with corresponding values.</param>
        /// <param name="entity">object to be created</param>
        /// <param name="queryStringParams"></param>
        /// <returns>object of type T</returns>
        public virtual TResponse Put<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity, Dictionary<string, string> queryStringParams = null) where TResponse : class
        {
            return MakeRequest<TResponse, TRequest>(urlParamDict, WebMethod.Put, entity, queryStringParams);
        }

        public virtual RestResponse Put<TRequest>(TRequest entity, Dictionary<string, string> urlParamDict, Dictionary<string, string> queryStringParams = null)
        {
            return MakePutOrPostRequestNoReturn(urlParamDict, WebMethod.Put, entity, queryStringParams);
        }

		private TReply MakeRestCall<TReply, TRequest>
            (TRequest request, String basePath, List<String> segments, WebMethod webMethod) where TReply:class 
        {
			var client = CreateJsonRestClient();
            if (client == null)
            {
                throw new Exception("Could not create a Json Rest Client");
            }
            var restRequest = new RestRequest { Method = webMethod };
            var path = String.Format(basePath, segments.ToArray());
            restRequest.Path = path;

            RestResponse<TReply> restResponseT;
            
            if (typeof(TReply) == (typeof(RestResponse)))
            {
                var restResponse = client.Request(restRequest);
                return restResponse as TReply;
            }
            else
            {
                restResponseT = client.Request<TReply>(restRequest);
            }
            if (restResponseT.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return restResponseT.ContentEntity;
            }

            return default(TReply);
        }



		//public RestResponse Post<TRequest>(TRequest entity, Dictionary<string, string> queryStringParams = null)
		//{
		//    throw new NotImplementedException();
		//}
	}  
}
