﻿using System.Collections.Generic;
using System;
using System.Xml.Serialization;

namespace Spark.Common.RestConsumer.Configuration
{
    [Serializable]
    public class Resource
    {
        [XmlAttribute("Type")]
        public string TypeName { get; set; }

        [XmlElement("Route")]
        public List<Route> Routes { get; set; }
    }
}
