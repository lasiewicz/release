﻿using System;
using System.Xml.Serialization;

namespace Spark.Common.RestConsumer.Configuration
{
    [Serializable]
    public class Route
    {
        [XmlAttribute("Path")]
        public string Path { get; set; }

        [XmlAttribute("Methods")]
        public string Methods { get; set; }
    }
}
