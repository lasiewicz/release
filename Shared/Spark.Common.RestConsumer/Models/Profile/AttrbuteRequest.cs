﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.Models.Profile
{
	public class AttributeRequest
	{
        //[DataMember(Name = "brandUri")]
        //public string BrandUri { get; set; }

        //[DataMember(Name = "memberId")]
        //public int MemberId { get; set; }

		//[DataMember(Name = "attributes")]
		public Dictionary<string, object> Attributes;

	    [DataMember(Name = "attributesJson")]
	    public string AttributesJson
	    {
	        get { return JsonConvert.SerializeObject(Attributes); }
	    }

	    //[DataMember(Name = "attributesMultivalue")]
		public Dictionary<string, List<int>> AttributesMultivalue;


	    [DataMember(Name = "attributesMultiValueJson")]
	    public string AttributesMultiValueJson
	    {
            get { return JsonConvert.SerializeObject(AttributesMultivalue); }
	    }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("AttributeRequest Attributes ");

            foreach (var key in Attributes.Keys)
            {
                sb.AppendLine(string.Format("Key: {0} Value: {1} ", key, Attributes[key]));
            }

            if (AttributesMultivalue != null && AttributesMultivalue.Count > 0)
            {
                sb.Append("AttributeRequest MultiValueAttributes ");

                foreach(var key in AttributesMultivalue.Keys)
                {
                    sb.Append(Environment.NewLine + string.Format("Key: {0} Values: ", key));

                    int valueCount = 0;
                    foreach(var value in AttributesMultivalue[key])
                    {
                        valueCount++;
                        sb.Append(value.ToString());

                        if(valueCount != AttributesMultivalue[key].Count)
                        {
                            sb.Append(", ");
                        }
                    }
                }
            }

            return sb.ToString();
        }
    }
}
