using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using Spark.Common.RestConsumer.Models.Photos;

namespace Spark.Common.RestConsumer.Models.Profile
{
	[DataContract(Name = "FullProfile", Namespace = "")]
	public class FullProfile : MiniProfile
	{
		[DataMember(Name = "primaryPhoto")]
		public Photo PrimaryPhoto { get; set; }

        public List<Photo> Photos { get; set; }

		[DataMember(Name = "photoCount")]
		public int PhotoCount { get; set; }
		// My Details
        //      Physical Info
		[DataMember(Name = "heightCm")]
		[DisplayFormat(DataFormatString = "({0} cm)")]
		public int? HeightCm { get; set; }

		private int HeightTotalInches
		{
			get
			{
				return Convert.ToInt32(HeightCm / 2.54);
			}
		}

		[DisplayFormat(DataFormatString = "{0}\'", NullDisplayText = "(not answered yet)")]
		public int? HeightFeet
		{
			get
			{
				if (HeightCm == null) return null;
				int inches = HeightTotalInches % 12;
				int feet = (HeightTotalInches - inches) / 12;
				return feet;

			}
		}

		[DisplayFormat(DataFormatString = "{0}\"")]
		public int? HeightInches
		{
			get
			{
				if (HeightCm == null) return null;
				return HeightTotalInches % 12;
			}
		}

		[DataMember(Name = "weightGrams")]
		[DisplayFormat(DataFormatString = "({0} kg)")]
		public int? WeightGrams { get; set; }

		[DisplayFormat(DataFormatString = "{0}")]
		public int? WeightPounds
		{
			get
			{
				return (WeightGrams == null) ? null : (int?) Convert.ToInt32(WeightGrams / 453.6);
			}
		}

		/// <summary>
		/// custom rounding to the nearest 1/2 kg
		/// modified from framework globals \bedrock\web\bedrock.matchnet.com\Framework\FrameworkGlobals.cs
		/// </summary>
		/// <param name="grams"></param>
		/// <returns></returns>
		private static double RoundKilograms(int grams)
		{
			double remainder = (grams % 1000);
			double kg = ((grams - remainder) / 1000);
			if (remainder >= 250) // round up 1/2 kg
			{
				kg += 0.5;
			}
			if (remainder >= 750) // round up a full kg
			{
				kg += 0.5;
			}
			return kg;
		}


		[DisplayFormat(DataFormatString = " ({0:#.#} kg)", NullDisplayText = "(not answered yet)")]
		public double? WeightKg
		{
			get
			{
				return (WeightGrams != null) ? (int?) Convert.ToDouble(RoundKilograms(Convert.ToInt32(WeightGrams))) : null;
			}
		}

		[DataMember(Name = "hairColor")]
		[DisplayFormat(NullDisplayText = "(not answered yet)")]
		public string Hair { get; set; }

		[DataMember(Name = "eyeColor")]
		[DisplayFormat(NullDisplayText = "(not answered yet)")]
		public string EyeColor { get; set; }

		[DataMember(Name = "bodyType")]
		[DisplayFormat(NullDisplayText = "(not answered yet)")]
		public List<string> BodyTypes { get; set; }

		[DisplayFormat(NullDisplayText = "(not answered yet)")]
		public string BodyTypeDescription
		{
			get
			{
				return BodyTypes != null ? BodyTypes.FirstOrDefault(bodyType => !String.IsNullOrEmpty(bodyType)) : null; // return first valid bodytype
			}
		}

        //      Lifestyle
        [DataMember(Name = "relocation")]
        public string Relocation { get; set; }

        [DataMember(Name = "children")]
		[DisplayFormat(NullDisplayText = "(not answered yet)")]
        public string ChildrenCountText { get; set; }

        [DataMember(Name = "custody")]
		[DisplayFormat(NullDisplayText = "(not answered yet)")]
		public string Custody { get; set; }

        [DataMember(Name = "planOnChildren")]
		[DisplayFormat(NullDisplayText = "(not answered yet)")]
		public string PlanOnChildren { get; set; }

        [DataMember(Name = "keepKosher")]
        public string KeepKosher { get; set; }

        [DataMember(Name = "synagogueAttendance")]
        public string GoToSynagogue { get; set; }

        [DataMember(Name = "smokingHabits")]
        public string SmokingHabits { get; set; }

        [DataMember(Name = "drinkingHabits")]
        public string DrinkingHabits { get; set; }

        [DataMember(Name = "zodiac")]
        public string ZodiacSign { get; set; }

        [DataMember(Name = "activityLevel")]
        public string ActivityLevel { get; set; }


        //      Background
        [DataMember(Name = "grewUpIn")]
        public string GrewUpIn { get; set; }

        [DataMember(Name = "jDateEthnicity")]
        public string JDateEthnicity { get; set; }

        [DataMember(Name = "languagesSpoken")]
        public List<string> LanguagesSpoken { get; set; }

        [DataMember(Name = "studiedOrInterestedIn")]
        public string StudiedOrInterestedIn { get; set; }

        [DataMember(Name = "educationLevel")]
        public string Education { get; set; }

        [DataMember(Name = "occupation")]
        public string Occupation { get; set; }

        [DataMember(Name = "occupationDescription")]
        public string OccupationDescription { get; set; }

        [DataMember(Name = "incomeLevel")]
        public string AnualIncome { get; set; }

        [DataMember(Name = "politicalOrientation")]
        public string PoliticalOrientation { get; set; }

        [DataMember(Name = "jDateReligion")]
        public string Religion { get; set; }


        // In My Own Words
        [DataMember(Name = "aboutMe")]
		[DisplayFormat(ConvertEmptyStringToNull = true)]
        public string AboutMe { get; set; }

		[DataMember(Name = "perfectMatchEssay")] // this will be displayed as "I'm looking for..."
		[DisplayFormat(ConvertEmptyStringToNull = true)]
		public string MyPerfectMatch { get; set; }

		[DataMember(Name = "perfectFirstDateEssay")]
		[DisplayFormat(ConvertEmptyStringToNull = true)]
		public string MyPerfectFirstDate { get; set; }

		[DataMember(Name = "idealRelationshipEssay")]
		[DisplayFormat(ConvertEmptyStringToNull = true)]
		public string MyIdealRelationship { get; set; }

		[DataMember(Name = "learnFromThePastEssay")]
		[DisplayFormat(ConvertEmptyStringToNull = true)]
		public string MyPastRelationships { get; set; }

        [DataMember(Name = "personalityTraits")] // personailty best described as
        public List<string> PersonalityTraits { get; set; }

        [DataMember(Name = "inMyFreeTimeILikeTo")]
        public List<string> InMyFreeTimeILikeTo { get; set; }

        [DataMember(Name = "likeGoingOutTo")]
        public List<string> LikeGoingOutTo { get; set; }

        [DataMember(Name = "physicalActivity")]
        public List<string> PhysicalActivity { get; set; }

        [DataMember(Name = "pets")]
        public List<string> PetsIOwnOrLike { get; set; }

        [DataMember(Name = "favoriteFoods")]
        public List<string> FavoriteFoods { get; set; }

        [DataMember(Name = "favoriteMusic")]
        public List<string> FavoriteMusic { get; set; }

        [DataMember(Name = "likeToRead")]
        public List<string> LikeToRead { get; set; }

        // My Ideal Match
        [DataMember(Name = "matchMinAge")]
		public int? AgeLowest { get; set; }

        [DataMember(Name = "matchMaxAge")]
		public int? AgeHighest { get; set; }

		public string AgeRange
		{
			get
			{
				if (AgeLowest == null || AgeHighest == null) return null;
				return String.Format("{0} to {1}", AgeLowest, AgeHighest);
			}
		}

		[DataMember(Name = "matchMaritalStatus")]
        public List<string> MatchMaritalStatus { get; set; }

        [DataMember(Name = "matchJDateReligion")]
        public List<string> MatchReligiousBackground { get; set; }

        [DataMember(Name = "matchEducationLevel")]
        public List<string> MatchEducationLevel { get; set; }

        [DataMember(Name = "matchSmokingHabits")]
        public List<string> MatchSmokingHabits { get; set; }

        [DataMember(Name = "matchDrinkingHabits")]
        public List<string> MatchDrinkingHabits { get; set; }

		[DataMember(Name = "isViewersFavorite")]
		public bool IsViewersFavorite { get; set; }
	}
}