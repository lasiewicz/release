﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Profile
{

    [DataContract(Name = "MemberStatus", Namespace = "")]
    public class MemberStatus
    {
        [DataMember(Name = "subscriptionStatus")]
        public string SubscriptionStatus { get; set; }
    }
}
