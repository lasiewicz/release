﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Mail
{
    [DataContract(Name = "Tease", Namespace = "")]
    public class Tease : IEntity
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

    }
}
