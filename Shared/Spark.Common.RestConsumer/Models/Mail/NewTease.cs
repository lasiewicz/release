﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Spark.Common.RestConsumer.Models.Mail
{
	[Serializable]
	public class NewTease
	{
        //[DataMember(Name = "brandUri")]
        //public string BrandUri { get; set; }

		[DataMember(Name = "recipientMemberId")]
		public int RecipientMemberId { get; set; }

		[DataMember(Name = "body")]
		public string Body { get; set; }

		[DataMember(Name = "teaseId")]
		public int TeaseId { get; set; }

		[DataMember(Name = "teaseCategoryId")]
		public int TeaseCategoryId { get; set; }
		
//		[DataMember(Name = "originalMessageRepliedToId")]
//		public int OriginalMessageRepliedToId { get; set; }
	}
}