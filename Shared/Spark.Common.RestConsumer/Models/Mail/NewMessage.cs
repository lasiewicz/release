﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Spark.Common.RestConsumer.Models.Mail
{
	[Serializable]
	public class NewMessage
	{
        //[DataMember(Name = "brandUri")]
        //public string BrandUri { get; set; }

		[DataMember(Name = "recipientMemberId")]
		public int RecipientMemberId { get; set; }

        //[DataMember(Name = "senderMemberId")]
        //public int SenderMemberId { get; set; }

		[DataMember(Name = "mailType")]
		public string MailType { get; set; }

		[Required(ErrorMessage="Please enter a subject")]
		[DataMember(Name = "subject")]
		public string Subject { get; set; }

		[Required(ErrorMessage = "Please enter a message body")]
		[DataMember(Name = "body")]
		public string Body { get; set; }

		[DataMember(Name = "originalMessageRepliedToId")]
		public int OriginalMessageRepliedToId { get; set; }
	}
}