﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Member
{
    [DataContract(Name = "AccessInfo", Namespace = "")]
    public class AccessInfo
    {
        [DataMember(Name = "isPayingMember")]
        public bool IsPayingMember { get; set; }
    }
}
