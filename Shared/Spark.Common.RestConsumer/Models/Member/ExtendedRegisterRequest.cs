﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.Models.Member
{
    [DataContract(Name = "ExtendedRegisterRequest")]
    public class ExtendedRegisterRequest 
    {
        public Dictionary<string, object> AttributeData { get; set; }
        public Dictionary<string, List<int>> AttributeDataMultiValue { get; set; }

        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        [DataMember(Name = "emailAddress")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "ipAddress")]
        public string IpAddress { get; set; }

        [DataMember(Name = "IovationBlackBox")]
        public string IovationBlackBox { get; set; }

        [DataMember(Name = "RegistrationSessionID")]
        public string RegistrationSessionID { get; set; }

        [DataMember(Name = "RecaptureID")]
        public string RecaptureID { get; set; }

        [DataMember(Name = "SessionTrackingID")]
        public int SessionTrackingID { get; set; }

        [DataMember(Name = "AttributeDataJson")]
        public string AttributeDataJson
        {
            get { return JsonConvert.SerializeObject(AttributeData); }
        }

        [DataMember(Name = "AttributeDataMultiValueJson")]
        public string AttributeDataMultiValueJson
        {
            get { return JsonConvert.SerializeObject(AttributeDataMultiValue); }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("ExtendedRegisterRequest" + Environment.NewLine);
            sb.AppendLine(string.Format("EmailAddress: {0}, IP: {1} Password: {2} UserName: {3} {4} ", EmailAddress, IpAddress,
                            string.Empty, UserName, Environment.NewLine));

            sb.AppendLine("Attributes");

            foreach (var key in AttributeData.Keys)
            {
                var value = key.ToLower() == "password" ? string.Empty : AttributeData[key];
                sb.AppendLine(string.Format("Key: {0} Value: {1} ", key, value));
            }

            if (AttributeDataMultiValue != null && AttributeDataMultiValue.Count > 0)
            {
                sb.AppendLine("MultiValueAttributes");

                foreach (var key in AttributeDataMultiValue.Keys)
                {
                    sb.Append(Environment.NewLine + string.Format("Key: {0} Values: ", key));

                    int valueCount = 0;
                    foreach (var value in AttributeDataMultiValue[key])
                    {
                        valueCount++;
                        sb.Append(value.ToString());

                        if (valueCount != AttributeDataMultiValue[key].Count)
                        {
                            sb.Append(", ");
                        }
                    }
                }
            }

            return sb.ToString();
        }
    }
}
