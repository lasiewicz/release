﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Member
{
    [DataContract(Name = "CompleteRegistrationWithIovationRequest", Namespace = "")]
    public class CompleteRegistrationWithIovationRequest
    {
        ///<summary>
        ///</summary>
        [DataMember(Name = "IovationBlackBox")]
        public string IovationBlackBox { get; set; }
    }
}