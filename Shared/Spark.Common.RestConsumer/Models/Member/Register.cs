﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Member
{
    [DataContract(Name = "Register", Namespace = "")]
    public class Register
    {
        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        [DataMember(Name = "emailAddress")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "ipAddress")]
        public string IpAddress { get; set; }

        public override string ToString()
        {
            return string.Format("UserName: {0} EmailAddress: {1} Password: {2} IPAddress: {3}", UserName, EmailAddress,
                                 Password, IpAddress);
        }
    }
}
