﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.Models.Member
{
    [DataContract(Name = "RegisterResult", Namespace = "")]
    public class RegisterResult
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "RegisterStatus")]
        public string RegisterStatus { get; set; }
        [DataMember(Name = "Username")]
        public string Username { get; set; }
    }
}
