﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Member
{
    [DataContract(Name = "CompleteRegistrationWithIovation", Namespace = "")]
    public class CompleteRegistrationWithIovationResult
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "Status")]
        public string Status { get; set; }
    }
}