﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Models.Profile;

namespace Spark.Common.RestConsumer.Models.Search
{
    [Serializable]
    [DataContract(Name = "SearchResults", Namespace = "")]
    public class SearchResults
    {
        [DataMember(Name = "Members")]
        public List<MiniProfile> Members { get; set; }
    }
}
