﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Search
{
	public class SearchPreferencesWithPaging : SearchPreferences
	{
		public SearchPreferencesWithPaging() { }

		public SearchPreferencesWithPaging(SearchPreferences searchPreferences)
		{
			this.MemberId = searchPreferences.MemberId;
			//this.BrandUri = searchPreferences.BrandUri;
			this.Gender = searchPreferences.Gender;
			this.SeekingGender = searchPreferences.SeekingGender;
			this.MinAge = searchPreferences.MinAge;
			this.MaxAge = searchPreferences.MaxAge;
			this.MaxDistance = searchPreferences.MaxDistance;
			this.Location = searchPreferences.Location;
			this.ShowOnlyMembersWithPhotos = searchPreferences.ShowOnlyMembersWithPhotos;
			base.ShowOnlyJewishMembers = searchPreferences.ShowOnlyJewishMembers;
		}

		[DataMember(Name = "pageSize")]
		public int PageSize { get; set; }

		[DataMember(Name = "pageNumber")]
		public int PageNumber { get; set; }

		[DataMember(Name = "showOnlyJewishMembers")]
		public override bool ShowOnlyJewishMembers // using this override to mark this as a DataMember
		{
			get { return base.ShowOnlyJewishMembers; }
			set { base.ShowOnlyJewishMembers = value; }
		}

	}
}
