﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.HotList
{
	public class HotListCounts
	{
		[DataMember(Name = "hasNewMail")]
		public bool HasNewMail { get; set; }

		[DataMember(Name = "myFavoritesTotal")]
		public int MyFavoritesTotal { get; set; }

		[DataMember(Name = "viewedMyProfileTotal")]
		public int ViewedMyProfileTotal { get; set; }

		[DataMember(Name = "addedMeToTheirFavoritesTotal")]
		public int AddedMeToTheirFavoritesTotal { get; set; }

		//[DataMember(Name = "myFavoritesNew", EmitDefaultValue = false)]
		//public int MyFavoritesNew { get; set; }

		[DataMember(Name = "viewedMyProfileNew")]
		public int ViewedMyProfileNew { get; set; }

		[DataMember(Name = "addedMeToTheirFavoritesNew")]
		public int AddedMeToTheirFavoritesNew { get; set; }
	}
}
