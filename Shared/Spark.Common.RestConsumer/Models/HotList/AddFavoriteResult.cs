﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.HotList
{
	[DataContract(Name = "AddFavoriteResult", Namespace = "")]
	public class AddFavoriteResult
	{
		[DataMember(Name = "success")]
		public bool Success { get; set; }

		[DataMember(Name = "failReason")]
		public string FailureReason { get; set; }
	}
}
