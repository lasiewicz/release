﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Photos
{
	public class PhotoUpload
	{
		[DataMember(Name = "memberId")]
		public int RecipientMemberId { get; set; }

		/// <summary>
		/// base 64 encoded file
		/// </summary>
		[DataMember(Name = "photoFile")]
		public string PhotoFile { get; set; }

		/// <summary>
		/// Name of the file from the user's file system.  Only used for logging/feedback, file is not saved with this name
		/// </summary>
		[DataMember(Name = "fileName")]
		public string FileName { get; set; }

		/// <summary>
		/// Optional caption for the photo
		/// </summary>
		[DataMember(Name = "caption")]
		public string Caption { get; set; }
	}
}
