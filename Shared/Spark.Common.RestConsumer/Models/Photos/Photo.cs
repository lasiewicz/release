﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Photos
{
    [DataContract(Name = "Photo", Namespace = "")]
    public class Photo
    {
        [DataMember(Name = "memberPhotoId")]
        public Int32 MemberPhotoId { get; set; }
        [DataMember(Name = "fullId")]
        public Int32 FullId { get; set; }
        [DataMember(Name = "thumbId")]
        public Int32 ThumbId { get; set; }
        [DataMember(Name = "fullPath")]
        public String FullPath { get; set; }
        [DataMember(Name = "thumbPath")]
        public String ThumbPath { get; set; }
        [DataMember(Name = "caption")]
        public String Caption { get; set; }
        [DataMember(Name = "listorder")]
        public byte ListOrder { get; set; }
        [DataMember(Name = "isCaptionApproved")]
        public Boolean IsCaptionApproved { get; set; }
        [DataMember(Name = "isPhotoApproved")]
        public Boolean IsPhotoApproved { get; set; }
	}
}