﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.Models.Session
{
    [DataContract(Name = "CreateSessionTrackingRequest")]
    public class CreateSessionTrackingRequest
    {
        [DataMember(Name = "SessionID")]
        public string SessionID { get; set; }
        
        [DataMember(Name = "PromotionID")]
        public int PromotionID { get; set; }

        [DataMember(Name = "LuggageID")]
        public string LuggageID { get; set; }

        [DataMember(Name = "BannerID")]
        public int BannerID { get; set; }

        [DataMember(Name = "ClientIP")]
        public string ClientIP { get; set; }

        [DataMember(Name = "ReferrerURL")]
        public string ReferrerURL { get; set; }
    }
}
