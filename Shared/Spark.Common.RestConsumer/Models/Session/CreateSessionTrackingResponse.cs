﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.Models.Session
{
    [DataContract(Name = "CreateSessionTrackingResponse")]
    public class CreateSessionTrackingResponse
    {
        [DataMember(Name = "SessionTrackingID")]
        public int SessionTrackingID { get; set; }
    }
}
