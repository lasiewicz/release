namespace Spark.Common.RestConsumer.Models
{
	public interface IEntity
	{
		int Id { get; set; }
	}
}