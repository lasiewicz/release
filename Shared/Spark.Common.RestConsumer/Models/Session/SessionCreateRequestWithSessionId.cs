﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Session
{
    [DataContract(Name = "SessionCreateRequestWithSessionId", Namespace = "")]
    public class SessionCreateRequestWithSessionId: SessionCreateRequest
    {
        [DataMember(Name = "SessionId")]
        public string SessionId { get; set; }
    }
}
