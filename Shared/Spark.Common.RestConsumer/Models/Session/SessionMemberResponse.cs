﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Session
{
    [DataContract(Name = "SessionMemberResponse")]
    public class SessionMemberResponse
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
    }
}
