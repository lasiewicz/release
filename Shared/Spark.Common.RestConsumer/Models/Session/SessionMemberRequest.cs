﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Session
{
    [DataContract(Name = "SessionMemberRequest")]
    public class SessionMemberRequest
    {
        [DataMember(Name = "SessionId")]
        public string SessionId { get; set; }
    }
}
