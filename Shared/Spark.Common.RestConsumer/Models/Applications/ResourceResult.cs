﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Applications
{
	[DataContract(Name = "ResourceResult", Namespace = "")]
	public class ResourceResult
	{
		[DataMember(Name = "resource")]
		public string Resource { get; set; }
	}
}
