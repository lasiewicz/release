using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.MockData
{
	[DataContract(Name = "user", Namespace = "")]
	public class MockUser : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

		[DataMember(Name = "username")]
		public string UserName { get; set; }

		[DataMember(Name = "name")]
		public string Name { get; set; }

		[DataMember(Name = "email")]
		public string Email { get; set; }
	}
}