﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.OAuth2
{
    [DataContract(Name = "TokenRequestTrusted")]
    public class TokenRequestTrusted
    {
        [DataMember(Name = "applicationSecret")]
        public string ApplicationSecret { get; set; }
        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }
    }
}
