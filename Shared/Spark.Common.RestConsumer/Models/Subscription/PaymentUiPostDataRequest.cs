﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.Models.Subscription
{
    [Serializable]
    [DataContract(Name = "PaymentUiPostDataRequest", Namespace = "")]
    public class PaymentUiPostDataRequest
    {
        public PaymentUiPostDataRequest()
        {
            CancelUrl = "";
            ReturnUrl = "";
            ConfirmationUrl = "";
            DestinationUrl = "";
            PrtId = "";
            SrId = "";
        }

        //[DataMember(Name = "BrandUri")]
        //public string BrandUri { get; set; }
        [DataMember(Name = "CancelUrl")]
        public string CancelUrl { get; set; }
        [DataMember(Name = "ReturnUrl")]
        public string ReturnUrl { get; set; }
        [DataMember(Name = "ConfirmationUrl")]
        public string ConfirmationUrl { get; set; }
        [DataMember(Name = "DestinationUrl")]
        public string DestinationUrl { get; set; }
        /// <summary>
        /// Purchase Reason Type Id. Semicolon separated for multiple values.
        /// </summary>
        [DataMember(Name = "PrtId")]
        public string PrtId { get; set; }
        /// <summary>
        /// Subscription Reason (?) Id. Semicolon separated for multiple values.
        /// </summary>
        [DataMember(Name = "SrId")]
        public string SrId { get; set; }
        [DataMember(Name = "ClientIp")]
        public string ClientIp { get; set; }
        [DataMember(Name = "PrtTitle")]
        public string PrtTitle { get; set; }
        /// <summary>
        /// temporary, util MVC3 is able to deserialize a dictionary
        /// </summary>
        [DataMember(Name = "omnitureVariablesJson")]
        public string OmnitureVariablesJson
        {
            get
            {
                return JsonConvert.SerializeObject(OmnitureVariables);
            }
        }

        [DataMember(Name = "properties")]
        public Dictionary<String, String> OmnitureVariables { get; set; }
    }
}
