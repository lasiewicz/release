﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.MembersOnline
{
    [Serializable]
    [DataContract(Name = "MembersOnlineRequest", Namespace = "")]
    public class MembersOnlineRequest
    {
    }
}
