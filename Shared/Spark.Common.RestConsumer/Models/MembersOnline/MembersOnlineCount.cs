﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.MembersOnline
{
    [Serializable]
    [DataContract(Name = "MembersOnlineCount", Namespace = "")]
    public class MembersOnlineCount
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }
    }
}
