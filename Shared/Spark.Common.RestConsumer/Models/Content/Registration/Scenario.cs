﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.Models.Content.Registration
{
    //if you change this enum, it needs to be updated in the Content VO as well
    public enum DeviceType
    {
        Desktop = 1,
        Handheld = 2,
        Tablet = 3
    }

    //if you change this enum, it needs to be updated in the Content VO as well
    public enum ExternalSessionType
    {
        Bedrock = 0,
        MOS = 1
    }  
    
    [DataContract(Name = "Scenario", Namespace = "")]
    public class Scenario
    {
        [DataMember(Name = "ID")]
        public int ID { get; set; }
        
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        [DataMember(Name = "IsOnePageReg")]
        public bool IsOnePageReg { get; set; }

        [DataMember(Name = "IsControl")]
        public bool IsControl { get; set; }

        [DataMember(Name = "SplashTemplate")]
        public string SplashTemplate { get; set; }

        [DataMember(Name = "RegistrationTemplate")]
        public string RegistrationTemplate { get; set; }

        [DataMember(Name = "ConfirmationTemplate")]
        public string ConfirmationTemplate { get; set; }

        [DataMember(Name = "DeviceType")]
        public DeviceType DeviceType { get; set; }

        [DataMember(Name = "ExternalSessionType")]
        public ExternalSessionType ExternalSessionType { get; set; }

        [DataMember(Name = "Steps")]
        public List<Step> Steps { get; set; }


    }
}
