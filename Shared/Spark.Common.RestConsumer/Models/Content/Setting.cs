﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Content
{
    [DataContract(Name = "Setting", Namespace = "")]
    public class Setting
    {
        [DataMember(Name = "settingName")]
        public string SettingName { get; set; }

        [DataMember(Name = "isEnabled")]
        public bool IsEnabled { get; set; }
    }
}
