﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationCompleteWithRecaptureInfo", Namespace = "")]
    public class RegistrationCompleteWithRecaptureInfo : RegistrationComplete
    {
        [DataMember(Name = "RecaptureID")]
        public string RecaptureID { get; set; }

        [DataMember(Name = "BrandID")]
        public int BrandID { get; set; }

        [DataMember(Name = "EmailAddress")]
        public string EmailAddress { get; set; }
    }
}
