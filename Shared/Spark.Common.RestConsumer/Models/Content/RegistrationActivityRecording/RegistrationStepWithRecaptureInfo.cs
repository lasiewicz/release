﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationStepWithRecaptureInfo", Namespace = "")]
    public class RegistrationStepWithRecaptureInfo : RegistrationStep
    {
        public Dictionary<string, object> AllRegFields { get; set; }

        [DataMember(Name = "RecaptureID")]
        public string RecaptureID { get; set; }

        [DataMember(Name = "BrandID")]
        public int BrandID { get; set; }

        [DataMember(Name = "EmailAddress")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "AllRegFieldsJson")]
        public string AllRegFieldsJson
        {
            get { return JsonConvert.SerializeObject(AllRegFields); }
        }
    }
}
