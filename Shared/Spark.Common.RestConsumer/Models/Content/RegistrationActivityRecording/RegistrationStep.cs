﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json;

namespace Spark.Common.RestConsumer.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationStep", Namespace = "")]
    public class RegistrationStep
    {
        public Dictionary<string, object> StepDetails { get; set; }
        
        [DataMember(Name = "RegistrationSessionID")]
        public string RegistrationSessionID { get; set; }

        [DataMember(Name = "StepID")]
        public int StepID { get; set; }

        [DataMember(Name = "TimeStamp")]
        public DateTime TimeStamp { get; set; }
        
        [DataMember(Name = "StepDetailsJson")]
        public string StepDetailsJson
        {
            get { return JsonConvert.SerializeObject(StepDetails); }
        }
    }
}