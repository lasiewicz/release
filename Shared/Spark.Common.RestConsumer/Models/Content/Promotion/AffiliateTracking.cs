﻿using System;
using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Content.Promotion
{
    [Serializable]
    [DataContract(Name = "affiliateTracking", Namespace = "")]
    public class AffiliateTracking
    {
        [DataMember(Name = "promotionId")]
        public Int32 PromotionId { get; set; }

        [DataMember(Name = "luggage")]
        public String Luggage { get; set; }
    }
}
