﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Spark.Common.RestConsumer.Models.Content.Promotion
{
    [DataContract(Name = "PromotionMappingRequest")]
    public class PromotionMappingRequest
    {
        [DataMember(Name = "ReferralURL")]
        public string ReferralURL { get; set; }

        [DataMember(Name = "ReferralHost")]
        public string ReferralHost { get; set; }

        [DataMember(Name = "ReferralURLQueryString")]
        public string ReferralURLQueryString { get; set; }
    }
}
