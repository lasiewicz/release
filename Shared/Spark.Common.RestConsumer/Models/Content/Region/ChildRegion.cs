﻿using System.Runtime.Serialization;

namespace Spark.Common.RestConsumer.Models.Content.Region
{
    [DataContract(Name = "ChildRegion", Namespace = "")]
    public class ChildRegion : Region
    {
    }
}

