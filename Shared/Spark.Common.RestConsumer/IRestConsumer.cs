﻿using System;
using System.Collections.Generic;
using Hammock;
using Hammock.Web;

namespace Spark.Common.RestConsumer
{
    public interface IRestConsumer
    {

        /// <summary>
        /// Get an object by its resource ID
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <returns>an object of type T</returns>
        TResponse Get<TResponse>(Dictionary<string, string> urlParamDict,
                                 Dictionary<string, string> queryStringParams = null) where TResponse : class;

        /// <summary>
        /// Delete an object by its resource ID
        /// </summary>
        /// <typeparam name="TRequest">Type of object to delete</typeparam>
        /// <returns></returns>
        RestResponse Delete<TRequest>(Dictionary<string, string> urlParamDict,
                              Dictionary<string, string> queryStringParams = null) where TRequest : class;

        /// <summary>
        /// update object at given resource ID
        /// </summary>
        /// <typeparam name="E">type of the object to udpate</typeparam>
        /// <param name="urlParamDict">URL varaibles will be matched with keys and replaced with corresponding values.</param>
        /// <param name="entity">the new object, which will replace the old object</param>
        /// <param name="queryStringParams"></param>
        RestResponse Put<E>(Dictionary<string, string> urlParamDict, E entity,
                            Dictionary<string, string> queryStringParams = null);

        /// <summary>
        /// Create object of type E.  Also returns an object of type T
        /// </summary>
        /// <typeparam name="TResponse">Type of object to create</typeparam>
        /// <typeparam name="TRequest">Type of object to be returned</typeparam>
        /// <param name="urlParamDict"></param>
        /// <param name="entity">object to be created</param>
        /// <param name="queryStringParams"></param>
        /// <returns>object of type T</returns>
        TResponse Post<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity,
                                            Dictionary<string, string> queryStringParams = null) where TResponse : class;

        TResponse Delete<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity,
                                              Dictionary<string, string> queryStringParams = null)
            where TResponse : class;

        /// <summary>
        /// Update object of type E.  Also returns an object of type T
        /// </summary>
        /// <typeparam name="TResponse">Type of object to update</typeparam>
        /// <typeparam name="TRequest">Type of object to be returned</typeparam>
        /// <param name="urlParamDict">URL varaibles will be matched with keys and replaced with corresponding values.</param>
        /// <param name="entity">object to be created</param>
        /// <param name="queryStringParams"></param>
        /// <returns>object of type T</returns>
        TResponse Put<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity,
                                           Dictionary<string, string> queryStringParams = null) where TResponse : class;

        RestResponse Put<TRequest>(TRequest entity, Dictionary<string, string> urlParamDict,
                                   Dictionary<string, string> queryStringParams = null);

    }
}
