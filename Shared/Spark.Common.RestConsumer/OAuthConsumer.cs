﻿#region

using System;
using System.Collections.Generic;
using Hammock;

#endregion

namespace Spark.Common.RestConsumer
{
    /// <summary>
    ///     Adapter Pattern.  The wrapper handles oauth validation, including refreshing the token before passing request to OAuthConsumer
    ///     Deprecate this if then else shorthands once the multiton pattern has been implemented in all rest consumer clients.
    /// </summary>
    public class OAuthConsumer : IRestConsumer
    {
        private int _brandId;

        // Deprecate this constructor once the multiton pattern has been implemented in all rest consumer clients.
        public OAuthConsumer(int memberId, string accessToken, DateTime accessExpires)
        {
            Initialize(memberId, accessToken, accessExpires, int.MinValue);
        }

        public OAuthConsumer(int memberId, string accessToken, DateTime accessExpires, int brandId)
        {
            Initialize(memberId, accessToken, accessExpires, brandId);
        }

        public int LoggedInMemberId { get; set; }
        private string AccessToken { get; set; }
        private DateTime AccessExpires { get; set; }

        #region IRestConsumer Members

        public TResponse Delete<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity,
                                                     Dictionary<string, string> queryStringParams = null)
            where TResponse : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? RestConsumer.Instance.Delete<TResponse, TRequest>(urlParamDict, entity, queryStringParams)
                       : RestConsumer.GetInstance(_brandId).Delete<TResponse, TRequest>(urlParamDict, entity,
                                                                                        queryStringParams);
        }


        public TResponse Get<TResponse>(Dictionary<string, string> urlParamDict,
                                        Dictionary<string, string> queryStringParams = null) where TResponse : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? RestConsumer.Instance.Get<TResponse>(urlParamDict, queryStringParams)
                       : RestConsumer.GetInstance(_brandId).Get<TResponse>(urlParamDict, queryStringParams);
        }


        public TResponse Post<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity,
                                                   Dictionary<string, string> queryStringParams = null)
            where TResponse : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? RestConsumer.Instance.Post<TResponse, TRequest>(urlParamDict, entity, queryStringParams)
                       : RestConsumer.GetInstance(_brandId).Post<TResponse, TRequest>(urlParamDict, entity,
                                                                                      queryStringParams);
        }

        public RestResponse Put<TRequest>(TRequest entity, Dictionary<string, string> urlParamDict,
                                          Dictionary<string, string> queryStringParams = null)
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? RestConsumer.Instance.Put(entity, urlParamDict, queryStringParams)
                       : RestConsumer.GetInstance(_brandId).Put(entity, urlParamDict, queryStringParams);
        }

        public TResponse Put<TResponse, TRequest>(Dictionary<string, string> urlParamDict, TRequest entity,
                                                  Dictionary<string, string> queryStringParams = null)
            where TResponse : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? RestConsumer.Instance.Put<TResponse, TRequest>(urlParamDict, entity, queryStringParams)
                       : RestConsumer.GetInstance(_brandId).Put<TResponse, TRequest>(urlParamDict, entity,
                                                                                     queryStringParams);
        }

        public RestResponse Put<TRequest>(Dictionary<string, string> urlParamDict, TRequest entity,
                                          Dictionary<string, string> queryStringParams = null)
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? RestConsumer.Instance.Put(urlParamDict, entity, queryStringParams)
                       : RestConsumer.GetInstance(_brandId).Put(urlParamDict, entity, queryStringParams);
        }

        public RestResponse Delete<TRequest>(Dictionary<string, string> urlParamDict,
                                             Dictionary<string, string> queryStringParams = null) where TRequest : class
        {
            urlParamDict = urlParamDict ?? new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? RestConsumer.Instance.Delete<TRequest>(urlParamDict, queryStringParams)
                       : RestConsumer.GetInstance(_brandId).Delete<TRequest>(urlParamDict, queryStringParams);
        }

        #endregion

        private void Initialize(int memberId, string accessToken, DateTime accessExpires, int brandId)
        {
            LoggedInMemberId = memberId;
            AccessToken = accessToken;
            AccessExpires = accessExpires;
            _brandId = brandId;
        }

        private void DecorateOauthParams(IDictionary<string, string> urlParamDict,
                                         IDictionary<string, string> queryStringParams)
        {
            //var tokens = LoginManager.GetTokensFromCookies();
            if (LoggedInMemberId < 1 || String.IsNullOrEmpty(AccessToken))
            {
                throw new Exception("Missing OAuth tokens");
            }
            //AccessToken = tokens.AccessToken;
            //AccessExpires = DateTime.Now.AddSeconds(tokens.ExpiresIn);
            if (AccessExpires == DateTime.MinValue)
            {
                throw new Exception("Invalid time for Access Expires cookie");
            }
            if (AccessExpires < DateTime.Now.AddSeconds(30))
                // if access token will expires within a minute, get a new one
            {
                throw new Exception(String.Format("access token expiring, should have been refreshed: {0}",
                                                  AccessExpires));
            }
            queryStringParams.Add("access_token", AccessToken);

            if (LoggedInMemberId > 0)
            {
                urlParamDict["memberId"] = LoggedInMemberId.ToString();
            }
        }

        /// <summary>
        ///     POST (create) an entity object of type E
        /// </summary>
        /// <typeparam name = "TRequest">type of the object</typeparam>
        /// <param name = "entity"></param>
        /// <param name = "queryStringParams"></param>
        public virtual RestResponse Post<TRequest>(TRequest entity, Dictionary<string, string> queryStringParams = null)
        {
            var urlParamDict = new Dictionary<string, string>();
            queryStringParams = queryStringParams ?? new Dictionary<string, string>();
            DecorateOauthParams(urlParamDict, queryStringParams);
            return (_brandId == int.MinValue)
                       ? RestConsumer.Instance.Post(entity, urlParamDict, queryStringParams)
                       : RestConsumer.GetInstance(_brandId).Post(entity, urlParamDict, queryStringParams);
        }
    }
}