﻿using System;
using System.Collections.Generic;
using Spark.Common.RestConsumer.Consumers;

namespace Spark.Common.RestConsumer
{
    public class RestPortal
    {

        public static readonly RestPortal Instance = new RestPortal();
        
        private RestPortal()
        {
        }

        public SessionConsumer SessionConsumer = SessionConsumer.Instance;
    }
}
