﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.CommonLibrary.Exceptions;

using log4net;
using log4net.Config;

namespace Spark.CommonLibrary.Logging
{
    public class Logger
    {
        ILog log;

        public static void Configure(string configFilePath)
        {
            System.IO.FileInfo configFile = new System.IO.FileInfo(configFilePath);
            XmlConfigurator.ConfigureAndWatch(configFile);
        }

        public Logger(Type type)
        {
            log = LogManager.GetLogger(type);
        }

        public void Debug(object message)
        {
            log.Debug(message);
        }

        public void Debug(object message, Exception ex)
        {
            log.Debug(message, ex);
        }

        public void Error(object message)
        {
            log.Error(message);
        }

        public void Error(object message, Exception ex)
        {
            log.Error(message, ex);
        }

        public void Error(object message, ExceptionBase exceptionBase, string title)
        {
            log.Error(title + "\r\n" + "\r\n" + exceptionBase.ExceptionMessageTrace + "\r\n", exceptionBase);
        }

        public void Fatal(object message)
        {
            log.Fatal(message);
        }

        public void Fatal(object message, Exception ex)
        {
            log.Fatal(message, ex);
        }
    }
}
