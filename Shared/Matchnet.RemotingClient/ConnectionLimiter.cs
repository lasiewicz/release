#region

using System;
using System.Diagnostics;

#endregion

namespace Matchnet.RemotingClient
{
    internal class ConnectionLimiter
    {
        private const string LockOut = "";
        private const string LockIn = "";

        private const string CategoryName = "Matchnet.RemotingClient";
        private const string RateOfConnectionsPerSecond = "Rate per second";
        // ReSharper disable InconsistentNaming
        private const string MemberSM = "MemberSM";
        private const string SettingsSM = "SettingsSM";
        // ReSharper restore InconsistentNaming
        private readonly PerformanceCounter _perfRatePerSecond;
        private readonly bool[] _queue;
        private byte _indexIn;
        private byte _indexOut;

        public ConnectionLimiter(byte maxConnections, string uri)
        {
            // Now we have "maxConnections" number of false, default value, items in the bool array.
            _queue = new bool[maxConnections];

            #region Performance Counters for measuring connections

            // You cannot dynamically add and remove counters in a custom category.
            // asp.net does not allow creating custom categories without registry permissions.
            // Custom Category and Performance Counters need to be manually added for this code to work.
            var counterName = string.Empty;

            if (uri.IndexOf(MemberSM, StringComparison.Ordinal) > 0)
                counterName = string.Format("{0} {1}", RateOfConnectionsPerSecond, MemberSM);

            if (uri.IndexOf(SettingsSM, StringComparison.Ordinal) > 0)
                counterName = string.Format("{0} {1}", RateOfConnectionsPerSecond, SettingsSM);

            // Calling Exists is an expensive operation but should fire only when a new uri is requested.
            try
            {
                if ((counterName != string.Empty) &&
    PerformanceCounterCategory.Exists(CategoryName) &&
    PerformanceCounterCategory.CounterExists(counterName, CategoryName))
                    _perfRatePerSecond = new PerformanceCounter
                    {
                        CategoryName = CategoryName,
                        CounterName = counterName,
                        ReadOnly = false,
                        RawValue = 0,
                    };
            }
            catch (UnauthorizedAccessException exception)
            {
                // The account running the application may not have access to the registry to lookup 
                // Performance Counters. Ignore the message and do no instantiate the counter.
                // This exception can happen if using an asp.net apppool identity which is not
                // part of the Perf Monitor Group.
                // System.UnauthorizedAccessException: Access to the registry key 'Global' is denied.
                if (exception.Message.IndexOf("registry key", StringComparison.Ordinal) > 0)
                    _perfRatePerSecond = null;
                else
                    throw;
            }

            #endregion
        }

        public bool Checkout()
        {
            lock (LockOut)
            {
                // This conditional statement causes the max connection exception.
                // Since the default value is false, this is skipped the very first time.
                if (_queue[_indexOut]) return false;
                // Indicating that we have now checked out an item with the current indexOut value.
                _queue[_indexOut] = true;
                // Bump up the indexOut number as the next available item to check out. Next available item.
                _indexOut++;
                // If the check out index reaches the queue's max length, set it to 0.
                if (_indexOut == _queue.Length)
                {
                    _indexOut = 0;
                }

                if (_perfRatePerSecond != null) _perfRatePerSecond.Increment();

                return true;
            }
        }

        public void Checkin()
        {
            lock (LockIn)
            {
                // Indicating that we have checked in an item with the current indexIn value.
                _queue[_indexIn] = false;
                // Bump up the indexIn number as the next available item to check in.
                _indexIn++;
                // If the number of checkins equal to the length of the queue set the check index to 0.
                if (_indexIn == _queue.Length)
                {
                    _indexIn = 0;
                }

                if (_perfRatePerSecond != null) _perfRatePerSecond.Decrement();
            }
        }
    }
}