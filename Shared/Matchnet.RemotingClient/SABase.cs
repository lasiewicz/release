#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Web;

#endregion

namespace Matchnet.RemotingClient
{
    public abstract class SABase
    {
        private Dictionary<string, ConnectionLimiter> _uriList = new Dictionary<string, ConnectionLimiter>();
        private object _uriLock = new object();
        private byte _maxConnections;
        private bool _maxConnectionsSet;
        private Hashtable _GetValueObjectLock = new Hashtable();

        protected SABase()
        {

        }

        protected byte MaxConnections
        {
            get
            {
                if (!_maxConnectionsSet)
                {
                    if (HttpContext.Current != null)
                    {
                        GetConnectionLimit();
                    }
                    else
                    {
                        MaxConnections = 255;
                    }

                    if (_maxConnections < 1)
                    {
                        throw new Exception("MaxConnections must be greater than zero.");
                    }
                }
                return _maxConnections;
            }
            set
            {
                if (_maxConnectionsSet)
                {
                    throw new Exception("MaxConnections has already been set.");
                }

                if (value < 1)
                {
                    throw new Exception("MaxConnections must be greater than zero.");
                }

                _maxConnections = value;
                _maxConnectionsSet = true;
            }
        }

        protected abstract void GetConnectionLimit();

        protected void Checkout(string uri)
        {
            if (getConnectionLimiter(uri).Checkout())
            {
                return;
            }

            throw new Exception("Maximum connections reached (uri: " + uri + ", maxConnections: " +
                                MaxConnections + ").");
        }

        protected void Checkin(string uri)
        {
            getConnectionLimiter(uri).Checkin();
        }

        private ConnectionLimiter getConnectionLimiter(string uri)
        {
            string uriLower = uri.ToLower();
            if (!_uriList.ContainsKey(uriLower))
            {
                lock (_uriLock)
                {
                    if (!_uriList.ContainsKey(uriLower))
                    {
                        var connectionLimiter = new ConnectionLimiter(MaxConnections, uriLower);
                        _uriList.Add(uriLower, connectionLimiter);
                    }
                }
            }

            return _uriList[uriLower];
        }

        /// <summary>
        /// This returns an object used to perform a lock when making MT call for certain VO objects
        /// that may be time consuming, which will avoid connection limit issues and will also reduce
        /// resources on MT side
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        protected object GetServiceVOLock(string cacheKey)
        {
            if (!_GetValueObjectLock.ContainsKey(cacheKey))
            {
                lock (_GetValueObjectLock.SyncRoot)
                {
                    if (!_GetValueObjectLock.ContainsKey(cacheKey))
                    {
                        _GetValueObjectLock.Add(cacheKey, new object());
                    }
                }
            }

            return _GetValueObjectLock[cacheKey];
        }
    }
}