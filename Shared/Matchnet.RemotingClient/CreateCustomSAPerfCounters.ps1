﻿
# Taken from http://blogs.technet.com/b/omx/archive/2014/04/23/use-powershell-to-create-and-update-performance-counters.aspx
# 
# This script creates a new Perf Category and 2 Perf Counters in it. 
# Specifically, to capture SA connections to MemberSM and SettingsSM.
# Due to restrictions in dynamically creating perf counters in ASP.NET I've resorted to this method of
# creating the counter by using this script. 
# Matchnet.RemotingClient should only use these counters only if they exist. Refer to ConnectionLimiter.cs.
#
# Installation Guide
#
# 1. When running this script, STOP all Spark services and web sites on the server. 
# 2. In PowerShell as admin, Run Set-ExecutionPolicy Unrestricted.
# 3. Run the script to delete and create new counters.
# 4. Run Set-ExecutionPolicy Restricted
# 5. IIS APPPOOL\[Specific App Pool Name]. Put this user in Performance Monitor Users group.
# 5. Restart Perfmon completely to see new counters.
# 6. Start the services and websites on the server.
#
# Won Lee Jan 2015

Write-Host "Starting..."

$categoryName = "Matchnet.RemotingClient"

# If you need to delete the performance object and have it re-created call this:
If ([System.Diagnostics.PerformanceCounterCategory]::Exists($categoryName))
{
    [System.Diagnostics.PerformanceCounterCategory]::Delete($categoryName)
    Write-Host "Custom Performance Category Matchnet.RemotingClient deleted."
}

$categoryHelp = "For monitoring SA connections"

# Using multiple instance as the option makes more sense but it's not simple to come up with IIS web site identifiers
# to use as the instance names
$categoryType = [System.Diagnostics.PerformanceCounterCategoryType]::SingleInstance

$categoryExists = [System.Diagnostics.PerformanceCounterCategory]::Exists($categoryName)


If (-Not $categoryExists)
{
  $objCCDC = New-Object System.Diagnostics.CounterCreationDataCollection
 
  $objCCD1 = New-Object System.Diagnostics.CounterCreationData
  $objCCD1.CounterName = "Rate per second MemberSM"
  $objCCD1.CounterType = "RateOfCountsPerSecond32"
  $objCCD1.CounterHelp = "Number Member SA connections per second"
  $objCCDC.Add($objCCD1) | Out-Null
 
  $objCCD2 = New-Object System.Diagnostics.CounterCreationData
  $objCCD2.CounterName = "Rate per second SettingsSM"
  $objCCD2.CounterType = "RateOfCountsPerSecond32"
  $objCCD2.CounterHelp = "Number Config Settings SA connections per second"
  $objCCDC.Add($objCCD2) | Out-Null
 
  [System.Diagnostics.PerformanceCounterCategory]::Create($categoryName, $categoryHelp, $categoryType, $objCCDC) | Out-Null
  Write-Host "Custom Performance Category Matchnet.RemotingClient created. Please verify in Perfmon!"
}

