﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Matchnet.Configuration.ServiceAdapters;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace Spark.CloudStorage.Tests
{
    public class UnitTests
    {
        public const string CLOUD_URL = "s3.amazonaws.com";
        public const string AWS_ACCESSKEY = "AKIAJLZ5E2GDEO72NACA";
        public const string AWS_SECRETKEY = "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";

        [TestFixtureSetUp]
        public void StartUp()
        {
        }

       
        [Test]
        public void BucketSuppliedAndInPath()
        {
            MockSettingsService settingsService = new MockSettingsService();
            string bucketName = "mybucket";
            InitSettingsMock(settingsService, bucketName, true);

            string photoPath = "/2008/02/01/08/140875191.jpg";
            Client cloudClient = new Client(3, 103, settingsService);
            string fullCloudPath = cloudClient.GetFullCloudImagePath(photoPath, FileType.MemberPhoto, false);
            Assert.IsTrue(fullCloudPath == "http://" + CLOUD_URL + "/" + bucketName + photoPath);
        }

        [Test]
        public void NoBucketAndNotInPath()
        {
            MockSettingsService settingsService = new MockSettingsService();
            InitSettingsMock(settingsService, string.Empty, false);

            string photoPath = "/2008/02/01/08/140875191.jpg";
            Client cloudClient = new Client(3, 103, settingsService);
            string fullCloudPath = cloudClient.GetFullCloudImagePath(photoPath, FileType.MemberPhoto, false);
            Assert.IsTrue(fullCloudPath == "http://" + CLOUD_URL + photoPath);
        }

        [Test]
        public void IsSecureParameterRespected()
        {
            MockSettingsService settingsService = new MockSettingsService();
            InitSettingsMock(settingsService, string.Empty, false);

            string photoPath = "/2008/02/01/08/140875191.jpg";
            Client cloudClient = new Client(3, 103, settingsService);
            string fullCloudPath = cloudClient.GetFullCloudImagePath(photoPath, FileType.MemberPhoto, false);
            Assert.IsTrue(fullCloudPath.Substring(0,7) == "http://");
            fullCloudPath = cloudClient.GetFullCloudImagePath(photoPath, FileType.MemberPhoto, true);
            Assert.IsTrue(fullCloudPath.Substring(0, 8) == "https://");
        }

        [Test]
        public void TestCacheMetadata()
        {
            MockSettingsService settingsService = new MockSettingsService();
            InitSettingsMock(settingsService, "sparkmemberphotosdev", true);

            Client cloudClient = new Client(3, 103, settingsService);
            var result = cloudClient.UploadFile(@"C:\Users\mmishkoff\Pictures\legobrokenhill01.jpg", "/2012/09/19/1/",
                                   FileType.MemberPhoto);

            Assert.IsTrue(result == OperationResult.S3AndCloudFrontSuccess);

            var amazonclient = Amazon.AWSClientFactory.CreateAmazonS3Client(AWS_ACCESSKEY, AWS_SECRETKEY);

            GetObjectMetadataResponse response = amazonclient.GetObjectMetadata(new GetObjectMetadataRequest()
                   .WithBucketName("sparkmemberphotosdev")
                   .WithKey("2012/09/19/1/legobrokenhill01.jpg"));

            Assert.IsTrue(response.Headers["Cache-Control"] == "max-age=31556926");

        }

        [Test]
        public void TestHidePhoto()
        {
            MockSettingsService settingsService = new MockSettingsService();
            string bucketName = "sparkmemberphotosdev";
            string filePath = @"C:\temp\Photo1000\120131711.jpg";
            string virtualPath = "/2013/06/27/14/";
            string virtualFilePath = virtualPath + "120131711.jpg";
            InitSettingsMock(settingsService, bucketName, true);

            Client cloudClient = new Client(3, 103, settingsService);
            cloudClient.QueueProcessor = new MockQueueProcessor();
            cloudClient.QueueProcessor.SettingsService = settingsService;

            cloudClient.HideFile(filePath, virtualPath, virtualFilePath, FileType.MemberPhoto);
            var amazonclient = Amazon.AWSClientFactory.CreateAmazonS3Client(AWS_ACCESSKEY, AWS_SECRETKEY);

            GetObjectRequest objectRequest = new GetObjectRequest()
                                              {
                                                  BucketName = bucketName, 
                                                  Key = virtualFilePath
                                              };
            try
            {
                GetObjectResponse response = amazonclient.GetObject(objectRequest);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is AmazonS3Exception);
                Assert.AreEqual("The specified key does not exist.".ToLower(), e.Message.ToLower());
            }            
        }

        [Test]
        public void TestUnhidePhoto()
        {
            MockSettingsService settingsService = new MockSettingsService();
            string bucketName = "sparkmemberphotosdev";
            string filePath = @"C:\temp\Photo1000\120131711.jpg";
            string virtualPath = "/2013/06/27/14/";
            string virtualFilePath = virtualPath+"120131711.jpg";
            InitSettingsMock(settingsService, bucketName, true);

            Client cloudClient = new Client(3, 103, settingsService);
            cloudClient.QueueProcessor = new MockQueueProcessor();
            cloudClient.QueueProcessor.SettingsService = settingsService;

            cloudClient.UnhideFile(filePath,virtualPath,virtualFilePath, FileType.MemberPhoto);
            var amazonclient = Amazon.AWSClientFactory.CreateAmazonS3Client(AWS_ACCESSKEY, AWS_SECRETKEY);

            GetObjectRequest objectRequest = new GetObjectRequest()
            {
                BucketName = bucketName,
                Key = virtualFilePath.Substring(1)
            };

            try
            {
                GetObjectResponse response = amazonclient.GetObject(objectRequest);
                Assert.IsNotNull(response);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [Test]
        public void TestGetAclResponse()
        {
            string bucketName = "sparkmemberphotosdev";
            string virtualFilePath = "2013/06/27/14/120131711.jpg";

            var amazonclient = Amazon.AWSClientFactory.CreateAmazonS3Client(AWS_ACCESSKEY, AWS_SECRETKEY);

            var response = GetAclResponse(amazonclient, bucketName, virtualFilePath);
            Assert.NotNull(response);            
            Assert.NotNull(response.AccessControlList);
        }

        [Test]
        public void TestDateTimeRangeInWhileLoop()
        {
            int StartHourOfDayToProcessBatchItems = 14;
            int EndHourOfDayToProcessBatchItems = 15;
            int ProcessBatchItemsTimeSpan = 60000;
            //only send requests to aws at midnight
            int currentHourOfDay = DateTime.Now.Hour;
            while (currentHourOfDay >= StartHourOfDayToProcessBatchItems && currentHourOfDay < EndHourOfDayToProcessBatchItems)
            {
                Console.WriteLine("Current time is " + DateTime.Now.ToShortTimeString());
                Thread.Sleep(ProcessBatchItemsTimeSpan);
                currentHourOfDay = DateTime.Now.Hour;
            }
        }

        private static GetACLResponse GetAclResponse(AmazonS3 amazonclient, string bucketName, string virtualFilePath)
        {
            GetACLRequest getAclRequest = new GetACLRequest()
                                              {
                                                  BucketName = bucketName,
                                                  Key = virtualFilePath
                                              };
            GetACLResponse response = amazonclient.GetACL(getAclRequest);
            return response;
        }


        private void InitSettingsMock(MockSettingsService settingsMock, string bucketName, bool includeBucketName)
        {
            settingsMock.AddSetting("AWS_ACCESSKEY", AWS_ACCESSKEY);
            settingsMock.AddSetting("AWS_SECRETKEY", AWS_SECRETKEY);
            settingsMock.AddSetting("CLOUD_URL", CLOUD_URL);
            settingsMock.AddSetting("AWS_MEMBER_PHOTO_BUCKET_NAME", bucketName);
            settingsMock.AddSetting("CLOUD_URL_INCLUDE_BUCKET_NAME", includeBucketName.ToString());
            settingsMock.AddSetting("MEMBER_PHOTOS_CLOUDFRONT_DISTRIBUTIONID", "E1D5KJBS6YMVH3");
            settingsMock.AddSetting("MEMBER_PHOTOS_CLOUDFRONT_ENABLED", "false");
            settingsMock.AddSetting("AWS_MEMBER_HIDDEN_PHOTO_BUCKET_NAME", "sparkhiddenmemberphotosdev");
            
        }
    }
}
