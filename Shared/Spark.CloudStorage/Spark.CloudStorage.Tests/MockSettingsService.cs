﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Reflection;
using Amazon.CloudFront;
using Amazon.CloudFront.Model;
using Matchnet.Configuration.ServiceAdapters;


namespace Spark.CloudStorage.Tests
{
    public class MockSettingsService : ISettingsSA
    {

        private Dictionary<string, string> dictionary = new Dictionary<string, string>();

        public void AddSetting(string name, string value)
        {
            dictionary.Add(name, value);
        }

        public void RemoveSetting(string name)
        {
            dictionary.Remove(name);
        }

        #region ISettingsSA Members

        public List<Matchnet.Configuration.ValueObjects.MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            return dictionary[constant];
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            return dictionary[constant];
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            return dictionary[constant];
        }

        public string GetSettingFromSingleton(string constant)
        {
            return dictionary[constant];
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityID, int siteID)
        {
            return dictionary.ContainsKey(constant);
        }

        #endregion

        #region ISettingsSA Members

        public List<Matchnet.Configuration.ValueObjects.ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }


        public Matchnet.Configuration.ValueObjects.MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class MockQueueProcessor: IQueueProcessor
    {

        #region IQueueProcessor Members

        public ISettingsSA SettingsService
        {
            get; set;
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public void Enqueue(AWSRequestMessage message)
        {
            throw new NotImplementedException();
        }

        public void Enqueue(string virtualPath, AWSMessageType type)
        {
            string accessKey = SettingsService.GetSettingFromSingleton("AWS_ACCESSKEY");
            string secretKey = SettingsService.GetSettingFromSingleton("AWS_SECRETKEY");
            string _cloudFrontDistributionId = SettingsService.GetSettingFromSingleton("MEMBER_PHOTOS_CLOUDFRONT_DISTRIBUTIONID");
            AmazonCloudFront amazonCloudFrontClient = Amazon.AWSClientFactory.CreateAmazonCloudFrontClient(accessKey, secretKey);

            var invalidationRequest = new CreateInvalidationRequest
            {
                DistributionId = _cloudFrontDistributionId,
                InvalidationBatch = new InvalidationBatch
                {
                    Paths = new Paths
                    {
                        Quantity = 1,
                        Items = new List<string>(){virtualPath}
                    },
                    CallerReference = DateTime.Now.Ticks.ToString()
                }
            };

            amazonCloudFrontClient.CreateInvalidation(invalidationRequest);
        }

        #endregion
    }
}