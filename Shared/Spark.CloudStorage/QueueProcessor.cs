﻿using System;
using System.Collections.Generic;
using System.Messaging;
using System.Threading;
using Amazon.CloudFront;
using Amazon.CloudFront.Model;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Logging;

namespace Spark.CloudStorage
{
    [Serializable]
    public enum AWSMessageType
    {
        CloudFrontInvalidationRequest=0,
        S3DeletionRequest=1
    }

    public interface IQueueProcessor
    {
        ISettingsSA SettingsService { get; set; }
        void Start();
        void Stop();
        void Enqueue(AWSRequestMessage message);
        void Enqueue(string virtualPath, AWSMessageType type);
    }

    public class QueueProcessor : IDisposable, IQueueProcessor
    {
        const string MODULE_NAME = "Spark.CloudStorage.QueueProcessor";
        public const string SERVICE_CONSTANT = "PHOTOFILE_SVC";
        public const string QUEUE_PATH = @".\private$\{0}_external_aws";
        public const string BATCH_QUEUE_PATH = @".\private$\{0}_batch_external_aws";
        bool _runnable;

        public static readonly IQueueProcessor Instance = new QueueProcessor();
        private MessageQueue externalAwsQueue;
        private MessageQueue externalAwsBatchQueue;
        private MessageQueue peekExternalAwsQueue;
        private MessageQueue peekExternalAwsBatchQueue;
        private Thread createBatchQueueThread;
        private Thread processBatchQueueThread;
        private ISettingsSA _settingsService;

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private int MaxInvalidatePaths
        {
            get
            {
                int maxInvalidatePaths = 1000;
                try
                {
                    string s = SettingsService.GetSettingFromSingleton("AWS_MAX_INVALID_PATHS");
                    maxInvalidatePaths = Int32.Parse(s);
                }
                catch (Exception e)
                {
                    maxInvalidatePaths = 1000;
                }
                return maxInvalidatePaths;
            }
        }

        private TimeSpan CreateBatchItemsTimeSpan
        {
            get
            {
                int waitMinutes = 60;
                try
                {
                    string s = SettingsService.GetSettingFromSingleton("AWS_CREATE_BATCH_ITEMS_WAIT_MINUTES");
                    waitMinutes = Int32.Parse(s);
                }
                catch (Exception e)
                {
                    waitMinutes = 60;
                }
                return new TimeSpan(0,waitMinutes,0);
            }
        }

        private TimeSpan ProcessBatchItemsTimeSpan
        {
            get
            {
                int waitMinutes = 20;
                try
                {
                    string s = SettingsService.GetSettingFromSingleton("AWS_PROCESS_BATCH_ITEMS_WAIT_MINUTES");
                    waitMinutes = Int32.Parse(s);
                }
                catch (Exception e)
                {
                    waitMinutes = 20;
                }
                return new TimeSpan(0, waitMinutes, 0);
            }
        }

        private int StartHourOfDayToProcessBatchItems
        {
            get
            {
                int hourOfDay = 0;
                try
                {
                    string s = SettingsService.GetSettingFromSingleton("AWS_START_HOUR_OF_DAY_T0_PROCESS_BATCH_ITEMS");
                    hourOfDay = Int32.Parse(s);
                }
                catch (Exception e)
                {
                    hourOfDay = 0;
                }
                return hourOfDay;
            }
        }

        private int EndHourOfDayToProcessBatchItems
        {
            get
            {
                int hourOfDay = 0;
                try
                {
                    string s = SettingsService.GetSettingFromSingleton("AWS_END_HOUR_OF_DAY_T0_PROCESS_BATCH_ITEMS");
                    hourOfDay = Int32.Parse(s);
                }
                catch (Exception e)
                {
                    hourOfDay = 0;
                }
                return hourOfDay;
            }
        }


        private QueueProcessor()
        {
            Start();
        }

        public void Start()
        {
            const string functionName = "Start";
            try
            {
                startThreads();
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, MODULE_NAME, "Started AWS QueueProcessor!", null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, ex, "Start() FAILED EPICALLY!!");
                throw ex;
            }
        }

        public void Stop()
        {
            const string functionName = "Stop";
            try
            {
                stopThreads();
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, MODULE_NAME, "Stopped AWS QueueProcessor!",null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, ex, "Stop() FAILED EPICALLY!!");
                throw ex;
            }
        }

        private void startThreads()
        {
            const string functionName = "startThreads";
            try
            {
                string queuePath = string.Format(QUEUE_PATH, Environment.MachineName.ToLower());
                if (!MessageQueue.Exists(queuePath)) MessageQueue.Create(queuePath, true);
                externalAwsQueue = new MessageQueue(queuePath);
                externalAwsQueue.Formatter = new BinaryMessageFormatter();

                peekExternalAwsQueue = new MessageQueue(queuePath, false, true, QueueAccessMode.Peek);
                peekExternalAwsQueue.Formatter= new BinaryMessageFormatter();

                string batchQueuePath = string.Format(BATCH_QUEUE_PATH, Environment.MachineName.ToLower());
                if (!MessageQueue.Exists(batchQueuePath)) MessageQueue.Create(batchQueuePath, true);
                externalAwsBatchQueue = new MessageQueue(batchQueuePath);
                externalAwsBatchQueue.Formatter = new BinaryMessageFormatter();
                peekExternalAwsBatchQueue = new MessageQueue(batchQueuePath, false, true, QueueAccessMode.Peek);
                peekExternalAwsBatchQueue.Formatter = new BinaryMessageFormatter();

                processBatchQueueThread = new Thread(new ThreadStart(processBatchQueue));
                processBatchQueueThread.Name = "ProcessAWSBatchRequestThread";
                processBatchQueueThread.Start();

                createBatchQueueThread = new Thread(new ThreadStart(createBatchItems));
                createBatchQueueThread.Name = "CreateAWSBatchRequestThread";
                createBatchQueueThread.Start();

                _runnable = true;
            }
            catch (Exception ex)
            {
                throw ex;               
            }

        }


        private void stopThreads()
        {
            const string functionName = "stopThreads";
            try
            {
                _runnable = false;
                processBatchQueueThread.Join(10000);
                createBatchQueueThread.Join(10000);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Enqueue(string virtualPath, AWSMessageType type)
        {
            AWSRequestMessage awsRequestMessage = new AWSRequestMessage(type);
            awsRequestMessage.Path = virtualPath;
            Enqueue(awsRequestMessage);
        }

        public void Enqueue(AWSRequestMessage message)
        {
            const string functionName = "Enqueue";
            MessageQueueTransaction trans = null;
            try
            {
                trans = new MessageQueueTransaction();
                trans.Begin();
                externalAwsQueue.Send(message, trans);
                trans.Commit();
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, ex, message.Path);
                throw ex;
            }
            finally
            {
                if (null != trans) trans.Dispose();
            }
        }

        private void Enqueue(AWSBatchRequestMessage message)
        {
            const string functionName = "Enqueue";
            MessageQueueTransaction trans = null;
            try
            {
                trans = new MessageQueueTransaction();
                trans.Begin();
                externalAwsBatchQueue.Send(message, trans);
                trans.Commit();

            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, ex, message.Paths);
                throw ex;
            }
            finally
            {
                if (null != trans) trans.Dispose();
            }
        }


        public void createBatchItems()
        {
            const string functionName = "createBatchItems";
            while (_runnable)
            {
                int length = peekExternalAwsQueue.GetAllMessages().Length;
//                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, MODULE_NAME, length+ " num of messages from Peek.",null);
                if (length >= MaxInvalidatePaths)
                {
//                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, MODULE_NAME, "Entered batch create.", null);
                    AWSBatchRequestMessage batchMsg = null;
                   for(int i=0; i < MaxInvalidatePaths; i++) {
                       AWSRequestMessage msg=null;
                       Message message=null;
                       try
                       {
                           message = externalAwsQueue.Receive(new TimeSpan(0, 0, 30)); //30 second timeout
//                           RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, MODULE_NAME, "Retrieved message "+i, null);
                           msg = (AWSRequestMessage)message.Body;
                           switch (msg.Type)
                           {
                               case AWSMessageType.CloudFrontInvalidationRequest:
                                   if (null == batchMsg)
                                   {                                       
                                       batchMsg = new AWSBatchRequestMessage(msg.Type);
//                                       RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, MODULE_NAME, "Created batch message.", null);
                                   }
                                   batchMsg.Paths.Add(msg.Path);
                                   RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, MODULE_NAME, "Added path to batch:"+msg.Path, null);
                                   break;
                               default:
                                   break;
                           }
                       }
                       catch (MessageQueueException mex)
                       {
                           RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, mex);
                       }
                       catch (Exception ex)
                       {
                           if (null != message)
                           {
                               RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, ex, "Failed to batch: " + msg.Path);
                               message.Priority = MessagePriority.Lowest;
                               externalAwsQueue.Send(message);
                           }
                       }
                    }
                   Enqueue(batchMsg);
//                   RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, MODULE_NAME, "Queued batch message.", null);
                }
                else
                {
                    //Sleep every hour
                    Thread.Sleep(CreateBatchItemsTimeSpan);
                }
            }
        }


        private void processBatchQueue()
        {
            const string functionName = "processBatchQueue";
            try
            {
                while (_runnable)
                {
                    //only send requests to aws during time span
                    int currentHourOfDay = DateTime.Now.Hour;
                    if (currentHourOfDay >= StartHourOfDayToProcessBatchItems && currentHourOfDay <= EndHourOfDayToProcessBatchItems)
                    {
                        processBatchQueueTransaction();
                    } 
                    else
                    {
                        Thread.Sleep(ProcessBatchItemsTimeSpan); //20 minute sleep by default
                    }
                }
            }           
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, ex, functionName +" thread no longer running!");
                throw ex;
            }           
        }

        private void processBatchQueueTransaction()
        {
            MessageQueueTransaction tran = null;
            AWSBatchRequestMessage msg = null;
            try
            {
                tran = new MessageQueueTransaction();
                tran.Begin();
                msg = receive(tran);
                processMessage(msg);
                tran.Commit();
                tran.Dispose();
                tran = null;
            }
            catch (MessageQueueException mex)
            {
                RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, mex, "No batch items in queue!");
                //sleep and wait for queue items
                Thread.Sleep(5000); 
            }
            catch (Exception ex)
            {
                var paths = (null != msg) ? msg.Paths.ToString():string.Empty;
                RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, ex, paths);
                Thread.Sleep(ProcessBatchItemsTimeSpan); //20 minutes by default
                try
                {
                    attemptResend(tran, msg, ex);
                }
                catch (Exception ex2)
                {
                    attemptRollback(tran);
                }
            }
        }

        private void processMessage(AWSBatchRequestMessage msg)
        {
            if (msg == null)
            { throw (new Exception("Queue message is null")); }

            switch (msg.Type)
            {
                case AWSMessageType.CloudFrontInvalidationRequest:

                    if (msg.Paths.Count == MaxInvalidatePaths)
                    {
                        string accessKey = SettingsService.GetSettingFromSingleton("AWS_ACCESSKEY");
                        string secretKey = SettingsService.GetSettingFromSingleton("AWS_SECRETKEY");
                        string _cloudFrontDistributionId = SettingsService.GetSettingFromSingleton("MEMBER_PHOTOS_CLOUDFRONT_DISTRIBUTIONID");
                        AmazonCloudFront amazonCloudFrontClient = Amazon.AWSClientFactory.CreateAmazonCloudFrontClient(accessKey, secretKey);

                        var invalidationRequest = new CreateInvalidationRequest
                        {
                            DistributionId = _cloudFrontDistributionId,
                            InvalidationBatch = new InvalidationBatch
                            {
                                Paths = new Paths
                                {
                                    Quantity = msg.Paths.Count,
                                    Items = msg.Paths
                                },
                                CallerReference = DateTime.Now.Ticks.ToString()
                            }
                        };

                        amazonCloudFrontClient.CreateInvalidation(invalidationRequest);
                    }
                    break;
            }
        }

        private AWSBatchRequestMessage receive(MessageQueueTransaction tran)
        {
            AWSBatchRequestMessage msg = (AWSBatchRequestMessage)externalAwsBatchQueue.Receive(tran).Body;
            return msg;
        }
       
        private void attemptResend(MessageQueueTransaction tran, AWSBatchRequestMessage msg, Exception ex)
        {
            try
            {
                if (msg == null)
                {
                    attemptRollback(tran);
                    return;
                }

                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Commit();

                    msg.Errors.Add(ex.Source + ", " + ex.Message);
                    msg.Attempts += 1;
                    msg.LastAttemptDateTime = DateTime.Now;

                    Enqueue(msg);
                }
            }
            catch (Exception e)
            {
                string message = "Exception while trying to resend msg: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, e, message);
                throw (new Exception(message));
            }
        }

        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran != null)
                {
                    if (tran.Status == MessageQueueTransactionStatus.Pending)
                    {
                        tran.Abort();
                    }
                    tran.Dispose();
                    tran = null;
                }
            }
            catch (Exception ex)
            {
                string message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                RollingFileLogger.Instance.LogException(SERVICE_CONSTANT, MODULE_NAME, ex, message);
                throw (new Exception(message));
            }
        }




        #region IDisposable Members

        public void Dispose()
        {
            Stop();
            SettingsService = null;
        }

        #endregion
    }

    [Serializable]
    public class AWSRequestMessage
    {
        private DateTime _insertDateTime = DateTime.Now;
        public DateTime InsertDateTime { get { return _insertDateTime; } }
        public AWSMessageType Type { get; set; }
        public string Path { get; set; }
        public int Attempts { get; set; }
        public List<string> Errors { get; set; }
        public DateTime LastAttemptDateTime { get; set; }

        public AWSRequestMessage(AWSMessageType _type)
        {
            Errors = new List<string>();
            LastAttemptDateTime = DateTime.Now;
            _insertDateTime = DateTime.Now;
            Type = _type;
        }

    }

    [Serializable]
    public class AWSBatchRequestMessage
    {
        private DateTime _insertDateTime = DateTime.Now;
        public DateTime InsertDateTime { get { return _insertDateTime; } }
        public AWSMessageType Type { get; set; }
        public List<string> Paths { get; set; }
        public int Attempts { get; set; }
        public List<string> Errors { get; set; }
        public DateTime LastAttemptDateTime { get; set; }

        public AWSBatchRequestMessage(AWSMessageType _type)
        {
            Paths = new List<string>(1000);
            Errors = new List<string>();
            LastAttemptDateTime = DateTime.Now;
            _insertDateTime = DateTime.Now;
            Type = _type;
        }
    }

}
