﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace Spark.CloudStorage
{
    public enum BasicOperationResult
    {
        Success = 0,
        Failure = 1
    }

    public class BasicClient
    {
        private AmazonS3 _client;
        private string _bucketName;
        private string _cloudUrl;
        private bool _appendBucketName;

        public BasicClient(string accessKey, string secretKey, string bucketName, string cloudUrl, bool appendBucketName)
        {
            _bucketName = bucketName;
            _cloudUrl = cloudUrl;
            _appendBucketName = appendBucketName;
            _client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretKey);
        }

        public BasicOperationResult UploadFile(string filePath, string virtualPath)
        {
            BasicOperationResult result = BasicOperationResult.Success;
            string bucketName = _bucketName;
            string fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1).ToLower();

            if (virtualPath.Substring(0, 1) == "/")
            {
                virtualPath = virtualPath.Substring(1);
            }

            if (virtualPath.Substring(virtualPath.Length - 1) != "/")
            {
                virtualPath = virtualPath + "/";
            }

            virtualPath = virtualPath.ToLower();

            try
            {
                var request = new TransferUtilityUploadRequest()
                    .WithBucketName(bucketName)
                    .WithFilePath(filePath)
                    .WithKey(virtualPath + fileName);

                var nvc = new NameValueCollection {{"Cache-Control", "max-age=31556926"}};
                request.AddHeaders(nvc);

                var utility = new TransferUtility(_client);
                utility.Upload(request);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {

                result = BasicOperationResult.Failure;
            }
            catch (Exception ex)
            {
                result = BasicOperationResult.Failure;
            }

            return result;
        }

        public BasicOperationResult DeleteFile(string virtualFilePath)
        {
            BasicOperationResult result = BasicOperationResult.Success;

            try
            {
                if (virtualFilePath.Substring(0, 1) == "/")
                {
                    virtualFilePath = virtualFilePath.Substring(1);
                }

                virtualFilePath = virtualFilePath.ToLower();

                string bucketName = _bucketName;

                DeleteObjectRequest request = new DeleteObjectRequest();
                request.WithBucketName(bucketName)
                       .WithKey(virtualFilePath);

                _client.DeleteObject(request);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                result = BasicOperationResult.Failure;
            }
            catch (Exception ex)
            {
                result = BasicOperationResult.Failure;
            }

            return result;
        }

        public bool Exists(string fileName)
        {
            try
            {
                if (fileName.Substring(0, 1) == "/")
                {
                    fileName = fileName.Substring(1);
                }

                GetObjectMetadataResponse response = _client.GetObjectMetadata(new GetObjectMetadataRequest()
                                                                                   .WithBucketName(_bucketName)
                                                                                   .WithKey(fileName));

                return true;
            }

            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;

                //status wasn't not found, so throw the exception 
                throw new Exception("Error connecting to cloud provider", ex);
            }
        }

        public string GetFullCloudImagePath(string virtualFilePath, bool isSecure)
        {
            if (string.IsNullOrEmpty(virtualFilePath))
                return string.Empty;

            string fullCloudPath = string.Empty;
            string domain = _cloudUrl;

            string prefix = isSecure ? "https://" : "http://";
            bool virtualStartsWithSlash = virtualFilePath.Substring(0, 1) == "/";

            if (_appendBucketName)
            {
                fullCloudPath = prefix + domain + "/" + _bucketName + (virtualStartsWithSlash ? "" : "/") + virtualFilePath;
            }
            else
            {
                fullCloudPath = prefix + domain + (virtualStartsWithSlash ? "" : "/") + virtualFilePath;
            }

            return fullCloudPath;
        }
    }
}
