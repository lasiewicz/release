﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Configuration;
using Amazon.CloudFront;
using Amazon.CloudFront.Model;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

using Matchnet.Configuration.ServiceAdapters;

namespace Spark.CloudStorage
{
    public enum OperationResult
    {
        S3AndCloudFrontSuccess=0,
        S3SuccessCloudFrontFailure=1,
        S3FailureCloudFrontSuccess=2,
        S3AndCloudFrontFailure=3
    }

    public enum FileType
    {
        None = -1,
        MemberPhoto=0, 
        MemberOriginalPhoto=1,
        MemberHiddenPhoto = 2,
        MessageAttachment=3
    }
    
    public class Client
    {
        private static AmazonS3 _s3Client; // keeping it static, no need for multiple instances
        private static AmazonCloudFront _cloudFrontClient; // keeping it static, no need for multiple instances
        private ISettingsSA _settingService;
        private string _cloudFrontDistributionId;
        private int _communityID;
        private int _siteID;
        private IQueueProcessor _queueProcessor;


        public IQueueProcessor QueueProcessor
        {
            get
            {
                if (null == _queueProcessor) _queueProcessor = Spark.CloudStorage.QueueProcessor.Instance;
                return _queueProcessor;
            }
            set { _queueProcessor = value; }
        }

        #region Constructors
        /// <summary>
        /// Client is configured with settings based on member photos
        /// </summary>
        public Client(int communityID, int siteID, ISettingsSA settingService)
        {
            string accessKey = settingService.GetSettingFromSingleton("AWS_ACCESSKEY");
            string secretKey = settingService.GetSettingFromSingleton("AWS_SECRETKEY");
            string cloudFrontDistributionID = settingService.GetSettingFromSingleton("MEMBER_PHOTOS_CLOUDFRONT_DISTRIBUTIONID");

            InitializeClient(communityID, siteID, settingService, accessKey, secretKey, cloudFrontDistributionID);
        }

        /// <summary>
        /// Client is configured with settings based on file type
        /// </summary>
        public Client(int communityID, int siteID, ISettingsSA settingService, FileType fileType)
        {
            string accessKey = "";
            string secretKey = "";
            string cloudFrontDistributionID = "";

            //Settings based on file type
            switch (fileType)
            {
                case FileType.MessageAttachment:
                    accessKey = settingService.GetSettingFromSingleton("AWS_MESSAGE_ATTACHMENTS_ACCESSKEY");
                    secretKey = settingService.GetSettingFromSingleton("AWS_MESSAGE_ATTACHMENTS_SECRETKEY");
                    cloudFrontDistributionID = settingService.GetSettingFromSingleton("MESSAGE_ATTACHMENTS_CLOUDFRONT_DISTRIBUTIONID");
                    break;
                default:
                    accessKey = settingService.GetSettingFromSingleton("AWS_ACCESSKEY");
                    secretKey = settingService.GetSettingFromSingleton("AWS_SECRETKEY");
                    cloudFrontDistributionID = settingService.GetSettingFromSingleton("MEMBER_PHOTOS_CLOUDFRONT_DISTRIBUTIONID");
                    break;

            }

            InitializeClient(communityID, siteID, settingService, accessKey, secretKey, cloudFrontDistributionID);
        }

        /// <summary>
        /// Client is configured with values fully provided by the user
        /// </summary>
        public Client(int communityID, int siteID, ISettingsSA settingService, string accessKey, string secretKey, string cloudFrontDistributionID)
        {
            InitializeClient(communityID, siteID, settingService, accessKey, secretKey, cloudFrontDistributionID);
        }

        #endregion

        #region Public Methods
        public OperationResult UploadFile(string filePath, string virtualPath, FileType fileType)
        {
            OperationResult result = OperationResult.S3AndCloudFrontSuccess;
            string bucketName = getBucketName(fileType);
            string fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1).ToLower();

            if (virtualPath.Substring(0,1) == "/")
            {
                virtualPath = virtualPath.Substring(1);
            }

            if (virtualPath.Substring(virtualPath.Length - 1) != "/")
            {
                virtualPath = virtualPath + "/";
            }

            virtualPath = virtualPath.ToLower();

            try
            {
                var request = new TransferUtilityUploadRequest()
                    .WithBucketName(bucketName)
                    .WithFilePath(filePath)
                    .WithKey(virtualPath + fileName);

                var nvc = new NameValueCollection { { "Cache-Control", "max-age=31556926" } };
                request.AddHeaders(nvc);

                var utility = new TransferUtility(_s3Client);
                utility.Upload(request);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                
                result = OperationResult.S3AndCloudFrontFailure;
            }
            catch (Exception ex)
            {
                result = OperationResult.S3SuccessCloudFrontFailure;
            }

            return result;
        }

        public OperationResult DeleteFile(string virtualFilePath, FileType fileType)
        {
            OperationResult result = OperationResult.S3AndCloudFrontSuccess;

            try
            {
                if (virtualFilePath.Substring(0, 1) == "/")
                {
                    virtualFilePath = virtualFilePath.Substring(1);
                }

                virtualFilePath = virtualFilePath.ToLower();

                string bucketName = getBucketName(fileType);

                DeleteObjectRequest request = new DeleteObjectRequest();
                request.WithBucketName(bucketName)
                    .WithKey(virtualFilePath);

                _s3Client.DeleteObject(request);

                if (isCloudFrontEnabled())
                {
                    //invaldation request REQUIRES the / prefix 
                    string correctedVirtualFilePath = "/" + virtualFilePath;
                    QueueProcessor.Enqueue(correctedVirtualFilePath, AWSMessageType.CloudFrontInvalidationRequest);
                }

            }
            catch (AmazonS3Exception ase)
            {
                result=OperationResult.S3AndCloudFrontFailure;
            }
            catch (AmazonCloudFrontException acfx)
            {
                result=OperationResult.S3SuccessCloudFrontFailure;
                throw acfx;
            }
            catch (Exception ex)
            {
                result = OperationResult.S3AndCloudFrontFailure;
                throw ex;
            }

            return result; 
        }

        public OperationResult HideFile(string filePath, string virtualPath, string virtualFilePath, FileType fileType)
        {
            OperationResult result = OperationResult.S3AndCloudFrontSuccess;
            try
            {
                result = UploadFile(filePath, virtualPath, FileType.MemberHiddenPhoto);
                if (result == OperationResult.S3AndCloudFrontSuccess)
                {
                    result = DeleteFile(virtualFilePath, fileType);
                }
            }
            catch (AmazonS3Exception ase)
            {
                result = OperationResult.S3AndCloudFrontFailure;
            }
            catch (AmazonCloudFrontException acfx)
            {
                result = OperationResult.S3SuccessCloudFrontFailure;
                throw acfx;
            }
            catch (Exception ex)
            {
                result = OperationResult.S3AndCloudFrontFailure;
                throw ex;
            }

            return result;
        }

        public OperationResult UnhideFile(string filePath, string virtualPath, string virtualFilePath, FileType fileType)
        {
            OperationResult result = OperationResult.S3AndCloudFrontSuccess;

            try
            {
                result = UploadFile(filePath, virtualPath, fileType);
                if (result == OperationResult.S3AndCloudFrontSuccess)
                {
                    result = DeleteFile(virtualFilePath, FileType.MemberHiddenPhoto);
                }
            }
            catch (AmazonS3Exception ase)
            {
                result = OperationResult.S3AndCloudFrontFailure;
            }
            catch (AmazonCloudFrontException acfx)
            {
                result = OperationResult.S3SuccessCloudFrontFailure;
                throw acfx;
            }
            catch (Exception ex)
            {
                result = OperationResult.S3AndCloudFrontFailure;
                throw ex;
            }
            return result;
        }

        public bool Exists(string fileName, FileType fileType)
        {
            try
            {
                string bucketName = getBucketName(fileType);

                if (fileName.Substring(0, 1) == "/")
                {
                    fileName = fileName.Substring(1);
                }

                GetObjectMetadataResponse response = _s3Client.GetObjectMetadata(new GetObjectMetadataRequest()
                   .WithBucketName(bucketName)
                   .WithKey(fileName));

                return true;
            }

            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;

                //status wasn't not found, so throw the exception 
                throw new Exception("Error connecting to cloud provider", ex);
            }
        }

        public string GetFullCloudImagePath(string virtualFilePath, FileType fileType, bool isSecure,
            int communityId = 0, int siteId = 0)
        {
            if (string.IsNullOrEmpty(virtualFilePath))
                return string.Empty;

            string fullCloudPath;
            var appendBucketName =
                Convert.ToBoolean(_settingService.GetSettingFromSingleton("CLOUD_URL_INCLUDE_BUCKET_NAME", _communityID,
                    _siteID));
            string domain;
            switch (fileType)
            {
                case FileType.MessageAttachment:
                    appendBucketName =
                        Convert.ToBoolean(
                            _settingService.GetSettingFromSingleton("MESSAGE_ATTACHMENT_CLOUD_URL_INCLUDE_BUCKET_NAME",
                                _communityID, _siteID));
                    domain = _settingService.GetSettingFromSingleton("MESSAGE_ATTACHMENTS_CLOUD_URL");
                    break;
                default:
                    if (communityId == 0 && siteId == 0)
                        domain = _settingService.GetSettingFromSingleton("CLOUD_URL");
                    else
                    {
                        domain = _settingService.GetSettingFromSingleton("CLOUD_URL", communityId, siteId);
                    }
                    break;
            }

            var prefix = isSecure ? "https://" : "http://";
            var virtualStartsWithSlash = virtualFilePath.Substring(0, 1) == "/";

            if (appendBucketName)
            {
                var bucketName = getBucketName(fileType);
                fullCloudPath = prefix + domain + "/" + bucketName + (virtualStartsWithSlash ? "" : "/") +
                                virtualFilePath;
            }
            else
            {
                fullCloudPath = prefix + domain + (virtualStartsWithSlash ? "" : "/") + virtualFilePath;
            }

            return fullCloudPath;
        }

        #endregion

        #region Private methods

        private void InitializeClient(int communityID, int siteID, ISettingsSA settingService, string accessKey,
            string secretKey, string cloudFrontDistributionID)
        {
            _settingService = settingService;
            _communityID = communityID;
            _siteID = siteID;
            _cloudFrontDistributionId = cloudFrontDistributionID;
            // both are static variables
            if (_s3Client == null)
                _s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretKey);
            if (_cloudFrontClient == null)
                _cloudFrontClient = Amazon.AWSClientFactory.CreateAmazonCloudFrontClient(accessKey, secretKey);
        }

        private string getBucketName(FileType fileType)
        {
            string bucketName = string.Empty;

            switch (fileType)
            {
                case FileType.MemberPhoto:
                    bucketName = _settingService.GetSettingFromSingleton("AWS_MEMBER_PHOTO_BUCKET_NAME", _communityID, _siteID); 
                    break;
                case FileType.MemberOriginalPhoto:
                    bucketName = _settingService.GetSettingFromSingleton("AWS_ORIGINAL_MEMBER_PHOTO_BUCKET_NAME", _communityID, _siteID);
                    break;
                case FileType.MemberHiddenPhoto:
                    bucketName = _settingService.GetSettingFromSingleton("AWS_MEMBER_HIDDEN_PHOTO_BUCKET_NAME", _communityID, _siteID);
                    break;
                case FileType.MessageAttachment:
                    bucketName = _settingService.GetSettingFromSingleton("AWS_MESSAGE_ATTACHMENTS_BUCKET_NAME", _communityID, _siteID);
                    break;
            }

            return bucketName;
        }

        private bool isCloudFrontEnabled()
        {
            return Convert.ToBoolean(_settingService.GetSettingFromSingleton("MEMBER_PHOTOS_CLOUDFRONT_ENABLED", _communityID, _siteID));
        }

        #endregion
    }
}
