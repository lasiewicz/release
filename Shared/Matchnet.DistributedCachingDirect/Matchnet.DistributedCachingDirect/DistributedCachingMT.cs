﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics;
using Microsoft.ApplicationServer.Caching;
using System.Threading;

namespace Matchnet.DistributedCachingDirect
{
    public class DistributedCachingMT
    {
        private const string SOURCE = "Matchnet.DistributedCaching";
        public readonly static DistributedCachingMT Instance = new DistributedCachingMT();
        private CircuitBreaker m_breaker;
        private DataCacheFactory m_cacheFactory = null;
        private ReaderWriterLock m_externalLock = null;
        


        private DistributedCachingMT()
        {
            System.Diagnostics.Trace.Write("DistributedCachingMT constructor.");
            m_breaker = new CircuitBreaker();
            m_externalLock = new ReaderWriterLock();
        }


        private DataCacheFactory CacheFactory
        {
            get
            {
                if (m_cacheFactory == null)
                {
                    string thisMachine = System.Environment.MachineName;
                    string[] thoseMachines = new string[] { "LADEVAPPFAB01" };

                    try
                    {
                        string machineStr = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("APP_FABRIC_SERVER_FROM_MT");
                        if (!string.IsNullOrEmpty(machineStr))
                        {
                            thoseMachines = machineStr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Matchnet.Exceptions.BLException("Cannot retrieve the app fabric server list", ex);
                    }

                    try
                    {
                        m_externalLock.AcquireWriterLock(-1);
                        try
                        {
                            if (m_cacheFactory == null)
                            {
                                DataCacheServerEndpoint[] cacheServers = new DataCacheServerEndpoint[thoseMachines.Length];
                                for (int i = 0; i < thoseMachines.Length; i++)
                                {
                                    cacheServers[i] = new DataCacheServerEndpoint(thoseMachines[i], 22233);
                                }
                                DataCacheFactoryConfiguration dcc = new DataCacheFactoryConfiguration()
                                {
                                    Servers = cacheServers,
                                    SecurityProperties = new DataCacheSecurity(DataCacheSecurityMode.Transport, DataCacheProtectionLevel.EncryptAndSign)
                                };
                                int requestTimeout = GetIntSetting("DISTRIBUTEDCACHINGSVC_REQUEST_TO_SECONDS", 5);
                                int channelOpenTimeout = GetIntSetting("DISTRIBUTEDCACHINGSVC_CHANNELOPEN_TO_SECONDS", 5);
                                int maxConnections = GetIntSetting("DISTRIBUTEDCACHINGSVC_MAX_APPFAB_CONNECTIONS", 3);

                                dcc.ChannelOpenTimeout = new TimeSpan(0, 0, channelOpenTimeout);
                                dcc.RequestTimeout = new TimeSpan(0, 0, requestTimeout);
                                dcc.MaxConnectionsToServer = maxConnections;
                                m_cacheFactory = new DataCacheFactory(dcc);
                            }
                        }
                        finally
                        {
                            m_externalLock.ReleaseLock();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Matchnet.Exceptions.BLException("Unable to retrieve the CacheFactory from " + thoseMachines.ToString(), ex);
                    }
                }

                return m_cacheFactory;
            }
        }

        public void ResetConnection()
        {
            Thread t = new Thread(new ThreadStart(internalReset));
            t.Start();
        }

        private void internalReset()
        {
            try
            {
                DataCacheFactory close = this.m_cacheFactory;
                this.m_cacheFactory = null;
                if (null != close) close.Dispose();
                close = null;
            }
            catch (Exception e) { }
        }
        
        private int GetIntSetting(string settingConstant, int defaultVal)
        {
            int value = defaultVal;
            try
            {
                string valueStr = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(settingConstant);
                if (!string.IsNullOrEmpty(valueStr))
                {
                    value = Convert.ToInt32(valueStr);
                }
            }
            catch (Exception e)
            {
                value = defaultVal;
            }
            return value;
        }

        private DataCache getDataCache(string namedCacheName)
        {
            DataCache dc = null;
          
            try
            {
                dc = CacheFactory.GetCache(namedCacheName);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to retrieve the named cache: " + namedCacheName, ex);
            }

            return dc;
        }

        public void Put(string cacheKey, object cacheItem, string cacheName)
        {
            try
            {
                CircuitBreaker.Apply put = delegate()
                {
                    DataCache dc = getDataCache(cacheName);
                    dc.Put(cacheKey, cacheItem);
                    return null;
                };
                m_breaker.Intercept(put);
            }
            catch (OpenCircuitException oce)
            {
                //don't throw exception.  circuit is open so just write an info message
                EventLog.WriteEntry(SOURCE, "Circuit still in open state", EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                string msg = string.Format("Put cache operation failed to named cache: {0}. Exception message: {1}", cacheName, ex.Message);
                EventLog.WriteEntry(SOURCE, msg, EventLogEntryType.Error);
            }
        }

        public object Get(string cacheKey, string cacheName)
        {
            object o = null;
            try
            {
                CircuitBreaker.Apply get = delegate()
                {
                    DataCache dc = getDataCache(cacheName);
                    return dc.Get(cacheKey);
                };
                o = m_breaker.Intercept(get);
            }
            catch (OpenCircuitException oce)
            {
                //don't throw exception.  circuit is open so just write an info message
                EventLog.WriteEntry(SOURCE, "Circuit still in open state", EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                string msg = string.Format("Get cache operation failed to named cache: {0}. Exception message: {1}", cacheName, ex.Message);
                EventLog.WriteEntry(SOURCE, msg, EventLogEntryType.Error);
            }

            return o;    
        }

    }
}
