﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if DEBUG
using NUnit.Framework;
using Matchnet.DistributedCachingDirect;
#endif
namespace Matchnet.DistributedCachingDirect_Tests
{
#if DEBUG
    [TestFixture]
    public class DistributedCachingDirectTests
    {

        [Test]
        public void TestPutInAndGetFromCache()
        {
            Random r = new Random();
            int rInt = r.Next();
            string cacheName = "searchresults";
            string cacheKey = "default" + r;
            DistributedCachingMT.Instance.Put(cacheKey,cacheKey, cacheName);
            object cacheItem = DistributedCachingMT.Instance.Get(cacheKey, cacheName);
            Assert.NotNull(cacheItem);
            Assert.IsTrue(cacheItem is string);
            Assert.AreEqual(cacheKey, cacheItem);
        }

        [Test]
        public void TestPutInAndGetFromCacheManyTimes()
        {
            Random r = new Random();
            int numberOfPuts = 1000;
            for (int i = 0; i < numberOfPuts; i++)
            {
                int rInt = r.Next();
                string cacheName = "searchresults";
                string cacheKey = "default" + r;
                //System.Threading.Thread.Sleep(r.Next(500, 750));
                DistributedCachingMT.Instance.Put(cacheKey, cacheKey, cacheName);
                object cacheItem = DistributedCachingMT.Instance.Get(cacheKey, cacheName);
                Assert.NotNull(cacheItem);
                Assert.IsTrue(cacheItem is string);
                Assert.AreEqual(cacheKey, cacheItem);
            }
        }

    }
#endif
}
