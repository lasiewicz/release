﻿namespace Spark.Common.Chat
{
    public enum ChatProvider
    {
        UserPlane = 1,
        Connect = 2,
        Ejabberd = 3,
    }
}