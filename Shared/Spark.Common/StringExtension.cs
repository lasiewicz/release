﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO.Compression;
using System.IO;
using CryptoHelperRSA;
using System.Text.RegularExpressions;

namespace Spark.Common
{
    public static class StringExtension
    {
        public static string Compress(this string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            MemoryStream ms = new MemoryStream();
            using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
            {
                zip.Write(buffer, 0, buffer.Length);
            }

            ms.Position = 0;
            MemoryStream outStream = new MemoryStream();

            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            byte[] gzBuffer = new byte[compressed.Length + 4];
            System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
            return Convert.ToBase64String(gzBuffer);
        }

        public static string Decompress(this string compressedText)
        {
            byte[] gzBuffer = Convert.FromBase64String(compressedText);
            using (MemoryStream ms = new MemoryStream())
            {
                int msgLength = BitConverter.ToInt32(gzBuffer, 0);
                ms.Write(gzBuffer, 4, gzBuffer.Length - 4);

                byte[] buffer = new byte[msgLength];

                ms.Position = 0;
                using (GZipStream zip = new GZipStream(ms, CompressionMode.Decompress))
                {
                    zip.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        public static string Encrypt(this string encryptString)
        {
            RSACryptoServiceProvider provider = new RSACryptoServiceProvider();
            provider.FromXmlString(@"<RSAKeyValue><Modulus>yFQsoH8ld4LlF1T9288lDjSYC4tkJRQJnSABh16p29CdSw1StQkFeGEjPUVombLARCBEAs29DuxAvVmdxhZQYdfQJ6ENbLF+FPo+uvXgxHMa61xvgBNGAwd7pbo/fLrAUXdfqMzonnBAuEWUjcXbtF6odncvJXWgoj+lA9afV80=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
            return CryptoHelper.EncryptRSA(provider, encryptString);
        }


        // 
        public static string Decrypt(this string decryptString)
        {
            RSACryptoServiceProvider provider = new RSACryptoServiceProvider();
            provider.FromXmlString(@"<RSAKeyValue><Modulus>yFQsoH8ld4LlF1T9288lDjSYC4tkJRQJnSABh16p29CdSw1StQkFeGEjPUVombLARCBEAs29DuxAvVmdxhZQYdfQJ6ENbLF+FPo+uvXgxHMa61xvgBNGAwd7pbo/fLrAUXdfqMzonnBAuEWUjcXbtF6odncvJXWgoj+lA9afV80=</Modulus><Exponent>AQAB</Exponent><P>6GtKs9wvXjzJfxuOv3j1zP5kKX9SGoNSTHUo9YHbb/taCkVtTnsJmWJJmd9sQIzHa0BbF3HdUK4oR5R9X58L2Q==</P><Q>3KdjnS9CJlNLBhmjxERNlibSN60DyKW9QQYSuYngd6/H0oOVRj+jZ/nxUFCjWpb9MZ2Ll+vNU/9iq/V+/S33FQ==</Q><DP>0HhsaSf21PBxTDHLuLMmxtjnvtEVM/LWvv3X3167FZa/DLd5dAuocIxYuExPLRcZSHpROpZHjTapqormaf6fAQ==</DP><DQ>r3C4F2ZX5jkb0cHZIFheiCBdzL7X31VIYMb/ZF5oB0HQU2Uj7zLyLd3hLMaw23pp3mUxdJ2cKElKnwcW7R768Q==</DQ><InverseQ>kvfxdXgCRrDAD+WtfK8ZZj32W7KrxYUJlzOIhI3uN2ujbEQ9qilmBbqnnKm5JIzMt1n7iZDqgU6CpAuJ3khV+A==</InverseQ><D>NHyxcJ+/nk+CXJmdJcFhURaAm826wGg+mJdxLxmjYX+IJsjn2ZzyfPbed8g+vr5x16eoih+DoGxQDgxz5Rmll3ZxhF5tFnVQKnh/tetJ2B/pXbDTiTMv8q8NertSpPvpOh7Dx+tCRlaqsRmH+/KPXNcmjoNpJEeo2XrMPiox7KE=</D></RSAKeyValue>");
            return CryptoHelper.DecryptRSA(provider, decryptString);
        }

        public static bool IsValidIP(this string address)
        {
            //create our match pattern
            string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
            //create our Regular Expression object
            Regex check = new Regex(pattern);
            //boolean variable to hold the status
            bool valid = false;
            //check to make sure an ip address was provided
            if (address == "")
            {
                //no address provided so return false
                valid = false;
            }
            else
            {
                //address provided so use the IsMatch Method
                //of the Regular Expression object
                valid = check.IsMatch(address, 0);
            }
            //return the results
            return valid;
        }
    }
}
