﻿#region

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.Common.Security;

#endregion

namespace Spark.Common.Test
{
    [TestClass]
    public class SecurityTest
    {
        [TestMethod]
        public void EncryptAndDecryptString()
        {
            const string value = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit";
            var encryptedString = Encryption.EncryptString(value);
            var decryptedString = Encryption.DecryptString(encryptedString);

            Assert.AreEqual(value, decryptedString);
        }

        [TestMethod]
        public void EncryptAndDecryptUrlSafeString()
        {
            const string value = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit";
            var encryptedString = Encryption.EncryptString(value, true);
            var decryptedString = Encryption.DecryptString(encryptedString, true);

            Assert.AreEqual(value, decryptedString);
        }
    }
}