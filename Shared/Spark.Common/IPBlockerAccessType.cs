﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Common
{
    public enum IPBlockerAccessType
    {
        Registration = 0,
        Subscription = 1
    }
}
