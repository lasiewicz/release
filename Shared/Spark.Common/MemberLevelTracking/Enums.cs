﻿namespace Spark.Common.MemberLevelTracking
{
    /// <summary>
    /// Used by member level tracking
    /// </summary>
    public enum Application
    {
        FullSite = 1,
        MobileOptimizedSite = 2,
        Hybrid = 3,
        Native = 4,
        TabletOptimizedSite = 5
    }

    /// <summary>
    /// 
    /// </summary>
    public enum Key
    {
        TrackingRegApplication,
        TrackingRegOs,
        TrackingRegFormFactor,
        TrackingLastApplication,
        TrackingLastOs,
        TrackingLastFormFactor
    }
}
