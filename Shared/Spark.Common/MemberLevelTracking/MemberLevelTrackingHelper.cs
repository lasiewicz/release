﻿using System.Web;

namespace Spark.Common.MemberLevelTracking
{
    public static class MemberLevelTrackingHelper
    {
        public static string GetOs(HttpRequest request)
        {
            var bc = request.Browser;
            var os = bc.Platform + " " + bc.MajorVersion;

            return os;
        }

        public static string GetFormFactor(HttpRequest request)
        {
            var bc = request.Browser;

            var device = "<DeviceProperties>" +
                         "<IsMobileDevice>" + bc.IsMobileDevice.ToString().ToLower() + "</IsMobileDevice>" +
                         "<IsTablet>" + (bc["is_tablet"] == null ? "false": bc["is_tablet"].ToLower()) + "</IsTablet>" +
                         "<Manufacturer>" + bc.MobileDeviceManufacturer + "</Manufacturer>" +
                         "<Model>" + bc.MobileDeviceModel + "</Model>" +
                         "<ScreenPixelsHeight>" + bc.ScreenPixelsHeight + "</ScreenPixelsHeight>" +
                         "<ScreenPixelsWidth>" + bc.ScreenPixelsWidth + "</ScreenPixelsWidth>" +
                         "</DeviceProperties>";

            return device;
        }
    }
}
