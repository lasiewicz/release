﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Spark.Common.Fraud
{
    public class RegistrationRiskInquiryData
    {
        private const string REG_DEFAULT_STRING = "REG";
        // REQUIRED FIELDS TO RIS 
        public bool IsAuthorized { get; set; } // AUTH 
        public string Currency { get; set; } // CURR 
        public string Email { get; set; } // EMAL
        public string IPAddress { get; set; } // IPAD
        public bool IsMerchantAcknowledgementOn { get; set; } // MACK 
        public string MerchantID { get; set; } // MERC
        public string Mode { get; set; } // MODE

        public List<RiskInquiryPackage> Packages { get; set; }

        public string PaymentToken { get; set; } // PTOK 
        public string PaymentType { get; set; } // PTYP
        public string SessionID { get; set; } // SESS
        public string SiteID { get; set; } // SITE
        public decimal OrderTotal { get; set; } // TOTL  
        public string Version { get; set; } // VERS
        public string CustomerID { get; set; } // CUSTOMER_ID

        // OPTIONAL FIELDS TO RIS 
        //Mapping to shipping address 
        public string StreetAddressLine1 { get; set; } // S2A1
        public string StreetAddressLine2 { get; set; } // S2A2
        public string AddressCountry { get; set; } // S2CC 
        public string AddressCity { get; set; } // S2CI
        public string AddressPostalCode { get; set; } //S2PC
        public string AddressState { get; set; } // S2ST
        public string DateOfBirth { get; set; } // DOB
        public string DateOfRegistration { get; set; } // EPOC
        public string Gender { get; set; } // GENDER

        public string Name { get; set; } // NAME

        public string MerchantAccountNumber { get; set; } // UNIQ

        // EXTRA DATA FOR LOGGING ONLY 
        public string CallingSystemTypeID { get; set; }
        public string RiskInquiryTypeID { get; set; }

        #region UDF

        public string UserName { get; set; }
        public string Age { get; set; }
        public string MaritalStatus { get; set; }
        public string Occupation { get; set; }
        public string Education { get; set; }
        public string Religion { get; set; }
        public string Ethnicity { get; set; }
        public string AboutMe { get; set; }
        public string PromotionID { get; set; }
        public string Eyes { get; set; }
        public string Hair { get; set; }
        public string Height { get; set; }

        #endregion

        public RegistrationRiskInquiryData()
        {
            IsAuthorized = true;
            //TODO: check for right mapping
            IsMerchantAcknowledgementOn = true;
            Mode = "Q";      //Convert.ToString(FraudConstants.INITIAL_QUERY_MODE);
            RiskInquiryTypeID = "Registration"; //
            PaymentToken = null;
            PaymentType = "NONE";
            OrderTotal = 0;
            Currency = "USD";
            StreetAddressLine1 = string.Empty;
            StreetAddressLine2 = string.Empty;
            Packages = new List<RiskInquiryPackage>();
            //As package information is not applicable for registration
            //Sending a fake package to avoid kount errors
            IntializePackage();
            InitializeUDFs();
        }

        private void InitializeUDFs()
        {
            UserName = "";
            Age = "";
            MaritalStatus = "";
            Occupation = "";
            Education = "";
            Religion = "";
            Ethnicity = "";
            AboutMe = "";
            PromotionID = "";
            Eyes = "";
            Hair = "";
            Height = "";
        }

        private void IntializePackage()
        {
            RiskInquiryPackage package = new RiskInquiryPackage();
            package.PackageDescription = REG_DEFAULT_STRING;
            package.PackageID = "0";
            package.PackagePrice = 0;
            package.PackageQuantity = 1;
            package.PackageType = REG_DEFAULT_STRING;

            this.Packages.Add(package);

        }

        public string SerializeRiskInquiryData
        {
            get
            {
                string serializedRequestData = string.Empty;

                try
                {
                    serializedRequestData = JSONHelper.Serialize<RegistrationRiskInquiryData>(this);
                }
                catch (SerializationException serex)
                {

                }


                return serializedRequestData;
            }
        }

        public static RegistrationRiskInquiryData DeserializeRiskInquiryData(string serializedRequestData)
        {
            RegistrationRiskInquiryData requestData = null;

            try
            {
                requestData = JSONHelper.Deserialize<RegistrationRiskInquiryData>(serializedRequestData);
            }
            catch (SerializationException serex)
            {

            }

            return requestData;
        }
    }

    public class RiskInquiryPackage
    {
        public string PackageDescription { get; set; } // PROD_DESC
        public string PackageID { get; set; } // PROD_ITEM
        public decimal PackagePrice { get; set; } // PROD_PRICE
        public long PackageQuantity { get; set; } // PROD_QUANT
        public string PackageType { get; set; } // PROD_TYPE
    }

    public class JSONHelper
    {
        public static string Serialize<T>(T obj)
        {
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.Default.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }
    }
}
