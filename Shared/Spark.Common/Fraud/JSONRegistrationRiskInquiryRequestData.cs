﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Spark.Common.Fraud
{
    public class JSONRegistrationRiskInquiryRequestData
    {
        public string registrationRiskInquiryRequestData { get; set; }

        public string SerializeJSONRegistrationRiskInquiryRequestData
        {
            get
            {
                string serializedRequestData = string.Empty;

                try
                {
                    serializedRequestData = JSONHelper.Serialize(this);
                }
                catch (SerializationException serex)
                {
                    //
                }

                return serializedRequestData;
            }
        }

        public static JSONRegistrationRiskInquiryRequestData DeserializeJSONRegistrationRiskInquiryRequestData(string serializedRequestData)
        {
            JSONRegistrationRiskInquiryRequestData requestData = null;

            try
            {
                requestData = JSONHelper.Deserialize<JSONRegistrationRiskInquiryRequestData>(serializedRequestData);
            }
            catch (SerializationException serex)
            {
                //
            }

            return requestData;
        }
    }
}
