﻿using System;
using System.Collections.Generic;

namespace Spark.Common.UPS
{
    #region Payment JSON Classes

    /// <summary>
    /// Used to exchanging information between UPS and Bedrock.
    /// </summary>
    [Serializable]
    public class PaymentJson
    {
        #region Private Variables

        private PaymentJsonData data = new PaymentJsonData();

        #endregion

        #region Public Variables

        public int UPSLegacyDataID { get; set; }

        public int UPSGlobalLogID { get; set; }

        public int GetCustomerPaymentProfile { get; set; }

        public int Version { get; set; }

        public int CallingSystemID { get; set; }

        public DateTime TimeStamp { get; set; }

        public bool IsSubscriptionConfirmationEmailEnabled { get; set; }

        public PaymentJsonData Data
        {
            get { return data; }
            set { data = value; }
        }

        public int TemplateID { get; set; }

        public string KountMerchantID { get; set; }
        public bool IsUPSConfirm { get; set; }

        #endregion
    }

    public class PaymentJsonData
    {
        private PaymentJsonNavigation navigation = new PaymentJsonNavigation();
        private PaymentJsonMemberInfo memberInfo = new PaymentJsonMemberInfo();
        private PaymentLegacyDataInfo legacyDataInfo = new PaymentLegacyDataInfo();
        private PaymentJsonPackages packages = new PaymentJsonPackages();
        private List<PaymentJsonArray> colHeaderTemplates = new List<PaymentJsonArray>();
        private List<PaymentJsonArray> rowHeaderTemplates = new List<PaymentJsonArray>();
        private String orderAttributes = String.Empty;
        private List<PaymentJsonArray> promoInfo = new List<PaymentJsonArray>();
        private List<PaymentJsonArray> omnitureVariables = new List<PaymentJsonArray>();
        private List<PaymentJsonArray> analyticVariables = new List<PaymentJsonArray>();
        private List<PaymentJsonArray> pageInfo = new List<PaymentJsonArray>();
        private List<PaymentJsonArray> orderConfirmation = new List<PaymentJsonArray>();
        private string affiliateTrackingPixel = String.Empty;

        public PaymentJsonNavigation Navigation
        {
            get { return navigation; }
            set { navigation = value; }
        }

        public PaymentJsonMemberInfo MemberInfo
        {
            get { return memberInfo; }
            set { memberInfo = value; }
        }

        public PaymentLegacyDataInfo LegacyDataInfo
        {
            get { return legacyDataInfo; }
            set { legacyDataInfo = value; }
        }

        public PaymentJsonPackages Packages
        {
            get { return packages; }
            set { packages = value; }
        }

        public List<PaymentJsonArray> ColHeaderTemplates
        {
            get { return colHeaderTemplates; }
            set { colHeaderTemplates = value; }
        }

        public List<PaymentJsonArray> RowHeaderTemplates
        {
            get { return rowHeaderTemplates; }
            set { rowHeaderTemplates = value; }
        }

        public String OrderAttributes
        {
            get { return orderAttributes; }
            set { orderAttributes = value; }
        }

        public List<PaymentJsonArray> PromoInfo
        {
            get { return promoInfo; }
            set { promoInfo = value; }
        }

        public List<PaymentJsonArray> OmnitureVariables
        {
            get { return omnitureVariables; }
            set { omnitureVariables = value; }
        }

        public List<PaymentJsonArray> AnalyticVariables
        {
            get { return analyticVariables; }
            set { analyticVariables = value; }
        }

        public List<PaymentJsonArray> PageInfo
        {
            get { return pageInfo; }
            set { pageInfo = value; }
        }

        public List<PaymentJsonArray> OrderConfirmation
        {
            get { return orderConfirmation; }
            set { orderConfirmation = value; }
        }

        public string AffiliateTrackingPixel
        {
            get { return affiliateTrackingPixel; }
            set { affiliateTrackingPixel = value; }
        }
    }

    public class PaymentJsonHeaderTemplate : PaymentJsonArray
    {

    }

    public class PaymentJsonPackages : List<PaymentJsonPackage>
    {
    }

    public class PaymentJsonNavigation
    {
        /// <summary>
        ///  AKA Back URL
        /// </summary>
        public string CancelURL { get; set; }

        public string ReturnURL { get; set; }

        public string ConfirmationURL { get; set; }

        public string DestinationURL { get; set; }
    }

    public class PaymentJsonMemberInfo
    {
        public int RegionID { get; set; }

        public int CustomerID { get; set; }

        public string Language { get; set; }

        public string State { get; set; }

        public string NextRenewalDate { get; set; }

        public Decimal MonthlyRenewalRate { get; set; }

        public string IPAddress { get; set; }

        public string EmailAddress { get; set; }

        public string GenderMask { get; set;} 

        public DateTime BirthDate { get; set; }

        public DateTime BrandInsertDate { get; set; }

        //JS-1228 UDF's for Kount
        public string UserName { get; set; }
        public string MaritalStatus { get; set; }
        public string Occupation { get; set; }
        public string Education { get; set; }
        public string Religion { get; set; }
        public string Ethnicity { get; set; }
        public string AboutMe { get; set; }
        public string PromotionID { get; set; }
        public string Eyes { get; set; }
        public string Hair { get; set; }
        public string Height { get; set; }

    }

    /// <summary>
    /// These are fields required by Purchase when compeleting a transaction. New Payment will call Purchase 
    /// retrieve this information back from Bedrock.
    /// </summary>
    public class PaymentLegacyDataInfo
    {
        public int Member { get; set; }
        public int AdminMemberID { get; set; }
        public int PlanID { get; set; }
        public int DisountID { get; set; }
        public int SiteID { get; set; }
        public int BrandID { get; set; }
        public int ConversionMemberID { get; set; }
        public int SourceID { get; set; }
        public int PurchaseReasonTypeID { get; set; }
        public int UICreditAmount { get; set; }
        public int PurchaseMode { get; set; }
        public int ReUsePreviousPayment { get; set; }
        public int PromoID { get; set; }
    }

    public class PaymentJsonPackage
    {
        private PaymentJsonItems items = new PaymentJsonItems();

        public int ID { get; set; }

        public string Description { get; set; }

        public int SytemID { get; set; }

        public int Status { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime ExpiredDate { get; set; }

        public string CurrencyType { get; set; }

        public PaymentJsonItems Items
        {
            get { return items; }
            set { items = value; }
        }
    }

    public class PaymentJsonItems : PaymentJsonArray
    {
    }

    public class PaymentJsonArray : Dictionary<string, object>
    {

    }

    #endregion
}
