﻿using System;
using System.Net;
using System.Web;
using System.Collections;
using System.IO;

namespace Spark.Common
{
    public class WebPostRequestUtility
    {
        WebRequest theRequest;
        HttpWebResponse theResponse;
        ArrayList theQueryData;

        public WebPostRequestUtility(string url, string method, int timeout)
        {
            theRequest = WebRequest.Create(url);
            theRequest.Method = method;
            theRequest.Timeout = timeout;
            theQueryData = new ArrayList();
        }

        public void Add(string key, string value)
        {
            theQueryData.Add(String.Format("{0}={1}", key, HttpUtility.UrlEncode(value)));
        }

        public string GetResponse()
        {
            // Set the encoding type
            theRequest.ContentType = "application/x-www-form-urlencoded";

            // Build a string containing all the parameters
            string Parameters = String.Join("&", (String[])theQueryData.ToArray(typeof(string)));
            theRequest.ContentLength = Parameters.Length;

            // We write the parameters into the request
            StreamWriter sw = new StreamWriter(theRequest.GetRequestStream());
            sw.Write(Parameters);
            sw.Close();

            // Execute the query
            theResponse = (HttpWebResponse)theRequest.GetResponse();
            StreamReader sr = new StreamReader(theResponse.GetResponseStream());
            return sr.ReadToEnd();
        }

        public Stream GetResponseStream()
        {
            // Set the encoding type
            theRequest.ContentType = "application/x-www-form-urlencoded";

            // Build a string containing all the parameters
            string Parameters = String.Join("&", (String[])theQueryData.ToArray(typeof(string)));
            theRequest.ContentLength = Parameters.Length;

            // We write the parameters into the request
            StreamWriter sw = new StreamWriter(theRequest.GetRequestStream());
            sw.Write(Parameters);
            sw.Close();

            // Execute the query
            theResponse = (HttpWebResponse)theRequest.GetResponse();
            return theResponse.GetResponseStream();
        }

        public string GetGETResponse()
        {
            string returnString = string.Empty;
            HttpWebResponse response = null;
            StreamReader reader = null;
            theRequest.Timeout = 7000;

            try
            {
                response = (HttpWebResponse)theRequest.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                returnString = reader.ReadToEnd();
            }
            finally
            {
                if (response != null)
                    response.Close();
                if (reader != null)
                    reader.Close();
            }

            return returnString;
        }
    }
}
