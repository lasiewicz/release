﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Routing;
using RestSharp;
using RestSharp.Deserializers;

namespace Spark.Common.Adapter
{
    [DataContract]
    public enum IAPControllerStatus
    {
        [EnumMember] None = 0,
        [EnumMember] PurchaseOrder = 1,
        [EnumMember] RenewOrder = 2,
        [EnumMember] TerminateRenewal = 3,
        [EnumMember] VoidPreviousOrder = 4,
        [EnumMember] InvalidReceipt = 5
    }

    public class APIAdapter
    {
        public static readonly APIAdapter Singleton = new APIAdapter();

        // Multiton pattern. Not a static variable since contained in a singleton
        private readonly ConcurrentDictionary<string, RestClient> _restClients =
            new ConcurrentDictionary<string, RestClient>();

        // Safe way to get an instance of RestClient to use. Should only be accessed through Singleton.
        // Not a static variable since contained in a singleton
        public RestClient GetRestClient(string baseUrl)
        {
            RestClient restClient;
            if (_restClients.TryGetValue(baseUrl, out restClient)) return restClient;
            restClient = new RestClient(baseUrl);
            _restClients.TryAdd(baseUrl, restClient);
            return restClient;
        }

        // For singleton support
        private APIAdapter()
        {
            
        }

        public RestStatus SendApiRequest<T>(string apiUrl, string path, string method,
            Dictionary<string, string> urlSegments, Dictionary<string, string> urlParams, object requestBody,
            out T returnObject, string responseContentType=null, int requestTimeout = 0, bool useResponseWrapper = false, bool ignoreSSLErrors = false) where T : new()
        {
            try
            {
                returnObject = default(T);
                Method meth = (Method) Enum.Parse(typeof (Method), method);

                var client = Singleton.GetRestClient(apiUrl);
                var request = new RestRequest(path, meth);

                if (!string.IsNullOrEmpty(responseContentType))
                {
                    //set the response content type explicity in case api doesn't return in header
                    request.OnBeforeDeserialization = resp => { resp.ContentType = responseContentType; };
                }

                if (requestTimeout != 0)
                {
                    client.Timeout = requestTimeout;
                    client.ReadWriteTimeout = requestTimeout;
                }

                // ignore ssl errors from using self ssl in dev/stage envs.
                if (ignoreSSLErrors)
                {
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                }

                //todo -  do we need to really pass the appopriate value? the scope is platform wide. same goes for appid and clientsecret
                if (null != urlSegments)
                {
                    foreach (string key in urlSegments.Keys)
                    {
                        request.AddUrlSegment(key, urlSegments[key]);
                    }
                }

                if (null != urlParams)
                {
                    foreach (string key in urlParams.Keys)
                    {
                        request.AddParameter(key, urlParams[key]);
                    }
                }

                if (null != requestBody)
                {
                    request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                    request.RequestFormat = DataFormat.Json;
                    request.AddBody(requestBody);
                }

                var errorMessage = string.Empty;
                var status = HttpStatusCode.OK;
                var timedOut = false;
                var responseStatus = ResponseStatus.None;
                var rawContent = String.Empty;
                if (useResponseWrapper)
                {
                    var response = client.Execute<ResponseWrapper<T>>(request) as RestResponse<ResponseWrapper<T>>;
                    // Null response
                    if (response != null)
                    {
                        status = response.StatusCode;
                        responseStatus = response.ResponseStatus;
                    }

                    if (response != null && response.Data.Data != null)
                    {
                        returnObject = response.Data.Data;
                        return new RestStatus { ValidResponse = true, StatusCode = response.StatusCode };
                    }
                    if (response != null && !string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        errorMessage = response.ErrorMessage;
                        rawContent = response.Content;
                    }
                }
                else
                {
                    var response = client.Execute<T>(request) as RestResponse<T>;

                    // Null response
                    if (response != null)
                    {
                        status = response.StatusCode;
                        responseStatus = response.ResponseStatus;
                    }

                    if (response != null && response.Data != null)
                    {
                        returnObject = response.Data;
                        return new RestStatus {ValidResponse = true, StatusCode = response.StatusCode};
                    }

                    if (response !=  null && !string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        errorMessage = response.ErrorMessage;
                        rawContent = response.Content;
                    }
                }

                if ((errorMessage != string.Empty && errorMessage.ToLower().IndexOf("timeout") > -1) || responseStatus == ResponseStatus.TimedOut)
                {
                    timedOut = true;
                }

                
                return new RestStatus
                {
                    ValidResponse = false,
                    StatusCode = status,
                    ErrorMessage = errorMessage,
                    TimedOut = timedOut,
                    Content = rawContent
                };
            }
            catch (Exception exception)
            {
                throw new Exception("Error calling the API.", exception);
            }
        }

        public class RestStatus
        {
            public bool ValidResponse { get; set; }
            public HttpStatusCode StatusCode { get; set; }
            public string ErrorMessage { get; set; }
            public bool TimedOut { get; set; }
            public string Content { get; set; }
        }

        public class ResponseWrapper<T>
        {
            public int Code { get; set; }
            public T Data { get; set; }
            public string Status { get; set; }
        }
    }
}
