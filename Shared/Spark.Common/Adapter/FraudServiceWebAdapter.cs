﻿using System;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.FraudService;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// 
    /// </summary>
    public class FraudServiceWebAdapter
    {
        /// <summary>
        /// Gets proxy assuming client has the endpoing (with url) and binding configurations in their configuration file
        /// </summary>
        /// <returns></returns>
        public static FraudServiceClient GetProxyInstance()
        {
            FraudServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["FraudServiceClient"] != null)
                {
                    _Client = (FraudServiceClient)HttpContext.Current.Items["FraudServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new FraudServiceClient();
                        HttpContext.Current.Items["FraudServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new FraudServiceClient();
                        HttpContext.Current.Items["FraudServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new FraudServiceClient();
                    HttpContext.Current.Items["FraudServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static FraudServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            FraudServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["FraudServiceClient"] != null)
                {
                    _Client = (FraudServiceClient)HttpContext.Current.Items["FraudServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new FraudServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["FraudServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new FraudServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["FraudServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new FraudServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["FraudServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public static FraudServiceClient GetProxyInstanceForBedrock()
        {
            FraudServiceClient _Client = null;
            try
            {
                string FraudServiceURL = RuntimeSettings.GetSetting("UPS_FRAUD_SERVICE_URL");
                _Client = GetProxyInstance(FraudServiceURL, "BasicHttpBinding_IFraudService");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }


        public static void CloseProxyInstance()
        {
            FraudServiceClient _Client;
            if (HttpContext.Current.Items["FraudServiceClient"] != null)
            {
                _Client = (FraudServiceClient)HttpContext.Current.Items["FraudServiceClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    HttpContext.Current.Items.Remove("FraudServiceClient");
                }
            }
        }
    }
}
