﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.OrderHistoryService;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Order History Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: instance; which means the Service Manager needs to instantiate this adapter and call its closeProxyInstance()
    /// at the end of the SM process.
    /// </summary>
    public class OrderHistoryServiceAdapter
    {
        private OrderHistoryManagerClient _Client = null;

        public OrderHistoryManagerClient GetProxyInstance()
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new OrderHistoryManagerClient();
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new OrderHistoryManagerClient();
                    }
                }
                else
                {
                    _Client = new OrderHistoryManagerClient();
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public OrderHistoryManagerClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new OrderHistoryManagerClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new OrderHistoryManagerClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                }
                else
                {
                    _Client = new OrderHistoryManagerClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public OrderHistoryManagerClient GetProxyInstanceForBedrock()
        {
            try
            {
                string ServiceURL = RuntimeSettings.GetSetting("UPS_ORDERHISTORY_SERVICE_URL");
                _Client = GetProxyInstance(ServiceURL, "BasicHttpBinding_IOrderHistoryManager");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public void CloseProxyInstance()
        {
            if (_Client != null)
            {
                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
            }
        }
    }
}
