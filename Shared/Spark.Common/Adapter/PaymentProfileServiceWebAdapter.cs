﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.PaymentProfileService;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Payment Profile Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: Static; this adapter does not need to be instantiated, but it's CloseProxyInstance() should be called at the 
    /// end of the web request (e.g. in Global.asax: Application_EndRequest)
    /// </summary>
    public class PaymentProfileServiceWebAdapter
    {

        public static PaymentProfileMapperClient GetProxyInstance()
        {
            return GetProxyInstance(new HttpContextWrapper(HttpContext.Current));
        }

        public static PaymentProfileMapperClient GetProxyInstance(HttpContextBase context)
        {
            PaymentProfileMapperClient _Client = null;
            try
            {
                if (context.Items["PaymentProfileMapperClient"] != null)
                {
                    _Client = (PaymentProfileMapperClient)context.Items["PaymentProfileMapperClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new PaymentProfileMapperClient();
                        context.Items["PaymentProfileMapperClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new PaymentProfileMapperClient();
                        context.Items["PaymentProfileMapperClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new PaymentProfileMapperClient();
                    context.Items["PaymentProfileMapperClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;
        }

        public static PaymentProfileMapperClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            PaymentProfileMapperClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["PaymentProfileMapperClient"] != null)
                {
                    _Client = (PaymentProfileMapperClient)HttpContext.Current.Items["PaymentProfileMapperClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new PaymentProfileMapperClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["PaymentProfileMapperClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new PaymentProfileMapperClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["PaymentProfileMapperClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new PaymentProfileMapperClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["PaymentProfileMapperClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static PaymentProfileMapperClient GetProxyInstanceForBedrock()
        {
            PaymentProfileMapperClient _Client = null;
            try
            {
                string ServiceURL = RuntimeSettings.GetSetting("UPS_PAYMENT_PROFILE_SERVICE_URL");
                _Client = GetProxyInstance(ServiceURL, "BasicHttpBinding_IPaymentProfileMapper");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static void CloseProxyInstance()
        {
            CloseProxyInstance(new HttpContextWrapper(HttpContext.Current));
        }

        public static void CloseProxyInstance(HttpContextBase context)
        {
            PaymentProfileMapperClient _Client;
            if (context.Items["PaymentProfileMapperClient"] != null)
            {
                _Client = (PaymentProfileMapperClient)context.Items["PaymentProfileMapperClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    context.Items.Remove("PaymentProfileMapperClient");
                }
            }
        }
    }
}
