﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.AccessService;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Access Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: instance; which means the Service Manager needs to instantiate this adapter and call its closeProxyInstance()
    /// at the end of the SM process.
    /// </summary>
    public class AccessServiceAdapter : IAccessServiceAdapter
    {
        private AccessServiceClient _Client = null;

        public AccessServiceClient GetProxyInstance()
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new AccessServiceClient();
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new AccessServiceClient();
                    }
                }
                else
                {
                    _Client = new AccessServiceClient();
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public AccessServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new AccessServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new AccessServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                }
                else
                {
                    _Client = new AccessServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public AccessServiceClient GetProxyInstanceForBedrock()
        {
            try
            {
                string AccessServiceURL = RuntimeSettings.GetSetting("UPS_ACCESS_SERVICE_URL");
                _Client = GetProxyInstance(AccessServiceURL, "BasicHttpBinding_IAccessService");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// Since there are many ways to obtain the client object, this method is going to assume the caller already set it.
        /// If not set, it will use GetProxyInstanceForBedrock() to obtain it.
        /// </summary>
        /// <param name="customerID"></param>
        /// <param name="privilegeTypeID"></param>
        /// <param name="count"></param>
        /// <param name="AdminID"></param>
        /// <param name="AdminUserName"></param>
        /// <param name="AccessReasonID"></param>
        /// <param name="callingSystemID"></param>
        /// <param name="callingSystemTypeID"></param>
        /// <returns>AccessResponse object from calling Access</returns>
        public AccessResponse AdjustCountPrivilege(int customerID, int[] privilegeTypeID, int count, int AdminID, string AdminUserName, int AccessReasonID, int callingSystemID, int callingSystemTypeID)
        {
            if(_Client == null)
            {
                GetProxyInstanceForBedrock();
            }

            return _Client.AdjustCountPrivilege(customerID, privilegeTypeID, count, AdminID, AdminUserName, AccessReasonID, callingSystemID, callingSystemTypeID);
        }

        public void CloseProxyInstance()
        {
            if (_Client != null)
            {
                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
            }
        }
    }
}
