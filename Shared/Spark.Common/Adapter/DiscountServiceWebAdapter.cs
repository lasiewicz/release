﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.DiscountService;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{


    /// <summary>
    /// Adapter for interfacing with Access Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: Static; this adapter does not need to be instantiated, but it's CloseProxyInstance() should be called at the 
    /// end of the web request (e.g. in Global.asax: Application_EndRequest)
    /// </summary>
    public class DiscountServiceWebAdapter
    {
        /// <summary>
        /// Gets proxy assuming client has the endpoing (with url) and binding configurations in their configuration file
        /// </summary>
        /// <returns></returns>
        public static DiscountServiceClient GetProxyInstance()
        {
            DiscountServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["DiscountServiceClient"] != null)
                {
                    _Client = (DiscountServiceClient)HttpContext.Current.Items["DiscountServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new DiscountServiceClient();
                        HttpContext.Current.Items["DiscountServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new DiscountServiceClient();
                        HttpContext.Current.Items["DiscountServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new DiscountServiceClient();
                    HttpContext.Current.Items["DiscountServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static DiscountServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            DiscountServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["DiscountServiceClient"] != null)
                {
                    _Client = (DiscountServiceClient)HttpContext.Current.Items["DiscountServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new DiscountServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["DiscountServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new DiscountServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["DiscountServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new DiscountServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["DiscountServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public static DiscountServiceClient GetProxyInstanceForBedrock()
        {
            DiscountServiceClient _Client = null;
            try
            {
                string AccessServiceURL = RuntimeSettings.GetSetting("UPS_DISCOUNT_SERVICE_URL");
                _Client = GetProxyInstance(AccessServiceURL, "BasicHttpBinding_IDiscountService");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }


        public static void CloseProxyInstance()
        {
            DiscountServiceClient _Client;
            if (HttpContext.Current.Items["DiscountServiceClient"] != null)
            {
                _Client = (DiscountServiceClient)HttpContext.Current.Items["DiscountServiceClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    HttpContext.Current.Items.Remove("DiscountServiceClient");
                }
            }
        }
    }
}
