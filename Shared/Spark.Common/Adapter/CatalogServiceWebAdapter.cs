﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.CatalogService;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Catalog Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: Static; this adapter does not need to be instantiated, but it's CloseProxyInstance() should be called at the 
    /// end of the web request (e.g. in Global.asax: Application_EndRequest)
    /// </summary>
    public class CatalogServiceWebAdapter
    {
        /// <summary>
        /// Gets proxy assuming client has the endpoing (with url) and binding configurations in their configuration file
        /// </summary>
        /// <returns></returns>
        public static CatalogServiceClient GetProxyInstance()
        {
            return GetProxyInstance(new HttpContextWrapper(HttpContext.Current));
        }

        public static CatalogServiceClient GetProxyInstance(HttpContextBase context)
        {
            CatalogServiceClient _Client = null;
            try
            {                
                if (context.Items["CatalogServiceClient"] != null)
                {
                    _Client = (CatalogServiceClient)context.Items["CatalogServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new CatalogServiceClient();
                        context.Items["CatalogServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new CatalogServiceClient();
                        context.Items["CatalogServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new CatalogServiceClient();
                    context.Items["CatalogServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static CatalogServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            CatalogServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["CatalogServiceClient"] != null)
                {
                    _Client = (CatalogServiceClient)HttpContext.Current.Items["CatalogServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new CatalogServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["CatalogServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new CatalogServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["CatalogServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new CatalogServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["CatalogServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public static CatalogServiceClient GetProxyInstanceForBedrock()
        {
            CatalogServiceClient _Client = null;
            try
            {
                string serviceURL = RuntimeSettings.GetSetting("UPS_CATALOG_SERVICE_URL");
                _Client = GetProxyInstance(serviceURL, "BasicHttpBinding_ICatalogService");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }


        public static void CloseProxyInstance()
        {
            CloseProxyInstance(new HttpContextWrapper(HttpContext.Current));
        }

        public static void CloseProxyInstance(HttpContextBase context)
        {
            CatalogServiceClient _Client;
            if (context.Items["CatalogServiceClient"] != null)
            {
                _Client = (CatalogServiceClient)context.Items["CatalogServiceClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    context.Items.Remove("CatalogServiceClient");
                }
            }
        }
    }
}
