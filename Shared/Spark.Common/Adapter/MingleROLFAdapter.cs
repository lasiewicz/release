﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    public class MingleROLFAdapter
    {
        private const string STATUS = "active";
        private const string RETURN_STATUS_PASSED = "OK";
        private const string RETURN_STATUS_FAILED = "BAD";
        private const string RETURN_STATUS_ERROR = "ERROR";
        private string _ROLFURL;
        private string _ROLFKey;
        private int _memberID;
        private int _brandID;
        private int _siteID;
        private int _communityID;
        
        public int RegIP { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int GenderMask { get; set; }
        public string MaritalStatus { get; set; }
        public string PRM { get; set; }
        public string LGID { get; set; }
        public string PID { get; set; }
        public string EmailAddress { get; set; }
        public List<string> Ethnicity { get; set; }
        public string Religion { get; set; }
        public string ColorCode { get; set; }
        public string IOBlackBoxValue { get; set; }

        public MingleROLFAdapter(int memberID, int brandID, int siteID, int communityID, string emailAddress)
        {
            _ROLFURL = RuntimeSettings.GetSetting("MINGLE_ROLF_URL");
            _ROLFKey = RuntimeSettings.GetSetting("MINGLE_ROLF_KEY");
            _memberID = memberID;
            _brandID = brandID;
            _siteID = siteID;
            _communityID = communityID;
            EmailAddress = emailAddress;
        }

        public ROLFResponse GetROLFResponse()
        {
            string webResponse = string.Empty;
            ROLFResponse response = null;

            bool makeROLFCall = Convert.ToBoolean(RuntimeSettings.GetSetting("MINGLE_ROLF_ENABLE_CALL", _communityID, _siteID, _brandID));
            if (!makeROLFCall)
            {
                response = new ROLFResponse();
                response.Code = 9999;
                response.Description = "CALL NOT ENABLED";
                response.Passed = true;
                response.Error = false;
                return response;
            }

            WebPostRequest request = new WebPostRequest(_ROLFURL, "POST", 2000);
            request.Add("key", _ROLFKey);
            request.Add("memberid", _memberID.ToString());
            request.Add("bhreportingsiteid", _siteID.ToString());
            request.Add("profile_status", STATUS);
            request.Add("reg_ip_int", RegIP.ToString());
            request.Add("reg_location_country", Country);
            request.Add("reg_location_postalcode", PostalCode);
            request.Add("reg_firstname", FirstName);
            request.Add("reg_lastname", LastName);
            request.Add("reg_gender", GetGender(GenderMask));
            request.Add("reg_marital_status", MaritalStatus);
            request.Add("campaign_category", string.Empty);
            request.Add("campaign_source", PRM);
            request.Add("campaign_adid", string.Empty);
            request.Add("campaign_optional", LGID);
            request.Add("campaign_pid", PID);
            request.Add("reg_emailaddress", EmailAddress);
            request.Add("reg_religion", Religion);
            request.Add("reg_ethnicity", GetDelimitedEthnicityValue(Ethnicity));
            request.Add("reg_colorcode", ColorCode);
            request.Add("reg_iovation_bb", IOBlackBoxValue);

            try
            {
                webResponse = request.GetResponse();
                response = ParseWebResponseIntoROLFResponse(webResponse);
                
            }
            catch
            {
                response = new ROLFResponse();
                response.Error = true;
                response.Passed = true;
                response.Code = Constants.NULL_INT;
                response.Description = "Error calling ROLF";
            }

            return response;

        }

        private string GetGender(int genderMask)
        {
            if ((genderMask & 1) == 1)
                return "M";
            else
                return "F";
        }

        private string GetDelimitedEthnicityValue(List<string> ethnicites)
        {
            if (ethnicites.Count == 1)
            {
                return ethnicites[0];
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (string ethnicity in ethnicites)
                {
                    if (ethnicity.ToLower().Trim().Equals("_blank")) continue;
                    if (sb.Length == 0)
                    {
                        sb.Append(ethnicity);
                    }
                    else
                    {
                        sb.Append("|" + ethnicity);
                    }
                }

                return sb.ToString();
            }
        }

        private ROLFResponse ParseWebResponseIntoROLFResponse(string webResponse)
        {
            ROLFResponse response = new ROLFResponse();
            string[] tokens = webResponse.Split(':');

            switch (tokens[0])
            {
                case RETURN_STATUS_PASSED:
                    response.Error = false;
                    response.Passed = true;
                    response.Code = int.Parse(tokens[1]);
                    response.Description = tokens[2];
                    break;
                case RETURN_STATUS_FAILED:
                    response.Error = false;
                    response.Passed = false;
                    response.Code = int.Parse(tokens[1]);
                    response.Description = tokens[2];
                    break;
                case RETURN_STATUS_ERROR:
                    response.Error = true;
                    response.Passed = false;
                    response.ErrorDescription = tokens[1];
                    break;
            }

            return response;
        }

    }

    public class ROLFResponse
    {
        public bool Passed { get; set; }
        public bool Error { get; set; }
        public string ErrorDescription { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
    }

    public class WebPostRequest
    {
        WebRequest theRequest;
        HttpWebResponse theResponse;
        ArrayList theQueryData;

        public WebPostRequest(string url, string method, int timeout)
        {
            theRequest = WebRequest.Create(url);
            theRequest.Method = method;
            theRequest.Timeout = timeout;
            theQueryData = new ArrayList();
        }

        public void Add(string key, string value)
        {
            theQueryData.Add(String.Format("{0}={1}", key, HttpUtility.UrlEncode(value)));
        }

        public string GetResponse()
        {
            // Set the encoding type
            theRequest.ContentType = "application/x-www-form-urlencoded";

            // Build a string containing all the parameters
            string Parameters = String.Join("&", (String[])theQueryData.ToArray(typeof(string)));
            theRequest.ContentLength = Parameters.Length;

            // We write the parameters into the request
            StreamWriter sw = new StreamWriter(theRequest.GetRequestStream());
            sw.Write(Parameters);
            sw.Close();

            // Execute the query
            theResponse = (HttpWebResponse)theRequest.GetResponse();
            StreamReader sr = new StreamReader(theResponse.GetResponseStream());
            return sr.ReadToEnd();
        }

        public Stream GetResponseStream()
        {
            // Set the encoding type
            theRequest.ContentType = "application/x-www-form-urlencoded";

            // Build a string containing all the parameters
            string Parameters = String.Join("&", (String[])theQueryData.ToArray(typeof(string)));
            theRequest.ContentLength = Parameters.Length;

            // We write the parameters into the request
            StreamWriter sw = new StreamWriter(theRequest.GetRequestStream());
            sw.Write(Parameters);
            sw.Close();

            // Execute the query
            theResponse = (HttpWebResponse)theRequest.GetResponse();
            return theResponse.GetResponseStream();
        }

        public string GetGETResponse()
        {
            string returnString = string.Empty;
            HttpWebResponse response = null;
            StreamReader reader = null;
            theRequest.Timeout = 7000;

            try
            {
                response = (HttpWebResponse)theRequest.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                returnString = reader.ReadToEnd();
            }
            finally
            {
                if (response != null)
                    response.Close();
                if (reader != null)
                    reader.Close();
            }

            return returnString;
        }
    }
}

