﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.AccessService;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Access Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: Static; this adapter does not need to be instantiated, but it's CloseProxyInstance() should be called at the 
    /// end of the web request (e.g. in Global.asax: Application_EndRequest)
    /// </summary>
    public class AccessServiceWebAdapter
    {
        /// <summary>
        /// Gets proxy assuming client has the endpoing (with url) and binding configurations in their configuration file
        /// </summary>
        /// <returns></returns>
        public static AccessServiceClient GetProxyInstance()
        {
            AccessServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["AccessServiceClient"] != null)
                {
                    _Client = (AccessServiceClient)HttpContext.Current.Items["AccessServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new AccessServiceClient();
                        HttpContext.Current.Items["AccessServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new AccessServiceClient();
                        HttpContext.Current.Items["AccessServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new AccessServiceClient();
                    HttpContext.Current.Items["AccessServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static AccessServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            AccessServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["AccessServiceClient"] != null)
                {
                    _Client = (AccessServiceClient)HttpContext.Current.Items["AccessServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new AccessServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["AccessServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new AccessServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["AccessServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new AccessServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["AccessServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public static AccessServiceClient GetProxyInstanceForBedrock()
        {
            AccessServiceClient _Client = null;
            try
            {
                string AccessServiceURL = RuntimeSettings.GetSetting("UPS_ACCESS_SERVICE_URL");
                _Client = GetProxyInstance(AccessServiceURL, "BasicHttpBinding_IAccessService");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }


        public static void CloseProxyInstance()
        {
            AccessServiceClient _Client;
            if (HttpContext.Current.Items["AccessServiceClient"] != null)
            {
                _Client = (AccessServiceClient)HttpContext.Current.Items["AccessServiceClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    HttpContext.Current.Items.Remove("AccessServiceClient");
                }
            }
        }
    }
}
