﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.RenewalService;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Renewal Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: Static; this adapter does not need to be instantiated, but it's CloseProxyInstance() should be called at the 
    /// end of the web request (e.g. in Global.asax: Application_EndRequest)
    /// </summary>
    public class RenewalServiceWebAdapter
    {
        /// <summary>
        /// Gets proxy assuming client has the endpoing (with url) and binding configurations in their configuration file
        /// </summary>
        /// <returns></returns>
        public static RenewalServiceClient GetProxyInstance()
        {
            return GetProxyInstance(new HttpContextWrapper(HttpContext.Current));
        }

        public static RenewalServiceClient GetProxyInstance(HttpContextBase context)
        {
            RenewalServiceClient _Client = null;
            try
            {
                if (context.Items["RenewalServiceClient"] != null)
                {
                    _Client = (RenewalServiceClient)context.Items["RenewalServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new RenewalServiceClient();
                        context.Items["RenewalServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new RenewalServiceClient();
                        context.Items["RenewalServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new RenewalServiceClient();
                    context.Items["RenewalServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static RenewalServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            RenewalServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["RenewalServiceClient"] != null)
                {
                    _Client = (RenewalServiceClient)HttpContext.Current.Items["RenewalServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new RenewalServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["RenewalServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new RenewalServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["RenewalServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new RenewalServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["RenewalServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public static RenewalServiceClient GetProxyInstanceForBedrock()
        {
            RenewalServiceClient _Client = null;
            try
            {
                string AccessServiceURL = RuntimeSettings.GetSetting("UPS_RENEWAL_SERVICE_URL");
                _Client = GetProxyInstance(AccessServiceURL, "BasicHttpBinding_IRenewalService");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }


        public static void CloseProxyInstance()
        {
            CloseProxyInstance(new HttpContextWrapper(HttpContext.Current));
        }

        public static void CloseProxyInstance(HttpContextBase context)
        {
            RenewalServiceClient _Client;
            if (context.Items["RenewalServiceClient"] != null)
            {
                _Client = (RenewalServiceClient)context.Items["RenewalServiceClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    context.Items.Remove("RenewalServiceClient");
                }
            }
        }
    }

}
