﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.PurchaseService;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    public class PurchaseServiceWebAdapter
    {
        /// <summary>
        /// Gets proxy assuming client has the endpoing (with url) and binding configurations in their configuration file
        /// </summary>
        /// <returns></returns>
        public static PurchaseServiceClient GetProxyInstance()
        {
            return GetProxyInstance(new HttpContextWrapper(HttpContext.Current));
        }

        public static PurchaseServiceClient GetProxyInstance(HttpContextBase context)
        {
            PurchaseServiceClient _Client = null;
            try
            {
                if (context.Items["PurchaseServiceClient"] != null)
                {
                    _Client = (PurchaseServiceClient)context.Items["PurchaseServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new PurchaseServiceClient();
                        context.Items["PurchaseServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new PurchaseServiceClient();
                        context.Items["PurchaseServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new PurchaseServiceClient();
                    context.Items["PurchaseServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static PurchaseServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            PurchaseServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["PurchaseServiceClient"] != null)
                {
                    _Client = (PurchaseServiceClient)HttpContext.Current.Items["PurchaseServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new PurchaseServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["PurchaseServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new PurchaseServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["PurchaseServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new PurchaseServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["PurchaseServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public static PurchaseServiceClient GetProxyInstanceForBedrock()
        {
            PurchaseServiceClient _Client = null;
            try
            {
                string PurchaseServiceURL = RuntimeSettings.GetSetting("UPS_PURCHASE_SERVICE_URL");
                _Client = GetProxyInstance(PurchaseServiceURL, "BasicHttpBinding_IPurchaseService");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;
        }

        public static void CloseProxyInstance()
        {
            CloseProxyInstance(new HttpContextWrapper(HttpContext.Current));
        }

        public static void CloseProxyInstance(HttpContextBase context)
        {
            PurchaseServiceClient _Client;
            if (context.Items["PurchaseServiceClient"] != null)
            {
                _Client = (PurchaseServiceClient)context.Items["PurchaseServiceClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    context.Items.Remove("PurchaseServiceClient");
                }
            }
        }
    }
}
