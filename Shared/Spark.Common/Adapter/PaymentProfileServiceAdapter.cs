﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.PaymentProfileService;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Payment Profile Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: instance; which means the Service Manager needs to instantiate this adapter and call its closeProxyInstance() 
    /// at the end of the SM process.
    /// </summary>
    public class PaymentProfileServiceAdapter
    {
        PaymentProfileMapperClient _Client = null;

        public PaymentProfileMapperClient GetProxyInstance()
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new PaymentProfileMapperClient();
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new PaymentProfileMapperClient();
                    }
                }
                else
                {
                    _Client = new PaymentProfileMapperClient();
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;
        }

        public PaymentProfileMapperClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new PaymentProfileMapperClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new PaymentProfileMapperClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                }
                else
                {
                    _Client = new PaymentProfileMapperClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public PaymentProfileMapperClient GetProxyInstanceForBedrock()
        {
            try
            {
                string ServiceURL = RuntimeSettings.GetSetting("UPS_PAYMENT_PROFILE_SERVICE_URL");
                _Client = GetProxyInstance(ServiceURL, "BasicHttpBinding_IPaymentProfileMapper");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }


        public void CloseProxyInstance()
        {
            if (_Client != null)
            {
                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
            }
        }
    }
}
