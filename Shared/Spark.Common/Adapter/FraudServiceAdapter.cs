﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.FraudService;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Order History Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: instance; which means the Service Manager needs to instantiate this adapter and call its closeProxyInstance()
    /// at the end of the SM process.
    /// </summary>
    public class FraudServiceAdapter
    {
        private FraudServiceClient _Client = null;

        public FraudServiceClient GetProxyInstance()
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new FraudServiceClient();
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new FraudServiceClient();
                    }
                }
                else
                {
                    _Client = new FraudServiceClient();
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public FraudServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new FraudServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new FraudServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                }
                else
                {
                    _Client = new FraudServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public FraudServiceClient GetProxyInstanceForBedrock()
        {
            try
            {
                string ServiceURL = RuntimeSettings.GetSetting("UPS_FRAUD_SERVICE_URL");
                _Client = GetProxyInstance(ServiceURL, "BasicHttpBinding_IFraudServiceManager");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public void CloseProxyInstance()
        {
            if (_Client != null)
            {
                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
            }
        }
    }
}
