﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.AuthorizationService;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    public class AuthorizationServiceWebAdapter
    {
        /// <summary>
        /// Gets proxy assuming client has the endpoing (with url) and binding configurations in their configuration file
        /// </summary>
        /// <returns></returns>
        public static AuthorizationServiceClient GetProxyInstance()
        {
            AuthorizationServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["AuthorizationServiceClient"] != null)
                {
                    _Client = (AuthorizationServiceClient)HttpContext.Current.Items["AuthorizationServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new AuthorizationServiceClient();
                        HttpContext.Current.Items["AuthorizationServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new AuthorizationServiceClient();
                        HttpContext.Current.Items["AuthorizationServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new AuthorizationServiceClient();
                    HttpContext.Current.Items["AuthorizationServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static AuthorizationServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            AuthorizationServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["AuthorizationServiceClient"] != null)
                {
                    _Client = (AuthorizationServiceClient)HttpContext.Current.Items["AuthorizationServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new AuthorizationServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["AuthorizationServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new AuthorizationServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["AuthorizationServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new AuthorizationServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["AuthorizationServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public static AuthorizationServiceClient GetProxyInstanceForBedrock()
        {
            AuthorizationServiceClient _Client = null;
            try
            {
                string AccessServiceURL = RuntimeSettings.GetSetting("UPS_ACCESS_SERVICE_URL");
                _Client = GetProxyInstance(AccessServiceURL, "BasicHttpBinding_IAccessService");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }


        public static void CloseProxyInstance()
        {
            AuthorizationServiceClient _Client;
            if (HttpContext.Current.Items["AuthorizationServiceClient"] != null)
            {
                _Client = (AuthorizationServiceClient)HttpContext.Current.Items["AuthorizationServiceClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    HttpContext.Current.Items.Remove("AuthorizationServiceClient");
                }
            }
        }
    }
}
