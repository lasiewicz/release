using Spark.Common.AccessService;

namespace Spark.Common.Adapter
{
    public interface IAccessServiceAdapter
    {
        AccessServiceClient GetProxyInstance();
        AccessServiceClient GetProxyInstance(string URL, string endpointConfigurationName);

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        AccessServiceClient GetProxyInstanceForBedrock();

        AccessResponse AdjustCountPrivilege(int customerID, int[] privilegeTypeID, int count, int AdminID, string AdminUserName, int AccessReasonID, int callingSystemID, int callingSystemTypeID);

        void CloseProxyInstance();
    }
}