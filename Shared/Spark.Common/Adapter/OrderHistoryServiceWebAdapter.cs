﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.OrderHistoryService;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Order History Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: Static; this adapter does not need to be instantiated, but it's CloseProxyInstance() should be called at the 
    /// end of the web request (e.g. in Global.asax: Application_EndRequest)
    /// </summary>
    public class OrderHistoryServiceWebAdapter
    {
        public static OrderHistoryManagerClient GetProxyInstance()
        {
            OrderHistoryManagerClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["OrderHistoryManagerClient"] != null)
                {
                    _Client = (OrderHistoryManagerClient)HttpContext.Current.Items["OrderHistoryManagerClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new OrderHistoryManagerClient();
                        HttpContext.Current.Items["OrderHistoryManagerClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new OrderHistoryManagerClient();
                        HttpContext.Current.Items["OrderHistoryManagerClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new OrderHistoryManagerClient();
                    HttpContext.Current.Items["OrderHistoryManagerClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static OrderHistoryManagerClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            OrderHistoryManagerClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["OrderHistoryManagerClient"] != null)
                {
                    _Client = (OrderHistoryManagerClient)HttpContext.Current.Items["OrderHistoryManagerClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new OrderHistoryManagerClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["OrderHistoryManagerClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new OrderHistoryManagerClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["OrderHistoryManagerClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new OrderHistoryManagerClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["OrderHistoryManagerClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public static OrderHistoryManagerClient GetProxyInstanceForBedrock()
        {
            OrderHistoryManagerClient _Client = null;
            try
            {
                string ServiceURL = RuntimeSettings.GetSetting("UPS_ORDERHISTORY_SERVICE_URL");
                _Client = GetProxyInstance(ServiceURL, "BasicHttpBinding_IOrderHistoryManager");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static void CloseProxyInstance()
        {
            OrderHistoryManagerClient _Client;
            if (HttpContext.Current.Items["OrderHistoryManagerClient"] != null)
            {
                _Client = (OrderHistoryManagerClient)HttpContext.Current.Items["OrderHistoryManagerClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    HttpContext.Current.Items.Remove("OrderHistoryManagerClient");
                }
            }
        }        
    }
}
