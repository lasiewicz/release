﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.TraceLogConnectorService;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    public class TraceLogConnectorServiceWebAdapter
    {
        public static void InsertEndpointTraceLog(int globalLogID, string servicename, string functionName, string traceLog, Dictionary<string, string> colDataAttributes, int direction, UPSEndpointProcessingStatus status, DateTime insertDate)
        {
            TraceLogConnectorServiceClient client = null;

            try
            {
                client = GetProxyInstance();
                if (client != null)
                {
                    client.InsertEndpointTraceLog(globalLogID,
                            servicename,
                            functionName,
                            traceLog,
                            colDataAttributes,
                            direction,
                            status,
                            String.Empty,
                            String.Empty,
                            insertDate);
                }
            }
            catch (Exception ex)
            {
                return;
            }
            finally
            {
                CloseProxyInstance();
            }
        }

        public static string GetListenURL()
        {
            TraceLogConnectorServiceClient client = null;

            try
            {
                client = GetProxyInstance();
                if (client != null)
                {
                    return client.Endpoint.Address.Uri.AbsoluteUri;
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception ex)
            {
                return String.Empty;
            }
            finally
            {
                CloseProxyInstance();
            }

        }

        public static TraceLogConnectorServiceClient GetProxyInstance()
        {
            TraceLogConnectorServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["TraceLogConnectorServiceClient"] != null)
                {
                    _Client = (TraceLogConnectorServiceClient)HttpContext.Current.Items["TraceLogConnectorServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new TraceLogConnectorServiceClient();
                        HttpContext.Current.Items["TraceLogConnectorServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new TraceLogConnectorServiceClient();
                        HttpContext.Current.Items["TraceLogConnectorServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new TraceLogConnectorServiceClient();
                    HttpContext.Current.Items["TraceLogConnectorServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static TraceLogConnectorServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            TraceLogConnectorServiceClient _Client = null;
            try
            {
                if (HttpContext.Current.Items["TraceLogConnectorServiceClient"] != null)
                {
                    _Client = (TraceLogConnectorServiceClient)HttpContext.Current.Items["TraceLogConnectorServiceClient"];

                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new TraceLogConnectorServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["TraceLogConnectorServiceClient"] = _Client;
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new TraceLogConnectorServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                        HttpContext.Current.Items["TraceLogConnectorServiceClient"] = _Client;
                    }
                }
                else
                {
                    _Client = new TraceLogConnectorServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    HttpContext.Current.Items["TraceLogConnectorServiceClient"] = _Client;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public static TraceLogConnectorServiceClient GetProxyInstanceForBedrock()
        {
            TraceLogConnectorServiceClient _Client = null;
            try
            {
                string ServiceURL = RuntimeSettings.GetSetting("UPS_ACCESS_SERVICE_URL");
                _Client = GetProxyInstance(ServiceURL, "BasicHttpBinding_IOrderHistoryManager");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public static void CloseProxyInstance()
        {
            TraceLogConnectorServiceClient _Client;
            if (HttpContext.Current.Items["TraceLogConnectorServiceClient"] != null)
            {
                _Client = (TraceLogConnectorServiceClient)HttpContext.Current.Items["TraceLogConnectorServiceClient"];

                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
                finally
                {
                    HttpContext.Current.Items.Remove("TraceLogConnectorServiceClient");
                }
            }
        }
    }
}
