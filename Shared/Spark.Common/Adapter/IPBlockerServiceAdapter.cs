﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.Common.Adapter
{
    public class IPBlockerServiceAdapter
    {
        public static readonly IPBlockerServiceAdapter Instance = new IPBlockerServiceAdapter();

        private IPBlockerServiceAdapter(){		}

        public bool CheckIPAccess(string ipAddress, int siteID, IPBlockerAccessType accessType)
        {
            string url = RuntimeSettings.GetSetting("IP_BLOCKER_URL");
            bool isEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_IP_BLOCKER"));
            int requestTimeout = Convert.ToInt32(RuntimeSettings.GetSetting("IP_BLOCKER_TIMEOUT"));
            bool allowed = true;

            if (isEnabled)
            {
                try
                {
                    if (ipAddress.IsValidIP())
                    {
                        string qs = "?ipaddress=" + ipAddress + "&accessType=" + accessType.ToString("d") + "&siteid=" + siteID.ToString();

                        WebPostRequestUtility req = new WebPostRequestUtility(url + qs, "GET", requestTimeout);
                        string xmlResult = req.GetGETResponse();

                        XmlDocument xdoc = new XmlDocument();
                        xdoc.LoadXml(xmlResult);

                        XmlNode successNode = null;

                        //we changed the name of the node, this looks for the old name. Following IF statement could 
                        //be removed in future release (2/4/11)
                        successNode = xdoc.DocumentElement.SelectNodes("/Response/OperationSuccessful")[0];

                        if (successNode == null)
                        {
                            successNode = xdoc.DocumentElement.SelectNodes("/Response/HasAccess")[0];
                        }

                        XmlNode messageNode = xdoc.DocumentElement.SelectNodes("/Response/Message")[0];

                        allowed = Convert.ToBoolean(successNode.InnerText);
                    }
                    else
                    {
                        allowed = false;
                    }
                }
                catch (Exception ex)
                {
                    Exception innerException = ex;
                    Exception newException = new Exception(string.Format("IPBlocker unavailable. IPAddress: {0} AccessType: {1}", string.IsNullOrEmpty(ipAddress) ? string.Empty : ipAddress, IPBlockerAccessType.Subscription.ToString()), innerException);
                }
            }

            return allowed;
        }
    }
}
