﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Web;
using Spark.Common.RestConsumer.V2.Models;
using log4net;
using Spark.Common.RestConsumer.V2;
using Spark.Common.RestConsumer.V2.Models.OAuth2;

#endregion

namespace Spark.Authentication.OAuth.V2
{
    /// <summary>
    ///     This class is used to log members in and out using OAuth by saving tokens to cookies.  
    ///     This is how JDate Mobile and IM handle login.  
    ///     Bedrock does not depend on OAuth login, but needs to save OAuth tokens because IM depends on OAuth.
    /// </summary>
    public class LoginManager : ILoginManager
    {
        private const string CookieUserPreviouslyRegisteredKey = "MOS_IS_REG";
        private const string CookieEmailPasswordKey = "MOS_REM";

        private const string CookieMemberId = "MOS_MEMBER";
        private const string CookieAccessToken = "MOS_ACCESS";
        private const string CookieSuaAccessToken = "sua_at";
        private const string CookieAccessExpires = "MOS_ACCESS_EXPIRES";
        private const string CookieRefreshToken = "MOS_REFRESH";
        private const string CookieIsSubscriber = "MOS_SUB";
        private const string CookieEmail = "MOS_EA";
        private const string CookieDomain = "MOS_DOMAIN";
        private static readonly ILog Log;

        // TODO: remove these when all clients pass in app IDs
        private static readonly int ApplicationId;
        private static readonly string ClientSecret;

        public static readonly int LoginMinutesNoRememberMe;
        public static readonly int LoginMinutesRememberMe;

        public static readonly LoginManager Instance = new LoginManager();

        private LoginManager()
        {
        }

        static LoginManager()
        {
            Log = LogManager.GetLogger(typeof (LoginManager));

            var str = ConfigurationManager.AppSettings["LoginMinutesNoRememberMe"];
            if (!Int32.TryParse(str, out LoginMinutesNoRememberMe))
            {
                throw new Exception("Missing or invalid config entry for LoginMinutesNoRememberMe");
            }

            str = ConfigurationManager.AppSettings["LoginMinutesRememberMe"];
            if (!Int32.TryParse(str, out LoginMinutesRememberMe))
            {
                throw new Exception("Missing or invalid config entry for LoginMinutesRememberMe");
            }

            // TODO: remove these when all clients pass in app IDs
            if (ConfigurationManager.AppSettings["ApplicationId"] != null)
            {
                Int32.TryParse(ConfigurationManager.AppSettings["ApplicationId"], out ApplicationId);
            }
            if (ConfigurationManager.AppSettings["ClientSecret"] != null)
            {
                ClientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            }
        }

        /// <summary>
        ///     Makes a REST API call to fetch access tokens
        /// </summary>
        /// <param name = "email"></param>
        /// <param name = "password"></param>
        /// <param name="passwordIsEncrypted"></param>
        /// <param name="brandId"></param>
        /// <param name="applicationId"> </param>
        /// <param name="clientSecret"> </param>
        /// <param name="responseDetail"></param>
        /// <returns></returns>
        public OAuthTokens GetTokensViaPassword(string email, string password, bool passwordIsEncrypted, int brandId, int applicationId, string clientSecret, out ResponseDetail responseDetail)
        {
            var urlParams = new Dictionary<string, string>
                                {{"applicationId", applicationId.ToString(CultureInfo.InvariantCulture)}};
            var queryParams = new Dictionary<string, string> {{"client_secret", clientSecret}};
            OAuthTokens tokenResult = null;
            responseDetail = null;
            try
            {
                var tokenRequestPassword = new TokenRequestPassword
                    {
                        Email = email,
                        Password = password,
                        PasswordIsEncrypted = passwordIsEncrypted
                    };

                tokenResult = RestConsumer.GetInstance(brandId)
                                          .Post<OAuthTokens, TokenRequestPassword>(urlParams, tokenRequestPassword,
                                                                                   out responseDetail, queryParams);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Exception getting OAuth tokens from REST using email/password.  Email: {0}", email);
                Log.Error(ex);
                return null;
            }

            if (tokenResult == null)
            {
                Log.ErrorFormat("Failed to obtain new access token using email and password credentials. Email: {0} Error code: {1} Error message: {2}", email, responseDetail.ErrorCode, responseDetail.ErrorMessage);
                return null;
            }

            Log.DebugFormat("Successful token request for member ID {0}", tokenResult.MemberId);
            var expirationTime = DateTime.Now.AddSeconds(tokenResult.ExpiresIn);
            Log.DebugFormat("New tokens - Access: {0} Expires in {1} seconds at {2}", tokenResult.AccessToken,
                            tokenResult.ExpiresIn, expirationTime);
            return tokenResult;

        }

        public OAuthTokens GetTokensViaRefresh(int memberId, string refreshToken, int brandId, int applicationId, string clientSecret)
        {
            var urlParams = new Dictionary<string, string>
                                {{"applicationId", applicationId.ToString(CultureInfo.InvariantCulture)}};
            var tokenRequest = new TokenRefreshRequest {RefreshToken = refreshToken, MemberId = memberId};
            var queryParams = new Dictionary<string, string> {{"client_secret", clientSecret}};
            Log.DebugFormat("Making token request using refresh token for member id {0}", memberId);
            string errorMessage;
            OAuthTokens tokenResult = null;
            try
            {
                ResponseDetail responseDetail;
                tokenResult = RestConsumer.GetInstance(brandId).Post<OAuthTokens, TokenRefreshRequest>(urlParams, tokenRequest, out responseDetail,
                                                                                           queryParams);
                if (tokenResult != null)
                {
                    Log.DebugFormat("Successful token refresh request for member ID {0}", tokenResult.MemberId);
                    var expirationTime = DateTime.Now.AddSeconds(tokenResult.ExpiresIn);
                    Log.DebugFormat("New tokens - Access: {0} Expires in {1} seconds at {2}", tokenResult.AccessToken,
                                    tokenResult.ExpiresIn, expirationTime);
                    return tokenResult;
                }
                errorMessage = responseDetail.ErrorMessage;
            }
            catch (ThreadAbortException ex)
            {
                errorMessage = "Thread abort exception during refresh token request";
                Log.Error(errorMessage, ex);
                if (ex.InnerException != null)
                {
                    Log.Error("Inner exception: ", ex.InnerException);
                }
            }
            catch (Exception ex)
            {
                errorMessage = "Exception during refresh token request";
                Log.Error(errorMessage, ex);
            }

            var message =
                String.Format(
                    "Failed to obtain new access token using refresh token for app id {0}, user id {1} error {2}",
                    applicationId, tokenRequest.MemberId, errorMessage);
            Log.Error(message);
            return tokenResult;
        }

        public OAuthTokens GetTokensFromCookies()
        {
            var tokens = new OAuthTokens();
            var cookie = CookieUtils.GetCookieFromRequestOrResponse(CookieMemberId);
            if (cookie != null)
            {
                int memberId;
                if (Int32.TryParse(cookie.Value, out memberId))
                {
                    tokens.MemberId = memberId;
                }
            }

            cookie = CookieUtils.GetCookieFromRequestOrResponse(CookieAccessToken);
            if (cookie != null)
            {
                tokens.AccessToken = cookie.Value;
            }

            cookie = CookieUtils.GetCookieFromRequestOrResponse(CookieRefreshToken);
            if (cookie != null)
            {
                tokens.RefreshToken = cookie.Value;
            }

            cookie = CookieUtils.GetCookieFromRequestOrResponse(CookieIsSubscriber);
            if (cookie != null)
            {
                tokens.IsPayingMember = cookie.Value == "Y";
            }

            cookie = CookieUtils.GetCookieFromRequestOrResponse(CookieAccessExpires);
            if (cookie != null)
            {
                var expireTimeStr = cookie.Value;
                DateTime accessExpires;
                if (DateTime.TryParse(expireTimeStr, out accessExpires))
                {
                    tokens.ExpiresIn = (int) (accessExpires - DateTime.Now).TotalSeconds;
                }
            }

            return tokens;
        }

        public void SetSubscriberCookie(bool isSubscriber, string oauthCookieDomain)
        {
            var cookie = new HttpCookie(CookieIsSubscriber)
                             {
                                 Expires = DateTime.Now.AddMinutes(LoginMinutesRememberMe),
                                 Value = isSubscriber ? "Y" : "N",
                                 Domain = oauthCookieDomain
                             };
            CookieUtils.AddHttpCookie(cookie);

        }


        public void SaveTokensToCookies(HttpResponse response, OAuthTokens tokens, bool useRememeberMeMinutes,
                                               string email, string environment, string brandDomain)
        {
            var oauthCookieDomain = !String.IsNullOrEmpty(environment) ? "." + environment : string.Empty;
            oauthCookieDomain += "." + brandDomain;
            var refreshTokenMins = useRememeberMeMinutes ? LoginMinutesRememberMe : LoginMinutesNoRememberMe;

            var cookie = new HttpCookie(CookieMemberId)
                             {
                                 Expires = DateTime.Now.AddMinutes(LoginMinutesRememberMe),
                                 Value = tokens.MemberId.ToString(CultureInfo.InvariantCulture),
                                 Domain = oauthCookieDomain
                             };
            // using LoginMinutesRememberMe time (currently 60 days)
            CookieUtils.AddHttpCookie(cookie);


            if (refreshTokenMins != 0 && !string.IsNullOrEmpty(tokens.RefreshToken))
                // if 0, refresh token cookie doesn't need to be saved, value was just read from cookie
            {
                // refresh token expiration time controls when user will need to login with email/password
                cookie = new HttpCookie(CookieRefreshToken)
                             {
                                 Expires = DateTime.Now.AddMinutes(refreshTokenMins),
                                 Value = tokens.RefreshToken,
                                 Domain = oauthCookieDomain
                             };
                CookieUtils.AddHttpCookie(cookie);
            }

            SetSubscriberCookie(tokens.IsPayingMember, oauthCookieDomain);

            var accessExpires = DateTime.Now.AddSeconds(tokens.ExpiresIn);
            cookie = new HttpCookie(CookieAccessToken)
                         {Expires = accessExpires, Value = tokens.AccessToken, Domain = oauthCookieDomain};
            CookieUtils.AddHttpCookie(cookie);

            cookie = new HttpCookie(CookieSuaAccessToken)
                        { Expires = accessExpires, Value = tokens.AccessToken, Domain = oauthCookieDomain };
            CookieUtils.AddHttpCookie(cookie);


            cookie = new HttpCookie(CookieAccessExpires)
                         {
                             Expires = accessExpires,
                             Value = DateTime.Now.AddSeconds(tokens.ExpiresIn).ToString(CultureInfo.InvariantCulture),
                             Domain = oauthCookieDomain
                         };
            CookieUtils.AddHttpCookie(cookie);
            
            // Remember email addy in a cookie, fill out for convenience
            if (email != null)
            {
                cookie = new HttpCookie(CookieEmail)
                             {Expires = DateTime.Now.AddDays(7), Value = email, Domain = oauthCookieDomain};
                CookieUtils.AddHttpCookie(cookie);
            }

            cookie = new HttpCookie(CookieUserPreviouslyRegisteredKey)
                         {Expires = DateTime.Now.AddDays(30), Value = "Y", Domain = oauthCookieDomain};
            CookieUtils.AddHttpCookie(cookie);

            cookie = new HttpCookie(CookieDomain)
                         {
                             Expires = DateTime.Now.AddMinutes(refreshTokenMins),
                             Value = oauthCookieDomain,
                             Domain = oauthCookieDomain
                         };
            // setting a cookie with the domain of all the OAuth cookies.  With this set, the client can detect which environment the cookies were set for, stage/prod, 
            // and can be used to keep users logged into multiple environments without having to clear cookies
            CookieUtils.AddHttpCookie(cookie);
        }

        /// <summary>
        ///     This method effectively logs in users via OAuth.  Bedrock calls this method so that IM users will be logged in.
        ///     This method makes a REST call to collect tokens, then saves to cookies.
        /// </summary>
        /// <param name = "email"></param>
        /// <param name = "password"></param>
        /// <param name="passwordIsEncrypted"></param>
        /// <param name = "useRememberMeExpirationTime">if true, use longer cookie lifespan, else use shorter.  Both times are config driven</param>
        /// <param name="environment">www, dev, stgv3, etc. </param>
        /// <param name="brandDomain">jdate.com, blacksingles.com, etc.</param>
        /// <param name="brandId"></param>
        /// <param name="applicationId"> </param>
        /// <param name="clientSecret"> </param>
        /// <returns></returns>
        public OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted,
                                                              bool useRememberMeExpirationTime, string environment, string brandDomain, int brandId, int applicationId, string clientSecret)
        {
            try
            {
                const int retryLimit = 3;
                var retries = 0;
                OAuthTokens tokens;
                do
                {
                    retries++;
                    ResponseDetail responseDetail;
                    tokens = GetTokensViaPassword(email, password, passwordIsEncrypted, brandId, applicationId, clientSecret, out responseDetail);
                    if (tokens == null)
                    {
                        Log.Warn("Failed to get oauth tokens");
                        continue;
                    }
                    break;
                } while (retries < retryLimit);

                if (tokens == null)
                {
                    return null;
                }

                SaveTokensToCookies(HttpContext.Current.Response, tokens, useRememberMeExpirationTime, email, environment, brandDomain);
                return tokens;
            }
            catch (Exception ex)
            {
                Log.Error("Failed to collect and cookie tokens", ex);
                return null;
            }
        }

        public void RemoveTokenCookies(string oauthCookieDomain)
        {
            HttpCookie domainCookie = CookieUtils.GetCookieFromRequestOrResponse(CookieDomain);
            string domain = (null != domainCookie) ? domainCookie.Value : null;

            // kill the autologin cookie
//            CookieUtils.RemoveCookie(CookieEmailPasswordKey, domain, "/");
            CookieUtils.RemoveCookie(CookieAccessToken, domain, "/");
            CookieUtils.RemoveCookie(CookieAccessExpires, domain, "/");
            CookieUtils.RemoveCookie(CookieRefreshToken, domain, "/");
            CookieUtils.RemoveCookie(CookieEmail, domain, "/");
            CookieUtils.RemoveCookie(CookieSuaAccessToken, domain, "/");
//            CookieUtils.RemoveCookie(CookieIsSubscriber, domain, "/");
//            CookieUtils.RemoveCookie(CookieUserPreviouslyRegisteredKey, domain, "/");
//            CookieUtils.RemoveCookie(CookieMemberId, domain, "/");
            CookieUtils.RemoveCookie(CookieDomain, domain, "/");
        }

        public bool UserPreviouslyLoggedIn()
        {
            var cookie = HttpContext.Current.Request.Cookies.Get(CookieUserPreviouslyRegisteredKey);
            return cookie != null && cookie.Value == "Y";
        }

        [Obsolete("please use the overload that has the environment parameter instead")]
        public void SaveTokensToCookies(HttpResponse response, OAuthTokens tokens, bool useRememeberMeMinutes, string email, string brandDomain)
        {
            SaveTokensToCookies(response, tokens, useRememeberMeMinutes, email, String.Empty, brandDomain);
        }

        [Obsolete("please use the overload that has the environment parameter instead")]
        public OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted, bool useRememberMeExpirationTime, string brandDomain, int brandId)
        {
            return FetchTokensAndSaveToCookies(email, password, passwordIsEncrypted, useRememberMeExpirationTime, String.Empty, brandDomain, brandId, ApplicationId, ClientSecret);
        }

        [Obsolete("please use the overload that accepts app id and client secret instead")]
        public OAuthTokens GetTokensViaPassword(string email, string password, bool passwordIsEncrypted, int brandId)
        {
            ResponseDetail responseDetail;
            return GetTokensViaPassword(email, password, passwordIsEncrypted, brandId, ApplicationId, ClientSecret, out responseDetail);
        }

        [Obsolete("please use the overload that accepts app id and client secret instead")]
        public OAuthTokens GetTokensViaRefresh(int memberId, string refreshToken, int brandId)
        {
            return GetTokensViaRefresh(memberId, refreshToken, brandId, ApplicationId, ClientSecret);
        }

        [Obsolete("please use the overload with app id and client secret instead")]
        public OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted, bool useRememberMeExpirationTime, string environment, string brandDomain, int brandId)
        {
            return FetchTokensAndSaveToCookies(email, password, passwordIsEncrypted, useRememberMeExpirationTime,
                                               environment, brandDomain, brandId, ApplicationId, ClientSecret);
        }
    }
}
