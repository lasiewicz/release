﻿using System;
using System.Collections.Generic;
using System.Web;
using log4net;
using Spark.Authentication.OAuth.V2;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;

namespace Spark.Authentication.OAuth.V2
{
    public class CookieUtils
    {
        private static ILog _loggingManager = LogManager.GetLogger(typeof (CookieUtils));

        public const int NULL_INT = Int32.MinValue + 1;
        public const string NULL_STRING = null;

        public static void RemoveCookie(string cookiename)
        {
            RemoveCookie(cookiename, null, null);
        }

        public static void RemoveCookie(string cookiename, string domain, string path)
        {
            try
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(cookiename);

                if (cookie == null)
                    return;

                cookie.Expires = DateTime.Now.AddDays(-1);
                if (null != domain) cookie.Domain = domain;
                if (null != path) cookie.Path = path;
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch (Exception ex)
            {
                _loggingManager.Error(
                    "RemoveCookie: " +
                    String.Format("Cannot remove cookie - name:{0}, domain:{1}, path:{2}", cookiename, domain, path),
                    ex);
            }
        }

        public static string GetTopLevelDomain(Brand brand)
        {
            try
            {
                string topLevelDomain = "." + brand.Uri.ToLower();
                return topLevelDomain;
            }
            catch (Exception e)
            {
                _loggingManager.Error("GetTopLevelDomain()", e);
                return HttpContext.Current.Request.Url.Host;
            }
        }

        public static string GetCurrentDomain()
        {
            return "." + HttpContext.Current.Request.Url.Host;
        }

        public static void AddCookie(string cookiename, string cookieval, int expirehours, bool encode)
        {
            AddCookie(cookiename, cookieval, expirehours, encode, null, null);
        }

    public static void AddCookie(string cookiename, string cookieval, int expirehours, bool encode, string domain,
        string path)
    {
     
            try
            {
                string val;
                if (encode)
                {
                    val = HttpUtility.UrlEncode(cookieval);
                }
                else
                {
                    val = cookieval;
                }

                HttpCookie cookie = new HttpCookie(cookiename, val);
                if (expirehours != NULL_INT)
                {
                    // If expirehours is set to Constants.NULL_INT, then the cookie will expired
                    // with the browser session  
                    cookie.Expires = DateTime.Now.AddHours(expirehours);
                }

                if (null != domain)
                {
                    cookie.Domain = domain;
                }

                if (null != path)
                {
                    cookie.Path = path;
                }

                AddHttpCookie(cookie);
            }
            catch (Exception ex)
            {
                _loggingManager.Error(
                    "AddCookie: " +
                    String.Format("Cannot add cookie - name:{0}, value:{1}, domain:{2}, path:{3}", cookiename,
                        cookieval, domain, path), ex);
            }
    }

        public static void AddHttpCookie(HttpCookie cookie)
        {
            try
            {
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch (Exception e)
            {
                if (null != cookie)
                {
                    _loggingManager.Error("AddHttpCookie: " +
                                          String.Format(
                                              "Cannot add http cookie - name:{0}, value:{1}, domain:{2}, path:{3}",
                                              cookie.Name,
                                              cookie.Value, cookie.Domain, cookie.Path), e);
                }
                else
                {
                    _loggingManager.Error("AddHttpCookie: Cookie is NULL!!", e);
                }
            }
        }

        public static string GetCookie(string cookiename, string defaultVal, bool decode)
        {
            string cookieVal = defaultVal;
            try
            {

                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(cookiename);
                if (cookie != null)
                {
                    if (decode)
                        cookieVal = HttpUtility.UrlDecode(cookie.Value);
                    else
                        cookieVal = cookie.Value;

                }
                return cookieVal;
            }
            catch (Exception ex)
            {
                return cookieVal;
            }
        }

        public static bool CookieExists(string cookieName)
        {
            return (HttpContext.Current.Request.Cookies[cookieName] != null);
        }

        public static void AddOrUpdateCookie(HttpCookie newCookie)
        {
            var keys = new HashSet<string>(HttpContext.Current.Response.Cookies.AllKeys);
            if (!keys.Contains(newCookie.Name))
            {
                HttpContext.Current.Response.AppendCookie(newCookie);
            }
            else
            {
                // reuse the cookie already in httpcookiecollection to avoid duplication
                HttpContext.Current.Response.SetCookie(newCookie);
            }
        }

        public static HttpCookie GetCookieFromRequestOrResponse(string cookieName)
        {
            var outGoing = HttpContext.Current.Response.Cookies[cookieName];
            // this call creates a new cookie if it doesn't exist
            if (outGoing != null && !String.IsNullOrEmpty(outGoing.Value))
            {
                return outGoing;
            }
            if (outGoing != null)
            {
                HttpContext.Current.Response.Cookies.Remove(cookieName);
                // no value?  this cookie was just created by the indexer above, so remove it
            }
            var cookie = HttpContext.Current.Request.Cookies.Get(cookieName);
            return cookie;
        }

    }
}