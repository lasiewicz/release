﻿using System.Collections.Generic;

namespace Spark.Authentication.OAuth.V2
{
    public interface IPostLogonProcessor
    {
        string RedirectUrl { get; set; }
        int ApplicationID { get; }
        string ClientSecret { get; }
        string Environment { get; }  
        string SiteName { get; }
        bool IsTopLevelDomainCookiesEnabled();
        bool IsSuaLogon();
        void ProcessSuccessfulLogon(int memberId, bool startLogonSession, bool isAutoLogon = false);
        void AdminImpersonate();
        void RedirectToSua(string url);
        RedirectUrlSet RedirectSet { get; set; }
    }

    public class RedirectUrlSet
    {
        public string RedirectUrl { get; set; }
        public Dictionary<string, string> SuaParams { get; set; }        
    }
}