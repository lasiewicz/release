﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Web;
using RestSharp.Validation;
using Spark.Common.RestConsumer.V2.Models.Activity;
using log4net;
using Spark.Common.RestConsumer.V2;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Common.RestConsumer.V2.Models.OAuth2;

#endregion

namespace Spark.Authentication.OAuth.V2
{
    public class SuaLogonManager
    {
        public const int ScopeLong = 90;
        public const int ScopeShort = 2;
        public const string CookieSuaState = "sua_s";
        public const string CookieSuaAccessToken = "sua_at";
        public const string CookieSuaAccessTokenExpiration = "sua_ate";
        public const string CookieSuaMemberId = "sua_mid";
        public const string CookieSuaUserId = "sua_userid";
        public const string CookieSuaIsSubscriber = "sua_sub";
        public const string CookieSuaDomain = "sua_d";
        public const string CookieSuaLoginSession = "sua_ls";
        public const string CookieBetaRedesign = "sua_b";
        public const string CookieBetaRedesignOffered = "sua_bo";
        public const string ParamSuaReferer = "sua_ref";
        public const string ParamLoginSessionId = "LoginSessionId";
        public const string ParamSuaGoogleAnalyticsClientID = "ga_clientid";
        public const string RootPath = "/";
        private static readonly ILog Log = LogManager.GetLogger(typeof(SuaLogonManager));
        private readonly int _brandId;
        private const string ActionTypeLoginArrivedAtClientSystem = "LoginArrivedAtClientSystem";
        private const string ActionTypeLoginSentBackToClient = "LoginSentBackToClient";

        private readonly IPostLogonProcessor _postLogonProcessor;
        private Brand _brand;

        public SuaLogonManager(int brandId, IPostLogonProcessor postLogonProcessor)
        {
            _postLogonProcessor = postLogonProcessor;
            _brandId = brandId;
        }

        public IPostLogonProcessor PostLogonProcessor
        {
            get { return _postLogonProcessor; }
        }

        public Brand Brand
        {
            get
            {
                if (null == _brand)
                {
                    var urlParams = new Dictionary<string, string>
                    {
                        {"brandId", _brandId.ToString(CultureInfo.InvariantCulture)}
                    };
                    try
                    {
                        ResponseDetail responseDetail;
                        _brand =
                            RestConsumer.Instance.Get<Brand>(
                                urlParams, out responseDetail);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Exception getting brand", ex);
                        throw;
                    }
                }
                return _brand;
            }
        }

        public void ProcessSuaLogon()
        {
            if (!IsSuaLogon()) return;

            if (!IsSuaResponse())
            {
                if (PostLogonProcessor.IsTopLevelDomainCookiesEnabled())
                {
                    //remove any old sua access tokens before redirect
                    CookieUtils.RemoveCookie(CookieSuaAccessToken, CookieUtils.GetCurrentDomain(), RootPath);
                }
                ProcessSuaRedirect();
                return;
            }

            ProcessSuaResponse();
        }

        public bool IsSuaLogon()
        {
            return PostLogonProcessor.IsSuaLogon();
        }

        private static bool IsSuaResponse()
        {
            return (HttpContext.Current.Request.QueryString["accesstoken"] != null &&
                    HttpContext.Current.Request.QueryString["state"] != null);
        }

        private void ProcessSuaRedirect()
        {
            PostLogonProcessor.AdminImpersonate();
            var url = CreateSuaUrl();
            RedirectToSua(url);
        }

        /// <summary>
        ///     Construct a request to SUA with OAuth parameters
        /// </summary>
        private string CreateSuaUrl()
        {
            var tokenCookieEnvironmentValue = PostLogonProcessor.Environment;
            string host = string.Empty;
            
            //we are going to use the Brand to decide what url to display
            var useSiteDomain = false;
           if(Brand != null)
           {
               useSiteDomain = RuntimeSettings.Instance.IsSettingEnabled("USE_LOGIN_SITENAME_COM_URL_FOR_SUA", Brand.Id);

           }
               

            switch (tokenCookieEnvironmentValue.ToLower())
            {
                case "local":
                case "dev":
                case "stgv3":
                case "preprod":
                    if (useSiteDomain)
                        host = string.Format("https://{0}-login."+Brand.Uri, tokenCookieEnvironmentValue.ToLower());
                    else
                        host = string.Format("https://accounts.{0}.spark.net", tokenCookieEnvironmentValue.ToLower());
                    break;
                case "newstgv3":
                case "stage":
                case "stage3":
                    if (useSiteDomain)
                        host = "https://stage3-login." + Brand.Uri;
                    else
                        host = "https://accounts.stage3.spark.net";
                    break;
                case "newstgv2":
                    if (useSiteDomain)
                        host = "https://accounts-temp-stgv2.login" + Brand.Uri;
                    else
                        host = "https://accounts-temp.stgv2.spark.net";
                    break;
                default:
                    if(useSiteDomain)
                        host = "https://login." + Brand.Uri;
                    else
                        host = "https://accounts.spark.net";
                    break;
            }

            host += string.Format("/logon/{0}", PostLogonProcessor.SiteName);

            var state = CreateAndStoreStateKey();
            var loginSessionId = CreateAndStoreLoginSessionId();

            var clientId = PostLogonProcessor.ApplicationID;
            // todo: create a setting and sitesettings for different clientIds

            var redirectUrlSet = PostLogonProcessor.RedirectSet;
            var suaUrl = host + "?redirecturl=" + HttpUtility.UrlEncode(redirectUrlSet.RedirectUrl) + "&clientid=" +
                         clientId +
                         "&scope=long" +
                         "&state=" + state + "&displayMode=full&" + ParamLoginSessionId + "=" + loginSessionId;
            foreach (var suaParam in redirectUrlSet.SuaParams)
            {
                suaUrl += string.Format("&{0}={1}", suaParam.Key, suaParam.Value);
            }    

            //add url referrer for omniture
            if (null != HttpContext.Current.Request.UrlReferrer)
            {
                var encodedReferer = HttpUtility.UrlEncode(HttpContext.Current.Request.UrlReferrer.ToString());
                suaUrl += String.Format("&{0}={1}", ParamSuaReferer, encodedReferer);
            }

           //add url referrer for omniture

            if (null != HttpContext.Current.Request.UrlReferrer)
            {
                var encodedReferer = HttpUtility.UrlEncode(HttpContext.Current.Request.UrlReferrer.ToString());
                suaUrl += String.Format("&{0}={1}", ParamSuaReferer, encodedReferer);
            }

            // Send Google Analytics client id for tracking continuity 
            if (HttpContext.Current.Request.Cookies["_ga"] != null && !string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["_ga"].Value))
            {
                suaUrl += String.Format("&{0}={1}", ParamSuaGoogleAnalyticsClientID, HttpContext.Current.Request.Cookies["_ga"].Value.Remove(0, 6));
            }


            Log.Info("LogonManager.CreateSuaUrl - " + string.Format("suaUrl:{0}", suaUrl));

            return suaUrl;
        }

        /// <summary>
        ///     Randomized key to match with SUA redirects back to the confirmation page
        /// </summary>
        private string CreateAndStoreStateKey()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            var state = Convert.ToString(random.Next());
            var domain = (PostLogonProcessor.IsTopLevelDomainCookiesEnabled())
                ? CookieUtils.GetTopLevelDomain(Brand)
                : CookieUtils.GetCurrentDomain();
            CookieUtils.AddCookie(CookieSuaState, state, 1, false, domain, RootPath);
            return state;
        }

        private string CreateAndStoreLoginSessionId()
        {
            var loginSessionId = string.Empty;

            if (CookieUtils.CookieExists(CookieSuaLoginSession))
            {
                loginSessionId = CookieUtils.GetCookie(CookieSuaLoginSession, string.Empty, false);
            }
            else
            {
                loginSessionId = Guid.NewGuid().ToString();
                var domain = (PostLogonProcessor.IsTopLevelDomainCookiesEnabled())
                    ? CookieUtils.GetTopLevelDomain(Brand)
                    : CookieUtils.GetCurrentDomain();

                CookieUtils.AddCookie(CookieSuaLoginSession, loginSessionId, 2, false, domain, RootPath);
            }

            return loginSessionId;
        }

        public void RedirectToSua(string url)
        {
            RecordLoginArrivedAtClientSystem();
            PostLogonProcessor.RedirectToSua(url);
        }

        public int GetBetaRedesignPercent()
        {
            int defaultVal = 0;
            try
            {
                defaultVal = Int32.Parse(RuntimeSettings.Instance.GetSettingValue("BETA_SITE_REDESIGN_PERCENT",Brand.Id));
            }
            catch (Exception e)
            {
                //TODO: Spark.Logger here
                defaultVal = 0;
            }
            return defaultVal;
        }

        public string GetBetaRedesignUrl()
        {
            string defaultVal = "beta." + Brand.Uri;
            try
            {
                defaultVal = RuntimeSettings.Instance.GetSettingValue("BETA_SITE_REDESIGN_URL", Brand.Id);
            }
            catch (Exception e)
            {
                //TODO: Spark.Logger here
                defaultVal = "beta." + Brand.Uri;
            }
            return defaultVal;
        }

        /// <summary>
        ///     Verify the response from SUA after successfully authenticating
        /// </summary>
        public void ProcessSuaResponse()
        {
            if (string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["accesstoken"]))
            {
                var exception = new Exception(string.Format("SUA response verification failed. Missing accesstoken."));
                Log.Info("LogonManager.ProcessSuaResponse()", exception);
                throw exception;
            }

            var accesstoken = HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString["accesstoken"]);
            var isSubscriber = (bool.TrueString.Equals(HttpContext.Current.Request.QueryString["issub"])) ? "Y" : "N";
            var state = HttpContext.Current.Request.QueryString["state"];
            var stateCookie = HttpContext.Current.Request[CookieSuaState];
            // although initially sent from the client, the scope's value can be changed on sua (autologin checkbox)
            var scope = HttpContext.Current.Request.QueryString["scope"];
            var cookieExpirationHours = (scope == "long") ? ScopeLong * 24 : ScopeShort * 24;

            var domain = (PostLogonProcessor.IsTopLevelDomainCookiesEnabled())
                ? CookieUtils.GetTopLevelDomain(Brand)
                : CookieUtils.GetCurrentDomain();

            var memberId = ValidateAccessToken(accesstoken);
            var statesMatch = (state == stateCookie);

            RecordLoginSentBackToClient(state, statesMatch, memberId);
            CookieUtils.RemoveCookie(CookieSuaLoginSession, domain, RootPath);

            if (memberId == CookieUtils.NULL_INT)
            {
                var exception = new Exception(string.Format("SUA response. Null member from API"));
                Log.Info("LogonManager.ProcessSuaResponse()", exception);
                throw exception;
            }

            if (!statesMatch)
            {
                var exception =
                    new Exception(
                        string.Format("SUA response verification failed. state:{0} sua_state:{1} acesstoken:{2}", state,
                            stateCookie, accesstoken));
                Log.Info("LogonManager.ProcessSuaResponse()", exception);
                throw exception;
            }


            // remove once used for verification
            CookieUtils.RemoveCookie(CookieSuaState, domain, RootPath);

            //add domain sua cookie
            CookieUtils.AddCookie(CookieSuaDomain, domain, cookieExpirationHours, false, domain, RootPath);
            // store the access token and set the expiration based on the scope value
            CookieUtils.AddCookie(CookieSuaAccessToken, accesstoken, cookieExpirationHours, false, domain, RootPath);
            // add access token expiration sua cookie            
            CookieUtils.AddCookie(CookieSuaAccessTokenExpiration,
                DateTime.Now.AddHours(cookieExpirationHours).ToString(CultureInfo.InvariantCulture),
                cookieExpirationHours, false, domain, RootPath);
            //add is subscriber sua cookie
            SetSubscriberCookie(isSubscriber, cookieExpirationHours, domain);

            Log.Info("LogonManager.ProcessSuaResponse - " +
                                 String.Format("Successfully stored sua access token. accesstoken:{0} state:{1}",
                                     accesstoken, state));

            // add member id sua cookie
            CookieUtils.AddCookie(CookieSuaMemberId, memberId.ToString(CultureInfo.InvariantCulture),
                cookieExpirationHours, false, domain, RootPath);
            PostLogonProcessor.ProcessSuccessfulLogon(memberId, true);
        }

        public static void SetSubscriberCookie(string isSubscriber, int cookieExpirationHours, string domain)
        {
            //add is subscriber sua cookie
            CookieUtils.AddCookie(CookieSuaIsSubscriber, isSubscriber, cookieExpirationHours, false, domain, RootPath);
        }

        public static void SaveTokensToCookies(HttpResponseBase response, OAuthTokens tokens, Brand brand, string scope, bool isTopLevelDomainEnabled)
        {
            var domain = (isTopLevelDomainEnabled)
                ? CookieUtils.GetTopLevelDomain(brand)
                : CookieUtils.GetCurrentDomain();
            var cookieExpirationHours = (scope == "long") ? ScopeLong * 24 : ScopeShort * 24;

            //add domain sua cookie
            CookieUtils.AddCookie(CookieSuaDomain, domain, cookieExpirationHours, false, domain, RootPath);
            // store the access token and set the expiration based on the scope value
            CookieUtils.AddCookie(CookieSuaAccessToken, tokens.AccessToken, cookieExpirationHours, false, domain, RootPath);
            // add access token expiration sua cookie            
            CookieUtils.AddCookie(CookieSuaAccessTokenExpiration,
                DateTime.Now.AddHours(cookieExpirationHours).ToString(CultureInfo.InvariantCulture),
                cookieExpirationHours, false, domain, RootPath);
            //add is subscriber sua cookie
            var isSubscriber = (tokens.IsPayingMember) ? "Y" : "N";
            SetSubscriberCookie(isSubscriber, cookieExpirationHours, domain);

            // add member id sua cookie
            CookieUtils.AddCookie(CookieSuaMemberId, tokens.MemberId.ToString(CultureInfo.InvariantCulture), cookieExpirationHours, false, domain, RootPath);
            CookieUtils.AddCookie(CookieSuaUserId, tokens.MemberId.ToString(CultureInfo.InvariantCulture), cookieExpirationHours, false, domain, RootPath);

            //add domain beta redesign cookie
            CookieUtils.AddCookie(CookieBetaRedesign, domain, cookieExpirationHours, false, domain, RootPath);            

            Log.Info("LogonManager.ProcessSuaResponse - " + String.Format("Successfully stored sua access token. accesstoken:{0} memberId:{1}", tokens.AccessToken, tokens.MemberId));
        }

        public bool AutoLogin()
        {
            Log.DebugFormat("AutoLogin called");
            var memberId = AuthenticateAccessToken();
            Log.DebugFormat("MemberId:{0}", memberId);
            if (memberId == CookieUtils.NULL_INT) return false;
            PostLogonProcessor.ProcessSuccessfulLogon(memberId, true, true);
            return true;
        }

        /// <summary>
        ///     Used after SUA logon to validate the access token that was returned.
        /// </summary>
        /// <returns></returns>
        private int ValidateAccessToken(string accessToken)
        {
            try
            {
                //var accessTokenCookie = CookieUtils.GetCookieFromRequestOrResponse(CookieSuaAccessToken);
                //if (accessTokenCookie == null) return CookieUtils.NULL_INT;
                //var accessToken = accessTokenCookie.Value;

                if (string.IsNullOrEmpty(accessToken)) return CookieUtils.NULL_INT;

                var urlParams = new Dictionary<string, string>
                {
                    {"brandId", _brandId.ToString(CultureInfo.InvariantCulture)}
                };
                var queryStringParams = new Dictionary<string, string>
                {
                    {"accessToken", accessToken},
                    {"applicationId", PostLogonProcessor.ApplicationID.ToString(CultureInfo.InvariantCulture)},
                    {"client_secret", PostLogonProcessor.ClientSecret}
                };
                ResponseDetail responseDetail;
                var validatedAccessToken = RestConsumer.Instance.Get<ValidatedAccessTokenV2>(urlParams,
                    out responseDetail, queryStringParams);
                if (validatedAccessToken == null)
                {
                    var error = string.Format("Null response from calling the API.");
                    Log.Info("LogonManager.ProcessSuaResponse - " + error);
                    return CookieUtils.NULL_INT;
                }
                return validatedAccessToken.MemberId;
            }
            catch (Exception exception)
            {
                Log.Error("LogonManager.ProcessSuaResponse.", exception);
            }
            return CookieUtils.NULL_INT;
        }

        /// <summary>
        ///     This method is called from SUA autologin
        ///     The method calls AuthenticatAccessToken on the API to not only
        ///     validate but also processes post logon attributes.
        /// </summary>
        /// <returns></returns>
        private int AuthenticateAccessToken()
        {
            try
            {
                var accessTokenCookie = CookieUtils.GetCookieFromRequestOrResponse(CookieSuaAccessToken);
                if (accessTokenCookie == null) return CookieUtils.NULL_INT;
                var accessToken = accessTokenCookie.Value;

                var urlParams = new Dictionary<string, string>
                {
                    {"brandId", _brandId.ToString(CultureInfo.InvariantCulture)}
                };
                var queryStringParams = new Dictionary<string, string>
                {
                    {"access_Token", accessToken}
                };
                ResponseDetail responseDetail;
                var validatedAccessToken = RestConsumer.Instance.Get<AuthenticateAccessTokenResponseV2>(urlParams,
                    out responseDetail, queryStringParams);
                if (validatedAccessToken == null)
                {
                    var error = string.Format("Null response from calling the API.");
                    Log.Info("LogonManager.ProcessSuaResponse - " + error);
                    // this is crucial since an infinite loop can happen from the Reg site
                    RemoveTokenCookies();
                    return CookieUtils.NULL_INT;
                }

                if (!validatedAccessToken.Authenticated)
                    // this is crucial since an infinite loop can happen from the Reg site
                    RemoveTokenCookies();

                return validatedAccessToken.MemberId;
            }
            catch (Exception exception)
            {
                Log.Error("LogonManager.ProcessSuaResponse.", exception);
            }
            return CookieUtils.NULL_INT;
        }

        private void RecordLoginArrivedAtClientSystem()
        {
            string loginSessionId = CookieUtils.GetCookie(CookieSuaLoginSession, string.Empty, false);
            string state = CookieUtils.GetCookie(CookieSuaState, string.Empty, false);
            string template = PostLogonProcessor.SiteName;
            string ipAddress = GetClientIP();

            var request = new ActivityWithParametersRequest { MemberId = 0, TargetMemberId = 0, ActionType = ActionTypeLoginArrivedAtClientSystem };
            request.Parameters.Add("LoginSessionId", loginSessionId);
            request.Parameters.Add("LoginTemplate", template);
            request.Parameters.Add("LoginStateValue", state);
            request.Parameters.Add("OriginIPAddress", ipAddress);

            RecordActivity(request);
        }


        private void RecordLoginSentBackToClient(string stateFromRequest, bool statesMatch, int memberId)
        {
            string loginSessionId = CookieUtils.GetCookie(CookieSuaLoginSession, string.Empty, false);
            string ipAddress = GetClientIP();

            var request = new ActivityWithParametersRequest { MemberId = memberId, TargetMemberId = 0, ActionType = ActionTypeLoginSentBackToClient };
            request.Parameters.Add("LoginSessionId", loginSessionId);
            request.Parameters.Add("LoginStateValuesMatch", statesMatch.ToString(CultureInfo.InvariantCulture));
            request.Parameters.Add("LoginStateValue", stateFromRequest);
            request.Parameters.Add("OriginIPAddress", ipAddress);

            RecordActivity(request);
        }

        private void RecordActivity(ActivityWithParametersRequest activityRequest)
        {
            ResponseDetail responseDetail;

            var urlParams = new Dictionary<string, string>
            {
                {"brandId", _brandId.ToString(CultureInfo.InvariantCulture)}
            };
            var queryStringParams = new Dictionary<string, string>
            {
                {"applicationId", PostLogonProcessor.ApplicationID.ToString(CultureInfo.InvariantCulture)},
                {"client_secret", PostLogonProcessor.ClientSecret}
            };

            try
            {
                var response = RestConsumer.Instance.Post<ActivityWithParametersRequest>(activityRequest, urlParams,
                    out responseDetail, queryStringParams);
            }
            catch (Exception exception)
            {
                Log.Error("LogonManager.RecordActivity.", exception);
            }
        }

        private string GetClientIP()
        {
            return HttpContext.Current.Request.Headers["client-ip"] ?? HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
        }

        /// <summary>
        ///     If admin imerpsonate, ignore SUA.
        /// </summary>
        public void AdminImpersonate()
        {
            PostLogonProcessor.AdminImpersonate();
        }

        public static OAuthTokens GetTokensFromCookies()
        {
            var tokens = new OAuthTokens();
            var cookie = CookieUtils.GetCookieFromRequestOrResponse(CookieSuaMemberId);
            if (cookie != null)
            {
                int memberId;
                if (Int32.TryParse(cookie.Value, out memberId))
                {
                    tokens.MemberId = memberId;
                }
            }

            cookie = CookieUtils.GetCookieFromRequestOrResponse(CookieSuaAccessToken);
            if (cookie != null)
            {
                tokens.AccessToken = cookie.Value;
            }

            cookie = CookieUtils.GetCookieFromRequestOrResponse(CookieSuaIsSubscriber);
            if (cookie != null)
            {
                tokens.IsPayingMember = cookie.Value == "Y";
            }

            cookie = CookieUtils.GetCookieFromRequestOrResponse(CookieSuaAccessTokenExpiration);
            if (cookie != null)
            {
                var expireTimeStr = cookie.Value;
                DateTime accessExpires;
                if (DateTime.TryParse(expireTimeStr, out accessExpires))
                {
                    tokens.ExpiresIn = (int)(accessExpires - DateTime.Now).TotalSeconds;
                }
            }

            return tokens;
        }

        public void RemoveTokenCookies()
        {
            var domainCookie = CookieUtils.GetCookieFromRequestOrResponse(CookieSuaDomain);
            var domain = (null != domainCookie) ? domainCookie.Value : null;
            // kill the autologin cookie
            CookieUtils.RemoveCookie(CookieSuaAccessToken, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaAccessTokenExpiration, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaMemberId, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaUserId, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaIsSubscriber, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaDomain, domain, RootPath);
            CookieUtils.RemoveCookie(CookieBetaRedesign, domain, RootPath);
        }

        public static void RemoveTokenCookiesSua()
        {
            var domainCookie = CookieUtils.GetCookieFromRequestOrResponse(CookieSuaDomain);
            var domain = (null != domainCookie) ? domainCookie.Value : null;
            // kill the autologin cookie
            CookieUtils.RemoveCookie(CookieSuaAccessToken, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaAccessTokenExpiration, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaMemberId, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaUserId, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaIsSubscriber, domain, RootPath);
            CookieUtils.RemoveCookie(CookieSuaDomain, domain, RootPath);
            CookieUtils.RemoveCookie(CookieBetaRedesign, domain, RootPath);
        }
    }
}