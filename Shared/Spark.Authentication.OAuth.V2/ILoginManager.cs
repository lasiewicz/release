﻿using System.Web;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.OAuth2;

namespace Spark.Authentication.OAuth.V2
{
    public interface ILoginManager
    {
        OAuthTokens GetTokensViaPassword(string email, string password, bool passwordIsEncrypted, int brandId);
        OAuthTokens GetTokensViaRefresh(int memberId, string refreshToken, int brandId);
        OAuthTokens GetTokensViaPassword(string email, string password, bool passwordIsEncrypted, int brandId, int applicationId, string clientSecret, out ResponseDetail responseDetail);
        OAuthTokens GetTokensViaRefresh(int memberId, string refreshToken, int brandId, int applicationId, string clientSecret);
        OAuthTokens GetTokensFromCookies();
        void SetSubscriberCookie(bool isSubscriber, string oauthCookieDomain);
        void SaveTokensToCookies(HttpResponse response, OAuthTokens tokens, bool useRememeberMeMinutes, string email, string brandDomain);
        void SaveTokensToCookies(HttpResponse response, OAuthTokens tokens, bool useRememeberMeMinutes, string email, string environment, string brandDomain);
        OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted, bool useRememberMeExpirationTime, string brandDomain, int brandId);
        OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted, bool useRememberMeExpirationTime, string environment, string brandDomain, int brandId);
        OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted, bool useRememberMeExpirationTime, string environment, string brandDomain, int brandId, int applicationId, string clientSecret);
        void RemoveTokenCookies(string oauthCookieDomain);
        bool UserPreviouslyLoggedIn();
    }
}
