﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Spark.Logger.Tests
{
    [TestFixture]
    public class SparkLoggerLoadTests
    {
        public Random random = new Random();
        public static int count = 0;
        private string[] IgnoreFormFields = new[]
        {
            "password", "username", "birthdate", "religion", "creditcard", "ssn", "lastname"

        };

        [SetUp]
        public void Setup()
        {
            count = 0;
            AssemblyUtilities.SetEntryAssembly();
        }

        [Test]
        public void SendThreadedLoggingRequestsNoSleep()
        {
            ISparkLogger sparkLogger = SparkLoggerManager.Singleton.GetSparkLogger("RollingWebLogFileAppender");

            for (int i = 0; i < 100; i++)
            {
                Utils.FireAndForget(o =>
                {
                    try
                    {
                        Object obj = null;
                        obj.ToString();
                    }
                    catch (Exception e)
                    {
                        count++;
                        sparkLogger.LogError("Object obj"+count+" is null!", e, new Dictionary<string, string>() { { "name", sparkLogger.ToString() } });
                    }                    
                });
            }
            Thread.Sleep((5)*(60)*(1000));
        }

        [Test]
        public void SendThreadedLoggingRequestsWithSleep()
        {
            ISparkLogger sparkLogger = SparkLoggerManager.Singleton.GetSparkLogger("RollingWebLogFileAppender");

            for (int i = 0; i < 100; i++)
            {
                Utils.FireAndForget(o =>
                {
                    try
                    {
                        Object obj = null;
                        obj.ToString();
                    }
                    catch (Exception e)
                    {
                        count++;
                        int length = (i%IgnoreFormFields.Length);
                        sparkLogger.LogError("Object obj" + count + " is null!", e, new Dictionary<string, string>() { { "name", sparkLogger.ToString() } }, true, RandomStringsGenerator(length));
                    }
                });
                Thread.Sleep(random.Next(500));
            }
            Thread.Sleep((5) * (60) * (1000));
        }

        private string[] RandomStringsGenerator(int length)
        {
            string [] randomStrings = new string[length];

            for (int i = 0; i < length; i++)
            {
//                string path = Path.GetRandomFileName();
//                path = path.Replace(".", ""); // Remove period.
//                randomStrings[i]=path.Substring(0, 8);  // Return 8 character string                
                randomStrings[i] = IgnoreFormFields[i];
            }
            return randomStrings;
        }

    }
}
