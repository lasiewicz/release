﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using NUnit.Framework;
using Spark.Logger.Tests.Mocks;

namespace Spark.Logger.Tests
{
    [TestFixture]
    public class SparkLoggerTests
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            AssemblyUtilities.SetEntryAssembly();
        }

        [Test]
        public void TestLogInfoMessage()
        {
            ISparkLogger sparkLogger = SparkLoggerManager.Singleton.GetSparkLogger(typeof (SparkLoggerTests));
            sparkLogger.LogInfoMessage("This info is in logfiles dir",new Dictionary<string, string>(){{"name",sparkLogger.ToString()}}, true);
        }

        [Test]
        public void TestLogError()
        {
            ISparkLogger sparkLogger = SparkLoggerManager.Singleton.GetSparkLogger("RollingWebLogFileAppender");
            try
            {
                Object o = null;
                o.ToString();
            }
            catch (Exception e)
            {
                sparkLogger.LogError("Object o is null!", e, new Dictionary<string, string>() { { "name", sparkLogger.ToString() } });
            }
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void TestIncorrectLogFileDir()
        {
            SparkLoggerManager.XMLConfigPath = Path.Combine(Environment.CurrentDirectory, "testConfig/App2.Config");
            SparkLoggerManager.Reconfigure();
            
            ISparkLogger sparkLogger = SparkLoggerManager.Singleton.GetSparkLogger("RollingWebLogFileAppender");
        }

        [Test]
        public void TestLogErrorIgnoreFieldNames()
        {
            ISparkLogger sparkLogger = SparkLoggerManager.Singleton.GetSparkLogger("RollingWebLogFileAppender");
            try
            {
                Object o = null;
                o.ToString();
            }
            catch (Exception e)
            {
                sparkLogger.LogError("Object o is null!", e, new Dictionary<string, string>() { { "name", sparkLogger.ToString() } }, true, new string [] {"email","pass"});
            }
        }

        private HttpContextBase GetMockHttpContext()
        {
            string urlHost = "http://api.local.spark.net";
            string urlPath = "/v2/brandId/1003/profile/miniProfile/0";
            string queryString = "access_token=1%2FF6BgNEpHyz%2BAjgjTR9An8X7E8%2F16DA3FeqgVbYF%2BIe4%";
            var stringWriter = new StringWriter();
            HttpResponse httpResponse = new HttpResponse(stringWriter);
            httpResponse.StatusCode = (int)HttpStatusCode.OK;
            string originIp = "192.168.1.141";
            var mockHttpContext = MockHttpContextFactory.GetMockHttpContext(urlHost + urlPath, queryString, originIp,
                new HttpResponseWrapper(httpResponse));
            return mockHttpContext;
        }

    }
}
