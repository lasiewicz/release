﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Spark.Logger
{
    public interface ISparkLogger
    {
        bool IsDebugEnabled { get; }
        bool IsInfoEnabled { get; }
        bool IsWarnEnabled { get; }
        bool IsErrorEnabled { get; }
        bool IsFatalEnabled { get; }

        void LogDebugMessage(string message, Dictionary<string, string> customData, bool shouldLogExternally = false, string[] ignoreFormFields = null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0);

        void LogInfoMessage(string message, Dictionary<string, string> customData, bool shouldLogExternally = false, string[] ignoreFormFields = null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0);

        void LogWarningMessage(string message, Dictionary<string, string> customData, bool shouldLogExternally = false, string[] ignoreFormFields = null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0);

        void LogError(string message, Exception exception, Dictionary<string, string> customData,
            bool shouldLogExternally = true, string[] ignoreFormFields = null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0);

        void LogException(string message, Exception exception, Dictionary<string, string> customData = null, bool shouldLogExternally = true, string[] ignoreFormFields = null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0);
    }
}
