using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using log4net;
using Mindscape.Raygun4Net;
using System.Web; // required per Raygun support for capturing context info
using Mindscape.Raygun4Net.Messages;

namespace Spark.Logger
{
    public class SparkLogger : ISparkLogger
    {
        private ILog _log = null;
        private RaygunClient _raygunClient = null;
        private ConcurrentDictionary<string, string> _ignoredFormFields = null;
        private string AppName = string.Empty; 
//        private CircuitBreaker breaker = new CircuitBreaker();
        
        public HttpContext Context
        {
            get { return HttpContext.Current; }
        }

        public HttpContextBase ContextBase
        {
            get { return new HttpContextWrapper(Context);}
        }

        public SparkLogger(ILog log, RaygunClient raygunClient, ConcurrentDictionary<string, string> ignoredFormFields)
        {
            _log = log;
            _raygunClient = raygunClient;
            if (null == _raygunClient)
            {
                _log.Warn("Excepions will not be sent to Raygun.io because api key is missing from config!!");
            }

            Assembly assembly = (null != Assembly.GetEntryAssembly())
                ? Assembly.GetEntryAssembly()
                : Assembly.GetCallingAssembly();
                AppName = assembly.GetName().Name;

            _ignoredFormFields = ignoredFormFields;
        }

        public bool IsDebugEnabled
        {
            get { return null != _log && _log.IsDebugEnabled; }
        }

        public bool IsInfoEnabled
        {
            get { return null != _log && _log.IsInfoEnabled; }
        }

        public bool IsWarnEnabled
        {
            get { return null != _log && _log.IsWarnEnabled; }
        }

        public bool IsErrorEnabled
        {
            get { return null != _log && _log.IsErrorEnabled; }
        }

        public bool IsFatalEnabled
        {
            get { return null != _log && _log.IsFatalEnabled; }
        }

        private bool IsRayGunEnabled()
        {
            return (null != _raygunClient);
        }

        private void SetupDefaultCustomData(Dictionary<string, string> customData, string methodName, string className)
        {
            customData["App"] = this.AppName;
            customData["Class"] = className;
            customData["MethodName"] = methodName;
            customData["MachineName"] = System.Environment.MachineName;
            if (null != Context && null != Context.Response)
            {
                customData["StatusCode"] = Context.Response.StatusCode.ToString();
            }
        }

        private void AddIgnoredFormFields(string[] ignoreFormFields)
        {
            //don't add already added ignored form fields to raygun client. otherwise, string list in client will keep growing.
            if (null != ignoreFormFields)
            {
                List<string> arr = null;
                foreach (var formField in ignoreFormFields)
                {
                    if (!_ignoredFormFields.ContainsKey(formField))
                    {
                        if (null == arr) arr = new List<string>();
                        arr.Add(formField);
                        _ignoredFormFields.TryAdd(formField, formField);
                    }
                }

                if (null != arr)
                {
                    _raygunClient.IgnoreFormFieldNames(arr.ToArray());
                }
            }
        }

        private RaygunIdentifierMessage CreateRaygunIdentifierMessage(Dictionary<string, string> customData)
        {
            RaygunIdentifierMessage raygunIdentifierMessage = null;
            if (null != customData && customData.ContainsKey("MemberID"))
            {
                string user = customData["MemberID"];
                raygunIdentifierMessage = new RaygunIdentifierMessage(user);
                raygunIdentifierMessage.UUID = user;
            }

            if (null != customData && customData.ContainsKey("Email"))
            {
                string email = customData["Email"];
                if (null == raygunIdentifierMessage) raygunIdentifierMessage = new RaygunIdentifierMessage(email);
                raygunIdentifierMessage.Email = email;
            }

            if (null == raygunIdentifierMessage)
            {
                raygunIdentifierMessage = _raygunClient.UserInfo ??
                                          (!String.IsNullOrEmpty(_raygunClient.User)
                                              ? new RaygunIdentifierMessage(_raygunClient.User)
                                              : null);
            }
            return raygunIdentifierMessage;
        }

        private void SendExceptionExternally(Exception exception, Dictionary<string, string> customData, string message = null, string [] ignoreFormFields=null)
        {
            if (IsRayGunEnabled())
            {
                AddIgnoredFormFields(ignoreFormFields);
                //building raygun message outside of ThreadWorkerQueue in order to capture current Request object
                List<string> tags = customData.Values.ToList();
                if (!string.IsNullOrEmpty(message))
                {
                    customData.Add("Message", message);
                }

                var raygunMessage = RaygunMessageBuilder.New.SetHttpDetails(Context)
                .SetEnvironmentDetails()
                .SetMachineName(Environment.MachineName)
                .SetExceptionDetails(exception)
                .SetClientDetails()
                .SetVersion(_raygunClient.ApplicationVersion)
                .SetTags(tags)
                .SetUserCustomData(customData)
                .SetUser(CreateRaygunIdentifierMessage(customData))                
                .Build();

//                CircuitBreaker.Apply send = delegate()
//                {                    
//
//                    _raygunClient.Send(raygunMessage);
//                };
//
//                try
//                {
//                    breaker.Intercept(send);
//                }
//                catch (Exception e)
//                {
//                    _log.Warn("Raygun.io Send Failed!", e);
//                }

                QueueProcessor.Singleton.AddPacketToQueue(raygunMessage, _log);
            }
        }


        private void SendMessageExternally(string message, Dictionary<string, string> customData, string[] ignoreFormFields = null)
        {
            if (IsRayGunEnabled())
            {
                AddIgnoredFormFields(ignoreFormFields);

                //building raygun message outside of ThreadWorkerQueue in order to capture current Request object
                List<string> tags = customData.Values.ToList();
                var raygunMessage = RaygunMessageBuilder.New.SetHttpDetails(Context)
                .SetEnvironmentDetails()
                .SetMachineName(Environment.MachineName)
                .SetExceptionDetails(new Exception(message))
                .SetClientDetails()
                .SetVersion(_raygunClient.ApplicationVersion)
                .SetTags(tags)
                .SetUserCustomData(customData)
                .SetUser(CreateRaygunIdentifierMessage(customData))
                .Build();

//                CircuitBreaker.Apply send = delegate()
//                {
//                    _raygunClient.Send(raygunMessage);
//                };
//
//                try
//                {
//                    breaker.Intercept(send);
//                }
//                catch (Exception e)
//                {
//                    _log.Warn("Raygun.io Send Failed!", e);
//                }

                QueueProcessor.Singleton.AddPacketToQueue(raygunMessage, _log);
            }
        }

        private string GetClassName(string classFile)
        {
            if (string.IsNullOrEmpty(classFile)) return string.Empty;
            var dirs = classFile.SplitDirs();
            string className = (null != dirs && dirs.Length > 1) ? dirs[dirs.Length - 1] : string.Empty;
            return className;
        }

        public void LogDebugMessage(string message,
            Dictionary<string, string> customData,
            bool shouldLogExternally = false,
            string [] ignoreFormFields=null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (null == customData) customData = new Dictionary<string, string>();
            var className = GetClassName(classFile);
            SetupDefaultCustomData(customData, methodName, className);
            string customValues = customData.ToString("=", ", ");
            _log.DebugFormat("App:{0}, Class:{1}, Method:{2}, Line #:{3}, Message:{4}, Data:{5}", this.AppName, className, methodName, sourceLineNumber, message, customValues);

            if (shouldLogExternally)
            {
                //sends message to external logger
                SendMessageExternally(message, customData, ignoreFormFields);
            }
        }


        public void LogInfoMessage(string message, 
            Dictionary<string, string> customData,
            bool shouldLogExternally = false,
            string[] ignoreFormFields = null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (null == customData) customData = new Dictionary<string, string>();
            var className = GetClassName(classFile);
            SetupDefaultCustomData(customData, methodName, className);
            string customValues = customData.ToString("=", ", ");
            _log.InfoFormat("App:{0}, Class:{1}, Method:{2}, Line #:{3}, Message:{4}, Data:{5}", this.AppName, className, methodName, sourceLineNumber, message, customValues);

            if (shouldLogExternally)
            {
                //sends message to external logger
                SendMessageExternally(message, customData, ignoreFormFields);
            }
        }

        public void LogWarningMessage(string message,
            Dictionary<string, string> customData,
            bool shouldLogExternally = false,
            string[] ignoreFormFields = null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (null == customData) customData = new Dictionary<string, string>();
            var className = GetClassName(classFile);
            SetupDefaultCustomData(customData, methodName, className);
            string customValues = customData.ToString("=", ", ");
            _log.WarnFormat("App:{0}, Class:{1}, Method:{2}, Line #:{3}, Message:{4}, Data:{5}", this.AppName, className, methodName, sourceLineNumber, message, customValues);

            if (shouldLogExternally)
            {
                //sends message to external logger
                SendMessageExternally(message, customData, ignoreFormFields);
            }
        }

        public void LogError(string message, 
            Exception exception, 
            Dictionary<string, string> customData, 
            bool shouldLogExternally = true,
            string[] ignoreFormFields = null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (null == customData) customData = new Dictionary<string, string>();
            var className = GetClassName(classFile);
            SetupDefaultCustomData(customData, methodName, className);
            string customValues = customData.ToString("=", ", ");
            _log.Error(string.Format("App:{0}, Class:{1}, Method:{2}, Line #:{3}, Message:{4}, Data:{5}", this.AppName, className, methodName, sourceLineNumber, message, customValues), exception);

            if (shouldLogExternally)
            {
                //sends exception to external logger
                SendExceptionExternally(exception, customData, message, ignoreFormFields);
            }
        }

        public void LogException(string message,
            Exception exception,
            Dictionary<string, string> customData = null,
            bool shouldLogExternally = true,
            string[] ignoreFormFields = null,
            [CallerMemberName] string methodName = "",
            [CallerFilePath] string classFile = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (null == customData) customData = new Dictionary<string, string>();
            var className = GetClassName(classFile);
            SetupDefaultCustomData(customData, methodName, className);
            string customValues = customData.ToString("=", ", ");
            _log.Error(string.Format("App:{0}, Class:{1}, Method:{2}, Line #:{3}, Message:{4}, Data:{5}", this.AppName, className, methodName, sourceLineNumber, message, customValues), exception);

            if (shouldLogExternally)
            {
                //sends exception to external logger
                SendExceptionExternally(exception, customData, message, ignoreFormFields);
            }
        }

        public override string ToString()
        {
            return _log.Logger.Name;
        }
    }
}