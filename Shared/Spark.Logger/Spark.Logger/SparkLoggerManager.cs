﻿using System.Collections.Concurrent;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Threading;
using log4net;
using log4net.Appender;
using log4net.Config;
using Mindscape.Raygun4Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; // required per Raygun support for capturing context info

namespace Spark.Logger
{
    //TODO: make env a tag instead of in the raygun.io app name.

     public class SparkLoggerManager
    {
        public const string LOGFILE_DIRECTORY = "logfiles";
        public static readonly SparkLoggerManager Singleton = new SparkLoggerManager();
        private RaygunClient _raygunClient = null;
        private string[] GlobalIgnoredFormFields = new string[] { "password" };
        private ConcurrentDictionary<string, string> _ignoredFormFields = new ConcurrentDictionary<string, string>();

#if DEBUG
        public static string XMLConfigPath { get; set; }

         public static void Reconfigure()
        {
            LogManager.ResetConfiguration();
            QueueProcessor.Singleton.Stop();
            Singleton.Configure();
        }
#endif

             private SparkLoggerManager()
        {
            Configure();            
        }

        private void Configure()
        {
            log4net.GlobalContext.Properties["LogFileFolder"] = @"c:/" + LOGFILE_DIRECTORY; //log file path 
#if DEBUG
            if (!string.IsNullOrEmpty(XMLConfigPath))
            {
                XmlConfigurator.Configure(new FileInfo(XMLConfigPath));
            }
            else
            {
#endif
                XmlConfigurator.Configure();
#if DEBUG
            }
#endif
            var appenders = LogManager.GetRepository().GetAppenders();
            foreach (var appender in appenders)
            {
                if (appender is FileAppender)
                {
                    var file = ((FileAppender)appender).File;
                    var dirs = file.SplitDirs();
                    if (null == dirs || dirs.Length < 2 || (dirs[0] != LOGFILE_DIRECTORY && dirs[1] != LOGFILE_DIRECTORY))
                    {
                        throw new ArgumentException(string.Format("Incorrect logfile directory at {0}. Log file dir must be c:/{1}.", file, LOGFILE_DIRECTORY));
                    }
                }
            }

            //do not create client if api key is missing
            try
            {
                _raygunClient = new RaygunClient();
            }
            catch (Exception ignore) { }

            _ignoredFormFields.Clear();
            foreach (var formField in GlobalIgnoredFormFields)
            {
                _ignoredFormFields.TryAdd(formField, formField);
            }
            // add global ignored form fields
            _raygunClient.IgnoreFormFieldNames(GlobalIgnoredFormFields);
            //filter out unhelpful exceptions to not send to raygun.io
            _raygunClient.SendingMessage += (sender, args) => { if (null == args.Message) args.Cancel = true; };
            
            //init queue
            QueueProcessor.Singleton.Init(_raygunClient);
        }

        /// <summary>
        /// Use this method to retreive a spark logger for your Class type.
        /// Usage: <code>private ISparkLogger _log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MyClass));</code>
        /// </summary>
        /// <param name="classType">Type of class for logger</param>
        /// <returns></returns>
        public ISparkLogger GetSparkLogger(Type classType)
        {
            return new SparkLogger(LogManager.GetLogger(classType), _raygunClient, _ignoredFormFields);
        }

        /// <summary>
        /// Use this method to retrieve a spark logger by name. The name of the logger would be defined in your xml file.
        /// Usage: <code>private ISparkLogger _log = SparkLoggerManager.Singleton.GetSparkLogger("MyLoggerName");</code>
        /// </summary>
        /// <param name="name">name of logger to retrieve</param>
        /// <returns></returns>
        public ISparkLogger GetSparkLogger(string name)
        {
            return new SparkLogger(LogManager.GetLogger(name), _raygunClient, _ignoredFormFields);
        }
    }

    public static class DictionaryExtensions
    {
        public static string ToString(this Dictionary<string, string> source, string keyValueSeparator, string sequenceSeparator)
        {
            if (source == null) throw new ArgumentException("Parameter source can not be null.");
            var pairs = source.Select(x => string.Concat(x.Key, keyValueSeparator, x.Value));
            return string.Join(sequenceSeparator, pairs);
        }
    }

    public static class StringExtensions
    {
        public static string[] SplitDirs(this string source)
        {
            if (string.IsNullOrEmpty(source)) throw new ArgumentException("Parameter source can not be null.");
            return source.Split(new[] { "\\", "/" }, StringSplitOptions.RemoveEmptyEntries);
        }
    }

    public static class Utils
    {
        public static bool FireAndForget(WaitCallback callback)
        {
            return ThreadPool.QueueUserWorkItem(o =>
            {
                try
                {
                    callback(o);
                }
                catch (Exception e)
                {
                }
            });
        }

        public static int GetIntSetting(string intSetting, int defaultValue)
        {
            try
            {
                var setting = ConfigurationManager.AppSettings[intSetting];
                if (!string.IsNullOrEmpty(setting))
                {
                    return Int32.Parse(setting);
                }
            }
            catch (Exception ignore)
            { }
            return defaultValue;
        }
        
    }
}
