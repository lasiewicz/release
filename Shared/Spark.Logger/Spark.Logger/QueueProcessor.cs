﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Mindscape.Raygun4Net;
using Mindscape.Raygun4Net.Messages;

namespace Spark.Logger
{
    public class QueueProcessor : IDisposable
    {

        private BlockingCollection<RaygunPacket> _raygunPackets = null;
        private Task[] _tasks;
        public static readonly QueueProcessor Singleton = new QueueProcessor();
        private RaygunClient _raygunClient = null;
        private Thread _process = null;

        public int MaxTaskCount
        {
            get
            {
                return Utils.GetIntSetting("Spark.Logger.MaxTaskCount", 5);
            }
        }


        private QueueProcessor()
        {}

        public void Init(RaygunClient client)
        {
            _raygunClient = client;
            _tasks = new Task[MaxTaskCount];
            _raygunPackets = new BlockingCollection<RaygunPacket>();
            _process = new Thread(ProcessPackets);
            _process.IsBackground = true;
            _process.Start();
        }

        public void AddPacketToQueue(RaygunMessage message, ILog log)
        {
            AddPacketToQueue(new RaygunPacket(log, message));
        }

        public void AddPacketToQueue(RaygunPacket raygunPacket)
        {
            //check for exception
            try
            {
                bool result = _raygunPackets.TryAdd(raygunPacket);
            }
            catch (Exception e)
            {
                if (null != raygunPacket)
                {
                    raygunPacket.Log.Error(e);
                    Thread.Sleep(1000);
                    Task t = new Task(() => _raygunClient.Send(e));
                    t.Start();
                    _raygunPackets.TryAdd(raygunPacket);
                }
            }
        }

        public void ProcessPackets()
        {
            foreach (var raygunPacket in _raygunPackets.GetConsumingEnumerable())
            {
                try
                {
                    Task task = GetCompletedTask(raygunPacket);
                    if (null != task)
                    {
                        task.Start();
                    }
                    else
                    {
#if DEBUG
                       raygunPacket.Log.DebugFormat("All {0} tasks in use, {1} messages in queue.", _tasks.Length, _raygunPackets.Count);
#endif

                        Thread.Sleep(150);
                        AddPacketToQueue(raygunPacket);
                    }
                }
                catch (Exception e)
                {
                    if (null != raygunPacket)
                    {
                        raygunPacket.Log.Error(e);
                        if (!raygunPacket.IsSent)
                        {
                            AddPacketToQueue(raygunPacket);
                        }
                    }
                }
            }
        }

        private Task GetCompletedTask(RaygunPacket packet)
        {
            Task t = null;
            for(int i=0; i < _tasks.Length; i++)
            {
                Task task = _tasks[i];
                if (null == task || task.IsCompleted)
                {
                    //cannot reuse Task so have to create new ones when old ones complete
                    t=new Task(() => SendMessage(packet), TaskCreationOptions.None);
                    _tasks[i] = t;
#if DEBUG
                    packet.Log.DebugFormat("Task at idx {0} used.", i);
#endif

                    break;
                }
            }
            return t;
        }

        private void SendMessage(RaygunPacket packet)
        {
            try
            {
                _raygunClient.Send(packet.Message);
                packet.IsSent = true;
            }
            catch (Exception e)
            {
                packet.Log.Warn("Raygun.io Send Failed!", e);
            }
        }

        public void Stop()
        {
            _raygunPackets.Dispose();
            _process.Abort();
            _process = null;
        }

        public void Dispose()
        {
            Stop();
        }
    }

    public class RaygunPacket
    {
        public RaygunMessage Message { get; private set; }
        public ILog Log { get; private set;}
        public bool IsSent { get; set; }
        public RaygunPacket(ILog log, RaygunMessage message)
        {
            Log = log;
            Message = message;
        }
    }
}
