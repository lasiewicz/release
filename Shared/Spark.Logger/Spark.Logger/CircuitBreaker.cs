﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;

namespace Spark.Logger
{
    public class CircuitBreaker
    {
        internal readonly object monitor = new object();

        public delegate void Apply();

        private CircuitBreakerState _state;
        private List<DateTime> failures;
        private static int runningThreadCount = 0;

        private static void IncrementRunningThreadCount()
        {
            runningThreadCount++;
        }
        
        private static void DecrementRunningThreadCount()
        {
            runningThreadCount--;
            if (runningThreadCount < 0)
            {
                runningThreadCount = 0;
            }
        }

        public int MaxRunningThreadCount
        {
            get
            {
                return Utils.GetIntSetting("Spark.Logger.MaxRunningThreads", 65);
            }
        }

        public int MaxExceptionCount
        {
            get
            {
                return Utils.GetIntSetting("Spark.Logger.MaxExceptionCount", 10);
            }
        }

        public TimeSpan ExceptionDuration
        {
            get
            {
                int maxExceptionDurationSeconds = Utils.GetIntSetting("Spark.Logger.ExceptionDurationInSeconds", 5);
                return new TimeSpan(0, 0, maxExceptionDurationSeconds);
            }
        }

        public TimeSpan OpenStateDuration
        {
            get
            {
                int openStateDurationInSeconds = Utils.GetIntSetting("Spark.Logger.OpenStateDurationInSeconds", 5);
                return new TimeSpan(0, 0, openStateDurationInSeconds);
            }
        }

        public int FailureCount
        {
            get { return failures.Count; }
        }

        public string State
        {
            get { return _state.GetType().Name; }
        }

        /// <summary>  
        /// Initializes a new instance of the CircuitBreaker class.  
        /// </summary>  
        public CircuitBreaker()
        {
            this.failures = new List<DateTime>();
            runningThreadCount = 0;
            this.MoveToClosedState();
        }

        /// <summary>  
        /// Intercepts the planned method invocation.  
        /// </summary>  
        /// <param name="invocation">Original method call.</param>  
        public void Intercept(Apply apply)
        {
            Exception thrownException = null;

            try
            {
                //Increment count of threads created.  If threshold is met, stop creating threads
                IncrementRunningThreadCount();
                _state.ProtectedCodeIsAboutToBeCalled();
                _state.ActUponThreshold();
                
                //If threshold is not met, create thread for call.
                ThreadPool.QueueUserWorkItem(o =>
                {
                    try
                    {
                        //Check inside thread before attempting to run method
                        _state.ProtectedCodeIsAboutToBeCalled();
                        apply();
                    }
                    catch (Exception e)
                    {
                        thrownException = e;
                        using (TimedLock.Lock(monitor))
                        {
                            // Add current datetime to list of failed invocations.  
                            failures.Add(DateTime.UtcNow);
                        }
                    }
                    finally
                    {
                        //Always decrement here since call is done, even if exception (timeout) is thrown
                        DecrementRunningThreadCount();
                        //At this point check threshold since call and exception count has changed
                        _state.ActUponThreshold();
                        _state.ProtectedCodeHasBeenCalled();
//                        Console.WriteLine("[IN THREAD] Open Api Calls = " + runningThreadCount);
                    }
                });

            }
            catch (Exception e)
            {
                //Always decrement here since call is done, most likely since circuit is open
                DecrementRunningThreadCount();
//                Console.WriteLine("[IN EXCEPTION] Open Api Calls = " + runningThreadCount +" , "+e.Message);
                throw;
            }

            _state.ProtectedCodeHasBeenCalled();
        }

        private void MoveToClosedState()
        {
            if (!(_state is ClosedState))
                _state = new ClosedState(this);
        }

        private void MoveToOpenState()
        {
            if (!(_state is OpenState))
                _state = new OpenState(this);
        }

        private void MoveToHalfOpenState()
        {
            if (!(_state is HalfOpenState))
                _state = new HalfOpenState(this);
        }

        private void ResetFailureList()
        {
            this.failures.Clear();
            runningThreadCount = 0;
        }

        private bool ThresholdReached()
        {
            // Remove log of any failed invocations that occurred earlier than period in which we are interested.  
            this.failures.RemoveAll(f => f < DateTime.UtcNow - ExceptionDuration);

//            Console.WriteLine("[IN THRESHOLD CHECK] Open Api Calls = " + runningThreadCount);
            // Have number of open api calls or failures breached the allowed threshold?  
            return (runningThreadCount > MaxRunningThreadCount) || (failures.Count > MaxExceptionCount);
        }

        private abstract class CircuitBreakerState
        {
            protected readonly CircuitBreaker circuitBreaker;

            protected CircuitBreakerState(CircuitBreaker circuitBreaker)
            {
                this.circuitBreaker = circuitBreaker;
            }

            public virtual void ProtectedCodeIsAboutToBeCalled()
            {
            }

            public virtual void ProtectedCodeHasBeenCalled()
            {
            }

            public virtual void ActUponThreshold()
            {
            }
        }

        /// <summary>  
        /// Represents a closed CircuitBreaker - this is the "normal" behaviour,  
        ///  with invocations passed through to the protected code.  
        /// </summary>  
        private class ClosedState : CircuitBreakerState
        {
            public ClosedState(CircuitBreaker circuitBreaker)
                : base(circuitBreaker)
            {
                circuitBreaker.ResetFailureList();
            }

            /// <summary>  
            /// Called when an exception occurs in the protected code.  
            /// </summary>  
            /// <param name="e"></param>  
            public override void ActUponThreshold()
            {

                // If the threshold has been breached, open the circuit.  
                if (circuitBreaker.ThresholdReached())
                {
                    using (TimedLock.Lock(circuitBreaker.monitor))
                    {
                        circuitBreaker.MoveToOpenState();
                    }
                }
            }
        }

        /// <summary>  
        /// Represents an open CircuitBreaker - in this state we do not pass the   
        ///  invocation to the protected code, but instead throw OpenCircuitExceptions  
        ///  until the timeout expires.  
        /// </summary>  
        private class OpenState : CircuitBreakerState
        {
            private readonly DateTime expiry;

            public OpenState(CircuitBreaker circuitBreaker)
                : base(circuitBreaker)
            {
                // Keep a note of the time at which the circuit should be moved  
                // to half-open state.  
                expiry = DateTime.UtcNow + circuitBreaker.OpenStateDuration;                
            }

            public override void ProtectedCodeIsAboutToBeCalled()
            {
                // Has the timeout expired?  
                if (DateTime.UtcNow < expiry)
                {
                    // Not yet, so throw an eppy.  
                    string message = string.Format("RunningThreadCount/MaxRunningThreadCount:{0}/{1}, Exceptions/Duration:{2}/{3}", CircuitBreaker.runningThreadCount, circuitBreaker.MaxRunningThreadCount, circuitBreaker.FailureCount, circuitBreaker.MaxExceptionCount);
                    OpenCircuitException openCircuitException = new OpenCircuitException(message);
                    throw openCircuitException;
                }

                using (TimedLock.Lock(circuitBreaker.monitor))
                {
                    // yes, timeout has expired, so move to half-open state.  
                    circuitBreaker.MoveToHalfOpenState();
                }
            }
        }

        /// <summary>  
        /// Represents a CircuitBreaker in the "Half-Open" state.  
        /// In this state a timeout has just expired and we are tentatively calling  
        ///  the protected code again. If all goes well, we will move to the closed state.  
        /// However, if this first dipping of our electronic toe into the water is met by  
        ///  the shock of an unhandled exception, we shall quickly open the circuit again  
        ///  for a further timeout period.  
        /// </summary>  
        private class HalfOpenState : CircuitBreakerState
        {
            public HalfOpenState(CircuitBreaker circuitBreaker) : base(circuitBreaker)
            {
            }

            public override void ActUponThreshold()
            {
                using (TimedLock.Lock(circuitBreaker.monitor))
                {
                    // Eek, problems remain. Open the circuit again quickly.  
                    circuitBreaker.MoveToOpenState();
                }
            }

            public override void ProtectedCodeHasBeenCalled()
            {
                using (TimedLock.Lock(circuitBreaker.monitor))
                {
                    // Things seem to be OK now. Close the circuit.  
                    circuitBreaker.MoveToClosedState();
                }
            }
        }
    }

    public struct TimedLock : IDisposable
    {
        private readonly object target;

        private TimedLock(object o)
        {
            target = o;
        }

        public void Dispose()
        {
            Monitor.Exit(target);
        }

        public static TimedLock Lock(object o)
        {
            return Lock(o, TimeSpan.FromSeconds(5));
        }

        public static TimedLock Lock(object o, TimeSpan timeout)
        {
            return Lock(o, Convert.ToInt32(timeout.TotalMilliseconds));
        }

        public static TimedLock Lock(object o, int milliSeconds)
        {
            var timedLock = new TimedLock(o);
            if (!Monitor.TryEnter(o, milliSeconds))
            {
                throw new LockTimeoutException();
            }
            return timedLock;
        }
    }

    public class LockTimeoutException : ApplicationException
    {
        public LockTimeoutException() : base("Timeout waiting for lock")
        {
        }
    }

    public class OpenCircuitException : ApplicationException
    {
        public OpenCircuitException(string message) : base(string.Format("Circuit is still open: {0}", message))
        {
            
        }
    }
}

