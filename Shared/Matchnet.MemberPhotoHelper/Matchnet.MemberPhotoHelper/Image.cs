using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.ComponentModel;
using System.Text;
using System.IO;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.MemberPhotoHelper
{
    [DefaultProperty("FileName"), 
    ToolboxData("<{0}:Image runat=server></{0}:Image>"),
    Designer(typeof(ImageDesigner))]
    public class Image : System.Web.UI.WebControls.Image
    {
        public const String QSPARAM_SITEID = "?siteID=";
        #region Private/Protected Fields
        private string				_FileName = String.Empty;
        private string				_NavigateUrl = String.Empty;
        private string				_ImageUrl = String.Empty;
        //protected ContextGlobal		_g;
        protected Brand		        _brand;
        private Unit				_Hspace = Unit.Empty;
        private Unit				_Vspace = Unit.Empty;
        private Unit				_Border = Unit.Empty;
        private string				_defaultTitleResourceConstant = string.Empty;
        private string				_TitleResourceConstant = string.Empty;
        private string				_defaultResourceConstant = string.Empty;
        private string				_ResourceConstant = string.Empty;
        private APP _App = APP.None;
        private bool				_RollOver;
        protected bool				_found;
        private string				_NavigateURLClass;
        private string		_Communities;
        //private string[]	_CommunitiesArray;

        #endregion

        #region App ID
		
        public enum APP : int
        {
            None = 0,
            Home = 1,
            Logon = 2,
            Subscription = 3,
            MemberProfile = 7,
            Chat = 12,
            Article = 13,
            ContactUs = 14,
            SendToFriend = 16,
            MemberServices = 18,
            IM = 19,
            Search = 22,
            MembersOnline = 23,
            HotList = 26,
            Email = 27,
            Events = 28,
            Termination = 32,
            Captcha = 50,
            Tease = 51,
            MemberPhotos = 52,
            Error = 53,
            Offer = 62,
            Telephony = 63,
            Resource = 507,
            MemberSearch = 22,
            Report = 523,
            FreeTextApproval = 525,
            MemberList = 26,
            EditProfile = 539,
            PixelAdministrator = 545,
            Options = 549
        }
        #endregion

        #region Properties

        [Bindable(true), Category("Appearance"), DefaultValue("")] 
        public virtual string FileName 
        {
            get	{ return _FileName;	}
            set {_FileName = value; }
        }
		

        [Bindable(true), Category("Appearance"), DefaultValue("")] 
        public string NavigateURLClass 
        {
            get { return _NavigateURLClass; }
            set { _NavigateURLClass = value; }
        }

        [Bindable(true), Category("Appearance"), DefaultValue("")] 
        public override string ImageUrl
        {
            get 
            { 
                if (_ImageUrl == null || _ImageUrl == String.Empty)
                {
                    _ImageUrl = GetUrl(FileName, (int)_App, false);

                    //If we don't get back an Image URL, return a dummy path to avoid requests to Default.aspx
                    if (_ImageUrl == null || _ImageUrl == string.Empty)
                        _ImageUrl = ".gif";
                }
                return _ImageUrl;
            }
            set
            {
                _ImageUrl = value;
            }
        }

//        [Bindable(true), Category("Appearance"), DefaultValue("")] 
//        public string NavigateUrl 
//        {
//            get
//            {
//                return FrameworkGlobals.LinkHref(_NavigateUrl, true);
//            }
//
//            set
//            {
//                _NavigateUrl = value;
//            }
//        }

//        [Bindable(true), Category("Appearance"), DefaultValue("")] 
//        public APP App
//        {
//            get
//            {		
//				
//                if (_App == WebConstants.APP.None && (_g.AppPage.App.ID != (int)WebConstants.APP.None))
//                {				
//                    _App = (WebConstants.APP)_g.AppPage.App.ID;
//                }
//                return _App;
//            }
//
//            set
//            {
//                _App = value;
//            }
//        }

        [Bindable(true), Category("Appearance"), DefaultValue("")] 
        public Unit Hspace 
        {
            get { return _Hspace; }
            set { _Hspace = value;}
        }

        [Bindable(true), Category("Appearance"), DefaultValue("")] 
        public Unit Vspace 
        {
            get { return _Vspace; }
            set { _Vspace = value; }
        }


        [Bindable(true),  Category("Appearance"), DefaultValue("")] 
        public Unit Border
        {
            get { return _Border; }
            set { _Border = value; }
        }

        [Bindable(true), Category("Appearance"), DefaultValue("")] 
        public string ResourceConstant
        {
            get { return _ResourceConstant; }
            set { _ResourceConstant = value; }
        }

        [Bindable(true), Category("Appearance"),  DefaultValue("")] 
        public string TitleResourceConstant
        {
            get { return _TitleResourceConstant; }
            set { _TitleResourceConstant = value; }
        }

        [Bindable(true),  Category("Appearance"),  DefaultValue("")] 
        public bool RollOver 
        {
            get { return _RollOver; }
            set { _RollOver = value; }
        }

        public bool Found 
        {
            get { return _found; }
        }

        protected string DefaultTitleResourceConstant
        {
            get { return _defaultTitleResourceConstant; }
            set { _defaultTitleResourceConstant = value; }
        }

        protected string DefaultResourceConstant
        {
            get { return _defaultResourceConstant; }
            set { _defaultResourceConstant = value; }
        }

        [Bindable(true), 
        Category("Appearance"), 
        DefaultValue("")]
        public string Communities
        {
            get{return _Communities;}
            set{_Communities = value;}
        }

        #endregion

        #region Public Methods
		
        public Image(Brand brand)
        {
            _brand = brand;
            
            /*
            if ( Context != null )
            {
                if ( Context.Items["g"] != null )
                    _g = (ContextGlobal) Context.Items["g"];
            }
            else
            {
                _g = new ContextGlobal( null );
            }
	        */
        }


        public string GetUrl(string filename, int applicationID, bool fullyQualified) 
        {
            string path = "";

            //TODO: Refactor to allow path lookup without g (for API pages)
            if (_brand == null)
            {
                return path;
            }
			
            try
            {
                //				string key = "IMAGE:" + filename + ":" + applicationID + ":" + _g.Brand.BrandID;		
                //				path = System.Convert.ToString(Matchnet.CachingTemp.Cache.GetInstance().Get(key));
                path = ImageUrlCache.Instance.GetUrl(filename, applicationID, _brand.BrandID);

                if (path == string.Empty)
                {
                    path = FileSrc(filename, applicationID);
                    if(path.Length > 0) 
                    {
                        path = path.Replace(@"\", "/");
                    }
                    //					Random r = new Random();
                    //					Matchnet.CachingTemp.Cache.GetInstance().Insert(key, path, null, DateTime.Now.AddMinutes(r.Next(60,120)), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
                    if (_found)
                    {
                        ImageUrlCache.Instance.InsertUrl(path,filename, applicationID, _brand.BrandID);
                    }
                }

                if(fullyQualified) 
                {
                    if(!ImageUrlRoot.StartsWith("http://"))
                    {
                        path = _brand.Site.DefaultHost + _brand.Uri + path;
                    } 
                }
            }
            catch(Exception ex)
            {
                    
            }

            return path;
        }

        public static string GetURLFromFilename(string filename, Brand brand)
        {
            Image img = new Image(brand);
            img.FileName = filename;
            return img.ImageUrl;
        }

//        public static string GetBrandAliasImagePath(string filename, BrandAlias brandAlias)
//        {
//            string path = string.Empty;
//
//            if (brandAlias != null)
//            {
//                // check if brand alias img path is in the cache
//                path = ImageUrlCache.Instance.GetUrl(filename, Matchnet.Constants.NULL_INT, brandAlias.BrandAliasID);
//
//                if (path == string.Empty)
//                {
//                    path = "/Brand/" + GetPathFromUri(brandAlias.Uri) + "/" + filename;
//					
//                    // check for existence, then store in cache
//                    if(!System.IO.File.Exists(ImageRoot + path))
//                    {
//                        path = GetURLFromFilename(filename);
//                    }
//                    else
//                    {
//                        path = ImageUrlRoot + path;
//                    }
//
//                    ImageUrlCache.Instance.InsertUrl(path, filename, Matchnet.Constants.NULL_INT, brandAlias.BrandAliasID);
//                }
//
//                return path;
//            }
//            else
//            {
//                return GetURLFromFilename(filename);
//            }
//        }

        #endregion

        #region Protected Methods

//        protected override void AddAttributesToRender(HtmlTextWriter writer)
//        {
//            if (ImageUrl.Length > 0) 
//            {
//                if ( _ResourceConstant != string.Empty || _defaultResourceConstant != string.Empty)
//                {
//                    if (_ResourceConstant == string.Empty)
//                    {
//                        AlternateText = System.Web.HttpUtility.HtmlEncode(_g.GetResource(_defaultResourceConstant,null));
//                    }
//                    else
//                    {
//                        AlternateText = System.Web.HttpUtility.HtmlEncode(_g.GetResource(_ResourceConstant, this.Parent));
//                    }
//
//                    //					if (AlternateText == string.Empty) // Check if ALT tag exists similar checks below for #13825
//                    //					{
//                    //						AlternateText = resourceValue;
//                    //					}
//                    //					else
//                    //					{
//                    //						writer.AddAttribute(HtmlTextWriterAttribute.Alt,resourceValue,true);
//                    //					}
//                }
//
//                // Issue #8178 klandrus 10.22.04, decision is now to display nothing when resourceconstant or titleresourceconstant is null
//                // 6162 & 6213 lcarter 07.19.04 if no alt text, use FileName
//                //				else if( (base.AlternateText == null) && (FileName.Length != 0) )
//                //				{
//                //					writer.AddAttribute(HtmlTextWriterAttribute.Alt, string.Empty);
//                //				}
//                // 6162 & 6213 end
//            }
//            else
//            {
//                base.AlternateText = FileName;
//
//                //				if (AlternateText == string.Empty)
//                //				{
//                //					base.AlternateText = FileName;
//                //				}
//                //				else
//                //				{
//                //					writer.AddAttribute(HtmlTextWriterAttribute.Alt, FileName);
//                //				}
//            }
//
//            // Write Title attribute (only if either a Default or actual title resource constant was specified)
//            if (_TitleResourceConstant != string.Empty || _defaultTitleResourceConstant != string.Empty )
//            {
//                if(_TitleResourceConstant == string.Empty)
//                {
//                    writer.AddAttribute(HtmlTextWriterAttribute.Title, 
//                        _g.GetResource(_defaultTitleResourceConstant, null), 
//                        true);
//                }
//                else
//                {
//                    writer.AddAttribute(HtmlTextWriterAttribute.Title, 
//                        _g.GetResource(_TitleResourceConstant, this.Parent), 
//                        true);
//                }
//            }
//            else 
//            {
//                //Interestingly, FireFox has different behavior for Alt text and Title text: Alt text will only render if
//                //the image cannot be found, otherwise it does NOT render as a tooltip (you could argue that this is the
//                //more "correct" behavior). Therefore, if no explict tooltip has been set, and there is not titleresourceconstant
//                //then as a default, copy the ALT text into the Title text. TT 18560 Adam
//                if (this.ToolTip == string.Empty && AlternateText != string.Empty) 
//                {	
//                    this.ToolTip = AlternateText;
//                }
//
//            }
//
//            if ((_Border != Unit.Empty) && (_NavigateUrl != String.Empty)) { writer.AddAttribute(HtmlTextWriterAttribute.Border,Border.ToString()); }
//            if (_Hspace != Unit.Empty) { writer.AddAttribute("hspace",_Hspace.Value.ToString()); }
//            if (_Vspace != Unit.Empty) { writer.AddAttribute("vspace",_Vspace.Value.ToString()); }
//            if (_RollOver == true) // 2005-07-20 nh: I don't see this used anywhere. probably good to remove this.
//            {
//                writer.AddAttribute("onmouseover","this.src='" + GetMouseOverSrc(ImageUrl) + "';");
//                writer.AddAttribute("onmouseout","this.src='" + ImageUrl + "';");
//            }
//
//            base.AddAttributesToRender (writer);
//        }


        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            DataBind();
            if ( _NavigateUrl != String.Empty)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class,_NavigateURLClass);
                writer.AddAttribute(HtmlTextWriterAttribute.Href,_NavigateUrl);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
            }
            base.RenderBeginTag (writer);
        }


        public override void RenderEndTag(HtmlTextWriter writer)
        {
            if ( _NavigateUrl != String.Empty)
            {
                base.RenderEndTag (writer);
                writer.RenderEndTag(); // A
            }
            else
            {
                base.RenderEndTag (writer);
            }
        }

        #endregion

        #region Private Methods

        private string FileSrc(string filename, int applicationID) 
        {
#if DEBUG
            System.Diagnostics.Trace.WriteLine(filename + " " + applicationID.ToString());
#endif
            string path = String.Empty;
			
            bool found = false;
			
            string[] delims = new string[] {"Brand", "Site", "Community", "p", "d", "l"};
            string[] vals = new string[] {GetPathFromUri(_brand.Uri), GetPathFromUri(_brand.Site.Name), _brand.Site.Community.Name, _brand.BrandID.ToString(), _brand.Site.Community.CommunityID.ToString(), _brand.Site.LanguageID.ToString()};
            for(int i = 0; i < delims.Length; i++) 
            {
                path = @"\" + delims[i] + @"\" + vals[i] + @"\" + filename;
                if(System.IO.File.Exists(ImageRoot + path)) 
                {
                    found = true;
                    break;
                }

                if(applicationID != Constants.NULL_INT) 
                {
                    path = @"\" + delims[i] + @"\" + vals[i] + @"\" + applicationID.ToString() + @"\" + filename;
                    if(System.IO.File.Exists(ImageRoot + path))
                    {
                        found = true;
                        break;
                    }
                }
            }

            if(!found) 
            {
                path = @"\" + applicationID.ToString() + @"\" + filename;
                found = System.IO.File.Exists(ImageRoot + path);
            }
			
            if(!found) 
            {
                path = @"\" + filename;
                found = System.IO.File.Exists(ImageRoot + path);
                //Regardless of whether the image exists, we'll return a path including the filename.
                //This will be logged as a 404 in IIS logs.
            }

            _found = found;

            path = path.Replace(@"\",@"/");
            path = ImageUrlRoot + path;

            return path;
        }

        private static string GetPathFromUri(string pUri)
        {
            return pUri.Replace(".", "-");
        }


        private string GetMouseOverSrc(string fileSrc)
        {
            string filePath = fileSrc.Substring(0, fileSrc.LastIndexOf("/") + 1);
            return filePath + Path.GetFileNameWithoutExtension(fileSrc) + "h" + Path.GetExtension(fileSrc);
        }

        public static string ImageRoot
        {
            get
            {
#if debug	
				return @"\\devweb01\img
#else
                return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_ROOT");
#endif
            }
        }

        public static string ImageUrlRoot
        {
            get
            {
                return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL");
            }
        }

        #endregion

    } // class Image

    #region Image URL Cache
    public class ImageUrlCache
    {
        public readonly static ImageUrlCache Instance = new ImageUrlCache();
        private Hashtable _Images;
        private ImageUrlCache()
        {
            _Images = Hashtable.Synchronized(new Hashtable());
        }

        public string GetUrl(string fileName, int applicationID, int brandID)
        {
            Hashtable appLevel = null;
            Hashtable brandLevel = null;

            appLevel = _Images[fileName] as Hashtable;
            if (appLevel == null) return string.Empty;

            brandLevel = appLevel[applicationID] as Hashtable;
            if (brandLevel == null ) return string.Empty;

            return Convert.ToString(brandLevel[brandID]);
        }

        public void InsertUrl(string URL, string fileName, int applicationID, int brandID)
        {
            Hashtable appLevel = null;
            Hashtable brandLevel = null;
            //#if DEBUG
            //			System.Diagnostics.Trace.WriteLine("___Image:ImageUrlCache.InstanceGetUrl() " + fileName + "|" + applicationID.ToString() + "|" + brandID.ToString());
            //#endif
            try 
            {
                if (_Images.ContainsKey(fileName))
                {
                    appLevel = _Images[fileName] as Hashtable;
                    if (appLevel.ContainsKey(applicationID)) 
                    {
                        brandLevel = appLevel[applicationID] as Hashtable;
                        if (brandLevel.ContainsKey(brandID))
                        {
                            // overwrite it
                            brandLevel[brandID] = URL;
                        }
                        else 
                        {
                            brandLevel.Add(brandID,URL);
                        }
                    }
                    else 
                    {
                        appLevel.Add(applicationID, Hashtable.Synchronized(new Hashtable()));
                        brandLevel = appLevel[applicationID] as Hashtable;
                        brandLevel.Add(brandID,URL);
                    }
                }
                else 
                {
                    _Images.Add(fileName, Hashtable.Synchronized(new Hashtable()));
                    appLevel = _Images[fileName] as Hashtable;
                    appLevel.Add(applicationID, Hashtable.Synchronized(new Hashtable()));
                    brandLevel = appLevel[applicationID] as Hashtable;
                    brandLevel.Add(brandID,URL);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.ToString());
            }
        }

        public void RemoveUrl(string fileName)
        {
            try 
            {
                _Images.Remove(fileName);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.ToString());
            }
        }
    }

    #endregion Image URL Cache

    #region Designer Class

    public class ImageDesigner : ControlDesigner
    {
        Image _ImgControl;

        public override void Initialize(IComponent component)
        {
            _ImgControl = (Image)component;
            base.Initialize (component);
        }

        public override bool DesignTimeHtmlRequiresLoadComplete
        {
            get
            {
                return true;
            }
        }


//        public override string GetDesignTimeHtml()
//        {
//            if ( _ImgControl.FileName != null)
//            {
//                StringBuilder sb = new StringBuilder();
//
//                string fileSrc = _ImgControl.GetUrl(_ImgControl.FileName, (int)_ImgControl.App, true);
//
//                sb.Append("<img src=\"" + fileSrc + "\"");
//                sb.Append(" border=\"" + _ImgControl.Border.ToString() + "\"");
//
//                if (_ImgControl.Width.Value > 0)
//                {
//                    sb.Append(" width=\"" + _ImgControl.Width.Value + "\"");
//                }
//
//                if (_ImgControl.Height.Value > 0)
//                {
//                    sb.Append(" height=\"" + _ImgControl.Height.Value + "\"");
//                }
//
//                if (_ImgControl.Hspace != 0)
//                {
//                    sb.Append(" hspace=\"" + _ImgControl.Hspace.ToString() + "\"");
//                }
//
//                if (_ImgControl.Vspace != 0)
//                {
//                    sb.Append(" vspace=\"" + _ImgControl.Vspace.ToString() + "\"");
//                }
//
//                sb.Append(">");
//
//                return sb.ToString();
//            }
//            else
//            {
//                return string.Format("mn:{0}",_ImgControl.ID);
//            }
//        }
    } // class ImageDesigner
    #endregion
}
