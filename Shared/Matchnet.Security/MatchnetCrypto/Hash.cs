using System;

namespace MatchnetCrypto
{
	public class Hash
	{
		public Hash()
		{
		}


		public string ComputeHash(string source)
		{
			return Matchnet.Security.Crypto.Hash(source);
		}
	}
}
