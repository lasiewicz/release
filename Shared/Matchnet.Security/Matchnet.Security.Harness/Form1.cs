using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet.Security;

namespace Matchnet.Security.Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnEncrypt;
		private System.Windows.Forms.Button btnDecrypt;
		private System.Windows.Forms.Button btnHash;
		private System.Windows.Forms.TextBox tbString;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbKey;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnEncrypt = new System.Windows.Forms.Button();
			this.tbString = new System.Windows.Forms.TextBox();
			this.btnDecrypt = new System.Windows.Forms.Button();
			this.btnHash = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tbKey = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btnEncrypt
			// 
			this.btnEncrypt.Location = new System.Drawing.Point(8, 8);
			this.btnEncrypt.Name = "btnEncrypt";
			this.btnEncrypt.TabIndex = 0;
			this.btnEncrypt.Text = "Encrypt";
			this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
			// 
			// tbString
			// 
			this.tbString.Location = new System.Drawing.Point(8, 40);
			this.tbString.Name = "tbString";
			this.tbString.Size = new System.Drawing.Size(272, 20);
			this.tbString.TabIndex = 1;
			this.tbString.Text = "123412341234123400025554F494";
			// 
			// btnDecrypt
			// 
			this.btnDecrypt.Location = new System.Drawing.Point(104, 8);
			this.btnDecrypt.Name = "btnDecrypt";
			this.btnDecrypt.TabIndex = 2;
			this.btnDecrypt.Text = "Decrypt";
			this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
			// 
			// btnHash
			// 
			this.btnHash.Location = new System.Drawing.Point(200, 8);
			this.btnHash.Name = "btnHash";
			this.btnHash.TabIndex = 3;
			this.btnHash.Text = "Hash";
			this.btnHash.Click += new System.EventHandler(this.btnHash_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 16);
			this.label1.TabIndex = 4;
			this.label1.Text = "Key";
			// 
			// tbKey
			// 
			this.tbKey.Location = new System.Drawing.Point(72, 72);
			this.tbKey.Name = "tbKey";
			this.tbKey.Size = new System.Drawing.Size(208, 20);
			this.tbKey.TabIndex = 5;
			this.tbKey.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.tbKey);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnHash);
			this.Controls.Add(this.btnDecrypt);
			this.Controls.Add(this.tbString);
			this.Controls.Add(this.btnEncrypt);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void btnEncrypt_Click(object sender, System.EventArgs e)
		{
			tbString.Text = Crypto.Encrypt(tbKey.Text, tbString.Text);
		}

		private void btnHash_Click(object sender, System.EventArgs e)
		{
			tbString.Text = Crypto.Hash(tbKey.Text);
		}

		private void btnDecrypt_Click(object sender, System.EventArgs e)
		{
			tbString.Text = Crypto.Decrypt(tbKey.Text, tbString.Text);
		}
	}
}
