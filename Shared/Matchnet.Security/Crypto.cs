using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Data;
using Microsoft.SqlServer.Server;
using System.Data.SqlTypes;

namespace Matchnet.Security
{
	/// <summary>
	/// 
	/// </summary>
	public class Crypto
	{
	    public const int MAX_ENCRYPT_LENGTH = 60;
	    private const int MAX_SALT_LENGTH = 29;

	    public static readonly Crypto Instance = new Crypto();

	    private Crypto()
		{}

		private static byte[] getLegalKey(DESCryptoServiceProvider cryptoService, string Key)
		{
			string sTemp;
			if (cryptoService.LegalKeySizes.Length > 0)
			{
				int lessSize = 0, moreSize = cryptoService.LegalKeySizes[0].MinSize;

				while (Key.Length * 8 > moreSize)
				{
					lessSize = moreSize;
					moreSize += cryptoService.LegalKeySizes[0].SkipSize;
				}
				sTemp = Key.PadRight(moreSize / 8, ' ');
			}
			else
			{
				sTemp = Key;
			}

			return ASCIIEncoding.ASCII.GetBytes(sTemp);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <param name="source"></param>
		/// <returns></returns>
		/// deprecated <see cref="EncryptText"/>
        [Obsolete("This method is deprecated. Use Crypto.Instance.EncryptText() instead.")]
		public static string Encrypt(string key, string source)
		{
			DESCryptoServiceProvider cryptoService = new DESCryptoServiceProvider();

			byte[] bytIn = System.Text.Encoding.Unicode.GetBytes(source);

			System.IO.MemoryStream ms = new System.IO.MemoryStream();

			Console.WriteLine(cryptoService.KeySize);

			byte[] bytKey = getLegalKey(cryptoService, key);

			cryptoService.Key = bytKey;
			cryptoService.IV = bytKey;

			ICryptoTransform encrypto = cryptoService.CreateEncryptor();

			CryptoStream cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Write);

			cs.Write(bytIn, 0, bytIn.Length);
			cs.FlushFinalBlock();

			byte[] bytOut = new byte[ms.Position];
			ms.Position = 0;
			ms.Read(bytOut, 0, bytOut.Length) ;
   
			cs.Close();
			ms.Close();
                    
			return System.Convert.ToBase64String(bytOut, 0, bytOut.Length);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <param name="source"></param>
		/// <returns></returns>
		/// deprecated
        [Obsolete("This method is deprecated.")]
        public static string Decrypt(string key, string source)
		{
			System.IO.MemoryStream ms = new System.IO.MemoryStream();
			DESCryptoServiceProvider cryptoService = new DESCryptoServiceProvider();

			byte[] bytKey = getLegalKey(cryptoService, key);

			cryptoService.Key = bytKey;
			cryptoService.IV = bytKey;

			ICryptoTransform encrypto = cryptoService.CreateDecryptor();
 
			CryptoStream cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Write);

			byte[] src = System.Convert.FromBase64String(source);
			cs.Write(src, 0, src.Length);
			cs.FlushFinalBlock();
			string result = Encoding.Unicode.GetString(ms.ToArray());

			ms.Close();
			cs.Close();

			return result;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static string Hash(string source)
		{
			SHA1 sha = new SHA1CryptoServiceProvider(); 
			return Convert.ToBase64String(sha.ComputeHash(Encoding.Unicode.GetBytes(source)));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static string HashForMessmo(string source)
		{
			ASCIIEncoding UE = new ASCIIEncoding();
			byte[] HashValue, MessageBytes = UE.GetBytes(source);
			SHA1Managed SHhash = new SHA1Managed();
			string strHex = "";

			HashValue = SHhash.ComputeHash(MessageBytes);
			foreach (byte b in HashValue)
			{
				strHex += String.Format("{0:x2}", b);
			}
			return strHex;
		}

        #region Important Encryption Code. Only modify with manager's permission.
        public string GenerateSalt(int workFactor)
	    {
	        string generateSalt = BCrypt.Net.BCrypt.GenerateSalt(workFactor);
	        return generateSalt;
	    }

	    public string EncryptText(string text, string salt)
        {
            string encryptText = BCrypt.Net.BCrypt.HashPassword(text, salt);
	        encryptText = encryptText.Replace(salt, string.Empty);
            return encryptText;            
        }

        public string EncryptTextPlusSalt(string text, string salt)
        {
            string encryptText = BCrypt.Net.BCrypt.HashPassword(text, salt);
            return encryptText;
        }

        public string EncryptTextPlusSalt(string text, int workFactor)
        {
            string salt = GenerateSalt(workFactor);
            return EncryptTextPlusSalt(text, salt);
        }

        public string[] SplitEncryptedTextAndSalt(string encryptedTextPlusSalt)
        {           
            if (string.IsNullOrEmpty(encryptedTextPlusSalt) || encryptedTextPlusSalt.Length < MAX_ENCRYPT_LENGTH) return null;
            string salt = encryptedTextPlusSalt.Substring(0, MAX_SALT_LENGTH);
            string encryptedText = encryptedTextPlusSalt.Replace(salt, string.Empty);
            return new string[] {encryptedText, salt};
        }

        public bool VerifyEncryptedText(string clearText, string salt, string encryptedText)
        {
            if (!string.IsNullOrEmpty(clearText) && !string.IsNullOrEmpty(salt) && !string.IsNullOrEmpty(encryptedText))
            {
                bool b = EncryptText(clearText, salt) == encryptedText;
                return b;
            }
            return false;
        }
        #endregion

        #region Sql Server procs
        [SqlProcedure]
        public static void SqlEncryptText(SqlString text, SqlString salt, out SqlString result)
        {
            result = Crypto.Instance.EncryptText(text.ToString(), salt.ToString());
        }

        [SqlProcedure]
        public static void SqlGenerateSalt(Int32 workFactor, out SqlString salt)
        {
            salt = Crypto.Instance.GenerateSalt(workFactor);
        }
        #endregion
    }
}
