﻿using System;
using NUnit.Framework;

namespace Matchnet.Security.Tests
{
    [TestFixture]
    public class SecurityTests
    {
        [Test]
        public void TestHashPassword()
        {
            string hashPassword = Matchnet.Security.Crypto.Instance.EncryptText("th1s1smyp@ssw0rd", Crypto.Instance.GenerateSalt(10));
            Console.WriteLine("Hashed password is "+ hashPassword);
        }

        [Test]
        public void TestHashPasswordAreSame()
        {
            string salt = Crypto.Instance.GenerateSalt(10);
            string hashPassword1 = Matchnet.Security.Crypto.Instance.EncryptText("th1s1smyp@ssw0rd",salt);           
            string hashPassword2 = Matchnet.Security.Crypto.Instance.EncryptText("th1s1smyp@ssw0rd", salt);
            Assert.AreEqual(hashPassword1, hashPassword2);
        }

        [Test]
        public void TestVerifyPasswords()
        {
            string salt = Crypto.Instance.GenerateSalt(10);
            string clearTextPassword = "th1s1smyp@ssw0rd";
            string hashPassword1 = Matchnet.Security.Crypto.Instance.EncryptText(clearTextPassword, salt);
            Assert.True(Crypto.Instance.VerifyEncryptedText(clearTextPassword, salt, hashPassword1));
        }

        [Test]
        public void TestHashEmail()
        {
            string salt = "$2a$04$MyRvgbMKaRBzoSoQyViZmu"; //key2
            string clearTextEmail = "repin1@aol.com";
            string hashEmail = Matchnet.Security.Crypto.Instance.EncryptText(clearTextEmail, salt);
            Console.WriteLine(string.Format("Email:{0}, Encrypt:{1}", clearTextEmail, hashEmail));
        }

        [Test]
        public void SplitEncryptedTextAndSalt()
        {
            string salt = Crypto.Instance.GenerateSalt(10);
            string clearTextPassword = "th1s1smyp@ssw0rd";
            string hashPassword1 = salt+Matchnet.Security.Crypto.Instance.EncryptTextPlusSalt(clearTextPassword, salt);
            string[] splitEncryptedTextAndSalt = Crypto.Instance.SplitEncryptedTextAndSalt(hashPassword1);

            Assert.True(Crypto.Instance.VerifyEncryptedText(clearTextPassword, salt, splitEncryptedTextAndSalt[0]));
            Assert.AreEqual(splitEncryptedTextAndSalt[1],salt);
        }

    }
}
