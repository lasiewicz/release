using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using SecureBlackbox;
using Matchnet.Configuration.ServiceAdapters;


namespace Matchnet.PGP
{
    public class Crypto
    {
        public static readonly Crypto Instance=new Crypto();
        private const string LICENSE_KEY1=  "PGP_LICENSE_KEY1";
        private const string LICENSE_KEY2 = "PGP_LICENSE_KEY2";


        private Crypto()
        {
            //SBUtils.Unit.SetLicenseKey(SBUtils.Unit.BytesOfString("0375F798E0FE4841F7BE6108C9380091070D223C95716FD7E54DC5AFAD2E1D9967B9D98111447F1EE1D63240089ECB469970710E9F35931A84012A4B0D6AA9A357353EF5B0B4B8C98A994347D93E3076EC5BEA10EB95A531D8FCFAC2295ECD59A495BE4E450C52AC431A274BE4B0B3122C7FF0E02FB9FD0CEB4094B98D290BC55BFD63A36D0EB355098F4B5A84FC333F1A8B932DBB86EFBAC8AD563ECA18851934F129370919B8683B88075A6980D1B0F850761DBFBBE44DAA788355CBF4C6B682EF8FF3894BD6D469A40E6DC8433C30FC6D8CBEB59DBA0E8DAEBA6DDFC04C8F78BD81385973AEE50C652F66A91888A37FCA08DF1CD730C4E55B6700DD1BB14D"));

            string key1 = RuntimeSettings.GetSetting(LICENSE_KEY1);
            string key2 =  RuntimeSettings.GetSetting(LICENSE_KEY2);
            // this is for security and security only putrpose only :)
            SBUtils.Unit.SetLicenseKey(SBUtils.Unit.BytesOfString(key1 + key2));
        }
        public void Encrypt(string sourceFile, string pubKeyFile, string outputFile)
        {
           
            System.IO.FileInfo info;
            SBPGPKeys.TElPGPKeyring keyring;

            SBPGPKeys.TElPGPKeyring pubKeyring;
            SBPGPKeys.TElPGPKeyring secKeyring;
            try
            {
                using (SBPGP.TElPGPWriter pgpWriter=new SBPGP.TElPGPWriter())
                {
                    keyring = new SBPGPKeys.TElPGPKeyring();
                    pubKeyring = new SBPGPKeys.TElPGPKeyring();
                    secKeyring = new SBPGPKeys.TElPGPKeyring();

                    keyring.Load(pubKeyFile, "", true);
                    pubKeyring.AddPublicKey((SBPGPKeys.TElPGPPublicKey)keyring.get_PublicKeys(0));

                    pgpWriter.EncryptingKeys = pubKeyring;

                    info = new FileInfo(sourceFile);
                    pgpWriter.Filename = info.Name;
                    pgpWriter.InputIsText = true;
                   // File.Open(
                using (FileStream inF = new System.IO.FileStream(sourceFile, FileMode.Open,FileAccess.Read,FileShare.None))
                {
                   //inF.ReadTimeout = -1;
                    using (FileStream outF = new System.IO.FileStream(outputFile, FileMode.Create))
                    {
                        //outF.WriteTimeout = -1;
                        
                        pgpWriter.Encrypt(inF, outF, 0);
                        
                    }
                  
                }
                    
               }
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { }

        }


        public void Decrypt(string sourceFile, string pubKeyFile, string secKeyFile, string passPhrase, string outputFile)
        {
            try
            { 
                using (SBPGP.TElPGPReader pgpReader = new SBPGP.TElPGPReader())
                {
                    using (SBPGPKeys.TElPGPKeyring keyring = new SBPGPKeys.TElPGPKeyring())
                    {
                        keyring.Load(pubKeyFile, secKeyFile, true);
                        pgpReader.KeyPassphrase = passPhrase;

                        pgpReader.DecryptingKeys = keyring;
                        pgpReader.VerifyingKeys = keyring;

                        using (FileStream inF = new System.IO.FileStream(sourceFile, FileMode.Open))
                        {
                            using (FileStream outF = new System.IO.FileStream(outputFile, FileMode.Create))
                            {
                                pgpReader.OutputStream = outF;
                                pgpReader.DecryptAndVerify(inF, 0);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw ex; }
            finally
            {  }
        }


    }
}
