using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Matchnet.PGP;
using System.IO;

namespace Matchnet.PGP.Harness
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            //Matchnet.PGP.Crypto crypto = new Matchnet.PGP.Crypto();
            try
            {
                Matchnet.PGP.Crypto.Instance.Encrypt(tbFile.Text, tbKeyFile.Text, tbTargetFile.Text);
            }
            catch (Exception ex)
            { string s = ex.Source + "," +  ex.Message;
            lblError.Text = s;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Matchnet.PGP.Crypto crypto = new Matchnet.PGP.Crypto();
            try
            {
                Matchnet.PGP.Crypto.Instance.Decrypt(tbFile.Text, tbKeyFile.Text, tbSecretKeyFile.Text, tbPass.Text, tbTargetFile.Text);
            }
            catch (Exception ex)
            { string s = ex.Source + "," +  ex.Message;
            lblError.Text = s;
        }
        }

        private void btnBrowseFile_Click(object sender, EventArgs e)
        {

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.tbFile.Text = openFileDialog.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.tbTargetFile.Text = openFileDialog.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.tbKeyFile.Text = openFileDialog.FileName;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.tbSecretKeyFile.Text = openFileDialog.FileName;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            convertUTF8(tbFile.Text, tbTargetFile.Text);
        }

        private void convertUTF8(string fileNameIn, string fileNameOut)
        {
            try
            {
                UTF8Encoding utf8 = new UTF8Encoding();
                UnicodeEncoding unicode = new UnicodeEncoding();

                using (FileStream stream = File.OpenRead(fileNameIn))
                {
                    using (TextWriter writer = new StreamWriter(fileNameOut))
                    {
                        int readBytes;
                        int bufferLength = 20480;
                        byte[] buffer = new byte[bufferLength];
                        do
                        {


                            readBytes = stream.Read(buffer, 0, bufferLength);
                            char[] unicodeChar = unicode.GetChars(buffer, 0, readBytes);
                            byte[] utf8Byte = utf8.GetBytes(unicodeChar);
                            char[] utfChar = utf8.GetChars(utf8Byte);
                            writer.Write(utfChar, 0, utfChar.GetLength(0));

                        } while (readBytes != 0);

                    }
                }
            }
            catch (Exception ex)
            { }


        }



    }
}