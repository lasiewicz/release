using System;
using System.Globalization;

namespace Matchnet.SharedLib
{
	/// <summary>
	/// Summary description for Enumerations.
	/// </summary>
	public class Enumerations
	{
		public Enumerations()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// The integer representation of a language
		/// </summary>
		public enum Language : int
		{
			English = 2,
			French = 8,
			German = 32,
			Hebrew = 262144
		}
	}
}
