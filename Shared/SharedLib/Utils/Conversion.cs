using System;
using Matchnet.SharedLib.Constants;

namespace Matchnet.SharedLib.Utils
{
	/// <summary>
	/// Summary description for Conversion.
	/// </summary>
	public class Conversion
	{
		public Conversion()
		{
			
		}
		
		#region CInt and Overrides
		/// <summary>
		/// Converts a string to an integer.
		/// </summary>
		/// <param name="strValue">The string to convert to an integer.</param>
		/// <returns>The integer representation of the object.</returns>
		public static int CInt(string strValue, int iDefaultValue)
		{
			double dResult;

			// shoudl use Int.TryParse, but that is not supported until v2005
			if(Double.TryParse(strValue, System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.InvariantInfo, out dResult))
			{
				if (dResult >= Int32.MinValue && dResult <= Int32.MaxValue)
					return Convert.ToInt32(dResult);
			}
			return iDefaultValue;
		}

		/// <summary>
		/// Overloaded CInt, uses NULL for the default value.
		/// </summary>
		/// <param name="strValue">The string to convert to an integer</param>
		/// <returns>The string represented as an integer.  If not an integer, then NULL_INT.</returns>
		public static int CInt(string strValue)
		{
			return CInt(strValue, DefaultValues.Integer);
		}

		/// <summary>
		/// Overloaded CInt, uses NULL for the default value and an object instead of a string.
		/// </summary>
		/// <param name="objValue"></param>
		/// <returns></returns>
		public static int CInt(object objValue)
		{
			if (objValue == null)
			{
				return DefaultValues.Integer;
			}

			return CInt(objValue, DefaultValues.Integer);
		}

		/// <summary>
		/// Overloaded CInt, uses an object instead of a string.
		/// </summary>
		/// <param name="objValue">The object to convert to an integer.</param>
		/// <param name="iDefaultValue">The default value to return if not an integer.</param>
		/// <returns>The integer representation of the object, or iDefaultValue if not an integer.</returns>
		public static int CInt(object objValue, int iDefaultValue)
		{
			if (objValue == null)
			{
				return iDefaultValue;
			}

			return CInt(objValue.ToString(), iDefaultValue);
		}
		#endregion
	}
}
