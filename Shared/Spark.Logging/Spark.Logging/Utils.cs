﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet;
namespace Spark.Logging
{
    public enum ELogLevel:int
    {
        NONE=0,
        DEBUG = 1,
        ERROR = 2,
        FATAL = 4,
        INFO = 8,
        WARN = 16,
        MIRRORTOTRACE=32,
        TRACEONLY=64
    }
    public class Utils
    {
        private static ISettingsSA _settingsService = null;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            } 
            
            set { _settingsService = value; }
        }

        public static bool IsLoggerEnabled(string appname, ELogLevel loglevel)
        {
            try
            {bool enabled=false;
            if (String.IsNullOrEmpty(appname))
                return false;

            string appconst = string.Format("{0}_LOGGER_MASK", appname.ToString());
                string setting = SettingsService.GetSettingFromSingleton(appconst);
                int enabledlevel = Conversion.CInt(setting);

                enabled = ((int)loglevel & enabledlevel) == (int)loglevel;
                return enabled;


            }
            catch (Exception ex)
            {
                return false;
            }


        }

        public static bool IsTraceMirrorEnabled(string appname)
        {
            try
            {
                if (String.IsNullOrEmpty(appname))
                    return false;
                bool enabled = false;
                string appconst = string.Format("{0}_LOGGER_MASK", appname.ToString());
                string setting = SettingsService.GetSettingFromSingleton(appconst);
                int enabledlevel = Conversion.CInt(setting);

                enabled = ((int)ELogLevel.MIRRORTOTRACE & enabledlevel) == (int)ELogLevel.MIRRORTOTRACE;
                return enabled;


            }
            catch (Exception ex)
            {
                return false;
            }


        }

        public static bool IsTraceEnabled(string appname)
        {
            try
            {
                if (String.IsNullOrEmpty(appname))
                    return false;
                bool enabled = false;
                string appconst = string.Format("{0}_LOGGER_MASK", appname.ToString());
                string setting = SettingsService.GetSettingFromSingleton(appconst);
                int enabledlevel = Conversion.CInt(setting);

                enabled = ((int)ELogLevel.TRACEONLY & enabledlevel) == (int)ELogLevel.TRACEONLY;
                return enabled;


            }
            catch (Exception ex)
            {
                return false;
            }


        }


    }
}
