﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using Matchnet.Configuration.ServiceAdapters;
namespace Spark.Logging
{ 

    public class RollingFileLogger
    {

        public static readonly RollingFileLogger Instance = new RollingFileLogger();

        private static readonly ILog log = LogManager.GetLogger(typeof(RollingFileLogger));
        private RollingFileLogger() 
        {
            
            XmlConfigurator.Configure();
        
        }



        public void EnterMethod(string appname,string classname,string methodName)
        {   try
            {
                if (Utils.IsLoggerEnabled(appname, ELogLevel.INFO))
                {
                    string logmsg = string.Format("ClassName:{0}, Entering Method {1}",classname, methodName);
                    log.Info(logmsg);
                    if (Utils.IsTraceMirrorEnabled(appname))
                        Trace(appname,ELogLevel.INFO, logmsg);
                }
                else
                {
                    if (Utils.IsTraceEnabled(appname))
                    {
                        string logmsg = string.Format("ClassName:{0}, Entering Method {1}", classname, methodName);
                        Trace(appname, ELogLevel.INFO, logmsg);
                    }
                }
            }
            catch (Exception ex) { }
          }

        public void EnterMethod(string appname, string classname, string methodName, string[] parameters)
        {
            try
            {
                if (Utils.IsLoggerEnabled(appname, ELogLevel.INFO))
                {
                    string logmsg = string.Format("ClassName:{0}, Entering Method {1}", classname, methodName);
                    logmsg += GetParamString(parameters);
                    log.Info(logmsg);
                    if (Utils.IsTraceMirrorEnabled(appname))
                        Trace(appname, ELogLevel.INFO, logmsg);
                }
                else
                {
                    if (Utils.IsTraceEnabled(appname))
                    {
                        string logmsg = string.Format("ClassName:{0}, Entering Method {1}", classname, methodName);
                        logmsg += GetParamString(parameters);
                        Trace(appname, ELogLevel.INFO, logmsg);
                    }
                }
            }
            catch (Exception ex) { }
        }

        public void EnterMethod(string appname, string classname, string methodName, Object[] parameters)
        {
            try
            {
                if (Utils.IsLoggerEnabled(appname, ELogLevel.INFO))
                {
                    string logmsg = string.Format("ClassName:{0}, Entering Method {1}", classname, methodName);
                    logmsg += GetParamString(parameters);
                    log.Info(logmsg);
                    if (Utils.IsTraceMirrorEnabled(appname))
                        Trace(appname, ELogLevel.INFO, logmsg);
                }
                else
                {
                    if (Utils.IsTraceEnabled(appname))
                    {
                        string logmsg = string.Format("ClassName:{0}, Entering Method {1}", classname, methodName);
                        logmsg += GetParamString(parameters);
                        Trace(appname, ELogLevel.INFO, logmsg);
                    }
                }

            }
            catch (Exception ex) { }
        }
        public void LeaveMethod(string appname, string classname, string methodName)
          {
              try
              {
                  if (Utils.IsLoggerEnabled(appname, ELogLevel.INFO))
                  {
                      string logmsg = string.Format("ClassName:{0} Leaving Method {1}",classname, methodName);
                      log.Info(logmsg);
                      if (Utils.IsTraceMirrorEnabled(appname))
                          Trace(appname, ELogLevel.INFO, logmsg);

                  }
                  else
                  {
                      if (Utils.IsTraceEnabled(appname))
                      {
                          string logmsg = string.Format("ClassName:{0} Leaving Method {1}", classname, methodName);
                          Trace(appname, ELogLevel.INFO, logmsg);

                      }
                  }
              }
              catch (Exception ex) { }
          }

        public void LeaveMethod(string appname, string classname, string methodName, Object returndata)
        {
            try
            {
                if (Utils.IsLoggerEnabled(appname, ELogLevel.INFO))
                {
                    string logmsg = string.Format("ClassName:{0} Leaving Method {1}", classname, methodName);
                    if (returndata != null)
                        logmsg += " Return:" + returndata.ToString();
                    else
                        logmsg += " Return:null";
                    log.Info(logmsg);
                    if (Utils.IsTraceMirrorEnabled(appname))
                        Trace(appname, ELogLevel.INFO, logmsg);

                }
                else
                {
                    if (Utils.IsTraceEnabled(appname))
                    {
                        string logmsg = string.Format("ClassName:{0} Leaving Method {1}", classname, methodName);
                        Trace(appname, ELogLevel.INFO, logmsg);

                    }
                }
            }
            catch (Exception ex) { }
        }

        public void LogException(string appname, string classname, Exception exception)
        {
            try{
                if (Utils.IsLoggerEnabled(appname, ELogLevel.ERROR))
                {
                    string logmsg = string.Format("ClassName:{0}, Exception:{1}",classname, exception.Message);
                    log.Error(logmsg, exception);
                    if (Utils.IsTraceMirrorEnabled(appname))
                        Trace(appname, ELogLevel.ERROR, logmsg,exception);
                }
                else
                {
                    if (Utils.IsTraceEnabled(appname))
                    {
                        string logmsg = string.Format("ClassName:{0}, Exception:{1}", classname, exception.Message);
                       Trace(appname, ELogLevel.ERROR, logmsg,exception);
                    }
                }
            }
            catch (Exception ex) { }
          }

        public void LogException(string appname, string classname, Exception exception, Object obj)
        {try{
            if (Utils.IsLoggerEnabled(appname, ELogLevel.ERROR))
            {
                string data = "";
                if (obj != null)
                    data = obj.ToString();
                string logmsg = string.Format("ClassName:{0}, Exception, {1} Data {2}", classname,exception.Message, data);

                log.Error(string.Format(logmsg), exception);
                if (Utils.IsTraceMirrorEnabled(appname))
                    Trace(appname, ELogLevel.ERROR, logmsg,exception);

            }
            else
            {
                if (Utils.IsTraceEnabled(appname))
                {
                    string data = "";
                    if (obj != null)
                        data = obj.ToString();
                    string logmsg = string.Format("ClassName:{0}, Exception, {1} Data {2}", classname, exception.Message, data);
                    if (Utils.IsTraceMirrorEnabled(appname))
                        Trace(appname, ELogLevel.ERROR, logmsg,exception);
                }
            }
        }
        catch (Exception ex) { }
        }


        
        public void LogException(string appname, string classname, string methodname,Exception exception, Object obj)
        {try{
            if (Utils.IsLoggerEnabled(appname, ELogLevel.ERROR))
            {
                string data = "";
                if (obj != null)
                    data = obj.ToString();
                string logmsg = string.Format("ClassName:{0}, Method:{1}, Exception {2}, Data {3}", classname,methodname,exception.Message, data);

                log.Error(string.Format(logmsg), exception);
                if (Utils.IsTraceMirrorEnabled(appname))
                    Trace(appname, ELogLevel.ERROR, logmsg,exception);

            }
            else
            {
                if (Utils.IsTraceEnabled(appname))
                {
                    string data = "";
                    if (obj != null)
                        data = obj.ToString();
                    string logmsg = string.Format("ClassName:{0}, Method:{1}, Exception {2}, Data {3}", classname, methodname, exception.Message, data);
                     Trace(appname, ELogLevel.ERROR, logmsg,exception);
                }
            }
        }
        catch (Exception ex) { }
        }

        public void LogError(string appname, string classname, Exception exception, Object obj)
        {try{
            if (Utils.IsLoggerEnabled(appname, ELogLevel.ERROR))
            {
                string data = "";
                if (obj != null)
                    data = obj.ToString();
                string logmsg = string.Format("ClassName:{0}, Exception, {1} Data {2}", classname, exception.Message, data);
                log.Error(string.Format(logmsg), exception);
                if (Utils.IsTraceMirrorEnabled(appname))
                    Trace(appname, ELogLevel.ERROR, logmsg);
            }
            else
            {
                if (Utils.IsTraceEnabled(appname))
                {
                    string data = "";
                    if (obj != null)
                        data = obj.ToString();
                    string logmsg = string.Format("ClassName:{0}, Exception, {1} Data {2}", classname, exception.Message, data);
                    Trace(appname, ELogLevel.ERROR, logmsg);
                }
            }
        }
        catch (Exception ex) { }
        }



        public void LogWarningMessage(string appname,string classname, string message, Object obj)
        {
          try{  
            if (Utils.IsLoggerEnabled(appname, ELogLevel.WARN))
            {
                string data = "";
                if (obj != null)
                    data = obj.ToString();
                string logmsg = string.Format("ClassName:{0}, Warning, {1} Data {2}", classname, message, data);
                log.Warn(logmsg);
                if (Utils.IsTraceMirrorEnabled(appname))
                    Trace(appname, ELogLevel.WARN, logmsg);

            }
            else
            {
                if (Utils.IsTraceEnabled(appname))
                {
                    string data = "";
                    if (obj != null)
                        data = obj.ToString();
                    string logmsg = string.Format("ClassName:{0}, Warning, {1} Data {2}", classname, message, data);
                     Trace(appname, ELogLevel.WARN, logmsg);
                }
            }
          }
          catch (Exception ex) { }

          }



        public void LogInfoMessage(string appname, string classname,string message, Object obj)
        {try{
            if (Utils.IsLoggerEnabled(appname, ELogLevel.INFO))
            {
                string data = "";
                if (obj != null)
                    data = obj.ToString();
                string logmsg = string.Format("ClassName:{0}, Info, {1} Data {2}", classname, message, data);
                log.Info(logmsg);
                if (Utils.IsTraceMirrorEnabled(appname))
                    Trace(appname,ELogLevel.INFO, logmsg);

            }
            else
            {
                if (Utils.IsTraceEnabled(appname))
                {
                    string data = "";
                    if (obj != null)
                        data = obj.ToString();
                    string logmsg = string.Format("ClassName:{0}, Info, {1} Data {2}", classname, message, data);
                     Trace(appname, ELogLevel.INFO, logmsg);
                }
            }
        }
        catch (Exception ex) { }
        }
        public void Trace(string appname,ELogLevel loglevel, string trace)
        {
            try
            {
                string format = "Application:{0}, LogLevel:{1} - Trace {2}";
                System.Diagnostics.Trace.Write(String.Format(format,appname,loglevel.ToString(),trace));

            }
            catch (Exception ex)
            {

            }

        }
        public void Trace(string appname, ELogLevel loglevel, string trace,Exception exception)
        {
            try
            {
                string format = "Application:{0}, LogLevel:{1} - Trace {2}, Exception - {3}";
                System.Diagnostics.Trace.Write(String.Format(format, appname, loglevel.ToString(), trace,exception.ToString()));

            }
            catch (Exception ex)
            {

            }

        }

        public string GetParamString(string[] parameters)
        {
            System.Text.StringBuilder bld = new StringBuilder();
            
            try
            {
                bld.Append(" Parameters:");
                if (parameters == null)
                    bld.Append("params=Null");
                else
                {
                    for(int i=0;i<parameters.Length;i++)
                    {
                        bld.Append(String.Format("param{0}={1}",i,parameters[i]));
                    }
                }
                return bld.ToString();

            }
            catch (Exception ex)
            {
                return bld.ToString();
            }

        }

        public string GetParamString(Object[] parameters)
        {
            System.Text.StringBuilder bld = new StringBuilder();

            try
            {
                bld.Append(" Parameters:");
                if (parameters == null)
                    bld.Append("params=Null");
                else
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        bld.Append(String.Format("param{0}={1}", i, parameters[i].ToString()));
                    }
                }
                return bld.ToString();

            }
            catch (Exception ex)
            {
                return bld.ToString();
            }

        }
       }

}
