using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;

namespace Matchnet.Data
{
	/// <summary>
	/// A SQLConfig is a Singleton that provides connection strings to
	/// databases based on a logical database name and key.
	/// </summary>
	public sealed class SQLConfig
	{
		// the one and only instance
		private static SQLConfig m_singleton = new SQLConfig();
		
		// how often we should load the configuraton, in seconds
		private const int DEFAULT_REFRESH = 30;

		// the config file that holds the location of the master
		private const string MATCHNET_CONFIG_PATH = @"c:\matchnet\matchnet.config";
		private const string INITIAL_CONNECTION_STRING_NODE = "initialconnectionstring";
		private const string CONFIG_DATABASE = "mnMaster";
		private DateTime m_flastMod = DateTime.MinValue;

		// the database version
		private string m_Version = Guid.NewGuid().ToString();
		private DateTime m_TimeCheck = DateTime.MinValue;
		private bool m_isCurrent = false;

		// the in memory data structure for the coniguration
		private Hashtable m_Cache = new Hashtable();

		/// <summary>
		/// Returns the one and only instance of the SQLConfig
		/// </summary>
		/// <returns>The one and only instance of the SQLConfig</returns>
		public static SQLConfig GetInstance() 
		{
			return m_singleton;
		}

		private SQLConfig() 
		{
			// private to support singleton
		}

		/// <summary>
		/// Returns a database connection string based on a SQLDescriptor
		/// </summary>
		/// <param name="descriptor">The SQLDescriptor to use when obtaining a connection string</param>
		/// <returns>A connection string to a database</returns>
		public string GetConnectionString(SQLDescriptor descriptor) 
		{
			return GetConnectionString(descriptor.LogicalDatabase, descriptor.Key, descriptor.WithLoad);
		}

		/// <summary>
		/// Returns a database connection string.
		/// </summary>
		/// <param name="logicalDatabase">The logical name of the database</param>
		/// <param name="key">The key of the item to provide a connection string for</param>
		/// <param name="forceLoad">Whether or not to force a fresh loading of logical databases</param>
		/// <returns>A connection string to a database</returns>
		public string GetConnectionString(string logicalDatabase, int key, bool forceLoad) 
		{
			// will load if forceLoad is true or existing version has expired
			Load(forceLoad);

			if (!m_Cache.ContainsKey(logicalDatabase)) 
			{
				throw new Exception("Unable to load database [" + logicalDatabase + "]");
			}

			ArrayList list = (ArrayList)m_Cache[logicalDatabase];

			Hashtable validKeys = new Hashtable();
			IEnumerator enumerator = list.GetEnumerator();
			int counter = 0;
			while (enumerator.MoveNext()) 
			{
				LogicalDatabase val = (LogicalDatabase)enumerator.Current;
				if (val.Name == logicalDatabase) 
				{
					if ((key % (list.Count / val.ServerCount)) == val.AccessFlag) 
					{
						if (val.ActiveDateTime <= DateTime.Now) 
						{
							validKeys.Add(counter, val.ConnectionString);
							counter++;
						}								
					}							
				}
			}

			if (counter == 0) 
			{
				throw new Exception("Database [" + logicalDatabase + "] is currently unavailable");
			}

			//DO NOT use Random.Next(MinValue, MaxValue). Does not balance.
			counter = (int) (Math.Floor(counter * (new Random((int)DateTime.Now.Ticks).NextDouble())));
			return validKeys[counter].ToString();
		}

		/// <summary>
		/// Returns an array of connection strings for a logical database
		/// based on logical database and key
		/// </summary>
		/// <param name="logicalDatabase">The logical name of the database</param>
		/// <param name="key">The key of the item to provide a connection string for</param>
		/// <returns>A connection string to a database</returns>
		public ArrayList GetLogicalConnectionStrings(string logicalDatabase, int key) 
		{
			// will load if forceLoad is true or existing version has expired
			Load(false);

			if (!m_Cache.ContainsKey(logicalDatabase)) 
			{
				throw new Exception("Unable to load database [" + logicalDatabase + "]");
			}

			ArrayList list = (ArrayList)m_Cache[logicalDatabase];
			ArrayList logicalDatabases = new ArrayList();

			IEnumerator enumerator = list.GetEnumerator();

			int counter = 0;
			while (enumerator.MoveNext()) 
			{
				LogicalDatabase val = (LogicalDatabase)enumerator.Current;
				if (val.Name == logicalDatabase) 
				{
					if ((key % (list.Count / val.ServerCount)) == val.AccessFlag) 
					{
						if (val.ActiveDateTime <= DateTime.Now) 
						{
							logicalDatabases.Add(val.ConnectionString);
							counter++;
						}								
					}							
				}
			}

			if (counter == 0) 
			{
				throw new Exception("Database [" + logicalDatabase + "] is currently unavailable");
			}
			
			return logicalDatabases;
		}


		private bool MasterChanged 
		{
			get 
			{
				// check to see if the file has been modified
				DateTime flastMod = File.GetLastWriteTime(MATCHNET_CONFIG_PATH);

				if (flastMod > m_flastMod) 
				{
					// need to reload
					m_flastMod = flastMod;

					// invalidate our cache
					m_Cache.Clear();
					return true;
				} 
				return false;
			}
		}

		private string LoadInitialConnectionString() 
		{
			XmlTextReader reader = null;
			string connectionString = string.Empty;
			try 
			{
				reader = new XmlTextReader(MATCHNET_CONFIG_PATH);
				while (reader.Read()) 
				{
					if (reader.NodeType == XmlNodeType.Element) 
					{
						if (reader.Name.ToLower() == INITIAL_CONNECTION_STRING_NODE) 
						{
							reader.Read();
							connectionString = reader.Value;
							break;
						}
					}
				}
			} 
			finally 
			{
				if (reader != null) 
				{
					reader.Close();
				}
			}
			if (connectionString.Trim() == string.Empty) 
			{
				throw new Exception("No Initial Connection String defined in " + MATCHNET_CONFIG_PATH);
			} 
			else 
			{
				return connectionString;
			}
		}

		private string GetConfigConnectionString() 
		{
			if (MasterChanged) 
			{
				return LoadInitialConnectionString();
			}
			
			ArrayList list = (ArrayList)m_Cache[CONFIG_DATABASE];

			if (list == null || list.Count == 0) 
			{
				// we don't have a loaded config
				return LoadInitialConnectionString();
			}

			Hashtable validKeys = new Hashtable();
			IEnumerator enumerator = list.GetEnumerator();
			int counter = 0;
			int key = 0;

			while (enumerator.MoveNext()) 
			{
				LogicalDatabase val = (LogicalDatabase)enumerator.Current;
				if (val.Name == CONFIG_DATABASE) 
				{
					if ((key % list.Count) == val.AccessFlag) 
					{
						if (val.ActiveDateTime <= DateTime.Now) 
						{
							validKeys.Add(counter, val.ConnectionString);
							counter++;
						}								
					}							
				}
			}

			if (counter == 0) 
			{
				// just return the initial, we don't have a loaded mnMaster
				return LoadInitialConnectionString();
			}

			// return one of the configured mnMasters
			counter = (int) (Math.Floor(counter * (new Random((int)DateTime.Now.Ticks).NextDouble())));
			return validKeys[counter].ToString();
		}

		private void Load(bool forceLoad) 
		{	
			lock (this) 
			{
				string newVersion;
				DataTable dataTable;
				LogicalDatabase logicalDatabase;

				// get an mnMaster connection string
				string connectionString = GetConfigConnectionString();

				try 
				{				
					TimeSpan ts = DateTime.Now - m_TimeCheck;
					if (ts.TotalSeconds <= DEFAULT_REFRESH) 
					{
						m_isCurrent = true;	
					}
					else 
					{
						m_isCurrent = false;
						if (m_Cache.Count > 0) 
						{
							dataTable = GetDataTable(connectionString, "up_LogicalDatabaseVersion_List");
							if (dataTable.Rows.Count == 0) 
							{
								throw (new Exception("Logical Database Version is not configured properly."));
							}

							newVersion = dataTable.Rows[0][0].ToString();
							if (newVersion == m_Version) 
							{
								m_TimeCheck = DateTime.Now;
								m_isCurrent = true;
							}
						}
					}
				
					if (!m_isCurrent || forceLoad) 
					{
						m_TimeCheck = DateTime.Now;
					
						try 
						{
							dataTable = GetDataTable(connectionString, "up_LogicalDatabases_List");
						} 
						catch (Exception ex) 
						{
							throw ex;
						}

						if (dataTable.Rows.Count == 0) 
						{
							throw (new Exception("Logical Databases are not configured properly."));
						}
						else 
						{
							newVersion = dataTable.Rows[0]["Version"].ToString();
						
							m_Version = newVersion;
							m_TimeCheck = DateTime.Now;

							ArrayList arr;

							// clear the existing config
							int serverCount;
							m_Cache.Clear();
							foreach (DataRow dataRow in dataTable.Rows) 
							{
								logicalDatabase = new LogicalDatabase();
								logicalDatabase.Name = dataRow["LogicalDatabase"].ToString();
								logicalDatabase.AccessFlag = (int) dataRow["AccessFlag"];
								logicalDatabase.ActiveDateTime = (DateTime) dataRow["ActiveDateTime"];
								logicalDatabase.ConnectionString = dataRow["ConnectionString"].ToString();
								serverCount = (int) dataRow["ServerCount"];
								logicalDatabase.ServerCount = (serverCount <= 0) ? 1 : serverCount;

								arr = (ArrayList) m_Cache[logicalDatabase.Name];
								if(arr == null) 
								{
									arr = new ArrayList();
									m_Cache.Add(logicalDatabase.Name, arr);
								}

								arr.Add(logicalDatabase);
							}
						}
					}
				}
				catch 
				{
					// force a reload on next attempt
					m_Cache.Clear();
					m_Version = "";
					m_TimeCheck = DateTime.MinValue;
					throw;
				}
			}
		}

		/// <summary>
		/// Obtains a connection string based on the physical database id
		/// </summary>
		/// <param name="physicalDatabaseID">The physical database id</param>
		/// <returns>A database connection string</returns>
		public string PhysicalConnectionString(int physicalDatabaseID) 
		{
			SqlConnection connection = new SqlConnection(GetConfigConnectionString());
			SqlCommand command = new SqlCommand("up_PhysicalDatabase_List");

			command.CommandType = CommandType.StoredProcedure;
			command.Connection = connection;

			SqlParameter param = command.CreateParameter();
			param.SqlDbType = SqlDbType.Int;
			param.Direction = ParameterDirection.Input;
			param.ParameterName = "@PhysicalDatabaseID";
			param.Value = physicalDatabaseID;
			command.Parameters.Add(param);

			SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

			DataTable dataTable = new DataTable();
			string connectionString = null;

			try 
			{
				connection.Open();
				dataAdapter.Fill(dataTable);
				connectionString = dataTable.Rows[0]["ConnectionString"].ToString();
			}
			finally 
			{
				if (connection.State != ConnectionState.Closed) 
				{
					connection.Close();
				}
				command.Dispose();
				connection.Dispose();
			}
			return connectionString;
		}

		// used to populate logical databases
		private DataTable GetDataTable(string connectionString, string commandText) 
		{
			SqlConnection connection = new SqlConnection(connectionString);
			SqlCommand command = new SqlCommand(commandText);
			command.CommandType = CommandType.StoredProcedure;
			command.Connection = connection;
			SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
			
			DataTable dataTable = new DataTable();

			try 
			{
				connection.Open();
				dataAdapter.Fill(dataTable);
				
				return dataTable;				
			}
			finally 
			{
				if (connection.State != ConnectionState.Closed) 
				{
					connection.Close();
				}
				command.Dispose();
				connection.Dispose();
			}			
		}

		private class LogicalDatabase 
		{
			public string Name;
			public int AccessFlag;
			public DateTime ActiveDateTime;
			public string ConnectionString;
			public int ServerCount = 1;
		}
	}
}
