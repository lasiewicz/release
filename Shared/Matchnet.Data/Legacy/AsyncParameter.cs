using System;
using System.Data;
using System.Data.SqlClient;

namespace Matchnet.Data
{
	/// <summary>
	/// Represents a parameter to be used with an AsyncCommand.
	/// </summary>
	[Serializable]
	public class AsyncParameter
	{
		private string m_name;
		private SqlDbType m_type;
		private ParameterDirection m_direction;
		private object m_value;
		private int m_size;

		/// <summary>
		/// Constructs a new instance using data from a SqlParameter
		/// </summary>
		/// <param name="param">The SqlParameter to copy data from</param>
		public AsyncParameter(SqlParameter param) 
		{
			m_name =  param.ParameterName;
			m_type = param.SqlDbType;
			m_direction = param.Direction;
			m_value = param.Value;
			m_size = param.Size;
		}

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		/// <param name="name">The name of the parameter</param>
		/// <param name="dbType">The type of the parameter</param>
		/// <param name="direction">The direction of the parameter</param>
		/// <param name="paramVal">The value of the parameter</param>
		/// <param name="size">The size of the parameter</param>
		public AsyncParameter(string name, SqlDbType dbType, ParameterDirection direction, object paramVal, int size)
		{
			m_name = name;
			m_type = dbType;
			m_direction = direction;
			m_value = paramVal;
			m_size = size;
		}

		/// <summary>
		/// The name of the parameter
		/// </summary>
		public string Name 
		{
			get 
			{
				return m_name;
			}
		}

		/// <summary>
		/// The type of the parameter
		/// </summary>
		public SqlDbType Type 
		{
			get 
			{
				return m_type;
			}
		}

		/// <summary>
		/// The direction of the parameter
		/// </summary>
		public ParameterDirection Direction 
		{
			get 
			{
				return m_direction;
			}
		}

		/// <summary>
		/// The value of the parameter
		/// </summary>
		public object Value 
		{
			get 
			{
				return m_value;
			}
		}

		/// <summary>
		/// The size of the parameter
		/// </summary>
		public int Size 
		{
			get 
			{
				return m_size;
			}
		}
	}
}
