using System;
using System.Data;
using System.Data.SqlClient;

namespace Matchnet.Data
{
	/// <summary>
	/// Extends the functionality of the SQLClient by adding support
	/// for handling Multiple Recordsets as the result of a single 
	/// query. 
	/// </summary>
	public class ExtendedSQLClient : SQLClient
	{
		/// <summary>
		/// Constructs a new instance with a predefined connection string
		/// </summary>
		/// <param name="connectionString"></param>
		public ExtendedSQLClient(string connectionString) 
			: base (connectionString)
		{
		}

		/// <summary>
		/// Constructs a new instance with a SQLDescriptor instance
		/// </summary>
		/// <param name="descriptor"></param>
		public ExtendedSQLClient(SQLDescriptor descriptor) 
			: base (descriptor)
		{
		}

		/// <summary>
		/// Executes a command against the database and returns the result in a populated DataSet.
		/// A default CommandType of text is used.
		/// </summary>
		/// <param name="commandText">The text of the command to execute</param>
		/// <returns>A populated DataSet</returns>
		public DataSet GetDataSet(string commandText) 
		{
			return GetDataSet(commandText, CommandType.Text);
		}

		/// <summary>
		/// Executes a command against the database and returns the result in a populated DataSet.
		/// </summary>
		/// <param name="commandText">The text of the command to execute</param>
		/// <param name="commandType">The type of the command to execute</param>
		/// <param name="commandTimeout">Timeout</param>
		/// <returns>A populated DataSet</returns>
		public DataSet GetDataSet(string commandText, CommandType commandType, int commandTimeout) 
		{
			SqlCommand command = new SqlCommand(commandText); 
			command.CommandTimeout = commandTimeout;
			command.CommandType = commandType;
			PopulateCommand(command);
			return GetDataSet(command);
		}

		/// <summary>
		/// Executes a command against the database and returns the result in a populated DataSet.
		/// </summary>
		/// <param name="commandText">The text of the command to execute</param>
		/// <param name="commandType">The type of the command to execute</param>
		/// <returns>A populated DataSet</returns>
		public DataSet GetDataSet(string commandText, CommandType commandType) 
		{
			SqlCommand command = new SqlCommand(commandText); 
			command.CommandType = commandType;
			PopulateCommand(command);
			return GetDataSet(command);
		}

		/// <summary>
		/// Executes a command against the database and returns the result in a populated DataSet.
		/// </summary>
		/// <param name="command">A SqlCommand instance to use for executing the command</param>
		/// <returns>A populated DataSet</returns>
		public DataSet GetDataSet(SqlCommand command) 
		{
			if (ConnectionString == null || ConnectionString.Trim() == string.Empty) 
			{
				throw new Exception("ConnectionString is not defined");
			}

			SqlConnection connection = new SqlConnection(ConnectionString);
			command.Connection = connection;

			try 
			{
				connection.Open();
				SqlDataReader reader = command.ExecuteReader();
				DataSet dataSet = ConvertToDataSet(reader);
				reader.Close();
				return dataSet;				
			} 
			catch (SqlException se) 
			{
				throw new Exception("Sql Error executing command [" + command.CommandText + "]", se);
			}
			catch (Exception e) 
			{
				throw new Exception("Error executing command [" + command.CommandText + "]", e);
			}
			finally 
			{
				if (connection.State != ConnectionState.Closed) 
				{
					connection.Close();
				}
				command.Dispose();
			}			
		}

		private DataSet ConvertToDataSet(SqlDataReader reader)
		{
			DataSet dataSet = new DataSet();
			do
			{
				// Create new data table
				DataTable schemaTable = reader.GetSchemaTable();
				DataTable dataTable = new DataTable();

				if (schemaTable != null)
				{
					// A query returning records was executed
					for ( int i = 0; i < schemaTable.Rows.Count; i++ )
					{
						DataRow dataRow = schemaTable.Rows[i];
						// Create a column name that is unique in the data table
						string columnName = (string)dataRow["ColumnName"]; 
						// Add the column definition to the data table
						DataColumn column = new DataColumn(columnName, (Type)dataRow["DataType"]);
						dataTable.Columns.Add(column);
					}
					dataSet.Tables.Add(dataTable);

					// Fill the data table we just created
					while ( reader.Read() )
					{
						DataRow dataRow = dataTable.NewRow();
						for ( int i = 0; i < reader.FieldCount; i++ )
							dataRow[ i ] = reader.GetValue( i );
						dataTable.Rows.Add( dataRow );
					}
				}
				else
				{
					// No records were returned
					DataColumn column = new DataColumn("RowsAffected");
					dataTable.Columns.Add(column);
					dataSet.Tables.Add( dataTable );
					DataRow dataRow = dataTable.NewRow();
					dataRow[0] = reader.RecordsAffected;
					dataTable.Rows.Add( dataRow );
				}
			}
			while (reader.NextResult()); // at the end so we don't skip first
			return dataSet;
		}
	}
}
