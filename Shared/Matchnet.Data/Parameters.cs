using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Matchnet.Data
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Parameters
	{
		private ArrayList _parameters;

		/// <summary>
		/// 
		/// </summary>
		public Parameters()
		{
			_parameters = new ArrayList();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="parameter"></param>
		public void Add(Parameter parameter)
		{
			for (Int32 parameterNum = 0; parameterNum < _parameters.Count; parameterNum++)
			{
				if (((Parameter)_parameters[parameterNum]).Name.ToLower() == parameter.Name.ToLower())
				{
					throw new Exception("Parameter \"" + parameter.Name + "\" has already been added.");
				}
			}

			_parameters.Add(parameter);
		}


		/// <summary>
		/// 
		/// </summary>
		public Parameter this[Int32 index]
		{
			get
			{
				return _parameters[index] as Parameter;
			}
		}


		public Parameter this[string name]
		{
			get
			{
				//this should use a hashtable, but it is rarely used and i don't want to break the interface on this serializable object
				for (Int32 i = 0; i < _parameters.Count; i++)
				{
					Parameter parameter =  _parameters[i] as Parameter;
					if (parameter.Name == name)
					{
						return parameter;
					}
				}

				throw new Exception("Parameter \"" + name + "\" not found.");
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public Int32 Count
		{
			get
			{
				return _parameters.Count;
			}
		}
	}
}
