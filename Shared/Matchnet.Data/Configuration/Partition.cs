using System;
using System.Collections;

namespace Matchnet.Data.Configuration
{
	/// <summary>
	/// 
	/// </summary>
	public class Partition
	{
		private PhysicalDatabases _physicalDatabases;
		private Int16 _offset;

		internal Partition(Int16 offset)
		{
			_offset = offset;
			_physicalDatabases = new PhysicalDatabases();
		}


		/// <summary>
		/// 
		/// </summary>
		public PhysicalDatabases PhysicalDatabases
		{
			get
			{
				return _physicalDatabases;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 Offset
		{
			get
			{
				return _offset;
			}
		}
	}
}
