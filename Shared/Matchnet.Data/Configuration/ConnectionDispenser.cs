using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Xml;

using Matchnet.Data.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Lpd;

namespace Matchnet.Data.Configuration
{
	/// <summary>
	/// Provides a fast, centralized runtime repository of database connection information and a singular
	/// gateway for retrieving connection objects and connection strings.
	/// </summary>
	public sealed class ConnectionDispenser
	{
		/// <summary>
		/// The one and only instance.
		/// </summary>
		public static readonly ConnectionDispenser Instance = new ConnectionDispenser();
		
		// Represents the frequency by which the configuration will be reloaded (in seconds)
		private const int DEFAULT_REFRESH_RATE = 30; // seconds

		private DateTime _timeCheck = DateTime.MinValue;

		private LogicalDatabases _logicalDatabases;
		private ReaderWriterLock _lockLogicalDatabases;

		internal static readonly string ProcessName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;

		private ConnectionDispenser() 
		{
			_lockLogicalDatabases = new ReaderWriterLock();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="logicalDatabaseName"></param>
		/// <returns></returns>
		public LogicalDatabase GetLogicalDatabase(string logicalDatabaseName)
		{
			load();
			if (_logicalDatabases == null)
			{
				throw new Exception("Failure retrieving database metadata.  _logicalDatabases was null.");
				
			}

			return _logicalDatabases[logicalDatabaseName.ToLower()];
		}


		/// <summary>
		/// 
		/// </summary>
		public string Version
		{
			get
			{
				return _logicalDatabases.Version.ToString();
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public LogicalDatabases LogicalDatabases
		{
			get
			{
				return _logicalDatabases;
			}
		}

		private bool isCurrent()
		{
			return (DateTime.Now - _timeCheck).TotalSeconds <= DEFAULT_REFRESH_RATE;
		}


		private void load()
		{
			_lockLogicalDatabases.AcquireReaderLock(-1);

			try
			{
				if (!isCurrent())
				{
					_lockLogicalDatabases.UpgradeToWriterLock(-1);

					/*
					bail out if someone else got the version writer lock first and reloaded the config
					this will prevent multiple re-loads back to back under heavy loads
					*/
					if (isCurrent() && _logicalDatabases != null)
					{
						return;
					}

					LogicalDatabases logicalDatabases = null;
					try
					{
						logicalDatabases = LpdSA.Instance.GetLogicalDatabases();
					}
					catch (Exception ex)
					{
						throw new Exception("Failure retrieving database metadata.", ex);
					}

					if (_logicalDatabases == null || _logicalDatabases.Version != logicalDatabases.Version)
					{
						_logicalDatabases = logicalDatabases;
					}

					_timeCheck = DateTime.Now;
				}
			}
			finally
			{
				if (_lockLogicalDatabases.IsReaderLockHeld || _lockLogicalDatabases.IsWriterLockHeld)
				{
					_lockLogicalDatabases.ReleaseLock();
				}
			}
		}


		/// <summary>
		/// Returns a new System.Data.SqlConnection object
		/// </summary>
		/// <param name="logicalDatabase">The logical name of the database</param>
		/// <param name="key">The key of the item to provide a connection string for</param>
		/// <returns>A System.Data.SqlConnection</returns>
		public SqlConnection GetSqlConnection(string logicalDatabase, int key)
		{
			return new SqlConnection(GetLogicalDatabase(logicalDatabase).GetPartition(key).PhysicalDatabases.GetRandomInstance().ConnectionString);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="logicalDatabase"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public string GetConnectionString(string logicalDatabase, int key)
		{
			return GetLogicalDatabase(logicalDatabase).GetPartition(key).PhysicalDatabases.GetRandomInstance().ConnectionString;
		}
	}
}
