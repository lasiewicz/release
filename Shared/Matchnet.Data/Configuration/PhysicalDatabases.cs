using System;
using System.Collections;


namespace Matchnet.Data.Configuration
{
	/// <summary>
	/// 
	/// </summary>
	public class PhysicalDatabases
	{
		private ArrayList _physicalDatabases;
		private Hashtable _physicalDatabasesKey;

		internal PhysicalDatabases()
		{
			_physicalDatabases = new ArrayList();
			_physicalDatabasesKey = new Hashtable();
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="physicalDatabase"></param>
		public void Add(PhysicalDatabase physicalDatabase)
		{
			_physicalDatabases.Add(physicalDatabase);
			_physicalDatabasesKey.Add(physicalDatabase.ConnectionString, physicalDatabase);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="connectionString"></param>
		/// <returns></returns>
		public PhysicalDatabase GetDatabaseByConnectionString(string connectionString)
		{
			return _physicalDatabasesKey[connectionString] as PhysicalDatabase;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public PhysicalDatabase GetRandomInstance()
		{
			ArrayList readList = new ArrayList();
			Int32 instanceIndex;
			Int32 totalCount = this.Count;
			Int32 upCount;

			if (totalCount > 0)
			{
				//is there only one?
				if (totalCount == 1)
				{
					PhysicalDatabase physicalDatabase = this[0];

					if (!physicalDatabase.IsActive)
					{
						return null;
					}

					return physicalDatabase;
				}


				//pick and random physical database that is active and not failed
				for (int num = 0; num < totalCount; num++)
				{
					PhysicalDatabase physicalDatabase = this[num];
					if (physicalDatabase.IsActive && !physicalDatabase.IsFailed)
					{
						readList.Add(physicalDatabase);
					}
				}

				upCount = readList.Count;
				if (upCount > 0)
				{
					instanceIndex = (Int32)(System.Math.Floor(upCount * (new Random((int)DateTime.Now.Ticks).NextDouble())));
					return (PhysicalDatabase)readList[instanceIndex];
				}


				//none found, pick any active
				readList.Clear();
				for (int num = 0; num < totalCount; num++)
				{
					PhysicalDatabase physicalDatabase = this[num];
					if (physicalDatabase.IsActive)
					{
						readList.Add(physicalDatabase);
					}
				}

				upCount = readList.Count;
				if (upCount > 0)
				{
					instanceIndex = (Int32)(System.Math.Floor(upCount * (new Random((int)DateTime.Now.Ticks).NextDouble())));
					return (PhysicalDatabase)readList[instanceIndex];
				}
			}

			//none found
			return null;
		}


		/// <summary>
		/// 
		/// </summary>
		public PhysicalDatabase this[Int32 index]
		{
			get
			{
				return (PhysicalDatabase)_physicalDatabases[index];
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 Count
		{
			get
			{
				return _physicalDatabases.Count;
			}
		}
	}
}
