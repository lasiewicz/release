using System;
using System.Collections;
using System.Threading;

using Matchnet.Data.Hydra;


namespace Matchnet.Data.Configuration
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class PhysicalDatabase
	{
		private string _serverName;
		private string _physicalDatabaseName;
		private bool _isActive;
		private bool _isRecovering = true;
		private Int16 _failureCount = 0;
		private Int16 _failureThreshold;
		private ReaderWriterLock _lock;
		private bool _isUsed = false;
		private Queue _errors;

		internal PhysicalDatabase(string serverName,
			string physicalDatabaseName,
			bool isActive,
			Int16 failureThreshold)
	{
			_lock = new ReaderWriterLock();

			_serverName = serverName;
			_physicalDatabaseName = physicalDatabaseName;
			_isActive = isActive;
			_failureThreshold = failureThreshold;
			_errors = new Queue();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="error"></param>
		public void AddError(HydraError error)
		{
#if DEBUG
			System.Diagnostics.Trace.WriteLine("__addError\t" + error.Description.Replace("\r", "").Replace("\n", ""));
#endif
			lock (_errors.SyncRoot)
			{
				_errors.Enqueue(error);

				while (_errors.Count > 10)
				{
					_errors.Dequeue();
				}
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public HydraError[] Errors
		{
			get
			{
				lock (_errors.SyncRoot)
				{
					HydraError[] errors = new HydraError[_errors.Count];
					Int32 errorNum = 0;

					while (_errors.Count > 0)
					{
						errors[errorNum] = _errors.Dequeue() as HydraError;
					}

					return errors;
				}
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsFailed
		{
			get
			{
				return (this.FailureCount >= _failureThreshold);
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public void IncrementFailureCount()
		{
			bool logError = false;

			_lock.AcquireWriterLock(-1);
			try
			{
				if (_failureCount < _failureThreshold)
				{
					_failureCount++;
					if (_failureCount == _failureThreshold && _failureThreshold > 0)
					{
						logError = true;
					}
				}
			}
			finally
			{
				_lock.ReleaseLock();
			}

			if (logError)
			{
				System.Diagnostics.EventLog.WriteEntry(ConnectionDispenser.ProcessName,
					"Physical database failure count has met threshold.\r\n\r\n" +
					"threshold: " + _failureThreshold.ToString() + "\r\n" +
					"server: " + _serverName + "\r\n" +
					"database: " + _physicalDatabaseName,
					System.Diagnostics.EventLogEntryType.Error);
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public void ResetFailureCount()
		{
			if (this.FailureCount > 0)
			{
				_lock.AcquireWriterLock(-1);
				try
				{
					_failureCount = 0;
				}
				finally
				{
					_lock.ReleaseLock();
				}
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public Int16 FailureThreshold
		{
			get
			{
				return _failureThreshold;
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public Int16 FailureCount
		{
			get
			{
				_lock.AcquireReaderLock(-1);
				try
				{
					return _failureCount;
				}
				finally
				{
					_lock.ReleaseLock();
				}
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public bool IsUsed
		{
			get
			{
				return _isUsed;
			}
			set
			{
				_isUsed = value;
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public string ServerName
		{
			get
			{
				return _serverName;
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public string PhysicalDatabaseName
		{
			get
			{
				return _physicalDatabaseName;
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public bool IsActive
		{
			get
			{
				return _isActive;
			}
			set
			{
				_isActive = value;
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public bool IsRecovering
		{
			get
			{
				return _isRecovering;
			}
			set
			{
				//System.Diagnostics.Trace.WriteLine("__IsRecovering_set = " + value.ToString() + " (" + this.ConnectionString + ")");
				_isRecovering = value;
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public string ConnectionString
		{
			get
			{
				return "Server=" + ServerName + ";Database=" + PhysicalDatabaseName + ";Integrated Security=SSPI";
			}
		}
	}
}
