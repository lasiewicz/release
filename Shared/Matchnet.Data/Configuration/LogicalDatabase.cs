using System;
using System.Collections;
using System.Threading;


namespace Matchnet.Data.Configuration
{
	/// <summary>
	/// 
	/// </summary>
	public enum HydraModeType : int
	{
		/// <summary></summary>
		None = 0,
		/// <summary></summary>
		Spray = 1,
		/// <summary></summary>
		Transactional = 2
	}

	/// <summary>
	/// 
	/// </summary>
	public class LogicalDatabase
	{
		private Hashtable _partitions;
		private string _logicalDatabaseName;
		private HydraModeType _hydraMode;
		private Int16 _hydraThreads;
		private ArrayList _recoveryList;
		private ReaderWriterLock _recoveryLock;

		internal LogicalDatabase(string logicalDatabaseName,
			HydraModeType hydraMode,
			Int16 hydraThreads)
		{
			_partitions = new Hashtable();
			_logicalDatabaseName = logicalDatabaseName;
			_hydraMode = hydraMode;
			_hydraThreads = hydraThreads;
			_recoveryList = new ArrayList();
			_recoveryLock = new ReaderWriterLock();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="partition"></param>
		public void AddPartition(Partition partition)
		{
			_partitions.Add(partition.Offset, partition);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public Int16 GetOffset(Int32 key)
		{
			Int16 offset = 0;

			if (key > 0)
			{
				offset = (Int16)(key % this.PartitionCount);
			}

			return offset;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public Partition GetPartition(Int32 key)
		{
			return this[GetOffset(key)];
		}


		/// <summary>
		/// 
		/// </summary>
		public Partition this[Int16 offset]
		{
			get
			{
				return _partitions[offset] as Partition;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 PartitionCount
		{
			get
			{
				return (Int16)_partitions.Count;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string LogicalDatabaseName
		{
			get
			{
				return _logicalDatabaseName;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public HydraModeType HydraMode
		{
			get
			{
				return _hydraMode;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 HydraThreads
		{
			get
			{
				return _hydraThreads;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="physicalDatabase"></param>
		public void AddToRecoveryList(PhysicalDatabase physicalDatabase)
		{
			_recoveryLock.AcquireWriterLock(-1);
			try
			{
				if (!_recoveryList.Contains(physicalDatabase))
				{
					_recoveryList.Add(physicalDatabase);
					physicalDatabase.IsRecovering = true;
				}
			}
			finally
			{
				_recoveryLock.ReleaseLock();
			}
		}


		public bool IsInRecoveryMode(short offset)
		{
			Partition partition = this[offset];
			for (Int32 pdbNum = 0; pdbNum < partition.PhysicalDatabases.Count; pdbNum++)
			{
				if (_recoveryList.Contains(partition.PhysicalDatabases[pdbNum]))
				{
					return true;
				}
			}

			return false;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="physicalDatabase"></param>
		public void RemoveFromRecoveryList(PhysicalDatabase physicalDatabase)
		{
			PhysicalDatabase pdbFound = null;
			_recoveryLock.AcquireWriterLock(-1);
			try
			{
				for (Int32 pdbNum = 0; pdbNum < _recoveryList.Count; pdbNum++)
				{
					PhysicalDatabase pdb = _recoveryList[pdbNum] as PhysicalDatabase;
					if (pdb.ServerName == physicalDatabase.ServerName && pdb.PhysicalDatabaseName == physicalDatabase.PhysicalDatabaseName)
					{
						pdbFound = pdb;
					}
				}

				if (pdbFound != null)
				{
					_recoveryList.Remove(pdbFound);
					physicalDatabase.IsRecovering = false;
				}
			}
			finally
			{
				_recoveryLock.ReleaseLock();
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool RecoveryMode
		{
			get
			{
				_recoveryLock.AcquireReaderLock(-1);
				try
				{
					return _recoveryList.Count > 0;
				}
				finally
				{
					_recoveryLock.ReleaseLock();
				}
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ArrayList GetRecoveryList()
		{
			_recoveryLock.AcquireReaderLock(-1);
			try
			{
				return (ArrayList)_recoveryList.Clone();
			}
			finally
			{
				_recoveryLock.ReleaseLock();
			}
		}
	}
}
