using System;
using System.Collections;

namespace Matchnet.Data.Configuration
{
	/// <summary>
	/// 
	/// </summary>
	public class LogicalDatabases : System.Collections.Specialized.NameObjectCollectionBase
	{
		internal LogicalDatabases()
		{

		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="logicalDatabase"></param>
		public void AddLogicalDatabase(LogicalDatabase logicalDatabase)
		{
			base.BaseAdd(logicalDatabase.LogicalDatabaseName.ToLower(), logicalDatabase);
		}


		/// <summary>
		/// 
		/// </summary>
		public LogicalDatabase this[string logicalDatabaseName]
		{
			get
			{
				return base.BaseGet(logicalDatabaseName.ToLower()) as LogicalDatabase;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public LogicalDatabase this[Int32 index]
		{
			get
			{
				return base.BaseGet(index) as LogicalDatabase;
			}
		}
	}
}
