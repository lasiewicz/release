using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Data.Configuration;


namespace Matchnet.Data
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Command
	{
		private Parameters _parameters;
		private string _logicalDatabaseName;
		private string _storedProcedureName;
		private Int32 _key;
		private string _connectionString = null;
	    private Int32 dbTimeoutInSecs = Constants.NULL_INT;

		/// <summary>
		/// 
		/// </summary>
		public Command()
		{
			_parameters = new Parameters();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="logicalDatabaseName"></param>
		/// <param name="storedProcedureName"></param>
		/// <param name="key"></param>
		public Command(string logicalDatabaseName,
			string storedProcedureName,
			Int32 key)
		{
			_parameters = new Parameters();
			_logicalDatabaseName = logicalDatabaseName;
			_storedProcedureName = storedProcedureName;
			_key = key;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="dataType"></param>
		/// <param name="parameterDirection"></param>
		/// <param name="parameterValue"></param>
		public void AddParameter(string name, SqlDbType dataType, ParameterDirection parameterDirection, Object parameterValue) 
		{
			_parameters.Add(new Parameter(name, dataType, parameterDirection, parameterValue));
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="dataType"></param>
		/// <param name="parameterDirection"></param>
		public void AddParameter(string name, SqlDbType dataType, ParameterDirection parameterDirection) 
		{
			_parameters.Add(new Parameter(name, dataType, parameterDirection, 0));
		}

		
		public void AddParameter(string name, SqlDbType dataType, Int32 size, ParameterDirection parameterDirection) 
		{
			_parameters.Add(new Parameter(name, dataType, parameterDirection, size));
		}


		/// <summary>
		/// 
		/// </summary>
		public Parameters Parameters
		{
			get
			{
				return _parameters;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string LogicalDatabaseName
		{
			get
			{
				return _logicalDatabaseName;
			}
			set
			{
				_logicalDatabaseName = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string StoredProcedureName
		{
			get
			{
				return _storedProcedureName;
			}
			set
			{
				_storedProcedureName = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 Key
		{
			get
			{
				return _key;
			}
			set
			{
				_key = value;
			}
		}

	    public Int32 DBTimeoutInSecs
	    {
	        get { return dbTimeoutInSecs; }
	        set { dbTimeoutInSecs = value; }
	    }
		
		internal string ConnectionString
		{
			get
			{
				return _connectionString;
			}
			set
			{
				_connectionString = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="connectionString"></param>
		public void SetConnectionString(string connectionString)
		{
			this.ConnectionString = connectionString;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="logicalDatabaseName"></param>
		/// <param name="key"></param>
		/// <param name="connectionString"></param>
		/// <param name="sqlCommand"></param>
		/// <returns></returns>
		public static Command Create(string logicalDatabaseName,
			Int32 key,
			string connectionString,
			SqlCommand sqlCommand)
		{
			Command command = new Command(logicalDatabaseName, sqlCommand.CommandText, key);
			command.ConnectionString = connectionString;
			for (Int32 parameterNum = 0; parameterNum < sqlCommand.Parameters.Count; parameterNum++)
			{
				SqlParameter parameter = sqlCommand.Parameters[parameterNum];
				command.AddParameter(parameter.ParameterName,
					parameter.SqlDbType,
					parameter.Direction,
					parameter.Value);
			}

			return command;
		}
	}
}
