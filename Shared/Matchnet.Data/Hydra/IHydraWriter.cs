using System;
using System.Messaging;


namespace Matchnet.Data.Hydra
{
	/// <summary>
	/// 
	/// </summary>
	public interface IHydraWriter
	{
		/// <summary></summary>
		void Stop();
		/// <summary></summary>
		void Join();
	}
}
