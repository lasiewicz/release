using System;
using System.Messaging;


namespace Matchnet.Data.Hydra
{
	public class MessageSendTask : ISyncWriterTask
	{
		private string _queuePath;
		private object _message;


		public MessageSendTask(string queuePath,
			object message)
		{
			_queuePath = queuePath;
			_message = message;
		}


		#region ISyncWriterTask Members
		public void Process(Int32 returnValue)
		{
			if (returnValue == 0)
			{
				MessageQueue queue = new MessageQueue(_queuePath);
				queue.Formatter = new BinaryMessageFormatter();
				queue.Send(_message, MessageQueueTransactionType.Automatic);
				queue.Dispose();
			}
		}


		public void Cleanup()
		{
		}
		#endregion
	}
}
