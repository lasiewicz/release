#region

using System;
using System.Data.SqlClient;
using System.Messaging;
using System.Threading;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Exceptions;

#endregion

namespace Matchnet.Data.Hydra
{
    public class RecoveryWorker
    {
        private static readonly TimeSpan Timespan = new TimeSpan(0, 0, 0, 2);
        private readonly PhysicalDatabase _pdb;
        private readonly MessageQueue _queue;
        private bool _runnable;
        private Thread _t;

        public RecoveryWorker(MessageQueue queue,
            PhysicalDatabase pdb)
        {
            _queue = queue;
            _pdb = pdb;
        }

        /// <summary>
        /// </summary>
        public bool HasRecovered
        {
            get { return !_t.IsAlive; }
        }

        public void Start()
        {
            System.Diagnostics.Trace.WriteLine("__recovery start " + _pdb.ConnectionString);
            _runnable = true;
            _t = new Thread(WorkCycle);
            _t.Start();
            System.Diagnostics.Trace.WriteLine("___t.IsAlive: " + _t.IsAlive);
        }

        public void Stop()
        {
            System.Diagnostics.Trace.WriteLine("__RecoveryWorker.Stop()");
            _runnable = false;
        }

        public void Join()
        {
            if (_t.IsAlive)
            {
                _t.Join();
            }
        }

        private void WorkCycle()
        {
            while (_runnable)
            {
                MessageQueueTransaction tran = null;
                string messageId = "";
                Command command = null;

                try
                {
                    tran = new MessageQueueTransaction();
                    tran.Begin();

                    try
                    {
                        var message = _queue.Receive(Timespan, tran);
                        //System.Diagnostics.Trace.WriteLine("__recovery quueue receives message: " + _pdb.ConnectionString);
                        if (message != null)
                        {
                            messageId = message.Id;
                            command = message.Body as Command;
                            //System.Diagnostics.Trace.WriteLine("__recovery quueue got command: " + _pdb.ConnectionString);
                            message.Dispose();
                        }
                    }
                    catch (MessageQueueException messageQueueException)
                    {
                        // ErrorCode property returns messageQueueException.ErrorCode = -2147467259
                        // Use MessageQueueErrorCode which is correctly populated
                        if (messageQueueException.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                        {
                            throw;
                        }

                        messageId = null;
                        command = null;
                        //System.Diagnostics.Trace.WriteLine("__no messages in " + _queue.Path);
                        break;
                    }

                    if (command != null)
                    {
                        try
                        {
                            //System.Diagnostics.Trace.WriteLine("__recovery msg");
                            ClientHelper.ExecuteNonQuery(command);

                            tran.Commit();
                        }
                        catch (Exception exception)
                        {
                            var addedToQueue = false;

                            // this loop should always eventually end
                            while (exception != null && exception.InnerException != null)
                            {
                                exception = exception.InnerException as SqlException;
                                if (exception != null) break;
                            }

                            // we want to handle only SQL specific exceptions
                            if (exception is SqlException)
                            {
                                var sqlException = exception as SqlException;
                                // we have a known issue with primary key violation, this will send any violation to the no retry queue
                                if (sqlException.Number == (int) SqlErrorCodes.PrimaryKeyViolation)
                                {
                                    var unrecoverableQueue =
                                        HydraUtility.GetQueue(string.Format("{0}_noretry", _queue.QueueName));
                                    unrecoverableQueue.Send(command, tran);
                                    unrecoverableQueue.Dispose();
                                    tran.Commit();
                                    addedToQueue = true;
                                }
                            }

                            // we weren't able to handle the exception, abort the transaction and add a error entry to the pdb, this is what you see in HydraUtil
                            if (!addedToQueue)
                            {
                                tran.Abort();

                                if (exception != null)
                                    _pdb.AddError(new HydraError(_queue.Path,
                                        messageId,
                                        exception.ToString()));

                                Thread.Sleep(1000);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // this writes to the event log
                    new ServiceBoundaryException("Matchnet.Data", "An exception occurred in RecoveryWorker.", ex);

                    if (tran != null)
                    {
                        tran.Abort();
                    }

                    System.Diagnostics.Trace.WriteLine("__recovery exception 2:" + ex);
                    Thread.Sleep(2000);
                }
            }

            System.Diagnostics.Trace.WriteLine("__recoverycomplete " + _runnable);
        }
    }
}