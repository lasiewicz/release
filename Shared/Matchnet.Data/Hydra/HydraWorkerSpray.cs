#region

using System;
using System.Data.SqlClient;
using System.Messaging;
using System.Threading;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data.Configuration;

#endregion

namespace Matchnet.Data.Hydra
{
    internal class HydraWorkerSpray
    {
        private static readonly ReaderWriterLock QueueLock = new ReaderWriterLock();
        private static readonly TimeSpan Timespan = new TimeSpan(0, 0, 6);
        private readonly MessageQueue _queue;
        private readonly MessageQueue _queueError;
        private readonly Thread _thread;
        private readonly HydraWriterSpray _writer;
        private bool _errorMode = true;

        public HydraWorkerSpray(HydraWriterSpray writer)
        {
            _writer = writer;

            QueueLock.AcquireWriterLock(-1);
            try
            {
                _queue = HydraUtility.GetQueue(writer.GetKey());
                _queueError = HydraUtility.GetQueue(writer.GetKey() + "_Error");
            }
            finally
            {
                QueueLock.ReleaseLock();
            }

            _thread = new Thread(WorkCycle);
            _thread.Start();
        }

        public void Join()
        {
            _thread.Join();
        }

        private void WorkCycle()
        {
            Command command = null;
            string messageId = "";
            string trace = "";
            while (_writer.Runnable)
            {
                MessageQueueTransaction tran = new MessageQueueTransaction();
                tran.Begin();

                try
                {
                    MessageQueue queue;
                    if (_errorMode)
                    {
                        queue = _queueError;
                    }
                    else
                    {
                        queue = _queue;
                    }

                    try
                    {
                        messageId = "";
                        Message message = queue.Receive(Timespan, tran);
                        command = message.Body as Command;
                        messageId = message.Id;
                        message.Dispose();
                    }
                    catch (MessageQueueException messageQueueException)
                    {
                        // ErrorCode property returns messageQueueException.ErrorCode = -2147467259
                        // Use MessageQueueErrorCode which is correctly populated
                        if (messageQueueException.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                        {
                            throw;
                        }

                        if (_errorMode)
                        {
                            _errorMode = false;
                        }
                    }

                    if (command != null)
                    {
                        PhysicalDatabase pdb = null;

                        try
                        {
                            trace = command.ConnectionString + ", " + command.LogicalDatabaseName;
                            pdb =
                                ConnectionDispenser.Instance.LogicalDatabases[command.LogicalDatabaseName.ToLower()]
                                    .GetPartition(command.Key)
                                    .PhysicalDatabases.GetDatabaseByConnectionString(command.ConnectionString);

                            pdb.IsUsed = true;
                            if (pdb.IsActive)
                            {
                                ClientHelper.ExecuteNonQuery(command);
                                tran.Commit();
                            }
                            else
                            {
                                tran.Abort();
                                Thread.Sleep(1000);
                            }
                        }
                        catch (Exception exception)
                        {
                            pdb.AddError(new HydraError((pdb != null) ? queue.Path : "",
                                messageId,
                                exception.ToString()));

                            if (!_errorMode)
                            {
                                var addedToQueue = false;

                                // this loop should always eventually end
                                while (exception.InnerException != null)
                                {
                                    exception = exception.InnerException as SqlException;
                                    if (exception != null) break;
                                }

                                // we want to handle only SQL specific exceptions
                                if (exception is SqlException)
                                {
                                    var sqlException = exception as SqlException;
                                    // we have a known issue with primary key violation, this will send any violation to the no retry queue
                                    if (sqlException.Number == (int) SqlErrorCodes.PrimaryKeyViolation)
                                    {
                                        var unrecoverableQueue = HydraUtility.GetQueue(_writer.GetKey() + "_noretry");
                                        unrecoverableQueue.Send(command, tran);
                                        unrecoverableQueue.Dispose();
                                        tran.Commit();
                                        addedToQueue = true;
                                    }
                                }

                                // something other than primary key violation occurred, send to the error queue for further analysis
                                if (!addedToQueue)
                                {
                                    var queueError = HydraUtility.GetQueue(_writer.GetKey() + "_Error");
                                    queueError.Send(command, tran);
                                    queueError.Dispose();
                                    _errorMode = true;
                                    tran.Commit();
                                }
                            }
                            else
                            {
                                tran.Abort();
                                Thread.Sleep(1000);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__a, " + trace + ", " +
                                                       ex.ToString().Replace("\r", "").Replace("\n", ""));
                    Thread.Sleep(2000);
                    tran.Abort();
                }

                command = null;
            }
        }
    }
}