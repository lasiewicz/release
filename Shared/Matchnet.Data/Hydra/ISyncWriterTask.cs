using System;

namespace Matchnet.Data.Hydra
{
	public interface ISyncWriterTask
	{
		void Process(Int32 returnValue);

		void Cleanup();
	}
}
