#region

using System;
using System.Collections;
using System.Threading;

#endregion

namespace Matchnet.Data.Hydra
{
    /// <summary>
    ///     This is the service that manages transactional threads
    /// </summary>
    internal class HydraWriterTransactional : IHydraWriter
    {
        private readonly ReaderWriterLock _lock;
        private readonly ArrayList _workers;

        public HydraWriterTransactional(string logicalDatabaseName,
            Int16 offset)
        {
            _lock = new ReaderWriterLock();
            LogicalDatabaseName = logicalDatabaseName;
            Offset = offset;
            _workers = new ArrayList();
        }

        public bool Runnable { get; private set; }
        public string LogicalDatabaseName { get; private set; }
        public Int16 Offset { get; private set; }

        public void Stop()
        {
            if (Runnable)
            {
                Runnable = false;
            }
        }

        public void Join()
        {
            SetThreadCount(0);
        }

        public void Start(Int16 threadCount)
        {
            if (!Runnable)
            {
                Runnable = true;
            }

            SetThreadCount(threadCount);
        }

        public void SetThreadCount(Int16 threadCount)
        {
            _lock.AcquireWriterLock(-1);
            try
            {
                while (_workers.Count < threadCount)
                {
                    HydraWorkerTransactional worker = new HydraWorkerTransactional(this);
                    _workers.Add(worker);
                }

                while (_workers.Count > threadCount)
                {
                    HydraWorkerTransactional worker = (HydraWorkerTransactional) _workers[0];
                    worker.Join();
                    _workers.RemoveAt(0);
                }
            }
            finally
            {
                _lock.ReleaseLock();
            }
        }

        public string GetKey()
        {
            return HydraUtility.GetKey(LogicalDatabaseName, Offset);
        }
    }
}