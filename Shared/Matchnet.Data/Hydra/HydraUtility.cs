using System;
using System.Messaging;


namespace Matchnet.Data.Hydra
{
    /// <summary>
    ///     Error codes from sql db
    /// </summary>
    public enum SqlErrorCodes
    {
        /// <summary>
        /// </summary>
        MissingParameter = 201,

        /// <summary>
        ///     https://msdn.microsoft.com/en-us/library/ms151757%28v=sql.120%29.aspx
        /// </summary>
        PrimaryKeyViolation = 2627
    }

    /// <summary>
    /// 
    /// </summary>
    public class HydraUtility
    {
        private HydraUtility()
        {
        }

        /// <summary>
        ///     Returns MSMQ queue based on the key value
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static MessageQueue GetQueue(string key)
        {
            var queuePath = @".\private$\" + key;

            var queue = !MessageQueue.Exists(queuePath) ? MessageQueue.Create(queuePath, true) : new MessageQueue(queuePath);

            queue.Formatter = new BinaryMessageFormatter();
            
            return queue;
        }


        /// <summary>
        ///     Returns MSMQ queue based on the key value and the server name
        /// </summary>
        /// <param name="key"></param>
        /// <param name="serverName"></param>
        /// <returns></returns>
        public static MessageQueue GetQueueRecovery(string key, string serverName)
        {
            return GetQueue(key + "_inactive_" + serverName);
        }


        /// <summary>
        ///     Returns a key string based on the logical database name and the offset
        /// </summary>
        /// <param name="logicalDatabaseName"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static string GetKey(string logicalDatabaseName,
            Int16 offset)
        {
            return "Transactional_" + logicalDatabaseName + "_" + (offset + 1);
        }
    }
}
