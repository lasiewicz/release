#region

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Messaging;
using System.Threading;
using System.Transactions;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data.Configuration;
using Matchnet.Exceptions;

#endregion

namespace Matchnet.Data.Hydra
{
    internal class HydraWorkerTransactional
    {
        private static readonly ReaderWriterLock QueueLock = new ReaderWriterLock();
        private static readonly TimeSpan Timespan = new TimeSpan(0, 0, 6);
        private static readonly TimeSpan TimespanRecovery = new TimeSpan(0, 0, 0, 10);
        private readonly MessageQueue _queue;
        private readonly Hashtable _recoveryWorkers;
        private readonly Thread _thread;
        private readonly HydraWriterTransactional _writer;

        public HydraWorkerTransactional(HydraWriterTransactional writer)
        {
            _writer = writer;
            _recoveryWorkers = new Hashtable();

            QueueLock.AcquireWriterLock(-1);
            try
            {
                _queue = HydraUtility.GetQueue(writer.GetKey());
            }
            finally
            {
                QueueLock.ReleaseLock();
            }

            _thread = new Thread(WorkCycle);
            _thread.Start();
        }

        public void Join()
        {
            _thread.Join();
        }

        private void WorkCycle()
        {
            string queuePath = null;

            while (_writer.Runnable)
            {
                MessageQueueTransaction tran = null;

                Command command;
                Message message;
                try
                {
                    LogicalDatabase ldb = ConnectionDispenser.Instance.GetLogicalDatabase(_writer.LogicalDatabaseName);

                    bool isInRecoveryMode = ldb.IsInRecoveryMode(_writer.Offset);
                    if (isInRecoveryMode)
                    {
                        //Console.WriteLine("__recovery " + _writer.Offset.ToString());
                        ArrayList recoveryList = ldb.GetRecoveryList();
                        for (Int32 pdbNum = 0; pdbNum < ldb[_writer.Offset].PhysicalDatabases.Count; pdbNum++)
                        {
                            PhysicalDatabase pdb = ldb[_writer.Offset].PhysicalDatabases[pdbNum];
                            //Console.WriteLine(DateTime.Now.ToString() + " " + Thread.CurrentThread.GetHashCode().ToString() + " recovery mode " + pdb.ConnectionString);

                            pdb.IsUsed = true;
                            if (recoveryList.Contains(pdb))
                            {
                                RecoveryWorker recoveryWorker = _recoveryWorkers[pdb.ConnectionString] as RecoveryWorker;
                                if (recoveryWorker == null)
                                {
                                    recoveryWorker =
                                        new RecoveryWorker(
                                            HydraUtility.GetQueueRecovery(_writer.GetKey(), pdb.ServerName), pdb);
                                    _recoveryWorkers.Add(pdb.ConnectionString, recoveryWorker);
                                    recoveryWorker.Start();
                                }

                                if (recoveryWorker.HasRecovered)
                                {
                                    ldb.RemoveFromRecoveryList(pdb);
                                    _recoveryWorkers.Remove(pdb.ConnectionString);
                                    System.Diagnostics.Trace.WriteLine("recovered " + pdb.ConnectionString);
                                }
                            }
                        }
                    }
                    else
                    {
                        tran = new MessageQueueTransaction();
                        tran.Begin();

                        //process regular queue
                        string messageId;
                        try
                        {
                            message = _queue.Receive(isInRecoveryMode ? TimespanRecovery : Timespan, tran);
                            queuePath = _queue.Path;
                            messageId = message.Id;
                            command = message.Body as Command;
                            message.Dispose();
                        }
                        catch (MessageQueueException messageQueueException)
                        {
                            messageId = null;
                            command = null;

                            // ErrorCode property returns messageQueueException.ErrorCode = -2147467259
                            // Use MessageQueueErrorCode which is correctly populated
                            if (messageQueueException.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                            {
                                throw;
                            }
                        }

                        if (command != null)
                        {
                            //Console.WriteLine("__" + Thread.CurrentThread.GetHashCode().ToString() + " processRegular");
                            PhysicalDatabases physicalDatabases = null;
                            try
                            {
                                Exception ex;
                                physicalDatabases =
                                    ConnectionDispenser.Instance.GetLogicalDatabase(command.LogicalDatabaseName)
                                        .GetPartition(command.Key)
                                        .PhysicalDatabases;

                                WriteWithTransactionScope(physicalDatabases,
                                    command,
                                    _writer.GetKey(),
                                    out ex);

                                if (ex != null)
                                {
                                    throw ex;
                                }

                                tran.Commit();
                            }
                            catch (Exception exception)
                            {
                                for (Int32 pdbNum = 0; pdbNum < physicalDatabases.Count; pdbNum++)
                                {
                                    PhysicalDatabase pdb = physicalDatabases[pdbNum];
                                    pdb.AddError(new HydraError(queuePath,
                                        messageId,
                                        exception.ToString()));
                                }

                                var addedToQueue = false;

                                // this loop should always eventually end
                                while (exception.InnerException != null)
                                {
                                    exception = exception.InnerException as SqlException;
                                    if (exception != null) break;
                                }

                                // we want to handle only SQL specific exceptions
                                if (exception is SqlException)
                                {
                                    var sqlException = exception as SqlException;
                                    // we have a known issue with primary key violation, this will send any violation to the no retry queue
                                    if (sqlException.Number == (int) SqlErrorCodes.PrimaryKeyViolation)
                                    {
                                        var unrecoverableQueue = HydraUtility.GetQueue(_writer.GetKey() + "_noretry");
                                        unrecoverableQueue.Send(command, tran);
                                        unrecoverableQueue.Dispose();
                                        tran.Commit();
                                        addedToQueue = true;
                                    }
                                }

                                if (!addedToQueue)
                                {
                                    tran.Abort();
                                    Thread.Sleep(1000);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    new ServiceBoundaryException("Matchnet.Data", "An exception occurred in Hydra.", ex);
                    if (tran != null)
                    {
                        tran.Abort();
                    }
                    //System.Diagnostics.Trace.WriteLine("__" + queuePath + "|" + messageID + "|" + ex.Message);
                    Thread.Sleep(2000);
                }

                message = null;
                command = null;
            }
        }

        private static void WriteWithTransactionScope(PhysicalDatabases physicalDatabases,
            Command command,
            string writerKey,
            out Exception ex)
        {
            Int32 pdbCount = physicalDatabases.Count;
            SqlConnection[] connections = new SqlConnection[pdbCount];
            PhysicalDatabase pdb = null;
            SqlCommand sqlCommand = null;

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    for (Int32 pdbNum = 0; pdbNum < pdbCount; pdbNum++)
                    {
                        pdb = physicalDatabases[pdbNum];

                        if (pdb.IsActive && !pdb.IsRecovering && !pdb.IsFailed)
                        {
                            SqlConnection conn = new SqlConnection(pdb.ConnectionString);
                            connections[pdbNum] = conn;

                            sqlCommand = ClientHelper.CreateSqlCommand(command);
                            sqlCommand.Connection = conn;
                            conn.Open();
                            sqlCommand.ExecuteNonQuery();
                        }
                        else
                        {
                            //Console.WriteLine("__send recovery " + pdb.ConnectionString);
                            command.ConnectionString = pdb.ConnectionString;
                            MessageQueue queue = HydraUtility.GetQueueRecovery(writerKey, pdb.ServerName);
                            queue.Send(command, MessageQueueTransactionType.Automatic);
                            queue.Dispose();
                        }
                    }

                    ex = null;
                    scope.Complete();
                }
            }
            catch (Exception exLocal)
            {
                ex = new Exception(DataUtility.buildErr(pdb.ConnectionString, sqlCommand), exLocal);
            }
            finally
            {
                foreach (SqlConnection conn in connections)
                {
                    if (conn == null) continue;
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}