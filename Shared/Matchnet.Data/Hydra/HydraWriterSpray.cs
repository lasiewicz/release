#region

using System;
using System.Collections;
using System.Threading;

#endregion

namespace Matchnet.Data.Hydra
{
    /// <summary>
    ///     This is the service that manages spray worker threads
    /// </summary>
    internal class HydraWriterSpray : IHydraWriter
    {
        private readonly ReaderWriterLock _lock;
        private readonly string _physicalDatabaseName;
        private readonly string _serverName;
        private readonly ArrayList _workers;

        public HydraWriterSpray(string serverName,
            string physicalDatabaseName)
        {
            _lock = new ReaderWriterLock();
            _serverName = serverName;
            _physicalDatabaseName = physicalDatabaseName;
            _workers = new ArrayList();
        }

        public bool Runnable { get; private set; }

        public string ServerName
        {
            get { return _serverName; }
        }

        public string PhysicalDatabaseName
        {
            get { return _physicalDatabaseName; }
        }

        public void Stop()
        {
            if (Runnable)
            {
                Runnable = false;
            }
        }

        public void Join()
        {
            SetThreadCount(0);
        }

        public void Start(Int16 threadCount)
        {
            if (!Runnable)
            {
                Runnable = true;
            }

            SetThreadCount(threadCount);
        }

        public void SetThreadCount(Int16 threadCount)
        {
            _lock.AcquireWriterLock(-1);
            try
            {
                while (_workers.Count < threadCount)
                {
                    HydraWorkerSpray worker = new HydraWorkerSpray(this);
                    _workers.Add(worker);
                }

                while (_workers.Count > threadCount)
                {
                    HydraWorkerSpray worker = (HydraWorkerSpray) _workers[0];
                    worker.Join();
                    _workers.RemoveAt(0);
                }
            }
            finally
            {
                _lock.ReleaseLock();
            }
        }

        public string GetKey()
        {
            return GetKey(ServerName, PhysicalDatabaseName);
        }

        public static string GetKey(string serverName, string physicalDatabaseName)
        {
            return "Spray_" + serverName + "_" + physicalDatabaseName;
        }
    }
}