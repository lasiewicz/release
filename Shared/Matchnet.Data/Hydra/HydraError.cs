using System;

namespace Matchnet.Data.Hydra
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class HydraError
	{
		private DateTime _dateTime;
		private string _queuePath;
		private string _messageID;
		private string _description;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="queuePath"></param>
		/// <param name="messageID"></param>
		/// <param name="description"></param>
		public HydraError(string queuePath,
			string messageID,
			string description)
		{
			_dateTime = DateTime.Now;
			_queuePath = queuePath;
			_messageID = messageID;
			_description = description;
		}


		/// <summary>
		/// 
		/// </summary>
		public DateTime DateTime
		{
			get
			{
				return _dateTime;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string QueuePath
		{
			get
			{
				return _queuePath;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string MessageID
		{
			get
			{
				return _messageID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get
			{
				return _description;
			}
		}
	}
}
