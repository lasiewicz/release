using System;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Messaging;
using System.Runtime.CompilerServices;
using System.Reflection;

using Matchnet.Data;
using Matchnet.Data.Configuration;

using Matchnet.Configuration.ValueObjects.Lpd;

namespace Matchnet.Data.Hydra
{
	[Transaction(TransactionOption.Required)]
	public class SyncWriter : ServicedComponent
	{
		public Int32 Execute(Command command,
			out Exception exception)
		{
			return Execute(command,
				null,
				out exception);
		}

		
		public Int32 Execute(Command[] commands,
			out Exception exception)
		{
			return Execute(commands,
				null,
				out exception);
		}

		
		public Int32 Execute(Command command,
			ISyncWriterTask[] syncWriterTasks,
			out Exception exception)
		{
			return Execute(new Command[]{command},
				syncWriterTasks,
				out exception);
		}

		
		public Int32 Execute(Command[] commands,
			ISyncWriterTask[] syncWriterTasks,
			out Exception exception)
		{
			exception = null;
			bool written = false;
			exception = null;
			Int32 returnValuePrev = 0;
			string connectionString = "";
			object[] connections = new object[commands.Length];
			Command command = null;

			try
			{
				for (Int32 commandNum = 0; commandNum < commands.Length; commandNum++)
				{
					command = commands[commandNum];
					Partition partition = ConnectionDispenser.Instance.GetLogicalDatabase(command.LogicalDatabaseName).GetPartition(command.Key);
					PhysicalDatabases physicalDatabases = partition.PhysicalDatabases;
					Int32 pdbCount = physicalDatabases.Count;
					SqlConnection[] commandConnections = new SqlConnection[pdbCount];
					connections[commandNum] = commandConnections;

					for (Int32 pdbNum = 0; pdbNum < pdbCount; pdbNum++)
					{
						PhysicalDatabase pdb = physicalDatabases[pdbNum];

						SqlCommand cmd = new SqlCommand();
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.CommandText = command.StoredProcedureName;
						cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
						for (Int32 parameterNum = 0; parameterNum < command.Parameters.Count; parameterNum++)
						{
							Parameter parameter = command.Parameters[parameterNum];
							SqlParameter sqlParameter = cmd.Parameters.Add(parameter.Name, parameter.DataType);
							sqlParameter.Value = parameter.ParameterValue;
							sqlParameter.Direction = parameter.ParameterDirection;
						}


						if (pdb.IsActive && !pdb.IsRecovering && !pdb.IsFailed)
						{
							connectionString = pdb.ConnectionString;
							SqlConnection conn = new SqlConnection(pdb.ConnectionString);
							commandConnections[pdbNum] = conn;

							cmd.Connection = conn;
							conn.Open();
							cmd.ExecuteNonQuery();
							Int32 returnValue = Convert.ToInt32(cmd.Parameters["@RETURN_VALUE"].Value);

							for (Int32 parameterNum = 0; parameterNum < command.Parameters.Count; parameterNum++)
							{
								Parameter parameter = command.Parameters[parameterNum];
								if (parameter.ParameterDirection == ParameterDirection.InputOutput || parameter.ParameterDirection == ParameterDirection.Output)
								{
									parameter.SetParameterValue(cmd.Parameters[parameter.Name].Value);
								}
							}

							connectionString = "";

							if (written)
							{
								if (returnValuePrev != returnValue)
								{
									throw new Exception("Return values did not match.");
								}
							}

							returnValuePrev = returnValue;

							System.Diagnostics.Trace.WriteLine("__wrote:" + pdb.ConnectionString + " " + command.StoredProcedureName);
							written = true;
						}
						else
						{
							System.Diagnostics.Trace.WriteLine("__queued:" + pdb.ConnectionString + " " + command.StoredProcedureName);
							command.SetConnectionString(pdb.ConnectionString);
							MessageQueue queue = HydraUtility.GetQueueRecovery(HydraUtility.GetKey(command.LogicalDatabaseName, partition.Offset), pdb.ServerName);
							queue.Send(command, MessageQueueTransactionType.Automatic);
							queue.Dispose();
						}
					}


					if (written == false)
					{
						throw new Exception("No active " + command.LogicalDatabaseName + " databases are active for offset " + partition.Offset.ToString());
					}
				}

				if (syncWriterTasks != null)
				{
					for (Int32 taskNum = 0; taskNum < syncWriterTasks.Length; taskNum++)
					{
						syncWriterTasks[taskNum].Process(returnValuePrev);
					}
				}

				ContextUtil.SetComplete();

				return returnValuePrev;
			}
			catch (Exception ex)
			{
				//06302008 TL -	Added inner exception message to new exception object; This was needed because a lot services are only showing the first inner exception message.
				//				Stack trace will not be added to message.  Services can be modified to retrieve that as necessary.
				//09082008 TL - Called share function to include exception logging of Command parameters.
				exception = new Exception("SyncWriter error. " + ex.Message + " " + DataUtility.buildErr(connectionString, command), ex);
				ContextUtil.SetAbort();
				return 0;
			}
			finally
			{
				for (Int32 commandNum = 0; commandNum < commands.Length; commandNum++)
				{
					SqlConnection[] commandConnections = (SqlConnection[])connections[commandNum];

					for (Int32 connectionNum = 0; connectionNum < commandConnections.Length; connectionNum++)
					{
						SqlConnection conn = commandConnections[connectionNum];
						if (conn != null)
						{
							if (conn.State == ConnectionState.Open)
							{
								conn.Close();
							}
						}
					}
				}
			}
		}
	}
}
