#region

using System;
using System.Collections;
using System.Threading;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data.Configuration;

#endregion

namespace Matchnet.Data.Hydra
{
    /// <summary>
    ///     This is the master service that manages all transactional and spray service threads
    /// </summary>
    public class HydraWriter
    {
        private readonly ReaderWriterLock _lockManagers;
        private readonly string[] _logicalDbNames;
        private readonly Hashtable _writers;
        private bool _runnable;
        private Thread _threadManager;
        private string _versionPrevious;

        /// <summary>
        /// </summary>
        /// <param name="logicalDatabaseNames"></param>
        public HydraWriter(params string[] logicalDatabaseNames)
        {
            _logicalDbNames = logicalDatabaseNames;
            _lockManagers = new ReaderWriterLock();
            _versionPrevious = "";
            _writers = new Hashtable();
        }

        /// <summary>
        /// </summary>
        public void Start()
        {
            if (_runnable) return;
            _runnable = true;
            Init();
            _threadManager = new Thread(WriterCycle);
            _threadManager.Start();
        }

        /// <summary>
        /// </summary>
        public void Stop()
        {
            if (!_runnable) return;
            _runnable = false;
            _threadManager.Join();

            _lockManagers.AcquireWriterLock(-1);
            try
            {
                _lockManagers.AcquireWriterLock(-1);
                try
                {
                    IDictionaryEnumerator enumerator = _writers.GetEnumerator();

                    while (enumerator.MoveNext())
                    {
                        ((IHydraWriter) enumerator.Value).Stop();
                    }

                    enumerator.Reset();

                    while (enumerator.MoveNext())
                    {
                        ((IHydraWriter) enumerator.Value).Join();
                    }
                }
                finally
                {
                    _lockManagers.ReleaseLock();
                }
            }
            finally
            {
                _lockManagers.ReleaseLock();
            }
        }

        private void WriterCycle()
        {
            while (_runnable)
            {
                if (_versionPrevious != ConnectionDispenser.Instance.Version)
                {
                    Init();
                }

                Thread.Sleep(1000);
            }
        }

        private void Init()
        {
            _lockManagers.AcquireWriterLock(-1);
            try
            {
                foreach (var logicalDatabaseName in _logicalDbNames)
                {
                    LogicalDatabase ldb = ConnectionDispenser.Instance.GetLogicalDatabase(logicalDatabaseName);

                    if (ldb == null)
                    {
                        throw new Exception("Logical database " + logicalDatabaseName + " not found.");
                    }

                    if (ldb.HydraMode == HydraModeType.Spray)
                    {
                        Int16 partitionCount = ldb.PartitionCount;

                        for (Int16 partitionNum = 0; partitionNum < partitionCount; partitionNum++)
                        {
                            Partition partition = ldb[partitionNum];

                            Int32 pdCount = partition.PhysicalDatabases.Count;
                            for (Int32 pdNum = 0; pdNum < pdCount; pdNum++)
                            {
                                PhysicalDatabase pd = partition.PhysicalDatabases[pdNum];
                                string key = HydraWriterSpray.GetKey(pd.ServerName, pd.PhysicalDatabaseName);

                                HydraWriterSpray writer = _writers[key] as HydraWriterSpray;

                                if (writer == null)
                                {
                                    writer = new HydraWriterSpray(pd.ServerName, pd.PhysicalDatabaseName);
                                    writer.Start(ldb.HydraThreads);
                                    _writers.Add(writer.GetKey(), writer);
                                }
                                else
                                {
                                    writer.SetThreadCount(ldb.HydraThreads);
                                }
                            }
                        }
                    }
                    else if (ldb.HydraMode == HydraModeType.Transactional)
                    {
                        Int16 partitionCount = ldb.PartitionCount;

                        for (Int16 offset = 0; offset < partitionCount; offset++)
                        {
                            string key = HydraUtility.GetKey(ldb.LogicalDatabaseName, offset);

                            HydraWriterTransactional writer = _writers[key] as HydraWriterTransactional;

                            if (writer == null)
                            {
                                writer = new HydraWriterTransactional(ldb.LogicalDatabaseName, offset);
                                writer.Start(ldb.HydraThreads);
                                _writers.Add(writer.GetKey(), writer);
                            }
                            else
                            {
                                writer.SetThreadCount(ldb.HydraThreads);
                            }
                        }
                    }
                }
            }
            finally
            {
                _lockManagers.ReleaseLock();
            }

            _versionPrevious = ConnectionDispenser.Instance.Version;
        }
    }
}