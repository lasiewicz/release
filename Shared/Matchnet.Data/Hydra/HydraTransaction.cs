using System;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Diagnostics;
using System.Messaging;

using Matchnet.Configuration.ValueObjects.Lpd;


namespace Matchnet.Data.Hydra
{
	/// <summary>
	/// 
	/// </summary>
	[Transaction(TransactionOption.Required)]
	public class HydraTransaction : ServicedComponent
	{
		/// <summary>
		/// 
		/// </summary>
		public HydraTransaction()
		{
		}


		public void Write(PhysicalDatabases physicalDatabases,
			Command command,
			MessageQueueTransaction tran,
			string writerKey,
			out Exception ex)
		{
			Int32 pdbCount = physicalDatabases.Count;
			SqlConnection[] connections = new SqlConnection[pdbCount];
			PhysicalDatabase pdb = null;
			SqlCommand sqlCommand = null;
			
			try
			{
				for (Int32 pdbNum = 0; pdbNum < pdbCount; pdbNum++)
				{
					pdb = physicalDatabases[pdbNum];

					if (pdb.IsActive && !pdb.IsRecovering && !pdb.IsFailed)
					{
						SqlConnection conn = new SqlConnection(pdb.ConnectionString);
						connections[pdbNum] = conn;

						sqlCommand = ClientHelper.CreateSqlCommand(command);
						sqlCommand.Connection = conn;
						conn.Open();
						sqlCommand.ExecuteNonQuery();
					}
					else
					{
						//Console.WriteLine("__send recovery " + pdb.ConnectionString);
						command.ConnectionString = pdb.ConnectionString;
						MessageQueue queue = HydraUtility.GetQueueRecovery(writerKey, pdb.ServerName);
						queue.Send(command, MessageQueueTransactionType.Single);
						queue.Dispose();
					}
				}

				ex = null;
				ContextUtil.SetComplete();
			}
			catch (Exception exLocal)
			{
				ex = new Exception(DataUtility.buildErr(pdb.ConnectionString,
					sqlCommand),
					exLocal);

				ContextUtil.SetAbort();
			}
			finally
			{
				for (Int32 connectionNum = 0; connectionNum < connections.Length; connectionNum++)
				{
					SqlConnection conn = connections[connectionNum];
					if (conn != null)
					{
						if (conn.State == ConnectionState.Open)
						{
							conn.Close();
						}
					}
				}
			}
		}
	}
}
