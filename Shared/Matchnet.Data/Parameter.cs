using System;
using System.Data;
using System.Data.SqlClient;

namespace Matchnet.Data
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Parameter
	{
		private string _name;
		private SqlDbType _dataType;
		private ParameterDirection _parameterDirection;
		private Object _parameterValue;
		private Int32 _size = 0;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="dataType"></param>
		/// <param name="parameterDirection"></param>
		/// <param name="parameterValue"></param>
		public Parameter(string name,
			SqlDbType dataType,
			ParameterDirection parameterDirection,
			Object parameterValue)
		{
			if (parameterValue != null || parameterDirection == ParameterDirection.Output)
			{
				switch (dataType)
				{
					case SqlDbType.Int:
						string val = parameterValue.ToString();

						switch (val)
						{
							case "True":
								parameterValue = 1;
								break;

							case "False":
								parameterValue = 0;
								break;

							default:
								double dbl;
								if (!double.TryParse(parameterValue.ToString(), System.Globalization.NumberStyles.Integer, null, out dbl))
								{
									throw new Exception("Unable to parse integer value from \"" + parameterValue.ToString() + "\" for parameter \"" + name + "\"");
								}
								break;
						}
					break;
				}
			}

			_name = name;
			_dataType = dataType;
			_parameterDirection = parameterDirection;
			_parameterValue = parameterValue;
		}

	    /// <summary>
	    /// 
	    /// </summary>
	    /// <param name="name"></param>
	    /// <param name="dataType"></param>
	    /// <param name="parameterDirection"></param>
	    /// <param name="size"></param>
	    public Parameter(string name,
			SqlDbType dataType,
			ParameterDirection parameterDirection,
			Int32 size)
		{
			_name = name;
			_dataType = dataType;
			_parameterDirection = parameterDirection;
			_size = size;
		}

		
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public SqlDbType DataType
		{
			get
			{
				return _dataType;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public ParameterDirection ParameterDirection
		{
			get
			{
				return _parameterDirection;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Object ParameterValue
		{
			get
			{
				return _parameterValue;
			}
		}


		public Int32 Size
		{
			get
			{
				return _size;
			}
		}

		
		internal void SetParameterValue(object val)
		{
			_parameterValue = val;
		}
	}
}
