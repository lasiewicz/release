using System;
using System.Data;
using System.Data.SqlClient;
using System.Messaging;

using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data.Configuration;

namespace Matchnet.Data
{
	internal class ClientHelper
	{
		public static Partition GetPartition(Command command)
		{
			return GetPartition(command, null);
		}

		public static Partition GetPartition(Command command, LogicalDatabases logicalDatabases)
		{
			LogicalDatabase logicalDatabase;

			if (logicalDatabases == null)
			{
				logicalDatabase = ConnectionDispenser.Instance.GetLogicalDatabase(command.LogicalDatabaseName);
			}
			else
			{
				logicalDatabase = logicalDatabases[command.LogicalDatabaseName];
			}

			Partition partition = logicalDatabase.GetPartition(command.Key);

			if (partition == null)
			{
				throw new Exception("No partition found for logical database \"" + command.LogicalDatabaseName + "\" and key " + command.Key.ToString() + ".");
			}

			return partition;
		}


		public static PhysicalDatabase GetRandomPhysicalDatabase(Command command)
		{
			return GetRandomPhysicalDatabase(command, null);
		}


		public static PhysicalDatabase GetRandomPhysicalDatabase(Command command, LogicalDatabases logicalDatabases)
		{
			Partition partition = GetPartition(command, logicalDatabases);

			PhysicalDatabase physicalDatabase = partition.PhysicalDatabases.GetRandomInstance();

			if (physicalDatabase == null)
			{
				throw new Exception("No active physical databases are available for logical database name \"" + command.LogicalDatabaseName + "\" (key " + command.Key.ToString() + ")");
			}

			return physicalDatabase;
		}


		public static SqlCommand CreateSqlCommand(Command command)
		{
			SqlCommand sqlCommand = new SqlCommand();
			sqlCommand.CommandText = command.StoredProcedureName;
			sqlCommand.CommandType = CommandType.StoredProcedure;
			if (command.DBTimeoutInSecs > Constants.NULL_INT)
			{
				sqlCommand.CommandTimeout = command.DBTimeoutInSecs;
			}

			Int32 parameterCount = command.Parameters.Count;

			for (Int32 parameterNum = 0; parameterNum < parameterCount; parameterNum++)
			{
				Parameter parameter = command.Parameters[parameterNum];

				SqlParameter sqlParameter = new SqlParameter(parameter.Name, parameter.DataType);
				sqlParameter.Direction = parameter.ParameterDirection;
				if (parameter.Size > 0)
				{
					sqlParameter.Size = parameter.Size;
				}
				object val = parameter.ParameterValue;

				//temporary hack for search prefs prob
				if (val != null)
				{
					if (val.ToString().IndexOf("0-2") == 0)
					{
						val = Matchnet.Constants.NULL_INT;
					}
				}

				sqlParameter.Value = val;

				sqlCommand.Parameters.Add(sqlParameter);
			}

			return sqlCommand;
		}


		public static DataSet ExecuteDataSet(Command command)
		{
			return ExecuteDataSet(GetRandomPhysicalDatabase(command), CreateSqlCommand(command));
		}


		public static DataSet ExecuteDataSet(PhysicalDatabase physicalDatabase, Command command)
		{
			return ExecuteDataSet(physicalDatabase, CreateSqlCommand(command));
		}


		public static DataSet ExecuteDataSet(PhysicalDatabase physicalDatabase, SqlCommand sqlCommand)
		{
			physicalDatabase.IsUsed = true;
			var ds = new DataSet();

			using (var connection = new SqlConnection(physicalDatabase.ConnectionString))
			{
				try
				{
					connection.Open();
					sqlCommand.Connection = connection;
					SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
					DateTime beginDate = DateTime.Now;
					da.Fill(ds);
					Double totalSeconds = DateTime.Now.Subtract(beginDate).TotalSeconds;
					if (totalSeconds > .1)
					{
						//System.Diagnostics.Trace.WriteLine("__ExecuteDataSet " + sqlCommand.CommandText + ", " + physicalDatabase.ConnectionString + ", " + totalSeconds.ToString());
					}
					physicalDatabase.ResetFailureCount();
				}
				catch (Exception ex)
				{
					physicalDatabase.IncrementFailureCount(ConnectionDispenser.ProcessName);
					throw new Exception(DataUtility.buildErr(physicalDatabase.ConnectionString,
						sqlCommand), ex);
				}
			}

			return ds;
		}


		public static DataTable ExecuteDataTable(Command command,
			out SqlParameterCollection sqlParameters)
		{
			return ExecuteDataTable(command, out sqlParameters, null);
		}


		public static DataTable ExecuteDataTable(Command command,
			out SqlParameterCollection sqlParameters,
			LogicalDatabases logicalDatabases)
		{
			SqlCommand sqlCommand = CreateSqlCommand(command);
			sqlParameters = sqlCommand.Parameters;
			return ExecuteDataTable(GetRandomPhysicalDatabase(command, logicalDatabases),
				sqlCommand);
		}


		public static DataTable ExecuteDataTable(PhysicalDatabase physicalDatabase, Command command)
		{
			return ExecuteDataTable(physicalDatabase, CreateSqlCommand(command));

		}


		public static DataTable ExecuteDataTable(PhysicalDatabase physicalDatabase,
			SqlCommand sqlCommand)
		{
			physicalDatabase.IsUsed = true;
			SqlConnection conn = new SqlConnection(physicalDatabase.ConnectionString);
			sqlCommand.Connection = conn;
			DataTable dt;

			try
			{
				dt = ExecuteDataTable(physicalDatabase.ConnectionString, sqlCommand);
				physicalDatabase.ResetFailureCount();
			}
			catch (Exception ex)
			{
				physicalDatabase.IncrementFailureCount(ConnectionDispenser.ProcessName);
				throw new Exception(DataUtility.buildErr(physicalDatabase.ConnectionString,
					sqlCommand), ex);
			}

			return dt;
		}


		internal static DataTable ExecuteDataTable(string connectionString, SqlCommand sqlCommand)
		{
			var dt = new DataTable();

			using (var connection = new SqlConnection(connectionString))
			{
				sqlCommand.Connection = connection;
				connection.Open();
				SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
				da.Fill(dt);
			}

			return dt;
		}


		public static SqlParameterCollection ExecuteNonQuery(Command command)
		{
			PhysicalDatabase physicalDatabase = null;

			if (command.ConnectionString != null)
			{
				physicalDatabase = ConnectionDispenser.Instance.GetLogicalDatabase(command.LogicalDatabaseName.ToLower()).GetPartition(command.Key).PhysicalDatabases.GetDatabaseByConnectionString(command.ConnectionString.ToLower());
				
				if (physicalDatabase == null)
				{System.Diagnostics.Trace.WriteLine("ExecuteNonQuery connection string != null - physicalDatabase == null, version:" + ConnectionDispenser.Instance.Version );
					throw new Exception("No active physical databases found (logicalDatabaseName: " + command.LogicalDatabaseName + ", connectionString: " + command.ConnectionString + ").");
				}
			}
			else
			{
				physicalDatabase = ConnectionDispenser.Instance.GetLogicalDatabase(command.LogicalDatabaseName).GetPartition(command.Key).PhysicalDatabases.GetRandomInstance();
				if (physicalDatabase == null)
				{//System.Diagnostics.Trace.WriteLine("ExecuteNonQuery connection string == null - physicalDatabase == null" );
					throw new Exception("No active physical databases found (logicalDatabaseName: " + command.LogicalDatabaseName + ", key: " + command.Key.ToString() + ").");
				}
			}

			SqlCommand sqlCommand = CreateSqlCommand(command);
			ExecuteNonQuery(physicalDatabase, sqlCommand);
			return sqlCommand.Parameters;
		}

	    public static void ExecuteNonQuery(PhysicalDatabase physicalDatabase, SqlCommand sqlCommand)
	    {
	        physicalDatabase.IsUsed = true;

	        using (var connection = new SqlConnection(physicalDatabase.ConnectionString))
	        {
	            try
	            {
	                connection.Open();
	                sqlCommand.Connection = connection;
	                sqlCommand.ExecuteNonQuery();
	                physicalDatabase.ResetFailureCount();
	            }
	            catch (Exception ex)
	            {
	                physicalDatabase.IncrementFailureCount(ConnectionDispenser.ProcessName);
	                throw new Exception(DataUtility.buildErr(physicalDatabase.ConnectionString,
	                    sqlCommand), ex);
	            }
	        }
	    }

		public static void ExecuteNonQuery(string connectionString, SqlCommand sqlCommand)
		{
			using (var connection = new SqlConnection(connectionString))
			{
				sqlCommand.Connection = connection;

				try
				{
					connection.Open();
					sqlCommand.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					throw new Exception(DataUtility.buildErr(connectionString, sqlCommand), ex);
				}
			}
		}
		
		public static SqlDataReader ExecuteReader(Command command)
		{
			SqlParameterCollection sqlParameters;
			return ExecuteReader(command, out sqlParameters);
		}

		public static SqlDataReader ExecuteReader(Command command, out SqlParameterCollection sqlParamters)
		{
			SqlCommand sqlCommand = CreateSqlCommand(command);
			sqlParamters = sqlCommand.Parameters;
			PhysicalDatabase physicalDatabase = GetRandomPhysicalDatabase(command);
			physicalDatabase.IsUsed = true;
			SqlConnection conn = new SqlConnection(physicalDatabase.ConnectionString);
			sqlCommand.Connection = conn;
			SqlDataReader dataReader = null;

			try
			{
				conn.Open();
				dataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
				physicalDatabase.ResetFailureCount();

				for (Int32 parameterNum = 0; parameterNum < command.Parameters.Count; parameterNum++)
				{
					Parameter parameter = command.Parameters[parameterNum];
					if (parameter.ParameterDirection == ParameterDirection.InputOutput || parameter.ParameterDirection == ParameterDirection.Output)
					{
						parameter.SetParameterValue(sqlCommand.Parameters[parameter.Name].Value);
					}
				}
			}
			catch (Exception ex)
			{
				physicalDatabase.IncrementFailureCount(ConnectionDispenser.ProcessName);
				throw new Exception(DataUtility.buildErr(physicalDatabase.ConnectionString,
					sqlCommand), ex);
			}

			return dataReader;
		}

		public static SqlDataReader ExecuteReader(string connectionString, Command command, out SqlParameterCollection sqlParamters)
		{
			SqlCommand sqlCommand = CreateSqlCommand(command);
			sqlParamters = sqlCommand.Parameters;
			SqlConnection conn = new SqlConnection(connectionString);
			sqlCommand.Connection = conn;
			SqlDataReader dataReader = null;

			try
			{
				conn.Open();
				dataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

				for (Int32 parameterNum = 0; parameterNum < command.Parameters.Count; parameterNum++)
				{
					Parameter parameter = command.Parameters[parameterNum];
					if (parameter.ParameterDirection == ParameterDirection.InputOutput || parameter.ParameterDirection == ParameterDirection.Output)
					{
						parameter.SetParameterValue(sqlCommand.Parameters[parameter.Name].Value);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(DataUtility.buildErr(connectionString, sqlCommand), ex);
			}

			return dataReader;
		}
		
		public static MessageQueue GetQueue(string queuePath)
		{
			if (!MessageQueue.Exists(queuePath))
			{
				return MessageQueue.Create(queuePath, true);
			}
			else
			{
				return new MessageQueue(queuePath);
			}
		}
		
	}
}
