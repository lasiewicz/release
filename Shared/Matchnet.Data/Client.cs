using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Threading;
using System.Messaging;

using Matchnet.Data.Hydra;
using Matchnet.Data.Configuration;

using Matchnet.Configuration.ValueObjects.Lpd;

namespace Matchnet.Data
{
	/// <summary>
	/// 
	/// </summary>
	public class Client
	{
		private Hashtable _queues;
		private ReaderWriterLock _queueLock;

		/// <summary>
		/// 
		/// </summary>
		public static readonly Client Instance = new Client();

		private Client()
		{
			_queues = new Hashtable();
			_queueLock = new ReaderWriterLock();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		public DataSet ExecuteDataSet(Command command)
		{
			return ClientHelper.ExecuteDataSet(command);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		public DataTable ExecuteDataTable(Command command)
		{
			return ExecuteDataTable(command, null);
		}


        public DataTable ExecuteDataTable(string sqlConnectionString, Command command)
        {
            SqlCommand sqlCommand = ClientHelper.CreateSqlCommand(command);
            return ClientHelper.ExecuteDataTable(sqlConnectionString, sqlCommand);
        }

	    /// <summary>
	    /// 
	    /// </summary>
	    /// <param name="command"></param>
	    /// <param name="logicalDatabases"></param>
	    /// <returns></returns>
	    public DataTable ExecuteDataTable(Command command, LogicalDatabases logicalDatabases)
		{
			SqlParameterCollection sqlParameters;
			return ExecuteDataTable(command,
				out sqlParameters,
				logicalDatabases);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="command"></param>
		/// <param name="sqlParameters"></param>
		/// <returns></returns>
		public DataTable ExecuteDataTable(Command command,
			out SqlParameterCollection sqlParameters)
		{
			return ExecuteDataTable(command, out sqlParameters, null);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="command"></param>
		/// <param name="sqlParameters"></param>
		/// <param name="logicalDatabases"></param>
		/// <returns></returns>
		public DataTable ExecuteDataTable(Command command,
			out SqlParameterCollection sqlParameters,
			LogicalDatabases logicalDatabases)
		{
			return ClientHelper.ExecuteDataTable(command,
				out sqlParameters,
				logicalDatabases);
		}

		
		/// <summary>
		/// Whatever command you send though this only executes on 1 random physical DB, so don't use this for DB writes
		/// unless that's what you desire.
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		public SqlParameterCollection ExecuteNonQuery(Command command)
		{
			return ClientHelper.ExecuteNonQuery(command);
		}


		public SqlDataReader ExecuteReader(Command command)
		{
			return ClientHelper.ExecuteReader(command);
		}


		public SqlDataReader ExecuteReader(Command command, out SqlParameterCollection sqlParameters)
		{
			return ClientHelper.ExecuteReader(command, out sqlParameters);
		}

        public SqlDataReader ExecuteReader(string connectionString, Command command, out SqlParameterCollection sqlParameters)
        {
            return ClientHelper.ExecuteReader(connectionString, command, out sqlParameters);
        }

        public void ExecuteAsyncWrite(string sqlConnectionString, Command command)
		{
            SqlCommand sqlCommand = ClientHelper.CreateSqlCommand(command);
            Thread t = new Thread(delegate () {
                               ClientHelper.ExecuteNonQuery(sqlConnectionString, sqlCommand);
                           });
            t.Start();
		}



		public void ExecuteAsyncWrite(Command command)
		{
			LogicalDatabase logicalDatabase = ConnectionDispenser.Instance.GetLogicalDatabase(command.LogicalDatabaseName);
			string queuePath = null;
			MessageQueueTransaction tran = new MessageQueueTransaction();

			if (logicalDatabase == null)
			{
				throw new Exception("Logical database \"" + command.LogicalDatabaseName + "\" not found");
			}

			if (logicalDatabase.HydraMode == HydraModeType.Transactional)
			{
				Partition partition = logicalDatabase.GetPartition(command.Key);
				string key = HydraUtility.GetKey(command.LogicalDatabaseName, partition.Offset);

				_queueLock.AcquireReaderLock(-1);
				try
				{
					queuePath = _queues[key] as string;

					if (queuePath == null)
					{
						LockCookie cookie = _queueLock.UpgradeToWriterLock(-1);
						queuePath = _queues[key] as string;

						if (queuePath == null)
						{
							queuePath = HydraUtility.GetQueue(key).Path;
							_queues.Add(key, queuePath);
						}

						_queueLock.DowngradeFromWriterLock(ref cookie);
					}
				}
				finally
				{
					_queueLock.ReleaseLock();
				}

				MessageQueue queue = new MessageQueue(queuePath);
				queue.Formatter = new BinaryMessageFormatter();
				tran.Begin();
				queue.Send(command, tran);
				tran.Commit();
				queue.Dispose();
			}
			else
			{
				Partition partition = logicalDatabase.GetPartition(command.Key);
				Int32 physicalCount = partition.PhysicalDatabases.Count;

				tran.Begin();
				try
				{
					for (Int32 physicalNum = 0; physicalNum < physicalCount; physicalNum++)
					{
						PhysicalDatabase physicalDatabase = partition.PhysicalDatabases[physicalNum];
						string key = HydraWriterSpray.GetKey(physicalDatabase.ServerName, physicalDatabase.PhysicalDatabaseName);

						_queueLock.AcquireReaderLock(-1);
						try
						{
							queuePath = _queues[key] as string;

							if (queuePath == null)
							{
								LockCookie cookie = _queueLock.UpgradeToWriterLock(-1);
								queuePath = _queues[key] as string;

								if (queuePath == null)
								{
									queuePath = HydraUtility.GetQueue(key).Path;
									_queues.Add(key, queuePath);
								}

								_queueLock.DowngradeFromWriterLock(ref cookie);
							}
						}
						finally
						{
							_queueLock.ReleaseLock();
						}

						command.ConnectionString = physicalDatabase.ConnectionString;
					    var queue = new MessageQueue(queuePath) {Formatter = new BinaryMessageFormatter()};
					    queue.Send(command, tran);
						queue.Dispose();
					}

					tran.Commit();
				}
				catch (Exception)
				{
					tran.Abort();
					throw;
				}
			}
		}
	}	
}
