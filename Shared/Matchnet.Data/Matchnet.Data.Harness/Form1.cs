#region

using System;
using System.ComponentModel;
using System.Data;
using System.Threading;
using System.Windows.Forms;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;

#endregion

namespace Matchnet.Data.Harness
{
    public class Form1 : Form
    {
        private readonly Container _components = null;
        private bool _running;
        private HydraWriter _writer;
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Thread[] t;

        public Form1()
        {
            InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_components != null)
                {
                    _components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._button1 = new System.Windows.Forms.Button();
            this._button2 = new System.Windows.Forms.Button();
            this._button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this._button1.Location = new System.Drawing.Point(8, 8);
            this._button1.Name = "_button1";
            this._button1.Size = new System.Drawing.Size(120, 48);
            this._button1.TabIndex = 0;
            this._button1.Text = "button1";
            this._button1.Click += new System.EventHandler(this.Button1Click);
            // 
            // button2
            // 
            this._button2.Location = new System.Drawing.Point(136, 8);
            this._button2.Name = "_button2";
            this._button2.Size = new System.Drawing.Size(120, 48);
            this._button2.TabIndex = 1;
            this._button2.Text = "button2";
            this._button2.Click += new System.EventHandler(this.Button2Click);
            // 
            // button3
            // 
            this._button3.Location = new System.Drawing.Point(8, 64);
            this._button3.Name = "_button3";
            this._button3.Size = new System.Drawing.Size(120, 48);
            this._button3.TabIndex = 2;
            this._button3.Text = "button3";
            this._button3.Click += new System.EventHandler(this.Button3Click);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this._button3);
            this.Controls.Add(this._button2);
            this.Controls.Add(this._button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
        }

        #endregion

        [STAThread]
        private static void Main()
        {
            Application.Run(new Form1());
        }

        private void Button1Click(object sender, EventArgs e)
        {
            Matchnet.Configuration.ValueObjects.Lpd.LogicalDatabase mnSystem =
                ConnectionDispenser.Instance.GetLogicalDatabase("mnIMailNew");

            if (_writer == null)
            {
                _writer = new HydraWriter("mnMember");
            }

            if (!_running)
            {
                _button1.Enabled = false;
                _writer.Start();
                _running = true;
                t = new Thread[1];
                for (var i = 0; i < t.Length; i++)
                {
                    t[i] = new Thread(WorkCycle);
                    t[i].Start();
                }
                _button1.Enabled = true;
            }
            else
            {
                _button1.Enabled = false;
                _writer.Stop();
                _running = false;
                foreach (Thread t1 in t)
                {
                    t1.Join();
                }
                _button1.Enabled = true;
            }

            /*
			Command command = new Command("mnAlert", "up_MatchMail_Save", 0);
			command.AddParameter("@MatchMailID", SqlDbType.Int, ParameterDirection.Input, 3698);
			command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, 101);
			command.AddParameter("@SentFlag", SqlDbType.Bit, ParameterDirection.Input, 0);
			command.AddParameter("@LastSentDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
			command.AddParameter("@SentMemberIDList", SqlDbType.VarChar, ParameterDirection.Input, "");

			try
			{
				Client.Instance.ExecuteNonQuery(command);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.ToString());
			}
			*/
        }

        private void WorkCycle()
        {
            while (_running)
            {
                Command command = new Command("mnMember", "dbo.up_Member_Save", 16000206);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, 16000206);
                command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);

                //SyncWriter sw = new SyncWriter();
                //Exception ex;
                //sw.Execute(command,
                //    out ex);

                Client.Instance.ExecuteAsyncWrite(command);

                Thread.Sleep(1000);

                System.Diagnostics.Trace.WriteLine("");
            }
        }

        private void Button2Click(object sender, EventArgs e)
        {
            Command command = new Command("mnList", "up_YNMList_List_Service", 16000206);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, 16000206);
            command.AddParameter("@DomainIDs", SqlDbType.VarChar, ParameterDirection.Input, "1,12");

            try
            {
                Client.Instance.ExecuteDataTable(command);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.ToString());
            }
        }

        private void Button3Click(object sender, EventArgs e)
        {
            Command command = new Command("mnAlert", "up_MatchMail_Save", 0);
            command.AddParameter("@MatchMailID", SqlDbType.Int, ParameterDirection.Input, 3698);
            command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, 101);
            command.AddParameter("@SentFlag", SqlDbType.Bit, ParameterDirection.Input, 0);
            command.AddParameter("@LastSentDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            command.AddParameter("@SentMemberIDList", SqlDbType.VarChar, ParameterDirection.Input, "");
            command.AddParameter("@SentMemberIDList", SqlDbType.VarChar, ParameterDirection.Input, "");
            command.AddParameter("@SentMemberIDList", SqlDbType.VarChar, ParameterDirection.Input, "");

            /*
			try
			{
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.ToString());
			}
			*/
        }
    }
}