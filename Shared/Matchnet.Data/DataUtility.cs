using System;
using System.Data;
using System.Data.SqlClient;

namespace Matchnet.Data
{
	/// <summary>
	/// A Utility class for shared data related functionality.
	/// </summary>
	public class DataUtility
	{
		/// <summary>
		/// constructor
		/// </summary>
		public DataUtility()
		{
		}

		#region Static Methods
		/// <summary>
		/// Builds a string containing the connection string, command, and its parameters to be used for logging.
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="sqlCommand"></param>
		/// <returns></returns>
		public static string buildErr(string connectionString,
			SqlCommand sqlCommand)
		{
			string err = "Connection string: ";

			if (connectionString != null)
			{
				err = err + connectionString + "\r\n";
			}
			else
			{
				err = err + "(unknown)\r\n";
			}

			err = err + "Command: ";

			if (sqlCommand != null)
			{
				err = err + "Command: " + sqlCommand.CommandText + "\r\n";

				for (Int32 parameterNum = 0; parameterNum < sqlCommand.Parameters.Count; parameterNum++)
				{
					SqlParameter parameter = sqlCommand.Parameters[parameterNum];
					if (parameter.Direction == ParameterDirection.Input)
					{
						err = err + "\t" + parameter.ParameterName + ": ";
						if (parameter.Value != null)
						{
							err = err + parameter.Value.ToString() + "\r\n";
						}
						else
						{
							err = err + "(null)\r\n";
						}
					}
				}
				err = err + "\r\n";
			}
			else
			{
				err = err + "(null)\r\n";
			}

			return err;
		}

		/// <summary>
		/// Builds a string containing the connection string, command, and its parameters to be used for logging.
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="sqlCommand"></param>
		/// <returns></returns>
		public static string buildErr(string connectionString,
			Matchnet.Data.Command sqlCommand)
		{
			string err = "Connection string: ";

			if (connectionString != null)
			{
				err = err + connectionString + "\r\n";
			}
			else
			{
				err = err + "(unknown)\r\n";
			}

			err = err + "Command: ";

			if (sqlCommand != null)
			{
				err = err + "Command: " + sqlCommand.StoredProcedureName + "\r\n";

				for (Int32 parameterNum = 0; parameterNum < sqlCommand.Parameters.Count; parameterNum++)
				{
					Matchnet.Data.Parameter parameter = sqlCommand.Parameters[parameterNum];
					if (parameter.ParameterDirection == ParameterDirection.Input)
					{
						err = err + "\t" + parameter.Name + ": ";
						if (parameter.ParameterValue != null)
						{
							err = err + parameter.ParameterValue.ToString() + "\r\n";
						}
						else
						{
							err = err + "(null)\r\n";
						}
					}
				}
				err = err + "\r\n";
			}
			else
			{
				err = err + "(null)\r\n";
			}

			return err;
		}
		#endregion
	}
}
