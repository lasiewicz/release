using System;
using System.Runtime.Serialization;

using Matchnet.Exceptions;

namespace Matchnet.Data.Exceptions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class StoredProcedureException : ExceptionBase, ISerializable
	{
		/// <summary>
		/// 
		/// </summary>
		private StoredProcedureException()
		{
			// Hide default consructor			
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message">The message describing the exception.</param>
		public StoredProcedureException(string message) : this(message, null)
		{

		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message">The message describing the exception.</param>
		/// <param name="innerException">The inner exception that this exception will wrap (for the purpose of "exception chaining".</param>
		public StoredProcedureException(string message, Exception innerException) : base(message, innerException, null)
		{

		}
		
		/// <summary>
		/// This protected constructor is used for deserialization.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected StoredProcedureException(SerializationInfo info, StreamingContext context) : base( info, context )
		{

		}
	}
}
