using System;
using System.Diagnostics;
using System.Threading;

using Matchnet.TypeInterfaces;


namespace Matchnet.SABackgroundWorker
{
	internal class WorkerInternal
	{
		private IBackgroundSA _backgroundSA;
		private Thread _t;
		private bool _runnable = true;
		private const Int32 SLEEP_INTERVAL = 1000;


		public WorkerInternal(IBackgroundSA backgroundSA)
		{
			_backgroundSA = backgroundSA;
		}


		public void Start()
		{
			_t = new Thread(new ThreadStart(workCycle));
			_t.Start();
		}


		public void SignalStop()
		{
			_runnable = false;
		}


		public void Stop()
		{
			_t.Join();
		}


		private void workCycle()
		{
			Int32 counter = 0;

			while (_runnable)
			{
				Thread.Sleep(SLEEP_INTERVAL);
				counter++;

				if (counter >= _backgroundSA.GetInterval())
				{
					try
					{
						_backgroundSA.DoBackgroundWork();
					}
					catch (Exception ex)
					{
						EventLog.WriteEntry(Matchnet.Constants.EVENT_SRC_WWW,
							"SABackgroundWorker (" + _backgroundSA.GetType().Name + ") error: " + ex.ToString(),
							EventLogEntryType.Error);
					}

					counter = 0;
				}
			}
		}
	}
}
