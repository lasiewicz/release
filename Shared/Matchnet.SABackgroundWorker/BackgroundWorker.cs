using System;
using System.Collections;
using System.Threading;

using Matchnet.TypeInterfaces;


namespace Matchnet.SABackgroundWorker
{
	public class BackgroundWorker
	{
		public static readonly BackgroundWorker Instance = new BackgroundWorker();
		private Hashtable _workerList;
		private ReaderWriterLock _workerListLock;

		private BackgroundWorker()
		{
			_workerList = new Hashtable();
			_workerListLock = new ReaderWriterLock();
		}


		public void RegisterSA(IBackgroundSA backgroundSA)
		{
			string typeName = backgroundSA.GetType().ToString();

			_workerListLock.AcquireReaderLock(-1);
			try
			{
				if (_workerList.ContainsKey(typeName))
				{
					throw new Exception("IBackgroundSA " + typeName + " has already been registered.");
				}
				else
				{
					_workerListLock.UpgradeToWriterLock(-1);
					WorkerInternal workerInternal = new WorkerInternal(backgroundSA);
					_workerList.Add(typeName, workerInternal);
					workerInternal.Start();
				}
			}
			finally
			{
				_workerListLock.ReleaseLock();
			}
		}


		public void Stop()
		{
			_workerListLock.AcquireWriterLock(-1);
			try
			{
				IDictionaryEnumerator de = _workerList.GetEnumerator();
				while (de.MoveNext())
				{
					WorkerInternal workerInternal = de.Value as WorkerInternal;
					workerInternal.SignalStop();
				}

				de.Reset();
				while (de.MoveNext())
				{
					WorkerInternal workerInternal = de.Value as WorkerInternal;
					workerInternal.Stop();
				}
			}
			finally
			{
				_workerListLock.ReleaseLock();
			}
		}
	}
}
