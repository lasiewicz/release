using System;
using System.Text;
using System.Net.Sockets;

namespace Matchnet.RemotingServices
{

	/// <summary>
	/// State object for reading client data asynchronously 
	/// </summary>
	public sealed class SocketState 
	{

		/// <summary>
		/// Intializes the Matchnet.Services.SocketState class
		/// </summary>
		public SocketState()
		{
			
		}

		/// <summary>
		/// Client socket. 
		/// </summary>
		public Socket WorkSocket = null;

		/// <summary>
		/// Size of receive buffer. 
		/// </summary>
		public const int BufferSize = 1024;
		
		/// <summary>
		/// Receive buffer. 
		/// </summary>
		public byte[] buffer = new byte[BufferSize];
		
		/// <summary>
		/// Received data string. 
		/// </summary>
		public StringBuilder sb = new StringBuilder();  
	}

}
