using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.IO;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels.Http;

using Matchnet;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.RemotingServices.BusinessLogic;
using Matchnet.RemotingServices.ServiceManagers;
using Matchnet.InitialConfiguration;

namespace Matchnet.RemotingServices
{
	/// <summary>
	/// 
	/// </summary>
	public class RemotingServiceBase : System.ServiceProcess.ServiceBase
	{
		private const Int32 HEALTH_CHECK_PORT_OFFSET = 99;
		private const Int32 CONFIG_PORT_OFFSET = 98;

		private System.ComponentModel.Container components = null;
		private ServiceInstanceConfig _serviceInstanceConfig = null;
		private Matchnet.RemotingServices.Channels.TcpServerChannel _channel = null;
		private int _servicePort = Constants.NULL_INT;
		private int _healthCheckPort = Constants.NULL_INT;
		private int _configurationPort = Constants.NULL_INT;
		private Thread _deployCheckThread;
		private bool _runnable = true;
		private System.DateTime _serviceStartTime = System.DateTime.Now;
		private ServiceManagerCollection _serviceManagers = new ServiceManagerCollection();

		/// <summary>
		/// 
		/// </summary>
		protected int ServicePort
		{
			get{return _servicePort;}
		}


		/// <summary>
		/// 
		/// </summary>
		protected int HealthCheckPort
		{
			get{return _healthCheckPort;}
		}

		
		/// <summary>
		/// 
		/// </summary>
		protected int ConfigurationPort
		{
			get{return _configurationPort;}
		}


		/// <summary>
		/// Gets/sets a value indicating whether the service should pass health checks
		/// </summary>
		protected bool IsEnabled
		{
			get
			{
				return _channel.IsEnabled;
			}
			set
			{
				_channel.IsEnabled = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public ServiceInstanceConfig ServiceInstanceConfig
		{
			get{return _serviceInstanceConfig;}
			set{_serviceInstanceConfig = value;}
		}


		/// <summary>
		/// 
		/// </summary>
		public DateTime ServiceStartTime
		{
			get{return _serviceStartTime;}
		}



		/// <summary>
		/// 
		/// </summary>
		public RemotingServiceBase()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();
		}


		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "ServiceNameGoesHere";
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			IDictionaryEnumerator de = _serviceManagers.GetEnumerator();
			while (de.MoveNext())
			{
				IDisposable disposable = ((ServiceManager)de.Value).O as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}

			base.Dispose(disposing);
		}


		private void autoConfigure()
		{
			// Allow the machine to be overriden in the configuration file
			string machineName = System.Environment.MachineName;
			
			string overrideMachineName = InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");

			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

//#if DEBUG
			//configure to show exceptions to client
			try
			{
				string configFileName = System.Environment.GetEnvironmentVariable("TEMP") + @"\" + Guid.NewGuid().ToString() + ".config";
				StreamWriter sw = new StreamWriter(configFileName);
				sw.Write("<configuration><system.runtime.remoting><customErrors mode=\"off\" /></system.runtime.remoting></configuration>");
				sw.Close();
				System.Runtime.Remoting.RemotingConfiguration.Configure(configFileName);
				File.Delete(configFileName);
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry(this.ServiceName, ex.ToString(), EventLogEntryType.Warning);
			}
//#endif


			// Automatically define the ports for this service
			this._servicePort = ServiceInstanceConfig.Port;
			this._healthCheckPort = _servicePort + HEALTH_CHECK_PORT_OFFSET;
			this._configurationPort = _servicePort + CONFIG_PORT_OFFSET;


			// Automatically manage channel registration
			BinaryServerFormatterSinkProvider serverSink = new BinaryServerFormatterSinkProvider();
			serverSink.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
			_channel = new Matchnet.RemotingServices.Channels.TcpServerChannel("", _servicePort, serverSink);

			try
			{
				ChannelServices.UnregisterChannel(_channel);
			}
			catch
			{
				// Do nothing
			}

			try
			{
				ChannelServices.RegisterChannel(_channel);
			}
			catch(Exception ex)
			{
				ServiceBoundaryException sbEx = new ServiceBoundaryException(this.ServiceName, "Error occurred when attempting to register channel on port " + _servicePort.ToString() + ".  " + ex.ToString(), ex);
			}

			RegisterServiceManager(new Matchnet.RemotingServices.ServiceManagers.CacheManagerSM(this.ServiceName));
			RegisterServiceManager(new Matchnet.RemotingServices.ServiceManagers.HydraManagerSM(this.ServiceName));
			Matchnet.CacheSynchronization.Context.Evaluator.Instance.SetRemotingPort(this.ServicePort);
			RegisterServiceManagers();
			_channel.IsEnabled = true;
		}

		/// <summary>
		/// 
		/// </summary>
		protected virtual void RegisterServiceManagers()
		{
			
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="args"></param>
		protected override void OnStart(string[] args)
		{
			autoConfigure();
			_deployCheckThread = new Thread(new ThreadStart(deployCheckCycle));
			_deployCheckThread.Start();
		}


		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			_channel.IsEnabled = false;

			_runnable = false;
			_deployCheckThread.Join();

			IDictionaryEnumerator de = _serviceManagers.GetEnumerator();
			while (de.MoveNext())
			{
				IBackgroundProcessor backgroundProcessor = ((ServiceManager)de.Value).O as IBackgroundProcessor;
				if (backgroundProcessor != null)
				{
					backgroundProcessor.Stop();
				}
			}

			base.OnStop();
		}
		

		private void deployCheckCycle()
		{
			string fileName = System.AppDomain.CurrentDomain.BaseDirectory + "deploy.txt";

			while (_runnable)
			{
				try
				{
					if (File.Exists(fileName))
					{
						_channel.IsEnabled = false;
					}
					else
					{
						_channel.IsEnabled = true;
					}
					Thread.Sleep(1000);
				}
				catch (Exception ex)
				{
					System.Diagnostics.Trace.WriteLine("__" + ex.ToString());
					Thread.Sleep(5000);
				}
			}
		}

		/// <summary>
		/// Pause this service.
		/// </summary>
		protected override void OnPause()
		{
			_channel.IsEnabled = false;
			base.OnPause ();
		}


		/// <summary>
		/// Continue this Service.
		/// </summary>
		protected override void OnContinue()
		{
			_channel.IsEnabled = true;
			base.OnContinue();
		}


		/// <summary>
		/// PowerEvent for this service.
		/// </summary>
		/// <param name="powerStatus"></param>
		/// <returns></returns>
		protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
		{
			return base.OnPowerEvent(powerStatus);
		}


		/// <summary>
		/// Shutdown this service.
		/// </summary>
		protected override void OnShutdown()
		{
			base.OnShutdown ();
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceManager"></param>
		protected void RegisterServiceManager(MarshalByRefObject serviceManager)
		{
			Type type = serviceManager.GetType();

			try
			{
				System.Runtime.Remoting.RemotingServices.Marshal(serviceManager, type.Name + ".rem");
				_serviceManagers.Add(serviceManager, "tcp://" + System.Environment.MachineName + ":" + _servicePort.ToString() + "/" + type.Name + ".rem");

				IBackgroundProcessor backgroundProcessor = serviceManager as IBackgroundProcessor;
				if (backgroundProcessor != null)
				{
					backgroundProcessor.Start();
				}

			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(this.ServiceName, "System could not register " + type.Name + " as a WellKnownObject (on Administration Port: " + _servicePort.ToString() + ").  Please view inner exception.", ex);
			}
		}		
	}
}
