/*
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Matchnet;
using Matchnet.Exceptions;

namespace Matchnet.RemotingServices
{
	/// <summary>
	/// 
	/// </summary>
	public class ConfigurationListener : IDisposable
	{

		private int _portNumber = 0;

		/// <summary>
		/// Thread signal.
		/// </summary>
		public System.Threading.ManualResetEvent _allDone = new  System.Threading.ManualResetEvent(false);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="portNumber"></param>
		public ConfigurationListener(int portNumber)
		{
			_portNumber = portNumber;
		}



		/// <summary>
		/// 
		/// </summary>
		public void StartListening()
		{
			// Data buffer for incoming data.
			byte[] bytes = new Byte[1024];

			// Establish the local endpoint for the socket.
			IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
			IPAddress ipAddress = System.Net.IPAddress.Any;
			IPEndPoint localEndPoint = new IPEndPoint(ipAddress, _portNumber);

			// Create a TCP/IP socket.
			Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

			// Bind the socket to the local endpoint and listen for incoming connections.
			try 
			{
				listener.Bind(localEndPoint);
				listener.Listen(100);

				// Loop while
				while(true) 
				{
					// Set the event to non-signaled state.
					_allDone.Reset();

					// Start an asynchronous socket to listen for connections.
					listener.BeginAccept(new AsyncCallback(AcceptCallback),	listener);

					// Wait until a connection is made before continuing.
					_allDone.WaitOne();
				}
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException("Internal error occurred when listening on ConfigurationPort.", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ar"></param>
		public void AcceptCallback(IAsyncResult ar) 
		{
			// Signal the main thread to continue.
			_allDone.Set();

			// Get the socket that handles the client request.
			Socket listener = (Socket)ar.AsyncState;
			Socket handler = listener.EndAccept(ar);

			// Create the SocketState object.
			SocketState socketState = new SocketState();
			socketState.WorkSocket = handler;
			handler.BeginReceive(socketState.buffer, 0, SocketState.BufferSize, 0, new AsyncCallback(ReadCallback), socketState);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ar"></param>
		public void ReadCallback(IAsyncResult ar) 
		{
			String content = String.Empty;
        
			// Retrieve the state object and the handler socket from the asynchronous state object.
			SocketState socketState = (SocketState) ar.AsyncState;
			Socket handler = socketState.WorkSocket;

			// Read data from the client socket. 
			int bytesRead = handler.EndReceive(ar);

			if(bytesRead > 0) 
			{
				// There  might be more data, so store the data received so far.
				socketState.sb.Append(Encoding.ASCII.GetString(socketState.buffer,0,bytesRead));

				// Check for end-of-file tag. If it is not there, read more data.
				content = socketState.sb.ToString();
				
				bool showRuntimeSettings = false;

				if (content.IndexOf("\n") > -1) 
				{

					if (content.IndexOf("SHOW_CONFIGURATION_SETTINGS") > -1)
					{
						showRuntimeSettings = true;
					}

					// Send a response to the caller
					Send(handler, AdministrationHelper.GetHtmlDump(showRuntimeSettings).Replace("\n","<BR>"));
				} 
				else 
				{
					// Not all data received. Get more.
					handler.BeginReceive(socketState.buffer, 0, SocketState.BufferSize, 0, new AsyncCallback(ReadCallback), socketState);
				}
			}
		}
    
		/// <summary>
		/// 
		/// </summary>
		/// <param name="handler"></param>
		/// <param name="data"></param>
		private void Send(Socket handler, String data) 
		{
			// Convert the string data to byte data using ASCII encoding.
			byte[] byteData = Encoding.ASCII.GetBytes(data);

			// Begin sending the data to the remote device.
			handler.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), handler);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="asyncResults"></param>
		private void SendCallback(IAsyncResult asyncResults) 
		{
			try 
			{
				// Retrieve the socket from the state object.
				Socket handler = (Socket)asyncResults.AsyncState;

				// Shutdown and close the socket
				handler.Shutdown(SocketShutdown.Both);
				handler.Close();
			} 
			catch
			{

			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public void Dispose()
		{
			try
			{

			}
			catch
			{
				// do nothing here
			}
		}
	}
}
*/