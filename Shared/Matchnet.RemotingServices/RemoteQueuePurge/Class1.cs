using System;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;

namespace RemoteQueuePurge
{
	class Class1
	{
		[STAThread]
		static void Main(string[] args)
		{
			IHydraManagerService svc = (IHydraManagerService)Activator.GetObject(typeof(IHydraManagerService), "tcp://" + args[0] + "/HydraManagerSM.rem");

			for (Int32 i = 1; i < args.Length; i++)
			{
				Console.Write("purging " + args[i] + ": ");
				try
				{
					svc.PurgeQueue(args[i]);
					Console.WriteLine("done");
				}
				catch (Exception ex)
				{
					Console.WriteLine("error - " + ex.ToString());
				}
			}
		}
	}
}
