using System;
using System.Collections;
using System.Messaging;

using Matchnet;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data.Configuration;
using Matchnet.Exceptions;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;
using Matchnet.RemotingServices.ValueObjects;


namespace Matchnet.RemotingServices.ServiceManagers
{
	/// <summary>
	/// 
	/// </summary>
	public class HydraManagerSM : MarshalByRefObject, IServiceManager, IDisposable, IHydraManagerService
	{
		private string _serviceName;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceName"></param>
		public HydraManagerSM(string serviceName)
		{
			_serviceName = serviceName;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}

		
		/// <summary>
		/// 
		/// </summary>
		public void PrePopulateCache()
		{
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="queuePath"></param>
		/// <param name="messageID"></param>
		/// <returns></returns>
		public System.Messaging.Message GetMessage(string queuePath,
			string messageID)
		{
			MessageQueueTransaction tran = null;
			System.Messaging.Message message = null; 
			MessageQueue queue = new MessageQueue(queuePath);

			if (queue.Transactional)
			{
				tran = new MessageQueueTransaction();
				tran.Begin();
			}

			try
			{
				message = queue.ReceiveById(messageID, new TimeSpan(0, 0, 2), tran);

				if (queue.Transactional)
				{
					tran.Commit();
				}
			}
			catch (Exception ex)
			{
				if (queue.Transactional)
				{
					tran.Abort();
				}

				throw ex;
			}

			return message;
		}


		public void PurgeQueue(string queuePath)
		{
			try
			{
				queuePath = @"FormatName:DIRECT=OS:.\private$\" + queuePath;
				MessageQueue queue = new MessageQueue(queuePath);
				queue.Purge();
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(_serviceName, "Error purging queue (queuePath: " + queuePath + ").", ex);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="queuePath"></param>
		/// <param name="messageID"></param>
		public void RequeueMessage(string queuePath,
			string messageID)
		{
			MessageQueueTransaction tran = null;
			System.Messaging.Message message = null; 
			MessageQueue queue = new MessageQueue(queuePath);

			if (queue.Transactional)
			{
				tran = new MessageQueueTransaction();
				tran.Begin();
			}

			try
			{
				message = queue.ReceiveById(messageID, new TimeSpan(0, 0, 30), tran);
				queue.Send(message);
				if (queue.Transactional)
				{
					tran.Commit();
				}
			}
			catch (Exception ex)
			{
				if (queue.Transactional)
				{
					tran.Abort();
				}

				throw ex;
			}
		}


		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public PhysicalDatabaseStatus[] GetPhysicalDatabaseStatus()
		{
			try
			{
				ArrayList list = new ArrayList();

				if (ConnectionDispenser.Instance.LogicalDatabases != null)
				{
					for (Int32 ldbIndex = 0; ldbIndex < ConnectionDispenser.Instance.LogicalDatabases.Count; ldbIndex++)
					{
						LogicalDatabase logicalDatabase = ConnectionDispenser.Instance.LogicalDatabases[ldbIndex];
						bool isRecovering = logicalDatabase.RecoveryMode;
						for (Int16 offset = 0; offset < logicalDatabase.PartitionCount; offset++)
						{
							Partition partition = logicalDatabase[offset];
							for (Int16 pdbIndex = 0; pdbIndex < partition.PhysicalDatabases.Count; pdbIndex++)
							{
								PhysicalDatabase physicalDatbase = partition.PhysicalDatabases[pdbIndex];
								if (physicalDatbase.IsUsed)
								{
									list.Add(new PhysicalDatabaseStatus(physicalDatbase.ServerName,
										physicalDatbase.PhysicalDatabaseName,
										physicalDatbase.IsActive,
										isRecovering,
										physicalDatbase.IsFailed,
										physicalDatbase.Errors));
								}
							}
						}
					}
				}

				return (PhysicalDatabaseStatus[])list.ToArray(typeof(PhysicalDatabaseStatus));
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(_serviceName, "Error retrieving PhysicalDatabaseStatus", ex);
			}
		}


		#region IDisposable Members
		/// <summary>
		/// 
		/// </summary>
		public void Dispose()
		{
		}
		#endregion
	}
}
