using System;

namespace Matchnet.RemotingServices.ServiceManagers
{
	public class ServiceManager
	{
		private string _uri;
		private object _o;

		public ServiceManager(string uri,
			object o)
		{
			_uri = uri;
			_o = o;
		}


		public string Uri
		{
			get
			{
				return _uri;
			}
		}


		public object O
		{
			get
			{
				return _o;
			}
		}
	}
}
