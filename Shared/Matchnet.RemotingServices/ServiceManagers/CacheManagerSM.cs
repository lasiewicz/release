using System;

using Matchnet.Exceptions;
using Matchnet.RemotingServices.BusinessLogic;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;


namespace Matchnet.RemotingServices.ServiceManagers
{
	/// <summary>
	/// 
	/// </summary>
	public class CacheManagerSM : MarshalByRefObject, ICacheManagerService, IServiceManager, IDisposable
	{
		private string _serviceName;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceName"></param>
		public CacheManagerSM(string serviceName)
		{
			_serviceName = serviceName;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}

		
		/// <summary>
		/// 
		/// </summary>
		public void PrePopulateCache()
		{
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		public void Remove(string key)
		{
			try
			{
				CacheManagerBL.Instance.Remove(key);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(_serviceName, "Error removing item from cache. (key: " + key + ")", ex);
			}
		}


		#region IDisposable Members
		/// <summary>
		/// 
		/// </summary>
		public void Dispose()
		{
		}
		#endregion
	}
}
