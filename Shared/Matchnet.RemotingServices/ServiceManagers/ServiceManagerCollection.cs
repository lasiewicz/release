using System;
using System.Collections;

namespace Matchnet.RemotingServices.ServiceManagers
{
	public class ServiceManagerCollection
	{
		public Hashtable _table;

		public ServiceManagerCollection()
		{
			_table = new Hashtable();
		}


		public void Add(object o,
			string uri)
		{
			_table.Add(o.GetType().Name, new ServiceManager(uri, o));
		}


		public IDictionaryEnumerator GetEnumerator()
		{
			return _table.GetEnumerator();
		}
	}
}
