namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.Runtime.Remoting;
	using System.Runtime.Remoting.Channels;

	internal class TcpChunkedReadingStream : TcpReadingStream
	{
		// Methods
		internal TcpChunkedReadingStream(SocketHandler inputStream)
		{
			this._inputStream = null;
			this._bFoundEnd = false;
			this._byteBuffer = new byte[1];
			this._inputStream = inputStream;
			this._bytesLeft = 0;
		}

		public override void Close()
		{
		}

		private int min(int a, int b)
		{
			if (a >= b)
			{
				return b;
			}
			return a;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int num1 = 0;
			while (!this._bFoundEnd && (count > 0))
			{
				if (this._bytesLeft == 0)
				{
					this._bytesLeft = this._inputStream.ReadInt32();
					if (this._bytesLeft == 0)
					{
						this.ReadTrailer();
						this._bFoundEnd = true;
					}
				}
				if (!this._bFoundEnd)
				{
					int num2 = this.min(this._bytesLeft, count);
					int num3 = this._inputStream.Read(buffer, offset, num2);
					if (num3 <= 0)
					{
						throw new RemotingException(CoreChannel.GetResourceString("Remoting_Tcp_ChunkedEncodingError"));
					}
					this._bytesLeft -= num3;
					count -= num3;
					offset += num3;
					num1 += num3;
					if (this._bytesLeft == 0)
					{
						this.ReadTrailer();
					}
				}
			}
			return num1;
		}

		public override int ReadByte()
		{
			int num1 = this.Read(this._byteBuffer, 0, 1);
			if (num1 == 0)
			{
				return -1;
			}
			return this._byteBuffer[0];
		}

		private void ReadTrailer()
		{
			int num1 = this._inputStream.ReadByte();
			if (num1 != 13)
			{
				throw new RemotingException(CoreChannel.GetResourceString("Remoting_Tcp_ChunkedEncodingError"));
			}
			num1 = this._inputStream.ReadByte();
			if (num1 != 10)
			{
				throw new RemotingException(CoreChannel.GetResourceString("Remoting_Tcp_ChunkedEncodingError"));
			}
		}


		// Properties
		public override bool FoundEnd
		{
			get
			{
				return this._bFoundEnd;
			}
		}


		// Fields
		private bool _bFoundEnd;
		private byte[] _byteBuffer;
		private int _bytesLeft;
		private SocketHandler _inputStream;
	}
}

