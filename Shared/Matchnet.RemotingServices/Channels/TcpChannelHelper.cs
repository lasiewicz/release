namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.Remoting.Channels;

	internal class TcpChannelHelper
	{
		// Methods
		public TcpChannelHelper()
		{
		}

		internal static string ParseURL(string url, out string objectURI)
		{
			int num1;
			objectURI = null;
			if (StringHelper.StartsWithAsciiIgnoreCasePrefixLower(url, "tcp://"))
			{
				num1 = "tcp://".Length;
			}
			else
			{
				return null;
			}
			num1 = url.IndexOf('/', num1);
			if (-1 == num1)
			{
				return url;
			}
			string text1 = url.Substring(0, num1);
			objectURI = url.Substring(num1);
			return text1;
		}

		internal static bool StartsWithTcp(string url)
		{
			return StringHelper.StartsWithAsciiIgnoreCasePrefixLower(url, "tcp://");
		}


		// Fields
		private const string _tcp = "tcp://";
	}
}

