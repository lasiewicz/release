namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.IO;
	using System.Net;
	using System.Net.Sockets;
	using System.Runtime.Remoting;
	using System.Text;
	using System.Threading;

	internal abstract class SocketHandler
	{
		byte[] _healthCheckBuffer;

		// Methods
		private SocketHandler()
		{
			this._byteBuffer = new byte[4];
			_healthCheckBuffer = new byte[41];
			this._controlCookie = 1;
		}

		public SocketHandler(Socket socket)
		{
			this._byteBuffer = new byte[4];
			_healthCheckBuffer = new byte[41];
			this._controlCookie = 1;
			this._beginReadCallback = new AsyncCallback(this.BeginReadMessageCallback);
			this.NetSocket = socket;
			this.NetStream = new SocketStream(this.NetSocket);
			this._dataBuffer = CoreChannel.BufferPool.GetBuffer();
			this._dataBufferSize = this._dataBuffer.Length;
			this._dataOffset = 0;
			this._dataCount = 0;
		}

		internal SocketHandler(Socket socket, RequestQueue requestQueue) : this(socket)
		{
			this._requestQueue = requestQueue;
		}

		public Int32 DataOffset
		{
			set
			{
				_dataOffset = value;
			}
		}

		public void BeginReadMessage()
		{
			bool flag1 = false;
			try
			{
				if (this._requestQueue != null)
				{
					this._requestQueue.ScheduleMoreWorkIfNeeded();
				}
				this.PrepareForNewMessage();
				if (this._dataCount == 0)
				{
					this._beginReadAsyncResult = this.NetStream.BeginRead(this._dataBuffer, 0, this._dataBufferSize, this._beginReadCallback, null);
				}
				else
				{
					flag1 = true;
				}
			}
			catch (Exception exception1)
			{
				this.CloseOnFatalError(exception1);
			}
			if (flag1)
			{
				if (this._requestQueue != null)
				{
					this._requestQueue.ProcessNextRequest(this);
				}
				else
				{
					this.ProcessRequestNow();
				}
				this._beginReadAsyncResult = null;
			}
		}

		public void BeginReadMessageCallback(IAsyncResult ar)
		{
			bool flag1 = false;
			try
			{
				this._beginReadAsyncResult = null;
				this._dataOffset = 0;
				this._dataCount = this.NetStream.EndRead(ar);
				if (this._dataCount <= 0)
				{
					this.Close();
				}
				else
				{
					flag1 = true;
				}
			}
			catch (Exception exception1)
			{
				this.CloseOnFatalError(exception1);
			}
			if (flag1)
			{
				if (this._requestQueue != null)
				{
					this._requestQueue.ProcessNextRequest(this);
				}
				else
				{
					this.ProcessRequestNow();
				}
			}
		}

		private int BufferMoreData()
		{
			int num1 = this.ReadFromSocket(this._dataBuffer, 0, this._dataBufferSize);
			this._dataOffset = 0;
			this._dataCount = num1;
			return num1;
		}

		public virtual void Close()
		{
			if (this._requestQueue != null)
			{
				this._requestQueue.ScheduleMoreWorkIfNeeded();
			}
			if (this.NetStream != null)
			{
				this.NetStream.Close();
				this.NetStream = null;
			}
			if (this.NetSocket != null)
			{
				this.NetSocket.Close();
				this.NetSocket = null;
			}
			if (this._dataBuffer != null)
			{
				CoreChannel.BufferPool.ReturnBuffer(this._dataBuffer);
				this._dataBuffer = null;
			}
		}

		internal void CloseOnFatalError(Exception e)
		{
			try
			{
				this.SendErrorMessageIfPossible(e);
				this.Close();
			}
			catch
			{
				try
				{
					this.Close();
					return;
				}
				catch
				{
					return;
				}
			}
		}

		internal bool CustomErrorsEnabled()
		{
			bool flag1;
			try
			{
				flag1 = RemotingConfiguration.CustomErrorsEnabled(this.IsLocalhost());
			}
			catch (Exception)
			{
				flag1 = true;
			}
			return flag1;
		}

		internal bool IsLocal()
		{
			return IPAddress.IsLoopback(((IPEndPoint) this.NetSocket.RemoteEndPoint).Address);
		}

		internal bool IsLocalhost()
		{
			IPAddress address1 = ((IPEndPoint) this.NetSocket.RemoteEndPoint).Address;
			if (!IPAddress.IsLoopback(address1))
			{
				return CoreChannel.IsLocalIpAddress(address1);
			}
			return true;
		}

		public virtual void OnInputStreamClosed()
		{
		}

		protected abstract void PrepareForNewMessage();

		internal void ProcessRequestNow()
		{
			try
			{
				WaitCallback callback1 = this._dataArrivedCallback;
				if (callback1 != null)
				{
					callback1(this);
				}
			}
			catch (Exception exception1)
			{
				this.CloseOnFatalError(exception1);
			}
		}

		public bool RaceForControl()
		{
			if (1 == Interlocked.Exchange(ref this._controlCookie, 0))
			{
				return true;
			}
			return false;
		}

		public int Read(byte[] buffer, int offset, int count)
		{
			int num1 = 0;
			if (this._dataCount > 0)
			{
				int num2 = Math.Min(this._dataCount, count);
				StreamHelper.BufferCopy(this._dataBuffer, this._dataOffset, buffer, offset, num2);
				this._dataCount -= num2;
				this._dataOffset += num2;
				count -= num2;
				offset += num2;
				num1 += num2;
			}
			while (count > 0)
			{
				if (count < 0x100)
				{
					this.BufferMoreData();
					int num3 = Math.Min(this._dataCount, count);
					StreamHelper.BufferCopy(this._dataBuffer, this._dataOffset, buffer, offset, num3);
					this._dataCount -= num3;
					this._dataOffset += num3;
					count -= num3;
					offset += num3;
					num1 += num3;
					continue;
				}
				int num4 = this.ReadFromSocket(buffer, offset, count);
				count -= num4;
				offset += num4;
				num1 += num4;
			}
			return num1;
		}

		protected bool ReadAndMatchFourBytes(byte[] buffer,
			byte[] buffer2,
			out bool isHealthCheck)
		{
			isHealthCheck = false;

			this.Read(this._byteBuffer, 0, 4);
			if ((this._byteBuffer[0] == buffer[0]
				&& this._byteBuffer[1] == buffer[1]
				&& this._byteBuffer[2] == buffer[2]
				&& this._byteBuffer[3] == buffer[3]))
			{
				return true;
			}
			else
			{
				if (this._byteBuffer[0] == buffer2[0]
					&& this._byteBuffer[1] == buffer2[1]
					&& this._byteBuffer[2] == buffer2[2]
					&& this._byteBuffer[3] == buffer2[3])
				{
					isHealthCheck = true;
				}

				return false;
			}
		}

		public int ReadByte()
		{
			if (this.Read(this._byteBuffer, 0, 1) != -1)
			{
				return this._byteBuffer[0];
			}
			return -1;
		}

		private int ReadFromSocket(byte[] buffer, int offset, int count)
		{
			int num1 = this.NetStream.Read(buffer, offset, count);
			if (num1 <= 0)
			{
				throw new RemotingException(CoreChannel.GetResourceString("Remoting_Socket_UnderlyingSocketClosed"));
			}
			return num1;
		}

		public int ReadInt32()
		{
			this.Read(this._byteBuffer, 0, 4);
			return ((((this._byteBuffer[0] & 0xff) | (this._byteBuffer[1] << 8)) | (this._byteBuffer[2] << 0x10)) | (this._byteBuffer[3] << 0x18));
		}

		protected byte[] ReadToByte(byte b)
		{
			return this.ReadToByte(b, null);
		}

		protected byte[] ReadToByte(byte b, ValidateByteDelegate validator)
		{
			byte[] buffer1 = null;
			if (this._dataCount == 0)
			{
				this.BufferMoreData();
			}
			int num1 = this._dataOffset + this._dataCount;
			int num2 = this._dataOffset;
			int num3 = num2;
			bool flag1 = false;
			while (!flag1)
			{
				bool flag2 = num3 == num1;
				flag1 = !flag2 && (this._dataBuffer[num3] == b);
				if (((validator != null) && !flag2) && (!flag1 && !validator(this._dataBuffer[num3])))
				{
					throw new RemotingException(CoreChannel.GetResourceString("Remoting_Http_InvalidDataReceived"));
				}
				if (flag2 || flag1)
				{
					int num4 = num3 - num2;
					if (buffer1 == null)
					{
						buffer1 = new byte[num4];
						StreamHelper.BufferCopy(this._dataBuffer, num2, buffer1, 0, num4);
					}
					else
					{
						int num5 = buffer1.Length;
						byte[] buffer2 = new byte[num5 + num4];
						StreamHelper.BufferCopy(buffer1, 0, buffer2, 0, num5);
						StreamHelper.BufferCopy(this._dataBuffer, num2, buffer2, num5, num4);
						buffer1 = buffer2;
					}
					this._dataOffset += num4;
					this._dataCount -= num4;
					if (flag2)
					{
						this.BufferMoreData();
						num1 = this._dataOffset + this._dataCount;
						num2 = this._dataOffset;
						num3 = num2;
						continue;
					}
					if (flag1)
					{
						this._dataOffset++;
						this._dataCount--;
					}
					continue;
				}
				num3++;
			}
			return buffer1;
		}

		protected string ReadToChar(char ch)
		{
			return this.ReadToChar(ch, null);
		}

		protected string ReadToChar(char ch, ValidateByteDelegate validator)
		{
			byte[] buffer1 = this.ReadToByte((byte) ch, validator);
			if (buffer1 == null)
			{
				return null;
			}
			if (buffer1.Length == 0)
			{
				return string.Empty;
			}
			return Encoding.ASCII.GetString(buffer1);
		}

		public string ReadToEndOfLine()
		{
			string text1 = this.ReadToChar('\r');
			if (this.ReadByte() == 10)
			{
				return text1;
			}
			return null;
		}

		public ushort ReadUInt16()
		{
			this.Read(this._byteBuffer, 0, 2);
			return (ushort) ((this._byteBuffer[0] & 0xff) | (this._byteBuffer[1] << 8));
		}

		internal void RejectRequestNowSinceServerIsBusy()
		{
			this.CloseOnFatalError(new RemotingException(CoreChannel.GetResourceString("Remoting_ServerIsBusy")));
		}

		public void ReleaseControl()
		{
			this._controlCookie = 1;
		}

		protected virtual void SendErrorMessageIfPossible(Exception e)
		{
		}

		public void WriteByte(byte value, Stream outputStream)
		{
			this._byteBuffer[0] = value;
			outputStream.Write(this._byteBuffer, 0, 1);
		}

		public void WriteInt32(int value, Stream outputStream)
		{
			this._byteBuffer[0] = (byte) value;
			this._byteBuffer[1] = (byte) (value >> 8);
			this._byteBuffer[2] = (byte) (value >> 0x10);
			this._byteBuffer[3] = (byte) (value >> 0x18);
			outputStream.Write(this._byteBuffer, 0, 4);
		}

		public void WriteUInt16(ushort value, Stream outputStream)
		{
			this._byteBuffer[0] = (byte) value;
			this._byteBuffer[1] = (byte) (value >> 8);
			outputStream.Write(this._byteBuffer, 0, 2);
		}


		// Properties
		public WaitCallback DataArrivedCallback
		{
			get
			{
				return this._dataArrivedCallback;
			}
			set
			{
				this._dataArrivedCallback = value;
			}
		}

		public object DataArrivedCallbackState
		{
			get
			{
				return this._dataArrivedCallbackState;
			}
			set
			{
				this._dataArrivedCallbackState = value;
			}
		}


		// Fields
		private IAsyncResult _beginReadAsyncResult;
		private AsyncCallback _beginReadCallback;
		private byte[] _byteBuffer;
		private int _controlCookie;
		private WaitCallback _dataArrivedCallback;
		private object _dataArrivedCallbackState;
		private byte[] _dataBuffer;
		private int _dataBufferSize;
		private int _dataCount;
		private int _dataOffset;
		private RequestQueue _requestQueue;
		protected Socket NetSocket;
		protected Stream NetStream;
	}
}

