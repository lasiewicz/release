namespace Matchnet.RemotingServices.Channels
{
	using System;

	internal enum SinkChannelProtocol
	{
		// Fields
		Http = 0,
		Other = 1
	}
}

