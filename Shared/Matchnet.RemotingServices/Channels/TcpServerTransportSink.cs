using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;

namespace Matchnet.RemotingServices.Channels
{
	internal class TcpServerTransportSink : IServerChannelSink, IChannelSinkBase
	{
		// Methods
		public TcpServerTransportSink(IServerChannelSink nextSink)
		{
			this._nextSink = nextSink;
		}

		public void AsyncProcessResponse(IServerResponseChannelSinkStack sinkStack, object state, IMessage msg, ITransportHeaders headers, Stream stream)
		{
			TcpServerSocketHandler handler1 = null;
			handler1 = (TcpServerSocketHandler) state;
			handler1.SendResponse(headers, stream);
			if (handler1.CanServiceAnotherRequest())
			{
				handler1.BeginReadMessage();
			}
			else
			{
				handler1.Close();
			}
		}

		public Stream GetResponseStream(IServerResponseChannelSinkStack sinkStack, object state, IMessage msg, ITransportHeaders headers)
		{
			return null;
		}

		public ServerProcessing ProcessMessage(IServerChannelSinkStack sinkStack, IMessage requestMsg, ITransportHeaders requestHeaders, Stream requestStream, out IMessage responseMsg, out ITransportHeaders responseHeaders, out Stream responseStream)
		{
			throw new NotSupportedException();
		}

		internal void ServiceRequest(object state)
		{
			IMessage message1;
			ITransportHeaders headers2;
			Stream stream2;
			bool isHealthCheck;
			TcpServerSocketHandler handler1 = (TcpServerSocketHandler) state;
			ITransportHeaders headers1 = handler1.ReadHeaders(out isHealthCheck);

			Stream stream1 = handler1.GetRequestStream();
			headers1["__CustomErrorsEnabled"] = handler1.CustomErrorsEnabled();
			ServerChannelSinkStack stack1 = new ServerChannelSinkStack();
			stack1.Push(this, handler1);
			ServerProcessing processing1 = this._nextSink.ProcessMessage(stack1, null, headers1, stream1, out message1, out headers2, out stream2);

			if (isHealthCheck)
			{
				handler1.SendHealthCheck(headers1, stream2);
				handler1.Close();
				return;
			}

			switch (processing1)
			{
				case ServerProcessing.Complete:
				{
					stack1.Pop(this);
					handler1.SendResponse(headers2, stream2);
					break;
				}
				case ServerProcessing.OneWay:
				{
					handler1.SendResponse(headers2, stream2);
					break;
				}
				case ServerProcessing.Async:
				{
					stack1.StoreAndDispatch(this, handler1);
					break;
				}
			}

			//close here if failed health check?
			if (processing1 != ServerProcessing.Async)
			{
				if (handler1.CanServiceAnotherRequest())
				{
					handler1.BeginReadMessage();
				}
				else
				{
					handler1.Close();
				}
			}
		}


		// Properties
		public IServerChannelSink NextChannelSink
		{
			get
			{
				return this._nextSink;
			}
		}

		public IDictionary Properties
		{
			get
			{
				return null;
			}
		}


		// Fields
		private IServerChannelSink _nextSink;
		private const int s_MaxSize = 0x2000000;
	}
}

