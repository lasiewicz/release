namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.Collections;
	using System.Diagnostics;
	using System.Globalization;
	using System.IO;
	using System.Net;
	using System.Net.Sockets;
	using System.Resources;
	using System.Runtime.Remoting;
	using System.Runtime.Remoting.Channels;
	using System.Runtime.Remoting.Messaging;
	using System.Runtime.Remoting.Metadata;
	using System.Runtime.Serialization;
	using System.Runtime.Serialization.Formatters;
	using System.Runtime.Serialization.Formatters.Binary;
	using System.Text;
	using System.Web;

	internal class CoreChannel
	{
		// Methods
		static CoreChannel()
		{
			CoreChannel._bufferPool = new ByteBufferPool(10, 0x1000);
			CoreChannel._requestQueue = new RequestQueue(8, 4, 250);
			CoreChannel.s_hostName = null;
			CoreChannel.s_MachineName = null;
			CoreChannel.s_MachineIp = null;
			CoreChannel.s_MachineIpAddress = null;
			CoreChannel.s_isEnabled = false;
		}

		public CoreChannel()
		{
		}

		internal static string AppendApplicationNameToChannelUri(string channelUri)
		{
			if (channelUri == null)
			{
				return null;
			}
			if (!channelUri.EndsWith("/"))
			{
				channelUri = channelUri + "/";
			}
			string text1 = RemotingConfiguration.ApplicationName;
			if ((text1 != null) && (text1.Length != 0))
			{
				return (channelUri + text1 + "/");
			}
			return channelUri;
		}

		internal static void AppendProviderToClientProviderChain(IClientChannelSinkProvider providerChain, IClientChannelSinkProvider provider)
		{
			if (providerChain == null)
			{
				throw new ArgumentNullException("providerChain");
			}
			while (providerChain.Next != null)
			{
				providerChain = providerChain.Next;
			}
			providerChain.Next = provider;
		}

		internal static void CleanupUrlBashingForIisSslIfNecessary(bool bBashedUrl)
		{
			if (bBashedUrl)
			{
				CallContext.FreeNamedDataSlot("__bashChannelUrl");
			}
		}

		internal static void CollectChannelDataFromServerSinkProviders(ChannelDataStore channelData, IServerChannelSinkProvider provider)
		{
			while (provider != null)
			{
				provider.GetChannelData(channelData);
				provider = provider.Next;
			}
		}

		internal static BinaryFormatter CreateBinaryFormatter(bool serialize, bool includeVersionsOrStrictBinding)
		{
			BinaryFormatter formatter1 = new BinaryFormatter();
			if (serialize)
			{
				RemotingSurrogateSelector selector1 = new RemotingSurrogateSelector();
				formatter1.SurrogateSelector = selector1;
			}
			else
			{
				formatter1.SurrogateSelector = null;
			}
			formatter1.Context = new StreamingContext(StreamingContextStates.Other);
			formatter1.AssemblyFormat = includeVersionsOrStrictBinding ? FormatterAssemblyStyle.Full : FormatterAssemblyStyle.Simple;
			return formatter1;
		}

		/*
		internal static SoapFormatter CreateSoapFormatter(bool serialize, bool includeVersions)
		{
			SoapFormatter formatter1 = new SoapFormatter();
			if (serialize)
			{
				RemotingSurrogateSelector selector1 = new RemotingSurrogateSelector();
				formatter1.SurrogateSelector = selector1;
				selector1.UseSoapFormat();
			}
			else
			{
				formatter1.SurrogateSelector = null;
			}
			formatter1.Context = new StreamingContext(StreamingContextStates.Other);
			formatter1.AssemblyFormat = includeVersions ? FormatterAssemblyStyle.Full : FormatterAssemblyStyle.Simple;
			return formatter1;
		}
		*/

		[Conditional("_DEBUG")]
		internal static void DebugException(string name, Exception e)
		{
		}

		internal static void DebugInitialize(string s)
		{
		}

		[Conditional("_DEBUG")]
		internal static void DebugMessage(IMessage msg)
		{
		}

		[Conditional("_DEBUG")]
		internal static void DebugOut(string s)
		{
		}

		[Conditional("_DEBUG")]
		internal static void DebugOutXMLStream(Stream stm, string tag)
		{
		}

		[Conditional("_DEBUG")]
		internal static void DebugStream(Stream stm)
		{
		}

		internal static string DecodeMachineName(string machineName)
		{
			if (machineName.Equals("$hostName"))
			{
				return CoreChannel.GetHostName();
			}
			return machineName;
		}

		internal static IMessage DeserializeBinaryRequestMessage(string objectUri, Stream inputStream, bool bStrictBinding, TypeFilterLevel securityLevel)
		{
			BinaryFormatter formatter1 = CoreChannel.CreateBinaryFormatter(false, bStrictBinding);
			formatter1.FilterLevel = securityLevel;
			UriHeaderHandler handler1 = new UriHeaderHandler(objectUri);
			return (IMessage) formatter1.UnsafeDeserialize(inputStream, new HeaderHandler(handler1.HeaderHandler));
		}

		internal static IMessage DeserializeBinaryResponseMessage(Stream inputStream, IMethodCallMessage reqMsg, bool bStrictBinding)
		{
			BinaryFormatter formatter1 = CoreChannel.CreateBinaryFormatter(false, bStrictBinding);
			return (IMessage) formatter1.UnsafeDeserializeMethodResponse(inputStream, null, reqMsg);
		}

		internal static IMessage DeserializeMessage(string mimeType, Stream xstm, bool methodRequest, IMessage msg)
		{
			return CoreChannel.DeserializeMessage(mimeType, xstm, methodRequest, msg, null);
		}

		internal static IMessage DeserializeMessage(string mimeType, Stream xstm, bool methodRequest, IMessage msg, Header[] h)
		{
			object obj1;
			Stream stream1 = null;
			bool flag1 = false;
			bool flag2 = true;
			if (string.Compare(mimeType, "application/octet-stream", false, CultureInfo.InvariantCulture) == 0)
			{
				flag2 = true;
			}
			if (string.Compare(mimeType, "text/xml", false, CultureInfo.InvariantCulture) == 0)
			{
				flag2 = false;
			}
			if (!flag1)
			{
				stream1 = xstm;
			}
			else
			{
				long num1 = xstm.Position;
				MemoryStream stream2 = (MemoryStream) xstm;
				byte[] buffer1 = stream2.ToArray();
				xstm.Position = num1;
				string text1 = Encoding.ASCII.GetString(buffer1, 0, buffer1.Length);
				byte[] buffer2 = Convert.FromBase64String(text1);
				MemoryStream stream3 = new MemoryStream(buffer2);
				stream1 = stream3;
			}
			IRemotingFormatter formatter1 = CoreChannel.MimeTypeToFormatter(mimeType, false);
			if (flag2)
			{
				obj1 = ((BinaryFormatter) formatter1).UnsafeDeserializeMethodResponse(stream1, null, (IMethodCallMessage) msg);
			}
			else if (methodRequest)
			{
				MethodCall call1 = new MethodCall(h);
				formatter1.Deserialize(stream1, new HeaderHandler(call1.HeaderHandler));
				obj1 = call1;
			}
			else
			{
				IMethodCallMessage message1 = (IMethodCallMessage) msg;
				MethodResponse response1 = new MethodResponse(h, message1);
				formatter1.Deserialize(stream1, new HeaderHandler(response1.HeaderHandler));
				obj1 = response1;
			}
			return (IMessage) obj1;
		}

		internal static IMessage DeserializeSoapRequestMessage(Stream inputStream, Header[] h, bool bStrictBinding, TypeFilterLevel securityLevel)
		{
			/*
			SoapFormatter formatter1 = CoreChannel.CreateSoapFormatter(false, bStrictBinding);
			formatter1.FilterLevel = securityLevel;
			MethodCall call1 = new MethodCall(h);
			formatter1.Deserialize(inputStream, new HeaderHandler(call1.HeaderHandler));
			return call1;
			*/
			throw new Exception("not implemented");
		}

		internal static IMessage DeserializeSoapResponseMessage(Stream inputStream, IMessage requestMsg, Header[] h, bool bStrictBinding)
		{
			/*
			SoapFormatter formatter1 = CoreChannel.CreateSoapFormatter(false, bStrictBinding);
			IMethodCallMessage message1 = (IMethodCallMessage) requestMsg;
			MethodResponse response1 = new MethodResponse(h, message1);
			formatter1.Deserialize(inputStream, new HeaderHandler(response1.HeaderHandler));
			return response1;
			*/
			throw new Exception("not implemented");
		}

		internal static SinkChannelProtocol DetermineChannelProtocol(System.Runtime.Remoting.Channels.IChannel channel)
		{
			string text1;
			string text2 = channel.Parse("http://foo.com/foo", out text1);
			if (text2 != null)
			{
				return SinkChannelProtocol.Http;
			}
			return SinkChannelProtocol.Other;
		}

		internal static string GetHostName()
		{
			if (CoreChannel.s_hostName == null)
			{
				CoreChannel.s_hostName = Dns.GetHostName();
				if (CoreChannel.s_hostName == null)
				{
					throw new ArgumentNullException("hostName");
				}
			}
			return CoreChannel.s_hostName;
		}

		internal static IPAddress GetMachineAddress(IPHostEntry host, AddressFamily addressFamily)
		{
			IPAddress address1 = null;
			if (host != null)
			{
				IPAddress[] addressArray1 = host.AddressList;
				for (int num1 = 0; num1 < addressArray1.Length; num1++)
				{
					if (addressArray1[num1].AddressFamily == addressFamily)
					{
						address1 = addressArray1[num1];
						break;
					}
				}
			}
			return address1;
		}

		internal static string GetMachineIp()
		{
			if (CoreChannel.s_MachineIp == null)
			{
				string text1 = CoreChannel.GetMachineName();
				IPHostEntry entry1 = Dns.GetHostByName(text1);
				IPAddress address1 = CoreChannel.GetMachineAddress(entry1, AddressFamily.InterNetwork);
				if (address1 != null)
				{
					CoreChannel.s_MachineIp = address1.ToString();
				}
				if (CoreChannel.s_MachineIp == null)
				{
					throw new ArgumentNullException("ip");
				}
			}
			return CoreChannel.s_MachineIp;
		}

		internal static string GetMachineName()
		{
			if (CoreChannel.s_MachineName == null)
			{
				string text1 = CoreChannel.GetHostName();
				if (text1 != null)
				{
					IPHostEntry entry1 = Dns.GetHostByName(text1);
					if (entry1 != null)
					{
						CoreChannel.s_MachineName = entry1.HostName;
					}
				}
				if (CoreChannel.s_MachineName == null)
				{
					throw new ArgumentNullException("machine");
				}
			}
			return CoreChannel.s_MachineName;
		}

		internal static Header[] GetMessagePropertiesAsSoapHeader(IMessage reqMsg)
		{
			IDictionary dictionary1 = reqMsg.Properties;
			if (dictionary1 == null)
			{
				return null;
			}
			int num1 = dictionary1.Count;
			if (num1 == 0)
			{
				return null;
			}
			IDictionaryEnumerator enumerator1 = dictionary1.GetEnumerator();
			bool[] flagArray1 = new bool[num1];
			int num2 = 0;
			int num3 = 0;
			IMethodMessage message1 = (IMethodMessage) reqMsg;
			while (enumerator1.MoveNext())
			{
				string text1 = (string) enumerator1.Key;
				if (((text1.Length >= 2) && (string.CompareOrdinal(text1, 0, "__", 0, 2) == 0)) && (((((text1.Equals("__Args") || text1.Equals("__OutArgs")) || (text1.Equals("__Return") || text1.Equals("__Uri"))) || (text1.Equals("__MethodName") || ((text1.Equals("__MethodSignature") && !RemotingServices.IsMethodOverloaded(message1)) && !message1.HasVarArgs))) || (text1.Equals("__TypeName") || text1.Equals("__Fault"))) || (text1.Equals("__CallContext") && ((enumerator1.Value != null) ? !((LogicalCallContext) enumerator1.Value).HasInfo : true))))
				{
					num3++;
					continue;
				}
				flagArray1[num3] = true;
				num3++;
				num2++;
			}
			if (num2 == 0)
			{
				return null;
			}
			Header[] headerArray1 = new Header[num2];
			enumerator1.Reset();
			int num4 = 0;
			num3 = 0;
			while (enumerator1.MoveNext())
			{
				object obj1 = enumerator1.Key;
				if (!flagArray1[num4])
				{
					num4++;
					continue;
				}
				Header header1 = enumerator1.Value as Header;
				if (header1 == null)
				{
					header1 = new Header((string) obj1, enumerator1.Value, false, "http://schemas.microsoft.com/clr/soap/messageProperties");
				}
				if (num3 == headerArray1.Length)
				{
					Header[] headerArray2 = new Header[num3 + 1];
					Array.Copy(headerArray1, headerArray2, num3);
					headerArray1 = headerArray2;
				}
				headerArray1[num3] = header1;
				num3++;
				num4++;
			}
			return headerArray1;
		}

		internal static string GetObjectUriFromRequestUri(string uri, System.Runtime.Remoting.Channels.IChannel receiver)
		{
			string text1;
			receiver.Parse(uri, out text1);
			if (text1 == null)
			{
				text1 = uri;
			}
			return CoreChannel.RemoveApplicationNameFromUri(text1);
		}

		internal static string GetResourceString(string key)
		{
			if (CoreChannel.SystemResMgr == null)
			{
				CoreChannel.InitResourceManager();
			}
			return CoreChannel.SystemResMgr.GetString(key, null);
		}

		internal static Header[] GetSoapHeaders(IMessage reqMsg)
		{
			return CoreChannel.GetMessagePropertiesAsSoapHeader(reqMsg);
		}

		private static ResourceManager InitResourceManager()
		{
			if (CoreChannel.SystemResMgr == null)
			{
				CoreChannel.SystemResMgr = new ResourceManager("System.Runtime.Remoting", typeof(System.Runtime.Remoting.Channels.Tcp.TcpChannel).Module.Assembly);
			}
			return CoreChannel.SystemResMgr;
		}

		internal static bool IsLocalIpAddress(IPAddress remoteAddress)
		{
			if (CoreChannel.s_MachineIpAddress == null)
			{
				string text1 = CoreChannel.GetMachineName();
				IPHostEntry entry1 = Dns.GetHostByName(text1);
				if ((entry1 != null) && (entry1.AddressList.Length == 1))
				{
					CoreChannel.s_MachineIpAddress = CoreChannel.GetMachineAddress(entry1, AddressFamily.InterNetwork);
				}
				else
				{
					return CoreChannel.IsLocalIpAddress(entry1, AddressFamily.InterNetwork, remoteAddress);
				}
			}
			return CoreChannel.s_MachineIpAddress.Equals(remoteAddress);
		}

		internal static bool IsLocalIpAddress(IPHostEntry host, AddressFamily addressFamily, IPAddress remoteAddress)
		{
			if (host != null)
			{
				IPAddress[] addressArray1 = host.AddressList;
				for (int num1 = 0; num1 < addressArray1.Length; num1++)
				{
					if ((addressArray1[num1].AddressFamily == addressFamily) && addressArray1[num1].Equals(remoteAddress))
					{
						return true;
					}
				}
			}
			return false;
		}

		internal static IRemotingFormatter MimeTypeToFormatter(string mimeType, bool serialize)
		{
			/*
			if (string.Compare(mimeType, "text/xml", false, CultureInfo.InvariantCulture) == 0)
			{
				return CoreChannel.CreateSoapFormatter(serialize, true);
			}
			if (string.Compare(mimeType, "application/octet-stream", false, CultureInfo.InvariantCulture) == 0)
			{
				return CoreChannel.CreateBinaryFormatter(serialize, true);
			}
			return null;
			*/
			throw new Exception("not implemented");
		}

		internal static string PrependApplicationNameToObjectUri(string objectUri)
		{
			if (objectUri == null)
			{
				return null;
			}
			if (objectUri.StartsWith("/"))
			{
				objectUri = objectUri.Substring(1);
			}
			string text1 = RemotingConfiguration.ApplicationName;
			if ((text1 != null) && (text1.Length != 0))
			{
				return (text1 + "/" + objectUri);
			}
			return objectUri;
		}

		internal static string RemoveApplicationNameFromUri(string uri)
		{
			if (uri == null)
			{
				return null;
			}
			string text1 = RemotingConfiguration.ApplicationName;
			if (((text1 != null) && (text1.Length != 0)) && ((uri.Length >= (text1.Length + 2)) && ((string.Compare(text1, 0, uri, 0, text1.Length, true, CultureInfo.InvariantCulture) == 0) && (uri[text1.Length] == '/'))))
			{
				uri = uri.Substring(text1.Length + 1);
			}
			return uri;
		}

		internal static void ReportUnknownProviderConfigProperty(string providerTypeName, string propertyName)
		{
			throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Providers_Config_UnknownProperty"), providerTypeName, propertyName));
		}

		internal static Stream SerializeBinaryMessage(IMessage msg, bool includeVersions)
		{
			MemoryStream stream1 = new MemoryStream();
			CoreChannel.SerializeBinaryMessage(msg, stream1, includeVersions);
			stream1.Position = 0;
			return stream1;
		}

		internal static void SerializeBinaryMessage(IMessage msg, Stream outputStream, bool includeVersions)
		{
			BinaryFormatter formatter1 = CoreChannel.CreateBinaryFormatter(true, includeVersions);
			formatter1.Serialize(outputStream, msg, null);
		}

		internal static Stream SerializeMessage(string mimeType, IMessage msg)
		{
			return CoreChannel.SerializeMessage(mimeType, msg, false);
		}

		internal static Stream SerializeMessage(string mimeType, IMessage msg, bool includeVersions)
		{
			Stream stream1 = new MemoryStream();
			CoreChannel.SerializeMessage(mimeType, msg, stream1, includeVersions);
			stream1.Position = 0;
			return stream1;
		}

		internal static void SerializeMessage(string mimeType, IMessage msg, Stream outputStream, bool includeVersions)
		{
			if (string.Compare(mimeType, "text/xml", false, CultureInfo.InvariantCulture) == 0)
			{
				CoreChannel.SerializeSoapMessage(msg, outputStream, includeVersions);
			}
			else if (string.Compare(mimeType, "application/octet-stream", false, CultureInfo.InvariantCulture) == 0)
			{
				CoreChannel.SerializeBinaryMessage(msg, outputStream, includeVersions);
			}
		}

		internal static Stream SerializeSoapMessage(IMessage msg, bool includeVersions)
		{
			MemoryStream stream1 = new MemoryStream();
			CoreChannel.SerializeSoapMessage(msg, stream1, includeVersions);
			stream1.Position = 0;
			return stream1;
		}

		internal static void SerializeSoapMessage(IMessage msg, Stream outputStream, bool includeVersions)
		{
			/*
			SoapFormatter formatter1 = CoreChannel.CreateSoapFormatter(true, includeVersions);
			IMethodMessage message1 = msg as IMethodMessage;
			if ((message1 != null) && (message1.MethodBase != null))
			{
				SoapTypeAttribute attribute1 = (SoapTypeAttribute) InternalRemotingServices.GetCachedSoapAttribute(message1.MethodBase.DeclaringType);
				if ((attribute1.SoapOptions & SoapOption.AlwaysIncludeTypes) == SoapOption.AlwaysIncludeTypes)
				{
					formatter1.TypeFormat |= FormatterTypeStyle.TypesAlways;
				}
				if ((attribute1.SoapOptions & SoapOption.XsdString) == SoapOption.XsdString)
				{
					formatter1.TypeFormat |= FormatterTypeStyle.XsdString;
				}
			}
			Header[] headerArray1 = CoreChannel.GetSoapHeaders(msg);
			((RemotingSurrogateSelector) formatter1.SurrogateSelector).SetRootObject(msg);
			formatter1.Serialize(outputStream, msg, headerArray1);
			*/
			throw new Exception("not implemented");
		}

		internal static bool SetupUrlBashingForIisSslIfNecessary()
		{
			/*
			HttpContext context1 = HttpContext.Current;
			bool flag1 = false;
			if ((context1 != null) && context1.Request.IsSecureConnection)
			{
				Uri uri1 = context1.Request.Url;
				StringBuilder builder1 = new StringBuilder(100);
				builder1.Append("https://");
				builder1.Append(uri1.Host);
				builder1.Append(":");
				builder1.Append(uri1.Port);
				builder1.Append("/");
				builder1.Append(RemotingConfiguration.ApplicationName);
				string[] textArray1 = new string[2] { IisHelper.ApplicationUrl, builder1.ToString() } ;
				CallContext.SetData("__bashChannelUrl", textArray1);
				flag1 = true;
			}
			return flag1;
			*/
			throw new Exception("not implemented");
		}

		internal static void VerifyNoProviderData(string providerTypeName, ICollection providerData)
		{
			if ((providerData != null) && (providerData.Count > 0))
			{
				throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Providers_Config_NotExpectingProviderData"), providerTypeName));
			}
		}


		// Properties
		internal static IByteBufferPool BufferPool
		{
			get
			{
				return CoreChannel._bufferPool;
			}
		}

		internal static RequestQueue RequestQueue
		{
			get
			{
				return CoreChannel._requestQueue;
			}
		}

		internal static bool IsEnabled
		{
			get
			{
				return s_isEnabled;
			}
			set
			{
				s_isEnabled = value;
			}
		}


		// Fields
		private static IByteBufferPool _bufferPool;
		private static RequestQueue _requestQueue;
		internal const string BinaryMimeType = "application/octet-stream";
		internal const int CLIENT_END_CALL = 0x13;
		internal const int CLIENT_MSG_GEN = 1;
		internal const int CLIENT_MSG_SEND = 4;
		internal const int CLIENT_MSG_SER = 3;
		internal const int CLIENT_MSG_SINK_CHAIN = 2;
		internal const int CLIENT_RET_DESER = 0x10;
		internal const int CLIENT_RET_PROPAGATION = 0x12;
		internal const int CLIENT_RET_RECEIVE = 15;
		internal const int CLIENT_RET_SINK_CHAIN = 0x11;
		internal const int MaxStringLen = 0x200;
		private static string s_hostName;
		private static string s_MachineIp;
		private static IPAddress s_MachineIpAddress;
		private static string s_MachineName;
		internal const int SERVER_DISPATCH = 9;
		internal const int SERVER_MSG_DESER = 6;
		internal const int SERVER_MSG_RECEIVE = 5;
		internal const int SERVER_MSG_SINK_CHAIN = 7;
		internal const int SERVER_MSG_STACK_BUILD = 8;
		internal const int SERVER_RET_END = 14;
		internal const int SERVER_RET_SEND = 13;
		internal const int SERVER_RET_SER = 12;
		internal const int SERVER_RET_SINK_CHAIN = 11;
		internal const int SERVER_RET_STACK_BUILD = 10;
		internal const string SOAPContentType = "text/xml; charset=\"utf-8\"";
		internal const string SOAPMimeType = "text/xml";
		internal static ResourceManager SystemResMgr;
		internal const int TIMING_DATA_EOF = 0x63;
		private static bool s_isEnabled;

		// Nested Types
		private class UriHeaderHandler
		{
			// Methods
			internal UriHeaderHandler(string uri)
			{
				this._uri = null;
				this._uri = uri;
			}

			public object HeaderHandler(Header[] Headers)
			{
				return this._uri;
			}


			// Fields
			private string _uri;
		}
	}
}

