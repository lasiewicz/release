using System;
using System.Net;
using System.Net.Sockets;

namespace Matchnet.RemotingServices.Channels
{
	internal class ExclusiveTcpListener : TcpListener
	{
		internal ExclusiveTcpListener(IPAddress localaddr, int port) : base(localaddr, port)
		{
		}

		internal void Start(bool exclusiveAddressUse)
		{
			bool flag1 = ((exclusiveAddressUse && (Environment.OSVersion.Platform == PlatformID.Win32NT)) && (base.Server != null)) && !base.Active;
			if (flag1)
			{
				base.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, 1);
			}
			try
			{
				base.Start();
			}
			catch (SocketException)
			{
				if (flag1)
				{
					base.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, 0);
					base.Start();
					return;
				}
				throw;
			}
		}

	}
}

