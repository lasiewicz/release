namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.Collections;
	using System.Threading;

	internal class RequestQueue
	{
		// Methods
		internal RequestQueue(int minExternFreeThreads, int minLocalFreeThreads, int queueLimit)
		{
			this._localQueue = new Queue();
			this._externQueue = new Queue();
			this._minExternFreeThreads = minExternFreeThreads;
			this._minLocalFreeThreads = minLocalFreeThreads;
			this._queueLimit = queueLimit;
			this._workItemCallback = new WaitCallback(this.WorkItemCallback);
		}

		private SocketHandler DequeueRequest(bool localOnly)
		{
			object obj1 = null;
			if (this._count > 0)
			{
				lock (this)
				{
					if (this._localQueue.Count > 0)
					{
						obj1 = this._localQueue.Dequeue();
						this._count--;
					}
					else if (!localOnly && (this._externQueue.Count > 0))
					{
						obj1 = this._externQueue.Dequeue();
						this._count--;
					}
				}
			}
			return (SocketHandler) obj1;
		}

		internal void Drain()
		{
			SocketHandler handler1;
			this._draining = true;
			while (this._workItemCount > 0)
			{
				Thread.Sleep(100);
			}
			if (this._count == 0)
			{
				return;
			}
			Label_0022:
				handler1 = this.DequeueRequest(false);
			if (handler1 != null)
			{
				handler1.RejectRequestNowSinceServerIsBusy();
				goto Label_0022;
			}
		}

		internal SocketHandler GetRequestToExecute(SocketHandler sh)
		{
			int num1;
			int num2;
			ThreadPool.GetAvailableThreads(out num1, out num2);
			int num3 = (num2 > num1) ? num1 : num2;
			if ((num3 < this._minExternFreeThreads) || (this._count != 0))
			{
				bool flag1 = RequestQueue.IsLocal(sh);
				if ((flag1 && (num3 >= this._minLocalFreeThreads)) && (this._count == 0))
				{
					return sh;
				}
				if (this._count >= this._queueLimit)
				{
					sh.RejectRequestNowSinceServerIsBusy();
					return null;
				}
				this.QueueRequest(sh, flag1);
				if (num3 >= this._minExternFreeThreads)
				{
					sh = this.DequeueRequest(false);
				}
				else if (num3 >= this._minLocalFreeThreads)
				{
					sh = this.DequeueRequest(true);
				}
				else
				{
					sh = null;
				}
				if (sh == null)
				{
					this.ScheduleMoreWorkIfNeeded();
				}
			}
			return sh;
		}

		private static bool IsLocal(SocketHandler sh)
		{
			return sh.IsLocal();
		}

		internal void ProcessNextRequest(SocketHandler sh)
		{
			sh = this.GetRequestToExecute(sh);
			if (sh != null)
			{
				sh.ProcessRequestNow();
			}
		}

		private void QueueRequest(SocketHandler sh, bool isLocal)
		{
			lock (this)
			{
				if (isLocal)
				{
					this._localQueue.Enqueue(sh);
				}
				else
				{
					this._externQueue.Enqueue(sh);
				}
				this._count++;
			}
		}

		internal void ScheduleMoreWorkIfNeeded()
		{
			if ((!this._draining && (this._count != 0)) && (this._workItemCount < 2))
			{
				Interlocked.Increment(ref this._workItemCount);
				ThreadPool.QueueUserWorkItem(this._workItemCallback);
			}
		}

		private void WorkItemCallback(object state)
		{
			Interlocked.Decrement(ref this._workItemCount);
			if (!this._draining && (this._count != 0))
			{
				int num1;
				int num2;
				ThreadPool.GetAvailableThreads(out num1, out num2);
				bool flag1 = false;
				if (num1 >= this._minLocalFreeThreads)
				{
					SocketHandler handler1 = this.DequeueRequest(num1 < this._minExternFreeThreads);
					if (handler1 != null)
					{
						handler1.ProcessRequestNow();
						flag1 = true;
					}
				}
				if (!flag1)
				{
					Thread.Sleep(250);
					this.ScheduleMoreWorkIfNeeded();
				}
			}
		}


		// Properties
		internal bool IsEmpty
		{
			get
			{
				return (this._count == 0);
			}
		}


		// Fields
		private int _count;
		private bool _draining;
		private Queue _externQueue;
		private Queue _localQueue;
		private int _minExternFreeThreads;
		private int _minLocalFreeThreads;
		private int _queueLimit;
		private WaitCallback _workItemCallback;
		private int _workItemCount;
		private const int _workItemLimit = 2;
	}
}

