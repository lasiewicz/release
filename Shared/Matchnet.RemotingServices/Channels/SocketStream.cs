namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.IO;
	using System.Net.Sockets;
	using System.Runtime.Remoting;

	internal class SocketStream : Stream
	{
		// Methods
		public SocketStream(Socket socket)
		{
			this._timeout = 0;
			if (socket == null)
			{
				throw new ArgumentNullException("socket");
			}
			this._socket = socket;
		}

		public override IAsyncResult BeginRead(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			return this._socket.BeginReceive(buffer, offset, size, SocketFlags.None, callback, state);
		}

		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			return this._socket.BeginSend(buffer, offset, size, SocketFlags.None, callback, state);
		}

		public override void Close()
		{
			this._socket.Close();
		}

		public override int EndRead(IAsyncResult asyncResult)
		{
			return this._socket.EndReceive(asyncResult);
		}

		public override void EndWrite(IAsyncResult asyncResult)
		{
			this._socket.EndSend(asyncResult);
		}

		public override void Flush()
		{
		}

		public override int Read(byte[] buffer, int offset, int size)
		{
			if (this._timeout <= 0)
			{
				return this._socket.Receive(buffer, offset, size, SocketFlags.None);
			}
			IAsyncResult result1 = this._socket.BeginReceive(buffer, offset, size, SocketFlags.None, null, null);
			if ((this._timeout > 0) && !result1.IsCompleted)
			{
				result1.AsyncWaitHandle.WaitOne(this._timeout, false);
				if (!result1.IsCompleted)
				{
					throw new RemotingTimeoutException();
				}
			}
			return this._socket.EndReceive(result1);
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			this._socket.Send(buffer, offset, count, SocketFlags.None);
		}


		// Properties
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		public override bool CanWrite
		{
			get
			{
				return true;
			}
		}

		public override long Length
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		public override long Position
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		public TimeSpan Timeout
		{
			get
			{
				return TimeSpan.FromMilliseconds((double) this._timeout);
			}
			set
			{
				this._timeout = (int) value.TotalMilliseconds;
			}
		}


		// Fields
		private Socket _socket;
		private int _timeout;
	}
}

