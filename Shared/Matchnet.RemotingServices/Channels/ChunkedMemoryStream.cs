namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.IO;
	using System.Runtime.Remoting;

	internal class ChunkedMemoryStream : Stream
	{
		// Methods
		static ChunkedMemoryStream()
		{
			ChunkedMemoryStream.s_defaultBufferPool = new ByteBufferAllocator(0x400);
		}

		public ChunkedMemoryStream()
		{
			this._chunks = null;
			this._bufferPool = null;
			this._bClosed = false;
			this._writeChunk = null;
			this._writeOffset = 0;
			this._readChunk = null;
			this._readOffset = 0;
			this._bufferPool = ChunkedMemoryStream.s_defaultBufferPool;
		}

		public ChunkedMemoryStream(IByteBufferPool bufferPool)
		{
			this._chunks = null;
			this._bufferPool = null;
			this._bClosed = false;
			this._writeChunk = null;
			this._writeOffset = 0;
			this._readChunk = null;
			this._readOffset = 0;
			this._bufferPool = bufferPool;
		}

		private MemoryChunk AllocateMemoryChunk()
		{
			MemoryChunk chunk1 = new MemoryChunk();
			chunk1.Buffer = this._bufferPool.GetBuffer();
			chunk1.Next = null;
			return chunk1;
		}

		public override void Close()
		{
			this._bClosed = true;
			this.ReleaseMemoryChunks(this._chunks);
			this._chunks = null;
			this._writeChunk = null;
			this._readChunk = null;
		}

		public override void Flush()
		{
		}

		private static int min(int a, int b)
		{
			if (a >= b)
			{
				return b;
			}
			return a;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (this._bClosed)
			{
				throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_StreamIsClosed"));
			}
			if (this._readChunk == null)
			{
				if (this._chunks == null)
				{
					return 0;
				}
				this._readChunk = this._chunks;
				this._readOffset = 0;
			}
			byte[] buffer1 = this._readChunk.Buffer;
			int num1 = buffer1.Length;
			if (this._readChunk.Next == null)
			{
				num1 = this._writeOffset;
			}
			int num2 = 0;
			while (count > 0)
			{
				if (this._readOffset == num1)
				{
					if (this._readChunk.Next == null)
					{
						break;
					}
					this._readChunk = this._readChunk.Next;
					this._readOffset = 0;
					buffer1 = this._readChunk.Buffer;
					num1 = buffer1.Length;
					if (this._readChunk.Next == null)
					{
						num1 = this._writeOffset;
					}
				}
				int num3 = ChunkedMemoryStream.min(count, num1 - this._readOffset);
				Buffer.BlockCopy(buffer1, this._readOffset, buffer, offset, num3);
				offset += num3;
				count -= num3;
				this._readOffset += num3;
				num2 += num3;
			}
			return num2;
		}

		public override int ReadByte()
		{
			if (this._bClosed)
			{
				throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_StreamIsClosed"));
			}
			if (this._readChunk == null)
			{
				if (this._chunks == null)
				{
					return 0;
				}
				this._readChunk = this._chunks;
				this._readOffset = 0;
			}
			byte[] buffer1 = this._readChunk.Buffer;
			int num1 = buffer1.Length;
			if (this._readChunk.Next == null)
			{
				num1 = this._writeOffset;
			}
			if (this._readOffset == num1)
			{
				if (this._readChunk.Next == null)
				{
					return -1;
				}
				this._readChunk = this._readChunk.Next;
				this._readOffset = 0;
				buffer1 = this._readChunk.Buffer;
				num1 = buffer1.Length;
				if (this._readChunk.Next == null)
				{
					num1 = this._writeOffset;
				}
			}
			return buffer1[this._readOffset++];
		}

		private void ReleaseMemoryChunks(MemoryChunk chunk)
		{
			if (!(this._bufferPool is ByteBufferAllocator))
			{
				while (chunk != null)
				{
					this._bufferPool.ReturnBuffer(chunk.Buffer);
					chunk = chunk.Next;
				}
			}
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			if (this._bClosed)
			{
				throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_StreamIsClosed"));
			}
			switch (origin)
			{
				case SeekOrigin.Begin:
				{
					this.Position = offset;
					break;
				}
				case SeekOrigin.Current:
				{
					this.Position += offset;
					break;
				}
				case SeekOrigin.End:
				{
					this.Position = this.Length + offset;
					break;
				}
			}
			return this.Position;
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		public virtual byte[] ToArray()
		{
			int num1 = (int) this.Length;
			byte[] buffer1 = new byte[this.Length];
			MemoryChunk chunk1 = this._readChunk;
			int num2 = this._readOffset;
			this._readChunk = this._chunks;
			this._readOffset = 0;
			this.Read(buffer1, 0, num1);
			this._readChunk = chunk1;
			this._readOffset = num2;
			return buffer1;
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			if (this._bClosed)
			{
				throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_StreamIsClosed"));
			}
			if (this._chunks == null)
			{
				this._chunks = this.AllocateMemoryChunk();
				this._writeChunk = this._chunks;
				this._writeOffset = 0;
			}
			byte[] buffer1 = this._writeChunk.Buffer;
			int num1 = buffer1.Length;
			while (count > 0)
			{
				if (this._writeOffset == num1)
				{
					this._writeChunk.Next = this.AllocateMemoryChunk();
					this._writeChunk = this._writeChunk.Next;
					this._writeOffset = 0;
					buffer1 = this._writeChunk.Buffer;
					num1 = buffer1.Length;
				}
				int num2 = ChunkedMemoryStream.min(count, num1 - this._writeOffset);
				Buffer.BlockCopy(buffer, offset, buffer1, this._writeOffset, num2);
				offset += num2;
				count -= num2;
				this._writeOffset += num2;
			}
		}

		public override void WriteByte(byte value)
		{
			if (this._bClosed)
			{
				throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_StreamIsClosed"));
			}
			if (this._chunks == null)
			{
				this._chunks = this.AllocateMemoryChunk();
				this._writeChunk = this._chunks;
				this._writeOffset = 0;
			}
			byte[] buffer1 = this._writeChunk.Buffer;
			int num1 = buffer1.Length;
			if (this._writeOffset == num1)
			{
				this._writeChunk.Next = this.AllocateMemoryChunk();
				this._writeChunk = this._writeChunk.Next;
				this._writeOffset = 0;
				buffer1 = this._writeChunk.Buffer;
				num1 = buffer1.Length;
			}
			buffer1[this._writeOffset++] = value;
		}

		public virtual void WriteTo(Stream stream)
		{
			if (this._bClosed)
			{
				throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_StreamIsClosed"));
			}
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (this._readChunk == null)
			{
				if (this._chunks == null)
				{
					return;
				}
				this._readChunk = this._chunks;
				this._readOffset = 0;
			}
			byte[] buffer1 = this._readChunk.Buffer;
			int num1 = buffer1.Length;
			if (this._readChunk.Next == null)
			{
				num1 = this._writeOffset;
			}
			while (true)
			{
				if (this._readOffset == num1)
				{
					if (this._readChunk.Next == null)
					{
						return;
					}
					this._readChunk = this._readChunk.Next;
					this._readOffset = 0;
					buffer1 = this._readChunk.Buffer;
					num1 = buffer1.Length;
					if (this._readChunk.Next == null)
					{
						num1 = this._writeOffset;
					}
				}
				int num2 = num1 - this._readOffset;
				stream.Write(buffer1, this._readOffset, num2);
				this._readOffset = num1;
			}
		}


		// Properties
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		public override bool CanSeek
		{
			get
			{
				return true;
			}
		}

		public override bool CanWrite
		{
			get
			{
				return true;
			}
		}

		public override long Length
		{
			get
			{
				MemoryChunk chunk2;
				if (this._bClosed)
				{
					throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_StreamIsClosed"));
				}
				int num1 = 0;
				for (MemoryChunk chunk1 = this._chunks; chunk1 != null; chunk1 = chunk2)
				{
					chunk2 = chunk1.Next;
					if (chunk2 != null)
					{
						num1 += chunk1.Buffer.Length;
					}
					else
					{
						num1 += this._writeOffset;
					}
				}
				return (long) num1;
			}
		}

		public override long Position
		{
			get
			{
				if (this._bClosed)
				{
					throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_StreamIsClosed"));
				}
				if (this._readChunk == null)
				{
					return (long) 0;
				}
				int num1 = 0;
				for (MemoryChunk chunk1 = this._chunks; chunk1 != this._readChunk; chunk1 = chunk1.Next)
				{
					num1 += chunk1.Buffer.Length;
				}
				num1 += this._readOffset;
				return (long) num1;
			}
			set
			{
				if (this._bClosed)
				{
					throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_StreamIsClosed"));
				}
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				MemoryChunk chunk1 = this._readChunk;
				int num1 = this._readOffset;
				this._readChunk = null;
				this._readOffset = 0;
				int num2 = (int) value;
				for (MemoryChunk chunk2 = this._chunks; chunk2 != null; chunk2 = chunk2.Next)
				{
					if ((num2 < chunk2.Buffer.Length) || ((num2 == chunk2.Buffer.Length) && (chunk2.Next == null)))
					{
						this._readChunk = chunk2;
						this._readOffset = num2;
						break;
					}
					num2 -= chunk2.Buffer.Length;
				}
				if (this._readChunk == null)
				{
					this._readChunk = chunk1;
					this._readOffset = num1;
					throw new ArgumentOutOfRangeException("value");
				}
			}
		}


		// Fields
		private bool _bClosed;
		private IByteBufferPool _bufferPool;
		private MemoryChunk _chunks;
		private MemoryChunk _readChunk;
		private int _readOffset;
		private MemoryChunk _writeChunk;
		private int _writeOffset;
		private static IByteBufferPool s_defaultBufferPool;

		// Nested Types
		private class MemoryChunk
		{
			// Methods
			public MemoryChunk()
			{
				this.Buffer = null;
				this.Next = null;
			}


			// Fields
			public byte[] Buffer;
			public ChunkedMemoryStream.MemoryChunk Next;
		}
	}
}

