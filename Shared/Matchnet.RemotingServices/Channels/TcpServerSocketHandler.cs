namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.IO;
	using System.Net;
	using System.Net.Sockets;
	using System.Runtime.Remoting;
	using System.Runtime.Remoting.Channels;
	using System.Text;
	using System.Threading;

	internal class TcpServerSocketHandler : TcpSocketHandler
	{
		// Methods
		static TcpServerSocketHandler()
		{
			TcpServerSocketHandler.s_endOfLineBytes = Encoding.ASCII.GetBytes("\r\n");
			TcpServerSocketHandler._connectionIdCounter = 0;
		}

		internal TcpServerSocketHandler(Socket socket, RequestQueue requestQueue) : base(socket, requestQueue)
		{
			this._connectionId = Interlocked.Increment(ref TcpServerSocketHandler._connectionIdCounter);
		}

		public bool CanServiceAnotherRequest()
		{
			return true;
		}

		private string GenerateFaultString(Exception e)
		{
			if (!base.CustomErrorsEnabled())
			{
				return e.ToString();
			}
			return CoreChannel.GetResourceString("Remoting_InternalError");
		}

		public Stream GetRequestStream()
		{
			if (!this._bChunked)
			{
				this._requestStream = new TcpFixedLengthReadingStream(this, this._contentLength);
			}
			else
			{
				this._requestStream = new TcpChunkedReadingStream(this);
			}
			return this._requestStream;
		}

		protected override void PrepareForNewMessage()
		{
			if (this._requestStream != null)
			{
				if (!this._requestStream.FoundEnd)
				{
					this._requestStream.ReadToEnd();
				}
				this._requestStream = null;
			}
		}

		public ITransportHeaders ReadHeaders(out bool isHealthCheck)
		{
			ushort num1;
			BaseTransportHeaders headers1 = new BaseTransportHeaders();
			base.ReadVersionAndOperation(out num1, out isHealthCheck);

			if (num1 == 0 || isHealthCheck)
			{
				this._bOneWayRequest = false;
			}
			else
			{
				if (num1 != 1)
				{
					throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Tcp_ExpectingRequestOp"), num1.ToString()));
				}
				this._bOneWayRequest = true;
			}
			if (!isHealthCheck)
			{
				base.ReadContentLength(out this._bChunked, out this._contentLength);
				base.ReadToEndOfHeaders(headers1);
			}
			headers1.IPAddress = ((IPEndPoint) this.NetSocket.RemoteEndPoint).Address;
			headers1.ConnectionId = this._connectionId;
			return headers1;
		}

		protected override void SendErrorMessageIfPossible(Exception e)
		{
			try
			{
				this.SendErrorResponse(e, true);
			}
			catch
			{
			}
		}

		public void SendErrorResponse(Exception e, bool bCloseConnection)
		{
			if (!this._bOneWayRequest)
			{
				ChunkedMemoryStream stream1 = new ChunkedMemoryStream(CoreChannel.BufferPool);
				base.WritePreambleAndVersion(stream1);
				base.WriteUInt16(2, stream1);
				base.WriteUInt16(0, stream1);
				base.WriteInt32(0, stream1);
				base.WriteUInt16(2, stream1);
				base.WriteByte(3, stream1);
				base.WriteUInt16(1, stream1);
				base.WriteUInt16(3, stream1);
				base.WriteByte(1, stream1);
				base.WriteCountedString(this.GenerateFaultString(e), stream1);
				base.WriteUInt16(5, stream1);
				base.WriteByte(0, stream1);
				base.WriteUInt16(0, stream1);
				stream1.WriteTo(this.NetStream);
				stream1.Close();
			}
		}

		public void SendResponse(ITransportHeaders headers, Stream contentStream)
		{
			if (!this._bOneWayRequest)
			{
				ChunkedMemoryStream stream1 = new ChunkedMemoryStream(CoreChannel.BufferPool);
				base.WritePreambleAndVersion(stream1);
				base.WriteUInt16(2, stream1);
				base.WriteUInt16(0, stream1);
				base.WriteInt32((int) contentStream.Length, stream1);
				base.WriteHeaders(headers, stream1);
				stream1.WriteTo(this.NetStream);
				stream1.Close();
				StreamHelper.CopyStream(contentStream, this.NetStream);
				contentStream.Close();
			}
		}

		public void SendHealthCheck(ITransportHeaders headers, Stream contentStream)
		{
			ChunkedMemoryStream stream1 = new ChunkedMemoryStream(CoreChannel.BufferPool);
			if (CoreChannel.IsEnabled)
			{
				base.WriteString("Matchnet Server Enabled", stream1);
			}
			else
			{
				base.WriteString("Matchnet Server Disabled", stream1);
			}
			stream1.WriteTo(this.NetStream);
			stream1.Close();
		}

		// Fields
		private bool _bChunked;
		private bool _bOneWayRequest;
		private long _connectionId;
		private static long _connectionIdCounter;
		private int _contentLength;
		private TcpReadingStream _requestStream;
		private static byte[] s_endOfLineBytes;
	}
}

