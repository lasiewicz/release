namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.IO;
	using System.Runtime.Remoting;

	internal class StreamHelper
	{
		// Methods
		static StreamHelper()
		{
			StreamHelper._asyncCopyStreamReadCallback = new AsyncCallback(StreamHelper.AsyncCopyStreamReadCallback);
			StreamHelper._asyncCopyStreamWriteCallback = new AsyncCallback(StreamHelper.AsyncCopyStreamWriteCallback);
		}

		public StreamHelper()
		{
		}

		private static void AsyncCopyReadHelper(AsyncCopyStreamResult streamState)
		{
			if (streamState.AsyncRead)
			{
				byte[] buffer1 = streamState.Buffer;
				streamState.Source.BeginRead(buffer1, 0, buffer1.Length, StreamHelper._asyncCopyStreamReadCallback, streamState);
			}
			else
			{
				byte[] buffer2 = streamState.Buffer;
				int num1 = streamState.Source.Read(buffer2, 0, buffer2.Length);
				if (num1 == 0)
				{
					streamState.SetComplete(null, null);
				}
				else
				{
					if (num1 < 0)
					{
						throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_UnknownReadError"));
					}
					StreamHelper.AsyncCopyWriteHelper(streamState, num1);
				}
			}
		}

		private static void AsyncCopyStreamReadCallback(IAsyncResult iar)
		{
			AsyncCopyStreamResult result1 = (AsyncCopyStreamResult) iar.AsyncState;
			try
			{
				int num1 = result1.Source.EndRead(iar);
				if (num1 == 0)
				{
					result1.SetComplete(null, null);
				}
				else
				{
					if (num1 < 0)
					{
						throw new RemotingException(CoreChannel.GetResourceString("Remoting_Stream_UnknownReadError"));
					}
					StreamHelper.AsyncCopyWriteHelper(result1, num1);
				}
			}
			catch (Exception exception1)
			{
				result1.SetComplete(null, exception1);
			}
		}

		private static void AsyncCopyStreamWriteCallback(IAsyncResult iar)
		{
			AsyncCopyStreamResult result1 = (AsyncCopyStreamResult) iar.AsyncState;
			try
			{
				result1.Target.EndWrite(iar);
				StreamHelper.AsyncCopyReadHelper(result1);
			}
			catch (Exception exception1)
			{
				result1.SetComplete(null, exception1);
			}
		}

		private static void AsyncCopyWriteHelper(AsyncCopyStreamResult streamState, int bytesRead)
		{
			if (streamState.AsyncWrite)
			{
				byte[] buffer1 = streamState.Buffer;
				streamState.Target.BeginWrite(buffer1, 0, bytesRead, StreamHelper._asyncCopyStreamWriteCallback, streamState);
			}
			else
			{
				byte[] buffer2 = streamState.Buffer;
				streamState.Target.Write(buffer2, 0, bytesRead);
				StreamHelper.AsyncCopyReadHelper(streamState);
			}
		}

		internal static IAsyncResult BeginAsyncCopyStream(Stream source, Stream target, bool asyncRead, bool asyncWrite, bool closeSource, bool closeTarget, AsyncCallback callback, object state)
		{
			AsyncCopyStreamResult result1 = new AsyncCopyStreamResult(callback, state);
			byte[] buffer1 = CoreChannel.BufferPool.GetBuffer();
			result1.Source = source;
			result1.Target = target;
			result1.Buffer = buffer1;
			result1.AsyncRead = asyncRead;
			result1.AsyncWrite = asyncWrite;
			result1.CloseSource = closeSource;
			result1.CloseTarget = closeTarget;
			try
			{
				StreamHelper.AsyncCopyReadHelper(result1);
			}
			catch (Exception exception1)
			{
				result1.SetComplete(null, exception1);
			}
			return result1;
		}

		internal static void BufferCopy(byte[] source, int srcOffset, byte[] dest, int destOffset, int count)
		{
			if (count > 8)
			{
				Buffer.BlockCopy(source, srcOffset, dest, destOffset, count);
			}
			else
			{
				for (int num1 = 0; num1 < count; num1++)
				{
					dest[destOffset + num1] = source[srcOffset + num1];
				}
			}
		}

		internal static void CopyStream(Stream source, Stream target)
		{
			if (source != null)
			{
				ChunkedMemoryStream stream1 = source as ChunkedMemoryStream;
				if (stream1 != null)
				{
					stream1.WriteTo(target);
				}
				else
				{
					MemoryStream stream2 = source as MemoryStream;
					if (stream2 != null)
					{
						stream2.WriteTo(target);
					}
					else
					{
						byte[] buffer1 = CoreChannel.BufferPool.GetBuffer();
						int num1 = buffer1.Length;
						for (int num2 = source.Read(buffer1, 0, num1); num2 > 0; num2 = source.Read(buffer1, 0, num1))
						{
							target.Write(buffer1, 0, num2);
						}
						CoreChannel.BufferPool.ReturnBuffer(buffer1);
					}
				}
			}
		}

		internal static void EndAsyncCopyStream(IAsyncResult iar)
		{
			AsyncCopyStreamResult result1 = (AsyncCopyStreamResult) iar;
			if (!iar.IsCompleted)
			{
				iar.AsyncWaitHandle.WaitOne();
			}
			if (result1.Exception != null)
			{
				throw result1.Exception;
			}
		}


		// Fields
		private static AsyncCallback _asyncCopyStreamReadCallback;
		private static AsyncCallback _asyncCopyStreamWriteCallback;
	}
}

