namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.Collections;
	using System.Reflection;
	using System.Security.Permissions;
	using System.Runtime.Remoting.Messaging;
	using System.Runtime.Remoting.Channels;

	public interface IClientChannelSinkProvider
	{
		// Methods
		[SecurityPermission(SecurityAction.LinkDemand, Infrastructure=true)]
		System.Runtime.Remoting.Channels.IClientChannelSink CreateSink(IChannelSender channel, string url, object remoteChannelData);

		// Properties
		IClientChannelSinkProvider Next { [SecurityPermission(SecurityAction.LinkDemand, Infrastructure=true)] get; [SecurityPermission(SecurityAction.LinkDemand, Infrastructure=true)] set; }
	}
 

	public interface IChannelSender : System.Runtime.Remoting.Channels.IChannel
	{
		// Methods
		[SecurityPermission(SecurityAction.LinkDemand, Infrastructure=true)]
		IMessageSink CreateMessageSink(string url, object remoteChannelData, out string objectURI);
	}
 


	[Serializable, SecurityPermission(SecurityAction.LinkDemand, Infrastructure=true), SecurityPermission(SecurityAction.InheritanceDemand, Infrastructure=true)]
	public class ChannelDataStore : IChannelDataStore
	{
		// Methods
		public ChannelDataStore(string[] channelURIs)
		{
			this._channelURIs = channelURIs;
			this._extraData = null;
		}

		private ChannelDataStore(string[] channelUrls, DictionaryEntry[] extraData)
		{
			this._channelURIs = channelUrls;
			this._extraData = extraData;
		}

		internal ChannelDataStore InternalShallowCopy()
		{
			return new ChannelDataStore(this._channelURIs, this._extraData);
		}


		// Properties
		public string[] ChannelUris
		{
			get
			{
				return this._channelURIs;
			}
			set
			{
				this._channelURIs = value;
			}
		}

		public object this[object key]
		{
			get
			{
				DictionaryEntry[] entryArray1 = this._extraData;
				for (int num1 = 0; num1 < entryArray1.Length; num1++)
				{
					DictionaryEntry entry1 = entryArray1[num1];
					if (entry1.Key.Equals(key))
					{
						return entry1.Value;
					}
				}
				return null;
			}
			set
			{
				if (this._extraData == null)
				{
					this._extraData = new DictionaryEntry[1] { new DictionaryEntry(key, value) } ;
				}
				else
				{
					int num1 = this._extraData.Length;
					DictionaryEntry[] entryArray1 = new DictionaryEntry[num1 + 1];
					int num2 = 0;
					while (num2 < num1)
					{
						entryArray1[num2] = this._extraData[num2];
						num2++;
					}
					entryArray1[num2] = new DictionaryEntry(key, value);
					this._extraData = entryArray1;
				}
			}
		}


		// Fields
		private string[] _channelURIs;
		private DictionaryEntry[] _extraData;
	}
}

