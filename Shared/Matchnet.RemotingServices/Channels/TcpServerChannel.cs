namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.Collections;
	using System.Net;
	using System.Net.Sockets;
	using System.Runtime.InteropServices;
	using System.Runtime.Remoting;
	using System.Runtime.Remoting.Channels;
	using System.Threading;

	public class TcpServerChannel : IChannelReceiver, IChannel
	{
		// Methods
		public TcpServerChannel(int port)
		{
			this._channelPriority = 1;
			this._channelName = "tcp";
			this._machineName = null;
			this._port = -1;
			this._channelData = null;
			this._forcedMachineName = null;
			this._bUseIpAddress = true;
			this._bindToAddr = IPAddress.Any;
			this._bSuppressChannelData = false;
			this._sinkProvider = null;
			this._transportSink = null;
			this._bExclusiveAddressUse = true;
			this._bListening = false;
			this._startListeningException = null;
			this._waitForStartListening = new AutoResetEvent(false);
			this._port = port;
			this.SetupMachineName();
			this.SetupChannel();
		}


		public TcpServerChannel(IDictionary properties, IServerChannelSinkProvider sinkProvider)
		{
			this._channelPriority = 1;
			this._channelName = "tcp";
			this._machineName = null;
			this._port = -1;
			this._channelData = null;
			this._forcedMachineName = null;
			this._bUseIpAddress = true;
			this._bindToAddr = IPAddress.Any;
			this._bSuppressChannelData = false;
			this._sinkProvider = null;
			this._transportSink = null;
			this._bExclusiveAddressUse = true;
			this._bListening = false;
			this._startListeningException = null;
			this._waitForStartListening = new AutoResetEvent(false);
			if (properties != null)
			{
				foreach (DictionaryEntry entry1 in properties)
				{
					switch (((string) entry1.Key))
					{
						case "name":
						{
							this._channelName = (string) entry1.Value;
							continue;
						}
						case "bindTo":
						{
							this._bindToAddr = IPAddress.Parse((string) entry1.Value);
							continue;
						}
						case "port":
						{
							this._port = Convert.ToInt32(entry1.Value);
							continue;
						}
						case "priority":
						{
							this._channelPriority = Convert.ToInt32(entry1.Value);
							continue;
						}
						case "machineName":
						{
							this._forcedMachineName = (string) entry1.Value;
							continue;
						}
						case "rejectRemoteRequests":
						{
							if (Convert.ToBoolean(entry1.Value))
							{
								this._bindToAddr = IPAddress.Loopback;
							}
							continue;
						}
						case "suppressChannelData":
						{
							this._bSuppressChannelData = Convert.ToBoolean(entry1.Value);
							continue;
						}
						case "useIpAddress":
						{
							this._bUseIpAddress = Convert.ToBoolean(entry1.Value);
							continue;
						}
						case "exclusiveAddressUse":
						{
							goto Label_0267;
						}
						default:
						{
							continue;
						}
					}
				Label_0267:
					this._bExclusiveAddressUse = Convert.ToBoolean(entry1.Value);
				}
			}
			this._sinkProvider = sinkProvider;
			this.SetupMachineName();
			this.SetupChannel();
		}


		public TcpServerChannel(string name, int port)
		{
			this._channelPriority = 1;
			this._channelName = "tcp";
			this._machineName = null;
			this._port = -1;
			this._channelData = null;
			this._forcedMachineName = null;
			this._bUseIpAddress = true;
			this._bindToAddr = IPAddress.Any;
			this._bSuppressChannelData = false;
			this._sinkProvider = null;
			this._transportSink = null;
			this._bExclusiveAddressUse = true;
			this._bListening = false;
			this._startListeningException = null;
			this._waitForStartListening = new AutoResetEvent(false);
			this._channelName = name;
			this._port = port;
			this.SetupMachineName();
			this.SetupChannel();
		}


		public TcpServerChannel(string name, int port, IServerChannelSinkProvider sinkProvider)
		{
			this._channelPriority = 1;
			this._channelName = "tcp";
			this._machineName = null;
			this._port = -1;
			this._channelData = null;
			this._forcedMachineName = null;
			this._bUseIpAddress = true;
			this._bindToAddr = IPAddress.Any;
			this._bSuppressChannelData = false;
			this._sinkProvider = null;
			this._transportSink = null;
			this._bExclusiveAddressUse = true;
			this._bListening = false;
			this._startListeningException = null;
			this._waitForStartListening = new AutoResetEvent(false);
			this._channelName = name;
			this._port = port;
			this._sinkProvider = sinkProvider;
			this.SetupMachineName();
			this.SetupChannel();
		}


		private IServerChannelSinkProvider CreateDefaultServerProviderChain()
		{
			IServerChannelSinkProvider provider1 = new BinaryServerFormatterSinkProvider();
			IServerChannelSinkProvider provider2 = provider1;
			provider2.Next = new SoapServerFormatterSinkProvider();
			return provider1;
		}


		public string GetChannelUri()
		{
			object[] objArray1 = new object[4] { "tcp://", this._machineName, ":", this._port } ;
			return string.Concat(objArray1);
		}


		public virtual string[] GetUrlsForUri(string objectUri)
		{
			string[] textArray1 = new string[1];
			if (!objectUri.StartsWith("/"))
			{
				objectUri = "/" + objectUri;
			}
			textArray1[0] = this.GetChannelUri() + objectUri;
			return textArray1;
		}


		private void Listen()
		{
			bool flag1 = false;
			try
			{
				this._tcpListener.Start(this._bExclusiveAddressUse);
				flag1 = true;
			}
			catch (Exception exception1)
			{
				this._startListeningException = exception1;
			}
			this._waitForStartListening.Set();
			while (flag1)
			{
				try
				{
					Socket socket1 = this._tcpListener.AcceptSocket();
					if (socket1 == null)
					{
						int num1 = Marshal.GetLastWin32Error();
						throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Socket_Accept"), num1.ToString()));
					}
					socket1.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.Debug, 1);
					socket1.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 1);
					LingerOption option1 = new LingerOption(true, 3);
					socket1.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, option1);
					TcpServerSocketHandler handler1 = new TcpServerSocketHandler(socket1, CoreChannel.RequestQueue);
					handler1.DataArrivedCallback = new WaitCallback(this._transportSink.ServiceRequest);
					handler1.BeginReadMessage();
					continue;
				}
				catch (Exception exception2)
				{
					if (!this._bListening)
					{
						flag1 = false;
						continue;
					}
					SocketException exception5 = exception2 as SocketException;
					continue;
				}
			}
		}


		public string Parse(string url, out string objectURI)
		{
			return TcpChannelHelper.ParseURL(url, out objectURI);
		}


		private void SetupChannel()
		{
			this._channelData = new ChannelDataStore(null);
			if (this._port > 0)
			{
				this._channelData.ChannelUris = new string[1] { this.GetChannelUri() } ;
			}
			if (this._sinkProvider == null)
			{
				this._sinkProvider = this.CreateDefaultServerProviderChain();
			}
			CoreChannel.CollectChannelDataFromServerSinkProviders(this._channelData, this._sinkProvider);
			IServerChannelSink sink1 = ChannelServices.CreateServerChannelSinkChain(this._sinkProvider, this);
			this._transportSink = new TcpServerTransportSink(sink1);
			if (this._port >= 0)
			{
				this._tcpListener = new ExclusiveTcpListener(this._bindToAddr, this._port);
				ThreadStart start1 = new ThreadStart(this.Listen);
				this._listenerThread = new Thread(start1);
				this._listenerThread.IsBackground = true;
				this.StartListening(null);
			}
		}


		private void SetupMachineName()
		{
			if (this._forcedMachineName != null)
			{
				this._machineName = CoreChannel.DecodeMachineName(this._forcedMachineName);
			}
			else if (!this._bUseIpAddress)
			{
				this._machineName = CoreChannel.GetMachineName();
			}
			else if (this._bindToAddr == IPAddress.Any)
			{
				this._machineName = CoreChannel.GetMachineIp();
			}
			else
			{
				this._machineName = this._bindToAddr.ToString();
			}
		}


		public void StartListening(object data)
		{
			if ((this._port >= 0) && !this._listenerThread.IsAlive)
			{
				this._listenerThread.Start();
				this._waitForStartListening.WaitOne();
				if (this._startListeningException != null)
				{
					Exception exception1 = this._startListeningException;
					this._startListeningException = null;
					throw exception1;
				}
				this._bListening = true;
				if (this._port == 0)
				{
					this._port = ((IPEndPoint) this._tcpListener.LocalEndpoint).Port;
					if (this._channelData != null)
					{
						this._channelData.ChannelUris = new string[1] { this.GetChannelUri() } ;
					}
				}
			}
		}


		public void StopListening(object data)
		{
			if (this._port > 0)
			{
				this._bListening = false;
				if (this._tcpListener != null)
				{
					this._tcpListener.Stop();
				}
			}
		}


		// Properties
		public object ChannelData
		{
			get
			{
				if (!this._bSuppressChannelData && this._bListening)
				{
					return this._channelData;
				}
				return null;
			}
		}


		public string ChannelName
		{
			get
			{
				return this._channelName;
			}
		}


		public int ChannelPriority
		{
			get
			{
				return this._channelPriority;
			}
		}


		public bool IsEnabled
		{
			get
			{
				return CoreChannel.IsEnabled;
			}
			set
			{
				CoreChannel.IsEnabled = value;
			}
		}

//		public bool 

		// Fields
		private bool _bExclusiveAddressUse;
		private IPAddress _bindToAddr;
		private bool _bListening;
		private bool _bSuppressChannelData;
		private bool _bUseIpAddress;
		private ChannelDataStore _channelData;
		private string _channelName;
		private int _channelPriority;
		private string _forcedMachineName;
		private Thread _listenerThread;
		private string _machineName;
		private int _port;
		private IServerChannelSinkProvider _sinkProvider;
		private Exception _startListeningException;
		private ExclusiveTcpListener _tcpListener;
		private TcpServerTransportSink _transportSink;
		private AutoResetEvent _waitForStartListening;
	}
}

