namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.Collections;
	using System.Globalization;
	using System.IO;
	using System.Net.Sockets;
	using System.Runtime.InteropServices;
	using System.Runtime.Remoting;
	using System.Runtime.Remoting.Channels;
	using System.Text;

	internal abstract class TcpSocketHandler : SocketHandler
	{
		static byte[] _getPreamble;

		// Methods
		static TcpSocketHandler()
		{
			TcpSocketHandler.s_protocolPreamble = Encoding.ASCII.GetBytes(".NET");
			_getPreamble = Encoding.ASCII.GetBytes("GET /\n");
			byte[] buffer1 = new byte[2];
			buffer1[0] = 1;
			TcpSocketHandler.s_protocolVersion1_0 = buffer1;
		}

		public TcpSocketHandler(Socket socket) : this(socket, null)
		{
		}

		public TcpSocketHandler(Socket socket, RequestQueue requestQueue) : base(socket, requestQueue)
		{
		}

		private void ReadAndMatchPreamble(out bool isHealthCheck)
		{
			isHealthCheck = false;

			if (base.ReadAndMatchFourBytes(TcpSocketHandler.s_protocolPreamble, _getPreamble, out isHealthCheck))
			{
				return;
			}

			if (isHealthCheck)
			{
				ChunkedMemoryStream stream1 = new ChunkedMemoryStream(CoreChannel.BufferPool);
				isHealthCheck = true;
				return;
			}			

			throw new RemotingException(CoreChannel.GetResourceString("Remoting_Tcp_ExpectingPreamble"));
		}

		private void ReadAndVerifyHeaderFormat(string headerName, byte expectedFormat)
		{
			byte num1 = (byte) base.ReadByte();
			if (num1 != expectedFormat)
			{
				throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Tcp_IncorrectHeaderFormat"), expectedFormat, headerName));
			}
		}

		protected void ReadContentLength(out bool chunked, out int contentLength)
		{
			contentLength = -1;
			ushort num1 = base.ReadUInt16();
			if (num1 == 1)
			{
				chunked = true;
			}
			else
			{
				if (num1 != 0)
				{
					throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Tcp_ExpectingContentLengthHeader"), num1.ToString()));
				}
				chunked = false;
				contentLength = base.ReadInt32();
			}
		}

		protected string ReadCountedString()
		{
			byte num1 = (byte) base.ReadByte();
			int num2 = base.ReadInt32();
			if (num2 <= 0)
			{
				return null;
			}
			byte[] buffer1 = new byte[num2];
			base.Read(buffer1, 0, num2);
			switch (num1)
			{
				case 0:
				{
					return Encoding.Unicode.GetString(buffer1);
				}
				case 1:
				{
					return Encoding.UTF8.GetString(buffer1);
				}
			}
			throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Tcp_UnrecognizedStringFormat"), num1.ToString()));
		}

		protected void ReadToEndOfHeaders(BaseTransportHeaders headers)
		{
			bool flag1 = false;
			string text1 = null;
			for (ushort num1 = base.ReadUInt16(); num1 != 0; num1 = base.ReadUInt16())
			{
				if (num1 == 1)
				{
					string text2 = this.ReadCountedString();
					string text3 = this.ReadCountedString();
					headers[text2] = text3;
				}
				else if (num1 == 4)
				{
					string text6;
					this.ReadAndVerifyHeaderFormat("RequestUri", 1);
					string text4 = this.ReadCountedString();
					string text5 = TcpChannelHelper.ParseURL(text4, out text6);
					if (text5 == null)
					{
						text6 = text4;
					}
					headers.RequestUri = text6;
				}
				else if (num1 == 2)
				{
					this.ReadAndVerifyHeaderFormat("StatusCode", 3);
					ushort num2 = base.ReadUInt16();
					if (num2 != 0)
					{
						flag1 = true;
					}
				}
				else if (num1 == 3)
				{
					this.ReadAndVerifyHeaderFormat("StatusPhrase", 1);
					text1 = this.ReadCountedString();
				}
				else if (num1 == 6)
				{
					this.ReadAndVerifyHeaderFormat("Content-Type", 1);
					string text7 = this.ReadCountedString();
					headers.ContentType = text7;
				}
				else
				{
					byte num3 = (byte) base.ReadByte();
					switch (num3)
					{
						case 0:
						{
							goto Label_013D;
						}
						case 1:
						{
							this.ReadCountedString();
							goto Label_013D;
						}
						case 2:
						{
							base.ReadByte();
							goto Label_013D;
						}
						case 3:
						{
							base.ReadUInt16();
							goto Label_013D;
						}
						case 4:
						{
							base.ReadInt32();
							goto Label_013D;
						}
					}
					throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Tcp_UnknownHeaderType"), num1, num3));
				}
			Label_013D:;
			}
			if (flag1)
			{
				if (text1 == null)
				{
					text1 = "";
				}
				throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Tcp_GenericServerError"), text1));
			}
		}

		protected void ReadVersionAndOperation(out ushort operation,
			out bool isHealthCheck)
		{
			this.ReadAndMatchPreamble(out isHealthCheck);
			
			byte num1 = (byte) base.ReadByte();
			byte num2 = (byte) base.ReadByte();
			if (((num1 != 1) || (num2 != 0)) && !isHealthCheck)
			{
				throw new RemotingException(string.Format(CoreChannel.GetResourceString("Remoting_Tcp_UnknownProtocolVersion"), num1.ToString() + "." + num2.ToString()));
			}
			operation = base.ReadUInt16();
		}

		private void WriteContentTypeHeader(string value, Stream outputStream)
		{
			base.WriteUInt16(6, outputStream);
			base.WriteByte(1, outputStream);
			this.WriteCountedString(value, outputStream);
		}

		protected void WriteCountedString(string str, Stream outputStream)
		{
			int num1 = 0;
			if (str != null)
			{
				num1 = str.Length;
			}
			if (num1 > 0)
			{
				byte[] buffer1 = Encoding.UTF8.GetBytes(str);
				base.WriteByte(1, outputStream);
				base.WriteInt32(buffer1.Length, outputStream);
				outputStream.Write(buffer1, 0, buffer1.Length);
			}
			else
			{
				base.WriteByte(0, outputStream);
				base.WriteInt32(0, outputStream);
			}
		}

		protected void WriteString(string str, Stream outputStream)
		{
			byte[] buffer1 = Encoding.UTF8.GetBytes(str);
			outputStream.Write(buffer1, 0, buffer1.Length);
		}

		private void WriteCustomHeader(string name, string value, Stream outputStream)
		{
			base.WriteUInt16(1, outputStream);
			this.WriteCountedString(name, outputStream);
			this.WriteCountedString(value, outputStream);
		}

		protected void WriteHeaders(ITransportHeaders headers, Stream outputStream)
		{
			IEnumerator enumerator1 = null;
			BaseTransportHeaders headers1 = headers as BaseTransportHeaders;
			if (headers1 != null)
			{
				if (headers1.ContentType != null)
				{
					this.WriteContentTypeHeader(headers1.ContentType, outputStream);
				}
				enumerator1 = headers1.GetOtherHeadersEnumerator();
			}
			else
			{
				enumerator1 = headers.GetEnumerator();
			}
			if (enumerator1 != null)
			{
				while (enumerator1.MoveNext())
				{
					DictionaryEntry entry1 = (DictionaryEntry) enumerator1.Current;
					string text1 = (string) entry1.Key;
					if (!StringHelper.StartsWithDoubleUnderscore(text1))
					{
						string text2 = entry1.Value.ToString();
						if ((headers1 == null) && (string.Compare(text1, "Content-Type", true, CultureInfo.InvariantCulture) == 0))
						{
							this.WriteContentTypeHeader(text2, outputStream);
							continue;
						}
						this.WriteCustomHeader(text1, text2, outputStream);
					}
				}
			}
			base.WriteUInt16(0, outputStream);
		}

		protected void WritePreambleAndVersion(Stream outputStream)
		{
			outputStream.Write(TcpSocketHandler.s_protocolPreamble, 0, TcpSocketHandler.s_protocolPreamble.Length);
			outputStream.Write(TcpSocketHandler.s_protocolVersion1_0, 0, TcpSocketHandler.s_protocolVersion1_0.Length);
		}


		// Fields
		private static byte[] s_protocolPreamble;
		private static byte[] s_protocolVersion1_0;
	}
}

