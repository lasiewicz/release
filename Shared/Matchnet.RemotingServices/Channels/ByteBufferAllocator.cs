namespace Matchnet.RemotingServices.Channels
{
	using System;

	internal class ByteBufferAllocator : IByteBufferPool
	{
		// Methods
		public ByteBufferAllocator(int bufferSize)
		{
			this._bufferSize = bufferSize;
		}

		public byte[] GetBuffer()
		{
			return new byte[this._bufferSize];
		}

		public void ReturnBuffer(byte[] buffer)
		{
		}


		// Fields
		private int _bufferSize;
	}
}

