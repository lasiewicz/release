namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.Threading;

	internal interface IByteBufferPool
	{
		// Methods
		byte[] GetBuffer();
		void ReturnBuffer(byte[] buffer);
	}
 

	internal class ByteBufferPool : IByteBufferPool
	{
		// Methods
		public ByteBufferPool(int maxBuffers, int bufferSize)
		{
			this._bufferPool = null;
			this._controlCookie = "cookie object";
			this._max = maxBuffers;
			this._bufferPool = new byte[this._max][];
			this._bufferSize = bufferSize;
			this._current = -1;
			this._last = -1;
		}

		public byte[] GetBuffer()
		{
			byte[] buffer2;
			object obj1 = null;
			try
			{
				obj1 = Interlocked.Exchange(ref this._controlCookie, (object) null);
				if (obj1 != null)
				{
					if (this._current == -1)
					{
						this._controlCookie = obj1;
						return new byte[this._bufferSize];
					}
					byte[] buffer1 = this._bufferPool[this._current];
					this._bufferPool[this._current] = null;
					if (this._current == this._last)
					{
						this._current = -1;
					}
					else
					{
						this._current = (this._current + 1) % this._max;
					}
					this._controlCookie = obj1;
					return buffer1;
				}
				buffer2 = new byte[this._bufferSize];
			}
			catch (ThreadAbortException)
			{
				if (obj1 != null)
				{
					this._current = -1;
					this._last = -1;
					this._controlCookie = obj1;
				}
				throw;
			}
			return buffer2;
		}

		public void ReturnBuffer(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			object obj1 = null;
			try
			{
				obj1 = Interlocked.Exchange(ref this._controlCookie, (object) null);
				if (obj1 == null)
				{
					return;
				}
				if (this._current == -1)
				{
					this._bufferPool[0] = buffer;
					this._current = 0;
					this._last = 0;
				}
				else
				{
					int num1 = (this._last + 1) % this._max;
					if (num1 != this._current)
					{
						this._last = num1;
						this._bufferPool[this._last] = buffer;
					}
				}
				this._controlCookie = obj1;
			}
			catch (ThreadAbortException)
			{
				if (obj1 != null)
				{
					this._current = -1;
					this._last = -1;
					this._controlCookie = obj1;
				}
				throw;
			}
		}


		// Fields
		private byte[][] _bufferPool;
		private int _bufferSize;
		private object _controlCookie;
		private int _current;
		private int _last;
		private int _max;
	}
}

