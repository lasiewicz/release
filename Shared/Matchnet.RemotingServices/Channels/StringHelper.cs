namespace Matchnet.RemotingServices.Channels
{
	using System;

	internal class StringHelper
	{
		// Methods
		public StringHelper()
		{
		}

		internal static bool StartsWithAsciiIgnoreCasePrefixLower(string str, string asciiPrefix)
		{
			int num1 = asciiPrefix.Length;
			if (str.Length < num1)
			{
				return false;
			}
			for (int num2 = 0; num2 < num1; num2++)
			{
				if (StringHelper.ToLowerAscii(str[num2]) != asciiPrefix[num2])
				{
					return false;
				}
			}
			return true;
		}

		internal static bool StartsWithDoubleUnderscore(string str)
		{
			if ((str.Length >= 2) && (str[0] == '_'))
			{
				return (str[1] == '_');
			}
			return false;
		}

		private static char ToLowerAscii(char ch)
		{
			if ((ch >= 'A') && (ch <= 'Z'))
			{
				throw new Exception("not implemented");
				//return (ch + ' ');
			}
			return ch;
		}

	}
}

