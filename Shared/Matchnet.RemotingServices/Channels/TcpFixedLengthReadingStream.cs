namespace Matchnet.RemotingServices.Channels
{
	using System;
	using System.Runtime.Remoting.Channels;

	internal class TcpFixedLengthReadingStream : TcpReadingStream
	{
		// Methods
		internal TcpFixedLengthReadingStream(SocketHandler inputStream, int contentLength)
		{
			this._inputStream = inputStream;
			this._bytesLeft = contentLength;
		}

		public override void Close()
		{
			this._inputStream.OnInputStreamClosed();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (this._bytesLeft == 0)
			{
				return 0;
			}
			int num1 = this._inputStream.Read(buffer, offset, Math.Min(this._bytesLeft, count));
			if (num1 > 0)
			{
				this._bytesLeft -= num1;
			}
			return num1;
		}

		public override int ReadByte()
		{
			if (this._bytesLeft == 0)
			{
				return -1;
			}
			this._bytesLeft--;
			return this._inputStream.ReadByte();
		}


		// Properties
		public override bool FoundEnd
		{
			get
			{
				return (this._bytesLeft == 0);
			}
		}


		// Fields
		private int _bytesLeft;
		private SocketHandler _inputStream;
	}
}

