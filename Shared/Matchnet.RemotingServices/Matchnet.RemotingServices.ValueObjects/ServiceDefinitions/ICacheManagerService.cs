using System;

namespace Matchnet.RemotingServices.ValueObjects.ServiceDefinitions
{
	public interface ICacheManagerService
	{
		void Remove(string key);
	}
}
