using System;
using System.Messaging;

using Matchnet.RemotingServices.ValueObjects;

namespace Matchnet.RemotingServices.ValueObjects.ServiceDefinitions
{
	public interface IHydraManagerService
	{
		PhysicalDatabaseStatus[] GetPhysicalDatabaseStatus();

		System.Messaging.Message GetMessage(string queuePath,
			string messageID);

		void RequeueMessage(string queuePath,
			string messageID);

		void PurgeQueue(string queuePath);
	}
}
