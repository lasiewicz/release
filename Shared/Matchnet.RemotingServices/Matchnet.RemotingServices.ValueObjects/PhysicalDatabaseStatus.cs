using System;
using System.Collections;

using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data.Hydra;

namespace Matchnet.RemotingServices.ValueObjects
{
	[Serializable]
	public class PhysicalDatabaseStatus
	{
		private string _serverName;
		private string _physicalDatabaseName;
		private bool _isActive;
		private bool _isRecovering;
		private bool _isFailed;
		private HydraError[] _errors;

		public PhysicalDatabaseStatus(string serverName,
			string physicalDatabaseName,
			bool isActive,
			bool isRecovering,
			bool isFailed,
			HydraError[] errors)
		{
			_serverName = serverName;
			_physicalDatabaseName = physicalDatabaseName;
			_isActive = isActive;
			_isRecovering = isRecovering;
			_isFailed = isFailed;
			_errors = errors;
		}


		public string ServerName
		{
			get
			{
				return _serverName;
			}
		}


		public string PhysicalDatabaseName
		{
			get
			{
				return _physicalDatabaseName;
			}
		}


		public bool IsActive
		{
			get
			{
				return _isActive;
			}
		}


		public bool IsRecovering
		{
			get
			{
				return _isRecovering;
			}
		}


		public bool IsFailed
		{
			get
			{
				return _isFailed;
			}
		}


		public HydraError[] Errors
		{
			get
			{
				return _errors;
			}
		}
	}
}
