/*
using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Text;
using System.Reflection;

using Matchnet;


namespace Matchnet.RemotingServices
{

	/// <summary>
	/// 
	/// </summary>
	public sealed class AdministrationHelper
	{
		private AdministrationHelper()
		{

		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static string GetServiceConfiguration()
		{
			return ServiceInstance.Instance.ToString();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static int GetThreadCount()
		{
			return Process.GetCurrentProcess().Threads.Count;
		}
				
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static DateTime GetServiceStartTime()
		{
			return MonitoredServiceBase.ServiceStartTime;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static string GetProcessName()
		{
			return Process.GetCurrentProcess().ProcessName;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static int GetPeakMemoryUsage()
		{
			return Process.GetCurrentProcess().PeakWorkingSet;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static int GetCacheItemCount()
		{
			return Matchnet.Caching.Cache.Instance.Count;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static int GetMemoryUsage()
		{
			return Process.GetCurrentProcess().WorkingSet;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static string GetMachineSettings()
		{
			return "!!!! NOT IMPLEMENTED !!!!";
		}



		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static string GetCustomSettings()
		{
			StringBuilder output = new StringBuilder(1000);

			try
			{
				NameValueCollection customSettings = Configuration.InitializationSettings.GetAll();
				foreach(string key in customSettings.Keys)
				{
					output.Append("KEY: " + key + "  VALUE: " + Configuration.InitializationSettings.Get(key) + "\n");
				}
			}
			catch(Exception ex)
			{
				output.Append("!!!!! Exception occurred when outputting the Custom Settings for this machine: " + ex.ToString());
			}

			return output.ToString();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static bool IsHealthCheckEnabled()
		{
			return HealthCheckListener.IsEnabled();
		}


		/// <summary>
		/// 
		/// </summary>
		public static void EnableService()
		{
			HealthCheckListener.Enable();
		}

		/// <summary>
		/// 
		/// </summary>
		public static void DisableService()
		{
			HealthCheckListener.Disable();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static string GetHtmlDump(bool showRuntimeSettings)
		{

			StringBuilder output = new StringBuilder(10000);

			output.Append("<HTML>");
			output.Append("<HEAD>");
			output.Append("	<TITLE>" + Environment.MachineName + ": " + MonitoredServiceBase.ServiceConstant + "</TITLE>");
			output.Append("</HEAD>");
			output.Append("<style>");
			output.Append("	.FieldLabel{border:1px solid #999999;font-family:arial;font-size:12px;};");
			output.Append("	.FieldData{border:1px solid #999999;font-family:arial;font-size:12px;};");
			output.Append("	.SectionHead{font-family:arial;font-size:12px;color:#ffffff;font-weight:bold;background-color:#000066;};");
			output.Append("	.SubSectionHead{border:1px solid #999999;font-family:arial;font-size:12px;color:#e4e4e4;font-weight:bold;background-color:#333333;};");
			output.Append("	.TinyHeader{font-family:verdana;font-size:11px;};");
			output.Append("	.LargeHeader{font-family:verdana;font-size:18px;font-weight:bold;};");
			output.Append("</style>");
			output.Append("<BODY bgcolor=#d4d0c8>");
			output.Append("<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>");
			output.Append("	<TR>");
			output.Append("		<TD class=TinyHeader valign=bottom>server=</TD>");
			output.Append("		<TD class=LargeHeader valign=bottom>\\\\" + Environment.MachineName + "</TD>");
			output.Append("		<TD class=TinyHeader valign=bottom>&nbsp;&nbsp;&nbsp;service=</TD>");
			output.Append("		<TD class=LargeHeader>" + MonitoredServiceBase.ServiceConstant + "</TD>");
			output.Append("		<TD class=TinyHeader valign=bottom>&nbsp;&nbsp;&nbsp;port=</TD>");
			output.Append("		<TD class=LargeHeader valign=bottom>" + ServiceInstance.Instance.Port.ToString() + "</TD>");
			output.Append("	</TR>");
			output.Append("</TABLE>");
			output.Append("<br>");
			output.Append("<TABLE WIDTH=800 BORDER=0 CELLSPACING=2 CELLPADDING=3>");
			output.Append("	<TR>");
			output.Append("		<td class=SectionHead>SERVICE CONFIGURATION</td>");
			output.Append("		<td class=SectionHead>ENVIRONMENTALS</td>");
			output.Append("	</TR>");
			output.Append("	<TR>");
			output.Append("		<TD valign=top>");
			output.Append("			<TABLE WIDTH=100% BORDER=0 CELLSPACING=1 CELLPADDING=1>");
			output.Append("				<TR>");
			output.Append("					<td class=SubSectionHead colspan=2>SERVICE INSTANCE</td>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Server URI:</TD>");
			output.Append("					<TD class=FieldData>" + ServiceInstance.Instance.PartitionUri + "</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Service Constant:</TD>");
			output.Append("					<TD class=FieldData>" + MonitoredServiceBase.ServiceConstant + "</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Partition Offset:</TD>");
			output.Append("					<TD class=FieldData>" + ServiceInstance.Instance.PartitionOffset.ToString() + "</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Service Port:</TD>");
			output.Append("					<TD class=FieldData>" + ServiceInstance.Instance.Port.ToString() + "</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Configuration Port:</TD>");
			output.Append("					<TD class=FieldData>" + ServiceInstance.Instance.ConfigurationPort.ToString() + "</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Health Check Port:</TD>");
			output.Append("					<TD class=FieldData>" + ServiceInstance.Instance.HealthCheckPort.ToString() + "</TD>");
			output.Append("				</TR>");
			output.Append("			</TABLE>");
			output.Append("		</TD>");
			output.Append("		<TD valign=top>");
			output.Append("			<TABLE WIDTH=100% BORDER=0 CELLSPACING=0 CELLPADDING=0>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Memory Usage:</TD>");
			output.Append("					<TD class=FieldData>" + AdministrationHelper.GetMemoryUsage().ToString("####,####,####,####,##0") + " Bytes</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Peak Memory Usage:</TD>");
			output.Append("					<TD class=FieldData>" + AdministrationHelper.GetPeakMemoryUsage().ToString("####,####,####,####,##0") + " Bytes</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Process Name:</TD>");
			output.Append("					<TD class=FieldData>" + MonitoredServiceBase.ServiceConstant + "</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Thread Count:</TD>");
			output.Append("					<TD class=FieldData>" + AdministrationHelper.GetThreadCount().ToString("###,###0") + "</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Matchnet.Caching.Cache:</TD>");
			output.Append("					<TD class=FieldData>" + AdministrationHelper.GetCacheItemCount().ToString() + " item(s)</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel><B><a href='http://" + ServiceInstance.Instance.PartitionUri + ":" + ServiceInstance.Instance.HealthCheckPort + "'><font color:#000000;>Health Check (Heartbeat)</font></a>:</B></TD>");
			if(HealthCheckListener.IsEnabled())
			{
				output.Append("						<TD class=FieldData>ENABLED</TD>");
			}
			else
			{
				output.Append("						<TD class=FieldData><font color=Red><B>DISABLED</B></font></TD>");
			}
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>Service Start Time:</TD>");
			output.Append("					<TD class=FieldData>" + AdministrationHelper.GetServiceStartTime().ToString() + "</TD>");
			output.Append("				</TR>");
			output.Append("				<TR>");
			output.Append("					<TD class=FieldLabel>(System) Clock:</TD>");
			output.Append("					<TD class=FieldData>" + System.DateTime.Now.ToString() + "</TD>");
			output.Append("				</TR>");
			output.Append("			</TABLE>");
			output.Append("		</TD>");
			output.Append("	</TR>");
			NameValueCollection serviceManagers = MonitoredServiceBase.ServiceManagers;
			output.Append("	<tr>");
			output.Append("		<td colspan=2 class=SectionHead>SERVICE MANAGERS (" + serviceManagers.Count.ToString() + " total)</td>");
			output.Append("	</tr>");
			output.Append("	<tr>");
			output.Append("		<td colspan=2>");

			output.Append("			<TABLE WIDTH=100% BORDER=0 CELLSPACING=1 CELLPADDING=1>");
			if(serviceManagers.Count > 0)
			{
				foreach(string key in serviceManagers.Keys)
				{
					output.Append("				<TR>");
					output.Append("					<TD class=FieldData>" + serviceManagers[key].ToLower() + "</TD>");
					output.Append("				</TR>");
				}
			}
			else
			{
				output.Append("				<TR>");
				output.Append("					<TD class=FieldData align=center bgcolor='Red'>No Service Managers has been registered for this service.</TD>");
				output.Append("				</TR>");
			}
			output.Append("			</TABLE>");

			output.Append("		</td>");
			output.Append("	</tr>");
			output.Append("	<tr>");
			NameValueCollection administrativeServiceManagers = MonitoredServiceBase.AdministrativeServiceManagers;
			output.Append("		<td colspan=2 class=SectionHead>ADMINISTRATIVE SERVICE MANAGERS (" + administrativeServiceManagers.Count.ToString() + " total)</td>");
			output.Append("	</tr>");
			output.Append("	<tr>");
			output.Append("		<td colspan=2>");

			output.Append("			<TABLE WIDTH=100% BORDER=0 CELLSPACING=1 CELLPADDING=1>");
			
			if(administrativeServiceManagers.Count > 0)
			{
				foreach(string key in administrativeServiceManagers.Keys)
				{
					output.Append("				<TR>");
					output.Append("					<TD class=FieldData>" + administrativeServiceManagers[key].ToLower() + "</TD>");
					output.Append("				</TR>");
				}
			}
			else
			{
				output.Append("				<TR>");
				output.Append("					<TD class=FieldData align=center>No <I>Administration</I> Service Managers has been registered for this service.</TD>");
				output.Append("				</TR>");
			}
			output.Append("			</TABLE>");

			output.Append("		</td>");
			output.Append("	</tr>");
			NameValueCollection runtimeSettings = Configuration.RuntimeSettings.GetAllSettings();
			output.Append("	<tr>");
			output.Append("		<td colspan=2 class=SectionHead>RUNTIME SETTINGS (" + runtimeSettings.Count.ToString() + " total)</td>");
			output.Append("	</tr>");
			output.Append("	<tr>");
			output.Append("		<td colspan=2>");

			output.Append("			<TABLE WIDTH=100% BORDER=0 CELLSPACING=1 CELLPADDING=1>");
			if(showRuntimeSettings)
			{
				output.Append("				<TR>");
				output.Append("					<td class=SubSectionHead>SETTING PATH</td>");
				output.Append("					<td class=SubSectionHead>SETTING VALUE</td>");
				output.Append("				</TR>");
				foreach(string path in runtimeSettings)
				{
					output.Append("				<TR>");
					output.Append("					<TD class=FieldLabel>" + path + "</TD>");
					output.Append("					<TD class=FieldData>" + runtimeSettings[path].ToString() + "</TD>");
					output.Append("				</TR>");
				}
			}
			else
			{
				output.Append("				<TR>");
				output.Append("					<TD class=FieldLabel  colspan=2 align=center bgcolor='#ffffff'><BR><a href='http://" + ServiceInstance.Instance.PartitionUri + ":" + ServiceInstance.Instance.ConfigurationPort + "/SHOW_CONFIGURATION_SETTINGS'>Click here</a> to view all the runtime configuration settings for this machine.<BR><BR></TD>");
				output.Append("				</TR>");
				
			}
			output.Append("			</TABLE>");

			output.Append("		</td>");
			output.Append("	</tr>");
			output.Append("	<tr>");
			NameValueCollection initializationSettings = Configuration.InitializationSettings.GetAll();
			output.Append("		<td colspan=2 class=SectionHead>INTIALIZATION SETTINGS (" + initializationSettings.Count.ToString() + " total)</td>");
			output.Append("	</tr>");
			output.Append("	<tr>");
			output.Append("		<td colspan=2>");

			output.Append("			<TABLE WIDTH=100% BORDER=0 CELLSPACING=1 CELLPADDING=1>");
			output.Append("				<TR>");
			output.Append("					<td class=SubSectionHead>SETTING PATH</td>");
			output.Append("					<td class=SubSectionHead>SETTING VALUE</td>");
			output.Append("				</TR>");
			foreach(string path in initializationSettings)
			{
				output.Append("				<TR>");
				output.Append("					<TD class=FieldLabel>" + path + "</TD>");
				output.Append("					<TD class=FieldData>" + initializationSettings[path].ToString() + "</TD>");
				output.Append("				</TR>");
			}
			output.Append("			</TABLE>");

			output.Append("		</td>");
			output.Append("	</tr>");

			output.Append("	<tr>");
			AssemblyName[] assemblies = System.Reflection.Assembly.GetExecutingAssembly().GetReferencedAssemblies();
			output.Append("		<td colspan=2 class=SectionHead>ASSEMBLY REFERENCES (" + assemblies.Length.ToString() + " total)</td>");
			output.Append("	</tr>");

			output.Append("	<tr>");
			output.Append("		<td colspan=2>");

			output.Append("			<TABLE WIDTH=100% BORDER=0 CELLSPACING=1 CELLPADDING=1>");
			output.Append("				<TR>");
			output.Append("					<td class=SubSectionHead>ASSEMBLY NAME</td>");
			output.Append("					<td class=SubSectionHead>VERSION</td>");
			output.Append("				</TR>");
			foreach(AssemblyName assemblyName in assemblies)
			{
				output.Append("				<TR>");
				output.Append("					<TD class=FieldLabel>" + assemblyName.Name + "</TD>");
				output.Append("					<TD class=FieldData>" + assemblyName.Version.ToString() + "</TD>");
				output.Append("				</TR>");
			}
			output.Append("			</TABLE>");

			output.Append("		</td>");
			output.Append("	</tr>");




			
			output.Append("</TABLE>");
			output.Append("</BODY>");
			output.Append("</HTML>");

			try
			{
			
			}
			catch(Exception ex)
			{
				output.Append("!!! Exception occurred when building configuration screen output.  Actual Exception:" +  ex.ToString());
			}

			return output.ToString();
		}
	}
}
*/