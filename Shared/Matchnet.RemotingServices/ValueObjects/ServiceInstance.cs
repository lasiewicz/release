using System;

namespace Matchnet.RemotingServices
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public sealed class ServiceInstance : IValueObject
	{
		private byte _partitionOffset = 0;
		private string _partitionUri = Constants.NULL_STRING;
		private int _port = Constants.NULL_INT;

		/// <summary>
		/// 
		/// </summary>
		public static readonly ServiceInstance Instance = new ServiceInstance();


		private ServiceInstance()
		{
			// Hide default constructor
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="partitionOffset"></param>
		/// <param name="partitionUri"></param>
		/// <param name="port"></param>
		public void Populate(byte partitionOffset, string partitionUri, int port)
		{
			_partitionOffset = partitionOffset;
			_partitionUri = partitionUri;
			_port = port;
		}

		/// <summary>
		/// 
		/// </summary>
		public int PartitionOffset
		{
			get{return _partitionOffset;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string PartitionUri
		{
			get{return _partitionUri;}
		}
	
		/// <summary>
		/// 
		/// </summary>
		public int Port
		{
			get{return _port;}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public int HealthCheckPort
		{
			get{return _port + 99;}
		}

			
		/// <summary>
		/// 
		/// </summary>
		public int ConfigurationPort
		{
			get{return _port + 98;}
		}

	
	}
}

