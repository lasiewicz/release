using System;

using Matchnet.Exceptions;
using Matchnet.Caching;


namespace Matchnet.RemotingServices.BusinessLogic
{
	/// <summary>
	/// 
	/// </summary>
	public class CacheManagerBL
	{
		/// <summary>
		/// 
		/// </summary>
		public static readonly CacheManagerBL Instance = new CacheManagerBL();
		
		private Matchnet.Caching.Cache _cache;

		private CacheManagerBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		public void Remove(string key)
		{
			try
			{
				_cache.Remove(key);
			}
			catch (Exception ex)
			{
				throw new BLException("Error removing item from cache. (key: " + key + ")", ex);
			}
		}
	}
}
