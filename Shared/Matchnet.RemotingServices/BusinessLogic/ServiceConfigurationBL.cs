using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;


namespace Matchnet.RemotingServices.BusinessLogic
{
	/// <summary>
	/// 
	/// </summary>
	public class ServiceConfigurationBL
	{
		/// <summary>
		/// 
		/// </summary>
		public static readonly ServiceConfigurationBL Instance = new ServiceConfigurationBL();

		private ServiceConfigurationBL()
		{
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <param name="machineName"></param>
		public void ConfigureService(string serviceConstant, string machineName)
		{
			const string CONFIGURATION_SVC = Matchnet.Configuration.ValueObjects.ServiceConstants.SERVICE_CONSTANT;

			try
			{
				if (serviceConstant == CONFIGURATION_SVC)
				{
					//todo
					Command command = new Command("mnSystem", "dbo.up_ServiceInstance_GetSingle", 0);
					command.AddParameter("@ServiceConstant", SqlDbType.VarChar, ParameterDirection.Input,  serviceConstant);
					command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input,  machineName);
					DataTable dataTable = Client.Instance.ExecuteDataTable(command);

					if(dataTable == null || dataTable.Rows.Count == 0)
					{
						throw(new BLException("Could not load Service Configuration meta data from database for ServiceConstant = '" + serviceConstant + "', ServerName = '" + machineName + "'."));
					}

					DataRow dataRow = dataTable.Rows[0];
					byte partitionOffset = (byte)dataRow["PartitionOffset"];
					string partitionUri = dataRow["PartitionUri"].ToString();
					int port = (int)dataRow["ServicePort"];

					ServiceInstance.Instance.Populate(partitionOffset, partitionUri, port);
				}
				else
				{
					ServiceInstanceConfig serviceInstanceConfig = RuntimeSettings.GetServiceInstanceConfig(serviceConstant, machineName);
					ServiceInstance.Instance.Populate(serviceInstanceConfig.PartitionOffset,
						serviceInstanceConfig.PartitionUri,
						serviceInstanceConfig.Port);
				}
			}
			catch(Exception ex)
			{
				throw(new BLException("Failure loading ServiceInstance metadata.", ex));
			}
		}

	}
}
