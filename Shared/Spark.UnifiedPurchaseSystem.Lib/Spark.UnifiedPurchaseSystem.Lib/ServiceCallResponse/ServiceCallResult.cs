﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.UnifiedPurchaseSystem.Lib.ServiceCallResponse
{
    public struct ServiceCallResult
    {
        public string response { get; set; }
        public string responseCode { get; set; }
        public string responseMessage { get; set; }
        public Dictionary<string, object> responseValues { get; set; }
    }
}
