﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Spark.UnifiedPurchaseSystem.Lib.ServiceCallResponse
{
    public class PaymentResponseHelper
    {
        public static ServiceCallResult LoadResultObject(string data)
        {
            ServiceCallResult result = new ServiceCallResult();
            Dictionary<string, object> values = new Dictionary<string, object>();
            result.responseValues = values;

            XmlDocument doc = new XmlDocument();
            doc.InnerXml = data;

            XmlNodeList node = doc.GetElementsByTagName("InternalResponse");

            result.responseCode = node[0].FirstChild.InnerText;
            result.responseMessage = node[0].FirstChild.NextSibling.InnerText;

            if (result.responseCode == "0")
            {
                //node = doc.GetElementsByTagName("ResponseValues");
                //Dictionary<string, string> responsevalues = DesrializeGatewayResponse(node);
                //result.responseValues.Add("responseTime", responsevalues["responseTime"]);
                //result.responseValues.Add("litleTxnId", responsevalues["litleTxnId"]);
                if (doc.GetElementsByTagName("paymentID").Count > 0)
                {
                    result.responseValues.Add("paymentID", doc.GetElementsByTagName("paymentID")[0].InnerText);
                }
                if (doc.GetElementsByTagName("paymentEngineOperation").Count > 0)
                {
                    result.responseValues.Add("paymentEngineOperation", doc.GetElementsByTagName("paymentEngineOperation")[0].InnerText);
                }
                if (doc.GetElementsByTagName("paymentprofileid").Count > 0)
                {
                    result.responseValues.Add("paymentProfileID", doc.GetElementsByTagName("paymentprofileid")[0].InnerText);
                }
                if (doc.GetElementsByTagName("isCreditSetToVoid").Count > 0)
                {
                    result.responseValues.Add("isCreditSetToVoid", doc.GetElementsByTagName("isCreditSetToVoid")[0].InnerText);
                }
            }
            result.responseValues.Add("xml", data);
            return result;
        }

        private static Dictionary<string, string> DesrializeGatewayResponse(XmlNodeList nodes)
        {

            Dictionary<string, string> list = new Dictionary<string, string>();
            foreach (XmlNode node in nodes[0].ChildNodes)
            {
                if (!list.ContainsKey(node.FirstChild.InnerText))
                    list.Add(node.FirstChild.InnerText, node.FirstChild.NextSibling.InnerText);
            }

            return list;
        }
    }
}
