﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Spark.UnifiedPurchaseSystem.Lib.CustomContext
{
    public class ServiceRequestContext : IExtension<OperationContext>
    {
        private int _requestID;

        public int RequestID
        {
            get { return this._requestID; }
            set { this._requestID = value; }
        }

        public static ServiceRequestContext Current
        {
            get { return OperationContext.Current.Extensions.Find<ServiceRequestContext>(); }
        }

        public static void AddToOperationContext()
        {
            OperationContext.Current.Extensions.Add(new ServiceRequestContext());
        }

        public static void RemoveFromOperationContext()
        {
            OperationContext.Current.Extensions.Remove(ServiceRequestContext.Current);
        }

        #region IExtension<OperationContext> Members

        public void Attach(OperationContext owner)
        {
            // initialize here
            this.RequestID = (new Random().Next(1, System.Int32.MaxValue));
        }

        public void Detach(OperationContext owner)
        {
            // clean up here
            this.RequestID = 0;
        }

        #endregion
    }
}
