﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.UnifiedPurchaseSystem.Lib.ServiceCallStatus
{
    public class InternalServiceCallStatusResponse
    {
        #region Private Members

        private int _internalResponseID = Constants.NULL_INT;
        private string _internalResponseCode = Constants.NULL_STRING;
        private string _internalResponseDescription = Constants.NULL_STRING;

        #endregion

        #region Constructors

        public InternalServiceCallStatusResponse()
        {

        }

        public InternalServiceCallStatusResponse(string internalResponseCode, string internalResponseDescription)
        {
            this._internalResponseCode = internalResponseCode;
            this._internalResponseDescription = internalResponseDescription;
        }

        #endregion

        #region Properties

        public int InternalResponseID
        {
            get { return this._internalResponseID; }
            set { this._internalResponseID = value; }
        }

        public string InternalResponseCode
        {
            get { return this._internalResponseCode; }
            set { this._internalResponseCode = value; }
        }

        public string InternalResponseDescription
        {
            get { return this._internalResponseDescription; }
            set { this._internalResponseDescription = value; }
        }

        #endregion
    }

    public enum ServiceCallStatus : int
    {
        Success = 0
    }


}
