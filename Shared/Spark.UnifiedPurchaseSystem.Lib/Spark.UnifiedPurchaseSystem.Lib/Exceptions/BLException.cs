﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.UnifiedPurchaseSystem.Lib.Exceptions
{
    [Serializable]
    public class BLException : ExceptionBase
    {
        #region Constructors

        public BLException(string message)
            : base(message)
        {

        }

        public BLException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public BLException(string message, Exception innerException, ExceptionLevel exceptionLevelOverride)
            : base(message, innerException, exceptionLevelOverride)
        {

        }

        protected BLException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        #endregion
    }
}
