﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.UnifiedPurchaseSystem.Lib.Exceptions
{
    [Serializable]
    public class SMException : ExceptionBase
    {
        #region Constructors

        public SMException(string message)
            : base(message)
        {

        }

        public SMException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public SMException(string message, Exception innerException, ExceptionLevel exceptionLevelOverride)
            : base(message, innerException, exceptionLevelOverride)
        {

        }

        protected SMException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        #endregion
    }
}
