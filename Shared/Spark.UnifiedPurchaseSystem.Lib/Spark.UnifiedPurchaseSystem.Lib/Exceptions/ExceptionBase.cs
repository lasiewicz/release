﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.UnifiedPurchaseSystem.Lib.Exceptions
{
    [Serializable]
    public abstract class ExceptionBase : ApplicationException
    {
        #region Private Members

        private string _exceptionMessageTrace = String.Empty;
        private ExceptionLevel _exceptionLevel = ExceptionLevel.None;

        #endregion

        #region Constructors

        public ExceptionBase(string message)
            : base(message)
        {

        }

        public ExceptionBase(string message, Exception innerException)
            : base(message, innerException)
        {
            this._exceptionMessageTrace = BuildExceptionMessageTrace(message, innerException, Constants.NULL_STRING);
            SetLastExceptionLevel(innerException);
        }

        public ExceptionBase(string message, Exception innerException, ExceptionLevel exceptionLevelOverride)
            : base(message, innerException)
        {
            this._exceptionMessageTrace = BuildExceptionMessageTrace(message, innerException, Constants.NULL_STRING);
            this._exceptionLevel = exceptionLevelOverride;
        }

        public ExceptionBase(string message, Exception innerException, string detailedDatabaseMessage)
            : base(message, innerException)
        {
            this._exceptionMessageTrace = BuildExceptionMessageTrace(message, innerException, detailedDatabaseMessage);
            SetLastExceptionLevel(innerException);
        }

        public ExceptionBase(string message, Exception innerException, string detailedDatabaseMessage, ExceptionLevel exceptionLevelOverride)
            : base(message, innerException)
        {
            this._exceptionMessageTrace = BuildExceptionMessageTrace(message, innerException, detailedDatabaseMessage);
            this._exceptionLevel = exceptionLevelOverride;
        }

        protected ExceptionBase(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        #endregion

        #region Properties

        public string ExceptionMessageTrace
        {
            get { return this._exceptionMessageTrace; }
            set { this._exceptionMessageTrace = value; }
        }

        public ExceptionLevel ExceptionLevel
        {
            get { return this._exceptionLevel; }
            set { this._exceptionLevel = value; }
        }

        #endregion

        private string BuildExceptionMessageTrace(string newMessage, Exception previousException, string detailedDatabaseException)
        {
            StringBuilder sb = new StringBuilder();
            ExceptionBase previousExceptionBase = previousException as ExceptionBase;
            if (previousExceptionBase != null)
            {
                if (previousExceptionBase.ExceptionMessageTrace != String.Empty
                    || previousExceptionBase.ExceptionMessageTrace.Length > 0)
                {
                    previousExceptionBase.ExceptionMessageTrace = previousExceptionBase.ExceptionMessageTrace.Replace("[Error Rethrown]", "[Error Caught and Rethrown]");
                }

                sb.Append(previousExceptionBase.ExceptionMessageTrace);
            }
            else
            {
                sb.Append("Exception messages trace\r\n");
                sb.Append("[Originated]\r\n");
                sb.Append(previousException.Message.ToString());
                if (detailedDatabaseException != Constants.NULL_STRING)
                {
                    sb.Append("\r\n");
                    sb.Append(detailedDatabaseException);
                }
            }
            sb.Append("\r\n\r\n");
            sb.Append("[Error Rethrown]\r\n");
            sb.Append(newMessage);
            //sb.Append("\r\n");

            return sb.ToString();
        }

        private void SetLastExceptionLevel(Exception previousException)
        {
            ExceptionBase previousExceptionBase = previousException as ExceptionBase;
            if (previousExceptionBase != null)
            {
                this._exceptionLevel = previousExceptionBase.ExceptionLevel;
            }
        }

    }
}
