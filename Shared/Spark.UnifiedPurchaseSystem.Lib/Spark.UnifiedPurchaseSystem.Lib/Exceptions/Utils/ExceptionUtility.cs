﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.Logging;
//using Matchnet.Data;

namespace Spark.UnifiedPurchaseSystem.Lib.Exceptions
{
    public class ExceptionUtility
    {
        public static ExceptionLevel DetermineSQLDBExceptionLevel(SqlException ex)
        {
            ExceptionLevel exceptionLevel = ExceptionLevel.None;

            switch (ex.Number)
            {
                case -2:
                    // Connection timeout 
                    exceptionLevel = ExceptionLevel.Fatal;
                    break;
                case 18456:
                case 1326:
                    // login failed
                    exceptionLevel = ExceptionLevel.Fatal;
                    break;
                case 4060:
                    // database is not found 
                    exceptionLevel = ExceptionLevel.Fatal;
                    break;
                case 229:
                    // login credentials check
                    exceptionLevel = ExceptionLevel.Fatal;
                    break;
                default:
                    exceptionLevel = ExceptionLevel.Error;
                    break;
            }

            return exceptionLevel;
        }

        public static void LogException(ExceptionBase exceptionBase, Type type, string title)
        {
            Logger logger = new Logger(type);

            if (exceptionBase.ExceptionLevel == ExceptionLevel.Error)
            {
                logger.Error(title + "\r\n" + exceptionBase.ExceptionMessageTrace + "\r\n", exceptionBase);
            }
            else if (exceptionBase.ExceptionLevel == ExceptionLevel.Fatal)
            {
                logger.Fatal(title + "\r\n" + exceptionBase.ExceptionMessageTrace + "\r\n", exceptionBase);
            }
        }

        /*
        public static string BuildDBErr(string connectionString,
            Command command)
        {
            StringBuilder err = new StringBuilder();
            err.Append("Connection string: ");

            if (connectionString != null)
            {
                err.Append(connectionString + "\r\n");
            }
            else
            {
                err.Append("(unknown)\r\n");
            }

            err.Append("Command: ");

            if (command != null)
            {
                err.Append("Command: " + command.StoredProcedureName + "\r\n");

                for (Int32 parameterNum = 0; parameterNum < command.Parameters.Count; parameterNum++)
                {
                    Parameter parameter = command.Parameters[parameterNum];
                    if (parameter.ParameterDirection == ParameterDirection.Input)
                    {
                        err.Append("\t" + parameter.Name + ": ");
                        if (parameter.ParameterValue != null)
                        {
                            if (parameter.Name.ToLower().IndexOf("credit") > 0
                                && parameter.Name.ToLower().IndexOf("card") > 0
                                && parameter.Name.ToLower().IndexOf("number") > 0)
                            {
                                err.Append("****************" + "\r\n");
                            }
                            else
                            {
                                err.Append(parameter.ParameterValue.ToString() + "\r\n");
                            }
                        }
                        else
                        {
                            err.Append("(null)\r\n");
                        }
                    }
                }
            }
            else
            {
                err.Append("(null)\r\n");
            }

            return err.ToString();
        }
        */

    }
}
