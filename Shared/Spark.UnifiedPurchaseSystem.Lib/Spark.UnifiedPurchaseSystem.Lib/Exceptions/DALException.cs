﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.UnifiedPurchaseSystem.Lib.Exceptions
{
    [Serializable]
    public class DALException : ExceptionBase
    {
        #region Constructors

        public DALException(string message)
            : base(message)
        {

        }

        public DALException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public DALException(string message, Exception innerException, string detailedDatabaseException)
            : base(message, innerException, detailedDatabaseException)
        {

        }

        public DALException(string message, Exception innerException, string detailedDatabaseException, ExceptionLevel exceptionLevel)
            : base(message, innerException, detailedDatabaseException, exceptionLevel)
        {

        }

        protected DALException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        #endregion

    }
}
