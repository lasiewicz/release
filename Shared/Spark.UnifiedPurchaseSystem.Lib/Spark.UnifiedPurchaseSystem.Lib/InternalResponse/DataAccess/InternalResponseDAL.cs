﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Data;
using System.Data;
using System.Data.SqlClient;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;

namespace Spark.UnifiedPurchaseSystem.Lib.InternalResponse.DataAccess
{
    public class InternalResponseDAL
    {
        public static InternalResponse GetInternalResponse(ServiceStatusType serviceStatus)
        {
            InternalResponse internalResponse = null;
            SqlDataReader dataReader = null;

            try
            {
                Command command = new Command("epValidation", "dbo.up_InternalServiceStatus_Get", 0);
                command.AddParameter("@ServiceStatusID", SqlDbType.Int, ParameterDirection.Input, (int)serviceStatus);
                dataReader = Client.Instance.ExecuteReader(command);

                if (dataReader.Read())
                {
                    internalResponse = new InternalResponse();
                    internalResponse.InternalResponseID = dataReader.GetInt32(dataReader.GetOrdinal("InternalResponseStatusID"));
                    internalResponse.InternalResponseCode = dataReader.GetString(dataReader.GetOrdinal("InternalStatusCode"));
                    internalResponse.InternalResponseDescription = dataReader.GetString(dataReader.GetOrdinal("InternalStatusMessage"));
                }

                return internalResponse;
            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("InternalResponseDAL.GetInternalResponseFromServiceCallStatus() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("InternalResponseDAL.GetInternalResponseFromServiceCallStatus() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
        }

    }
}
