﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.UnifiedPurchaseSystem.Lib.InternalResponse
{
    public enum ServiceStatusType
    {
        Success = 0,
        SystemError = -1,
        PendingTransactionExistsForMemberOnThisSite = 105,
        ZipCodeNotFound = 106,
        CountryNotFound = 107,
        NegativeOrderAmount = 108
    }
}
