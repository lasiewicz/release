﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.UnifiedPurchaseSystem.Lib.InternalResponse
{
    [DataContract(Name = "internalresponse", Namespace = Constants.CONTRACT_NAMESPACE)]
    [KnownType(typeof(InternalResponse))]
    public class InternalResponse
    {
        #region Private Members

        private int _internalResponseID = Constants.NULL_INT;
        private string _internalResponseCode = Constants.NULL_STRING;
        private string _internalResponseDescription = Constants.NULL_STRING;

        #endregion

        #region Constructors

        public InternalResponse()
        {

        }

        public InternalResponse(string internalResponseCode, string internalResponseDescription)
        {
            this._internalResponseCode = internalResponseCode;
            this._internalResponseDescription = internalResponseDescription;
        }

        #endregion

        #region Properties

        //[XmlIgnore]
        public int InternalResponseID
        {
            get { return this._internalResponseID; }
            set { this._internalResponseID = value; }
        }

        [DataMember(Name = "responsecode", Order = 1)]
        public string InternalResponseCode
        {
            get { return this._internalResponseCode; }
            set { this._internalResponseCode = value; }
        }

        [DataMember(Name = "responsemessage", Order = 2)]
        public string InternalResponseDescription
        {
            get { return this._internalResponseDescription; }
            set { this._internalResponseDescription = value; }
        }

        #endregion
    }
}
