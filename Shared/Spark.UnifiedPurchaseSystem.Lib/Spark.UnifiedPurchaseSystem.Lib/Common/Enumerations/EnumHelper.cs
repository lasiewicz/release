﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.UnifiedPurchaseSystem.Lib.Common
{
    public class EnumHelper
    {
        public static TransactionType ConvertTransactionTypeStringToEnum(string transactionType)
        {
            if (Enum.IsDefined(typeof(TransactionType), transactionType))
            {
                return (TransactionType)Enum.Parse(typeof(TransactionType), transactionType, true);
            }
            else
            {
                throw new ApplicationException("Transaction type does not exists");
            }
        }

        public static PaymentType ConvertPaymentTypeStringToEnum(string paymentType)
        {
            if (Enum.IsDefined(typeof(PaymentType), paymentType))
            {
                return (PaymentType)Enum.Parse(typeof(PaymentType), paymentType, true);
            }
            else
            {
                throw new ApplicationException("PaymentType type does not exists");
            }
        }
    }
}
