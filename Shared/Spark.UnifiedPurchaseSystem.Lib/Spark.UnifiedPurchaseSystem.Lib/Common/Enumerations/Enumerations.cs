﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.UnifiedPurchaseSystem.Lib.Common
{
    #region Exception Level

    /// <summary>
    /// The level of exception    
    /// </summary>
    public enum ExceptionLevel : int
    {
        None = 0,
        Debug = 1,
        Information = 2,
        Warning = 3,
        Error = 4,
        Fatal = 5
    }

    #endregion

    #region Payment Transaction Types

    /// <summary>
    /// Order transaction types
    /// </summary>
    [DataContract]
    public enum TransactionType
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        InitialSubscriptionPurchase = 1,
        [EnumMember]
        Renewal = 2,
        [EnumMember]
        Credit = 3,
        [EnumMember]
        Void = 4,
        [EnumMember]
        AutoRenewalTerminate = 5,
        [EnumMember]
        Authorization = 6,
        [EnumMember]
        PaymentProfileAuthorization = 10,
        [EnumMember]
        PlanChange = 11,
        [EnumMember]
        TrialPayAdjustment = 12,
        [EnumMember]
        AutoRenewalReopen = 18,
        [EnumMember]
        AdditionalSubscriptionPurchase = 23, //upgrade before
        [EnumMember]
        AdditionalNonSubscriptionPurchase = 24, //downgrade before
        [EnumMember]
        VirtualTerminalPurchase = 34,
        [EnumMember]
        VirtualTerminalCredit = 35,
        [EnumMember]
        VirtualTerminalVoid = 36,
        [EnumMember]
        TrialTakenInitialSubscription = 1001,
        [EnumMember]
        ChargeBack = 1004,
        [EnumMember]
        CheckReversal = 1005,
        [EnumMember]
        Adjustment = 1007,
        [EnumMember]
        GiftPurchase = 1013,
        [EnumMember]
        GiftRedeem = 1014,
        //Swap = 25,
        //Extension = 26,
        //TrialTermination = 1006,
        //CreditSales = 1008,
        //CreditRenewal = 1009,
        //VoidSubscription = 1010,
        //VoidRenewal = 1011,        
    }

    #endregion

    #region Payment Profile Types

    /// <summary>
    /// The payment type of the payment profile
    /// </summary>
    [Flags]
    [DataContract]
    public enum PaymentType : int
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        CreditCard = 1,
        [EnumMember]
        Check = 2,
        [EnumMember]
        DirectDebit = 4,
        [EnumMember]
        PayPal = 8,
        [EnumMember]
        SMS = 16
    }

    #endregion

    #region System and its Types

    /// <summary>
    /// The type of the calling system    
    /// </summary>
    public enum SystemType : int
    {
        None = 0,
        System = 1,
        User = 2,
        VirtualTerminal = 3,
        CreditTool = 4,
        AccessScheduler = 5,
        SparkRenewal = 6,
        EmailTracker = 7
    }

    /// <summary>
    /// The various calling systems
    /// Note: Please update corresponding enum description collection if this enum changes
    /// </summary>
    public enum System : int
    {
        JDateCoIL = 4,
        DateCoUK = 6,
        DateCA = 13,
        CupidCoIL = 15,
        TravelEvents = 99,
        SparkCom = 100,
        AmericanSingles = 101,
        JDate = 103,
        JDateFR = 105,
        JDateCoUK = 107,
        Kizmeet = 301,
        JewishMingle = 9171
    }

    #endregion

    #region Order Status

    /// <summary>
    /// The status of the order    
    /// </summary>
    public enum OrderStatus : int
    {
        None = 0,
        Pending = 1,
        Successful = 2,
        Failed = 3
    }
    #endregion

    #region Order Reasons
    /// <summary>
    /// Note: Please update corresponding enum description collection if this enum changes
    /// </summary>
    public enum OrderReasons : int
    {
        None = 0
        ,AvoidingChargeback = 1
        ,CancellationOfService = 2
        ,CancelledEvent = 3
        ,DissatisfiedWithService = 4
        ,DoubleCharge = 5
        ,FraudulentTransactionUnauthorized = 6
        ,FraudulentTransactionUnknownUser = 7
        ,PurchasedWrongPackage = 8
        ,RefundPrepaidServices = 9
        ,TerminationNotProcessed = 10
        ,UnableToUseServiceOrAttendEvent = 11
        ,UnawareOfAutoRenewal = 12
        ,Other = 13
    }

    #endregion

    #region DurationType
    /// <summary>
    /// Note: Please update corresponding enum description collection if this enum changes
    /// </summary>
    public enum DurationType : int
    {
        None = 0,
        Minutes = 1,
        Hour = 2,
        Day = 3,
        Week = 4,
        Month = 5,
        Year = 6
    }
    #endregion

    #region EnumDescriptions
    /// <summary>
    /// These descriptions are mainly for the UI
    /// </summary>
    public class EnumDescriptions
    {
        public static List<KeyValuePair<OrderReasons, string>> GetOrderReasonsDescription()
        {
            List<KeyValuePair<OrderReasons, string>> OrderReasonsList = new List<KeyValuePair<OrderReasons, string>>();

            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.None, "None"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.AvoidingChargeback, "Avoiding a chargeback"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.CancellationOfService, "Cancellation of Service (per 3 day terms)"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.CancelledEvent, "Cancelled event"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.DissatisfiedWithService, "Dissatisfied with service / site unavailable"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.DoubleCharge, "Double charge"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.FraudulentTransactionUnauthorized, "Fraudulent Transaction - Unauthorized User"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.FraudulentTransactionUnknownUser, "Fraudulent Transaction - Unknown User"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.PurchasedWrongPackage, "Purchased the wrong package"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.RefundPrepaidServices, "Refund of Prepaid Services"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.TerminationNotProcessed, "Termination Not Processed"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.UnableToUseServiceOrAttendEvent, "Unable to use service / attend event"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.UnawareOfAutoRenewal, "Unaware of Auto-Renewal"));
            OrderReasonsList.Add(new KeyValuePair<OrderReasons, string>(OrderReasons.Other, "Other (note account)"));

            return OrderReasonsList;
        }

        public static List<KeyValuePair<System, string>> GetSystemDescription()
        {
            List<KeyValuePair<System, string>> DescList = new List<KeyValuePair<System, string>>();

            DescList.Add(new KeyValuePair<System, string>(System.AmericanSingles, "AmericanSingles.com"));
            DescList.Add(new KeyValuePair<System, string>(System.CupidCoIL, "Cupid.co.il"));
            DescList.Add(new KeyValuePair<System, string>(System.DateCA, "Date.ca"));
            DescList.Add(new KeyValuePair<System, string>(System.DateCoUK, "Date.co.uk"));
            DescList.Add(new KeyValuePair<System, string>(System.JDate, "JDate.com"));
            DescList.Add(new KeyValuePair<System, string>(System.JDateCoIL, "JDate.co.il"));
            DescList.Add(new KeyValuePair<System, string>(System.JDateCoUK, "JDate.co.uk"));
            DescList.Add(new KeyValuePair<System, string>(System.JDateFR, "JDate.fr"));
            DescList.Add(new KeyValuePair<System, string>(System.JewishMingle, "JewishMingle.com"));
            DescList.Add(new KeyValuePair<System, string>(System.Kizmeet, "Kizmeet.com"));
            DescList.Add(new KeyValuePair<System, string>(System.SparkCom, "Spark.com"));
            DescList.Add(new KeyValuePair<System, string>(System.TravelEvents, "Travel & Events"));

            return DescList;
        }

        public static List<KeyValuePair<DurationType, string>> GetDurationTypeDescription()
        {
            List<KeyValuePair<DurationType, string>> DescList = new List<KeyValuePair<DurationType, string>>();

            DescList.Add(new KeyValuePair<DurationType, string>(DurationType.None, "None"));
            DescList.Add(new KeyValuePair<DurationType, string>(DurationType.Minutes, "Minute"));
            DescList.Add(new KeyValuePair<DurationType, string>(DurationType.Hour, "Hour"));
            DescList.Add(new KeyValuePair<DurationType, string>(DurationType.Day, "Day"));
            DescList.Add(new KeyValuePair<DurationType, string>(DurationType.Week, "Week"));
            DescList.Add(new KeyValuePair<DurationType, string>(DurationType.Month, "Month"));
            DescList.Add(new KeyValuePair<DurationType, string>(DurationType.Year, "Year"));

            return DescList;
        }
    }
    #endregion
}
