﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.UnifiedPurchaseSystem.Lib.Common.Utils
{
    public class PurchaseUtility
    {
        public static bool IsRenewalTransaction(TransactionType trantype)
        {
            //as we settle on the complete list of transaction types, we can centralize all types that represents
            //a renewal transaction
            if (trantype == TransactionType.Renewal)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
