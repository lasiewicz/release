﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using CleanCode.Diagnostics;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.CustomContext;
using System.Configuration;

namespace Spark.UnifiedPurchaseSystem.Lib.Logging
{
    public class TraceLog
    {
        private static StructuredTraceSource _appTraceSource;

        static TraceLog()
        {
            AlignedTextWriterTraceListener.EventIdLength = 10;
            AlignedTextWriterTraceListener.TimestampLength = 17;
            //AlignedTextWriterTraceListener.ProcessIdLength = 8;
            //AlignedTextWriterTraceListener.ThreadIdLength = 8;
            AlignedTextWriterTraceListener.TraceSourceNameLength = 17;
            AlignedTextWriterTraceListener.TraceEventTypeLength = 15;

            TraceLog._appTraceSource = new StructuredTraceSource(ConfigurationSettings.AppSettings["TraceLogSourceName"],
                (TraceOptions.DateTime));
            //TraceLog._appTraceSource = new StructuredTraceSource("DemoMain",
            //    (TraceOptions.DateTime | TraceOptions.ProcessId | TraceOptions.ThreadId));
        }

        public static void WriteLine(string message, params object[] args)
        {
            try
            {
                TraceLog._appTraceSource.TraceEvent(TraceEventType.Information, ServiceRequestContext.Current.RequestID, message, FormatParameters(args));
            }
            catch (Exception exception)
            {
                return;
            }
        }

        public static void WriteErrorLine(Exception ex, string message, params object[] args)
        {
            try
            {
                SMException serviceManagerException = ex as SMException;
                if (serviceManagerException != null)
                {
                    TraceLog._appTraceSource.TraceEvent(TraceEventType.Error, ServiceRequestContext.Current.RequestID, message + "\r\n" + serviceManagerException.ExceptionMessageTrace, args);
                }
                else
                {
                    TraceLog._appTraceSource.TraceEvent(TraceEventType.Error, ServiceRequestContext.Current.RequestID, message, FormatParameters(args));
                }
            }
            catch (Exception exception)
            {
                return;
            }
        }

        public static void Enter(string name)
        {
            try
            {
                TraceLog._appTraceSource.TraceEnter(name, ServiceRequestContext.Current.RequestID);
            }
            catch (Exception exception)
            {
                return;
            }
        }

        public static void Leave(string name)
        {
            try
            {
                TraceLog._appTraceSource.TraceLeave(name, ServiceRequestContext.Current.RequestID);
            }
            catch (Exception exception)
            {
                return;
            }
        }

        #region Helper Methods

        public static object[] FormatParameters(object[] args)
        {
            try
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == null)
                    {
                        args[i] = "null";
                    }
                    else
                    {
                        Type type = args[i].GetType();

                        if (type == typeof(System.String))
                        {
                            args[i] = FormatStringParameter((string)args[i]);
                        }
                        else if (type == typeof(System.Int16)
                                    || type == typeof(System.Int32)
                                    || type == typeof(System.Int64))
                        {
                            args[i] = Convert.ToString(args[i]);
                        }
                        else if (type == typeof(System.Decimal))
                        {
                            args[i] = String.Format("{0:0.####}", args[i]);
                        }
                        else if (type == typeof(System.Boolean))
                        {
                            args[i] = Convert.ToString(args[i]);
                        }
                        else if (type == typeof(System.DateTime))
                        {
                            args[i] = Convert.ToString(args[i]);
                        }
                        else if (type == typeof(System.Guid))
                        {
                            args[i] = args[i].ToString();
                        }
                        else if (type.IsEnum)
                        {
                            args[i] = Enum.GetName(type, args[i]);
                        }
                        else
                        {
                            args[i] = FormatObjectParameter(args[i]);
                        }
                    }
                }

                return args;
            }
            catch (Exception exception)
            {
                return args;
            }           
        }

        public static string FormatObjectParameter(object obj)
        {
            try
            {
                if (obj != null)
                {
                    return "\"" + obj.ToString() + "\"";
                }
                else
                {
                    return "null";
                }
            }
            catch (Exception exception)
            {
                return String.Empty;
            }
        }

        public static string FormatStringParameter(string s)
        {
            try
            {
                if (s == null)
                {
                    return "null";
                }
                else
                {
                    return "\"" + s + "\"";
                }
            }
            catch (Exception exception)
            {
                return String.Empty;
            }            
        }

        #endregion


    }
}
