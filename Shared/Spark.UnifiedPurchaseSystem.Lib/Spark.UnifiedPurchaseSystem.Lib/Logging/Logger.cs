﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using log4net;
using log4net.Config;

namespace Spark.UnifiedPurchaseSystem.Lib.Logging
{
    public class Logger
    {
        ILog log;

        public Logger(Type type)
        {
            log = LogManager.GetLogger(type);
            string loggerFile = LoggerConfig.LoggerConfigFile;
            System.IO.FileInfo configFile = new System.IO.FileInfo(loggerFile);

            XmlConfigurator.ConfigureAndWatch(configFile);
        }

        public void Debug(object message)
        {
            log.Debug(message);
        }

        public void Debug(object message, Exception ex)
        {
            log.Debug(message, ex);
        }

        public void Error(object message)
        {
            log.Error(message);
        }

        public void Error(object message, Exception ex)
        {
            log.Error(message, ex);
        }

        public void Fatal(object message)
        {
            log.Fatal(message);
        }

        public void Fatal(object message, Exception ex)
        {
            log.Fatal(message, ex);
        }
    }
}
