﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.UnifiedPurchaseSystem.Lib.Logging
{
    public class LoggerConfig
    {
        public static string LoggerConfigFile
        {
            get
            {
                string configFile = string.Empty;

                /*
                try
                {
                    SettingDAL provider = new SettingDAL();

                    System.IO.FileInfo file = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    configFile = file.DirectoryName + provider.GetSetting(SettingType.LoggerConfigFile);
                }
                catch (Exception)
                {

                }
                */
                
                if (string.IsNullOrEmpty(configFile))
                {
                    System.IO.FileInfo file = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    configFile = file.DirectoryName + @"\Configuration\Logger.config";
                }
                return configFile;
            }
        }
    }
}
