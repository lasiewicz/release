using System;
using System.Collections;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Web;
using System.Messaging;
using System.Net.Http;
using Matchnet.Exceptions;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;


namespace Matchnet.CacheSynchronization
{
	/// <summary>
	/// 
	/// </summary>
	public class Synchronizer
	{
		private string _serviceName;
		private bool _runnable;
		private string _queuePath;
		private Thread[] _threads;
		private const int ThreadCount = 6;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceName"></param>
		public Synchronizer(string serviceName)
		{
			_serviceName = serviceName;
			_queuePath = @".\private$\Sync_" + serviceName.Replace(".", "");
		}

	    /// <summary>
	    /// </summary>
	    public void Start()
	    {
	        _runnable = true;
	        _threads = new Thread[ThreadCount];

	        for (var i = 0; i < _threads.Length; i++)
	        {
	            var t = new Thread(workerCycle);
	            _threads[i] = t;
	            t.Start();
	        }
	    }


		/// <summary>
		/// 
		/// </summary>
		public void Stop()
		{
			_runnable = false;
			if (_threads != null)
			{
				for (Int32 i = 0; i < _threads.Length; i++)
				{
					_threads[i].Join();
				}
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <param name="cacheReferences"></param>
		public void Enqueue(string key,
			Hashtable cacheReferences)
		{
			IDictionaryEnumerator de = cacheReferences.GetEnumerator();

			while (de.MoveNext())
			{
				string hostName = de.Key.ToString();
				CacheReference cacheReference = (CacheReference)de.Value;

				if (cacheReference.ExpirationDate > DateTime.Now)
				{
					MessageQueue queue = new MessageQueue(_queuePath);
					queue.Formatter = new BinaryMessageFormatter();
					queue.Send(new QueueItem(key, hostName, cacheReference));
					queue.Dispose();
				}
			}
		}

	    private void workerCycle()
	    {
	        MessageQueue queue = null;
	        var timeout = new TimeSpan(0, 0, 1);

	        if (!MessageQueue.Exists(_queuePath))
	        {
	            queue = MessageQueue.Create(_queuePath);
	            queue.MaximumQueueSize = 20480;
	        }
	        else
	        {
	            queue = new MessageQueue(_queuePath);
	        }

	        queue.Formatter = new BinaryMessageFormatter();

	        while (_runnable)
	        {
	            try
	            {
	                QueueItem queueItem = null;

	                try
	                {
	                    var message = queue.Receive(timeout);
	                    if (message != null)
	                    {
	                        queueItem = message.Body as QueueItem;
	                        message.Dispose();
	                    }
	                }
	                catch (MessageQueueException mEx)
	                {
	                    if (mEx.Message != "Timeout for the requested operation has expired.")
	                    {
	                        throw mEx;
	                    }
	                }

	                if (queueItem == null) continue;
	                switch (queueItem.Protocol)
	                {
	                    case ProtocolType.TcpRemoting:
	                        invalidateRemoting(queueItem.GetUri(), queueItem.Key);
	                        break;

	                    case ProtocolType.Http:
	                    case ProtocolType.Https:
	                        InvalidateHttp(queueItem.GetUri(), queueItem.Key);
	                        break;
	                }
	            }
	            catch (Exception ex)
	            {
	                new ServiceBoundaryException(_serviceName, "Error processing cache synchronization queue.", ex);
	                Thread.Sleep(5000);
	            }
	        }
	    }


		private void invalidateRemoting(string uri, string key)
		{
			try
			{
				((ICacheManagerService)Activator.GetObject(typeof(ICacheManagerService), uri)).Remove(key);
			}
			catch (Exception ex)
			{
				System.Diagnostics.EventLog.WriteEntry(_serviceName,
					"Error invalidating cache item (uri: "  + uri + ", key: " + key + ")\n\n" + ex.ToString(),
					System.Diagnostics.EventLogEntryType.Warning);
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="key"></param>
	    public void InvalidateHttp(string uri, string key)
	    {
	        uri = uri + "?DeleteKey=" + HttpUtility.UrlEncode(key);

	        if (uri.StartsWith("https"))
	        {
	            //ignore invalid/expired SSL cert
	            ServicePointManager.CertificatePolicy = new OpenCertificatePolicy();
	        }

	        try
	        {
	            using (var client = new HttpClient())
	            {
                    // having 2 seconds was causing some calls to API servers time out
	                client.Timeout = new TimeSpan(0, 0, 5);
	                // by calling .Result you are performing a synchronous call
	                var response = client.GetAsync(uri).Result;

	                if (response.IsSuccessStatusCode) return;

	                var message = string.Format("Error invalidating cache item {0}\n\nStatus Code: {1}", uri,
	                    response.StatusCode);
	                System.Diagnostics.EventLog.WriteEntry(_serviceName, message,
	                    System.Diagnostics.EventLogEntryType.Warning);
	            }
	        }
	        catch (Exception ex)
	        {
	            var message = string.Format("Error invalidating cache item {0}\n\n{1}", uri, ex);
	            System.Diagnostics.EventLog.WriteEntry(_serviceName, message, System.Diagnostics.EventLogEntryType.Warning);
	        }
	    }


		public class OpenCertificatePolicy : System.Net.ICertificatePolicy 
		{
			public bool CheckValidationResult(ServicePoint srvPoint,
				X509Certificate certificate, WebRequest request, int
				certificateProblem) 
			{
				return true;
			}
		}
	}
}
