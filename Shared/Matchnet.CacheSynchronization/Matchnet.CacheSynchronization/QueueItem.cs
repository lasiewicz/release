using System;

using Matchnet.CacheSynchronization.ValueObjects;

namespace Matchnet.CacheSynchronization
{
	[Serializable]
	internal class QueueItem
	{
		private string _key;
		private string _hostName;
		private CacheReference _cacheReference;

		public QueueItem(string key,
			string hostName,
			CacheReference cacheReference)
		{
			_key = key;
			_hostName = hostName;
			_cacheReference = cacheReference;
		}


		public ProtocolType Protocol
		{
			get
			{
				return _cacheReference.Protocol;
			}
		}


		public string Key
		{
			get
			{
				return _key;
			}
		}

		
		public string GetUri()
		{
			return _cacheReference.GetUri();
		}
	}
}
