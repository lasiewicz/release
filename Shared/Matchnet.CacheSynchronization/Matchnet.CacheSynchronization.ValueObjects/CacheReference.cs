using System;

namespace Matchnet.CacheSynchronization.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public enum ProtocolType
	{
		/// <summary>
		/// 
		/// </summary>
		TcpRemoting,
		/// <summary>
		/// 
		/// </summary>
		Http,
		/// <summary>
		/// 
		/// </summary>
		Https
	};

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class CacheReference : ICloneable
	{
		private string _address;
		private Int32 _port;
		private ProtocolType _protocol;
		private DateTime _expirationDate;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="address"></param>
		/// <param name="protocol"></param>
		/// <param name="port"></param>
		/// <param name="expirationDate"></param>
		public CacheReference(string address,
			Int32 port,
			ProtocolType protocol,
			DateTime expirationDate)
		{
			_address = address;
			_port = port;
			_protocol = protocol;
			_expirationDate = expirationDate;
		}


		/// <summary>
		/// 
		/// </summary>
		public string Address
		{
			get
			{
				return _address;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 Port
		{
			get
			{
				return _port;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public ProtocolType Protocol
		{
			get
			{
				return _protocol;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public DateTime ExpirationDate
		{
			get
			{
				return _expirationDate;
			}
			set
			{
				_expirationDate = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetUri()
		{
			string uri = _address + ":" + _port.ToString();

			switch (_protocol)
			{
				case ProtocolType.TcpRemoting:
					uri = "tcp://" + uri + "/CacheManagerSM.rem";
					break;

				case ProtocolType.Http:
				case ProtocolType.Https:
					uri = _protocol.ToString().ToLower() + "://" + uri + "/CacheManager.aspx";
					break;
			}

			return uri;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public object Clone()
		{
			return this.MemberwiseClone();
		}
	}
}
