using System;
using System.Collections;
using System.Threading;

using Matchnet;
using Matchnet.CacheSynchronization.ValueObjects;


namespace Matchnet.CacheSynchronization.Tracking
{
	/// <summary>
	/// 
	/// </summary>
	public class ReferenceTracker
	{
		private Hashtable _references;
		private ReaderWriterLock _lock;

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker()
		{
			_references = new Hashtable();
			_lock = new ReaderWriterLock();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="hostName"></param>
		/// <param name="reference"></param>
		public void Add(string hostName,
			CacheReference reference)
		{
			hostName = hostName.ToLower();

			_lock.AcquireWriterLock(-1);
			try
			{
				_references[hostName] = reference;
			}
			finally
			{
				_lock.ReleaseLock();
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="hostName"></param>
		public void Remove(string hostName)
		{
			hostName = hostName.ToLower();

			_lock.AcquireReaderLock(-1);
			try
			{
				if (_references.ContainsKey(hostName))
				{
					_lock.UpgradeToWriterLock(-1);
					_references.Remove(hostName);
				}
			}
			finally
			{
				_lock.ReleaseLock();
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="excludeHostName"></param>
		/// <returns></returns>
		public Hashtable Purge(string excludeHostName)
		{
			_lock.AcquireWriterLock(-1);
			try
			{
				CacheReference reference = null;

				if (excludeHostName != Constants.NULL_STRING)
				{
					excludeHostName = excludeHostName.ToLower();

					reference = _references[excludeHostName] as CacheReference;

					if (reference != null)
					{
						_references.Remove(excludeHostName);
					}
				}

				Hashtable result = _references.Clone() as Hashtable;
				_references.Clear();

				if (reference != null)
				{
					_references.Add(excludeHostName, reference);
				}

				/*
				System.Diagnostics.Trace.Write("___purge");
				IDictionaryEnumerator de = result.GetEnumerator();
				while (de.MoveNext())
				{
					System.Diagnostics.Trace.Write(" " + ((CacheReference)de.Value).Address);
				}
				System.Diagnostics.Trace.Write("\n");
				*/

				return result;
			}
			finally
			{
				_lock.ReleaseLock();
			}
		}
	}
}
