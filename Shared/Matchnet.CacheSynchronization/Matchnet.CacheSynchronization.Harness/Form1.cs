using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Web;
using System.Net;
using System.Reflection;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;


namespace Matchnet.CacheSynchronization.Harness
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
        private Button button2;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(109, 125);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "invalidateHttp";
            this.button2.Click += new System.EventHandler(this.InvalidateHttpClick);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			/*
			try
			{
				removeRemoting("172.16.1.101", 47000, "Member1380077");
				MessageBox.Show("done");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			*/

			//sub plans
			/*
			removeRemoting("lasvccontent01.sparkmt.com", 45300, "~PLANCOLLECTION^1001");
			removeRemoting("lasvccontent02.sparkmt.com", 45300, "~PLANCOLLECTION^1001");
			removeRemoting("lasvccontent01.sparkmt.com", 45300, "~PLANCOLLECTION^1003");
			removeRemoting("lasvccontent02.sparkmt.com", 45300, "~PLANCOLLECTION^1003");
			removeRemoting("lasvccontent01.sparkmt.com", 45300, "~PLANCOLLECTION^1013");
			removeRemoting("lasvccontent02.sparkmt.com", 45300, "~PLANCOLLECTION^1013");
			removeRemoting("lasvccontent01.sparkmt.com", 45300, "~PLANCOLLECTION^1015");
			removeRemoting("lasvccontent02.sparkmt.com", 45300, "~PLANCOLLECTION^1015");
			*/

			//attribute metadata
			/*
			removeRemoting("172.16.101.71", 41000, "AttributeMetadata");
			removeRemoting("172.16.101.72", 41000, "AttributeMetadata");
			removeHttp("laweb01.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb02.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb03.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb04.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb05.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb06.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb07.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb08.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb09.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb10.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb11.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb12.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb13.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb14.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb15.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb16.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb17.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb18.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb19.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb20.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb21.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb22.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb23.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb24.la.sparkwww.com", 80, "AttributeMetadata");
			removeHttp("laweb25.la.sparkwww.com", 80, "AttributeMetadata");
			*/

			//privilege collection
			removeRemoting("172.16.101.1", 42000, "^PC~");
			removeRemoting("172.16.101.2", 42000, "^PC~");
			removeRemoting("172.16.101.3", 42000, "^PC~");
			removeRemoting("172.16.101.4", 42000, "^PC~");
			removeHttp("64.16.81.101", 80, "^PC~");
			removeHttp("64.16.81.102", 80, "^PC~");
			removeHttp("64.16.81.103", 80, "^PC~");
			removeHttp("64.16.81.104", 80, "^PC~");

			MessageBox.Show("done");
		}

		private void removeRemoting(string host,
			Int32 port,
			string key)
		{
			((ICacheManagerService)Activator.GetObject(typeof(ICacheManagerService), "tcp://" + host + ":" + port.ToString() + "/CacheManagerSM.rem")).Remove(key);
		}

		private void removeHttp(string host,
			Int32 port,
			string key)
		{
			string uri = "http://" + host + "/CacheManager.aspx?DeleteKey=" + key; 
			Console.WriteLine(uri);
			WebRequest.Create(uri).GetResponse().Close();
		}

        /// <summary>
        ///     Check the event viewer for messages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InvalidateHttpClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var synchronizer = new Synchronizer("blah");
                synchronizer.InvalidateHttp(
                    @"http://api.stage3.spark.net/CacheManager.aspx",
                    @"%7eApi%5eApp%5e1014%5e152015026");
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error - " + exception);
            }

            Cursor.Current = Cursors.Default;
        }
	}
}
