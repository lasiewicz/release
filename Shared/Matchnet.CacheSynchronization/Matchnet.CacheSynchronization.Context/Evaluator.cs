using System;
using System.Net;
using System.Net.Sockets;
using System.Web;

using Matchnet.CacheSynchronization.ValueObjects;
using ProtocolType = Matchnet.CacheSynchronization.ValueObjects.ProtocolType;


namespace Matchnet.CacheSynchronization.Context
{
	/// <summary>
	/// 
	/// </summary>
	public class Evaluator
	{
		/// <summary>
		/// 
		/// </summary>
		public static readonly Evaluator Instance = new Evaluator();

		private CacheReference _reference;

		private Evaluator()
		{
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="port"></param>
		public void SetRemotingPort(Int32 port)
		{
			_reference = new CacheReference(Dns.GetHostEntry(Dns.GetHostName()).HostName,
				port,
				ProtocolType.TcpRemoting,
				DateTime.MaxValue);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="expirationDate"></param>
		/// <returns></returns>
		public CacheReference GetReference(DateTime expirationDate)
		{


			if (_reference == null)
			{
				var context = HttpContext.Current;

				if (context != null)
				{
                    // This is returning IPv6 when using .NET4 on Windows 2008 servers in stage3.
					//string address = context.Request.ServerVariables["LOCAL_ADDR"];
                    // This new call will ensure that an IPv4 address is returned.
                    // This problem was uncovered when Spark.REST was upgraded.
                    var address = Array.FindLast(
                        Dns.GetHostEntry(string.Empty).AddressList,
                        a => a.AddressFamily == AddressFamily.InterNetwork).ToString();

					if (address == "127.0.0.1")
					{
						address = Dns.GetHostEntry(Dns.GetHostName()).HostName;
					}

					_reference = new CacheReference(address,
						Convert.ToInt32(context.Request.ServerVariables["SERVER_PORT"]),
						context.Request.IsSecureConnection ? ProtocolType.Https : ProtocolType.Http,
						DateTime.MinValue);
				}
			}
			else
			{
				var reference = (CacheReference)_reference.Clone();
				reference.ExpirationDate = expirationDate;
				return reference;
			}

			return null;
		}
	}
}
