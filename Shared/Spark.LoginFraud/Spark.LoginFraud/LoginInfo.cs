﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.LoginFraud
{
    internal class LoginInfo
    {
        public string emailAddress { get; set; }
        public string ipAddress { get; set; }
        public string userAgent { get; set; }
        public string httpHeaders { get; set; }
        public string appID { get; set; }
    }
}
