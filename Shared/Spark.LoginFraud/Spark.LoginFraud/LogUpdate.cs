﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.LoginFraud
{
    public class LogUpdate
    {
        public int logID { get; set; }
        public string emailAddress { get; set; }
        public int memberid { get; set; }
        public int loginStatus { get; set; }
    }
}
