﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.LoginFraud
{
    internal class ResponseWrapper<T>
    {
        public int Code { get; set; }
        public T Data { get; set; }
        public string Status { get; set; }
    }
}
