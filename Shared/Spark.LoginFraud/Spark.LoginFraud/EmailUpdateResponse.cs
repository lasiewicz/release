﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.LoginFraud
{
    public class EmailUpdateResponse: ResponseBase
    {
        public bool Success { get; set; }
    }
}
