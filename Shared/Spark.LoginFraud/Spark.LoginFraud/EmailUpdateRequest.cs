﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.LoginFraud
{
    public class EmailUpdateRequest
    {
        public string newEmailAddress { get; set; }
        public int memberid { get; set; }
    }
}
