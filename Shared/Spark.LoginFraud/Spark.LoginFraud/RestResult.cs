﻿namespace Spark.LoginFraud
{
    /// <summary>
    /// </summary>
    internal class RestResult<T>
    {
        public T ReturnObject { get; set; }
        public RestStatus RestStatus { get; set; }
    }
}