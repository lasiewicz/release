﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.LoginFraud
{
    internal class InternalFraudCheckResponse: InternalResponseBase
    {
        public InternalResponseDetail Return { get; set; }

        public class InternalResponseDetail
        {
            public int StepId { get; set; }
            public FraudActionType ActionId { get; set; }
            public int LogId { get; set; }
        }
    }
}
