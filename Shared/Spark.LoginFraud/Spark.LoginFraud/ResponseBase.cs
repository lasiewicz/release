﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Spark.LoginFraud
{
    public class ResponseBase
    {
        public bool ValidResult { get; set; }
        public string ErrorMessage { get; set; }
        public bool TimedOut { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
