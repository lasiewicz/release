﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Spark.LoginFraud
{
    internal class RestStatus
    {
        public bool ValidResponse { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public bool TimedOut { get; set; }
    }
}
