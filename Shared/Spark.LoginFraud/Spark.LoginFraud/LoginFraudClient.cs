﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Managers.Managers;
using log4net;
using log4net.Config;

namespace Spark.LoginFraud
{
    public class LoginFraudClient
    {
        private SettingsManager _settingsManager;
        private Brand _brand;
        private string _mingleApiHost;
        private int _timeout;
        private int _callingSystemId;
        private bool _logTiming;

        private static readonly ILog Log = LogManager.GetLogger(typeof(LoginFraudClient));

        static LoginFraudClient()
        {
            XmlConfigurator.Configure();
        }

        public LoginFraudClient(Brand brand, int callingSystemId)
        {
            _brand = brand;
            _callingSystemId = callingSystemId;
            _mingleApiHost = SettingsManager.GetSettingString(SettingConstants.MINGLE_LOGIN_FRAUD_HOST, brand);
            _timeout = SettingsManager.GetSettingInt(SettingConstants.MINGLE_LOGIN_FRAUD_TIMEOUT, brand);
            _logTiming = SettingsManager.GetSettingBool(SettingConstants.LOG_LOGIN_TIMING_DATA, brand);
        }

        public SettingsManager SettingsManager 
        {
            get { return _settingsManager ?? (_settingsManager = new SettingsManager()); }
            set { _settingsManager = value; } 
        }

        public async Task<FraudCheckResponse> CheckFraudAsync(string emailAddress, string ipAddress, string userAgent, string httpHeaders, int appId)
        {
            Log.DebugFormat("CheckFraud entered. EmailAddress: {0} IpAddress: {1} AppId: {2} CallingSystemId {3}", emailAddress, ipAddress, appId, _callingSystemId);

            var urlPath = string.Format(SettingsManager.GetSettingString(SettingConstants.MINGLE_LOGIN_FRAUD_CHECK_PATH, _brand), _callingSystemId);

            var loginInfo = new LoginInfo
            {
                emailAddress = emailAddress,
                ipAddress = ipAddress,
                appID = appId.ToString(),
                httpHeaders = httpHeaders,
                userAgent = userAgent
            };

            var restClient = new BaseRestClient {RequestTimeout = _timeout};
            var restResult = await restClient.MakeRequestAsync<InternalFraudCheckResponse>(_mingleApiHost, urlPath,
                false, true,
                loginInfo, _logTiming);

            var status = restResult.RestStatus;
            var internalResponse = restResult.ReturnObject;

            var response = new FraudCheckResponse { ValidResult = status.ValidResponse && internalResponse != null && internalResponse.Result == 1 };

            if (response.ValidResult && internalResponse != null)
            {
                response.ActionId = internalResponse.Return.ActionId;
                response.LogId = internalResponse.Return.LogId;
                response.StepId = internalResponse.Return.StepId;
                Log.DebugFormat("CheckFraud success. EmailAddress: {0} ActionId: {1} StepId: {2} LogId {3}", emailAddress, response.ActionId, response.LogId, response.StepId);
            }
            else
            {
                if (!string.IsNullOrEmpty(status.ErrorMessage))
                {
                    response.ErrorMessage = status.ErrorMessage;
                }
                else if (internalResponse != null && !string.IsNullOrEmpty(internalResponse.Error_msgs))
                {
                    response.ErrorMessage = internalResponse.Error_msgs;
                }
                else
                {
                    response.ErrorMessage = "Unknown error";
                }
                Log.DebugFormat("CheckFraud failure. EmailAddress: {0} StatusCode: {1} TimedOut: {2} ErrorMessage: {3}", emailAddress, response.StatusCode, response.TimedOut, response.ErrorMessage);
            }

            return response;
        }

        public FraudCheckResponse CheckFraud(string emailAddress, string ipAddress, string userAgent, string httpHeaders, int appId)
        {
            Log.DebugFormat("CheckFraud entered. EmailAddress: {0} IpAddress: {1} AppId: {2} CallingSystemId {3}", emailAddress, ipAddress, appId, _callingSystemId);

            var urlPath = string.Format(SettingsManager.GetSettingString(SettingConstants.MINGLE_LOGIN_FRAUD_CHECK_PATH, _brand), _callingSystemId);
            
            var loginInfo = new LoginInfo
                                 {
                                     emailAddress = emailAddress,
                                     ipAddress = ipAddress,
                                     appID = appId.ToString(),
                                     httpHeaders = httpHeaders,
                                     userAgent = userAgent
                                 };
            
            
            InternalFraudCheckResponse internalResponse;
            var restClient = new BaseRestClient { RequestTimeout = _timeout };
            var status = restClient.MakeRequest(_mingleApiHost, urlPath, false, true, loginInfo, _logTiming, out internalResponse);

            var response = new FraudCheckResponse {ValidResult = status.ValidResponse && internalResponse != null && internalResponse.Result == 1};

            if (response.ValidResult && internalResponse != null)
            {
                response.ActionId = internalResponse.Return.ActionId;
                response.LogId = internalResponse.Return.LogId;
                response.StepId = internalResponse.Return.StepId;
                Log.DebugFormat("CheckFraud success. EmailAddress: {0} ActionId: {1} StepId: {2} LogId {3}", emailAddress, response.ActionId, response.LogId, response.StepId);
            }
            else
            {
                if (!string.IsNullOrEmpty(status.ErrorMessage))
                {
                    response.ErrorMessage = status.ErrorMessage;
                }
                else if(internalResponse != null && !string.IsNullOrEmpty(internalResponse.Error_msgs))
                {
                    response.ErrorMessage =  internalResponse.Error_msgs;
                }
                else
                {
                    response.ErrorMessage = "Unknown error";
                }
                Log.DebugFormat("CheckFraud failure. EmailAddress: {0} StatusCode: {1} TimedOut: {2} ErrorMessage: {3}", emailAddress, response.StatusCode, response.TimedOut, response.ErrorMessage);
            }

            return response;
        }

        public UpdateLogResponse UpdateLog(int logId, string emailAddress, int memberId, LoginStatus loginStatus)
        {
            Log.DebugFormat("UpdateLog entered. EmailAddress: {0} logId: {1} memberId: {2} loginStatus: {3} callingSystemId: {4}", emailAddress, logId, memberId, loginStatus, _callingSystemId);

            var urlPath = string.Format(SettingsManager.GetSettingString(SettingConstants.MINGLE_LOGIN_FRAUD_LOG_UPDATE_PATH, _brand), _callingSystemId);

            var logUpdate = new LogUpdate
                                {
                                    emailAddress = emailAddress,
                                    logID = logId,
                                    memberid = memberId,
                                    loginStatus = (int)loginStatus
                                };


            InternalUpdateLogResponse internalResponse;
            var restClient = new BaseRestClient { RequestTimeout = _timeout };
            var status = restClient.MakeRequest(_mingleApiHost, urlPath, false, true, logUpdate, _logTiming, out internalResponse);

            var response = new UpdateLogResponse { ValidResult = status.ValidResponse && internalResponse != null && internalResponse.Result == 1 };

            if (response.ValidResult && internalResponse != null)
            {
                response.Success = (internalResponse.Result == 1);
                Log.DebugFormat("UpdateLog success. EmailAddress: {0} Sucess: {1} ", emailAddress, response.Success);
            }
            else
            {
                if (!string.IsNullOrEmpty(status.ErrorMessage))
                {
                    response.ErrorMessage = status.ErrorMessage;
                }
                else if (internalResponse != null && !string.IsNullOrEmpty(internalResponse.Error_msgs))
                {
                    response.ErrorMessage = internalResponse.Error_msgs;
                }
                else
                {
                    response.ErrorMessage = "Unknown error";
                }
                Log.DebugFormat("UpdateLog failure. EmailAddress: {0}  StatusCode: {1} TimedOut: {2} ErrorMessage: {3}", emailAddress, response.StatusCode, response.TimedOut, response.ErrorMessage);
            }

            return response;
        }

        public LogActionResponse LogAction(string emailAddress, LogActionType actionType, string ipAddress, string userAgent, string httpHeaders, int memberId, int appId, bool throttled, bool loginSuccessful)
        {
            Log.DebugFormat("LogAction entered. EmailAddress: {0} actionType: {1} ipAddress: {2} memberId: {3} appId: {4} loginSuccessful: {5} callingSystemId: {6}",
                emailAddress, actionType, ipAddress, memberId, appId, loginSuccessful, _callingSystemId);

            var urlPath = string.Format(SettingsManager.GetSettingString(SettingConstants.MINGLE_LOGIN_FRAUD_LOG_ACTION_PATH, _brand), _callingSystemId);

            var logRequest = new LogRequest
            {
                emailAddress = emailAddress,
                action = LogActionTypeToString(actionType),
                ipAddress = ipAddress, 
                httpHeaders = httpHeaders,
                memberID = memberId,
                throttled = throttled,
                loginSuccessful =  loginSuccessful, 
                appID = appId
            };
            
            InternalLogActionResponse internalResponse;
            var restClient = new BaseRestClient { RequestTimeout = _timeout };
            var status = restClient.MakeRequest(_mingleApiHost, urlPath, false, true, logRequest, _logTiming, out internalResponse);

            var response = new LogActionResponse { ValidResult = status.ValidResponse && internalResponse != null && internalResponse.Result == 1 };

            if (response.ValidResult && internalResponse != null)
            {
                response.Success = (internalResponse.Result == 1);
                Log.DebugFormat("LogAction success. EmailAddress: {0} Success: {1} ", emailAddress, response.Success);
            }
            else
            {
                if (!string.IsNullOrEmpty(status.ErrorMessage))
                {
                    response.ErrorMessage = status.ErrorMessage;
                }
                else if (internalResponse != null && !string.IsNullOrEmpty(internalResponse.Error_msgs))
                {
                    response.ErrorMessage = internalResponse.Error_msgs;
                }
                else
                {
                    response.ErrorMessage = "Unknown error";
                }
                Log.DebugFormat("LogAction failure. EmailAddress: {0} StatusCode: {1} TimedOut: {2} ErrorMessage: {3}", emailAddress, response.StatusCode, response.TimedOut, response.ErrorMessage);
            }

            return response;
        }

        public EmailUpdateResponse UpdateEmailAddress(int memberId, string newEmail)
        {
            Log.DebugFormat("UpdateEmailAddress entered. MemberId: {0} NewEmailAddress: {1}", memberId, newEmail);

            var urlPath = string.Format(SettingsManager.GetSettingString(SettingConstants.MINGLE_LOGIN_FRAUD_CHANGE_EMAIL_PATH, _brand), _callingSystemId);

            var emailUpdateRequest = new EmailUpdateRequest()
            {
                newEmailAddress = newEmail,
                memberid = memberId
            };

            InternalEmailChangeResponse internalResponse;
            var restClient = new BaseRestClient { RequestTimeout = _timeout };
            var status = restClient.MakeRequest(_mingleApiHost, urlPath, false, true, emailUpdateRequest, _logTiming, out internalResponse);

            var response = new EmailUpdateResponse() { ValidResult = status.ValidResponse && internalResponse != null && internalResponse.Result == 1 };

            if (response.ValidResult && internalResponse != null)
            {
                response.Success = (internalResponse.Result == 1);
                Log.DebugFormat("UpdateEmailAddress success. MemberId: {0} NewEmailAddress: {1}", memberId, newEmail);
            }
            else
            {
                if (!string.IsNullOrEmpty(status.ErrorMessage))
                {
                    response.ErrorMessage = status.ErrorMessage;
                }
                else if (internalResponse != null && !string.IsNullOrEmpty(internalResponse.Error_msgs))
                {
                    response.ErrorMessage = internalResponse.Error_msgs;
                }
                else
                {
                    response.ErrorMessage = "Unknown error";
                }
                Log.DebugFormat("UpdateEmailAddress failure. MemberId: {0} NewEmailAddress: {1} StatusCode: {2} TimedOut: {3} ErrorMessage: {4}", memberId, newEmail, response.StatusCode, response.TimedOut, response.ErrorMessage);
            }

            return response;
        }

        private static string LogActionTypeToString(LogActionType actionType)
        {
            string returnType = string.Empty;
            
            switch(actionType)
            {
                case LogActionType.Registration:
                    returnType = "registation";
                    break;
                case LogActionType.ChangedPassword:
                    returnType = "changedPassword";
                    break;
                case LogActionType.LoginAttempt:
                    returnType = "loginAttempt";
                    break;
                case LogActionType.ResetPassword:
                    returnType = "resetPassword";
                    break;
                case LogActionType.ResetPasswordRequest:
                    returnType = "resetPasswordRequest";
                    break;
                case LogActionType.ChangedEmail:
                    returnType = "changedEmail";
                    break;
                case LogActionType.AutoLogin:
                    returnType = "autoLogin";
                    break;
            }

            return returnType;
        }
    }
}
