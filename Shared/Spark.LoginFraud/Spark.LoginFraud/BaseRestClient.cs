﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using RestSharp;
using Spark.Managers.Managers;
using log4net;

namespace Spark.LoginFraud
{
    internal class BaseRestClient
    {
        public bool UseResponseWrapper { get; set; }
        public int RequestTimeout { get; set; }

        private static readonly ILog Log = LogManager.GetLogger(typeof(BaseRestClient));

        public RestStatus MakeRequest<T>(string host, string requestPath, bool secureRequest, bool post, object jsonParameter, bool logTiming, out T returnObject) where T : new()
        {
            returnObject = default(T);
            
            if(string.IsNullOrEmpty(host))
            {
                throw new ArgumentNullException("Host can not be empty");
            }
            if (string.IsNullOrEmpty(requestPath))
            {
                throw new ArgumentNullException("RequestPath can not be empty");
            }
            
            var client = new RestClient(host);

            if(RequestTimeout != 0)
            {
                client.Timeout = RequestTimeout;
                client.ReadWriteTimeout = RequestTimeout;
            }

            var request = new RestRequest(requestPath, post ? Method.POST : Method.GET)
                              {RequestFormat = DataFormat.Json};

            if (post && jsonParameter != null)
            {
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddParameter("application/json", request.JsonSerializer.Serialize(jsonParameter), ParameterType.RequestBody);
            }

            var errorMessage = string.Empty;
            var status = HttpStatusCode.OK;
            var timedOut = false;
            var responseStatus = ResponseStatus.None;

            try
            {
                Stopwatch stopwatch =null;
                
                if (UseResponseWrapper)
                {
                    if (logTiming)
                    {
                        stopwatch = new Stopwatch();
                        stopwatch.Start();
                    }
                    
                    var response = client.Execute<ResponseWrapper<T>>(request) as RestResponse<ResponseWrapper<T>>;

                    if (logTiming)
                    {
                        stopwatch.Stop();
                        Log.DebugFormat("FraudCheck Call Completed URL: {0} ElapsedMilliseconds:{1} ", requestPath, stopwatch.ElapsedMilliseconds);
                    }

                    if (response != null && response.Data.Data != null)
                    {
                        returnObject = response.Data.Data;
                        return new RestStatus { ValidResponse = true, StatusCode = response.StatusCode };
                    }
                    if (response != null && !string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        errorMessage = response.ErrorMessage;
                    }
                }
                else
                {
                    if (logTiming)
                    {
                        stopwatch = new Stopwatch();
                        stopwatch.Start();
                    }
                    var response = client.Execute<T>(request) as RestResponse<T>;
                    if (logTiming)
                    {
                        stopwatch.Stop();
                        Log.DebugFormat("FraudCheck Call Completed URL: {0} ElapsedMilliseconds:{1} ", requestPath, stopwatch.ElapsedMilliseconds);
                    }

                    if(response != null)
                    {
                        status = response.StatusCode;
                        responseStatus = response.ResponseStatus;
                    }

                    if (response != null && response.Data != null)
                    {
                        returnObject = response.Data;
                        return new RestStatus { ValidResponse = true, StatusCode = response.StatusCode };
                    }
                    if (response!= null && !string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        errorMessage = response.ErrorMessage;
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                errorMessage = ex.Message;
            }

            if((errorMessage != string.Empty && errorMessage.ToLower().IndexOf("timeout") > -1) || responseStatus == ResponseStatus.TimedOut)
            {
                timedOut = true;
            }

            return new RestStatus { ValidResponse = false, ErrorMessage = errorMessage, StatusCode=status, TimedOut=timedOut};
        }

        public async Task<RestResult<T>> MakeRequestAsync<T>(string host, string requestPath, bool secureRequest, bool post, object jsonParameter, bool logTiming) where T : new()
        {
            var restResult = new RestResult<T>();

            if (string.IsNullOrEmpty(host))
            {
                throw new ArgumentNullException("host");
            }
            if (string.IsNullOrEmpty(requestPath))
            {
                throw new ArgumentNullException("requestPath");
            }

            var client = new RestClient(host);

            if (RequestTimeout != 0)
            {
                client.Timeout = RequestTimeout;
                client.ReadWriteTimeout = RequestTimeout;
            }

            var request = new RestRequest(requestPath, post ? Method.POST : Method.GET) { RequestFormat = DataFormat.Json };
            var cancellationTokenSource = new CancellationToken();

            if (post && jsonParameter != null)
            {
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddParameter("application/json", request.JsonSerializer.Serialize(jsonParameter), ParameterType.RequestBody);
            }

            var errorMessage = string.Empty;
            var status = HttpStatusCode.OK;
            var timedOut = false;
            var responseStatus = ResponseStatus.None;

            try
            {
                Stopwatch stopwatch = null;

                if (UseResponseWrapper)
                {
                    if (logTiming)
                    {
                        stopwatch = new Stopwatch();
                        stopwatch.Start();
                    }

                    var response = await client.ExecuteTaskAsync<ResponseWrapper<T>>(request, cancellationTokenSource) 
                        as RestResponse<ResponseWrapper<T>>;

                    if (logTiming)
                    {
                        stopwatch.Stop();
                        Log.DebugFormat("FraudCheck Call Completed URL: {0} ElapsedMilliseconds:{1} ", requestPath, stopwatch.ElapsedMilliseconds);
                    }

                    if (response != null && response.Data.Data != null)
                    {
                        restResult.ReturnObject = response.Data.Data;
                        restResult.RestStatus = new RestStatus { ValidResponse = true, StatusCode = response.StatusCode };
                        return restResult;
                    }
                    if (response != null && !string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        errorMessage = response.ErrorMessage;
                    }
                }
                else
                {
                    if (logTiming)
                    {
                        stopwatch = new Stopwatch();
                        stopwatch.Start();
                    }
                    var response = client.Execute<T>(request) as RestResponse<T>;
                    if (logTiming)
                    {
                        stopwatch.Stop();
                        Log.DebugFormat("FraudCheck Call Completed URL: {0} ElapsedMilliseconds:{1} ", requestPath, stopwatch.ElapsedMilliseconds);
                    }

                    if (response != null)
                    {
                        status = response.StatusCode;
                        responseStatus = response.ResponseStatus;
                    }

                    if (response != null && response.Data != null)
                    {
                        restResult.ReturnObject = response.Data;
                        restResult.RestStatus = new RestStatus { ValidResponse = true, StatusCode = response.StatusCode };
                        return restResult;
                    }
                    if (response != null && !string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        errorMessage = response.ErrorMessage;
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                errorMessage = ex.Message;
            }

            if ((errorMessage != string.Empty && errorMessage.ToLower().IndexOf("timeout", StringComparison.Ordinal) > -1) || responseStatus == ResponseStatus.TimedOut)
            {
                timedOut = true;
            }

            restResult.RestStatus = new RestStatus { ValidResponse = false, ErrorMessage = errorMessage, StatusCode = status, TimedOut = timedOut };
            return restResult;
        }

    }
}
