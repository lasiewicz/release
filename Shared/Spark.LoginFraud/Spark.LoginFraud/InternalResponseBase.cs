﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.LoginFraud
{
    internal class InternalResponseBase
    {
        public int Result { get; set; }
        public string Error_msgs { get; set; }

        public InternalResponseBase()
        {
            Error_msgs = string.Empty;
        }
    }
}
