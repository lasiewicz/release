﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.LoginFraud
{
    public enum FraudActionType
    {
        NotFraud = 0,
        AccountLocked = 1,
        Fraud = 2
    }

    public enum LoginStatus
    {
        Success = 0,
        FailedBadEmail=1,
        FailedBadPassword=2,
        FailedSuspended=3,
        FailedIpThrottling=4
    }

    public enum LogActionType
    {
        LoginAttempt=0,
        Registration=1,
        ChangedPassword=2,
        ResetPasswordRequest=3,
        ResetPassword=4,
        ChangedEmail=5,
        AutoLogin=6
    }
}
