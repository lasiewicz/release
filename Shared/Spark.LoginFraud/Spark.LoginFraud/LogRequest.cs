﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.LoginFraud
{
    public class LogRequest
    {
        public string action { get; set; }
        public string emailAddress { get; set; }
        public string ipAddress { get; set; }
        public string httpHeaders { get; set; }
        public int memberID { get; set; }
        public bool throttled { get; set; }
        public bool loginSuccessful { get; set; }
        public int appID { get; set; }
    }
}
