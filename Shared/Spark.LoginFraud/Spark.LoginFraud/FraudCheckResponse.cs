﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.LoginFraud
{


    public class FraudCheckResponse : ResponseBase
    {
        public int StepId { get; set; }
        public FraudActionType ActionId { get; set; }
        public int LogId { get; set; }
    }
}
