﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.LoginFraud
{
    public class UpdateLogResponse : ResponseBase
    {
        public bool Success { get; set; }
    }
}
