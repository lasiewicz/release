﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;

using System.IO;

namespace Spark.WCFService
{
    [ServiceContract(Namespace = "http://spark")]
    public interface IWCFHealthCheckService
    {
       
        [OperationContract, WebGet]
        Stream HealthCheck();
    }
}
