﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net;
using System.Web;
using System.ServiceModel.Web;
using System.IO;
using Matchnet.Exceptions;
using Spark.Logging;
namespace Spark.WCFService
{
    // NOTE: If you change the class name "WCFHealthCheckSvc" here, you must also update the reference to "WCFHealthCheckSvc" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class WCFHealthCheckSvc : IWCFHealthCheckService
    {
        string MATCHNET_SERVER_DOWN = "Matchnet Server Down";
        string MATCHNET_SERVER_ENABLED = "Matchnet Server Enabled";
        string _serviceConstant = "";
        public Stream HealthCheck()
        {
            byte[] resultBytes;
            string host="";
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceConstant, "WCFHealthCheckSvc", "HealthCheck");
              
                bool isHealthy = Check();
                host= WebOperationContext.Current.IncomingRequest.Headers["Host"];
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstant, "WCFHealthCheckSvc", "Got host - ",host);
                if (isHealthy)
                {
                    resultBytes = Encoding.UTF8.GetBytes(MATCHNET_SERVER_ENABLED);
                    WebOperationContext.Current.OutgoingResponse.Headers.Add("Health", MATCHNET_SERVER_ENABLED);

                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstant, "WCFHealthCheckSvc", "Healthy - ", host);
                }
                else
                {
                    resultBytes = Encoding.UTF8.GetBytes(MATCHNET_SERVER_DOWN);
                    WebOperationContext.Current.OutgoingResponse.Headers.Add("Health", MATCHNET_SERVER_DOWN);
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstant, "WCFHealthCheckSvc", "Not very Healthy - ", host);
                }

                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                RollingFileLogger.Instance.LeaveMethod(ServiceConstant, "WCFHealthCheckSvc", "HealthCheck");
                return new MemoryStream(resultBytes);
            }
            catch (Exception ex)
            {
                try
                {
                    
                    resultBytes = Encoding.UTF8.GetBytes(MATCHNET_SERVER_DOWN);
                    WebOperationContext.Current.OutgoingResponse.Headers.Add("Health", MATCHNET_SERVER_DOWN);
                    return new MemoryStream(resultBytes);
                    SAException saEx = new SAException("Exception in healthcheck for:" + host, ex);
                    saEx = null;
                    RollingFileLogger.Instance.LogException(ServiceConstant, "WCFHealthCheckSvc", ex);
                }
                catch (Exception exceptionInException)
                {
                    SAException saEx = new SAException("Exception in exception block for healthcheck for:" + host, exceptionInException);
                    RollingFileLogger.Instance.LogException(ServiceConstant, "WCFHealthCheckSvc - 2nd exception", ex);
                    throw saEx;
                }

            }
        }


        
   


        public bool Check()
        {
            bool healthy = true;
            
            try
            {
                if (!isEnabled()) healthy = false;
                //else
                //{
                //    if (!String.IsNullOrEmpty(WebOperationContext.Current.IncomingRequest.Headers["Host"]))
                //    {
                //        WebRequest webRequest = WebRequest.Create("http://" + WebOperationContext.Current.IncomingRequest.Headers["Host"]);
                //        webRequest.Timeout = 2000;
                //        WebResponse webResponse = webRequest.GetResponse();
                //    }
                //}
            }
            catch (Exception ex)
            {
                healthy = false;
            }

            return healthy;
        }

        private bool isEnabled()
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "deploy.txt";

            bool fileExists = System.IO.File.Exists(filePath);

            if (!fileExists)
            {
                return true;
            }
            else
            {
                RollingFileLogger.Instance.LogWarningMessage(ServiceConstant, "WCFHealthCheckSvc","Deploy.txt!!!!!!",null);
            }
            return false;
        }
        private string ServiceConstant
        {
            get
            {
                try
                {
                    if(String.IsNullOrEmpty(_serviceConstant))
                    {
                    _serviceConstant = System.Configuration.ConfigurationManager.AppSettings.Get("HealthServiceConstant");
                    
                    }
                    return _serviceConstant;
                }
                catch (Exception ex)
                { return ""; }
            }
        }
    }
}
