﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Matchnet;
using System.Reflection;

namespace Spark.WCFReplication
{
 
    [ServiceContract(Namespace = "spark.net")]
    public interface IWCFReplicationRecipient
    {
        [OperationContract]
        void Receive(IReplicable replicableObject);
    }

    
}
