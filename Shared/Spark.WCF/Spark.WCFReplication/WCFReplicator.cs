﻿using System;
using System.Collections;
using System.Threading;
using System.Messaging;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using Spark.WCFClient;
using Spark.Logging;

namespace Spark.WCFReplication
{
    /// <summary>
    /// Performs synchronous queuing of object for replication and asynchronous remoting of objects to a destination URI.
    /// </summary>
    public class WCFReplicator: WCFClientBase<IWCFReplicationRecipient>
    {
        private string _queuePath = null;
        private Thread[] _backgroundThreads = null;
        private bool _isRunning = false;
        private string _destinationUri = Constants.NULL_STRING;
        private string _serviceName;
        private string _serviceConst;
        private string _lastExceptionMessage = "";
        private DateTime _lastExceptionTime = DateTime.MinValue;
        private const Int32 THREAD_COUNT = 4;
        private MessageQueue _queue;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceName"></param>
        public WCFReplicator(string serviceName, string serviceconst,string queuename)
        {
            RollingFileLogger.Instance.EnterMethod(serviceconst, "WCFReplicator", "Constructor", new string[] { serviceName, serviceconst, queuename });
            _serviceName = serviceName;
            _serviceConst = serviceconst;
            _queuePath = @".\private$\Repl_" + queuename;
            
        }

        /// <summary>
        /// Sets the Uri that objects will be replicated to.
        /// </summary>
        /// <param name="destinationUri">The Service Manager Uri that objects will be replicated to.</param>
        public void SetDestinationUri(string destinationUri)
        {
            _destinationUri = destinationUri;
        }

        /// <summary>
        /// Starts the background processing of the oubound replication queue.
        /// </summary>
        public void Start()
        {
            try
            {
                RollingFileLogger.Instance.EnterMethod(_serviceConst, "WCFReplicator", "Start");
                if (!MessageQueue.Exists(_queuePath))
                {
                    RollingFileLogger.Instance.LogInfoMessage(_serviceConst, "WCFReplicator", "Creating queue:",_queuePath);
                    _queue = MessageQueue.Create(_queuePath);
                    _queue.MaximumQueueSize = 10240;
                }
                else
                {
                    _queue = new MessageQueue(_queuePath);
                }
                _queue.Formatter = new BinaryMessageFormatter();

                _isRunning = true;

                if (_backgroundThreads == null)
                {
                    _backgroundThreads = new Thread[THREAD_COUNT];
                    for (Int32 i = 0; i < _backgroundThreads.Length; i++)
                    {
                        _backgroundThreads[i] = new Thread(new ThreadStart(processOutboundReplicationQueue));
                        _backgroundThreads[i].IsBackground = true;
                        _backgroundThreads[i].Start();
                    }
                }
                RollingFileLogger.Instance.LeaveMethod(_serviceConst, "WCFReplicator", "Start");
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConst, "WCFReplicator-Start", ex);
                throw new ServiceBoundaryException(_serviceName, "Error starting Replicator.", ex);
            }
        }

        /// <summary>
        /// Stops the outbound replication queue processor. 
        /// </summary>
        public void Stop()
        {
            try
            {
                _isRunning = false;

                if (_backgroundThreads != null)
                {

                    for (Int32 i = 0; i < _backgroundThreads.Length; i++)
                    {
                        _backgroundThreads[i].Join(new TimeSpan(0, 0, 2));
                        _backgroundThreads[i].Abort();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(_serviceName, "Error stopping Replicator", ex);
            }
        }


        /// <summary>
        /// Adds a IReplicable object to the outbound replication queue.
        /// </summary>
        /// <param name="replicableObject">The IReplicable object which is to be replicated.</param>
        public void Enqueue(IReplicable replicableObject)
        {
            try
            {
                RollingFileLogger.Instance.EnterMethod(_serviceConst, "WCFReplicator", "Enqueue",new Object[]{replicableObject});
                MessageQueue queue = new MessageQueue(_queuePath);
                queue.Formatter = new BinaryMessageFormatter();
                queue.Send(replicableObject.GetReplicationPlaceholder());
                queue.Dispose();
                RollingFileLogger.Instance.LeaveMethod(_serviceConst, "WCFReplicator", "Enqueue");
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConst, "WCFReplicator-Enqueue", ex);
                throw new ServiceBoundaryException(_serviceName, "Error performing Matchnet.Replicator.Enqueue().", ex);
            }
        }


        public void Enqueue(IReplicationAction replicationAction)
        {
            try
            {
                MessageQueue queue = new MessageQueue(_queuePath);
                queue.Formatter = new BinaryMessageFormatter();
                queue.Send(replicationAction);
                queue.Dispose();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(_serviceName, "Error performing Matchnet.Replicator.Enqueue().", ex);
            }
        }

        /// <summary>
        /// Processes outbound replication queue.
        /// </summary>
        private void processOutboundReplicationQueue()
        {



            RollingFileLogger.Instance.EnterMethod(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue");
            TimeSpan timeout = new TimeSpan(0, 0, 6);
            IReplicable replicableObject = null;
            ReplicationPlaceholder replicationPlaceholder = null;
            object body = null;

            while (_isRunning == true)
            {
                string cacheKey = null;
                WcfServiceBasicHttpClient<IWCFReplicationRecipient> client = null;
                try
                {
                    body = null;
                    replicableObject = null;
                    replicationPlaceholder = null;

                    try
                    {
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue Receiving Q message",null);
                        System.Messaging.Message message = _queue.Receive();
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue Received Q message", null);
                        body = message.Body;
                        message.Dispose();
                    }
                    catch (MessageQueueException mEx)
                    {
                        if (mEx.Message != "Timeout for the requested operation has expired.")
                        {
                            RollingFileLogger.Instance.LogException(_serviceConst, "WCFReplicator-processOutboundReplicationQueue - timeout", mEx);
                            throw mEx;
                        }
                        else
                        {
                            RollingFileLogger.Instance.LogException(_serviceConst, "WCFReplicator-processOutboundReplicationQueue -not timeout", mEx);
                        }
                    }

                    //System.Diagnostics.Trace.WriteLine("__repl " + body.GetType().Name);

                    if (body is ReplicationPlaceholder)
                    {
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue Received ReplicationPlaceholder", null);
                        replicationPlaceholder = body as ReplicationPlaceholder;
                        cacheKey = replicationPlaceholder.GetCacheKey();
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue Received ReplicationPlaceholder", cacheKey);
                        replicableObject = Cache.Instance[cacheKey] as IReplicable;
                        if (replicationPlaceholder != null)
                        {
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue getting clientproxy", cacheKey);
                            client = base.GetProxy(_serviceConst, _serviceName, _destinationUri);
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue getting clientproxy:" + _destinationUri, cacheKey);
                            client.Instance.Receive(replicableObject);
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue Receive", cacheKey);
                            

                        }
                    }
                    RollingFileLogger.Instance.LeaveMethod(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue");
                }
                catch (Exception ex)
                {
                    RollingFileLogger.Instance.LogException(_serviceConst, "WCFReplicator-processOutboundReplicationQueue 1st exception", ex);

                    try
                    {
                        if (replicableObject != null)
                        {
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConst, "WCFReplicator", "processOutboundReplicationQueue Re-Enqueue in exception", cacheKey);
                            this.Enqueue(replicableObject);
                        }
                        else
                        {
                            RollingFileLogger.Instance.LogException(_serviceConst, "WCFReplicator-processOutboundReplicationQueue 1st exception replicableobject null", ex);
                        }
                    }
                    catch (Exception enqueueEx)
                    {
                        RollingFileLogger.Instance.LogException(_serviceConst, "WCFReplicator-processOutboundReplicationQueue - exception in reenqueue", enqueueEx);
                        new ServiceBoundaryException(_serviceName, "Enqueue error: " + enqueueEx.ToString());
                    }

                    if (DateTime.Now > _lastExceptionTime || ex.Message != _lastExceptionMessage)
                    {
                        ServiceBoundaryException sbEx = new ServiceBoundaryException(_serviceName, "Service Replication Failure (cacheKey: " + cacheKey + ")", ex, replicableObject);
                        _lastExceptionTime = DateTime.Now.AddMinutes(1);
                        _lastExceptionMessage = ex.Message;
                    }

                    Thread.Sleep(5000);
                }
                finally
                {
                    if(client != null)
                    client.Dispose();
                }

            }
        }
    }
}
