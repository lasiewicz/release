﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Threading;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;

namespace Spark.WCFClient
{
    public abstract class WCFClientBase<T>
        where T: class
    {
        protected WCFClientSettings _settings;
        protected Cache _cache;

        protected WCFClientBase()
		{
            _cache = Cache.Instance;
			
		}

        protected virtual WcfServiceBasicHttpClient<T> GetProxy(string serviceconst, string servicename,string uri)
        {
            try
            {
                WCFClientSettings settings = (WCFClientSettings)_cache.Get(WCFClientSettings.GetCacheKey(serviceconst));
                if (settings == null)
                {
                    settings = WCFClientHelper.GetServiceSettings(serviceconst);
                    _cache.Insert(settings);

                }
                UriBuilder uribld = new UriBuilder(uri);
               
                WcfServiceBasicHttpClient<T> _client = WcfServiceBasicHttpClient<T>.Create(uribld.Host, uribld.Port, servicename, settings);

                return _client;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected virtual WcfServiceBasicHttpClient<T> GetProxy(string serviceconst, string servicename)
        {
            try
            {
                WCFClientSettings settings = (WCFClientSettings)_cache.Get(WCFClientSettings.GetCacheKey(serviceconst));
                if (settings == null)
                {
                    settings = WCFClientHelper.GetServiceSettings(serviceconst);
                    _cache.Insert(settings);

                }

                string uri = AdapterConfigurationSA.GetServicePartition(serviceconst, PartitionMode.Random).ToUri(servicename);
                
                UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                string host = uriBuilder.Host;
                int port = uriBuilder.Port;
                string overrideHostName = WCFClientHelper.GetConfigurationSetting(serviceconst.ToUpper() + "_SA_HOST_OVERRIDE");
                if (overrideHostName.Length > 0)
                {
                    host = overrideHostName;
                }


                WcfServiceBasicHttpClient<T> _client = WcfServiceBasicHttpClient<T>.Create(host, port, servicename, settings);

                return _client;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected virtual WcfServiceBasicHttpClient<T> GetProxy(string serviceconst, string servicename, PartitionMode mode, int id)
        {
            try
            {
                WCFClientSettings settings = (WCFClientSettings)_cache.Get(WCFClientSettings.GetCacheKey(serviceconst));
                if (settings == null)
                {
                    settings = WCFClientHelper.GetServiceSettings(serviceconst);
                    _cache.Insert(settings);

                }

                string uri = AdapterConfigurationSA.GetServicePartition(serviceconst, mode, id).ToUri(servicename);

                UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                string host = uriBuilder.Host;
                int port = uriBuilder.Port;
                string overrideHostName = WCFClientHelper.GetConfigurationSetting(serviceconst.ToUpper() + "_SA_HOST_OVERRIDE");
                if (overrideHostName.Length > 0)
                {
                    host = overrideHostName;
                }


                WcfServiceBasicHttpClient<T> _client = WcfServiceBasicHttpClient<T>.Create(host, port, servicename, settings);

                return _client;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected virtual WcfServiceBasicHttpClient<T> GetProxy(string serviceconst, string servicename, PartitionMode mode,Guid guid)
        {
            try
            {
                WCFClientSettings settings = (WCFClientSettings)_cache.Get(WCFClientSettings.GetCacheKey(serviceconst));
                if (settings == null)
                {
                    settings = WCFClientHelper.GetServiceSettings(serviceconst);
                    _cache.Insert(settings);

                }

                string uri = AdapterConfigurationSA.GetServicePartition(serviceconst, mode,guid).ToUri(servicename);

                UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                string host = uriBuilder.Host;
                int port = uriBuilder.Port;
                string overrideHostName = WCFClientHelper.GetConfigurationSetting(serviceconst.ToUpper() + "_SA_HOST_OVERRIDE");
                if (overrideHostName.Length > 0)
                {
                    host = overrideHostName;
                }


                WcfServiceBasicHttpClient<T> _client = WcfServiceBasicHttpClient<T>.Create(host, port, servicename, settings);

                return _client;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       

    }
}
