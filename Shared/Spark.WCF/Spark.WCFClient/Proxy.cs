﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.WCFClient
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;

   
        /// <summary>
        /// Generic proxy class for a WCF services.
        /// </summary>
        /// <typeparam name="TProxy">The type of WCF service proxy to wrap.</typeparam>
        /// <typeparam name="TChannel">The type of WCF service interface to wrap.</typeparam>
        public class GenericServiceProxyAgent<TProxy, TChannel> : IDisposable
            where TProxy : ClientBase<TChannel>, new()
            where TChannel : class
        {
            /// <summary>
            /// variable to hold proxy instance
            /// </summary>
            private TProxy _proxy;

            /// <summary>
            /// Gets the WCF service proxy wrapped by this instance.
            /// </summary>
            protected TProxy Proxy
            {
                get
                {
                    if (_proxy != null)
                    {
                        return _proxy;
                    }
                    else
                    {
                        throw new ObjectDisposedException("GenericServiceProxyAgent");
                    }
                }
            }

            /// <summary>
            /// default constructor.
            /// </summary>
            protected GenericServiceProxyAgent()
            {
                _proxy = new TProxy();
            }

            /// <summary>
            /// Disposes the current instance.
            /// </summary>
            public void Dispose()
            {
                try
                {
                    if (_proxy != null)
                    {
                        if (_proxy.State != CommunicationState.Faulted)
                        {
                            _proxy.Close();
                        }
                        else
                        {
                            _proxy.Abort();
                        }
                    }
                }
                catch (FaultException unknownFault)
                {
                    System.Diagnostics.Debug.WriteLine("An unknown exception was received. " + unknownFault.Message);
                    _proxy.Abort();
                }

                catch (CommunicationException)
                {
                    System.Diagnostics.Debug.WriteLine("Service proxy encountred a communication exception and aborted");
                    _proxy.Abort();
                }
                catch (TimeoutException)
                {
                    System.Diagnostics.Debug.WriteLine("Service proxy encountred a timeout exception and aborted");
                    _proxy.Abort();
                }
                catch (Exception)
                {
                    System.Diagnostics.Debug.WriteLine("Service proxy encountred a unknown exception and aborted");
                    _proxy.Abort();
                    throw;
                }
                finally
                {
                    _proxy = null;
                }
            }
        }


}
