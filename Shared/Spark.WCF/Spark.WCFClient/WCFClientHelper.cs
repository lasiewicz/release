﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;


namespace Spark.WCFClient
{
    public class WCFClientHelper
    {

        public static string GetSettingString(Hashtable settings, string settingkey, string servicekey, string defaultvalue)
        {
            string key = String.Format("{0}_{1}", servicekey, settingkey).ToUpper();
            string setting = defaultvalue;
            if (settings.ContainsKey(key) && !String.IsNullOrEmpty(settings[key].ToString()))
            {
                setting = settings[key].ToString();
            }
            return setting;
        }


        public static void AddServiceSetting(WCFClientSettings settings, string settingkey)
        {
            try
            {
                string key = String.Format("{0}_{1}",  settings.ServiceConstant,settingkey).ToUpper();

                string setting = GetConfigurationSetting(key);
                if (!String.IsNullOrEmpty(setting) && !setting.Contains(key))
                {
                    settings.AddSetting(key, setting);
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static WCFClientSettings GetServiceSettings( string serviceconst)
        {
            WCFClientSettings settings = new WCFClientSettings();

            settings.ServiceConstant = serviceconst;
            WCFClientHelper.AddServiceSetting(settings, "MaxReceivedMessageSize");
            WCFClientHelper.AddServiceSetting(settings, "MaxBufferPoolSize");
            WCFClientHelper.AddServiceSetting(settings, "MaxArrayLength");
            WCFClientHelper.AddServiceSetting(settings, "MaxBytesPerRead");
            WCFClientHelper.AddServiceSetting(settings, "MaxDepth");
            WCFClientHelper.AddServiceSetting(settings, "OpenTimeout");
            WCFClientHelper.AddServiceSetting(settings, "CloseTimeout");
            WCFClientHelper.AddServiceSetting(settings, "ReceiveTimeout");
            WCFClientHelper.AddServiceSetting(settings, "SendTimeout");
            return settings;
           
        }
        public static string GetConfigurationSetting(string key)
        {
            try
            {
                return RuntimeSettings.GetSetting(key);
            }
            catch (Exception ex)
            {
                return "";
            }

        }
    }
}
