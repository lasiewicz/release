﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
namespace Spark.WCFClient
{
    public class WCFClientSettings:ICacheable
    {
        private const string CACHE_KEY_PREFIX = "WCFCLIENTSETTINGS";
        private Hashtable _settings;
        private int _cacheTTLSeconds = 60 * 60;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheMode = Matchnet.CacheItemMode.Absolute;

        string settingformat="{0}_{1}";
        
        public string ServiceConstant { get; set; }

        public void AddSetting(string key,string value)
        {
            try
            {
                if (_settings == null)
                    _settings = new Hashtable();

                string settingkey = String.Format(settingformat, ServiceConstant, key);
                _settings[settingkey] = value;
            }
            catch (Exception ex)
            {
               
            }

        }

        public string GetSetting(string key)
        {
            try
            {
                string settingkey=String.Format(settingformat,ServiceConstant,key);
                return _settings[settingkey].ToString();
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public string GetSetting(string key, string defaultsetting)
        {
            try
            {
                 string settingkey=String.Format(settingformat,ServiceConstant,key);
                return _settings[settingkey].ToString();
            }
            catch (Exception ex)
            {
                return defaultsetting;
            }

        }
        #region ICacheable Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public static string GetCacheKey(string serviceconstant)
        {
            return CACHE_KEY_PREFIX + "_" + serviceconstant.ToUpper();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            string key = "";

            key = GetCacheKey(ServiceConstant);

            return key;
        }


        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return _cacheMode;
            }
            set
            {
                _cacheMode = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }


        #endregion

    }
}
