﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Net.Security;
using System.ServiceModel.Description;

namespace Spark.WCFClient
{
    public class HttpBindingUtility
    {
        public static Int32 MaxConnections { get; private set; }

        /// <summary>
        /// The default is 65,536 bytes.
        /// </summary>
        public static Int64 MaxReceivedMessageSize { get; private set; }

        /// <summary>
        /// The default is 65,536 bytes
        /// </summary>
        public static Int64 MaxBufferPoolSize { get; private set; }

        public static Int32 MaxArrayLength { get; private set; }
        public static Int32 MaxBytesPerRead { get; private set; }
        public static Int32 MaxDepth { get; private set; }

        public static TimeSpan OpenTimeout { get; private set; }
        public static TimeSpan CloseTimeout { get; private set; }
        public static TimeSpan ReceiveTimeout { get; private set; }
        public static TimeSpan SendTimeout { get; private set; }
        /// <summary>
        /// Static initializer for default values in each build configuration.
        /// </summary>
        static HttpBindingUtility()
        {
            MaxConnections = 10;
            MaxReceivedMessageSize = 4194304;				// 4MB The default is 65,536 bytes
            MaxBufferPoolSize = 1048576;					// 1MB default is 65,536 bytes
            OpenTimeout = new TimeSpan(0, 0, 30);
            CloseTimeout = new TimeSpan(0, 0, 30);
            ReceiveTimeout = new TimeSpan(0, 0, 30);
            SendTimeout = new TimeSpan(0, 0, 30);
            MaxArrayLength = 524288;					// 512KB default is 16,384 bytes
            MaxBytesPerRead = 16384;					// 16KB default is 4,096 bytes
            MaxDepth = 96;									// 96 default is 32
        }

        public static WSHttpBinding CreateNetHttpBinding()
        {
            WSHttpBinding binding = new WSHttpBinding(SecurityMode.None);

            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            
            binding.MaxReceivedMessageSize = MaxReceivedMessageSize;
            binding.MaxBufferPoolSize = MaxBufferPoolSize;

            binding.ReaderQuotas.MaxArrayLength = MaxArrayLength;
            binding.ReaderQuotas.MaxBytesPerRead = MaxBytesPerRead;
            binding.ReaderQuotas.MaxDepth = MaxDepth;

            //not allowed by partially trusted 
            //binding.MaxBufferSize = 262144;							//256KB default is 65,536 bytes

            binding.OpenTimeout = OpenTimeout;
            binding.CloseTimeout = CloseTimeout;
            binding.ReceiveTimeout = ReceiveTimeout;
            binding.SendTimeout = SendTimeout;

           

            return binding;
        }

    

        public static BasicHttpBinding CreateNetBasicHttpBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.None);

            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;


            binding.MaxReceivedMessageSize = MaxReceivedMessageSize;
            binding.MaxBufferPoolSize = MaxBufferPoolSize;

            binding.ReaderQuotas.MaxArrayLength = MaxArrayLength;
            binding.ReaderQuotas.MaxBytesPerRead = MaxBytesPerRead;
            binding.ReaderQuotas.MaxDepth = MaxDepth;

            //not allowed by partially trusted 
            //binding.MaxBufferSize = 262144;							//256KB default is 65,536 bytes

            binding.OpenTimeout = OpenTimeout;
            binding.CloseTimeout = CloseTimeout;
            binding.ReceiveTimeout = ReceiveTimeout;
            binding.SendTimeout = SendTimeout;
            binding.MessageEncoding = WSMessageEncoding.Text;
            return binding;
        }

        public static BasicHttpBinding CreateNetBasicHttpBinding(WCFClientSettings settings)
        {

            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.None);

            if (settings == null)
            {
                return CreateNetBasicHttpBinding();
            }


            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;

            long longsetting = 0;
            
            long.TryParse(settings.GetSetting("MaxReceivedMessageSize",MaxReceivedMessageSize.ToString()),out longsetting);
            binding.MaxReceivedMessageSize = longsetting;

            long.TryParse(settings.GetSetting("MaxBufferPoolSize",  MaxBufferPoolSize.ToString()), out longsetting);
            binding.MaxBufferPoolSize = MaxBufferPoolSize;

            long.TryParse(settings.GetSetting("MaxArrayLength",  MaxArrayLength.ToString()), out longsetting);
            binding.ReaderQuotas.MaxArrayLength = MaxArrayLength;

            long.TryParse(settings.GetSetting("MaxBytesPerRead",  MaxBytesPerRead.ToString()), out longsetting);
            binding.ReaderQuotas.MaxBytesPerRead = MaxBytesPerRead;

            long.TryParse(settings.GetSetting("MaxDepth",  MaxDepth.ToString()), out longsetting);
            binding.ReaderQuotas.MaxDepth = MaxDepth;

            //not allowed by partially trusted 
            //binding.MaxBufferSize = 262144;							//256KB default is 65,536 bytes
            long.TryParse(settings.GetSetting("OpenTimeout",  OpenTimeout.ToString()), out longsetting);
            binding.OpenTimeout = OpenTimeout;

            long.TryParse(settings.GetSetting("CloseTimeout",  CloseTimeout.ToString()), out longsetting);
            binding.CloseTimeout = CloseTimeout;

            long.TryParse(settings.GetSetting("ReceiveTimeout",  ReceiveTimeout.ToString()), out longsetting);
            binding.ReceiveTimeout = ReceiveTimeout;

            long.TryParse(settings.GetSetting("SendTimeout",  SendTimeout.ToString()), out longsetting);
            binding.SendTimeout = SendTimeout;
            binding.MessageEncoding = WSMessageEncoding.Text;
            return binding;
        }
   


        public static EndpointAddress CreateEndpointAddress(string host, int port, string servicename)
        {

            string formatteduri = "http://{0}:{1}/{2}.svc";
         
            string serviceuri = null;
            serviceuri = String.Format(formatteduri, host,port,servicename);
           
            return new EndpointAddress(serviceuri);
        }
        
    }
}
