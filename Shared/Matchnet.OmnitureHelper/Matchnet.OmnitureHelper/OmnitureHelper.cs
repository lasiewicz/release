﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.Region;

namespace Matchnet.OmnitureHelper
{
    public class OmnitureHelper
    {
        public const int MAX_REGION_STRING_LENGTH = 40;
        public const int REGIONID_USA = 223;

        public static string GetGender(int genderMask)
        {
            string gender;

            genderMask = genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE;

            gender = (genderMask == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "Female" : "Male";

            return gender;
        }

        public static int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;
            return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }

        public static string GetRegionString(Member.ServiceAdapters.Member pMember, int brandID, int languageID, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry)
        {
            return GetRegionString(pMember, Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID), languageID, showPostalCode, showAbbreviatedState, showAbbreviatedCountry);
        }

        public static string GetRegionString(Member.ServiceAdapters.Member pMember, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, int languageID, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            int regionID = pMember.GetAttributeInt(brand, "regionid");

            string regionString = string.Empty;

            if (regionID > 0)
            {
                // regionLanguage = new RegionLanguage();
                // regionLanguage.PopulateHierarchy(regionAttribute, g.Brand.Site.LanguageID);
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);

                // Incrementally build up the regionString with commas where necessary
                // based on the contents being empty or not before and after each comma
                // This made for CI/CP and tested on CL/GL 
                // start WEL
                regionString = regionLanguage.CityName;

                string stateString = string.Empty;

                // TT #13622, hide the state name if the region is not in the same country as the Site's default region.
                if (isDefaultRegion(regionLanguage, brand))
                {
                    if (showAbbreviatedState && regionLanguage.CountryRegionID == REGIONID_USA)
                    {
                        stateString = regionLanguage.StateAbbreviation;
                    }
                    else
                    {
                        stateString = regionLanguage.StateDescription;
                    }
                }

                if (isNotBlankString(regionString) && isNotBlankString(stateString))
                {
                    regionString += ", ";
                }
                regionString += stateString;

                if (!isDefaultRegion(regionLanguage, brand))
                {
                    string countryString = string.Empty;
                    if (showAbbreviatedCountry)
                    {
                        countryString = regionLanguage.CountryAbbreviation;
                    }
                    else
                    {
                        countryString = regionLanguage.CountryName;
                    }
                    if (isNotBlankString(regionString) && isNotBlankString(countryString))
                    {
                        regionString += ", ";
                    }
                    regionString += countryString;
                }

                // end WEL

                if (regionString != null && regionString.Length > MAX_REGION_STRING_LENGTH)
                {
                    regionString = regionString.Substring(0, MAX_REGION_STRING_LENGTH - 3) + "...";
                }
            }
            else
            {
                regionString = "N/A";
            }
            return regionString;
        }

        public static string GetRegionString(int regionID, Content.ValueObjects.BrandConfig.Brand brand, int languageID, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry)
        {
            string regionString = string.Empty;

            if (regionID > 0)
            {
                // regionLanguage = new RegionLanguage();
                // regionLanguage.PopulateHierarchy(regionAttribute, g.Brand.Site.LanguageID);
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);

                // Incrementally build up the regionString with commas where necessary
                // based on the contents being empty or not before and after each comma
                // This made for CI/CP and tested on CL/GL 
                // start WEL
                regionString = regionLanguage.CityName;

                string stateString = string.Empty;

                // TT #13622, hide the state name if the region is not in the same country as the Site's default region.
                if (isDefaultRegion(regionLanguage, brand))
                {
                    if (showAbbreviatedState && regionLanguage.CountryRegionID == REGIONID_USA)
                    {
                        stateString = regionLanguage.StateAbbreviation;
                    }
                    else
                    {
                        stateString = regionLanguage.StateDescription;
                    }
                }

                if (isNotBlankString(regionString) && isNotBlankString(stateString))
                {
                    regionString += ", ";
                }
                regionString += stateString;

                if (!isDefaultRegion(regionLanguage, brand))
                {
                    string countryString = string.Empty;
                    if (showAbbreviatedCountry)
                    {
                        countryString = regionLanguage.CountryAbbreviation;
                    }
                    else
                    {
                        countryString = regionLanguage.CountryName;
                    }
                    if (isNotBlankString(regionString) && isNotBlankString(countryString))
                    {
                        regionString += ", ";
                    }
                    regionString += countryString;
                }

                // end WEL

                if (regionString != null && regionString.Length > MAX_REGION_STRING_LENGTH)
                {
                    regionString = regionString.Substring(0, MAX_REGION_STRING_LENGTH - 3) + "...";
                }
            }
            else
            {
                regionString = "N/A";
            }
            return regionString;
        }

        private static bool isDefaultRegion(RegionLanguage regionLanguage, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            if (regionLanguage.CountryRegionID == brand.Site.DefaultRegionID)
            {
                return true;
            }
            return false;
        }

        private static bool isNotBlankString(string s)
        {
            if (s != String.Empty && s != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetSubscriberStatus(Member.ServiceAdapters.Member member, int siteID)
        {
            if (member.IsPayingMember(siteID))
            {
                return "Subscriber";
            }
            else
            {
                return "Registered";
            }
        }

        public static string GetProfileCompetionPercentage(Matchnet.Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            bool hasPhoto = false;
            int currentStep;
            int completedSteps = 0;
            int stepValue = 1;
            if (member.GetPhotos(brand.Site.Community.CommunityID).Count > 0)
            {
                hasPhoto = true;
            }
            string regsteps = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("REGISTRATION_STEPS", brand.Site.Community.CommunityID, brand.Site.SiteID);
            int stepCount = Matchnet.Conversion.CInt(regsteps) + 1; //add 1 for photo upload page
            int completedStepMask = member.GetAttributeInt(brand, "NextRegistrationActionPageID");
            for (currentStep = 1; currentStep <= stepCount; currentStep++)
            {
                if (currentStep != stepCount)
                {
                    //regular steps
                    if ((completedStepMask & stepValue) == stepValue)
                    {
                        completedSteps++;
                    }
                }
                else
                {
                    //photoupload
                    if (hasPhoto)
                    {
                        completedSteps++;
                    }
                }

                stepValue = stepValue * 2;
            }

            return Convert.ToString(Convert.ToInt32((double)completedSteps / (double)stepCount * 100).ToString() + "%");
        }


    }

}
