using System;
using System.Runtime.Serialization;

namespace Matchnet.Exceptions
{

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class SAException : ExceptionBase, ISerializable
	{
		private SAException()
		{
		
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		public SAException(string message) : this(message, null)
		{
			
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="innerException"></param>
		public SAException(Exception innerException) : this(innerException.Message, innerException, null)
		{
			
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public SAException(string message, Exception innerException) : this(message, innerException, null)
		{
			
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="valueObject"></param>
		public SAException(string message, Exception innerException, IValueObject valueObject) : base(message, innerException, valueObject)
		{
			
		}

	
		
		/// <summary>
		/// This protected constructor is used for deserialization.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SAException(SerializationInfo info, StreamingContext context) : base( info, context )
		{

		}

	}
}
