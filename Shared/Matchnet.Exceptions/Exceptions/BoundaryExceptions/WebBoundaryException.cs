using System;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace Matchnet.Exceptions
{

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class WebBoundaryException : BoundaryExceptionBase, ISerializable
	{
		private const string SOURCE = "WWW";

		private WebBoundaryException()
		{
		
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		public WebBoundaryException(string message) : base(SOURCE, message, null, null, false, Constants.NULL_INT, Constants.NULL_INT)
		{

		}
	

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="memberID"></param>
		/// <param name="websiteID"></param>
		public WebBoundaryException(string message, int memberID, int websiteID) : base(SOURCE, message, null, null, false, memberID, websiteID)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public WebBoundaryException(string message, Exception innerException) : base(SOURCE, message, innerException, null, false, Constants.NULL_INT, Constants.NULL_INT)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="logType"></param>
		public WebBoundaryException(string message, Exception innerException, EventLogEntryType logType) : base(SOURCE, message, innerException, null, false, Constants.NULL_INT, Constants.NULL_INT, logType)
		{

		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="memberID"></param>
		/// <param name="websiteID"></param>
		public WebBoundaryException(string message, Exception innerException, int memberID, int websiteID) : base(SOURCE, message, innerException, null, false, memberID, websiteID)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="valueObject"></param>
		public WebBoundaryException(string message, Exception innerException, IValueObject valueObject) : base(SOURCE, message, innerException, valueObject, false, Constants.NULL_INT, Constants.NULL_INT)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="isLogSuppressed"></param>
		public WebBoundaryException(string message, Exception innerException, bool isLogSuppressed) : base(SOURCE, message, innerException, null, isLogSuppressed, Constants.NULL_INT, Constants.NULL_INT)
		{

		}

		
		
		/// <summary>
		/// This protected constructor is used for deserialization.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WebBoundaryException(SerializationInfo info, StreamingContext context) : base( info, context )
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="valueObject"></param>
		/// <param name="isLogSuppressed"></param>
		/// <param name="memberID"></param>
		/// <param name="websiteID"></param>
		/// <param name="logType"></param>
		public WebBoundaryException(string source, string message, Exception innerException, IValueObject valueObject, bool isLogSuppressed, int memberID, int websiteID, EventLogEntryType logType) : base(source, message, innerException, valueObject, isLogSuppressed, memberID, websiteID, logType)
		{

		}

	}
}
