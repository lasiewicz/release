using System;
using System.Diagnostics;
using System.Runtime.Serialization;

using Matchnet;


namespace Matchnet.Exceptions
{

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public abstract class BoundaryExceptionBase : ExceptionBase, ISerializable
	{

		/// <summary>
		/// 
		/// </summary>
		protected BoundaryExceptionBase()
		{

		}
	

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="valueObject"></param>
		/// <param name="isLogSuppressed"></param>
		/// <param name="memberID"></param>
		/// <param name="websiteID"></param>
		public BoundaryExceptionBase(string source, string message, Exception innerException, IValueObject valueObject, bool isLogSuppressed, int memberID, int websiteID) : base(message, innerException, valueObject)
		{
			if(!isLogSuppressed)
			{
				//06112008 TL - Modified to output message of all inner exceptions
				Exception innerEx = innerException; 
				while (innerEx != null)
				{
					message += "\r\n\r\n" + innerEx.ToString();
					innerEx = innerEx.InnerException;
				}
				EventLog.WriteEntry(source, message, EventLogEntryType.Error);
				/*
				ExceptionEvent exceptionEvent = new ExceptionEvent(this.Dump);
				exceptionEvent.MemberID = memberID;
				exceptionEvent.WebsiteID = websiteID;
				exceptionEvent.SetEventType(this.GetType().Name.ToString());
				exceptionEvent.Raise();
				*/
			}
		}

		/// <summary>
		/// 06122008 TL - Added.
		/// Boundary Exception constructor that provides event logging with specified event log entry type
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="valueObject"></param>
		/// <param name="isLogSuppressed"></param>
		/// <param name="memberID"></param>
		/// <param name="websiteID"></param>
		/// <param name="logType"></param>
		public BoundaryExceptionBase(string source, string message, Exception innerException, IValueObject valueObject, bool isLogSuppressed, int memberID, int websiteID, EventLogEntryType logType) : base(message, innerException, valueObject)
		{
			if(!isLogSuppressed)
			{
				Exception innerEx = innerException; 
				while (innerEx != null)
				{
					message += "\r\n\r\n" + innerEx.ToString();
					innerEx = innerEx.InnerException;
				}
				EventLog.WriteEntry(source, message, logType);
			}
		}
		
		
		/// <summary>
		/// This protected constructor is used for deserialization.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BoundaryExceptionBase(SerializationInfo info, StreamingContext context) : base( info, context )
		{

		}

	}
}
