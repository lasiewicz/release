using System;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace Matchnet.Exceptions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ServiceBoundaryException : BoundaryExceptionBase, ISerializable
	{
		private ServiceBoundaryException()
		{
		
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		public ServiceBoundaryException(string source, string message) : base(source, message, null, null, false, Constants.NULL_INT, Constants.NULL_INT)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="logType"></param>
		public ServiceBoundaryException(string source, string message, EventLogEntryType logType) : base(source, message, null, null, false, Constants.NULL_INT, Constants.NULL_INT, logType)
		{

		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="memberID"></param>
		public ServiceBoundaryException(string source, string message, int memberID) : base(source, message, null, null, false, memberID, Constants.NULL_INT)
		{

		}
	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public ServiceBoundaryException(string source, string message, Exception innerException) : base(source, message, innerException, null, false, Constants.NULL_INT, Constants.NULL_INT)
		{

		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="memberID"></param>
		public ServiceBoundaryException(string source, string message, Exception innerException, int memberID) : base(source, message, innerException, null, false, memberID, Constants.NULL_INT)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="valueObject"></param>
		public ServiceBoundaryException(string source, string message, Exception innerException, IValueObject valueObject) : base(source, message, innerException, valueObject, false, Constants.NULL_INT, Constants.NULL_INT)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="valueObject"></param>
		/// <param name="memberID"></param>
		public ServiceBoundaryException(string source, string message, Exception innerException, IValueObject valueObject, int memberID) : base(source, message, innerException, valueObject, false, memberID, Constants.NULL_INT)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="isLogSuppressed"></param>
		public ServiceBoundaryException(string source, string message, Exception innerException, bool isLogSuppressed) : base(source, message, innerException, null, isLogSuppressed, Constants.NULL_INT, Constants.NULL_INT)
		{

		}

		
		
		/// <summary>
		/// This protected constructor is used for deserialization.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ServiceBoundaryException(SerializationInfo info, StreamingContext context) : base( info, context )
		{

		}


	}
}
