using System;
using System.Text;
using System.Runtime.Serialization;

namespace Matchnet.Exceptions
{
	/// <summary>
	/// 
	/// </summary>
	public abstract class ExceptionBase : ApplicationException, ISerializable
	{
		private const string EMPTY_EXCEPTION_DUMP = "\r\n\r\n";
		
		private System.Guid _guid = System.Guid.NewGuid();
		private System.DateTime _exceptionDate = System.DateTime.Now;
		private string _innerExceptionDump = EMPTY_EXCEPTION_DUMP;
		private IValueObject _valueObject = null;

		/// <summary>
		/// 
		/// </summary>
		protected ExceptionBase()
		{
			_innerExceptionDump = dumpException(null);
		}
	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		/// <param name="valueObject"></param>
		public ExceptionBase(string message, Exception innerException, IValueObject valueObject) : base(message, innerException)
		{
			_innerExceptionDump = dumpException(innerException);
			_valueObject = valueObject;
		}

		/// <summary>
		/// 
		/// </summary>
		public System.Guid Guid
		{
			get{return _guid;}
		}

		/// <summary>
		/// 
		/// </summary>
		public System.DateTime ExceptionDate
		{
			get{return _exceptionDate;}
		}

		/// <summary>
		/// 
		/// </summary>
		public IValueObject ValueObject
		{
			get{return _valueObject;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Dump
		{
			get{return dumpException(this);}
		}

		/// <summary>
		/// 
		/// </summary>
		public string InnerExceptionDump
		{
			get{return _innerExceptionDump;}
		}

		private string dumpException(Exception innerException)
		{		
			if(innerException == null)
			{
				return EMPTY_EXCEPTION_DUMP;
			}

			if(innerException.GetType().ToString().IndexOf("Matchnet.") > -1)
			{
				ExceptionBase ex =(ExceptionBase)innerException;

				StringBuilder dump = new StringBuilder(1000);

				// Document event details
				dump.Append("\r\n" + ex.Message + "\r\n");
				dump.Append("---------------------------\r\n");
				dump.Append("Type: " + ex.GetType().ToString() + "\r\n");
				dump.Append("GUID: " + ex.Guid.ToString() + "\r\n");
				dump.Append("Date: " + ex.ExceptionDate.ToString() + "\r\n");
				dump.Append("Source: " + ex.Source + "\r\n");

				if(ex.ValueObject != null)
				{
					// Convert Payload to string
					dump.Append(dumpValueObject());
				}
				else
				{
					// Payload is null
					dump.Append("Value Object: [NULL]\r\n");
				}

				// Finally attach the prior inner exception dump and do a replace to make it appear tabified
				dump.Append(ex.InnerExceptionDump.Replace("\r\n", "\r\n     "));


				return dump.ToString();
			}
			else
			{
				StringBuilder dump = new StringBuilder(1000);

				// Document event details
				dump.Append("---------------------------\r\n");
				dump.Append("Innermost Exception:\r\n");
				dump.Append(innerException.ToString());

				return dump.ToString();
			}
		}

		private string dumpValueObject()
		{
			StringBuilder sb = new StringBuilder(5000);
			sb.Append("\r\n\t--- BEGIN PAYLOAD OBJECT DUMP ---\r\n");
			walkObjectGraph(ref sb, _valueObject, 0);
			sb.Append("\t--- END PAYLOAD OBJECT DUMP ---\r\n");
			return sb.ToString();
		}
				

		private void walkObjectGraph(ref StringBuilder formattedMessage, object obj, int objectDepth)
		{
			bool _logObjectGraph = true;

			// ASSERTION: System shall not process null objects
			if(obj == null)
			{
				return;
			}

			// ASSERTION: System shall not process objects below a depth of zero (0) unless explicitly instructed
			if(!_logObjectGraph && objectDepth > 0)
			{
				return;
			}

			// ASSERTION: System shall not recursively walk below a depth of one (1)
			if(objectDepth > 1)
			{
				//formattedMessage.Append("!!!!! Maximum object depth reached will logging the object graph, deeper child objects could not be logged !!!!!\r\n");
				return;
			}


			try
			{
				System.Reflection.PropertyInfo[] properties = obj.GetType().GetProperties();

				// ASSERTION: System shall not attempt to process a null property array
				if(properties == null)
				{
					return;
				}

				formattedMessage.Append(getTab(objectDepth) + "\tPayload Object: " + obj.GetType().ToString() + "   (Object Depth: " + objectDepth.ToString() + ")\r\n");

				object innerObject = null;

				foreach(System.Reflection.PropertyInfo property in properties)
				{
					object propertyValue = property.GetValue(obj, null);

					if(property.PropertyType.Name.IndexOf("Exception") > -1 || property.PropertyType.Name.IndexOf("Matchnet.") > -1)
					{
						innerObject = propertyValue;
					}
					else
					{
						if(propertyValue != null)
						{
							formattedMessage.Append(getTab(objectDepth) + "\t[ " + property.Name + " ] = " + propertyValue.ToString() + "\r\n");
						}
					}
				}

				// Attempt to walk the inner object
				walkObjectGraph(ref formattedMessage, innerObject, objectDepth + 1);
			}
			catch
			{
				return; // Bail out if an exception occurs
			}
		}

		private string getTab(int depth)
		{
			depth = depth * 5;
			return "".PadLeft(depth, char.Parse(" "));
		}

		
		
		/// <summary>
		/// This protected constructor is used for deserialization.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExceptionBase(SerializationInfo info, StreamingContext context) : base( info, context )
		{

		}

	}
}
