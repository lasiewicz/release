using System;
using System.Collections;
using System.Threading;
using System.Messaging;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;


using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;


namespace Matchnet.Replication
{
	/// <summary>
	/// Performs synchronous queuing of object for replication and asynchronous remoting of objects to a destination URI.
	/// </summary>
	public class Replicator
	{
		private string _queuePath = null;
		private Thread[] _backgroundThreads = null;
		private bool _isRunning = false;
		private string _destinationUri = Constants.NULL_STRING;
		private string _serviceName;
		private string _lastExceptionMessage = "";
		private DateTime _lastExceptionTime = DateTime.MinValue;
		private const Int32 THREAD_COUNT = 4;
		private MessageQueue _queue;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceName"></param>
		public Replicator(string serviceName)
		{
			_serviceName = serviceName;
			_queuePath = @".\private$\Repl_" + serviceName.Replace(".", "");
		}

		/// <summary>
		/// Sets the Uri that objects will be replicated to.
		/// </summary>
		/// <param name="destinationUri">The Service Manager Uri that objects will be replicated to.</param>
		public void SetDestinationUri(string destinationUri)
		{
			_destinationUri = destinationUri;
		}

		/// <summary>
		/// Starts the background processing of the oubound replication queue.
		/// </summary>
		public void Start()
		{
			try
			{
				if (!MessageQueue.Exists(_queuePath))
				{
					_queue = MessageQueue.Create(_queuePath);
					_queue.MaximumQueueSize = 20480;
				}
				else
				{
					_queue = new MessageQueue(_queuePath);
				}
				_queue.Formatter = new BinaryMessageFormatter();

				_isRunning = true;

				if (_backgroundThreads == null)
				{
					_backgroundThreads = new Thread[THREAD_COUNT];
					for (Int32 i = 0; i < _backgroundThreads.Length; i++)
					{
						_backgroundThreads[i] = new Thread(new ThreadStart(processOutboundReplicationQueue));
						_backgroundThreads[i].IsBackground = true;
						_backgroundThreads[i].Start();
					}
				}
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(_serviceName, "Error starting Replicator.", ex);
			}
		}

		/// <summary>
		/// Stops the outbound replication queue processor. 
		/// </summary>
		public void Stop()
		{
 			try
			{
				_isRunning = false;

				if (_backgroundThreads != null)
				{

					for (Int32 i = 0; i < _backgroundThreads.Length; i++)
					{
						_backgroundThreads[i].Join(new TimeSpan(0, 0, 2));
						_backgroundThreads[i].Abort();
					}
				}
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(_serviceName, "Error stopping Replicator", ex);
			}
		}

		
		/// <summary>
		/// Adds a IReplicable object to the outbound replication queue.
		/// </summary>
		/// <param name="replicableObject">The IReplicable object which is to be replicated.</param>
		public void Enqueue(IReplicable replicableObject)
		{
			try
			{
				MessageQueue queue = new MessageQueue(_queuePath);
				queue.Formatter = new BinaryMessageFormatter();
				queue.Send(replicableObject.GetReplicationPlaceholder());
				queue.Dispose();
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(_serviceName, "Error performing Matchnet.Replicator.Enqueue().", ex);
			}
		}


		public void Enqueue(IReplicationAction replicationAction)
		{
			try
			{
				MessageQueue queue = new MessageQueue(_queuePath);
				queue.Formatter = new BinaryMessageFormatter();
				queue.Send(replicationAction);
				queue.Dispose();
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(_serviceName, "Error performing Matchnet.Replicator.Enqueue().", ex);
			}
		}

		/// <summary>
		/// Processes outbound replication queue.
		/// </summary>
		private void processOutboundReplicationQueue()
		{
			IReplicationRecipient replicationRecipient = (IReplicationRecipient)Activator.GetObject(typeof(IReplicationRecipient), _destinationUri);
			IReplicationActionRecipient replicationActionRecipient = (IReplicationActionRecipient)Activator.GetObject(typeof(IReplicationActionRecipient), _destinationUri);
			TimeSpan timeout = new TimeSpan(0,0,6);
			IReplicable replicableObject = null;
			ReplicationPlaceholder replicationPlaceholder = null;
			object body = null;

			while (_isRunning == true)
			{
				string cacheKey = null;

				try
				{
					body = null;
					replicableObject = null;
					replicationPlaceholder = null;

					try
					{
						System.Messaging.Message message = _queue.Receive();
						body = message.Body;
						message.Dispose();
					}
					catch (MessageQueueException mEx)
					{
						if (mEx.Message != "Timeout for the requested operation has expired.")
						{
							throw mEx;
						}
					}

					//System.Diagnostics.Trace.WriteLine("__repl " + body.GetType().Name);

					if (body is ReplicationPlaceholder)
					{
						replicationPlaceholder = body as ReplicationPlaceholder;
						cacheKey = replicationPlaceholder.GetCacheKey();
						replicableObject = Cache.Instance[cacheKey] as IReplicable;
						if(replicableObject != null)
						{
							replicationRecipient.Receive(replicableObject);
						}
					}
					else if (body is IReplicationAction)
					{
						replicationActionRecipient.Receive(body as IReplicationAction);
					}
				}
				catch(Exception ex)
				{
					System.Diagnostics.Trace.WriteLine("__" + ex.ToString());

					try
					{
						if(replicableObject != null)
						{
							this.Enqueue(replicableObject);
						}
					}
					catch (Exception enqueueEx)
					{
						new ServiceBoundaryException(_serviceName, "Enqueue error: " + enqueueEx.ToString());
					}
						
					if (DateTime.Now > _lastExceptionTime || ex.Message != _lastExceptionMessage)
					{
                        try
                        {
                            ServiceBoundaryException sbEx = new ServiceBoundaryException(_serviceName, String.Format("Service Replication Failure, destinationURI:{0} (cacheKey: " + cacheKey + ")", _destinationUri), ex, replicableObject);
                            _lastExceptionTime = DateTime.Now.AddMinutes(1);
                            _lastExceptionMessage = ex.Message;
                        }
                        catch (Exception exInner)
                        {
                            string des2 = (_destinationUri == null) ? "null destinationUri" : _destinationUri;
                            string cacheKey2 = (cacheKey == null) ? "null cacheKey" : cacheKey;

                            ServiceBoundaryException sbEx = new ServiceBoundaryException(_serviceName, "Service Replication Failure Inner, destinationURI: " + des2 + ", cacheKey: " + cacheKey2);
                            _lastExceptionTime = DateTime.Now.AddMinutes(1);
                            _lastExceptionMessage = exInner.Message;
                        }
					}

					Thread.Sleep(5000);	
				}

			}
		}
	}
}
