using System;

using Matchnet.Search.Interfaces;

namespace Matchnet.FAST.Query.Interfaces {
	/// <summary>
	/// A service which performs a search against the FAST search system
	/// </summary>
	public interface IFASTQueryService {

		/// <summary>
		/// Performas a search against the FAST search cluster
		/// </summary>
		/// <param name="query">the query to run</param>
		/// <returns>a resultset with 0 or more results of the search</returns>
		IMatchnetQueryResults Search(IMatchnetQuery query);
	}
}
