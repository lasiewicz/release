using System;

namespace Matchnet.FAST.Push.Interfaces
{

	/// <summary>
	/// An exclusion rule encapsulate an pass fail test that can get single attributes and 
	/// return whether this document instance should be indexed or removed essentially.
	/// </summary>
	public interface IFastExclusionRule
	{
		/// <summary>
		/// Gets or sets whether this instance is excluded or not
		/// </summary>
		bool IsExcluded
		{
			get;
			set;
		}

		/// <summary>
		/// Adds evidence to support / refute whether this instance is excluded.
		/// Note that null values should NOT be submitted!
		/// </summary>
		bool AddEvidence(IFastExclusionEvidence evidence);
	}

	public interface IFastExclusionEvidence
	{
		string EvidenceName
		{
			get;
			set;
		}

		object Evidence
		{
			get;
			set;
		}
	}

}
