using System;

namespace Matchnet.FAST.Push.Interfaces
{
	/// <summary>
	/// A pointer to a member profile 
	/// </summary>
	public interface IFastPushPointer
	{
		/// <summary>
		/// The memberID of this person
		/// </summary>
		int MemberID {
			get;
		}

		/// <summary>
		/// Community (DomainID in legacy)
		/// </summary>
		int CommunityID{
			get;
		}

		/// <summary>
		/// Site ID (Private LabelID in legacy)
		/// </summary>
		int SiteID{
			get;
		}

		/// <summary>
		/// The action that to perform on the member profile pointed to
		/// </summary>
		IndexAction ActionID{
			get;
			set;
		}


	}

	/// <summary>
	/// The action that the push service is requested to perform for this member
	/// </summary>
	public enum IndexAction : int {
		Delete = 1,
		Insert = 2,
		Update = 3
	}


}
