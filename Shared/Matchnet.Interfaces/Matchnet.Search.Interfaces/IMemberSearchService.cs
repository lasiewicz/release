using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Search.Interfaces
{
	/// <summary>
	/// Summary description for IMemberSearchService interface definition.
	/// </summary>
	public interface IMemberSearchService
	{
		/// <summary>
		/// Executes a member search in accordance with the supplied parameters
		/// </summary>
		/// <param name="pPreferences">A search preference collection.</param>
		/// <param name="pCommunityID">The community or domain id for the search.</param>
		/// <param name="pSiteID">The site or private label id for the search.</param>
		/// <param name="pStartRow">A starting row or position</param>
		/// <param name="pPageSize">The page size or number of results to return.</param>
		/// <returns>A MemberSearchResults value object representing the identified members corresponding to the search criteria.</returns>
		IMatchnetQueryResults Search(ISearchPreferences pPreferences, int pCommunityID, int pSiteID, SearchEngineType pSearchEngineType);
        IMatchnetQueryResults Search(ISearchPreferences pPreferences, int pCommunityID, int pSiteID, SearchEngineType pSearchEngineType, bool ignoreCache);
        IMatchnetQueryResults SearchForPushFlirtMatches(ISearchPreferences pPreferences, int memberID, int communityID, int siteID, int brandID, int numMatchesToReturn);
        ArrayList SearchDetailed(ISearchPreferences pPreferences, int pCommunityID, int pSiteID);
        ArrayList SearchExplainDetailed(ISearchPreferences pPreferences, int pCommunityID, int pSiteID);
    }
}
