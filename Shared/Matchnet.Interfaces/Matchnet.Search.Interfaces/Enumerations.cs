using System;

namespace Matchnet.Search.Interfaces
{

	/// <summary>
	/// Temporary enum for differentiating between FAST and Relational member searches.
	/// </summary>
	public enum SearchEngineType : int
	{
		FAST = 1,
		Relational = 2
	}

	/// <summary>
	/// Query Search Parameters. These are parameters used to indicate filtering - what I want to narrow by.
	/// </summary>
	public enum QuerySearchParam: int 
	{
        None = 0,
		AgeMax = 1,
		AgeMin = 2,
		AllTextFields = 3,
		AreaCode = 4,
		BodyType = 5,
		DisplayPhotosToGuests = 6,
		DomainID = 7,
		DrinkingHabits = 8,
		EducationLevel = 9,
		EssayWords = 10,
		Ethnicity = 11,
		GenderMask = 13,
		GeoDistance = 14,
		HasPhotoFlag = 15,
		HeightMax = 16,
		HeightMin = 17,
		InsertDate = 18,
		JDateEthnicity = 19,
		JDateReligion = 20,
		KeepKosher = 21,
		LanguageMask = 22,
		LastLoginDate = 23,
		LastUpdated = 24,
		Latitude = 25,
		Longitude = 26,
		MajorType = 27,
		MaritalStatus = 28,
		RegionID = 29,
		RegionIDCity = 30,
		RegionIDCountry = 31,
		RelationshipMask = 32,
		RelationshipStatus = 33,
		Religion = 34,
		SchoolID = 35,
		SexualIdentityType = 36,
		SmokingHabits = 37,
		Sorting = 38,
		SynagogueAttendance = 39,
		Username = 40,
		ZipCode = 41,
		Zodiac = 42,
		WeightMin = 43,
		WeightMax = 44,
		ActivityLevel = 45,
		ChildrenCount = 46,
        Custody = 47,
		MoreChildrenFlag = 48,
		RelocateFlag = 49,
		SubscriptionExpirationDate = 50,
		ColorCode = 51,
        Radius = 52,
        MemberID = 53,
        BlockedMemberIDs = 54,
        SearchRedesign30 = 55,
        PreferredAge=56,
        PreferredGenderMask=57,
        SubscriptionExpirationDateMin=58,
        SubscriptionExpirationDateMax=59,
        RegistrationDateMin =60,
        RegistrationDateMax =61,
        PreferredRelocation=62,
        PreferredHeight=63,
        PreferredSynagogueAttendance=64,
        PreferredJdateReligion=65,
        PreferredSmokingHabit=66,
        PreferredDrinkingHabit=67,
        PreferredJdateEthnicity=68,
        PreferredActivityLevel=69,
        PreferredKosherStatus=70,
        PreferredMaritalStatus=71,
        PreferredLanguageMask=72,
        PreferredDistance=73,
        PreferredRegionId=74,
        PreferredAreaCodes=75,
        PreferredHasPhotoFlag=76,
        PreferredEducationLevel=77,
        PreferredReligion=78,
        PreferredEthnicity=79,
        PreferredSexualIdentityType=80,
        PreferredRelationshipMask=81,
        PreferredRelationshipStatus=82,
        PreferredBodyType=83,
        PreferredZodiac=84,
        PreferredMajorType=85,
        PreferredMoreChildrenFlag=86,
        PreferredChildrenCount=87,
        PreferredCustody=88,
        PreferredColorCode=89,
        KeywordSearch=90,
        MaxNumberOfMatches=91,
        IPAddress = 92,
        EmailAddress = 93,
        FirstName = 94,
        LastName = 95,
        HasBirthDate = 96,
        HasMaritalStatus = 97,
        HasJDateReligion = 98,
        HasReligion = 99,
        HasKeepKosher = 100,
        HasSynagogueAttendance = 101,
        HasEducationLevel = 102,
        HasChildrenCount = 103,
        HasMoreChildrenFlag = 104,
        HasBodyType = 105,
        HasLanguageMask = 106,
        HasHeight = 107,
        HasSmokingHabits = 108,
        HasActivityLevel = 109,
        HasCustody = 110,
        HasDrinkingHabits = 111,
        HasRelocateFlag = 112,
        HasJDateEthnicity = 113,
        HasEthnicity = 114,
        HasMemberID = 115,
        Online = 116,
        RamahAlum = 117,
        ExcludeYnmMemberids = 118
}

	/// <summary>
	/// Query Sort Parameters. These are parameters used to indicate the ordering of the resultset.
	/// NB: This enum is also defined in C code in papi.h.  If it is changed here, it must be changed there also.
	/// </summary>
	public enum QuerySorting : int 
	{
        None = 0,
		JoinDate = 1,
		LastLogonDate = 2,
		Proximity = 3,
		Popularity  = 4,
		ColorCode = 5,
        KeywordRelevance = 6,
        MutualMatch = 7
	}

	/// <summary>
	/// The type of the search performed
	/// </summary>
	public enum SearchType
	{
        /// <summary>
        /// 
        /// </summary>
        None = 0,
		/// <summary>
		/// 
		/// </summary>
		Default = 1,
		/// <summary>
		/// A search request from the web site
		/// </summary>
		WebSearch = 2,
		/// <summary>
		/// A search request from MatchMail service
		/// </summary>
		MatchMail = 3,
        /// <summary>
        /// A search request from Your Matches web page
        /// </summary>
        YourMatchesWebSearch = 4,
        /// <summary>
        /// A search request from QuickSearch web page
        /// </summary>
        QuickSearchWebSearch = 5,
        /// <summary>
        /// A search request from ReverseSearch web page
        /// </summary>
        ReverseSearchWebSearch = 6,
        /// <summary>
        /// A search request from KeywordSearch web page
        /// </summary>
        KeywordSearchWebSearch = 7,
        /// <summary>
        /// A search request from AdvancedSearch web page
        /// </summary>
        AdvancedSearchWebSearch = 8,
        /// <summary>
        /// A search request from SecretAdmirer web page
        /// </summary>
        SecretAdmirerWebSearch = 9,
        /// <summary>
        /// A search request from ProfileFilmStrip web page
        /// </summary>
        ProfileFilmStripWebSearch = 10,
        /// <summary>
        /// Search request from Admintool
        /// </summary>
        AdminToolSearch = 11,
        /// <summary>
        /// Search request from API
        /// </summary>
        APISearch = 12,
        /// <summary>
        /// Members online searches
        /// </summary>
        MembersOnline = 13,

	    /// <summary>
	    ///     Introduced with Site Redesign to mimick Match Mail
	    /// </summary>
	    DailyMatches = 14
    }

    /// <summary>
    /// List of entry points to indicate which page a search was performed
    /// Used to support analytics loggings of which page a search was performed on
    /// Note: Do not use for backend processes like matchmail
    /// </summary>
    public enum SearchEntryPoint
    {
        None = 0,
        YourMatchesPage = 1,
        HomePage = 2,
        QuickSearchPage = 3,
        KeywordSearchPage = 4,
        ReverseSearchPage = 5,
        AdvancedSearchPage = 6,
        ProfileFilmStripMatches = 7,
        SecretAdmirerPage = 8,
        MiniSearchProfilePage = 9
    }

	/// <summary>
	/// Query Sort Direction Parameters. The sorting direction to apply to sorting parameters.
	/// </summary>
	public enum QuerySortDirectionParam: int 
	{
		Ascending = 1,
		Descending = 2
	}

	/// <summary>
	/// Possible values for flag query parameters to e1 (e.g., HasPhoto and IsOnline).
	/// NB: This enum is also defined in C code in papi.h.  If it is changed here, it must be changed there also.
	/// </summary>
	public enum QueryFlag : byte
	{
		NoTest  = 0,
		MustYes = 1,
		MustNo  = 2
	}

	public enum UpdateFlag : byte
	{
		NoChange	= 0,
		SetYes		= 1,
		SetNo		= 2
	}

    public enum SearchWeight : int
    {
        NotImportant = 0,
        Desirable = 1,
        SomewhatImportant = 2,
        Important = 3,
        VeryImportant = 4
    }
}
