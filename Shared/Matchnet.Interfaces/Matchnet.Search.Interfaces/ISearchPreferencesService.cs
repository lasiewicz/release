using System;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.Interfaces
{
	/// <summary>
	/// Summary description for ISearchPreferencesService.
	/// </summary>
	public interface ISearchPreferencesService
	{
		/// <summary>
		/// Retrieves a collection of search preferences based on the supplied parameters.
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
		/// <returns></returns>
		ISearchPreferences GetSearchPreferences(int pMemberID, int pCommunityID);

		/// <summary>
		/// Saves a collection of search preferences for the associated session, member and community objects.
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
		/// <param name="pSearchPreferences"></param>
		void Save(
			int pMemberID
			, int pCommunityID
			, ISearchPreferences pSearchPreferences);
	}

	public interface ISearchPreferences
	{
		int Add(ISearchPreference pSearchPreference);

		int Add(string pName, string pValue);

		string this[string name]
		{
			get;
			set;
		}

		bool ContainsKey(string key);

		string Value(string name);

	}

	public interface ISearchPreference
	{
		string Name
		{
			get;
			set;
		}

		string Value
		{
			get;
			set;
		}

	}
		
}
