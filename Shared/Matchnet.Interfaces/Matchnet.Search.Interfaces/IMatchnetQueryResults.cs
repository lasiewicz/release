using System;
using System.Collections;

namespace Matchnet.Search.Interfaces
{
	/// <summary>
	/// Represents a full result set, must include result list , but may have other stuff such as drill down, 
	/// expansion, categories, timing etc.
	/// </summary>
	public interface IMatchnetQueryResults
	{
		/// <summary>
		/// Gets a list of items which represent query result items
		/// </summary>
		IMatchnetResultItems Items
		{
			get;
		}

		/// <summary>
		/// Returns a number representing the total number of matches identified by the search engine.  This number is 
		/// not intended to represent the number of results returned by this query.
		/// </summary>
		int MatchesFound
		{
			get;
			set;
		}

		/// <summary>
		/// Returns a number presenting the number of ResultsItems contained in this query result.
		/// </summary>
		int MatchesReturned
		{
			get;
		}

	}

	/// <summary>
	/// Represents a list of items returned from a query
	/// </summary>
	public interface IMatchnetResultItems{
		/// <summary>
		/// Returns the whole collection of items
		/// </summary>
		ICollection List
		{
			get;
		}

		/// <summary>
		/// Returns the IMatchnetResultItem in [index] position (zero based).
		/// </summary>
		IMatchnetResultItem this[int index] 
		{
			get;
		}

		/// <summary>
		/// Number of items in this list
		/// </summary>
		int Count
		{
			get;
		}
	}

	/// <summary>
	/// Represents a member or profile
	/// </summary>
	public interface IMatchnetResultItem
	{
		/// <summary>
		/// Represents the member identifier associated with this result item.
		/// </summary>
		int MemberID
		{
			get;
		}

		/// <summary>
		/// Allows access to an additional result item, such as extra fields returned with the MemberID
		/// </summary>
		/// <remarks>This property allows access to extra data that may be returned with each result item.
		/// The AttributeName, if present in the implementation will return the value associated with it or String.Empty if not found or item is empty.</remarks>
		string this[string AttributeName]{
			get;
			set;
		}

        int MatchScore
        {
            get;
            set;
        }
	}

}
