﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Resources;

namespace Spark.Common.Localization
{
    public interface IResourceProvider
    {
        string GetResourceValue(int siteId, CultureInfo cultureInfo, ResourceGroupEnum @group, string key);
        string GetResourceValue(int siteId, CultureInfo cultureInfo, ResourceGroupEnum @group, string key, StringDictionary tokenMap);
        string GetResourceValue(int siteId, ResourceGroupEnum @group, CultureInfo cultureInfo, string key,
                                       StringDictionary tokenMap = null);
    }
}
