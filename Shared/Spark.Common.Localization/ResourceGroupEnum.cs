﻿
namespace Spark.Common.Localization
{
	public enum ResourceGroupEnum
	{
		Global,
		UserNotifications,
        InstantMessenger,
		LastLoginDate, 
        PushNotifications
	}
}
