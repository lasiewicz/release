﻿#region

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.Resources;

#endregion

namespace Spark.Common.Localization
{
    public class ResourceProvider : IResourceProvider
    {
        public static readonly ResourceProvider Instance = new ResourceProvider();

        private ResourceProvider()
        {
        }

        public string GetResourceValue(int siteId, CultureInfo cultureInfo, ResourceGroupEnum @group, string key)
        {
            return GetResourceValue(siteId, @group, cultureInfo, key);
        }

        /// <summary>
        /// </summary>
        /// <param name="cultureInfo"></param>
        /// <param name = "group">Resx group</param>
        /// <param name = "key">Resource constant</param>
        /// <param name = "tokenMap">Values to replace data tokens with</param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public string GetResourceValue(int siteId, CultureInfo cultureInfo, ResourceGroupEnum @group, string key, StringDictionary tokenMap)
        {
            return GetResourceValue(siteId, @group, cultureInfo, key, tokenMap);
        }

        public string GetResourceValue(int siteId, ResourceGroupEnum @group, CultureInfo cultureInfo, string key,
                                       StringDictionary tokenMap = null)
        {
            var siteKey = String.Format("Resources.{0}.{1}", @group, siteId);
            var resourceTarget = String.Format("{0}.{1}", GetType().Namespace, siteKey);
            try
            {
                var manager = new ResourceManager(resourceTarget, GetType().Assembly);
                var resourceValue = manager.GetString(key.ToUpper(), cultureInfo);

                return tokenMap == null ? resourceValue : ExpandTokens(resourceValue, tokenMap);
            }
            catch (Exception ex)
            {
                string culturename = (cultureInfo != null) ? cultureInfo.Name : "null cultureInfo";
                Trace.WriteLine(string.Format("Error in GetResourceValue key: {0}, siteID: {1}, culture name: {2}, error: ", key, siteId, culturename) + ex.Message);
                // error logging here  
                return key + "-" + resourceTarget;
            }
        }

        ///<summary>
        ///  For every token (key) value found in the source string, replace it with the corresponding
        ///  expansion (value).
        ///</summary>
        ///<param name = "source"></param>
        ///<param name = "tokenMap"></param>
        ///<returns></returns>
        private static string ExpandTokens(string source, StringDictionary tokenMap)
        {
            if (tokenMap == null)
            {
                // error logging here
                return source;
            }

            var result = source;
            foreach (DictionaryEntry entry in tokenMap)
            {
                var key = "((" + entry.Key.ToString().ToUpper(CultureInfo.InvariantCulture) + "))";
                // error logging here
                result = result.Replace(key, entry.Value.ToString());
            }
            return result;
        }
    }
}